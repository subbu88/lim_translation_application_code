/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_event_linux.c
 * @brief   Contains Linux event/timeout functions for Freescale libosa.
 */

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/select.h>
#include <sys/timerfd.h>
#include <sys/eventfd.h>

#include "osa_common.h"

static OsaThread _timer_thread = 0;
static OsaMutex _timer_mutex;
static int _update_fd = -1;
static List *_fd_list = NULL;

static void _linux_timer_thread(void *arg)
{
    OsaEvent *event;
    ListIter li;
    fd_set readfds;
    struct itimerspec new, old;
    uint64_t read_buf;
    int ret, nfds, max;

    /* Create an event fd that can be written to by the OSA to indicate to
     * this function that it needs to update it's readfds set.
     * Initialize initval to 0 to indicate it will block in select() until
     * written to. */
    _update_fd = eventfd(0, 0);
    if (_update_fd == -1) {
        lprintf(LOGMDL_OSA, LOGLVL_ERROR, ERRCODE_GENERAL_ERROR,
                "ERROR: Could not create update file descriptor.");
        osa_thread_exit(NULL);
    }

    /* Initialize to timer-disabled values. */
    new.it_value.tv_sec = 0;
    new.it_value.tv_nsec = 0;
    new.it_interval.tv_sec = 0;
    new.it_interval.tv_nsec = 0;

    while (1) {
        /* Create the list of readfds, starting with update fd. */
        FD_ZERO(&readfds);
        FD_SET(_update_fd, &readfds);
        max = _update_fd;

        container_foreach(list, _fd_list, li, event) {
            FD_SET(event->fd, &readfds);
            if (event->fd > max)
                max = event->fd;
        }

        nfds = max + 1;

        /* Block until any of the descriptors in readfds is written to. */
        ret = select(nfds, &readfds, NULL, NULL, NULL);
        if (ret == -1) {
            lprintf(LOGMDL_OSA, LOGLVL_ERROR, errno,
                "ERROR: select() returned error: %s\n", strerror(errno));
        } else {
            /* Find each fd that's been set (except _update_fd) and call the
             * callback function registered for it. */
            container_foreach(list, _fd_list, li, event) {
                if (FD_ISSET(event->fd, &readfds)) {
                    /* Cancel the timer on this fd then invoke the callback. */
                    timerfd_settime(event->fd, 0, &new, &old);
                    event->cb(event->arg);
                }
            }

            /* If _update_fd is set, read from it to reset. */
            if (FD_ISSET(_update_fd, &readfds))
                read(_update_fd, &read_buf, sizeof(read_buf));
        }
    }
}

int osa_timeout_init(void)
{
    int ret;

    ret = osa_mutex_create(&_timer_mutex, false);
    if (ret != ERRCODE_NO_ERROR)
        return ERRCODE_GENERAL_ERROR;

    return ERRCODE_NO_ERROR;
}

int osa_timeout_create(OsaEvent *ev, osa_event_cb cb, void *arg)
{
    int ret;

    ev->fd = timerfd_create(CLOCK_REALTIME, 0);
    if (ev->fd == -1) {
        return ERRCODE_OUT_OF_MEMORY;
    }

    ev->cb = cb;
    ev->arg = arg;

    osa_mutex_lock(&_timer_mutex);

    _fd_list = list_push(_fd_list, ev);

    if (_timer_thread == 0) {
        ret = osa_thread_create(&_timer_thread, NULL, _linux_timer_thread,
                                NULL);
        if (ret != ERRCODE_NO_ERROR) {
            osa_mutex_unlock(&_timer_mutex);
            return ret;
        }
    } else {
        /* Need to write exactly 8-bytes to the update fd to break out of the
         * select() in the update thread. */
        uint64_t write_temp = 1;
        write(_update_fd, &write_temp, sizeof(uint64_t));
    }

    osa_mutex_unlock(&_timer_mutex);

    return ERRCODE_NO_ERROR;
}

int osa_timeout_start(OsaEvent *ev, const OsaTimeval *time)
{
    struct itimerspec new, old;
    int ret;

    if (!ev || ev->fd == -1)
        return ERRCODE_INVALID_ARGUMENT;

    /* Map OsaTimeval -> itimerspec. */
    new.it_value.tv_sec = time->tv_sec + (time->tv_usec / 1000000);
    new.it_value.tv_nsec = (time->tv_usec % 1000000) * 1000;
    /* Set up interval to 0 meaning the timer expires only once, at the
     * it_value time (above). */
    new.it_interval.tv_sec = 0;
    new.it_interval.tv_nsec = 0;

    /* Set the time as relative to the current time. */
    ret = timerfd_settime(ev->fd, 0, &new, &old);
    if (ret == -1)
        return ERRCODE_INVALID_ARGUMENT;

    return ERRCODE_NO_ERROR;
}

int osa_timeout_cancel(OsaEvent *ev)
{
    struct itimerspec new, old;
    int ret;

    if (!ev || ev->fd == -1)
        return ERRCODE_INVALID_ARGUMENT;

    /* Set all values to 0 which disables the timer. */
    new.it_value.tv_sec = 0;
    new.it_value.tv_nsec = 0;
    new.it_interval.tv_sec = 0;
    new.it_interval.tv_nsec = 0;

    /* Set the time for the fd to be 0.  This disables the kernel timer for
     * this fd. */
    ret = timerfd_settime(ev->fd, 0, &new, &old);
    if (ret == -1)
        return ERRCODE_INVALID_ARGUMENT;

    return ERRCODE_NO_ERROR;
}

/* Utility find comparison function for list_find() call below. */
static bool _is_my_fd(const void *comp_data, const void *node_data)
{
    OsaEvent *ev = (OsaEvent*) node_data;

    if (*((int*) comp_data) == ev->fd)
        return true;

    return false;
}

int osa_timeout_destroy(OsaEvent *ev)
{
    List *node;
    uint64_t write_temp = 1;

    if (!ev)
        return ERRCODE_INVALID_ARGUMENT;

    if (ev->fd != -1)
        osa_timeout_cancel(ev);

    osa_mutex_lock(&_timer_mutex);

    /* Delete the list node from _fd_list. */
    node = list_find(_fd_list, (void*) &ev->fd, _is_my_fd);
    if (node)
        _fd_list = list_delete(_fd_list, node);

    osa_mutex_unlock(&_timer_mutex);

    close(ev->fd);

    /* Kick the timer thread out of select() to recreate the readfds, or to
     * exit the timer thread if there are no timers remaining. */
    write(_update_fd, &write_temp, sizeof(uint64_t));

    return ERRCODE_NO_ERROR;
}

