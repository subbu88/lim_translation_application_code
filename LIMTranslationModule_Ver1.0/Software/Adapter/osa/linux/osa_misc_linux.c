/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_misc_linux.c
 * @brief   Contains Linux miscellaneous functions for Freescale libosa.
 */

#include <stdlib.h>

#include "osa_common.h"

void osa_exit(int status)
{
    exit(status);
}

