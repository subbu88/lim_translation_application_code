/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    memory-linux.c
 * @brief   Contains memory allocation function implementations for linux.
 */

#include <stdlib.h>
#include <string.h>

#include "osa_common.h"


void *osa_malloc(size_t size)
{
    return malloc(size);
}

void *osa_calloc(size_t nmemb, size_t size)
{
    return calloc(nmemb, size);
}

void *osa_realloc(void *ptr, size_t size)
{
    return realloc(ptr, size);
}

void *osa_memcpy(void *dest, const void *src, size_t n)
{
    return memcpy(dest, src, n);
}

void *osa_memset(void *src, char c, size_t n)
{
    return memset(src, c, n);
}

void *osa_memmove(void *dest, const void *src, size_t n)
{
    return memmove(dest, src, n);
}

void osa_free(void *ptr)
{
    free(ptr);
}

void osa_set_memtype(void *ptr, uint32_t type)
{
}

