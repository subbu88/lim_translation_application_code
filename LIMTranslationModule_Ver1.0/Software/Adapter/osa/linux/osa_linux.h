
#ifndef OSA_LINUX_H
#define OSA_LINUX_H


/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_linux.h
 * @brief   Header includes specific to Linux.
 * @details This file includes Linux-specifc system headers for behavior such
 *          as standard I/O.
 */

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <mqueue.h>
#include <limits.h>

#define PATHNAME_SIZE PATH_MAX

typedef pthread_t OsaThread;
typedef pthread_mutex_t OsaMutex;
typedef pthread_cond_t OsaCondition;

typedef mqd_t OsaMq;
typedef sem_t OsaSem;

typedef void (*osa_event_cb)(void*);

typedef struct _OsaEvent {
    int fd;
    osa_event_cb cb;
    void *arg;
} OsaEvent;

#endif

