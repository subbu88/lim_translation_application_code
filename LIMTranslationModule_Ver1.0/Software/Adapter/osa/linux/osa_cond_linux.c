
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_cond_linux.c
 * @brief   Contains Linux CV functions for Freescale libosa.
 */

#include <time.h>
#include <pthread.h>
#include <errno.h>

#include "osa_common.h"

int osa_cond_create(OsaCondition *cond)
{
    int ret;

    DBG_entering

    ret = pthread_cond_init(cond, NULL);
    if (ret != 0) {
        if (ret == EBUSY)
            return ERRCODE_BUSY;
        else
            return ERRCODE_GENERAL_ERROR;
    }

    DBG_leaving

    return ERRCODE_NO_ERROR;
}

int osa_cond_destroy(OsaCondition *cond)
{
    int ret;

    DBG_entering

    ret = pthread_cond_destroy(cond);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    DBG_leaving

    return ERRCODE_NO_ERROR;
}

int osa_cond_wait(OsaCondition *cond, OsaMutex *mutex, uint32_t msec_timeout)
{
    int ret;

    DBG_entering

    /* Atomically release mutex and block until signaled. */
    if (msec_timeout == 0) {
        ret = pthread_cond_wait(cond, mutex);

    } else {
        struct timespec ts;

        /* pthread_cond_timedwait takes the timeout in absolute time so
         * calculate the current time then add the offset to it. */
        ret = clock_gettime(CLOCK_REALTIME, &ts);
        if (ret != 0)
            return ERRCODE_GENERAL_ERROR;

        if (msec_timeout >= 1000) {
            int num_seconds = msec_timeout / 1000;
            ts.tv_sec += num_seconds;
            msec_timeout -= (num_seconds * 1000);
        }

        ts.tv_nsec = (msec_timeout * 1000000);

        ret = pthread_cond_timedwait(cond, mutex, &ts);
    }

    if (ret == EINVAL)
        return ERRCODE_INVALID_ARGUMENT;
    else if (ret == EPERM)
        return ERRCODE_PERMISSION_DENIED;
    else if (ret == ETIMEDOUT)
        return ERRCODE_TIMED_OUT;
    else if (ret != 0)
        return ERRCODE_GENERAL_ERROR;

    DBG_leaving

    return ERRCODE_NO_ERROR;
}

int osa_cond_signal(OsaCondition *cond)
{
    int ret;

    DBG_entering

    ret = pthread_cond_signal(cond);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    DBG_leaving

    return ERRCODE_NO_ERROR;
}

int osa_cond_broadcast(OsaCondition *cond)
{
    int ret;

    DBG_entering

    ret = pthread_cond_broadcast(cond);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    DBG_leaving

    return ERRCODE_NO_ERROR;
}

