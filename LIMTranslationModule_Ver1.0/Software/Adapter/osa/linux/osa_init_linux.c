/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_init_linux.c
 * @brief   Contains Linux init functions for Freescale libosa.
 */

#include "osa_common.h"

/* Defined in osa_event_linux.c */
extern int osa_timeout_init(void);

int osa_init(void)
{
    return osa_timeout_init();
}

