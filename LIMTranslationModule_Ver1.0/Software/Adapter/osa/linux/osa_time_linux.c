/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_time_linux.c
 * @brief   Contains Linux time functions for Freescale libosa.
 */

#include <time.h>
#include <unistd.h>

#include "osa_common.h"
#include "error.h"

int osa_time_get(OsaTimeval *time)
{
    struct timespec timeout;
    int ret;

    ret = clock_gettime(CLOCK_REALTIME, &timeout);
    if (ret != 0)
        return ERRCODE_GENERAL_ERROR;

    time->tv_sec = timeout.tv_sec;
    time->tv_usec = timeout.tv_nsec / 1000;

    return ERRCODE_NO_ERROR;
}

void osa_time_delay(uint32_t msec)
{
    usleep(msec * 1000);
}

void osa_time_add(OsaTimeval *time, int32_t microseconds)
{
}

int osa_time_diff(const OsaTimeval *t1, const OsaTimeval *t2, OsaTimeval *diff)
{
    return ERRCODE_NO_ERROR;
}

