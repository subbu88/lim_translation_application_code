
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_isr_linux.c
 * @brief   Contains OSA functions related to ISR management.
 */

#include "osa_common.h"

void osa_isr_disable(void)
{
    /* Not possible on Linux - no action taken. */
}

void osa_isr_enable(void)
{
    /* Not possible on Linux - no action taken. */
}

