
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_thread_linux.c
 * @brief   Contains thread functions for Freescale libosa.
 */

#define _GNU_SOURCE

#include <pthread.h>
#include <limits.h>
#include <errno.h>
#include <sys/types.h>

#include <string.h>

#include "osa_common.h"

/* This is 2MB.  Default on most systems. */
#define OSA_LINUX_DEFAULT_STACK_SIZE  (2 * 1024 * 1024)
#define OSA_LINUX_DEFAULT_PRIORITY    OSA_SCHED_PRIORITY_NORMAL
#define OSA_LINUX_DEFAULT_POLICY      OSA_SCHED_POLICY_OTHER
#define OSA_LINUX_DEFAULT_RR_INTERVAL 5

/* Bytes */
#define OSA_LINUX_STACK_SIZE_MIN      PTHREAD_STACK_MIN
/* Pthreads doesn't define a max so set something large. */
#define OSA_LINUX_STACK_SIZE_MAX      10000000

/* Milliseconds */
#define OSA_LINUX_RR_INTERVAL_MIN     1
#define OSA_LINUX_RR_INTERVAL_MAX     1000

/* Map an OSA scheduling policy to an OSA value.  Return default on error. */
static int _osa_to_linux_sched_policy(OsaSchedPolicy policy)
{
    switch (policy) {
        case OSA_SCHED_POLICY_FIFO:
            return SCHED_FIFO;
        case OSA_SCHED_POLICY_RR:
            return SCHED_RR;
        case OSA_SCHED_POLICY_OTHER:
        default:
            return SCHED_OTHER;
    }
}

/* Map a Linux scheduling policy to an OSA value.  Return default on error. */
static OsaSchedPolicy _linux_to_osa_sched_policy(int policy)
{
    switch (policy) {
        case SCHED_FIFO:
            return OSA_SCHED_POLICY_FIFO;
        case SCHED_RR:
            return OSA_SCHED_POLICY_RR;
        case SCHED_OTHER:
        default:
            return OSA_SCHED_POLICY_OTHER;
    }
}

/* Map an OSA scheduling priority to a Linux value.  Return -1 on error. */
static int _osa_to_linux_sched_priority(OsaSchedPriority priority)
{
    /* Linux uses priority numbers starting at 0 and increasing.  0 means
     * non-RR/FIFO scheduling and the priority does not apply.  1 means lowest
     * priority real-time scheduling.
     * Increasing numbers is higher priority.
     *
     * We will attempt to map the 4 priority levels in the Freescale API to
     * Linux priorities. */
    switch (priority) {
        case OSA_SCHED_PRIORITY_LOW:
            return 1;
        case OSA_SCHED_PRIORITY_NORMAL:
            return 2;
        case OSA_SCHED_PRIORITY_HIGH:
            return 3;
        case OSA_SCHED_PRIORITY_URGENT:
            return 4;
        default:
            return -1;
    }
}

/* Map a Linux priority to an OSA value.  Return 'normal' on error. */
static OsaSchedPriority _linux_to_osa_sched_priority(int priority)
{
    switch (priority) {
        case 1:
            return OSA_SCHED_PRIORITY_LOW;
        case 2:
            return OSA_SCHED_PRIORITY_NORMAL;
        case 3:
            return OSA_SCHED_PRIORITY_HIGH;
        case 4:
            return OSA_SCHED_PRIORITY_URGENT;
        default:
            return OSA_SCHED_PRIORITY_NORMAL;
    }
}

int osa_thread_attr_init(OsaThreadAttr *attr)
{
    attr->stack_size = OSA_LINUX_DEFAULT_STACK_SIZE;
    attr->priority = OSA_LINUX_DEFAULT_PRIORITY;
    attr->policy = OSA_LINUX_DEFAULT_POLICY;
    attr->rr_interval = OSA_LINUX_DEFAULT_RR_INTERVAL;

    return ERRCODE_NO_ERROR;
}


void osa_thread_attr_destroy(OsaThreadAttr *attr)
{
}

int osa_thread_attr_set_stack_size(OsaThreadAttr *attr, size_t size)
{
    if (size < OSA_LINUX_STACK_SIZE_MIN ||
        size > OSA_LINUX_STACK_SIZE_MAX ||
        !attr)
        return ERRCODE_INVALID_ARGUMENT;

    attr->stack_size = size;

    return ERRCODE_NO_ERROR;
}

int osa_thread_attr_get_stack_size(const OsaThreadAttr *attr, size_t *size)
{
    if (!attr || !size)
        return ERRCODE_INVALID_ARGUMENT;

    *size = attr->stack_size;

    return ERRCODE_NO_ERROR;
}

int osa_thread_attr_set_sched_policy(OsaThreadAttr *attr, OsaSchedPolicy policy)
{
    if (_osa_to_linux_sched_policy(policy) == -1 || !attr)
        return ERRCODE_INVALID_ARGUMENT;

    attr->policy = policy;

    return ERRCODE_NO_ERROR;
}

int osa_thread_attr_get_sched_policy(const OsaThreadAttr *attr,
                                     OsaSchedPolicy *policy)
{
    if (!attr || !policy)
        return ERRCODE_INVALID_ARGUMENT;

    *policy = attr->policy;

    return ERRCODE_NO_ERROR;
}

int osa_thread_attr_set_sched_priority(OsaThreadAttr *attr,
                                       OsaSchedPriority priority)
{
    if (_osa_to_linux_sched_priority(priority) == -1 || !attr)
        return ERRCODE_INVALID_ARGUMENT;

    attr->priority = priority;

    return ERRCODE_NO_ERROR;
}

int osa_thread_attr_get_sched_priority(const OsaThreadAttr *attr,
                                       OsaSchedPriority *priority)
{
    if (!attr || !priority)
        return ERRCODE_INVALID_ARGUMENT;

    *priority = attr->priority;

    return ERRCODE_NO_ERROR;
}

int osa_thread_attr_set_sched_interval(OsaThreadAttr *attr, uint32_t interval)
{
    if (interval < OSA_LINUX_RR_INTERVAL_MIN ||
        interval > OSA_LINUX_RR_INTERVAL_MAX ||
        !attr)
        return ERRCODE_INVALID_ARGUMENT;

    attr->rr_interval = interval;

    return ERRCODE_NO_ERROR;
}

int osa_thread_attr_get_sched_interval(const OsaThreadAttr *attr,
                                       uint32_t *interval)
{
    if (!attr || !interval)
        return ERRCODE_INVALID_ARGUMENT;

    *interval = attr->rr_interval;

    return ERRCODE_NO_ERROR;
}

int osa_thread_create(OsaThread *thread, const OsaThreadAttr *attr,
                      void (*function)(void*), void *arg)
{
    pthread_attr_t pt_attr;
    struct sched_param sched;
    int policy, ret = ERRCODE_INVALID_ARGUMENT;

    /* According to docs (and linux code), this cannot fail. */
    pthread_attr_init(&pt_attr);

    /* XXX: can't change RR interval on linux? */

    if (attr) {
        policy = _osa_to_linux_sched_policy(attr->policy);
        sched.sched_priority = _osa_to_linux_sched_priority(attr->priority);
    } else {
        policy = _osa_to_linux_sched_policy(OSA_LINUX_DEFAULT_POLICY);
        sched.sched_priority =
            _osa_to_linux_sched_priority(OSA_LINUX_DEFAULT_PRIORITY);
    }

    ret = pthread_attr_setinheritsched(&pt_attr, PTHREAD_EXPLICIT_SCHED);
    if (ret != 0)
        goto error;

    ret = pthread_attr_setschedpolicy(&pt_attr, policy);
    if (ret != 0)
        goto error;

    /* Only attempt to set the priority if we're using realtime scheduling. */
    if (policy != SCHED_OTHER) {
        ret = pthread_attr_setschedparam(&pt_attr, &sched);
        if (ret != 0)
            goto error;
    }

    ret = pthread_create(thread, &pt_attr, (void*) function, arg);

    /* This function is the same as attr_init.  Won't error. */
    pthread_attr_destroy(&pt_attr);

    if (ret == 0)
        return ERRCODE_NO_ERROR;

error:
    if (ret == EAGAIN)
        ret = ERRCODE_TRY_AGAIN;
    else if (ret == EINVAL)
        ret = ERRCODE_INVALID_ARGUMENT;
    else
        ret = ERRCODE_GENERAL_ERROR;

    return ret;
}

int osa_thread_destroy(OsaThread thread)
{
    int ret;

    ret = pthread_cancel(thread);
    if (ret == 0)
        return ERRCODE_NO_ERROR;

    return ERRCODE_GENERAL_ERROR;
}

OsaThread osa_thread_self(void)
{
    return (OsaThread) pthread_self();
}

int osa_thread_join(OsaThread thread, void **value)
{
    void *status = NULL;
    int ret;

    /* XXX: Do something with value later. */
    if (value)
        *value = 0;

    ret = pthread_join(thread, &status);

    if (ret == EDEADLK)
        return ERRCODE_DEADLOCK;
    else if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    return ERRCODE_NO_ERROR;
}

void osa_thread_yield(void)
{
    /* This can return a status but it always succeeds on Linux. */
    pthread_yield();
}

void osa_thread_exit(void *value)
{
    pthread_exit(value);
}

int osa_thread_get_id(OsaThread thread, uint32_t *tid)
{
    *tid = (uint32_t) thread;

    return ERRCODE_NO_ERROR;
}

int osa_sched_set_policy(OsaThread thread, OsaSchedPolicy policy)
{
    struct sched_param cur_param;
    int cur_policy, new_policy, ret;

    if ((new_policy = _osa_to_linux_sched_policy(policy)) == -1)
        return ERRCODE_INVALID_ARGUMENT;

    ret = pthread_getschedparam(thread, &cur_policy, &cur_param);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    pthread_setschedparam(thread, new_policy, &cur_param);

    return ERRCODE_NO_ERROR;
}

int osa_sched_get_policy(OsaThread thread, OsaSchedPolicy *policy)
{
    struct sched_param cur_param;
    int cur_policy;

    if (pthread_getschedparam(thread, &cur_policy, &cur_param) != 0 ||
        !thread || !policy)
        return ERRCODE_INVALID_ARGUMENT;

    *policy = _linux_to_osa_sched_policy(cur_policy);

    return ERRCODE_NO_ERROR;
}

int osa_sched_set_priority(OsaThread thread, OsaSchedPriority priority)
{
    struct sched_param cur_param;
    int cur_policy, new_priority, ret;

    if ((new_priority = _osa_to_linux_sched_priority(priority)) == -1)
        return ERRCODE_INVALID_ARGUMENT;

    ret = pthread_getschedparam(thread, &cur_policy, &cur_param);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    /* Note that if policy is SCHED_OTHER, priority has no effect. */
    cur_param.sched_priority = new_priority;
    pthread_setschedparam(thread, cur_policy, &cur_param);

    return ERRCODE_NO_ERROR;
}

int osa_sched_get_priority(OsaThread thread, OsaSchedPriority *priority)
{
    struct sched_param cur_param;
    int cur_policy;

    if (pthread_getschedparam(thread, &cur_policy, &cur_param) != 0 ||
        !thread || !priority)
        return ERRCODE_INVALID_ARGUMENT;

    *priority = _linux_to_osa_sched_priority(cur_param.sched_priority);

    return ERRCODE_NO_ERROR;
}

int osa_sched_set_rr_interval(OsaThread thread, uint32_t interval)
{
    return ERRCODE_NOT_SUPPORTED;
}

int osa_sched_get_rr_interval(OsaThread thread, uint32_t *interval)
{
    return ERRCODE_NOT_SUPPORTED;
}

int osa_thread_get_name(OsaThread thread, char **name)
{
    *name = NULL;

    return ERRCODE_NO_ERROR;
}

int osa_thread_set_name(OsaThread thread, const char *name)
{
    return ERRCODE_NO_ERROR;
}

