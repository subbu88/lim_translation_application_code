
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_message_linux.c
 * @brief   Contains Linux message queue functions for Freescale libosa.
 */

#include <stddef.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#include <mqueue.h>

#include <errno.h>

#include "osa_common.h"

#define OSA_LINUX_MQ_FLAGS      (O_RDWR | O_CREAT)
#define OSA_LINUX_MQ_MODE       (S_IRWXU | S_IRWXG | S_IRWXO)
#define OSA_LINUX_MQ_PRIORITY   0

int osa_mq_open(OsaMq *queue, const char *name, uint32_t max_size,
                bool blocking)
{
    struct mq_attr attr;
    int flags = OSA_LINUX_MQ_FLAGS;

    DBG_entering

    if (!blocking)
        flags |= O_NONBLOCK;

    /* XXX: tune these params more later. */
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = max_size;
    attr.mq_flags = 0;

    /* Use default attributes (for now). */
    *queue = mq_open(name, flags, OSA_LINUX_MQ_MODE, &attr);
    if (*queue == -1) {
        switch(errno) {
        case EACCES:
            return ERRCODE_PERMISSION_DENIED;
        case EMFILE:
        case ENOSPC:
            return ERRCODE_SYSTEM_LIMIT;
        case EINVAL:
        case EEXIST:
        case ENAMETOOLONG:
        case ENOENT:
            return ERRCODE_INVALID_ARGUMENT;
        default:
            return ERRCODE_GENERAL_ERROR;
        }
    }

    DBG_leaving

    return ERRCODE_NO_ERROR;
}

int osa_mq_send(OsaMq *queue, const char *msg, size_t size)
{
    int ret;

    //DBG_entering

    /* This will (by default) block if the message queue is full. */
    ret = mq_send(*queue, msg, size, OSA_LINUX_MQ_PRIORITY);
    if (ret == -1) {
        switch (errno) {
        case EAGAIN:
            return ERRCODE_TRY_AGAIN;
        case EBADF:
        case EMSGSIZE:
        case EINVAL:
            return ERRCODE_INVALID_ARGUMENT;
        default:
            return ERRCODE_GENERAL_ERROR;
        }
    }

    //DBG_leaving

    return ERRCODE_NO_ERROR;
}

static int _mq_receive(OsaMq *queue, char *msg, size_t size,
                       uint32_t msec_timeout, size_t *receive_size, bool poll)
{
    uint32_t prio;
    size_t rec;
    int ret;

    //DBG_entering

    if (msec_timeout > 0 || poll) {
        struct timespec ts;

        ret = clock_gettime(CLOCK_REALTIME, &ts);
        if (ret != 0)
            goto error;

        if (poll) {
            /* Set the time into the past to avoid blocking at all in the
             * timedreceive. */
            if (ts.tv_sec >= 1)
                ts.tv_sec -= 1;
        } else {
            ts.tv_sec += (msec_timeout / 1000);
            ts.tv_nsec += (msec_timeout % 1000) * 1000000;
            if (ts.tv_nsec >= 1000000000) {
                ts.tv_sec += 1;
                ts.tv_nsec -= 1000000000;
            }
        }

        if ((rec = mq_timedreceive(*queue, msg, size, &prio, &ts)) == -1)
            goto error;
    } else {
        if ((rec = mq_receive(*queue, msg, size, &prio)) == -1)
            goto error;
    }

    //DBG_leaving

    if (receive_size)
        *receive_size = rec;

    return ERRCODE_NO_ERROR;

error:
    switch (errno) {
    case EAGAIN:
        return ERRCODE_TRY_AGAIN;
    case EBADF:
    case EINVAL:
    case EMSGSIZE:
        return ERRCODE_INVALID_ARGUMENT;
    case ETIMEDOUT:
        return ERRCODE_TIMED_OUT;
    default:
        return ERRCODE_GENERAL_ERROR;
    }
}

int osa_mq_receive(OsaMq *queue, char *msg, size_t size,
                   uint32_t msec_timeout, size_t *receive_size)
{
    return _mq_receive(queue, msg, size, msec_timeout, receive_size, false);
}

int osa_mq_poll(OsaMq *queue, char *msg, size_t size, size_t *receive_size)
{
    return _mq_receive(queue, msg, size, 0, receive_size, true);
}

int osa_mq_close(OsaMq *queue)
{
    int ret;

    DBG_entering

    ret = mq_close(*queue);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    DBG_leaving

    return ERRCODE_NO_ERROR;
}

int osa_mq_destroy(const char *name)
{
    int ret;

    DBG_entering

    ret = mq_unlink(name);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    DBG_leaving

    return ERRCODE_NO_ERROR;
}

