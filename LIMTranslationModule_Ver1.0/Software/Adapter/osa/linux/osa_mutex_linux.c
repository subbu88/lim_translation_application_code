
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_mutex_linux.c
 * @brief   Contains Linux mutex functions for Freescale libosa.
 */

/* Needed for PTHREAD_MUTEX_RECURSIVE. */
#define _GNU_SOURCE

#include <pthread.h>
#include <errno.h>

#include "osa_common.h"

int osa_mutex_create(OsaMutex *mutex, bool recursive)
{
    pthread_mutexattr_t attr;
    int type, ret;

    ret = pthread_mutexattr_init(&attr);
    if (ret != 0)
        goto error_init;

    if (recursive)
        type = PTHREAD_MUTEX_RECURSIVE;
    else
        type = PTHREAD_MUTEX_NORMAL;

    ret = pthread_mutexattr_settype(&attr, type);
    if (ret != 0)
        goto error;

    ret = pthread_mutex_init(mutex, &attr);
    if (ret != 0)
        goto error;

    return ERRCODE_NO_ERROR;

error:
    pthread_mutexattr_destroy(&attr);

error_init:
    switch (ret) {
    case EINVAL:
        return ERRCODE_INVALID_ARGUMENT;
    case ENOMEM:
        return ERRCODE_OUT_OF_MEMORY;
    case EAGAIN:
        return ERRCODE_TRY_AGAIN;
    case EPERM:
        return ERRCODE_PERMISSION_DENIED;
    default:
        return ERRCODE_GENERAL_ERROR;
    }
}

int osa_mutex_destroy(OsaMutex *mutex)
{
    int ret;

    ret = pthread_mutex_destroy(mutex);
    if (ret == EBUSY)
        return ERRCODE_BUSY;
    else if (ret == EINVAL)
        return ERRCODE_INVALID_ARGUMENT;

    return ERRCODE_NO_ERROR;
}

int osa_mutex_lock(OsaMutex *mutex)
{
    int ret;

    /* This could also fail with EBUSY or EAGAIN but those shouldn't really
     * happen in our situation. */
    ret = pthread_mutex_lock(mutex);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    return ERRCODE_NO_ERROR;
}

int osa_mutex_unlock(OsaMutex *mutex)
{
    int ret;

    ret = pthread_mutex_unlock(mutex);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    return ERRCODE_NO_ERROR;
}

int osa_mutex_trylock(OsaMutex *mutex)
{
    int ret;

    ret = pthread_mutex_trylock(mutex);
    if (ret == EINVAL)
        return ERRCODE_INVALID_ARGUMENT;
    else if (ret == EBUSY)
        return ERRCODE_BUSY;
    else if (ret != 0)
        return ERRCODE_GENERAL_ERROR;

    return ERRCODE_NO_ERROR;
}

