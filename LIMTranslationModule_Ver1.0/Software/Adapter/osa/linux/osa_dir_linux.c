
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_dir_linux.c
 * @brief   Contains Linux file and directory functions for Freescale libosa.
 */

#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <string.h>

#include "osa_common.h"
#include "lib_common.h"

int osa_mkdir(const char *path, int mode)
{
    int ret;

    ret = mkdir(path, mode);
    if (ret == -1) {
        switch (errno) {
            case EACCES:
                return ERRCODE_PERMISSION_DENIED;
            case EEXIST:
                return ERRCODE_PARAMETER_EXISTS;
            case ENOSPC:
                return ERRCODE_OUT_OF_SPACE;
            case ENOTDIR:
                return ERRCODE_INVALID_ARGUMENT;
            default:
                return ERRCODE_GENERAL_ERROR;
        }
    }

    return ERRCODE_NO_ERROR;
}

int osa_chdir(const char *path)
{
    int ret;

    ret = chdir(path);
    if (ret == -1) {
        switch (errno) {
            case EACCES:
                return ERRCODE_PERMISSION_DENIED;
            case ENOENT:
                return ERRCODE_INVALID_ARGUMENT;
            default:
                return ERRCODE_GENERAL_ERROR;
        }
    }

    return ERRCODE_NO_ERROR;
}

OsaDir *osa_opendir(const char *dirname)
{
    return (OsaDir*) opendir(dirname);
}

int osa_closedir(OsaDir *dirp)
{
    int ret;

    ret = closedir((DIR*) dirp);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    return ERRCODE_NO_ERROR;
}

int osa_readdir(OsaDir *dirp, OsaDirEnt *entry)
{
    struct dirent *ent;

    ent = readdir((DIR*) dirp);
    if (!ent)
        return ERRCODE_END_OF_DIRECTORY;

    strcpy(entry->name, ent->d_name);

    if (ent->d_type == DT_REG)
        entry->type = OSA_DT_FILE;
    else if (ent->d_type == DT_DIR)
        entry->type = OSA_DT_DIR;
    else
        entry->type = OSA_DT_UNKNOWN;

    return ERRCODE_NO_ERROR;
}

int osa_unlink(const char *path)
{
    int ret;

    ret = unlink(path);
    if (ret == -1) {
        switch (errno) {
            case EACCES:
            case EPERM:
                return ERRCODE_PERMISSION_DENIED;
            case EBUSY:
                return ERRCODE_BUSY;
            case ENOENT:
                return ERRCODE_NOT_FOUND;
            default:
                return ERRCODE_GENERAL_ERROR;
        }
    }

    return ERRCODE_NO_ERROR;
}

