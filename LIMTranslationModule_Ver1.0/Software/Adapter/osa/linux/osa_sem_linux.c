
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    osa_sem_linux.c
 * @brief   Contains Linux semaphore functions for Freescale libosa.
 */

#define _GNU_SOURCE

#include <semaphore.h>
#include <errno.h>
#include <time.h>

#include "osa_common.h"

int osa_sem_create(OsaSem *semaphore, uint32_t initial_value)
{
    int ret;

    ret = sem_init(semaphore, 0, initial_value);
    if (ret != 0) {
        switch (errno) {
        case EINVAL:
            return ERRCODE_INVALID_ARGUMENT;
        case ENOMEM:
            return ERRCODE_OUT_OF_MEMORY;
        case EAGAIN:
            return ERRCODE_TRY_AGAIN;
        case EPERM:
            return ERRCODE_PERMISSION_DENIED;
        default:
            return ERRCODE_GENERAL_ERROR;
        }
    }

    return ERRCODE_NO_ERROR;
}

int osa_sem_destroy(OsaSem *semaphore)
{
    int ret;

    /* This close will place all tasks waiting for the sem in ready queues. */
    ret = sem_destroy(semaphore);
    if (ret != 0)
        return ERRCODE_INVALID_ARGUMENT;

    return ERRCODE_NO_ERROR;
}

int osa_sem_post(OsaSem *semaphore)
{
    int ret;

    ret = sem_post(semaphore);
    if (ret != 0) {
        if (errno == EOVERFLOW)
            return ERRCODE_OVERFLOW;
        else
            return ERRCODE_INVALID_ARGUMENT;
    }

    return ERRCODE_NO_ERROR;
}

int osa_sem_wait(OsaSem *semaphore, uint32_t timeout)
{
    int ret;
    struct timespec ts;

    if (0 != timeout) {
        if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
            return ERRCODE_INTERNAL;

        ts.tv_sec += timeout/1000;
        ts.tv_nsec += (timeout % 1000) * 1000000;
        ret = sem_timedwait(semaphore, &ts);
    } else {
        ret = sem_wait(semaphore);
    }

    if (ret != 0) {
        if (errno == ETIMEDOUT)
            return ERRCODE_TIMED_OUT;
        else
            return ERRCODE_INVALID_ARGUMENT;
    }

    return ERRCODE_NO_ERROR;
}

int osa_sem_poll(OsaSem *semaphore)
{
    int ret;

    ret = sem_trywait(semaphore);
    if (ret == 0) {
        sem_post(semaphore);
        return ERRCODE_NO_ERROR;
    }

    return ERRCODE_TRY_AGAIN;
}

