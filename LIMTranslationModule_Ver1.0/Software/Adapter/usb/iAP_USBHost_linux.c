
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <asm/types.h>
#include <linux/hiddev.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <libusb-1.0/libusb.h>
#include <libudev.h>
#include <libgen.h>

#include "osa_common.h"

#include "ProjectTypes.h"
#include "MFi_Config.h"
#include "iAP_Transport.h"
#include "iAP_USBHost.h"

#ifdef MFI_IAP1
#include "iAP_Interface.h"
#else
#define USB_ATTACH (1<<1)
#endif

#include "iAP_USB_Audio_Interface.h"

//#define DEBUG_IAP

static uint8_t iAPOutReport[HID_REPORT_MAX_SIZE];
static uint8_t iAPInReport[HID_REPORT_MAX_SIZE];
static IAP_TRANSPORT_RECV_CALLBACK  iAP_USBHost_fpRecvCallback;
static IAP_TRANSPORT_SENT_CALLBACK  iAP_USBHost_fpSentCallback;
static IapUSBHostIntf iap_usb_host_intf;
static uint16_t g_wUSBHostStatus;

static unsigned short hidReportSize = 0;
static unsigned short hidReportIndex = 0;
static unsigned char hidReport[2048];


static void iap_get_report_descriptor_info(IapUSBHostIntf *usb_host_intf);
static uint16_t iap_send_report(int hid_fd, uint8_t *buff, uint32_t length,
                                uint32_t report_id, uint32_t report_size);
static uint32_t iap_get_hid_reports_info(int fd,
                                         IapHIDReportInfo *report_info,
                                         IapHIDReportType iap_report_type);
static uint32_t iap_USBHost_SendReport(int hid_fd, uint32_t report_id,
                                       uint8_t *buff, uint32_t size);



int16_t readUSB(int fd, IapHIDReportInfo *report_info, uint32_t num_reports,
                uint8_t *pbuffer, uint32_t len)
{
    struct hiddev_usage_ref receive_usage_ref;
    int i = 0, ret_val = 0;
    uint32_t receive_size = 0;
    uint32_t bytesRemain = len;
    uint32_t totalBytesRead = 0;
    uint8_t *buffer = pbuffer;
    static int err_count = 0;

    //printf("\n bytesRemain %d\n", bytesRemain );
    while (bytesRemain) {
        /* Parse buffer empty */
        if (hidReportSize == hidReportIndex) {
            /* Reset the buffer params */
            hidReportSize = 0;
            //printf("\n hidReportIndex %d\n", hidReportIndex );

            /* Read a full report in the parse buffer */
            while (1) {
                /* read reports from IAP device */
                ret_val = read(fd, &receive_usage_ref,
                               sizeof(struct hiddev_usage_ref));

                /* Bad file number */
                if (ret_val < 0) {
                    dprint("USB HID read error: retval = %d, error: %d\n",
                           ret_val, errno);

                    /* Reset the report index so we try to read again on the
                     * next call. */
                    hidReportIndex = 0;

                    /* If we get 2 read errors in succession, return read
                     * failure so the parent thread can exit if necessary.
                     * Otherwise return 0 bytes read so it will retry. */
                    err_count++;
                    if (err_count >= 2)
                        return -1;
                    else
                        return 0;

                } else {
                    /* Success - break to process the report. */
                    err_count = 0;
                    break;
                }
            }

            /* Report arrived, read it fully based on the size from the report
             * desc array. */

            /* Got input report, now get the report size using report id */
            for (i = 0; i < num_reports; i++) {
                /* look for this input report in iap report info table */
                if (report_info[i].report_id == receive_usage_ref.report_id) {
                    /* get input report size */
                    receive_size = report_info[i].report_size;
                    break;
                }
            }

            dprint("USB HID msg received - report id: %d, report size: %d\n",
                   receive_usage_ref.report_id, receive_size);

            /* check if we found the input report */
            if (receive_size != 0) {
                /* Get all the report data */
                for (i = 0; i < receive_size ; i++) {
                    /* Data from first read */  
                    hidReport[hidReportSize++] = receive_usage_ref.value;

                    /* read rest of the data */ 
                    ret_val = read(fd, &receive_usage_ref,
                                   sizeof(struct hiddev_usage_ref));
                }

                /* Store the last read data */
                hidReport[hidReportSize++] = (uint8_t) receive_usage_ref.value;

                /* Ignore the LCB */
                hidReportIndex = 2;
            }

        } else {
            unsigned short left = hidReportSize - hidReportIndex;

            if (left > bytesRemain)
                left = bytesRemain;

            //printf("\nhidReportIndex %d left %d\n", hidReportIndex, left);
            osa_memcpy(buffer, &hidReport[hidReportIndex], left);

            bytesRemain -= left;
            hidReportIndex += left;
            totalBytesRead += left;
            buffer += left;
        }
    }

    //printf("\n totalBytesRead %d\n", totalBytesRead );
    return totalBytesRead;
}

void iap_ReadTask(void *para)
{
    uint8_t tempByte;
    uint16_t tempShort;
    int16_t rStatus;
    uint32_t size;
    int fd = (int) para;
    uint8_t out_buf[1024];

    IapHIDReportInfo *report_info = &iap_usb_host_intf.report_info[0];
    uint32_t num_reports = iap_usb_host_intf.num_reports;

    /* read input reports using hid driver waiting read call */
    /* NOTE: hiddev read mode is set to return hiddev_usage_ref structure so as to
     * get report id of the report too. */

    hidReportSize = 0;
    hidReportIndex = 0;

    while (1) {
        if (iap_usb_host_intf.iap_thread_exit) {
            dprint("iap_ReadTask exiting\n");
            break;
        }

        /* read the SOP */
        rStatus = readUSB(fd, report_info, num_reports, &tempByte, 1);
        if (rStatus < 0) {
            dprint("iap_ReadTask exiting due to USB HID read failures\n");
            break;
        } else if (rStatus != 1) {
            continue;
        }
        if (tempByte == IAP1_SOP_BYTE || tempByte == IAP2_SOP_BYTE) {
            out_buf[0] = tempByte;

            g_wUSBHostStatus &= ~ (1 << USB_REPORT_RECEIVED);                              
            
            rStatus = readUSB(fd, report_info, num_reports, &out_buf[1],
                              hidReportSize - 3);
                              

#ifdef MFI_DEBUG
            int i;
            printf("  data: ");
            for (i = 0; i < hidReportSize; i++)
                printf("%x ", out_buf[i]);
            printf("\n");
#endif

            if (iAP_USBHost_fpRecvCallback) {
                //printf("\niAP_USBHost_fpRecvCallback hidReportSize %d\n",hidReportSize - 2);
                iAP_USBHost_fpRecvCallback(&out_buf[0], hidReportSize - 2);
                //osa_time_delay(10);
            }

            g_wUSBHostStatus |= (1 << USB_REPORT_RECEIVED); 

        } else {
            dprint("Received non-iAP USB HID message\n");
        }
        
    }

    g_wUSBHostStatus &= (uint16_t)(~(USB_ATTACH));

    dprint("iap_ReadTask exiting\n");
}

static uint8_t iap_get_report_index(IapHIDReportInfo * report_info,
                                    uint32_t total_num_reports,
                                    IapHIDReportType report_type,
                                    uint32_t length,
                                    uint32_t * puiIndex)
{
    uint8_t ret = 0;
    uint32_t report_size = 0;
    uint8_t i = 0;

    if ((!report_info) || (!puiIndex)) {
        printf("%s:parameter error!\n",__FUNCTION__);
        return -1;
    }

    *puiIndex = 0;
    for (i = 0; i < total_num_reports; i++) {
        /* Check if report type matches */
        if (report_info[i].report_type == report_type) {
            if (report_size > length) {
                /* Check if the report size is between current report size and
                 * required report size
                 */
                if ((report_size - length) >
                    (report_info[i].report_size - length)) {
                    report_size = report_info[i].report_size;
                    *puiIndex = i;
                }

            } else {
                /* Check if report size is greater than current report size */
                if (report_size < report_info[i].report_size) {
                    report_size = report_info[i].report_size;
                    *puiIndex = i;
                }

                /* if found the exact size return current index */
                if (report_size == length)
                    break;
            }
        }
    }

    if (report_size)
        ret = 0;

    return ret;
}

uint16_t iAP_USBHost_Transmit_Message(uint8_t *bpBuff, uint16_t wSize)
{
    uint32_t report_index = 0;
    uint16_t ret = 0;

    if (!bpBuff) {
        printf("%s:parameter error!\n", __FUNCTION__);
        return 1;
    }

    dprint("USB HID msg send - size: %d\n", wSize);

#ifdef MFI_DEBUG
    int i;
    printf("  data: ");
    for (i = 0; i < wSize; i++)
        printf("%x ", bpBuff[i]);
    printf("\n");
#endif

    /* Get output report of equal or suitable size for sending data */
    if (iap_get_report_index(iap_usb_host_intf.report_info,
                             iap_usb_host_intf.num_reports,
                             IAP_OUTPUT_REPORT,
                             wSize + 1,
                             &report_index) == 0) {
        /* Get the report id and size using report_index and send iap data */
        ret = iap_send_report(iap_usb_host_intf.hid_fd,
                              bpBuff,
                              wSize,
                              iap_usb_host_intf.report_info[report_index].report_id,
                              iap_usb_host_intf.report_info[report_index].report_size);

        if (iAP_USBHost_fpSentCallback)
            iAP_USBHost_fpSentCallback();
    } else
        dprint("iap_send_packet failed !! suitable output report not found\n");

    return ret;
}

uint16_t iap_send_report(int hid_fd, uint8_t *buff, uint32_t length,
                         uint32_t report_id, uint32_t report_size)
{
    uint32_t wDataIndex = 0;
    uint8_t bLCB = LCB_FIRSTREPORT;

    if (!buff) {
        printf("%s:parameter error!\n",__FUNCTION__);
        return -1;
    }

    /* Send command through HID report */
    if ((g_wUSBHostStatus & (1 << USB_HOST_TX_IN_PROGRESS)) == 0) {
        /* until all the data is not transmitted */
        g_wUSBHostStatus |= (1 << USB_HOST_TX_IN_PROGRESS);
 
        while (length > 0) {
            /* Determine next LCB packet */
            if (report_size <= length)
                bLCB |= LCB_MOREREPORTSTOFOLLOW; /* multiple packets */
            else
                bLCB &= ~(LCB_MOREREPORTSTOFOLLOW); /* single packet */

            iAPOutReport[0] = bLCB;
            osa_memcpy(&iAPOutReport[1], &buff[wDataIndex], report_size - 1);

            if ((report_size - 1) > length) {
                osa_memset(&iAPOutReport[length + 1], 0,
                           report_size - length - 1);
            }

            if (iap_USBHost_SendReport(hid_fd,
                                       report_id,
                                       iAPOutReport,
                                       report_size) == 0) {
                length -= ((report_size - 1) > length)? length:(report_size - 1);
                wDataIndex +=(report_size - 1);
                bLCB |= LCB_CONTINUINGREPORT;
            } else
                break;

        }

        g_wUSBHostStatus &= ~((1 << USB_HOST_TX_IN_PROGRESS));

    } else
        return -1;

    return 0;
}

static void iap_get_report_descriptor_info(IapUSBHostIntf *usb_host_intf)
{
    int fd = usb_host_intf->hid_fd;

    /* calculate total number of reports */
    usb_host_intf->num_reports = 0;

    /* Get all input reports inforamtion */
    usb_host_intf->num_reports += iap_get_hid_reports_info(fd,
            &usb_host_intf->report_info[usb_host_intf->num_reports],
            IAP_INPUT_REPORT);

    /* Get all output reports information */
    usb_host_intf->num_reports += iap_get_hid_reports_info(fd,
            &usb_host_intf->report_info[usb_host_intf->num_reports],
            IAP_OUTPUT_REPORT);
}

/**
 * iap_get_hid_report_type:
 *
 * @iap_report_type: enum value for iap report type
 *
 * Function Description: Returns the hid report type enum value
 * for apHIDReportType enum value.
 */
static uint32_t iap_get_hid_report_type(IapHIDReportType iap_report_type)
{
    uint32_t report_type;

    switch (iap_report_type) {
        case IAP_INPUT_REPORT:
            report_type = HID_REPORT_TYPE_INPUT;
            break;

        case IAP_OUTPUT_REPORT:
            report_type = HID_REPORT_TYPE_OUTPUT;
            break;

        default:
            /* invalid report type*/
            report_type = 0;
            break;
    }

    return report_type;
}

uint32_t iap_get_hid_reports_info(int fd,
                                  IapHIDReportInfo *report_info,
                                  IapHIDReportType iap_report_type)
{
    struct hiddev_report_info dev_report_info;
    struct hiddev_field_info field_info;
    unsigned int i = 0, index = 0;
    unsigned int report_type = 0, max = 0;

#ifdef DEBUG_IAP
    struct hiddev_devinfo hiddev_info;
    int ret = 0;
    
    /* Get device info */
    ret = ioctl(fd, HIDIOCGDEVINFO, &hiddev_info);
    
    if (ret >= 0) {
        printf("\n\nHIDIOCGDEVINFO: bustype=%d busnum=%d devnum=%d ifnum=%d\n"
               "\tvendor=0x%04hx product=0x%04hx version=0x%04hx\n"
               "\tnum_applications=%d\n",
               hiddev_info.bustype, hiddev_info.busnum, 
               hiddev_info.devnum, hiddev_info.ifnum,
               hiddev_info.vendor, hiddev_info.product, 
               hiddev_info.version, hiddev_info.num_applications);
    } 
#endif

    /* get hid report type names for input and output reports */
    report_type = iap_get_hid_report_type(iap_report_type);

    /* Check for valid report_type request */
    if (report_type >= HID_REPORT_TYPE_MIN &&
        report_type <= HID_REPORT_TYPE_MAX) {

        /* Traverse through reports and store required information */
        dev_report_info.report_type = report_type;

        /* start with first report id */
        dev_report_info.report_id = HID_REPORT_ID_FIRST;

        /* ioctl returns non-zero for no more next report ID */
        while (ioctl(fd, HIDIOCGREPORTINFO, &dev_report_info) >= 0) {
            /* Got report information */
            report_info[index].report_id = dev_report_info.report_id;
            report_info[index].report_type = iap_report_type;

            /* Get the field with maximum maxusage value */
            max = 0;

            /* num_fields : Number of fields present in this report */
            /* Now traverse through the report fields to get maxusage
                            * i.e report size
                            */
            for (i = 0; i < dev_report_info.num_fields; i++) {
                /* Fill field type, id and field index information */
                field_info.report_type = dev_report_info.report_type;
                field_info.report_id = dev_report_info.report_id;
                field_info.field_index = i; /* 0 to maxfield - 1 */       

                /* NOTE: field->maxusage is Report Count */
                if (ioctl(fd, HIDIOCGFIELDINFO, &field_info) >= 0) {
#ifdef DEBUG_IAP
                    printf("\nHIDIOCGFIELDINFO: Field_index=%u maxusage=%u "
                           "type=%u id=%u" 
                           "flags=0x%X\n \tphysical=0x%X logical=0x%X" 
                           "application=0x%X\n\tlogical_minimum=%d"
                           "maximum=%d physical_minimum=%d maximum=%d\n",
                           field_info.field_index,field_info.maxusage, 
                           field_info.report_type, field_info.report_id, 
                           field_info.flags,field_info.physical, 
                           field_info.logical,field_info.application,
                           field_info.logical_minimum,  
                           field_info.logical_maximum,
                           field_info.physical_minimum, 
                           field_info.physical_maximum);
#endif

                    /* Check the field->maxusage value */
                    if (field_info.maxusage > max) {
                        /* set report size */
                        max = field_info.maxusage;

                        /* Store this field index */
                        report_info[index].report_field_index = field_info.field_index;
                    }
                } else {
                    /* Unable to read field info */
                    dprint("ERROR: Failed to read field index: %d\n", 
                           field_info.field_index);
                }
            }

            /* Store the report size */
            if (max != 0) {
                report_info[index].report_size = max; 
                /* NOTE: loop as 0 to < report_size */
            } else {
                /* Error in reading report field information */
                report_info[index].report_size = 0;
            }

            /* Get next report */
            dev_report_info.report_id |= HID_REPORT_ID_NEXT;
            index++;
        }
    }

    /* return number of reports filled */
    return index;
}

uint32_t iap_USBHost_SendReport(int hid_fd, uint32_t report_id,
                                uint8_t *buff, uint32_t size)
{
    struct hiddev_usage_ref_multi uref;
    struct hiddev_report_info rinfo;
    int i = 0, ret = 0;

    /* To set an hid report first fill the usage values of the required
     * report and then use HIDIOCSREPORT to set the report. */

    /* Fill usage data */
    /* NOTE: using hiddev_usage_ref_multi to set usage data in one go */
    uref.uref.report_type = HID_REPORT_TYPE_OUTPUT;
    uref.uref.report_id = report_id;

    /* required to set exact field index of this report ? */
    /* Works with field index set to 0 */
    uref.uref.field_index = 0;
    uref.uref.usage_index = 0;

    /* Set number of data to transfer */
    uref.num_values = size;

    /* fill data values */
    for (i = 0; i < size; i++)
        uref.values[i] = buff[i];

    /* Set report usages values first */
    ret = ioctl(hid_fd, HIDIOCSUSAGES, &uref);
    if (ret >= 0) {
        /* Usage data successfully set */

        /* now send report */
        rinfo.report_type = HID_REPORT_TYPE_OUTPUT;
        rinfo.report_id = report_id;
        rinfo.num_fields = 1;

        /* Now set report */
        ret = ioctl(hid_fd, HIDIOCSREPORT, &rinfo);

        if (ret >= 0) {
            /* Report set successfully */
        } else
            dprint("HIDIOCSREPORT ERROR\n");
    } else
        dprint("HIDIOCSUSAGES ERROR\n");

    return ret;
}

void iAP_USBHost_SetSentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback)
{
    iAP_USBHost_fpSentCallback = fpCallback;
}

void iAP_USBHost_SetRecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback)
{
    iAP_USBHost_fpRecvCallback = fpCallback;
}

uint16_t iAP_USBHost_Status(void)
{
//printf("g_wUSBHostStatus %d\n", g_wUSBHostStatus);    
return g_wUSBHostStatus;
}

void iAP_USBHostTask(void)
{
    /* dummy function for FAS */
}

#if 0
#define A_DEV_BUS_REQ  "/sys/bus/platform/devices/ci_hdrc.0/inputs/a_bus_req"

static int switch_to_usb_device_mode(void)
{
	int32_t ret;
	FILE* fp;

	printf(  "SWITCHING TO USB DEVICE MODE..\n");

	/* perform role switch to device mode */
	fp = fopen(A_DEV_BUS_REQ, "w");

	if (fp) {
		/* write '0' to the A_DEV_BUS_REQ
		   file to switch the 'device' mode */
		fwrite("0", 1, 1, fp);
		ret = 0;
		fclose(fp);
	}else
		ret = -1;

	return ret;
}

static int32_t usb_mfi_request_role_switch(uint32_t vid,
                                           uint32_t pid,
                                           uint16_t value)
{
    libusb_device_handle* devHandle;
    uint32_t reqStatus;
    int ret;

    printf("\n usb_mfi_request_role_switch \n"); 

    ret = libusb_init(NULL);
    if ( ret < 0 )
       printf("\n libusb init failed \n");
   

    devHandle = libusb_open_device_with_vid_pid (NULL, vid, pid);
    if(devHandle){
        printf("\n Send role switch USB request using received value \n"); 

        /* Send role switch USB request using received value */
        reqStatus = libusb_control_transfer(devHandle,
                (LIBUSB_ENDPOINT_OUT |
                        LIBUSB_REQUEST_TYPE_VENDOR |
                        LIBUSB_RECIPIENT_DEVICE),
                        0x51,
                        value,
                        0x00,
                        NULL,
                        0x00,
                        10000);

	switch_to_usb_device_mode();

	usleep(10000);
	
	g_wUSBHostStatus |= ( 1 << 5 );
	g_wUSBHostStatus |= ( 1 << 6 );

    }else {
        printf("\n Invalid argument \n");  
        reqStatus=ERRCODE_INVALID_ARGUMENT;
    }
    printf("USB MFI role switch status: %i\n", reqStatus);

    /* return request status. if transaction successful value should be zero */
    return reqStatus;
}

unsigned int device_vendor_id, device_product_id;

void _process_usb_netlink_msg(struct udev_device *dev, int type)
{
    char *tempVid;
    char *tempPid;
    char *tempSerial;

    char *action = udev_device_get_action(dev);
    char *devpath_str = udev_device_get_devpath(dev);

    if ((action && !strcmp(action, "add")) || type & 0x04) {
        char *subsys_str = udev_device_get_subsystem(dev);

        /* read the device serial number */
        tempSerial = udev_device_get_sysattr_value(dev,"serial");

        /* new USB device to be added */
        if (devpath_str && (0x01 & type) &&
            !strcmp(subsys_str,"usb")) {

            /* read vendor ID to determine device type */
            tempVid = udev_device_get_sysattr_value(dev,"idVendor");

            if (!tempVid)
                return;

            device_vendor_id = strtol(tempVid, NULL, 16);

            printf("VID: 0x%x\n", device_vendor_id);

            /* read product ID to command AOAP mode */
            tempPid = udev_device_get_sysattr_value(dev,"idProduct");

            if (!tempPid)
                return;

            device_product_id = strtol(tempPid, NULL, 16);
            printf("PID: 0x%x\n", device_product_id);

            if (0x05ac == device_vendor_id) {
                printf("USB Apple Device Detected\n");
            } 
        } 
    } 
}


static int _check_usb_devices(void)
{
    struct udev *udev;
    struct udev_enumerate *enumerate;
    struct udev_list_entry *devices, *dev_list_entry;
    struct udev_device *dev;

    /* Create the udev object */
    udev = udev_new();
    if (!udev) {
        DEVMGR_DBG_PRINT("unable to create udev object\n");
        return -1;
    }

    enumerate = udev_enumerate_new(udev);
    udev_enumerate_add_match_subsystem(enumerate, "usb");

    udev_enumerate_scan_devices(enumerate);
    devices = udev_enumerate_get_list_entry(enumerate);

    udev_list_entry_foreach(dev_list_entry, devices) {
        const char *path;

        path = udev_list_entry_get_name(dev_list_entry);
        dev = udev_device_new_from_syspath(udev, path);

        const char *type = udev_device_get_property_value(dev, "TYPE");

        if(udev_device_get_devtype(dev) &&
           (!strcmp("usb_device", udev_device_get_devtype(dev)))
           &&
           !strstr(type, "9/") /* do not add hubs */
           ) {
            _process_usb_netlink_msg(dev, 0x01 | 0x04);
        }
    }
    return 0;
}


void iAP_USB_Role_Switch_Init(void)
{
    int32_t ret = 0;
    int flags = HIDDEV_FLAG_REPORT | HIDDEV_FLAG_UREF;

    printf("\n iAP_USB_Role_Switch_Init \n");
    osa_memset(&iap_usb_host_intf, 0, sizeof(iap_usb_host_intf));
    
    g_wUSBHostStatus |= (USB_ATTACH);

    iap_usb_host_intf.hid_fd = open(&g_hostConfig.hid_dev[0], O_RDWR);
    if (iap_usb_host_intf.hid_fd != -1) {
        iap_get_report_descriptor_info(&iap_usb_host_intf);
    } else {
        printf("ERROR: failed to open /dev/usb/hiddev0 for USB communication."
               " errno: %d\n", errno);
        return -1;
    }

    /* Set mode flags to return hiddev_usage_ref instead of hiddev_event */
    ret = ioctl(iap_usb_host_intf.hid_fd, HIDIOCSFLAG, &flags);
    if (ret < 0) {
        dprint("ERROR ioctl HIDIOCSFLAG: %d, errno: %d\n", ret, errno);
        return -1;
    }
    
    // perform role switch
    _check_usb_devices();    

    /* read vendor ID */
    ret=usb_mfi_request_role_switch(device_vendor_id,device_product_id ,1);
    
    if ( ret == 0 )
    {
        iAP_USBDev_Init();
    }
}
#endif
int32_t iAP_USBHost_Init(void)
{
    OsaThreadAttr iar_read_thread_attr;
    int32_t ret = 0;
    int flags = HIDDEV_FLAG_REPORT | HIDDEV_FLAG_UREF;

    osa_memset(&iap_usb_host_intf, 0, sizeof(iap_usb_host_intf));
    osa_memset(&iar_read_thread_attr, 0, sizeof(iar_read_thread_attr));

    g_wUSBHostStatus |= (USB_ATTACH);

    iap_usb_host_intf.hid_fd = open(&g_hostConfig.hid_dev[0], O_RDWR);
    if (iap_usb_host_intf.hid_fd != -1) {
        iap_get_report_descriptor_info(&iap_usb_host_intf);
    } else {
        printf("ERROR: failed to open /dev/usb/hiddev0 for USB communication."
               " errno: %d\n", errno);
        return -1;
    }

    /* Set mode flags to return hiddev_usage_ref instead of hiddev_event */
    ret = ioctl(iap_usb_host_intf.hid_fd, HIDIOCSFLAG, &flags);
    if (ret < 0) {
        dprint("ERROR ioctl HIDIOCSFLAG: %d, errno: %d\n", ret, errno);
        return -1;
    }

    iap_usb_host_intf.iap_thread_exit = false;

    /* Create iAP data read task */
    osa_thread_attr_init(&iar_read_thread_attr);
    osa_thread_attr_set_stack_size(&iar_read_thread_attr,
                                   MFI_HID_READ_TASK_STACK_SIZE);
    osa_thread_attr_set_sched_interval(&iar_read_thread_attr,
                                       MFI_HID_READ_TASK_INTERVAL);
    osa_thread_attr_set_sched_priority(&iar_read_thread_attr,
                                      OSA_SCHED_PRIORITY_NORMAL);
    ret = osa_thread_create(&(iap_usb_host_intf.iap_read_thread),
                            &iar_read_thread_attr,
                            iap_ReadTask,
                            (void*) iap_usb_host_intf.hid_fd);
    osa_thread_set_name(iap_usb_host_intf.iap_read_thread, "iAP2 Read Task");
    osa_thread_attr_destroy(&iar_read_thread_attr);

    return ret;
}

void iAP_USBHost_UnInit(void)
{
    /* Indicate to the stack USB has been dettached. */
    g_wUSBHostStatus &= (uint16_t)(~(USB_ATTACH));

    /* Request the read thread to exit if it hasn't already. */
    iap_usb_host_intf.iap_thread_exit = true;

    dprint("iAP_USBHost_UnInit: closing HID file descriptor\n");
    close(iap_usb_host_intf.hid_fd);
}

void iAP_USBHost_DataReceive(void)
{
    /* Dummy function */
}
