
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    iAP_USBDevice_linux.c
 * @brief   FunctionFS driver for USB device mode iAP communication.
 * @details User-space driver through FunctionFS USB device interface for
 *          device mode FAS to connect to USB host Apple device for iAP2
 *          communication over bulk USB endpoints.
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/usb/functionfs.h>

#include <libusb-1.0/libusb.h>
#include <libudev.h>
#include <libgen.h>


#include "osa_common.h"

#include "ProjectTypes.h"
#include "MFi_Config.h"
#include "iAP_Transport.h"
#include "iAP_USBDevice.h"
#include "iAP2_Nat_Ext_Acc_Feat.h"


static int  ep1_fd, ep2_fd, ep3_fd, ep4_fd;
static char  *ep1_buffer, *ep2_buffer,*ep3_buffer, *ep4_buffer;
static OsaThread  read_thread_id;
static OsaThread  read_native_thread_id;
static OsaThread  write_native_thread_id;
static int transportStatus;
static bool iap_usb_device_read_enable;
static bool iap_usb_device_read_native_enable;
static bool iap_usb_device_write_native_enable;

//static IAP_TRANSPORT_RECV_CALLBACK rxCallback;
//static IAP_TRANSPORT_SENT_CALLBACK txCallback;
static IAP_TRANSPORT_RECV_CALLBACK	iAP_USBDev_fpRecvCallback;
static IAP_TRANSPORT_SENT_CALLBACK	iAP_USBDev_fpSentCallback;
static IAP_TRANSPORT_SENT_CALLBACK	iAP_USBDev_fpNEASentCallback;
static IAP_TRANSPORT_RECV_CALLBACK	iAP_USBDev_fpNEARecvCallback;
 
/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define MFI_NAME "MFi Device"

#define HIGH    "1"
#define LOW     "0"

#define TIMEOUT 10000

#define USB_DEV_TO_HOST_SWITCH_TIMEOUT  500

#define A_DEV_BUS_REQ  "/sys/bus/platform/devices/ci_hdrc.0/inputs/a_bus_req"
#define A_DEV_BUS_DROP "/sys/bus/platform/devices/ci_hdrc.0/inputs/a_bus_drop"

#define USB_MFI_ROLE_SWITCH_REQ_INDEX   0x00
#define USB_MFI_ROLE_SWITCH_REQ_LENGTH  0x00

#define USB_MFI_CAPS_REQUEST                                            0x53
#define USB_MFI_CAPS_REQUEST_VALUE                                      0x00
#define USB_MFI_CAPS_REQUEST_INDEX                                      0x00
#define USB_MFI_CAPS_REQUEST_LENGTH                                     0x04
#define USB_MFI_ROLE_SWITCH_REQUEST                 0x51
#define USB_MFI_ROLE_SWITCH_CARPLAY_NO_SUPPORTED    0
#define USB_MFI_ROLE_SWITCH_CARPLAY_SUPPORTED       0x1

#define EP0_BUFFER_SIZE (4 * sizeof(struct usb_functionfs_event))

#define RS_BUFFER_SIZE          10
#define MAX_PACKET_SIZE_FS      64
#define MAX_PACKET_SIZE_HS      512

#define FFS_EP0 "/dev/mfi-ffs/ep0"

#define STR_INTERFACE_ "iAP Interface"
//#define STR_INTERFACE1_ "com.freescaleM.EADemo" //NXP MFI Name changes
#define STR_INTERFACE1_ "com.Philips.Lumify"

#define cpu_to_le16(x) htole16(x)
#define cpu_to_le32(x) htole32(x)

#define EP1_BUFFER_SIZE (MAX_PACKET_SIZE_HS)
#define EP2_BUFFER_SIZE (MAX_PACKET_SIZE_HS)
#define EP3_BUFFER_SIZE (MAX_PACKET_SIZE_HS)
#define EP4_BUFFER_SIZE (MAX_PACKET_SIZE_HS)

/*************************************************************************************************/
/*                              Structures and Enums Definition Section                          */
/*************************************************************************************************/
enum ep0_status_enum {
	EP0_WAITCONFIG, // Waiting to be configured
	EP0_IDLE,       // Waiting  for control / bulk-out
	EP0_DATA_OUT,   // Data arrival on bulk-out expected
	EP0_DATA_READY, // Finished receive, have to proceed and send (Data and) Response
	EP0_DATA_IN     // Waiting for bulk-in to become free for more mfiDeviceInfo
};

struct functionfs_descriptors {
	struct usb_functionfs_descs_head header;                /* Legacy format, deprecated as of 3.14   -- linux/usb/functionfs.h */
	struct {
		struct usb_interface_descriptor intf;           /* USB_DT_INTERFACE: Interface descriptor -- linux/usb/ch9.h        */
		struct usb_endpoint_descriptor_no_audio sink;   /* Descriptor of an non-audio endpoint    -- linux/usb/functionfs.h */
		struct usb_endpoint_descriptor_no_audio source;

		struct usb_interface_descriptor intf2;           /* USB_DT_INTERFACE: Interface descriptor -- linux/usb/ch9.h        */
		struct usb_endpoint_descriptor_no_audio sink2;   /* Descriptor of an non-audio endpoint    -- linux/usb/functionfs.h */
		struct usb_endpoint_descriptor_no_audio source2;

		struct usb_interface_descriptor intf1;           /* USB_DT_INTERFACE: Interface descriptor -- linux/usb/ch9.h        */

	} __attribute__((packed)) fs_descs, hs_descs;
};

struct functionfs_strings {
	struct usb_functionfs_strings_head header;      /* linux/usb/functionfs.h */
	struct {
		__le16 code;
		const char str1[sizeof(STR_INTERFACE_)];
		const char str2[sizeof(STR_INTERFACE1_)];
		/*const char str3[sizeof(STR_INTERFACE1_)];*/
	} __attribute__((packed)) lang0;
};

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static int ep0_fd;
static void * ep0_buffer;
static int ep0_thread_created = 0;
int wait_for_role_switch_done = 0;

static pthread_t functionFSThread, ep0_thread;

static enum ep0_status_enum ep0_status = EP0_WAITCONFIG;

static struct functionfs_strings strings =
{
	.header			={
		.magic		= cpu_to_le32(FUNCTIONFS_STRINGS_MAGIC),
		.length		= cpu_to_le32(sizeof(strings)),
		.str_count	= cpu_to_le32(2),
		.lang_count	= cpu_to_le32(1),
	},
	.lang0			={
		cpu_to_le16(0x0409), // will be overwritten
		STR_INTERFACE_,
		STR_INTERFACE1_,
	},
};

static struct functionfs_descriptors descriptors =
{
	.header					={
		.magic				= cpu_to_le32(FUNCTIONFS_DESCRIPTORS_MAGIC),
		.length				= cpu_to_le32(sizeof(descriptors)),
		.fs_count			= cpu_to_le32(7),                               //3
		.hs_count			= cpu_to_le32(7),                               //3
	},

	.fs_descs				={ 
		
		//{

			.intf				={
				.bLength		= sizeof(descriptors.fs_descs.intf),
				.bDescriptorType	= USB_DT_INTERFACE,
				.bInterfaceNumber	= 0,
				.bNumEndpoints		= 2,
				.bInterfaceClass	= USB_CLASS_VENDOR_SPEC,                //0xff
				.bInterfaceSubClass	= 0xf0,                                 // will be overwritten
				.bInterfaceProtocol	= 0x00,                                 // will be overwritten
				.iInterface		= 1,
			},
			.sink				={
				.bLength		= sizeof(descriptors.fs_descs.sink),
				.bDescriptorType	= USB_DT_ENDPOINT,
				.bEndpointAddress	= USB_DIR_IN | 1,
				.bmAttributes		= USB_ENDPOINT_XFER_BULK,
				.wMaxPacketSize		= MAX_PACKET_SIZE_FS,
			},
			.source				={
				.bLength		= sizeof(descriptors.fs_descs.source),
				.bDescriptorType	= USB_DT_ENDPOINT,
				.bEndpointAddress	= USB_DIR_OUT | 2,
				.bmAttributes		= USB_ENDPOINT_XFER_BULK,
				.wMaxPacketSize		= MAX_PACKET_SIZE_FS,
			},
		//},


		//{
			.intf2				={
				.bLength		= sizeof(descriptors.fs_descs.intf2),
				.bDescriptorType	= USB_DT_INTERFACE,
				.bInterfaceNumber	= 1,
		    		.bAlternateSetting      = 1,
				.bNumEndpoints		= 2,
				.bInterfaceClass	= USB_CLASS_VENDOR_SPEC,                //0xff
				.bInterfaceSubClass	= 0xf0,                                 // will be overwritten
				.bInterfaceProtocol	= 0x01,                                 // will be overwritten
				.iInterface		= 2,
			},
			.sink2				={
				.bLength		= sizeof(descriptors.fs_descs.sink2),
				.bDescriptorType	= USB_DT_ENDPOINT,
				.bEndpointAddress	= USB_DIR_IN | 3,
				.bmAttributes		= USB_ENDPOINT_XFER_BULK,
				.wMaxPacketSize		= MAX_PACKET_SIZE_FS,
			},
			.source2				={
				.bLength		= sizeof(descriptors.fs_descs.source2),
				.bDescriptorType	= USB_DT_ENDPOINT,
				.bEndpointAddress	= USB_DIR_OUT | 4,
				.bmAttributes		= USB_ENDPOINT_XFER_BULK,
				.wMaxPacketSize		= MAX_PACKET_SIZE_FS,
			},

		//},
		//{
			.intf1				={
				.bLength		= sizeof(descriptors.fs_descs.intf1),
				.bDescriptorType	= USB_DT_INTERFACE,
				.bInterfaceNumber	= 1,
		                .bAlternateSetting      = 0,
				.bNumEndpoints		= 0,
				.bInterfaceClass	= USB_CLASS_VENDOR_SPEC,                //0xff
				.bInterfaceSubClass	= 0xf0,                                 // will be overwritten
				.bInterfaceProtocol	= 0x01,                                 // will be overwritten
				.iInterface		= 2,
			},

		//},



	},

	.hs_descs				={
		
		//{

			.intf				={
				.bLength		= sizeof(descriptors.hs_descs.intf),
				.bDescriptorType	= USB_DT_INTERFACE,
				.bInterfaceNumber	= 0,
				.bNumEndpoints		= 2,
				.bInterfaceClass	= USB_CLASS_VENDOR_SPEC,
				.bInterfaceSubClass	= 0xf0,                                 // will be overwritten
				.bInterfaceProtocol	= 0x00,                                 // will be overwritten
				.iInterface		= 1,
			},
			.sink				={
				.bLength		= sizeof(descriptors.hs_descs.sink),
				.bDescriptorType	= USB_DT_ENDPOINT,
				.bEndpointAddress	= USB_DIR_IN | 1,
				.bmAttributes		= USB_ENDPOINT_XFER_BULK,
				.wMaxPacketSize		= MAX_PACKET_SIZE_HS,
			},
			.source				={
				.bLength		= sizeof(descriptors.hs_descs.source),
				.bDescriptorType	= USB_DT_ENDPOINT,
				.bEndpointAddress	= USB_DIR_OUT | 2,
				.bmAttributes		= USB_ENDPOINT_XFER_BULK,
				.wMaxPacketSize		= MAX_PACKET_SIZE_HS,
			},
		//},
		

		//{
			.intf2				={
				.bLength		= sizeof(descriptors.hs_descs.intf2),
				.bDescriptorType	= USB_DT_INTERFACE,
				.bInterfaceNumber	= 1,
		    		.bAlternateSetting      = 1,
				.bNumEndpoints		= 2,
				.bInterfaceClass	= USB_CLASS_VENDOR_SPEC,                //0xff
				.bInterfaceSubClass	= 0xf0,                                 // will be overwritten
				.bInterfaceProtocol	= 0x01,                                 // will be overwritten
				.iInterface		= 2,
			},
			.sink2				={
				.bLength		= sizeof(descriptors.hs_descs.sink2),
				.bDescriptorType	= USB_DT_ENDPOINT,
				.bEndpointAddress	= USB_DIR_IN | 3,
				.bmAttributes		= USB_ENDPOINT_XFER_BULK,
				.wMaxPacketSize		= MAX_PACKET_SIZE_HS,
			},
			.source2				={
				.bLength		= sizeof(descriptors.hs_descs.source2),
				.bDescriptorType	= USB_DT_ENDPOINT,
				.bEndpointAddress	= USB_DIR_OUT | 4,
				.bmAttributes		= USB_ENDPOINT_XFER_BULK,
				.wMaxPacketSize		= MAX_PACKET_SIZE_HS,
			},

		//},        

		//{
			.intf1				={
				.bLength		= sizeof(descriptors.hs_descs.intf1),
				.bDescriptorType	= USB_DT_INTERFACE,
				.bInterfaceNumber	= 1,
		    		.bAlternateSetting  	= 0,
				.bNumEndpoints		= 0,
				.bInterfaceClass	= USB_CLASS_VENDOR_SPEC,                //0xff
				.bInterfaceSubClass	= 0xf0,                                 // will be overwritten
				.bInterfaceProtocol	= 0x01,                                 // will be overwritten
				.iInterface		= 2,
			},

		//},        

       
	},
};

static int init_device(void);
static int close_device(void);
int switch_to_usb_host_mode(void);
int ffs_running;
int g_running;
extern uint8_t gSmartphoneHandshake;
extern uint8_t g_authentication_done;
static const char *const names[] =
{
	[FUNCTIONFS_BIND]    = "BIND",
	[FUNCTIONFS_UNBIND]  = "UNBIND",
	[FUNCTIONFS_ENABLE]  = "ENABLE",
	[FUNCTIONFS_DISABLE] = "DISABLE",
	[FUNCTIONFS_SETUP]   = "SETUP",
	[FUNCTIONFS_SUSPEND] = "SUSPEND",
	[FUNCTIONFS_RESUME]  = "RESUME",
};

void * ep0_thread_function(void * arg)
{

	while (g_running && ffs_running) {
		int ret;

		ret = read(ep0_fd, ep0_buffer, EP0_BUFFER_SIZE);

		if (ret > 0) {
			int i;
			const struct usb_functionfs_event *event = ep0_buffer;
			for (i = 0;
			     i < ret / sizeof(struct usb_functionfs_event);
			     i += sizeof(struct usb_functionfs_event), event++) {
				printf(  "ep0_thread_function(): EVENT %s\n", names[event->type]);
				switch (event->type) {
				case FUNCTIONFS_BIND:
					if (ep0_status != EP0_WAITCONFIG)
						ep0_status = EP0_IDLE;

					iAP_USB_Role_Switch_Init();
                                        
					break;

				case FUNCTIONFS_UNBIND:
					ep0_status = EP0_WAITCONFIG;
					break;

				case FUNCTIONFS_ENABLE:
				        //printf("\n event bRequestType %d\n", event->u.setup.bRequestType);
					//printf("\n event bRequest %d\n", event->u.setup.bRequest);
					//printf("\n event wValue %d\n", event->u.setup.wValue);
					//printf("\n event wIndex %d\n", event->u.setup.wIndex);
					//printf("\n event wLength %d\n", event->u.setup.wLength);
					if ( g_authentication_done )
					{					
						if (!gSmartphoneHandshake )
							IAP2_FEAT_NEA_CALLBACK(IAP2_FEAT_NEA_PROT_ACTIVE,0);
						else
							IAP2_FEAT_NEA_CALLBACK(IAP2_FEAT_NEA_PROT_INACTIVE,0);
					}
						
					break;

				case FUNCTIONFS_RESUME:
					break;

				case FUNCTIONFS_SUSPEND:
					break;

				case FUNCTIONFS_DISABLE:
					if (switch_to_usb_host_mode() == 0) 
						printf(  "USB BOARD SET TO HOST MODE.\n");

					ffs_running = 0;

					break;

				case FUNCTIONFS_SETUP:
					break;
				}
			}
		}else  {
			printf(  "Error: - read(ep0_thread) RETURN CODE: [%d]. (errno = %d)\n", ret, errno);
			usleep(1000);
		}
		usleep(10000);
	}

	close_device();
	printf(  "%s. ENDED\n",  __FUNCTION__);

	return NULL;
}


int ep0_thread_create(void)
{
	int ret;

        g_running = 1;
        ffs_running = 1;


	ret = pthread_create(&ep0_thread, NULL, ep0_thread_function, NULL);
	if (ret != 1) {
		printf(  "CREATED [ep0_thread] SUCCESSFULLY.\n");
		pthread_setname_np(ep0_thread, "ep0");
		ep0_thread_created = 1;
		return 0;
	}else  {
		printf(  "Error: ep0_thread creation failed [%d].\n", ret);
		return -1;
	}
}


int init_device(void)
{
	int ret;

	printf("\n Open /dev/mfi-ffs/ep0 \n ");
        ep0_fd = open(FFS_EP0, O_RDWR);
	if (ep0_fd < 0) {
		printf(  "Error: - CANNOT OPEN " FFS_EP0 "\n");
		ret = -errno;
		goto problem;
	}

        printf("\n Send USB descriptors \n");

	// Send USB descriptors
	ret = write(ep0_fd, &descriptors, sizeof(descriptors));
	if (ret < sizeof(descriptors)) {
		printf(  "Error: - CANNOT WRITE USB DECRIPTORS TO " FFS_EP0 "\n");
		ret = -errno;
		goto problem;
	}

        printf("\n Send USB strings \n "); 

	// Send USB strings
	ret = write(ep0_fd, &strings, sizeof(strings));
	if (ret < sizeof(strings)) {
		printf(  "Error: - CANNOT WRITE USB STRINGS TO " FFS_EP0 "\n");
		ret = -errno;
		goto problem;
	}

	printf("\n Allocate buffer for endpoint \n ");
        // Allocate buffer for each endpoint
	ep0_buffer = malloc(EP0_BUFFER_SIZE);
	if (!ep0_buffer) {
		printf(  "Error: - [ep0_buffer] IS NULL!\n");
		ret = -ENOMEM;
		goto problem;
	}

	return 0;

problem:
	close_device();
	return ret;
}

int close_device(void)
{
	int ret;

	if (ep0_buffer) {
		free(ep0_buffer);
		ep0_buffer = NULL;
	}

	if (ep0_fd > 0) {
		ret = close(ep0_fd);
		if (ret == 0)
			ep0_fd = -EINVAL;
		else{
			printf(  "Error: failed to close(ep0_fd) (errno = %d)\n", errno);
			return -1;
		}
	}

	return 0;
}

static void iAP_USBDev_read_thread(void *arg);
static void iAP_USBDev_read_thread_native(void *arg);
static void iAP_USBDev_write_thread_native(void *arg);

static int iAP_USBDev_init_usb_gffs_device(void)
{
    int ret = 0;

    dprint("Starting iAP_USBDev_init_usb_gffs_device \n");
    printf("\nStarting iAP_USBDev_init_usb_gffs_device \n");

    /* Allocate buffers for each end point. */
    ep1_buffer = malloc(EP1_BUFFER_SIZE);
    ep2_buffer = malloc(EP2_BUFFER_SIZE);
    if (!ep1_buffer || !ep2_buffer) {
        ret = -ENOMEM;
        goto exit_buffers;
    }
    ep3_buffer = malloc(EP3_BUFFER_SIZE);
    ep4_buffer = malloc(EP4_BUFFER_SIZE);
    if (!ep3_buffer || !ep4_buffer) {
        ret = -ENOMEM;
        goto exit_buffers;
    }

    /* Open USB gadget output end point. */
    ep1_fd = open(FFS_OUT, O_RDWR);
    if (ep1_fd < 0) {
        dprint("ERROR: Cannot open " FFS_OUT);
        printf("\nERROR: Cannot open FFS_OUT\n");
        ret = -ENOENT;
        goto exit_buffers;
    }
    printf("\noen FFS_OUT %s %d\n",FFS_OUT, ep1_fd);

    /* Open USB gadget input end point. */
    ep2_fd = open(FFS_IN, O_RDWR);
    if (ep2_fd < 0) {
        dprint("ERROR: Cannot open " FFS_IN);
        printf("\nERROR: Cannot open FFS_IN\n");
        close(ep1_fd);
        ret = -ENOENT;
        goto exit_buffers;
    }
    printf("\noen FFS_IN %s %d\n",FFS_IN, ep2_fd);
    ret = 0;

    
    ep3_fd = open(FFS_NATIVE_OUT, O_RDWR);
    if (ep3_fd < 0) {
        dprint("ERROR: Cannot open " FFS_NATIVE_OUT);
        printf("\nERROR: Cannot open FFS_NATIVE_OUT\n");
        ret = -ENOENT;
        goto exit_buffers;
    }
    printf("\noen FFS_NATIVE_OUT %s %d\n",FFS_NATIVE_OUT, ep3_fd);

    
    ep4_fd = open(FFS_NATIVE_IN, O_RDWR);
    if (ep4_fd < 0) {
        dprint("ERROR: Cannot open " FFS_NATIVE_IN);
        printf("\nERROR: Cannot open FFS_NATIVE_IN\n");
        close(ep1_fd);
        ret = -ENOENT;
        goto exit_buffers;
    }

	printf("\noen FFS_NATIVE_IN %s %d\n",FFS_NATIVE_IN, ep4_fd);

    iap_usb_device_read_enable = true;
    iap_usb_device_read_native_enable = true;
    iap_usb_device_write_native_enable = true;

    /* Start USB device read thread*/
    osa_thread_create(&read_thread_id, NULL, iAP_USBDev_read_thread, NULL);

    osa_thread_create(&read_native_thread_id, NULL, iAP_USBDev_read_thread_native, NULL);

    //osa_thread_create(&write_native_thread_id, NULL, iAP_USBDev_write_thread_native, NULL);

    /* This is IAP_ATTACH. */
    transportStatus |= (1 << 1);

    return ret;

/* If any error happend durin end point init free memory. */
exit_buffers:
    osa_free(ep1_buffer);
    osa_free(ep2_buffer);
    osa_free(ep3_buffer);
    osa_free(ep4_buffer);

    return ret;
}

extern uint8_t gEANativePortActive;
int g_ea_native_read_size = 0;
static void iAP_USBDev_read_thread_native(void *arg)
{
    int ret = 0;

    printf("\niAP_USBDev_read_thread_native started\n");
    printf("\niap_usb_device_read_native_enable %d\n",iap_usb_device_read_native_enable);
    while (iap_usb_device_read_native_enable) 
    {

	//if ( g_authentication_done )
	{
		//printf("\n Read on EA endpoint \n", ret);
		ret = read(ep4_fd, ep4_buffer, EP4_BUFFER_SIZE);
		printf("\n iAP_USBDev_read_thread_native ret %d \n", ret);
		if (ret < 0) {
		    printf("ERROR: iAP_USBDev_read_native, errno: %d ret %d\n", errno, ret);
		    //if(EAGAIN == errno ) {
		        continue;
		    //} else {
		    //    dprint("ERROR: iAP_USBDev_read_native, errno: %d\n", errno);
		    //    break;
		    //}
		}

		g_ea_native_read_size = ret;

	#ifdef MFI_DEBUG
		int i;
		//printf("  data: ");
		for (i = 0; i < ret; i++)
		    printf("%x ", ep4_buffer[i]);
		printf("\n");
	#endif

		//printf("\niAP_USBDev_fpNEARecvCallback ret %d\n",ret);        
		if (iAP_USBDev_fpNEARecvCallback) {
		    //printf("\n Callback is called \n");
		    iAP_USBDev_fpNEARecvCallback((uint8_t*) ep4_buffer, ret);
		    osa_time_delay(10);
		}
	}
	//else
	//   osa_time_delay(10);		
    }

    printf("iAP_USBDev_read_thread_native exiting\n");
}

static void iAP_USBDev_write_thread_native(void *arg)
{
    int ret = 0;

    
    printf("\niap_usb_device_write_native_enable %d\n",iap_usb_device_write_native_enable);
    while (iap_usb_device_write_native_enable) 
    {

	//if ( g_authentication_done )
	{
		
		ret = read(ep3_fd, ep3_buffer, EP3_BUFFER_SIZE);
		
		if (ret > 0) {
			int i;
			const struct usb_functionfs_event *event = ep3_buffer;
			for (i = 0;
			     i < ret / sizeof(struct usb_functionfs_event);
			     i += sizeof(struct usb_functionfs_event), event++) {
				printf(  "ep3_thread_function(): EVENT %s\n", names[event->type]);
				switch (event->type) {
				case FUNCTIONFS_BIND:
					break;

				case FUNCTIONFS_UNBIND:
					break;

				case FUNCTIONFS_ENABLE:
					break;

				case FUNCTIONFS_RESUME:
					break;

				case FUNCTIONFS_SUSPEND:
					break;

				case FUNCTIONFS_DISABLE:
					break;

				case FUNCTIONFS_SETUP:
					break;
				}
			}
		}
		

		if (ret < 0) {
		    //printf("ERROR: iAP_USBDev_write_native, errno: %d ret %d\n", errno, ret);
			osa_time_delay(10);
		        continue;
		}

	       if (iAP_USBDev_fpNEASentCallback)
	       {
		    iAP_USBDev_fpNEASentCallback();
		    osa_time_delay(1);
	       }

	}
    }

    printf("iAP_USBDev_write_thread_native exiting\n");
}

static void iAP_USBDev_read_thread(void *arg)
{
    int ret = 0;

    dprint("iAP_USBDev_read_thread started\n");
    printf("\niAP_USBDev_read_thread started\n");

    while (iap_usb_device_read_enable) {
        ret = read(ep2_fd, ep2_buffer, EP2_BUFFER_SIZE);
        if (ret < 0) {
            if(EAGAIN == errno ) {
                continue;
            } else {
                dprint("ERROR: iAP_USBDev_read, errno: %d\n", errno);
                break;
            }
        }

#ifdef MFI_DEBUG
        int i;
        printf("  data: ");
        for (i = 0; i < ret; i++)
            printf("%x ", ep2_buffer[i]);
        printf("\n");
#endif

        if (iAP_USBDev_fpRecvCallback) {
            iAP_USBDev_fpRecvCallback((uint8_t*) ep2_buffer, ret);
            osa_time_delay(10);
        }
    }

    dprint("iAP_USBDev_read_thread exiting\n");
}
//OsaTimeval gs_timeval;
//OsaTimeval ge_timeval;
uint16_t iAP_USBDev_Transmit_Message(uint8_t *bpBuff, uint16_t wSize)
{
    uint16_t ret;

    dprint("USB bulk msg send, size: %d\n", wSize);
    //printf("USB bulk msg send, size: %d %d\n", wSize, ep1_fd);

#ifdef MFI_DEBUG
    int i;
    printf("  data: ");
    for (i = 0; i < wSize; i++)
        printf("%x ", bpBuff[i]);
    printf("\n");
#endif

    //osa_time_get(&gs_timeval);

    ret = write(ep1_fd, bpBuff, wSize);

    //osa_time_get(&ge_timeval);

    //printf("\n Start Time %d.%d\n",gs_timeval.tv_sec, gs_timeval.tv_usec);
    //printf("\n End Time   %d.%d\n",ge_timeval.tv_sec, ge_timeval.tv_usec);		
    
    //printf("iAP_USBDev_Transmit_Message: %d \n", ret);
    if (ret < wSize) {
        dprint("iAP_USBDev_Transmit_Message ERROR: writing\n");
        //printf("iAP_USBDev_Transmit_Message ERROR: writing\n"); 
        return -1;
    }

    //printf("iAP_USBDev_Transmit_Message callback:\n");
    if (iAP_USBDev_fpSentCallback)
        iAP_USBDev_fpSentCallback();

   //printf("iAP_USBDev_Transmit_Message Done\n");  

    return 0;
}

uint16_t iAP_USBDev_NEATransmit(uint8_t *bpBuff, uint16_t wSize)
{
    uint16_t ret;

    //printf("USB bulk msg send, size: %d %d\n", wSize, ep3_fd);

    ret = write(ep3_fd, bpBuff, wSize);

    //printf("iAP_USBDev_Transmit_Message: %d \n", ret);
    if (ret < wSize) {
        //printf("iAP_USBDev_Transmit_Message ERROR: writing\n"); 
        return -1;
    }

    if (iAP_USBDev_fpNEASentCallback)
        iAP_USBDev_fpNEASentCallback();

   //printf("iAP_USBDev_Transmit_Message Done\n");  

    return 0;
}

void iAP_USBDev_NEADataReceive(uint8_t * pAppBuffer)
{
    int ret = 0;
    int i;
	
    for (i = 0; i < g_ea_native_read_size; i++)
	pAppBuffer[i] = ep4_buffer[i];

#if 1//def MFI_DEBUG
        printf("\n  data: ");
        for (i = 0; i < g_ea_native_read_size; i++)
            printf("%x ", pAppBuffer[i]);
        printf("\n");
#endif

   g_ea_native_read_size = 0;
}


uint16_t iAP_USBDev_Status(void)
{
    return transportStatus;
}

//void iAP_USBDev_SetRecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback)
//{
//    rxCallback = fpCallback;
//}
//
//void iAP_USBDev_SetSentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback)
//{
//    txCallback = fpCallback;
//}
//
void iAP_USBDev_SetSentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback)
{
	iAP_USBDev_fpSentCallback = fpCallback;
}

void iAP_USBDev_SetRecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback)
{
	iAP_USBDev_fpRecvCallback = fpCallback;
}

void iAP_USBDev_SetNEASentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback)
{
	iAP_USBDev_fpNEASentCallback = fpCallback;
}

void iAP_USBDev_SetNEARecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback)
{
	iAP_USBDev_fpNEARecvCallback = fpCallback;
}

#if 1
#define A_DEV_BUS_REQ  "/sys/bus/platform/devices/ci_hdrc.0/inputs/a_bus_req"

int switch_to_usb_host_mode(void)
{
	FILE* fp;

	printf(  "SWITCHING TO USB HOST MODE..\n");

	fp = fopen(A_DEV_BUS_DROP, "w");
	if (fp) {
		/* write '1' to the A_DEV_BUS_DROP
		   file to ask the host to drop the 'host' mode */
		fwrite(HIGH, 1, 1, fp);
		//ms_sleep(USB_DEV_TO_HOST_SWITCH_TIMEOUT); /* Should not be smaller */
		fclose(fp);

		fp = fopen(A_DEV_BUS_DROP, "w");
		if (fp) {
			/* write '0' to the A_DEV_BUS_DROP
			    file to ask the host to drop the 'host' mode */
			fwrite(LOW, 1, 1, fp);
			//ms_sleep(USB_DEV_TO_HOST_SWITCH_TIMEOUT); /* Should not be smaller */
			fclose(fp);

			fp = fopen(A_DEV_BUS_REQ, "w");
			if (fp) {
				/* write '1' to the A_DEV_BUS_REQ
				   file to switch to 'host' mode */
				fwrite(HIGH, 1, 1, fp);
				//ms_sleep(USB_DEV_TO_HOST_SWITCH_TIMEOUT); /* Should not be smaller */
				fclose(fp);

				return 0;
			}else  {
				printf(  "Error: - USB BOARD COULD NOT BE SET IN HOST MODE.\n");
				return -1;
			}
		}else  {
			printf(  "Error: - USB BOARD COULD NOT BE SET IN HOST MODE.\n");
			return -1;
		}
	}else  {
		printf(  "Error: - USB BOARD COULD NOT BE SET IN HOST MODE.\n");
		return -1;
	}
}

static int switch_to_usb_device_mode(void)
{
	int32_t ret;
	FILE* fp;

	printf(  "SWITCHING TO USB DEVICE MODE..\n");

	/* perform role switch to device mode */
	fp = fopen(A_DEV_BUS_REQ, "w");

	if (fp) {
		/* write '0' to the A_DEV_BUS_REQ
		   file to switch the 'device' mode */
		fwrite("0", 1, 1, fp);
		ret = 0;
		fclose(fp);
	}else
		ret = -1;

	return ret;
}

static int32_t usb_mfi_request_role_switch(uint32_t vid,
                                           uint32_t pid,
                                           uint16_t value)
{
    libusb_device_handle* devHandle;
    unsigned char buffer[10];
    uint32_t reqStatus;
    int ret;

    printf("\n usb_mfi_request_role_switch \n"); 

    ret = libusb_init(NULL);
    if ( ret < 0 )
       printf("\n libusb init failed \n");
   
	printf("Apple VID = %d \n",vid);
	printf("Apple PID = %d \n",pid);
    devHandle = libusb_open_device_with_vid_pid (NULL, vid, pid);
    if(devHandle){
        printf("\n  iAP2_USB_Device : Send role switch USB request using received value \n");
        memset(buffer, 0, sizeof(char) * 10); 


	/* Read mfi Device caps */
	libusb_control_transfer(devHandle,
				(LIBUSB_ENDPOINT_IN |
				 LIBUSB_REQUEST_TYPE_VENDOR |
				 LIBUSB_RECIPIENT_DEVICE),
				USB_MFI_CAPS_REQUEST,
				USB_MFI_CAPS_REQUEST_VALUE,
				USB_MFI_CAPS_REQUEST_INDEX,
				buffer,
				USB_MFI_CAPS_REQUEST_LENGTH,
				TIMEOUT);

	if (buffer[0] == USB_MFI_ROLE_SWITCH_CARPLAY_SUPPORTED)
		value = 1;


        /* Send role switch USB request using received value */
        reqStatus = libusb_control_transfer(devHandle,
                (LIBUSB_ENDPOINT_OUT |
                        LIBUSB_REQUEST_TYPE_VENDOR |
                        LIBUSB_RECIPIENT_DEVICE),
                        0x51,
                        value,
                        0x00,
                        NULL,
                        0x00,
                        10000);

#if 1
	/* Loop to detect the mfi device disconnection from USB bus */
	while (1) {
		int i, j;
		i = libusb_get_configuration(devHandle, &j);
		if (i != 0)
			break;
		usleep(10000);	
	}
	printf("Debug: Closing libusb \n");
	libusb_close(devHandle);
	libusb_exit(NULL);

#endif
	switch_to_usb_device_mode();
	printf("Debug: Switching to device mode \n");

	usleep(100000);

        wait_for_role_switch_done = 1;
	
	//g_wUSBHostStatus |= ( 1 << 5 );
	//g_wUSBHostStatus |= ( 1 << 6 );

    }else {
        printf("\n Invalid argument \n");  
        reqStatus=ERRCODE_INVALID_ARGUMENT;
    }
    printf("USB MFI role switch status: %i\n", reqStatus);

    /* return request status. if transaction successful value should be zero */
    return reqStatus;

}

unsigned int device_vendor_id, device_product_id;
unsigned int rdevice_vendor_id,rdevice_product_id;//SKM:Type A USB problem

void _process_usb_netlink_msg(struct udev_device *dev, int type)
{
    char *tempVid;
    char *tempPid;
    char *tempSerial;

    char *action = udev_device_get_action(dev);
    char *devpath_str = udev_device_get_devpath(dev);

    if ((action && !strcmp(action, "add")) || type & 0x04) {
        char *subsys_str = udev_device_get_subsystem(dev);

        /* read the device serial number */
        tempSerial = udev_device_get_sysattr_value(dev,"serial");

        /* new USB device to be added */
        if (devpath_str && (0x01 & type) &&
            !strcmp(subsys_str,"usb")) {

            /* read vendor ID to determine device type */
            tempVid = udev_device_get_sysattr_value(dev,"idVendor");

            if (!tempVid)
                return;

            device_vendor_id = strtol(tempVid, NULL, 16);

            printf("VID: 0x%x\n", device_vendor_id);

            /* read product ID to command AOAP mode */
            tempPid = udev_device_get_sysattr_value(dev,"idProduct");

            if (!tempPid)
                return;

            device_product_id = strtol(tempPid, NULL, 16);
            printf("PID: 0x%x\n", device_product_id);

            if (0x05ac == device_vendor_id) {
                rdevice_vendor_id = device_vendor_id;//SKM:Type A USB problem
                rdevice_product_id = device_product_id;//SKM:Type A USB problem
                printf("USB Apple Device Detected\n");
            } 
        } 
    } 
}


static int _check_usb_devices(void)
{
    struct udev *udev;
    struct udev_enumerate *enumerate;
    struct udev_list_entry *devices, *dev_list_entry;
    struct udev_device *dev;

    /* Create the udev object */
    udev = udev_new();
    if (!udev) {
        printf("unable to create udev object\n");
        return -1;
    }

    enumerate = udev_enumerate_new(udev);
    udev_enumerate_add_match_subsystem(enumerate, "usb");

    udev_enumerate_scan_devices(enumerate);
    devices = udev_enumerate_get_list_entry(enumerate);

    udev_list_entry_foreach(dev_list_entry, devices) {
        const char *path;

        path = udev_list_entry_get_name(dev_list_entry);
        dev = udev_device_new_from_syspath(udev, path);

        const char *type = udev_device_get_property_value(dev, "TYPE");

        if(udev_device_get_devtype(dev) &&
           (!strcmp("usb_device", udev_device_get_devtype(dev)))
           &&
           !strstr(type, "9/") /* do not add hubs */
           ) {
            _process_usb_netlink_msg(dev, 0x01 | 0x04);
        }
    }
    return 0;
}


void iAP_USB_Role_Switch_Init(void)
{
    int32_t ret = 0;
    
    // perform role switch
    _check_usb_devices();    

    /* read vendor ID */
	ret=usb_mfi_request_role_switch(rdevice_vendor_id,rdevice_product_id ,0);//Type A usb enumeration problem
//    ret=usb_mfi_request_role_switch(device_vendor_id,device_product_id ,0);
    
    if ( ret == 0 )
    {
        //iAP_USBDev_Init();
    }
}
#endif


/*
 * Initialize the FunctionFS driver for USB iAP2 communication.
 * In order for this to succeed, the FFS kernel module (g_ffs) must be loaded
 * prior to starting and the appropriate filesystem must be mounted under
 * /dev/cas-iap2-gffs.
 */
int32_t iAP_USBDev_Init(void)
{
    int ret;

    init_device();

    ep0_thread_create();

    //iAP_USB_Role_Switch_Init();
    
    while( wait_for_role_switch_done == 0 )
    {
	usleep(10000);
    }
       
    transportStatus = 0;
    g_authentication_done = FALSE;
    /*Init USB gadget end points and read thread*/
    ret = iAP_USBDev_init_usb_gffs_device();

    if (ret != 0)
        return ret;
    return ret;
}

void iAP_USBDev_UnInit(void)
{

    printf("\n iAP_USBDev_UnInit \n ");
    /* Reset transport status */
    transportStatus = 0;

    /*Stop USB Dev read thread*/
    iap_usb_device_read_enable = false;
    iap_usb_device_read_native_enable = false;
    iap_usb_device_write_native_enable = false;
    g_authentication_done = FALSE;

    if (switch_to_usb_host_mode() == 0) 
	printf(  "USB BOARD SET TO HOST MODE.\n");


    /* stop ep0 thread */
    g_running = 0;
    ffs_running = 0;

    /*Close USB gadget end points*/
    if (ep2_fd > 0)
        close(ep2_fd);
    ep2_fd = -EINVAL;

    if (ep1_fd > 0)
        close(ep1_fd);
    ep1_fd = -EINVAL;

    /*Close USB gadget end points*/
    if (ep4_fd > 0)
        close(ep4_fd);
    ep4_fd = -EINVAL;

    if (ep3_fd > 0)
        close(ep3_fd);
    ep3_fd = -EINVAL;

    /*Free memory buffers used by USB gadget end points*/
    osa_free(ep1_buffer);
    osa_free(ep2_buffer);

    ep1_buffer = NULL;
    ep2_buffer = NULL;
}

uint32_t iap_accessory_io_acc_power_high      (void)
{
    return 0;
}

uint32_t iap_accessory_io_ios_detect_low    (void)
{
   return 0;
}

void iap_accessory_io_set_acc_detect        (void)
{

}
void iap_accessory_io_clear_acc_detect        (void)
{
}

void iAP_USBDev_DataReceive()
{
	//dummy
}

void USB_Composite_DeInit()
{
}
