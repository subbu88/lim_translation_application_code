/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef _IAP_USBHOST_H_
#define _IAP_USBHOST_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "MFi_Config.h"
#include "osa_common.h"
#include "iAP_Transport.h"

#if (FSL_OS_SELECTED == FSL_OS_MQX)
//#include "host_main.h"
#include "fio.h"
#endif

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/

//#ifdef CASCFG_PLATFORM_LINUX
#define HID_REPORT_MAX_SIZE         (1024)
#define MAX_REPORT_SIZE             (1024)
//#else
//#define HID_REPORT_MAX_SIZE         (64)
//#define MAX_REPORT_SIZE             (64)
//#endif

#define LCB_FIRSTREPORT             (0x00)
#define LCB_CONTINUINGREPORT        (0x01)
#define LCB_MOREREPORTSTOFOLLOW     (0x02)
#define IAP1_SOP_BYTE               (0x55)
#define IAP2_SOP_BYTE               (0xFF)
#define IAP_MAX_REPORTS             (40)

/* MFI HID read task info */
#define MFI_HID_READ_TASK_STACK_SIZE (1024 * 2)
#define MFI_HID_READ_TASK_INTERVAL   (500)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
/* HID Definitions */
typedef enum {
    IAP_HID_REPORT_ID_IDX = 0,
    IAP_HID_REPORT_LCB_IDX = 1,
    IAP_HID_REPORT_SOP_IDX = 2,
    IAP_HID_REPORT_SIZE_IDX = 3,
    IAP_HID_REPORT_DATA_IDX = 4,
}IAPReportIndex;

typedef enum
{
	USB_HOST_TX_IN_PROGRESS = 0,
	USB_IDEV_INTERFACED,
	USB_IDEV_ATTACHED,
	USB_REPORT_SENT,
	USB_REPORT_RECEIVED,
	USB_ROLE_SWITCH_DONE,
	USB_ROLE_CHANGED,
	USB_ADEV_ATTACHED,
	USB_DEV_ANDROID_READY
}_eUsbHostStatus;


typedef enum
{
    IAP_NO_REPORT = 0,
    IAP_INPUT_REPORT,
    IAP_OUTPUT_REPORT
} IapHIDReportType;

typedef struct {
    IapHIDReportType          report_type;
    uint32_t                  report_id;
    uint32_t                  report_size;
    uint32_t                  report_field_index;
} IapHIDReportInfo;

typedef struct _IapUSBHostIntf {
    FILE *hid_fp;
    int hid_fd;
    OsaThread iap_read_thread;
    bool iap_thread_exit;
    IapHIDReportInfo    report_info[IAP_MAX_REPORTS];
    uint32_t            num_reports;
    uint16_t            max_payload_size;
} IapUSBHostIntf;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
int32_t iAP_USBHost_Init(void);
void iAP_USBHost_UnInit(void);
void iAP_USBHostTask(void);
void iAP_USBHost_DataReceive(void);
uint16_t iAP_USBHost_Transmit_Message(uint8_t *bpBuff, uint16_t wSize);

#ifdef MFI_IAP2
void iAP_USBHost_SetRecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback);
void iAP_USBHost_SetSentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback);
#endif
uint16_t iAP_USBHost_Status(void);

/*************************************************************************************************/

#endif /* IAP_USBHOST_H_ */
