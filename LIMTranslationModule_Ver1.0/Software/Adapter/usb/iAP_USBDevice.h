
#ifndef IAP_USBDEVICE_H
#define IAP_USBDEVICE_H

/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    carplay_ffs.h
 * @brief   FunctionFS driver for USB device mode iAP communication.
 * @details User-space driver through FunctionFS USB device interface for
 *          device mode FAS to connect to USB host Apple device for iAP2
 *          communication over bulk USB endpoints.
 */

#include <endian.h>


#define MAX_PACKET_SIZE_FS 64
#define MAX_PACKET_SIZE_HS 512


#define FFS_OUT "/dev/mfi-ffs/ep1"
#define FFS_IN  "/dev/mfi-ffs/ep2"
#define FFS_NATIVE_OUT "/dev/mfi-ffs/ep3"
#define FFS_NATIVE_IN  "/dev/mfi-ffs/ep4"


typedef enum {
    USB_DEV_TX_IN_PROGRESS = 0,
    USB_ENUMERATED,
    USB_BUS_SUSPENDED
} _eUsbStatus;

int32_t iAP_USBDev_Init(void);
void iAP_USBDev_UnInit(void);
uint16_t iAP_USBDev_Transmit_Message(uint8_t *bpBuff, uint16_t wSize);

//void iAP_USBDev_SetRecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback);
//void iAP_USBDev_SetSentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback);

uint16_t iAP_USBDev_Status(void);
void iAP_USBDev_DataReceive(void);

uint16_t iAP_USBDev_NEATransmit(uint8_t *bpBuff, uint16_t wSize);
//void iAP_USBDev_SetNEARecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback);
//void iAP_USBDev_SetNEASentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback);
void iAP_USBDev_NEADataReceive(uint8_t * pAppBuffer);
#endif

