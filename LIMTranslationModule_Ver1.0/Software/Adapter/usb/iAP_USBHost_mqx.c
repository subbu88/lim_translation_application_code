
#include "osa_types.h"
#include "osa_common.h"
#include "ProjectTypes.h"
#include "MFi_Config.h"
#include "iAP_Transport.h"
#include "usb_hid_intf.h"
#include "usb_host_hid.h"
#include "iAP_USBHost.h"
#ifdef MFI_IAP1
#include "iAP_Interface.h"
#else
#define USB_ATTACH								(1<<1)
#endif
#include "iAP_USB_Audio_Interface.h"

extern char *hid_dev_name;

#if defined(__IAR_SYSTEMS_ICC__)
__no_init static uint8_t iAPOutReport[HID_REPORT_MAX_SIZE];
#else
static uint8_t __attribute__((section(".usbSection"))) iAPOutReport[HID_REPORT_MAX_SIZE];
#endif
#if defined(__IAR_SYSTEMS_ICC__)
__no_init static uint8_t iAPInReport[HID_REPORT_MAX_SIZE];
#else
static uint8_t __attribute__((section(".usbSection"))) iAPInReport[HID_REPORT_MAX_SIZE];
#endif
static IAP_TRANSPORT_RECV_CALLBACK	iAP_USBHost_fpRecvCallback;
static IAP_TRANSPORT_SENT_CALLBACK	iAP_USBHost_fpSentCallback;
static IapUSBHostIntf iap_usb_host_intf;
static uint16_t g_wUSBHostStatus;
OsaMutex usbHostCloseMutex;
OsaSem usbHostDataSync;

static uint16_t iap_send_report(void *hid_fp, uint8_t *buff, uint32_t length,
                                uint32_t report_id, uint32_t report_size);
static uint32_t iap_get_hid_reports_info(uint32_t fd,
                                         IapHIDReportInfo *report_info,
                                         IapHIDReportType iap_report_type);
static uint32_t iap_USBHost_SendReport(void *hid_fp, uint32_t report_id,
                                       uint8_t *buff, uint32_t size);


void iap_ReadTask(void *para)
{
    uint8_t tempByte;
    uint32_t size = 0;
    int32_t act_len = 0;
    uint32_t data_start_pos = 2;
    FILE *fp = (FILE *)para;
    uint8_t err_count = 0;
    int semStatus;

    while(1) {
        if (iap_usb_host_intf.iap_thread_exit) {
            dprint("iap_ReadTask exiting\n");
            break;
        }

        /* Only wait for the semaphore if there's no USB read error */
        if(0 == err_count) {
            /* The MFi stack will post the semaphore after it finishes processing
             * the last received packet. This task will wait for that before
             * getting more data from USB*/
            semStatus = osa_sem_wait(&usbHostDataSync, 2000);
        } else {
            semStatus = ERRCODE_NO_ERROR;
        }

        if(semStatus == ERRCODE_NO_ERROR) {
            act_len = fread(iAPInReport, 1, HID_REPORT_MAX_SIZE, fp);
            if (act_len <= 0) {
                size = 0;
                err_count++;

                if (err_count >= 2)
                    break;
                else
                    osa_time_delay(10);

            } else {
                err_count = 0;
                if(iAP_USBHost_fpRecvCallback != NULL)	{
                    iAP_USBHost_fpRecvCallback(&iAPInReport[2], act_len - 2);
                }

                g_wUSBHostStatus |= (1 << USB_REPORT_RECEIVED);
            }
        }else {
            dprint("Semaphore error: %d in %s:\n", semStatus, __FUNCTION__);
        }
    }

    g_wUSBHostStatus &= (uint16_t)(~(USB_ATTACH));

    dprint("iap_ReadTask exiting\n");
}

static uint8_t iap_get_report_index(IapHIDReportInfo * report_info,
                                    uint32_t total_num_reports,
                                    IapHIDReportType report_type,
                                    uint32_t length,
                                    uint32_t * puiIndex)
{
    uint8_t ret = 0;
    uint32_t report_size = 0;
    uint8_t i = 0;

    if ((!report_info) ||
        (!puiIndex)) {
        printf("%s:parameter error!\n",__FUNCTION__);
        return -1;
    }

    *puiIndex = 0;
    for (i = 0; i < total_num_reports; i++) {
        /* Check if report type matches */
        if (report_info[i].report_type == report_type) {
            if (report_size > length){
                /* Check if the report size is between current report size and
                 * required report size
                 */
                if ((report_size - length) >
                    (report_info[i].report_size - length)) {
                    report_size = report_info[i].report_size;
                    *puiIndex = i;
                }
            } else {
                /* Check if report size is greater than current report size */
                if (report_size < report_info[i].report_size) {
                    report_size = report_info[i].report_size;
                    *puiIndex = i;
                }
                /* if found the exact size return current index */
                if (report_size == length) {
                    break;
                }
            }
        }
    }

    if (report_size) {
        ret = 0;
    }

    return ret;
}

uint16_t iAP_USBHost_Transmit_Message(uint8_t *bpBuff, uint16_t wSize)
{
    uint32_t report_index = 0;
    uint16_t ret = 0;

    if (!bpBuff) {
        printf("%s:parameter error!\n",__FUNCTION__);
        return 1;
    }

    /* Get output report of equal or suitable size for sending data */
    if (iap_get_report_index(iap_usb_host_intf.report_info,
                             iap_usb_host_intf.num_reports,
                             IAP_OUTPUT_REPORT, wSize + 1,
                             &report_index) == 0) {
        /* Get the report id and size using report_index and send iap data */
        ret = iap_send_report(iap_usb_host_intf.hid_fp,
                              bpBuff,
                              wSize,
                              iap_usb_host_intf.report_info[report_index].report_id,
                              iap_usb_host_intf.report_info[report_index].report_size);
        if(iAP_USBHost_fpSentCallback != NULL) {
            iAP_USBHost_fpSentCallback();
        }

    } else {
        printf("iap_send_packet failed !! suitable output report not found\n");
    }

    return ret;
}

uint16_t iap_send_report(void *hid_fp, uint8_t *buff, uint32_t length,
                         uint32_t report_id, uint32_t report_size)
{
    uint32_t wDataIndex = 0;
    uint8_t bLCB = LCB_FIRSTREPORT;

    if (!buff) {
        printf("%s:parameter error!\n",__FUNCTION__);
        return -1;
    }

	/* Send command through HID report */
	if((g_wUSBHostStatus & (1 << USB_HOST_TX_IN_PROGRESS)) == 0)
	{
	    /* till all the data is not transmitted */
        g_wUSBHostStatus |= (1 << USB_HOST_TX_IN_PROGRESS);
        iAPOutReport[0] = report_id;
        while (length > 0)
        {
            /* Determine next LCB packet */
            if (report_size <= length)
                bLCB |= LCB_MOREREPORTSTOFOLLOW; /* multiple packets */
            else
                bLCB &= ~(LCB_MOREREPORTSTOFOLLOW); /* single packet */

            iAPOutReport[1] = bLCB;
            osa_memcpy(&iAPOutReport[2], &buff[wDataIndex], report_size - 1);
            if ((report_size - 1) > length) {
                    osa_memset(&iAPOutReport[length + 2], 0,
                               report_size - length - 1);
            }
            if (iap_USBHost_SendReport(hid_fp,report_id, iAPOutReport, report_size) == 0 ) {
                length -= ((report_size - 1) > length)? length:(report_size - 1);
                wDataIndex +=(report_size - 1);
                bLCB |= LCB_CONTINUINGREPORT;
            } else {
                break;
            }

        }
        g_wUSBHostStatus &= ~((1 << USB_HOST_TX_IN_PROGRESS));
   } else {
       return -1;
   }

    return 0;
}

static void iap_get_report_descriptor_info(IapUSBHostIntf *usb_host_intf)
{
    FILE *fp = usb_host_intf->hid_fp;
    /* calculate total number of reports */
    usb_host_intf->num_reports = 0;

    /* Get all input reports inforamtion */
    usb_host_intf->num_reports += iap_get_hid_reports_info((uint32_t) fp,
            &usb_host_intf->report_info[usb_host_intf->num_reports],
            IAP_INPUT_REPORT);

    /* Get all output reports information */
    usb_host_intf->num_reports += iap_get_hid_reports_info((uint32_t) fp,
            &usb_host_intf->report_info[usb_host_intf->num_reports],
            IAP_OUTPUT_REPORT);
}

/**
 * iap_get_hid_report_type:
 *
 * @iap_report_type: enum value for iap report type
 *
 * Function Description: Returns the hid report type enum value
 * for apHIDReportType enum value.
 */
static uint32_t iap_get_hid_report_type(IapHIDReportType iap_report_type)
{
    uint32_t report_type = 0;

    switch(iap_report_type) {
    case IAP_INPUT_REPORT:
        report_type = HID_REPORT_TYPE_INPUT;
        break;

    case IAP_OUTPUT_REPORT:
        report_type = HID_REPORT_TYPE_OUTPUT;
        break;

    default:
        /* invalid report type*/
        report_type = 0;
        break;
    }

    return report_type;
}

uint32_t iap_get_hid_reports_info(uint32_t fd,
                                  IapHIDReportInfo *report_info,
                                  IapHIDReportType iap_report_type)
{
    HID_REPORT_STRUCT dev_report_info;
    FILE *fp = (FILE *)fd;
    uint32_t index = 0, report_type = 0;

    if (!report_info) {
        printf("get_hid_reports_info:paramter error!\n");
        return -1;
    }

    /* get hid report type names for input and output reports */
    report_type = iap_get_hid_report_type(iap_report_type);

    /* Check for valid report_type request */
    if ((report_type == HID_REPORT_TYPE_INPUT) ||
        (report_type <= HID_REPORT_TYPE_OUTPUT)) {

        /* Traverse through reports and store required information */
        dev_report_info.type = report_type;

        /* ioctl returns non-zero for no more next report ID */
        while (ioctl(fp, USB_HID_IOCTL_GET_REPORT_INFO, &dev_report_info)
               == MQX_OK) {

            /* Got report information */
            report_info[index].report_id = dev_report_info.id;
            report_info[index].report_type = iap_report_type;
            report_info[index].report_size = dev_report_info.size;
            report_info[index].report_field_index = 0;

            /* Get next report */
            dev_report_info.type = report_type;
            index++;
        }
    }

    /* return number of reports filled */
    return index;
}

uint32_t iap_USBHost_SendReport(void *hid_fp, uint32_t report_id,
                                uint8_t *buff, uint32_t size)
{
    FILE *fp = (FILE *)(hid_fp);
    HID_SET_REPORT_STRUCT set_report_para;
    uint32_t ret = 0;

    if (!buff) {
        printf("iAP_USBHost_SendReport:parameter error!\n");
        return -1;
    }
    osa_memset(&set_report_para,0x00,sizeof(HID_SET_REPORT_STRUCT));
    set_report_para.report_id = (uint8_t) report_id;
    set_report_para.report_type = HID_REPORT_TYPE_OUTPUT;
    set_report_para.report_size = (uint16_t) (size + 1);
    set_report_para.report_data = buff;

    /* Protect access to the fp because it can be uninitialised by 
     * the MFI task when detecting a disconnection */
    osa_mutex_lock(&usbHostCloseMutex);
    if(fp != NULL) {
        ret = ioctl(fp,USB_HID_IOCTL_SET_REPORT,&set_report_para);
    }else {
        ret = MQX_INVALID_POINTER;
    }
    osa_mutex_unlock(&usbHostCloseMutex);
    
    return ret;
}

void iAP_USBHost_SetSentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback)
{
	iAP_USBHost_fpSentCallback = fpCallback;
}

void iAP_USBHost_SetRecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback)
{
	iAP_USBHost_fpRecvCallback = fpCallback;
}

uint16_t iAP_USBHost_Status(void)
{
	return g_wUSBHostStatus;
}

void iAP_USBHostTask(void)
{
    /* dummy function for FAS */
}

int32_t iAP_USBHost_Init(void)
{
    OsaThreadAttr iar_read_thread_attr;
    int32_t ret = 0;

    osa_memset(&iap_usb_host_intf, 0x00, sizeof(iap_usb_host_intf));
    osa_memset(&iar_read_thread_attr, 0x00, sizeof(iar_read_thread_attr));
    g_wUSBHostStatus |= (USB_ATTACH);

    iap_usb_host_intf.hid_fp = fopen(hid_dev_name, "rd");
    if (iap_usb_host_intf.hid_fp) {
        iap_get_report_descriptor_info(&iap_usb_host_intf);
    } else {
        return -1;
    }

    iap_usb_host_intf.iap_thread_exit = false;

    osa_mutex_create(&usbHostCloseMutex, false);
    osa_sem_create(&usbHostDataSync, 1);
    
    /* Create iAP data read task */
    osa_thread_attr_init(&iar_read_thread_attr);
    osa_thread_attr_set_stack_size(&iar_read_thread_attr,
                                   MFI_HID_READ_TASK_STACK_SIZE);
    osa_thread_attr_set_sched_interval(&iar_read_thread_attr,
                                       MFI_HID_READ_TASK_INTERVAL);
    osa_thread_attr_set_sched_priority(&iar_read_thread_attr,
                                      OSA_SCHED_PRIORITY_NORMAL);
    ret = osa_thread_create(&(iap_usb_host_intf.iap_read_thread),
                            &iar_read_thread_attr,
                            iap_ReadTask,
                            iap_usb_host_intf.hid_fp);
    osa_thread_set_name(iap_usb_host_intf.iap_read_thread, "iap read Task");
    osa_thread_attr_destroy(&iar_read_thread_attr);
    return ret;
}

void iAP_USBHost_DataReceive(void)
{
    osa_sem_post(&usbHostDataSync);
}

void iAP_USBHost_UnInit(void)
{
    g_wUSBHostStatus &= (uint16_t)(~(USB_ATTACH));
    iap_usb_host_intf.iap_thread_exit = true;
    osa_sem_destroy(&usbHostDataSync);
    
    osa_mutex_lock(&usbHostCloseMutex);
    fclose(iap_usb_host_intf.hid_fp);
    iap_usb_host_intf.hid_fp = NULL;
    osa_mutex_unlock(&usbHostCloseMutex);
    
    osa_mutex_destroy(&usbHostCloseMutex);
}

