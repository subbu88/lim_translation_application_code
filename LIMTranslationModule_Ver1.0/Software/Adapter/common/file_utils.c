
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    file_utils.c
 * @brief   This file provides file manipulation functions.
 */

#include "osa_common.h"
#ifdef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#include "mfi_cfg.h"
#endif
uint32_t file_getsize(FILE *fd)
{
#if (MFICFG_PLATFORM_MQX)
    uint32_t size = 0;

    size = (uint32_t)fgetsize((void*) fd);

    return size;
#else
    int current_location;
    int size;

    current_location = ftell((void*) fd);
    fseek((void*) fd, 0, SEEK_END);
    size = ftell((void*) fd);
    fseek((void*) fd, current_location, SEEK_SET);

    return (uint32_t) size;
#endif
}

bool file_exists(const char *filename)
{
    FILE *fp = fopen(filename, "r");
    if (fp) {
        fclose(fp);
        return true;
    }

    return false;
}

bool dir_exists(const char *dirname)
{
    OsaDir *dp = osa_opendir(dirname);
    if (dp) {
        osa_closedir(dp);
        return true;
    }

    return false;
}

