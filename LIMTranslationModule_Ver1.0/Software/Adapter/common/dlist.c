/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    dlist.c
 * @brief   This file implement the doubly-linked list APIs.
 *
 */

/*
 * INCLUDE FILES
 */
#include "dlist.h"


/*
 * GLOBAL FUNCTIONS
 */
void dlist_init_head(DList *head)
{
    head->next = head;
    head->prev = head;
}

void dlist_init_element(DList *element)
{
    element->next = element->prev = 0;
}

void dlist_insert(DList *new_, DList *head)
{
    head->next->prev = new_;
    new_->next = head->next;
    new_->prev = head;
    head->next = new_;
}

void dlist_insert_tail(DList *new_, DList *head)
{
    head->prev->next = new_;
    new_->next = head;
    new_->prev = (head)->prev;
    head->prev = new_;
}

void dlist_delete(DList *element)
{
    element->prev->next = element->next;
    element->next->prev = element->prev;
    dlist_init_element(element);
}

bool dlist_is_empty(DList *head)
{
    return (head->next == head);
}

