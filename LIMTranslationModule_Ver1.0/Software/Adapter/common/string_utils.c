/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    string_utils.c
 * @brief   This file provides string manipulation functions.
 */

#include "string_utils.h"
#include <string.h>

static int
utf16_to_utf8(const uint16_t *szSrc, int nSrcLen, char *strDest, int nDestLen);

/*
 * @brief   check if a string leading with a specific token
 * @details if we don't find the token, the source pointer will be restored
 *          to its original value, so that the subsequent calling of this
 *          function can search from the beginning again.
 *
 * @param[in] ppStr pointer to the source string pointer
 * @param[in] pKey pointer to token key.
 * @return true if source string is leading with the token, otherwise false
 */
bool str_match_token(char **ppStr, const char *pKey)
{
   bool Found = true;
   char *pStrSave = *ppStr;
   char *pKeySave = (char*)pKey;

   while ((Found) && (*pKey != '\0') && (**ppStr != '\0')) {
      if ( *pKey != **ppStr ) {
          Found = false;
      } else {
          pKey++;
         (*ppStr)++;
      }
   }

   /* restore the pointer if we don't find the token
    * Then later calling can continure search from the begining
    */
   if (!Found)
       *ppStr = pStrSave;

   if (!Found)
       dlprintf(LOGMDL_COMMON, "No match %s in %s\n", pKeySave, pStrSave);
   else
       dlprintf(LOGMDL_COMMON, "%s mactched in %s\n", pKeySave, pStrSave);

   return Found;
}

/*
 * @brief   extract a value string
 * @details if string is quoted, return the quoted string
 *          else return the first whitespace seperated token
 *          leading whitespace and '=' will be ignored.
 *          eg: =ABCD EFG       result: ABCD
 *              ="ABCD EFG"     result: ABCD EFG
 *              ABCD EFG        result: ABCD
 *              "ABCD EFG"      result: ABCD EFG
 *
 * @param[out] pDest pointer to save the value
 * @param[in] maxSz max length to be extracted
 * @param[in] ppSrc the pointer to source string pointer
 * @return    true if value string is extracted, otherwise false
 */
bool str_get_value_str(char *pDest, const int maxSz, char **ppSrc)
{
   bool ret = true;
   bool quoted = false;
   int cnt = 0;

   STR_SKIP_WHITESPACE(*ppSrc);
   if ((**ppSrc) == '=')
       (*ppSrc)++;  /* ignore the '=' if present */
   STR_SKIP_WHITESPACE(*ppSrc);

   if ((**ppSrc) == '"') {
      (*ppSrc)++;
      quoted = true;
   }

   if (quoted) {
      while ((cnt < maxSz - 1) && (**ppSrc != '\0') && (**ppSrc != '"' )) {
         pDest[cnt++] = **ppSrc;
         (*ppSrc)++;
      }
      pDest[cnt] = '\0';

      if((**ppSrc) == '"')
         (*ppSrc)++;
      else
          ret = false;
   } else {
       while ((cnt < maxSz -1) && (**ppSrc != ' ') &&
               (**ppSrc != '\t') && (**ppSrc != '\0')) {
           pDest[cnt++] = **ppSrc;
           (*ppSrc)++;
       }
       pDest[cnt] = '\0';
   }
   dlprintf(LOGMDL_COMMON, "get value str: %s\n", pDest);

   return ret;
}

/*
 * @details Get the number of characters of a unicode.
 *          include the terminator charactor (0x0000)
 *          we should avoid using wcslen. this function is not always work.
 * @param   s   The unicode string pointer.
 * @returns Number of characters in the string.
 */
uint32_t unicode_strlen(const uint16_t *s)
{
    uint32_t cnt = 0;
    if (s != NULL) {
        while (*s++)
            cnt++;
    }
    cnt++;  /* take account for the terminator character */
    return cnt;
}

uint16_t str_unicode2ascii(char *dst, const char *src, const uint16_t len)
{
    uint16_t i = 0;
    uint16_t str_len = 0;

    for(i = 0; i < len; i++) {
        if (0 != src[i])
            dst[str_len++] = src[i];
    }
    dst[str_len] = '\0';
    return str_len;
}

/* Swap the two bytes in a 16-bit value. */
static inline uint16_t bswap_16(uint16_t v)
{
    return (v << 8 | v >> 8);
}

/*
 * Take an input string and a given encoding and output a new string with new
 * length that is UTF-8.
 * 
 * Bug 01:
 * return value for overflow error (TEXT_ENCODING_STATUS_OVERFLOW,
 * value example 0x11) will overlaps to a string conversion of
 * 0x11 in length.
 * 
 * Bug 02:
 * does not convert same length strings, since source must be
 * smaller than the destination.
 *
 * Values for 'text_encoding':
 * 0 : ISO-8859-1.  This is basically 1-byte ASCII.
 * 1 : UTF-16.  Terminated with 00 00.
 * 2 : (ID3v2.4 only) UTF-16BE (big endian).  Terminated with 00 00.
 * 3 : (ID3v2.4 only) UTF-8.  Terminated with 00.
 */
int str_utf8_encode(const char *src, const int srclen, char *dest, int destlen,
                    const TextEncoding encoding)
{
    unsigned char uch;
    int i;
    uint16_t *src16 = (uint16_t*) src;
    int src16len;
    int16_t src16tmp;
    int destindex = 0;
    int ret = TEXT_ENCODING_STATUS_OK;

    /* Convert ISO-8859-1 to UTF-8, worst case is 2x inflation. */
    if (encoding == TEXT_ENCODING_ISO_8859_1) {
        for (i = 0; i < srclen; i++) {
            uch = *src++;
            if (uch == 0)
                break;
            else if (uch & 0x80) {
                if (destindex + 2 >= destlen) {
                    ret = TEXT_ENCODING_STATUS_OVERFLOW;
                    break;
                }

                *dest++ = (uch >> 6) | 0xc0;
                *dest++ = (uch & 0x3f) | 0x80;
                destindex += 2;
            } else {
                if (destindex + 1 >= destlen) {
                    ret = TEXT_ENCODING_STATUS_OVERFLOW;
                    break;
                }

                *dest++ = uch;
                destindex++;
            }
        }
        *dest = '\0';

    /* Convert UTF-16 to UTF-8. */
    } else if (encoding == TEXT_ENCODING_UTF16) {
        src16len = srclen >> 1;
        if (*(src16) == (uint16_t) 0xfffe) {
            src16++;
            src16len--;
            for (i = 0; i < src16len; i++) {
                src16tmp = bswap_16(*src16);
                *src16++ = src16tmp;
            }
        }

        if (*(src16) == (uint16_t) 0xfeff) {
            src16 += 1;
            src16len -= 1;
        }

        ret = utf16_to_utf8(src16, src16len, dest, destlen);
        if (ret == 0)
            ret = TEXT_ENCODING_STATUS_OVERFLOW;

    } else if (encoding == TEXT_ENCODING_UTF16_BE) {
        src16len = srclen >> 1;
        for (i = 0; i < src16len; i++) {
            src16tmp = bswap_16(*src16);
            *src16++ = src16tmp;
        }
        ret = utf16_to_utf8((uint16_t*)src, src16len, dest, destlen);
        if (ret == 0)
            ret = TEXT_ENCODING_STATUS_OVERFLOW;
    } else if (encoding == TEXT_ENCODING_UTF8) {
        /* Do nothing to the string since it's already in UTF-8. */
        if (srclen > destlen) {
            osa_memcpy(dest, src, destlen);
            dest[destlen - 1] = '\0';
            ret = TEXT_ENCODING_STATUS_OVERFLOW;
        } else
            osa_memcpy(dest, src, srclen);
    } else {
        ret = TEXT_ENCODING_STATUS_FAILED;
    }

    return ret;
}

/* The following code uses this copyright and license: */
/* ------------------------------------------------------------------
 * Copyright (C) 2008 PacketVideo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 * -------------------------------------------------------------------
 */
/* If less than this, it can be represented as 1-byte UTF-8. */
#define BYTE_1_REP          0x80
/* If less than this, it can be represented as 2-byte UTF-8. */
#define BYTE_2_REP          0x800

/* If the unicode value falls on or between these values, it will be
 * represented as 4 bytes. */
#define SURROGATE_MIN       0xd800
#define SURROGATE_MAX       0xdfff

/*
 * Convert a UTF-16 string to UTF-8.
 *
 * Return the number of bytes in the destination buffer on success or 0 if the
 * dest buffer has been overflown.  If overflown, the buffer is filled as much
 * as it can be.
 */
static int
utf16_to_utf8(const uint16_t *szSrc, int nSrcLen, char *strDest, int nDestLen)
{
    int i = 0;
    int i_cur_output = 0;
    char ch_tmp_byte;

    if (nDestLen <= 0)
        return 0;

    for (i = 0; i < nSrcLen; i++) {
        /* 1-byte UTF-8. */
        if (BYTE_1_REP > szSrc[i]) {
            if (i_cur_output + 1 < nDestLen)
                strDest[i_cur_output++] = (char) szSrc[i];
            else {
                strDest[i_cur_output] = '\0';
                return 0;
            }
        /* 2-byte UTF-8. */
        } else if (BYTE_2_REP > szSrc[i]) {
            if (i_cur_output + 2 < nDestLen) {
                strDest[i_cur_output++] = (char) ((szSrc[i] >> 6) | 0xc0);
                strDest[i_cur_output++] = (char) ((szSrc[i] & 0x3f) | 0x80);
            } else {
                strDest[i_cur_output] = '\0';
                return 0;
            }
        /* 4-byte surrogate pair representation. */
        } else if (SURROGATE_MAX > szSrc[i] && SURROGATE_MIN < szSrc[i]) {
            if (i_cur_output + 4 < nDestLen) {
                ch_tmp_byte = (char) (((szSrc[i] & 0x3c0) >> 6) + 1);
                strDest[i_cur_output++] = (char) ((ch_tmp_byte >> 2) | 0xf0);
                strDest[i_cur_output++] = (char) (((ch_tmp_byte & 0x03) | 0x80) |
                                                  (szSrc[i] & 0x3e) >> 2);
            } else {
                strDest[i_cur_output] = '\0';
                return 0;
            }
        /* 3-byte UTF-8. */
        } else {
            if (i_cur_output + 3 < nDestLen) {
                strDest[i_cur_output++] = (char) ((szSrc[i] >> 12) | 0xe0);
                strDest[i_cur_output++] = (char) (((szSrc[i] >> 6) & 0x3f) |
                                                  0x80);
                strDest[i_cur_output++] = (char) ((szSrc[i] & 0x3f) | 0x80);
            } else {
                strDest[i_cur_output] = '\0';
                return 0;
            }
        }
    }

    strDest[i_cur_output] = '\0';

    return i_cur_output;
}

void str_prepend(char *dest, const char *src)
{
    char tmp[PATHNAME_SIZE];

    strcpy(tmp, dest);
    strcpy(dest, src);
    strcat(dest, tmp);
}

//#if defined(CASCFG_PLATFORM_LINUX) || defined(__ICCARM__)
char* strlcpy(char *dest, const char *src, size_t size)
{
    strncpy(dest, src, size);
    dest[size - 1] = '\0';
    return dest;
}

char* strlcat(char *dest, const char *src, size_t size)
{
    /* Only copy as much space as is left in the dest buffer. */
    size_t new_size = size - strlen(dest) - 1;
    /* NOTE that strncat always adds a NUL-terminator. */
    return strncat(dest, src, new_size);
}
//#endif

char* str_dup(const char *src)
{
    size_t s = strlen(src) + 1;
    char *newstr = osa_malloc(s);
    if (!newstr)
        return NULL;

    return (char*) osa_memcpy(newstr, src, s);
}

char* strn_dup(const char *src, size_t n)
{
    char *newstr;
    size_t s = strlen(src);
    if (n < s)
        s = n;

    newstr = osa_malloc(s + 1);
    if (!newstr)
        return NULL;

    newstr[s] = '\0';
    return (char*) osa_memcpy(newstr, src, s);
}

