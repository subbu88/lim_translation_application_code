#ifndef LIST_H
#define LIST_H


/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    list.h
 * @brief   Contains singly-linked list function prototypes for libcommon
 */

#include "osa_common.h"

/*!
 * Linked list data structure
 * Store a pointer to the next element and point to the data that is to be
 * dynamically allocated.
 */
struct _List {
    struct _List *next;
    void *data;
};

/*!
 * Linked list iterator type that can be used in generic container algorithms for
 * iteration.  Note: The list type only supports the forward-iterator, not
 * reverse.
 */
struct _ListIter {
    struct _List *node;
    bool end;
};

typedef struct _List List;
typedef struct _ListIter ListIter;

/*! Node comparison function. */
typedef bool (*list_findfn) (const void*, const void *);

/*!
 * @ingroup libcommon
 * @{
 */

/*!
 * @brief   Push an element into the front of a list.
 * @details Push a new data element onto a singly-linked list.
 *          Passing NULL for the list will create a new list.
 *          The list will contain the pointer to the original data; it is not
 *          copied.
 *
 * @param   list A valid list element or NULL
 * @param   data Pointer to a data object to store in the list
 *
 * @returns A pointer to the head of the list or NULL on failure
 */
List *list_push(List *list, void *data);

/*!
 * @brief   Pop an element off the front of a list.
 * @details Remove a data element from the front of a singly-linked list and
 *          return the contents.
 *          The calling function is responsible for freeing the data stored
 *          in the list.
 *
 * @param   list A valid list element
 * @param   data Pointer to a data object to hold the popped element
 *
 * @returns A pointer to the head of the list or NULL on failure
 */
List *list_pop(List *list, void **data);

/*!
 * @brief   Delete a list element.
 * @details Delete the list element 'node' from the list 'list'.
 *          The calling function is responsible for freeing the data stored
 *          in the list.
 *
 * @param   list A valid list element
 * @param   node A valid node to delete
 *
 * @returns A pointer to the head of the list or NULL on failure
 */
List *list_delete(List *list, List *node);

/*!
 * @brief   Get the size of a list.
 * @details Returns the number of elements in a list.
 *
 * @param   list A valid list element
 *
 * @returns Number of elements or zero if list is invalid
 */
size_t list_size(const List *list);

/*!
 * @brief   Find a node in a list.
 * @details Iterate over the list calling a compare function with each node
 *          and returning the found element.
 *
 * @param   list A valid list element
 * @param   comp_data Data to pass as the first argument to 'find' to compare
 *          with entry node data.
 * @param   find A function defined by the caller that accepts a list node
 *          and returns true if the element matches and false otherwise.  When
 *          the element is found, that element is returned.
 *
 * @returns A pointer to the element that matched the compare callback or NULL
 *          if nothing matches.
 */
List *list_find(List *list, const void *comp_data, list_findfn find);

/*!
 * @brief   Free the memory associated with a list.
 * @details Iterate over the list and free the list nodes.
 *          The calling function is responsible for freeing the data stored
 *          in the list.
 *
 * @param   list A valid list element
 */
void list_free(List **list);

/*!
 * Get the iterator for the first entry of a list.
 * This allows other functions to step through the entries or to
 * access the first entry in the list.
 *
 * @param   list Pointer to a list
 *
 * @returns iterator, NULL if the list is NULL
 */
ListIter list_iter_begin(List *list);

/*!
 * Get the iterator for the end of a list.
 * This is the point after the last entry in the list, where a new data
 * entry would be added.
 *
 * @param   list     pointer to a list
 *
 * @returns  iterator, NULL if the list is NULL
 */
ListIter list_iter_end(List *list);

/*!
 * Get the next iterator in a list.
 * This gives access to the next data entry in the list.
 *
 * @param   li    Current iterator
 *
 * @returns  iterator, NULL if the list is NULL
 */
ListIter list_iter_next(ListIter li);

/*!
 * Get the pointer to the data entry in a list indicated by the iterator.
 *
 * @param   li    Current iterator
 *
 * @returns  Internal node pointer of data member in a list.
 *           NULL indicates an error
 */
void *list_iter_node(const ListIter li);

/*!
 * Compare two iterators for equality.
 * They are equal if they point to the same data entry in the list.
 *
 * @param   a   Iterator a
 * @param   b   Iterator b
 *
 * @returns  true if two iterators are equal, else false
 */
bool list_iter_equal(const ListIter a, const ListIter b);

/*! @} */

#endif

