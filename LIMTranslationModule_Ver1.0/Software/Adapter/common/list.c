/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    list.c
 * @brief   Contains singly-linked list functions for Impresario libcommon.
 */

#include "osa_common.h"
#include "lib_common.h"

List *list_push(List *list, void *data)
{
    List *element;

    element = (List*) osa_calloc(1, sizeof(struct _List));

    if (!element)
        return NULL;

    osa_set_memtype(element, OSA_MEMTYPE_LIST);
    element->data = data;
    element->next = list;

    return element;
}

List *list_pop(List *list, void **data)
{
    if (list) {
        List *head = list->next;
        if (data)
            *data = list->data;

        osa_free(list);

        return head;

    } else {
        *data = NULL;
        return list;
    }
}

List *list_delete(List *list, List *node)
{
    List *save = NULL, *tmp;

    if (!list || !list)
        return NULL;

    if (list == node) {
        list = list->next;
        osa_free(node);
        return list;
    }

    for (tmp = list; tmp != NULL; save = tmp, tmp = save->next)
        if (tmp == node)
            break;

    /* If we didn't find the node in the list, just free the node. */
    if (!tmp) {
        osa_free(node);

        return NULL;
    }

    tmp = tmp->next;
    save->next = tmp;
    osa_free(node);


    return list;
}

size_t list_size(const List *list)
{
    int i;

    for (i = 0; list != NULL; list = list->next)
        i++;

    return i;
}

List *list_find(List *list, const void *comp_data, list_findfn find)
{
    List *node;

    if (!list)
        return NULL;

    for (node = list; node != NULL; node = node->next) {
        if (find(comp_data, node->data))
            return node;
    }

    return NULL;
}

void list_free(List **list)
{
    List *tmp, *next;

    for (tmp = *list; tmp != NULL; tmp = next) {
        next = tmp->next;
        osa_free(tmp);
    }

    *list = NULL;
}

ListIter list_iter_begin(List *list)
{
    ListIter li;
    li.node = list;
    li.end = false;
    return li;
}

ListIter list_iter_end(List *list)
{
    ListIter li;
    li.node = NULL;
    li.end = true;
    return li;
}

ListIter list_iter_next(ListIter li)
{
    li.node = li.node->next;
    return li;
}

ListIter list_iter_prev(ListIter li)
{
    /* XXX: not supported for this type! */
    /* Doesn't make sense for this type.  If this is the kind of iteration you
     * want to do, use a double-linked list or a vector. */
    return li;
}

void *list_iter_node(const ListIter li)
{
    return li.node->data;
}

bool list_iter_equal(const ListIter a, const ListIter b)
{
    /* Super hacky but done to make list iteration 'end' checking easy.  If
     * either iterator has been designated as an 'end' iterator, check the
     * other one to see if we actually are at the end of the list.  If neither
     * are 'end' iterators, just compare nodes directly. */
    if (a.end)
        return (!b.node);
    else if (b.end)
        return (!a.node);
    else
        return (a.node == b.node);
}

