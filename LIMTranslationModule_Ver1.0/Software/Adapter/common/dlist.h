#ifndef _DLIST_H_
#define _DLIST_H_

/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    dlist.h
 * @brief   Implement doubly link list
 */

#include "osa_common.h"

/*
 * STRUCTURES AND OTHER TYPEDEFS
 */

/*!
 * doubly link list element
 */
typedef struct list_element {
    struct list_element *next;
    struct list_element *prev;
} DList;


/*
 * CONSTANTS, DEFINES AND MACROS
 */

/*!
 * Get the sturcture entry for this element
 */
#define DLIST_ENTRY(element, type, member) \
    ((type *)((char *)(element)-(unsigned long)(&((type *)0)->member)))

/*!
 * Iterate over the doubly link list
 */
#define DLIST_LOOP(pos, n, head) \
    for (pos = (head)->next, n = (pos)->next; \
         pos != head; \
         pos = n, n = pos->next)

/*!
 * @{
 * @name LIST FUNCTION PROTOTYPES
 */

/*!
 * @ingroup libcommon
 *
 * @brief    Initialize the double-linked list head.
 * This sets the pointers for the first element to point to itself.
 *
 * @param    head Pointer to list head
 */
void dlist_init_head(DList *head);

/*!
 * @ingroup libcommon
 *
 * @brief    Initialize the double-linked list element.
 * This sets the list element pointers to 0.
 *
 * @param    element Pointer to list element
 */
void dlist_init_element(DList *element);

/*!
 * @ingroup libcommon
 *
 * @brief    Insert a new list element after list head.
 * The new element must already be allocated.  Pointers are set.
 *
 * @param    new_ Pointer to the new list element
 * @param    head Pointer to list head
 */
void dlist_insert(DList *new_, DList *head);

/*!
 * @ingroup libcommon
 *
 * @brief    Insert a new list element to list tail.
 * The new element must already be allocated.  Pointers are set.
 *
 * @param    new_ Pointer to the new list element
 * @param    head Pointer to list head
 */
void dlist_insert_tail(DList *new_, DList *head);

/*!
 * @ingroup libcommon
 *
 * @brief    Delete the list element.
 * List pointers are adjusted and the element is removed from the list.
 * The calling function is responsible for freeing the memory.
 *
 * @param    element Pointer to list element
 */
void dlist_delete(DList *element);

/*!
 * @ingroup libcommon
 *
 * @brief   Check if the list is empty.
 *
 * @param   head Pointer to list head
 *
 * @retval  true  The list is empty
 * @retval  false The list isn't empty
 */
bool dlist_is_empty(DList *head);

/*! @} */

#endif

