/*
 * $copyright$
 *
 * $license$
 *
 */

#include <string.h>
#include <stdarg.h>
#include <errno.h>

#include "osa_common.h"
#include "lib_common.h"

#define COMMON_ENTER LOG_ENTER(LOGMDL_COMMON)
#define COMMON_EXIT LOG_EXIT(LOGMDL_COMMON)

/* Maximum string sizes */
#define LOG_MAX_TXT_LINE_SIZE 128
#define LOG_MAX_FULL_LINE_SIZE 256

#ifdef LOG_ENABLED
/* Maximum number of timestamps to support */
#define NUM_TIMESTAMPS 1

/* Maximum description length for timestamp */
#define MAX_DESC_LEN   40

/*!
 * A structure to hold the string names and ids of the modules.
 */
typedef struct name_id_link {
    uint32_t id;
    char name[LOG_NAME_LENGTH + 1];
} name_id_link_t;

/*!
 * Structure used to store timestamp data.
 */
typedef struct {
    OsaTimeval tval;
    char desc[MAX_DESC_LEN];
} sTimestamp;

/* Local static variables */
static uint32_t ActiveModules;
static uint8_t  ActiveLevel;
static uint8_t  ActiveOutput;
static sTimestamp tsArray[NUM_TIMESTAMPS];
static uint8_t tsCount = 0;

/* Array to contain the id and text name of all modules */
static name_id_link_t module_names[LOGMDL_MAX_MODULES];

/* Contains the id and text string for all errors. */
static Vector *error_names;
#endif

/* Contains the text for an error name, temporarily. */
static char error_text[LOG_NAME_LENGTH + 1];

/* ------------------ Local Functions ----------------- */

#ifdef LOG_ENABLED
static uint32_t mscalc(OsaTimeval time);

/*!
 * @brief  Returns a name for a module code.
 * This is intended to be used with the lprintf module specified, not the
 * currently active modules, which can combine more than one.
 *
 * @param[in]  module: id of a module
 */
static char* module_name(uint32_t module)
{
    /* Printing enter and exit for this function is too messy. */
    int i;

    /* Look for this module in the table. */
    for (i = 0; i < LOGMDL_MAX_MODULES; i++) {
        if (module_names[i].id == module)
            return module_names[i].name;
    }

    return "UNKNOWN MODULE";
}

/*!
 * @brief  Returns a simple name for a level code.
 * This is intended to be used with the lprintf level specified, not the
 * currently active levels, which can combine more than one.
 *
 * @param[in]  level: id for a level
 */
static const char* level_name(uint8_t level)
{
    /* Printing enter and exit for this function is too messy. */
    switch (level) {
        case LOGLVL_CATASTROPHIC:
            return "CATASTROPHIC";
        case LOGLVL_ERROR:
            return "ERROR";
        case LOGLVL_WARNING:
            return "WARNING";
        case LOGLVL_INFO:
            return "INFO";
        case LOGLVL_DEBUG:
            return "DEBUG";
        case LOGLVL_FUNCTION_INFO:
            return "FUNCTION";
        default:
            return "UNKNOWN LEVEL";
    }
}
#endif

/*!
 *  @brief  Local function that handles printing of the message to the proper
 *          location.
 *          This function also handles pre-pending the time.
 *
 * @param[in]  message: the text of the message.
 */
void print_message(char* message)
{
#ifdef LOG_ENABLED
    /* Printing enter and exit for this function is too messy. */

    FILE *file = NULL;
    char filename[LOG_MAX_TXT_LINE_SIZE + 1];
    uint32_t hour = 0;
    uint32_t minute = 0;
    uint32_t second = 0;
    uint32_t millisecond = 0;
    bool error = FALSE;
    OsaTimeval ltime;
    char logoutput[LOG_MAX_FULL_LINE_SIZE + 1];

    ltime.tv_sec = 0;
    ltime.tv_usec = 0;

    osa_time_get(&ltime);
    if (&ltime.tv_usec == 0) {
        if (ActiveLevel >= LOGLVL_ERROR) {
            printf("LOG ERROR: Can't get current time.\n");
        }
    }

    hour = (ltime.tv_sec / 3600) % 24;
    minute = (ltime.tv_sec % (60 * 60)) / 60;
    second = (ltime.tv_sec % (60 * 60)) % 60;
    millisecond = ltime.tv_usec /1000;

    if (ActiveOutput & LOGPUT_FILE) {
        strncpy(filename, LOGFILE_PATH, LOG_MAX_TXT_LINE_SIZE);
        strncat(filename, LOG_FILE,
                        LOG_MAX_TXT_LINE_SIZE - strlen(LOGFILE_PATH));
        filename[LOG_MAX_TXT_LINE_SIZE] = '\0';
        
        /* Check if the log file exist */
        error = file_exists(filename);
        if (!error) {
            printf("LOG WARNING: Log file doesn't exist, creating a new one\n");
            /* Creating the a new empty log file. */
            file = fopen(filename, "w");
            if (file) {
                fclose(file);
                error = TRUE;
            }
            else{
                printf("LOG WARNING: Could not create a new log file.\n"
                        "Storage device is not yet installed or device is not"
                        " connected.\n");
                error = FALSE;
            }
        }
        if (error){
            /* Open the file in append mode */
            file = fopen(filename, "a");
            if (NULL == file) {
                if (ActiveLevel >= LOGLVL_WARNING) {
                    printf("LOG WARNING:  Cannot open output file %s.  >%s\n",
                            filename, message);
                }
            }
        }
    }

    /* Add time to beginning of message */
    snprintf(logoutput, LOG_MAX_FULL_LINE_SIZE, "%02d:%02d:%02d: (%d ms) %s",
            hour, minute, second, millisecond, message);
    logoutput[LOG_MAX_FULL_LINE_SIZE] = '\0';


    /* print to console */
    if (ActiveOutput & LOGPUT_CONSOLE) {
        printf(logoutput);
    }

    /* print to file */
    if (ActiveOutput & LOGPUT_FILE && file) {
        fprintf(file, logoutput);
        fflush(file);
        fclose(file);
    }
#endif
}

/* ------------------ External Functions ----------------- */

void lprintf(const uint32_t module, const uint8_t level, const uint32_t error,
             const char* message, ...)
{
    /* Printing enter and exit for this function is too messy. */
#ifdef LOG_ENABLED
    va_list args;
    char buffer[LOG_MAX_TXT_LINE_SIZE + 1];
    char logoutput[LOG_MAX_FULL_LINE_SIZE + 1];

    if ((level & (LOGLVL_DEFAULTLEVEL)) ||
        ((module & ActiveModules) && (level & ActiveLevel) ))
    {
        va_start(args, message);
        vsnprintf(buffer, sizeof(buffer), message, args);
        va_end(args);
        buffer[LOG_MAX_TXT_LINE_SIZE] = '\0';

        if (level < LOGLVL_INFO)
        {
            snprintf(logoutput, LOG_MAX_FULL_LINE_SIZE, "%s %s 0x%016x %s %s",
                        module_name(module), level_name(level), error,
                        error_name(error), buffer);
            logoutput[LOG_MAX_FULL_LINE_SIZE] = '\0';

        } else {
            snprintf(logoutput, LOG_MAX_FULL_LINE_SIZE, "%s %s %s",
                    module_name(module), level_name(level), buffer);
            logoutput[LOG_MAX_FULL_LINE_SIZE] = '\0';
        }

        print_message(logoutput);
    }
#endif
}

void dlprintf(const uint32_t module, const char* message, ...)
{
    /* Printing enter and exit for this function is too messy. */
#ifdef LOG_ENABLED
    va_list args;
    char buffer[LOG_MAX_TXT_LINE_SIZE + 1];
    char logoutput[LOG_MAX_FULL_LINE_SIZE + 1];

    if ((LOGLVL_DEBUG & (LOGLVL_DEFAULTLEVEL)) ||
        ((module & ActiveModules) && (LOGLVL_DEBUG & ActiveLevel) ))
    {
        va_start(args, message);
        vsnprintf(buffer, sizeof(buffer), message, args);
        va_end(args);
        buffer[LOG_MAX_TXT_LINE_SIZE] = '\0';

        snprintf(logoutput, LOG_MAX_FULL_LINE_SIZE, "%s %s %s",
                module_name(module), "DEBUG", buffer);
        logoutput[LOG_MAX_FULL_LINE_SIZE] = '\0';

        print_message(logoutput);
    }
#endif
}

void ilprintf(const uint32_t module, const char* message, ...)
{
    /* Printing enter and exit for this function is too messy. */
#ifdef LOG_ENABLED
    va_list args;
    char buffer[LOG_MAX_TXT_LINE_SIZE + 1];
    char logoutput[LOG_MAX_FULL_LINE_SIZE + 1];

    if ((LOGLVL_INFO & (LOGLVL_DEFAULTLEVEL)) ||
        ((module & ActiveModules) && (LOGLVL_INFO & ActiveLevel) ))
    {
        va_start(args, message);
        vsnprintf(buffer, sizeof(buffer), message, args);
        va_end(args);
        buffer[LOG_MAX_TXT_LINE_SIZE] = '\0';

        snprintf(logoutput, LOG_MAX_FULL_LINE_SIZE, "%s %s %s",
                module_name(module), "INFO", buffer);
        logoutput[LOG_MAX_FULL_LINE_SIZE] = '\0';

        print_message(logoutput);
    }
#endif
}

void set_debug_module(const uint32_t module)
{
#ifdef LOG_ENABLED
    COMMON_ENTER;

    ActiveModules = ActiveModules | module;
    dlprintf(LOGMDL_COMMON, "Log modules now set to 0x%x\n", ActiveModules);

    COMMON_EXIT;
#endif
}

void clear_debug_module(const uint32_t module)
{
#ifdef LOG_ENABLED
    COMMON_ENTER;

    ActiveModules = ActiveModules & ~module;
    dlprintf(LOGMDL_COMMON, "Clear log modules. Now set to 0x%x\n",
             ActiveModules);

    COMMON_EXIT;
#endif
}

void set_debug_level(const uint8_t level)
{
#ifdef LOG_ENABLED
    COMMON_ENTER;

    ActiveLevel = ActiveLevel | level;
    dlprintf(LOGMDL_COMMON, "Log level now set to 0x%x\n", ActiveLevel);

    COMMON_EXIT;
#endif
}

void clear_debug_level(const uint8_t level)
{
#ifdef LOG_ENABLED
    COMMON_ENTER;

    ActiveLevel = ActiveLevel & ~level;
    dlprintf(LOGMDL_COMMON, "Clear log level. Now set to 0x%x\n", ActiveLevel);

    COMMON_EXIT;
#endif
}

void set_debug_out(const uint8_t output)
{
#ifdef LOG_ENABLED
    COMMON_ENTER;

    ActiveOutput = ActiveOutput | output;
    dlprintf(LOGMDL_COMMON, "Log output now set to 0x%x\n", ActiveOutput);

    COMMON_EXIT;
#endif
}

void clear_debug_out(const uint8_t output)
{
#ifdef LOG_ENABLED
    COMMON_ENTER;

    ActiveOutput = ActiveOutput & ~output;
    dlprintf(LOGMDL_COMMON, "Clear log output. Now set to 0x%x\n",
                    ActiveOutput);

    COMMON_EXIT;
#endif
}

uint32_t add_module_name(const uint32_t module, const char* name)
{
#ifdef LOG_ENABLED
    int i;

    COMMON_ENTER;

    /* This function will not overwrite a module that is already in the table.
     * Check for it. */
    for (i = 0; i < LOGMDL_MAX_MODULES; i++) {
        if ((module_names[i].id == module) && (module_names[i].name != NULL)) {
            lprintf(LOGMDL_COMMON, LOGLVL_ERROR, ERRCODE_PARAMETER_EXISTS,
                    "ERROR: module %x already exists with name %s.\n",
                    module, module_names[i].name);

            COMMON_EXIT;
            return ERRCODE_PARAMETER_EXISTS;
        }
    }

    /* Find an empty spot for this module. */
    for (i = 0; i < LOGMDL_MAX_MODULES; i++) {
        if (0 == module_names[i].id) {
            module_names[i].id = module;
            strncpy(module_names[i].name, name, LOG_NAME_LENGTH);
            dlprintf(LOGMDL_COMMON, "   [%d] = %x  %s\n",
                    i, module_names[i].id, module_names[i].name);

            COMMON_EXIT;
            return ERRCODE_NO_ERROR;
        }
    }

    if (LOGMDL_MAX_MODULES == i) {
        lprintf(LOGMDL_COMMON, LOGLVL_ERROR, ERRCODE_SYSTEM_LIMIT,
               "ERROR: All module spaces are filled. %d modules are allowed.\n",
                LOGMDL_MAX_MODULES);

        COMMON_EXIT;
        return ERRCODE_SYSTEM_LIMIT;
    }

    COMMON_EXIT;
#endif

    return ERRCODE_NO_ERROR;
}

uint32_t remove_module_name(const uint32_t module)
{
#ifdef LOG_ENABLED
    int i;

    COMMON_ENTER;

    /* This function will not overwrite a module that is already in the table.
     * Check for it. */
    for (i = 0; i < LOGMDL_MAX_MODULES; i++) {
        if ((module_names[i].id == module) && (module_names[i].name != NULL)) {
            dlprintf(LOGMDL_COMMON, "   Removed [%d] = %x  %s\n",
                    i, module_names[i].id, module_names[i].name);
            module_names[i].id = 0;
            module_names[i].name[0] = '\0';

            COMMON_EXIT;
            return ERRCODE_NO_ERROR;
        }
    }

    if (LOGMDL_MAX_MODULES == i) {
        lprintf(LOGMDL_COMMON, LOGLVL_WARNING, ERRCODE_NOT_FOUND,
                "WARNING: Module %x was not found.\n", module);

        COMMON_EXIT;
        return ERRCODE_NOT_FOUND;
    }


    COMMON_EXIT;
#endif

    return ERRCODE_NO_ERROR;
}


uint32_t add_error_name(const uint32_t error_id, const char* text)
{
#ifdef LOG_ENABLED
    name_id_link_t error_info;

    COMMON_ENTER;

    error_info.id = error_id;
    strncpy(error_info.name, text, LOG_NAME_LENGTH);
    error_info.name[LOG_NAME_LENGTH - 1] = '\0';

    COMMON_EXIT;
    return vector_pushback(error_names, &error_info);
#else

    return ERRCODE_NO_ERROR;
#endif
}

/*!
 * @brief  Returns a simple name for an error code.
 *
 * @param[in]  error: id for an error
 */
char* error_name(const uint32_t error_id)
{
#ifdef LOG_ENABLED
    /* Printing enter and exit for this function is too messy. */
    VectorIter iter;
    name_id_link_t *node;
    void *n;

    container_foreach(vector, error_names, iter, n) {
        node = (name_id_link_t*) n;
        if (node->id == error_id)
            return node->name;
    }

    /* if not found, return the text of the id */
    snprintf(error_text, LOG_NAME_LENGTH, "0x%x", error_id);
    error_text[LOG_NAME_LENGTH - 1] = '\0';
#endif

    return error_text;
}

void log_dump_file()
{
    FILE *file = NULL;
    char filename[LOG_MAX_TXT_LINE_SIZE + 1];
    char chunk[4096];
    size_t sizeread;

    COMMON_ENTER;

    strncpy(filename, LOGFILE_PATH, LOG_MAX_TXT_LINE_SIZE);
    strncat(filename, LOG_FILE, LOG_MAX_TXT_LINE_SIZE - strlen(LOGFILE_PATH));
    filename[LOG_MAX_TXT_LINE_SIZE] = '\0';
    file = fopen(filename, "r");
    if (NULL == file) {
        printf("LOG ERROR:  Cannot open output file.  File may not exist. "
                "errno=0x%x\n", errno);
        return;
    }

    while (!feof(file)) {
        sizeread = fread((void*)chunk, 1, 4096, file);
        if (sizeread < 4096)
            chunk[sizeread] = '\0';
        printf("%s", chunk);
    }
    printf("\n");
    fclose(file);

    COMMON_EXIT;
}

void log_clear_file()
{
    FILE *file = NULL;
    char filename[LOG_MAX_TXT_LINE_SIZE + 1];

    COMMON_ENTER;

    strncpy(filename, LOGFILE_PATH, LOG_MAX_TXT_LINE_SIZE);
    strncat(filename, LOG_FILE, LOG_MAX_TXT_LINE_SIZE - strlen(LOGFILE_PATH));
    filename[LOG_MAX_TXT_LINE_SIZE] = '\0';
    file = fopen(filename, "w");
    fclose(file);

    COMMON_EXIT;
}

void get_debug_state()
{
#ifdef LOG_ENABLED
    printf("\n");
    printf("Active output bit field: 0x%x\n", ActiveOutput);
    printf("Active module bit field: 0x%x\n", ActiveModules);
    printf("Active level bit field: 0x%x\n", ActiveLevel);
    printf("Level always displayed bit field: 0x%x\n", LOGLVL_DEFAULT);
    printf("\n");
#else
    printf("\nLogging not enabled\n\n");
#endif
}

void init_logging()
{
#ifdef LOG_ENABLED
    int i;

    ActiveModules = LOGMDL_DEFAULT;
    ActiveOutput = LOGPUT_DEFAULT;

    COMMON_ENTER;

    /* This function will not overwrite a module that is already in the table.
     * Check for it. */
    for (i = 0; i < LOGMDL_MAX_MODULES; i++) {
        module_names[i].id = 0;
        module_names[i].name[0] = '\0';
    }

    /* Add the modules that must be there. */
    add_module_name(LOGMDL_COMMON, "COMMON");
    add_module_name(LOGMDL_OSA, "OSA");
    add_module_name(LOGMDL_APP, "APPLICATION");

    /* Create the vector that will hold this data. */
    error_names = vector_create(sizeof(name_id_link_t), 15);

    /* Fill it with the known error messages. */
    add_error_name(ERRCODE_NO_ERROR, "NO ERROR");
    add_error_name(ERRCODE_TRY_AGAIN, "TRY AGAIN");
    add_error_name(ERRCODE_INVALID_ARGUMENT, "INVALID ARGUMENT");
    add_error_name(ERRCODE_OUT_OF_MEMORY, "OUT OF MEMORY");
    add_error_name(ERRCODE_OUT_OF_SPACE, "OUT OF SPACE");
    add_error_name(ERRCODE_GENERAL_ERROR, "GENERAL ERROR");
    add_error_name(ERRCODE_DEADLOCK, "DEADLOCK");
    add_error_name(ERRCODE_NOT_SUPPORTED, "NOT SUPPORTED");
    add_error_name(ERRCODE_BUSY, "BUSY");
    add_error_name(ERRCODE_PERMISSION_DENIED, "PERMISSION DENIED");
    add_error_name(ERRCODE_TIMED_OUT, "TIMED OUT");
    add_error_name(ERRCODE_SYSTEM_LIMIT, "SYSTEM LIMIT REACHED");
    add_error_name(ERRCODE_PARAMETER_EXISTS, "PARAMETER ALREADY EXISTS");
    add_error_name(ERRCODE_END_OF_DIRECTORY, "END OF DIRECTORY REACHED");
    add_error_name(ERRCODE_NOT_FOUND, "NOT FOUND");
    add_error_name(ERRCODE_INTERNAL, "INTERNAL ERROR");
    add_error_name(ERRCODE_OVERFLOW, "OVERFLOW");

    COMMON_EXIT;
#endif
}

/*!
 * @brief  Adds a timestamp to the timestamp array.
 *         this function is not thread safe.
 */
void addTimeStamp(char *desc)
{
#ifdef LOG_ENABLED
    OsaTimeval ltime;

    if (tsCount < NUM_TIMESTAMPS) {
        osa_time_get(&ltime);
        tsArray[tsCount].tval = ltime;
        strncpy(tsArray[tsCount].desc, desc, MAX_DESC_LEN);
        tsCount++;
    }
    else
        dlprintf(LOGMDL_COMMON,
                "All timestamps in use, use resetTSCount to start over\n");
#endif
}

/*!
 * @brief  Prints out all timestamps.
 */
void print_timestamps(void)
{
#ifdef LOG_ENABLED
    uint32_t t1 = 0, t2 = 0;
    int x;

    printf("TS \t Absolute Time(ms) \t Delta(ms) \t Description\n");
    printf("0 \t %i \t\t\t 0 \t\t %s\n", mscalc(tsArray[0].tval),
            tsArray[0].desc);

    for (x = 1; x < NUM_TIMESTAMPS; x++)
    {
        t1 = mscalc(tsArray[x-1].tval);
        t2 = mscalc(tsArray[x].tval);

        if (t2 != 0)
            printf("%i \t %i\t\t\t %i \t\t %s\n", x, t2, t2-t1,
                    tsArray[x].desc);
    }
#endif
}

/*!
 * @brief  Resets the number of timestamps taken.
 */
void resetTSCount(void)
{
#ifdef LOG_ENABLED
    memset(&tsArray, 0, sizeof(tsArray));
    tsCount = 0;
#endif
}

/*!
 * @brief  Calculates the number of ms from the OsaTimeval.
 */
#ifdef LOG_ENABLED
static uint32_t mscalc(OsaTimeval time)
{
    uint32_t ms;

    ms = time.tv_usec / 1000;

    return ms;
}
#endif

