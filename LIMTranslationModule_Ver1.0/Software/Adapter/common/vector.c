
/*
 * $copyright$
 *
 * $license$
 *
 */

/*!
 * @file    vector.c
 * @brief   vector data type implementation
 */

/*
 * INCLUDE FILES
 */

#include "osa_common.h"
#include "lib_common.h"

static inline void move_foreward(const Vector *vt, void *from, void *to);
static inline void move_backward(const Vector *vt, void *from, void *to);

/*
 * GLOBAL CONSTANTS
 */
#define VECTOR_SHRINK_AFTER_DATA_REMOVE 1
#define VECTOR_SIZE_MIN 5
#define VECTOR_EXPAND_STEP 5
#define VECTOR_EXPAND_FACT 1
/*
 * GLOBAL FUNCTIONS
 */
/*
 * create a vector with minimal space, the vector will allocate
 *          VECTOR_SIZE_MIN * data_sz memory as initial space.
 * size size of each data member in vector
 * cap_init initial capacity size of the created vector
 */
Vector * vector_create(const uint32_t data_sz, uint32_t cap_init)
{
    uint32_t cap = cap_init;
    Vector *vt = (Vector*)osa_malloc(sizeof(Vector));

    if (NULL == vt)
        return NULL;

    osa_set_memtype(vt, OSA_MEMTYPE_VECTOR);

    if (0 == cap)
        cap = VECTOR_SIZE_MIN;

    vt->v_pdata = (void*)osa_malloc(cap * data_sz);

    if (NULL == vt->v_pdata) {
        osa_free(vt);
        return NULL;
    }

    osa_set_memtype(vt->v_pdata, OSA_MEMTYPE_VECTOR);
    vt->data_sz = data_sz;
    vt->size = 0;
    vt->cap = cap;

    return vt;
}

/*
 * destroy a vector, the internal memory also freed.
 */
void vector_destroy(Vector *vt)
{
    if (NULL != vt) {
        osa_free(vt->v_pdata);
        vt->v_pdata = NULL;
        osa_free(vt);
    }
}

/*
 * get the capacity of a vector
 */
uint32_t vector_capacity(const Vector *vt)
{
    if (NULL != vt)
        return vt->cap;
    else
        return 0;
}

/*
 * get the number of data members save in a vector
 */
uint32_t vector_size(const Vector *vt)
{
    if (NULL == vt)
        return 0;
    else
        return vt->size;
}

/*
 * check if a vector has saved any data
 */
bool vector_is_empty(const Vector *vt)
{
    if (NULL != vt)
        return (0 == vt->size);
    else
        return true;
}

/*
 * clear all data in a vector, set its capacity to default minimal.
 * set size to 0
 */
int vector_clear(Vector *vt)
{
    if (NULL == vt)
        return ERRCODE_INVALID_ARGUMENT;

#if 0
#ifdef VECTOR_SHRINK_AFTER_DATA_REMOVE
    if (vt->cap > VECTOR_SIZE_MIN) {
        void *p_save = vt->v_pdata;
        uint32_t cap_save = vt->cap;
        vt->cap = VECTOR_SIZE_MIN;
        vt->v_pdata = osa_realloc(vt->v_pdata, vt->cap * vt->data_sz);

        if (NULL == vt->v_pdata) {
            vt->v_pdata = p_save;
            vt->cap = cap_save;
            vt->size = 0;
            return ERRCODE_OUT_OF_MEMORY;
        }
        osa_set_memtype(vt->v_pdata, OSA_MEMTYPE_VECTOR);
    }
#endif
#endif
    vt->size = 0;
    return ERRCODE_NO_ERROR;
}

/*
 * reserve data member space for a vector.
 */
int vector_reserve(Vector *vt, const uint32_t size)
{
    if (NULL == vt)
        return ERRCODE_INVALID_ARGUMENT;

    if (size > vt->cap) {
        void *p_save = vt->v_pdata;
        uint32_t cap_save = vt->cap;
        vt->cap = size;
        vt->v_pdata = osa_realloc(vt->v_pdata, vt->cap * vt->data_sz);

        if (NULL == vt->v_pdata) {
            vt->v_pdata = p_save;
            vt->cap = cap_save;
            return ERRCODE_OUT_OF_MEMORY;
        }
        osa_set_memtype(vt->v_pdata, OSA_MEMTYPE_VECTOR);
    }
    return ERRCODE_NO_ERROR;
}

/*
 * push data to the end of a vector, if no space, vector will expand.
 */
int vector_pushback(Vector *vt, const void *data)
{
    if (NULL == vt || NULL == data)
        return ERRCODE_INVALID_ARGUMENT;

    if (vt->size >= vt->cap) {
        void *p_save = vt->v_pdata;
        uint32_t cap_save = vt->cap;
        vt->cap =
                vt->cap * VECTOR_EXPAND_FACT + VECTOR_EXPAND_STEP;
        vt->v_pdata = osa_realloc(vt->v_pdata, vt->cap * vt->data_sz);

        if (NULL == vt->v_pdata) {
            vt->v_pdata = p_save;
            vt->cap = cap_save;
            return ERRCODE_OUT_OF_MEMORY;
        }
        osa_set_memtype(vt->v_pdata, OSA_MEMTYPE_VECTOR);
    }

    osa_memcpy(((char*) vt->v_pdata + vt->size * vt->data_sz), data,
	           vt->data_sz);
    vt->size++;

    return ERRCODE_NO_ERROR;
}

/*
 * pop data from the end of vector
 */
int vector_popback(Vector *vt, void *data)
{
    if (NULL == vt)
        return ERRCODE_INVALID_ARGUMENT;

    if (vt->size <= 0)
        return ERRCODE_GENERAL_ERROR;

    vt->size--;
    if (NULL != data)
        osa_memcpy(data, ((char*) vt->v_pdata + vt->size * vt->data_sz),
	               vt->data_sz);

#ifdef VECTOR_SHRINK_AFTER_DATA_REMOVE
    if ((vt->cap >= (VECTOR_SIZE_MIN * VECTOR_EXPAND_FACT +
                     VECTOR_EXPAND_STEP))
        &&
        (vt->size <= (vt->cap - VECTOR_EXPAND_STEP) /
                        VECTOR_EXPAND_FACT)) {
        void *p_save = vt->v_pdata;
        uint32_t cap_save = vt->cap;
        vt->cap = (vt->cap - VECTOR_EXPAND_STEP) /
                    VECTOR_EXPAND_FACT;
        vt->v_pdata = osa_realloc(vt->v_pdata, vt->cap * vt->data_sz);

        if (NULL == vt->v_pdata) {
            vt->v_pdata = p_save;
            vt->cap = cap_save;
            return ERRCODE_OUT_OF_MEMORY;
        }
        osa_set_memtype(vt->v_pdata, OSA_MEMTYPE_VECTOR);
    }
#endif

    return ERRCODE_NO_ERROR;
}

/*
 * get data from vector by data member index
 */
int vector_at(const Vector *vt, const uint32_t index, void *data)
{
    if( NULL == vt || NULL == data)
        return ERRCODE_INVALID_ARGUMENT;

    if (index >= vt->size)
        return ERRCODE_GENERAL_ERROR;

    osa_memcpy(data, ((char*) vt->v_pdata + index * vt->data_sz), vt->data_sz);
    return ERRCODE_NO_ERROR;
}

/*
 * assign data to a vector by index
 */
int vector_assign(const Vector *vt, const uint32_t index, const void *data)
{
    if( NULL == vt || NULL == data)
        return ERRCODE_INVALID_ARGUMENT;

    if (index >= vt->size)
        return ERRCODE_GENERAL_ERROR;

    osa_memcpy(((char*) vt->v_pdata + index * vt->data_sz), data, vt->data_sz);
    return ERRCODE_NO_ERROR;
}

/*
 * insert data to a vector by index
 */
int vector_insert_at(Vector *vt, const uint32_t index, const void *data)
{
    void* from;
    if (NULL == vt || NULL == data || index > vt->size)
        return ERRCODE_INVALID_ARGUMENT;

    if (index == vt->size)
        return vector_pushback(vt,data);

    void *p_save = vt->v_pdata;
    uint32_t cap_save = vt->cap;

    if (vt->size >= vt->cap) {
        vt->cap =
                vt->cap * VECTOR_EXPAND_FACT + VECTOR_EXPAND_STEP;
        vt->v_pdata = osa_realloc(vt->v_pdata, vt->cap * vt->data_sz);
        if (NULL == vt->v_pdata) {
            vt->v_pdata = p_save;
            vt->cap = cap_save;
            return ERRCODE_OUT_OF_MEMORY;
        }
        osa_set_memtype(vt->v_pdata, OSA_MEMTYPE_VECTOR);
    }
    from = (char*) vt->v_pdata + vt->data_sz * index;
    move_foreward(vt, from,
		          ((char*) vt->v_pdata + (vt->size - 1) * vt->data_sz));
    osa_memcpy(from, data, vt->data_sz);
    vt->size++;

    return ERRCODE_NO_ERROR;
}

/*
 * remove a data from a vector by index
 */
int vector_remove_at(Vector *vt, const uint32_t index)
{
    if( NULL == vt || index >= vt->size)
        return ERRCODE_INVALID_ARGUMENT;

    void* from = (char*) vt->v_pdata + vt->data_sz * index;
    void* end = (char*) vt->v_pdata + vt->data_sz * (vt->size - 1);

    move_backward(vt, from, end);
    vt->size--;

#ifdef VECTOR_SHRINK_AFTER_DATA_REMOVE
    if ((vt->cap >= (VECTOR_SIZE_MIN * VECTOR_EXPAND_FACT +
                        VECTOR_EXPAND_STEP))
        &&
        (vt->size <= (vt->cap - VECTOR_EXPAND_STEP) /
                        VECTOR_EXPAND_FACT)) {
        void *p_save = vt->v_pdata;
        uint32_t cap_save = vt->cap;
        vt->cap = (vt->cap - VECTOR_EXPAND_STEP) /
                    VECTOR_EXPAND_FACT;
        vt->v_pdata = osa_realloc(vt->v_pdata, vt->cap * vt->data_sz);

        if (NULL == vt->v_pdata) {
            vt->v_pdata = p_save;
            vt->cap = cap_save;
            return ERRCODE_OUT_OF_MEMORY;
        }
        osa_set_memtype(vt->v_pdata, OSA_MEMTYPE_VECTOR);
    }
#endif

    return ERRCODE_NO_ERROR;
}

/*
 * swap data member in a vector
 */
int vector_swap(const Vector *vt, const uint32_t a, const uint32_t b)
{
    if (NULL == vt || a >= vt->size || b >= vt->size)
        return ERRCODE_INVALID_ARGUMENT;

    uint32_t leng = vt->data_sz;
    void * temp = (void *)osa_malloc(leng);
    if (NULL == temp)
        return ERRCODE_OUT_OF_MEMORY;

    osa_set_memtype(temp, OSA_MEMTYPE_VECTOR);
    void* iter_a = (char*) vt->v_pdata + leng * a;
    void* iter_b = (char*) vt->v_pdata + leng * b;
    osa_memcpy(temp, iter_a, leng);
    osa_memcpy(iter_a, iter_b, leng);
    osa_memcpy(iter_b, temp, leng);
    osa_free(temp);

    return ERRCODE_NO_ERROR;
}

/*
 * get the beginning iterator of a vector
 */
VectorIter vector_iter_begin(const Vector *vt)
{
    VectorIter iter = {NULL, 0};
    if (NULL != vt) {
        iter.node = vt->v_pdata;
        iter.data_sz = vt->data_sz;
    }
    return iter;
}

/*
 * get the end of iterator of a vector, the end iterator pointers to
 * next place of last data member.
 */
VectorIter vector_iter_end(const Vector *vt)
{
    VectorIter iter = {NULL, 0};
    if (NULL != vt) {
        iter.node = ((char*) vt->v_pdata + (vt->data_sz * vt->size));
        iter.data_sz = vt->data_sz;
    }
    return iter;
}

/*
 * get next iterator of a vector
 */
VectorIter vector_iter_next(VectorIter iter)
{
    iter.node = (char*) iter.node + iter.data_sz;
    return iter;
}

/*
 * get previous iterator of a vector
 */
VectorIter vector_iter_prev(const Vector *vt, VectorIter iter)
{
    iter.node = (char*) iter.node - iter.data_sz;
    return iter;
}

/*
 * Compare two iterators for equality
 */
bool vector_iter_equal(const VectorIter a, const VectorIter b)
{
    return (a.node == b.node && a.data_sz == b.data_sz);
}
/*
 * get the index of a iterator
 */
int vector_iter_at(const Vector *vt, VectorIter iter, uint32_t *at)
{
    if (NULL == vt || NULL == at)
        return ERRCODE_INVALID_ARGUMENT;

    if ((vector_iter_begin(vt).node > iter.node) ||
            (vector_iter_end(vt).node <= iter.node)) {
        return ERRCODE_INVALID_ARGUMENT;
    }
    *at = (uint32_t)((char*) iter.node - (char*) vt->v_pdata) / vt->data_sz;
    return ERRCODE_NO_ERROR;
}

/*
 * get the node pointer of a vector iterator
 */
void * vector_iter_node(const VectorIter iter)
{
    return (void *)iter.node;
}

/*
 * get the data member of a iterator
 */
int vector_iter_value(const Vector *vt, VectorIter iter, void *data)
{
    if (NULL == vt || NULL == data)
        return ERRCODE_INVALID_ARGUMENT;

    if ((vector_iter_begin(vt).node > iter.node) ||
            (vector_iter_end(vt).node <= iter.node)) {
        return ERRCODE_INVALID_ARGUMENT;
    }
    osa_memcpy(data, iter.node, iter.data_sz);
    return ERRCODE_NO_ERROR;
}

/*
 * LOCAL FUNCTIONS
 */
/*
 * @brief   move a set of data foreward
 * @detail  move data from "from" to "to" for one step forward
 *
 * @param   from    start position need be moved
 * @param   to      end position need be moved
 *
 * @return none
 */
static inline void move_foreward(const Vector *vt, void *from, void *to)
{
    if (NULL == vt || NULL == from || NULL == to)
        return;
    uint32_t size = vt->data_sz;
    char *p;
    for (p = (char*) to; p >= (char*) from; p -= size)
        osa_memcpy(p + size, p, size);
}

/*
 * @brief   move a set of data backward
 * @detail  move data from "from" to "to" for one step backward
 *
 * @param   from    start position need be moved
 * @param   to      end position need be moved
 *
 * @return none
 */
static inline void move_backward(const Vector *vt, void *from, void *to)
{
    if (NULL == vt || NULL == from || NULL == to)
        return;
    uint32_t size = vt->data_sz;
    char *p;
    for (p = (char*) from; p <= (char*) to ; p += size)
        osa_memcpy(p, p + size, size);
}

