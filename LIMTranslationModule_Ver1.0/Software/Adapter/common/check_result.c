
/*
 * $copyright$
 *
 * $license$
 *
 */

 #include "osa_common.h"
 #include "lib_common.h"

void _check_result(const char *test_name, int ret, const char *fn)
{
    printf("Executing test %s from function %s...\n", test_name, fn);

    if (ret != ERRCODE_NO_ERROR) {
        printf("ERROR: failed test %s, error = %s\n", fn, error_name(ret));
        osa_time_delay(1000);
        osa_exit(1);
    }

    printf("Success!\n");
}

