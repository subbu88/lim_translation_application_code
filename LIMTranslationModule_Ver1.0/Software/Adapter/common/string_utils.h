
#ifndef _STRING_UTILS_H_
#define _STRING_UTILS_H_

/*
 * $copyright$
 *
 * $license$
 */

/*!
 * @file    string_utils.h
 * @brief   String Utilities
 */

#include "osa_common.h"
#include <ctype.h>

/*!  The text encodings supported by Impresario. */
typedef enum {
    TEXT_ENCODING_ISO_8859_1 = 0,  /**< @brief 8-bit ASCII and extended. */
    TEXT_ENCODING_UTF16,           /**< @brief Fixed 2-byte unicode. */
    TEXT_ENCODING_UTF16_BE,        /**< @brief Big Endian UTF-16. */
    TEXT_ENCODING_UTF8,            /**< @brief Variable-length unicode. */
    TEXT_ENCODING_INVALID          /**< @brief Unsupported encoding type. */
} TextEncoding;

/*! @{
 * @name Text Encoding Status
 * The return value from str_utf8_encode() shows the status of the text
 * encoding.  When the encoding fails, the destination buffer is filled with
 * an empty string.  When the encoding overflows the destination buffer, the
 * buffer contains a partial UTF-8 string.
 */
#define TEXT_ENCODING_STATUS_OK ERRCODE_NO_ERROR
#define TEXT_ENCODING_STATUS_FAILED ERRCODE_GENERAL_ERROR
#define TEXT_ENCODING_STATUS_OVERFLOW ERRCODE_OVERFLOW
/*! @} */

/*!  @{
 *   @name STRING UTILITY FUNCTION PROTOTYPES
 */


/*!
 * @ingroup libcommon
 * @brief   Skip the leading whitespace of a string.
 *
 * @param[in,out] str Pointer to a source string
 * @note    The str must be a char pointer or a pointer to a char pointer
 *          whitespace is defined by isspace()
 *          which are ' ', '\\t', '\\f', '\\n','\\v','\\r'
 */
#define STR_SKIP_WHITESPACE(str)   do {while(isspace((int)(*str))) (str)++;} \
                                   while(0)

/*!
 * Checks if a string leads with a specified token and move the string pointer.
 *
 * @details If the source string starts with the specified token, return true
 *          and move the source string pointer forward beyond the token. If the
 *          source string doesn't start with the token, return false and the
 *          source string pointer remains unchanged, allowing subsequent calls
 *          of this function to process the original string again.
 *
 * @param[in] ppStr Pointer to the source string pointer
 * @param[in] pKey  Pointer to the string token
 *
 * @returns true if source string is leading with the token, otherwise false
 */
bool str_match_token(char **ppStr, const char *pKey);

/*!
 * @brief   Extract a value string.
 * @details If string is quoted, return the quoted string
 *          else return the first whitespace-seperated token.
 *          Leading whitespace and '=' will be ignored.\n
 *          eg: =ABCD EFG       result: ABCD\n
 *              ="ABCD EFG"     result: ABCD EAFG\n
 *              ABCD EFG        result: ABCD\n
 *              "ABCD EFG"      result: ABCD EFG
 *
 * @param[out] pDest Pointer to location for the value
 * @param[in]  maxSz Max length to be extracted
 * @param[in]  ppSrc The pointer to source string pointer
 *
 * @returns  true if value string is extracted, otherwise false
 */
bool str_get_value_str(char *pDest, const int maxSz, char **ppSrc);

/*!
 * @brief   Get the string length of a unicode string.
 * @details Get the number of characters of a unicode string
 *          including the terminator character (0x0000).
 * @warning Avoid using wcslen(), as this function will not always work.
 *
 * @param[in] s   Unicode string pointer
 *
 * @returns Number of characters in the string.
 */
uint32_t unicode_strlen(const uint16_t *s);

/*!
 * Convert Unicode string to ASCII string.
 *
 * @param[out] dst Pointer to the converted ASCII string
 * @param[in]  src Pointer to the Unicode string
 * @param[in]  len Number of Unicode characters in string to be converted
 * @returns length of converted string
 */
uint16_t str_unicode2ascii(char *dst, const char *src, const uint16_t len);

/*!
 * @brief    Convert a string from any supported text encoding to UTF-8.
 * @details  Take an input string that is in any of the supported
 *           text encodings and convert it to a destination string in UTF-8.
 *
 * @param[in]  src      Input string
 * @param[in]  srclen   Number of bytes in the input string
 * @param[out] dest     Destination buffer (allocated by the caller)
 * @param[out] destlen  Number of bytes in the destination buffer
 * @param[in]  encoding The encoding of the input string.
 * @note following encoding mode supported:<BR>
 *  TEXT_ENCODING_ISO_8859_1<BR>
 *  TEXT_ENCODING_UTF16<BR>
 *  TEXT_ENCODING_UTF16_BE<BR>
 *  TEXT_ENCODING_UTF8
 *
 * @returns  the status of the operation
 * @retval   TEXT_ENCODING_STATUS_SUCCESS  Encoding succeeded
 * @retval   TEXT_ENCODING_STATUS_FAILED   Encoding failed.  dest will be empty
 *                                         string.
 * @retval   TEXT_ENCODING_STATUS_OVERFLOW Encoding overflowed the destination
 *                                         buffer.  The buffer will be filled
 *                                         as far as it can.
 */
int str_utf8_encode(const char *src, const int srclen, char *dest, int destlen,
                    const TextEncoding encoding);

/*!
 * @brief    Prepend a string in front of an existing string
 * @details  Take an input string and prepend a separate string in front of it.
 *
 * @param[in]  src      String to prepend
 * @param[out] dest     Original string.  The output will contain the
 *                      prepended string in front of the original.
 *
 * @note     It is the caller's responsibility to ensure that the buffer
 *           pointed to in 'dest' has enough room for strlen(src) +
 *           strlen(dest).
 */
void str_prepend(char *dest, const char *src);

//#if defined(CASCFG_PLATFORM_LINUX) || defined(__ICCARM__)
/*!
 * @brief    Copy a size-bounded string safely
 * @details  Copy an input string into an output buffer, respecting size
 *           limits.  The resulting string will always be NUL-terminated.
 *
 * @param[in]  src      String to copy
 * @param[out] dest     Buffer to copy to
 * @param[in]  size     Maximum size of output (dest) buffer
 *
 * @returns  A pointer to dest buffer
 */
char* strlcpy(char *dest, const char *src, size_t size);

/*!
 * @brief    Concatenate a size-bounded string safely
 * @details  Concatenate a source string onto a dest buffer that already
 *           contains a NUL-terminated string, respecting the maximum dest
 *           buffer size.  The resulting string will always be NUL-terminated.
 *
 * @param[in]  src      String to concatenate
 * @param[out] dest     String to concatenate on to
 * @param[in]  size     Maximum size of output (dest) buffer
 *
 * @returns  A pointer to dest buffer
 */
char* strlcat(char *dest, const char *src, size_t size);
//#endif

/*!
 * @ingroup libcommon
 * @brief   Allocate memory and copy a string
 * @details Copy a string to a newly-allocated memory block.
 * @param   src The source string
 *
 * @returns A pointer to a new string that contains a copy of 'src', or NULL
 *          on failure.  The new string can be passed to osa_free().
 */
char* str_dup(const char *src);

/*!
 * @ingroup libcommon
 * @brief   Allocate memory and copy a string
 * @details Copy a string to a newly-allocated memory block, limited to a
 *          certain number of characters.
 * @param   src The source string
 * @param   n   The maximum number of characters in 'src' to duplicate
 *
 * @returns A pointer to a new string that contains a copy of 'src', or NULL
 *          on failure.  The new string can be passed to osa_free().
 */
char* strn_dup(const char *src, size_t n);

/*! @} */
#endif

