
#ifndef LIB_COMMON_H
#define LIB_COMMON_H

/*
 * $copyright$
 *
 * $license$
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "container.h"
#include "list.h"
#include "dlist.h"
#include "vector.h"
#include "string_utils.h"
#include "file_utils.h"
#include "logging.h"
#include "error.h"
#include "check_result.h"
#include "general_utils.h"

#define OSA_MEMTYPE_LIST      (OSA_FWMEMTYPE_BASE + 5)
#define OSA_MEMTYPE_VECTOR    (OSA_FWMEMTYPE_BASE + 6)

/* Convert BCD and integer from 0-99 */
#define BCD_TO_INT(bcd)  ((((bcd >> 4) & 0x0f) * 10) + (bcd & 0x0f))
#define INT_TO_BCD(val)   (((val/10) << 4) | (val % 10))

#ifdef __cplusplus
}
#endif

#endif

