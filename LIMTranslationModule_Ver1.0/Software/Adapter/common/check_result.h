#ifndef CHECK_RESULT_H
#define CHECK_RESULT_H

/*
 * $copyright$
 *
 * $license$
 *
 */


/*!
 * @ingroup libcommon
 * @brief Prints banner "Executing test %s from function xxx"
 * and checks results.  Also checks and prints error if occurred using error_name().
 * @note Primarily used for testing.
 *
 * @param[in] tests_name String pointer to the name of the test to be output.
 * @param[in] ret       Return value.
 * @param[in] fn        Pointer to the calling function.  __func__ is expected
 *                      to be passed in.
 *
 */
void _check_result(const char *tests_name, int ret, const char *fn);

#endif
