
SOLIBS = libadaptor.so
$(SOLIBS):
	mkdir -p libs/$(PLATFORM)
	$(CC) -shared -Wl,-soname,$(SOLIBS) -o libs/$(PLATFORM)/$(SOLIBS) \
	-Wl,--whole-archive \
	$(APPLIB_COMMON) \
	$(APPLIB_OSA) \
	-Wl,--no-whole-archive

