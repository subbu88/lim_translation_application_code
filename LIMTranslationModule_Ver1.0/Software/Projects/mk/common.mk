include $(MFI_TOP)/Projects/mk/build.mk

# if this is a library release, set the release type.
# this prevents libraries from being deleted during
# a make clean
ifdef LIB_RELEASE
RELEASE_TYPE=lib
endif

# Default compiler being used.  Options are 'gcc' and 'iar'
COMPILER ?= gcc

# Default host being used.  Options are 'unix' and 'windows'
HOST ?= unix

# Pull in MFi user configuration so it can influence the default setup.
include $(MFI_TOP)/Source/MFi_Lib/mfi_cfg/mfi.conf    

# Set PLATFORM based on the config.  This can still be overwritten on the
# command-line if necessary.
PLATFORM ?= linux
EXEC_ROOT ?= /nfsroot
FLAGS += -DMFI_OVERRIDE_DEFAULT_CONFIGURATION
FLAGS += -DFSL_OS_SELECTED=FSL_OS_LINUX

BIN_EXTENSION =

SUBDIR_OBJECTS = $(foreach dir,$(SUBDIRS),\
		   $(shell find $(dir) -name \*.o | sort -d))

# Include paths
CONFIG_INC = $(MFI_TOP)/Source/MFi_Lib/mfi_cfg/include


# Setup other configs based on selected board
include $(MFI_TOP)/Projects/mk/boards.mk

# Set this to define if we want to build thumb binaries or ARM.
USE_THUMB = 1

include $(MFI_TOP)/Projects/mk/gcc.mk

