
# Ensure the 'all' target is always first so it's the default.
.PHONY: all
all: $(OBJECTS_LDS) $(OBJECTS) $(OBJECTS_ASM) $(SUBDIRS) $(LIBS) $(SOLIBS) $(APP)

$(OBJECTS): %.o: %.c
	$(CC) -c $(CFLAGS) $(INC) $< -o $@
	$(CC) -MM $(CFLAGS) $(INC) $*.c | \
	sed 's,\($(notdir $*)\.o\) *:,$(dir $*.d)\1 $*.d: ,' > $*.d.tmp
	mv $*.d.tmp $*.d

$(OBJECTS_ASM): %.o: %.S $(FILES_RELA_BINARY)
	$(CC) -c $(CFLAGS) -D__ASM__ $(INC) $< -o $@
	$(CC) -MM $(CFLAGS) -D__ASM__ $(INC) $*.S | \
	sed 's,\($(notdir $*)\.o\) *:,$(dir $*.d)\1 $*.d: ,' > $*.d.tmp
	mv $*.d.tmp $*.d

$(OBJECTS_LDS): $(OBJECTS_LDS).S $(FILES_RELA_LDS)
	$(CC) -s $(OBJECTS_LDS).S $(CFLAGS) -E -P -D__ASM__ $(INC) -o $(OBJECTS_LDS)
	cp -f $(OBJECTS_LDS) $(MQX_LD_SCRIPT)

BIN_START_ADDR = `grep " __boot" $@.map | \
		  sed 's/\s\s*/ /g' | \
		  cut -d ' ' -f2`

$(APP): $(OBJECTS) $(OBJECTS_ASM) $(LIBRARIES)
	@echo "Linking application $@ for Linux"
	$(CC) \
		--sysroot=$(LINUX_FAKEROOT) \
		-Wl,--start-group \
		$(OBJECTS) \
		$(OBJECTS_ASM) \
		$(LIBRARIES) \
		$(APP_LIBS) \
		-Wl,--end-group \
		-Wl,-rpath=/usr/local/lib \
		$(LDADD) $(LDINC) -o $@
	@echo ""
	@echo "=========================================================="
	@echo "Application '$@' built successfully!"
	@echo ""
	@echo "  Raw binary:     $@.bin"
	@echo ""
	@echo "=========================================================="
	@echo ""

