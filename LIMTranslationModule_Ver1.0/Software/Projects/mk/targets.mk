include $(MFI_TOP)/Projects/mk/targets_gcc.mk

.PHONY: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@

BIN_SIZE = `size $@.elf | tail -n1 | cut -f1 | tr -d ' '`

ifeq ($(PLATFORM), linux)
# Move the application or library into place in the NFS fakeroot for Linux
# usage.
.PHONY: local
local:: all
	@if [ -n "$(APP)" ]; then \
		echo "Copying $(APP) to $(LINUX_FAKEROOT)/usr/bin"; \
		sudo cp -f $(APP) $(LINUX_FAKEROOT)/usr/bin; \
	fi
	@if [ -n "$(SOLIBS)" ]; then \
		echo "Copying $(SOLIBS) to $(LINUX_FAKEROOT)/usr/local/lib"; \
		sudo cp -f libs/$(PLATFORM)/$(SOLIBS) $(LINUX_FAKEROOT)/usr/local/lib; \
	fi
endif

.PHONY: fresh
fresh: distclean all


# Set clean as a phony target and it can be appended to.  This is the default
# behavior.
.PHONY: clean
clean::
ifeq ($(RELEASE_TYPE), lib)
	rm -f *.o *.d *.a *.map *.bin *.elf \
	$(OBJECTS) $(OBJECTS_LDS) $(OBJECTS_ASM) $(APP) \
		$(patsubst %.o,%.d,$(OBJECTS)) \
		$(patsubst %.o,%.d,$(OBJECTS_ASM))
	@for _dir in $(SUBDIRS); do \
		$(MAKE) -C $$_dir clean; \
	done
else
	rm -f *.o *.d *.a *.map *.bin *.elf libs/$(PLATFORM)/*.a \
					    libs/$(PLATFORM)/*.so \
	$(OBJECTS) $(OBJECTS_LDS) $(OBJECTS_ASM) $(APP) \
		$(patsubst %.o,%.d,$(OBJECTS)) \
		$(patsubst %.o,%.d,$(OBJECTS_ASM))
	@for _dir in $(SUBDIRS); do \
		$(MAKE) -C $$_dir clean; \
	done
endif

# This target will attempt to wipe the state of the system to pristine.
# It will remove *all* object files found through the build tree, regardless
# of directory.  This won't remove .a files or .so since they are occasionally
# stored in git.
# -o  is 'or' operator
.PHONY: distclean
distclean:: clean
	find . -path "*/linux/fakeroot/*" -prune  \
	       -name '*.orig' -o -name '*.rej' -o -name '*~' -o \
	       -name '*.bak' -o -name '*.o' -o -name '*.d' -o \
	       -name '*.bin' -o -name '*.elf' -o -name '*.map' -o \
	       -name '*.lds' -o -name '*.icf' -type f | xargs rm -f


-include $(OBJECTS:.o=.d)
-include $(OBJECTS_ASM:.o=.d)

