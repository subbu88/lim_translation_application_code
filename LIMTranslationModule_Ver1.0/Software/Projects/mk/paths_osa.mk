# Vars to define top level libs
APPLIB_OSA = $(MFI_TOP)/Adapter/osa/libs/$(PLATFORM)/libosa.a
APPLIB_COMMON = $(MFI_TOP)/Adapter/common/libs/$(PLATFORM)/libcommon.a

# platform libs in os, each should build in platform specific dir
LIBRARIES += $(APPLIB_OSA) \
	     $(APPLIB_COMMON)

ADAPTOR_INC = -I$(MFI_TOP)/Adapter/osa \
	  -I$(MFI_TOP)/Adapter/osa/linux \
      -I$(MFI_TOP)/Adapter/common \
	  -I$(MFI_TOP)/Source/MFi_Lib/mfi_cfg       
      
INC += $(ADAPTOR_INC)
