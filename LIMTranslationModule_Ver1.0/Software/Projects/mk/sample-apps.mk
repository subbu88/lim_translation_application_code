# MAKEFILE_LIST will be apps/mk/.
MFI_TOP := $(dir $(lastword $(MAKEFILE_LIST)))../../

include $(MFI_TOP)/Projects/mk/common.mk
include $(MFI_TOP)/Projects/mk/paths_mfi.mk
include $(MFI_TOP)/Projects/mk/paths_osa.mk

# Report all warning as error, compiling will stop on any warning
ifeq ($(COMPILER), gcc)
CFLAGS += -Werror
endif
ifeq ($(COMPILER), iar)
CFLAGS += --warnings_are_errors
endif

