#setup environment when building linux
CC_PREFIX ?= /opt/arm-gnu
CC_VERSION ?= 4.7.3
CROSS_COMPILE ?= arm-none-linux-gnueabi-

CC_INCLUDE = $(CC_PREFIX)/lib/gcc/arm-none-linux-gnueabi/$(CC_VERSION)/include
CC_INCLUDE_FIXED = $(CC_PREFIX)/lib/gcc/arm-none-linux-gnueabi/$(CC_VERSION)/include-fixed
CC_LIB = $(CC_PREFIX)/lib/gcc/arm-none-linux-gnueabi/$(CC_PREFIX)/thumb2

##CC = $(CROSS_COMPILE)gcc
##LD = $(CROSS_COMPILE)ld
##AR = $(CROSS_COMPILE)ar
OBJCOPY = $(CROSS_COMPILE)objcopy

CFLAGS += $(FLAGS)
ASFLAGS += $(FLAGS)

# ARM EABI compiler already includes newlib libc.  Use this.
LIBC_LDPATH = $(CC_PREFIX)/$(CROSS_COMPILE_STRIP)/lib/$(CC_LIB_POST)
LIBC_CFLAGS = $(CC_PREFIX)/$(CROSS_COMPILE_STRIP)/include

# Generate debug information.
# Add '-O0' at the end of this line to turn off optimizations.  This can make
# debugging (especially asm) much easier but it greatly increases the size of
# the code and reduces performance.
#CDEBUG = -g -O0
CDEBUG = -g

# Turns on all -O2 except size increasers.
# Any CDEBUG settings will come after this and can be used to override.
CFLAGS += -O3
# Turn on all warnings.
CFLAGS += -Wall
# Add suppor for gnu99 compiler
CFLAGS += -std=gnu99

CFLAGS += -mno-unaligned-access

ifeq ($(PLATFORM), linux)
CFLAGS += -fPIC
endif

# Generate code specifically for ARMv7-A, cortex-a8/a5 chip.
ifeq ($(CPU), kinetis_p3)
	CFLAGS += -march=armv7e-m
	CFLAGS += -mtune=cortex-m4
	# Must use thumb with cortex-m4.
	USE_THUMB = 1
endif
ifeq ($(CPU), imx6)  
	CFLAGS += -march=armv7-a
	CFLAGS += -mtune=cortex-a7
endif

# Always use the aapcs-linux ABI.  This will work for EABI and GNU/Linux.
CFLAGS += -mabi=aapcs-linux

ifeq ($(USE_THUMB), 1)
	# Generate thumb2 instructions (mixed 16/32-bit).
	CFLAGS += -mthumb
	CFLAGS += -DUSE_THUMB
	ifeq ($(ARCH), faraday_m4)
		CC_LIB_POST = armv7e-m/softfp
	endif
	ifeq ($(ARCH), cortex-m)
		CC_LIB_POST = armv7e-m/softfp
	endif
	ifeq ($(ARCH), faraday_a5)
		# Allow mixed ARM and thumb code.  All C code will generate thumb
		# instructions  but there is hand-written asm that requires ARM.
		CFLAGS += -mthumb-interwork
		CC_LIB_POST = armv7-ar/thumb/softfp
	endif
else
	# Generate ARM-only code.
	CFLAGS += -marm
	CC_LIB_POST =
endif

ifeq ($(ARCH), $(filter $(ARCH), faraday_a5 cortex-a))
	# Use NEON SIMD instructions for floating point.  Alternatively can specify
	# VFP which gives IEEE 754-compliance (unlike NEON which can have errors).
	CFLAGS += -mfpu=neon
	# Specify these options with NEON.
	CFLAGS += -ftree-vectorize
	CFLAGS += -fno-math-errno
	CFLAGS += -funsafe-math-optimizations
	CFLAGS += -fno-signed-zeros
	ifeq ($(PLATFORM), linux)
		CFLAGS += -fno-short-enums
	endif
endif

ifeq ($(ARCH), faraday_m4)
	CFLAGS += -mfpu=fpv4-sp-d16
endif

ifeq ($(ARCH), cortex-m)
	CFLAGS += -mfpu=fpv4-sp-d16
endif

# Use soft floating point api with HW instructions.
##CFLAGS += -mfloat-abi=softfp

# Build with debug symbols enabled by default.  Uncomment to remove.
CFLAGS += $(CDEBUG)

INC += -I$(OSA_ROOT) \
       -I$(OSA_ROOT)/$(PLATFORM) \
       -I$(LIBCOMMON_ROOT)

INC += -I$(LINUX_FAKEROOT)/usr/include \
	   -I$(LINUX_FAKEROOT)/usr/local/include

LDINC += -lpthread -lrt -lasound -lusb-1.0 -ludev

LDADD += -L. \
	 -L$(LINUX_FAKEROOT)/lib \
	 -L$(LINUX_FAKEROOT)/usr/lib \
	 -L$(LINUX_FAKEROOT)/usr/local/lib

INC += -I$(BOARD_INC) \
	   -I$(CONFIG_INC)


ARFLAGS = crsv

