ifeq ($(MFICFG_BOARD_IMX6_UL), y)
CPU = imx6
ARCH = cortex-a
CORE = CA7F
IMX = i.MX6UL
endif
ifeq ($(MFICFG_BOARD_IMX6_SABRE_AUTO), y)
CPU = imx6
ARCH = cortex-a
CORE = CA9F
IMX = i.MX6Q
endif
