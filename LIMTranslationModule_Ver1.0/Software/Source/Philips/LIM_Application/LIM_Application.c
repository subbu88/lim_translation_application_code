/*HEADER******************************************************************************************

************************************************************************************************** 
*
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
*
**************************************************************************************************
*
* Comments:
*       File Name : test_suite.c 
*
**END********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "iAP2_Host.h"
#if MFI_IAP1_ENABLE == TRUE
#include "iAP_Host.h"
#endif
#include "iAP2_Power_Feat.h"
#include "DigitalAudioDriver.h"
#if defined(IAPI_INTERFACE_USB_HOST) && !defined(IAPI_INTERFACE_USB_ROLE_SWITCH)
#include "iAP_USBHost.h"
#endif
#include "miscFunctions.h"
#include "MFi_Authenticator.h"
#include <libusb-1.0/libusb.h>
#include <string.h>
#include "ProbeInitializer.h"
#include "crc32.h"
#include "Module_Controller.h" 
#include "GPIO_lib.h"

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
uint32_t g_DemoModeType = DEMO_TYPE_MFI;     /* DEMO_TYPE_SIM; 1=simulation mode, 0= real mfi tests */
uint32_t g_CurrentTestId = NO_ACTIVE_TEST;
uint32_t g_TestStatus = TEST_STATUS_IDLE; /* 0=idle, 1=busy*/
uint8_t g_authentication_done = 0;
unsigned long result = 0;
uint8_t gu8BackUpCharBuffer[80]; 

/*************************************************************************************************/
/*                                   Extern Variables Section                                    */
/*************************************************************************************************/
extern uint32_t g_InfoFlag;
extern sSM gTestSuite_SM;
extern uint8_t gu8iOSDeviceDetectedFlag;
extern uint8_t gu8OnGoingTest;
extern uint16_t gu16TrackCounterPrv;
extern int32_t g_retry;
extern unsigned char gu8StillWaitingForDevice;
extern const uint8_t gau8One;
extern uint16_t gu16TrackCounter;
extern uint8_t *gu8pMLName;
extern void (*const vptrfnTestSuiteStateMachine[])(void);
extern uint8_t gu8ActualIDCable;
extern uint8_t gu8CurrentTransport;
extern uint32_t gEaTestNo;
extern int32_t g_waittime;
extern uint8_t gSmartphoneHandshake;
extern uint8_t iAP2_Feat_NEA_bfnSendData(uint8_t *pBuff, uint16_t wSize);
extern OsaMutex Mutex_Handle; 

/* Transducer communication */
struct libusb_device_handle *glumify=NULL;
struct libusb_context *glumify_ctx = NULL;
static struct libusb_transfer *bulk_in0 = NULL;
static struct libusb_transfer *bulk_in1 = NULL;
static unsigned char databuf0[TRANSDUCER_BUF_SIZE] = {0};
static unsigned char databuf1[TRANSDUCER_BUF_SIZE] = {0};
static int done = 0;
static void bulkin_cb1(struct libusb_transfer *);
static OsaThread  scan_thread_id;
static int pkt_cnt = 0;
static unsigned long long int data_size = 0;

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define I2C_PCA9672_SLAVE_ADRESS 		(0x23)
#define LIGHTNING_TO_USB_DEVICE_ID  	(0xF7)
#define LIGHTNING_TO_USB_HOST_ID    	(0xFB)
#define LIGHTNING_TO_SERIAL_ID      	(0xF3)
#define THIRTY_PIN_CONNECTOR_ID     	(0xEF)
#define NO_CABLE_DETECTED_ID        	(0xFF)

/*************************************************************************************************/
/*                            Extern Function Prototypes Section                                 */
/*************************************************************************************************/
extern int32_t iAP2_Host_AppLaunchDone();

/*************************************************************************************************/
/*                                   Function Section                                            */
/*************************************************************************************************/
struct libusb_device_handle* GetUSBDevicehandle()
{
	return glumify;
}
/*
* Opening Lumify device
*/
int lumify_open(void)
{
        int lret = -1;
        if (libusb_init(&glumify_ctx) < 0) {
            printf("Failed to initialize libusb library, please check libusb is available \n");
            return -1;
        }

        glumify = libusb_open_device_with_vid_pid(glumify_ctx, 0x0F36, 0x9006);
        if( glumify != NULL) {
            lret = libusb_claim_interface(glumify, 0);
            if( lret == 0) {
            	printf("Opened Lumify \n");
           }
           else {
    	       	printf("Failed to open Lumify \n");
        	   	libusb_exit(glumify_ctx);
				return -1;
           }
        }
        else {
            printf("Failed to open Lumify \n");
            libusb_exit(glumify_ctx);
			return -1;
        }

        return 0;
}

int testmode(libusb_device_handle* usbDevice)
{

    int count = 0;
    int ret = -1;

    unsigned char data1[8] = {0x00,0x81,0x01,0x00,0x00,0x00,0x05,0x20};
    ret = libusb_bulk_transfer(usbDevice, 0x02, data1, 8, &count, 0);
    printf("count : %d \n",count);
    if(ret < 0) {
        printf("Error during test mode transfer \n");
        return ret;
    }
    usleep(1);

    unsigned char data2[10] = {0};
    ret = libusb_bulk_transfer(usbDevice, 0x84, data2, 10, &count, 0);
    printf("count : %d \n",count);
    if(ret < 0) {
        printf("Error during test mode transfer \n");
        return ret;
    }
    usleep(1);

    unsigned char data3[8] = {0x00,0x81,0x01,0x00,0x00,0x00,0x04,0x20};
    ret = libusb_bulk_transfer(usbDevice, 0x02, data3, 8, &count, 0);
    printf("count : %d \n",count);
    if(ret < 0) {
        printf("Error during test mode transfer \n");
        return ret;
    }
    usleep(1);

    unsigned char data4[10] = {0};
    ret = libusb_bulk_transfer(usbDevice, 0x84, data4, 10, &count, 0);
    printf("count : %d \n",count);
    if(ret < 0) {
    printf("Error during test mode transfer \n");
        return ret;
    }
    usleep(1);

    unsigned char data5[12] = {0x00,0x11,0x02,0x00,0x00,0x00,0x04,0x20,0x0F,0x00,0x0F,0x00};
    ret = libusb_bulk_transfer(usbDevice, 0x02, data5, 12, &count, 0);
    printf("count : %d \n",count);
    if(ret < 0) {
        printf("Error during test mode transfer \n");
        return ret;
    }
    usleep(1);

    unsigned char data6[10] = {0x00,0x11,0x01,0x00,0x00,0x00,0x0D,0x20,0x05,0x00};
    ret = libusb_bulk_transfer(usbDevice, 0x02, data6, 10, &count, 0);
    printf("count : %d \n",count);
    if(ret < 0) {
        printf("Error during test mode transfer \n");
        return ret;
    }
    usleep(1);

    unsigned char data7[10] = {0x00,0x11,0x01,0x00,0x00,0x00,0xFF,0x20,0x01,0x00};
    ret = libusb_bulk_transfer(usbDevice, 0x02, data7, 10, &count, 0);
    printf("count : %d \n",count);
    if(ret < 0) {
        printf("Error during test mode transfer \n");
        return ret;
    }

    return 0;
}

void start_scan()
{
	int ret;
    //done = 0;//Subbu_TBD
	initProbe(glumify);
	startScanning(glumify);
	ret = testmode(glumify);
	if(ret < 0) {
		printf("Failed to put Transducer in test mode \n");
		libusb_exit(glumify_ctx);
		libusb_close(glumify);
		glumify = NULL;

		return ;
	}

	bulk_in0 = libusb_alloc_transfer(0);
    libusb_fill_bulk_transfer(bulk_in0, glumify, 0x86, (unsigned char *)(&databuf0[14]), (sizeof(databuf0)-18), bulkin_cb1, databuf0, 0);

	bulk_in1 = libusb_alloc_transfer(0);
    libusb_fill_bulk_transfer(bulk_in1, glumify, 0x86, (unsigned char *)(&databuf1[14]), (sizeof(databuf1)-18), bulkin_cb1, databuf1, 0);

	if (libusb_submit_transfer(bulk_in0) < 0) {
		printf(" Failed to submit request for Lumify bulkin transfer \n");
		libusb_exit(glumify_ctx);
		libusb_close(glumify);
		glumify	= NULL;

		return ;
	}

	while (!done) {
		if(libusb_handle_events(glumify_ctx) < 0) {
			printf("Error");
		}
	}

	return ;
}

int scan()
{
    printf(" Scan thread Start \n");
	if(osa_thread_create(&scan_thread_id, NULL, start_scan, NULL) != 0) {
		printf("Error while starting scan thread ");
		return -1;
	}

    sleep(7);
	return 0;
}


void stop_scan()
{
    printf("Stop Scanning \n");
   // osa_thread_destroy(scan_thread_id);//Subbu-TBD
    //scan_thread_id = 0;//Subbu-TBD
	done = 1;
    printf("Count : %d Data sent: %llu \n",pkt_cnt ,data_size);
}

void lumify_close()
{
    printf("Lumify Transducer port closed \n");
	libusb_close(glumify);
	libusb_exit(glumify_ctx);

	glumify = NULL;

	return;
}

static void bulkin_cb1(struct libusb_transfer *transfer)
{
	int ret;
    int32_t MutexLockStatus; 
    int32_t MutexUnlockStatus; 

    if (transfer->status != LIBUSB_TRANSFER_COMPLETED) {
        printf("Transducer bulkin transfer 1 failed , transfer status %d \n", transfer->status);
        return;
    }

    if(pkt_cnt == 500000) {
        stop_scan();
        return;
    }

    if(databuf0 == transfer->user_data) {
        ret = libusb_submit_transfer(bulk_in1);
        if (ret < 0) {
            printf("Failed to submit request for Lumify bulkin transfer 1 for next data : %d \n", ret );
            return;
        }
      
        databuf0[0] = 6;//Endpoint 6 data transfer
        databuf0[12] = (((transfer->actual_length) & (0x0000FF00)) >> 8) ;
        databuf0[13] = ((transfer->actual_length) & (0x000000FF)) ;

        result = calculate_crc32c((uint32_t)0xffffffff,databuf0, transfer->actual_length+14);

        transfer->buffer[transfer->actual_length] = (result & 0xFF000000)>>24;
        transfer->buffer[transfer->actual_length + 1] = (result & 0x00FF0000)>>16;
        transfer->buffer[transfer->actual_length + 2] = (result & 0x0000FF00)>>8;
        transfer->buffer[transfer->actual_length + 3] = (result & 0x000000FF);

        MutexLockStatus = osa_mutex_lock(&Mutex_Handle); 
        if(MutexLockStatus != 0)
            printf("Mutex unable to lock Error = %d \n", MutexLockStatus);

retry:
          if(iAP2_Feat_NEA_bfnSendData(databuf0, transfer->actual_length+18) != 0) {
            printf("Failed to send scan data 1 to iPhone going to retry \n");
            usleep(10);
            goto retry;
        }

        MutexUnlockStatus = osa_mutex_unlock(&Mutex_Handle); 
        if(MutexUnlockStatus != 0)
            printf("Mutex unable to Unlock Error = %d \n", MutexUnlockStatus);

    }
    else if(databuf1 == transfer->user_data) {
        ret = libusb_submit_transfer(bulk_in0);
        if (ret < 0) {
            printf("Failed to submit request for Lumify bulkin transfer for next data : %d \n", ret );
            return;
        }
        
        databuf1[0] = 6;//Endpoint 6 data transfer
        databuf1[12] = (((transfer->actual_length) & (0x0000FF00)) >> 8) ;
        databuf1[13] = ((transfer->actual_length) & (0x000000FF)) ;

        result = calculate_crc32c((uint32_t)0xffffffff,databuf1, transfer->actual_length+14);

        transfer->buffer[transfer->actual_length] = (result & 0xFF000000)>>24;
        transfer->buffer[transfer->actual_length + 1] = (result & 0x00FF0000)>>16;
        transfer->buffer[transfer->actual_length + 2] = (result & 0x0000FF00)>>8;
        transfer->buffer[transfer->actual_length + 3] = (result & 0x000000FF);

        MutexLockStatus = osa_mutex_lock(&Mutex_Handle); 
        if(MutexLockStatus != 0)
            printf("Mutex unable to lock Error = %d \n", MutexLockStatus);

retry1:
        if(iAP2_Feat_NEA_bfnSendData(databuf1, transfer->actual_length+18) != 0) {
            printf("Failed to send scan data 1 to iPhone going to retry \n");
            usleep(10);
            goto retry1;
        }

        MutexUnlockStatus = osa_mutex_unlock(&Mutex_Handle); //subbu_mutex
        if(MutexUnlockStatus != 0)
            printf("Mutex unable to Unlock Error = %d \n", MutexUnlockStatus);
    }
    else {
        printf("Received wrong data \n");
        return;
    }

    pkt_cnt++;
    data_size += (transfer->actual_length + 18);

	return;
}
/*************************************************************************************************
* @name     vfnIdle
*
* @brief    Check if device is disconnected, otherwise continue gTestSuite_SM state machine
*           
* @param   	None
*
* @return  	None
*
**************************************************************************************************/
void vfnIdle(void)
{
    #if 0
   // uint8_t write_buffer[10] = {'s','u','m','a','n','H'};
   uint8_t write_buffer[10] = {0xFF,0x03,0x01,0x0A,0x20};
    WriteToModuleController((uint8_t*)&write_buffer,5);
     #endif
     
    //Gpio test code start
    #if 0
    static unsigned int val = 0;
    int ReadResult = 0;
    gpio_write(4, (unsigned int)val);
    if(0 == val)
        val = 1;
    else
        val = 0;
    printf(" GPIO Test = %d \n",val);
   
    //Gpio Reading operation
       
     ReadResult = gpio_read(0);
     if(-1 != ReadResult)
        printf("GPIO0 read value = %d \n", ReadResult);
    else
        printf("GPIO reading failed \n");
    #endif
    //Gpio test code end

#if 0
    libusb_handle_events (NULL);//subbu_test
    osa_time_delay(1000);// 1 Sec delay
#endif
    WriteProtocolData(BATTERY_INFORMATION, 0, 0);//Only for testing purpose
    osa_time_delay(1000);// 1 Sec delay
    
}
/*************************************************************************************************
* @name     vfnClearInitScreen
*
* @brief    When the device is detached, clean the needed variables
*           
* @param   	None
*
* @return  	None  
*
**************************************************************************************************/
void vfnClearInitScreen(void)
{
    
    /*Clear Protocol*/
    TestSuite_res_en_Protocol[10] = ' ';
    TestSuite_res_en_Protocol[11] = ' ';
    TestSuite_res_en_Protocol[12] = ' ';
    TestSuite_res_en_Protocol[13] = ' ';

    vfnCopyBuffInfo((uint8_t*) &gau8empty[0], (uint8_t*)&TestSuite_res_en_Cable[CABLE_CHAR_INIT], CABLE_CHAR_NUM);
    vfnCopyBuffInfo((uint8_t*) &gau8empty[0], (uint8_t*)&TestSuite_res_en_Communication[TRANSPORT_CHAR_INIT], TRANSPORT_CHAR_NUM);
    gu8StillWaitingForDevice = 0;

#if MFI_IAP1_ENABLE == TRUE
    iAP_Cmd_gsSessionRxData.wSessionID = 0;
#endif
    gu8BackUpCharBuffer[0]= '0';
    gu8BackUpCharBuffer[1]= '%';
    gu8BackUpCharBuffer[2]= 0x00;

#if MFI_IAP1_ENABLE == TRUE
    iAP_Cmd_gdwEnabledFIDTokens &= ~(IPOD_PREF_VOICE_OVER_BIT_MASK | EA_PROTOCOL_TOKEN_BIT_MASK);
#endif
    
    gTestSuite_SM.ActualState = TS_WAIT_FOR_IOS_DEVICE_STATE;

    gTestSuite_SM.PrevState = 	TS_CLEAR_INIT_SCREEN_STATE;

    g_CurrentTestId = NO_ACTIVE_TEST;
    g_TestStatus = TEST_STATUS_IDLE;
    gu8OnGoingTest = NO_ACTIVE_TEST;
}

/*************************************************************************************************
* @name     PerformLIMTranslationAppTasks
*
* @brief    Perform the MFi Test Suite functionality.
*           
* @param   	None
*
* @return  	None  
*
**************************************************************************************************/
void PerformLIMTranslationAppTasks(void)
{
    /* Application State machine call*/
    vptrfnTestSuiteStateMachine[gTestSuite_SM.ActualState]();
#if MFI_IAP2_ENABLE && MFI_IAP1_ENABLE
    if((!(IAP_HOST_CHECK_STATUS_REGISTER(IAPH_APP_CMD_ALLOWED))) && (!(IAP2_HOST_MSG_ALLOWED)))
#else
#if MFI_IAP2_ENABLE
    if((!(IAP2_HOST_MSG_ALLOWED)))
#else /*MFI_IAP1_ENABLE*/
    if(!(IAP_HOST_CHECK_STATUS_REGISTER(IAPH_APP_CMD_ALLOWED)))
#endif
#endif
    {
        //Note::
    }
    else
    {
        gu8iOSDeviceDetectedFlag = FALSE;
    }
}

/*************************************************************************************************   
* @name     LIM_Translation_Application
*
* @brief    Main entry level function for LIM Translation Application.
*
* @param   	none     
*
* @return  	SUCCESS
*
**************************************************************************************************/
int32_t LIM_Translation_Application(void)
{
    /* initialize global variables */
    gTestSuite_SM.ActualState = TS_WAIT_FOR_IOS_DEVICE_STATE;
    g_CurrentTestId = NO_ACTIVE_TEST;
    g_TestStatus = TEST_STATUS_IDLE;

    /* This for loop should be replaced. By default this loop allows a single stepping. */
    for (;;)
    {
        if (g_InfoFlag == 0)
        {
            PRINTF("\r\n*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***\r\n");
            PRINTF("\r\n  LIM Translation Application Start    \r\n");
            PRINTF("\r\n*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***\r\n");
            WriteProtocolData(BOOT_COMPLETE, 0, 0);//Sending message to micro. LIM boot Success
            g_InfoFlag = 1;
        }

        if (g_DemoModeType == DEMO_TYPE_MFI)
        {
            PerformLIMTranslationAppTasks();
            if (g_CurrentTestId == ALL_TEST_END)
            {
                PRINTF("\r\n LIM Translation Application Exit \r\n");
                break;
            }
        }
        else
        {
            PRINTF("\r\n  Simulation Test    \r\n");
            /* simulation mode*/
            vfnIdle();
        }
    }

    /* Never leave main */
    return 0;
}
/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
