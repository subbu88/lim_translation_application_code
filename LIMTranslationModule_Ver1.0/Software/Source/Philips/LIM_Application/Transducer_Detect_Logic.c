/*******************************************************************************************
*
* Copyright 2018 L&T Technology Services Limited, Mysore India
*
**************************************************************************************************
**************************************************************************************************
*
* Comments:
*       File Name : Transducer_Detect_Logic.c
*
**END********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include "Transducer_Detect_Logic.h"
#include "osa_common.h"
#include "GPIO_lib.h"
#include <libusb-1.0/libusb.h>//subbu_test

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/

/* Linux GPIO number = ((Gpio_Bank - 1)*32 + (Gpio_bit))
   GPIO1_IO04, Power Enable pin for Transducer = Linux Gpio num = GPIO_4
   GPIO1_IO00, Transducer Detection Pin = Linux Gpio num = GPIO_0 */
#define GPIO_4          4
#define GPIO_0          0

/* 1 = Output Pin,  0 = Input Pin */
#define OUT_DIRECTION   1
#define IN_DIRECTION    0

/* Set bit = HIGH = 1,  Off bit = LOW = 0*/
#define High            1
#define LOW             0

/* Transducer detection status */
#define TRANSDUCER_CONNECTED    0
#define TRANSDUCER_DISCONNECTED 1


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

int g_iTransducer_Detection_Status = TRANSDUCER_DISCONNECTED;


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static OsaThread  Transducer_Detect_thread_id;
static int iTransducer_dect_Status = TRANSDUCER_DISCONNECTED;
static int iprev_tran_det_status = TRANSDUCER_DISCONNECTED;



/*************************************************************************************************/
/*                                 Volatile Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
 
static int Transducer_Detect_Thread_Init();
static void TransducerDetectionThread();
static void GPIO_Init();

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

void Init_Transducer_Detection_Logic(void)
{
    printf("Init_Transducer_Detection_Logic Entry \n");
    GPIO_Init();
    Transducer_Detect_Thread_Init();
}

static int Transducer_Detect_Thread_Init()
{
    printf("Transducer Detection Thread Initialization \n ");
	if(osa_thread_create(&Transducer_Detect_thread_id, NULL, TransducerDetectionThread, NULL) != 0) {
		printf("Error while starting Transducer detection thread ");
		return -1;
	}
	return 0;
}

static void TransducerDetectionThread()
{
    printf("TransducerDetectionThread Entry\n");
    while(1)
    {
        iTransducer_dect_Status = gpio_read(GPIO_0); //Please debug
       // iTransducer_dect_Status = TRANSDUCER_CONNECTED;//only for debugging
        if(-1 != iTransducer_dect_Status)
            printf("GPIO_0 read value = %d \n", iTransducer_dect_Status);
        else
            printf("GPIO_0 reading failed \n");

        if(iTransducer_dect_Status != iprev_tran_det_status)
        {
            if(TRANSDUCER_CONNECTED == iTransducer_dect_Status)
            {
                //gpio_write(GPIO_4, High); //Enable Power Supply to Transducer
                g_iTransducer_Detection_Status = TRANSDUCER_CONNECTED;
                printf("Transducer Connected - Enabling Power Supply \n");

                //Test Code _Start
                #if 0
                 osa_time_delay(5000);//1 sec delay
                struct libusb_device_handle *glumify=NULL;
                struct libusb_context *glumify_ctx = NULL;
                if (libusb_init(&glumify_ctx) < 0) {
                printf("Failed to initialize libusb library, please check libusb is available \n");
                }
                glumify = libusb_open_device_with_vid_pid(glumify_ctx, 0x0F36, 0x9006);
                if( glumify != NULL) {
                   
            	        printf("Opened Lumify \n");
                    }
                else {
    	       	    printf("Failed to open Lumify \n");
        	   	    libusb_exit(glumify_ctx);
                    }
                #endif
                //Test Code _End
            }
            else if (TRANSDUCER_DISCONNECTED == iTransducer_dect_Status)
            {
                //gpio_write(GPIO_4, LOW); //Disable Power Supply to transducer
                g_iTransducer_Detection_Status = TRANSDUCER_DISCONNECTED;
                printf("Transducer Disconnected - Disabling Power Supply \n");
            }
            iprev_tran_det_status = iTransducer_dect_Status;
        }

        //printf(" Tran det Thread running \n");
        osa_time_delay(1000);//1 sec thread
    }
}
static void GPIO_Init()
{
    #if 0
    /* Export Gpio4 pin */
    if(0 != gpio_export(GPIO_4))
		printf(" GPIO_4 export Failed \n");

	/* gpio_direction(pin num, direction) */
	if( 0 != gpio_direction(GPIO_4, OUT_DIRECTION))
		printf(" GPIO_4 set as out direction Failed");

    #endif

    if(0 != gpio_export(GPIO_0))
		printf(" GPIO_0 export Failed\n");
		
	if( 0 != gpio_direction(GPIO_0, IN_DIRECTION))
		printf(" GPIO_0 set as In-direction Failed");

    /* Initially Disable power Supply for transducer */
    //gpio_write(GPIO_4, LOW); //Please debug
}

int Get_Transducer_Detection_Status()
{
    return g_iTransducer_Detection_Status;
}
/************************** Transducer Detect Logic Code End  ************************************/

