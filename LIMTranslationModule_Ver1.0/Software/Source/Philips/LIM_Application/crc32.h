#ifndef CRC32_H_	
#define CRC32_H_

#if defined(__cplusplus)
extern "C" {
#endif

uint32_t
calculate_crc32c(uint32_t crc32c,
    const unsigned char *buffer,
    unsigned int length);

#if defined(__cplusplus)
}
#endif /* __cplusplus*/

#endif
