/*HEADER******************************************************************************************

************************************************************************************************** 
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
**************************************************************************************************
*
* Comments:
*       File Name : Packet_Manager.c
*
**END********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "MFi_Config.h"
#include "iAP_Transport.h"
#include "Packet_Manager.h"
#include "iAP2_Nat_Ext_Acc_Feat.h"
#include "ProjectTypes.h"
#include "iAP2_Host.h"
#include "ProbeInitializer.h"
#include "crc32.h"
#include <string.h>
#include <libusb-1.0/libusb.h>
#include "Module_Controller.h"

/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define SEND_TRANSFER_SIZE	(10485760)
#define	SEND_BLOCK_COUNTER	(SEND_TRANSFER_SIZE/EA_DEMO_BUFF_SIZE)

#define OPEN	1
#define SCAN	2
#define STOP	3
#define	CLOSE	4

#define LIMPKTHEADERSIZE 	14
#define CRCLength	 		4

#define DEFAULT_ 							0
#define DEFAULT_SERIAL_FLOW_ENDPOINT_PACKET	1
#define DATA_DIRECTION_PACKET				2
#define REQUEST_TYPE_PACKET					3
#define REQUEST_PACKET						4
#define VALUE_1_PACKET						5
#define VALUE_2_PACKET						6
#define INDEX_1_PACKET						7
#define INDEX_2_PACKET						8
#define TIMEOUT_1_PACKET					9
#define TIMEOUT_2_PACKET					10
#define RESERVED_1_PACKET					11
#define RESERVED_2_PACKET					12
#define PAYLOAD_DATA_LENGTH_PKT_1			13
#define PAYLOAD_DATA_LENGTH_PKT_2			14
#define PAYLOAD_DATA_PROCESS_PKT			15
#define CRC_1_PACKET						16
#define CRC_2_PACKET						17
#define CRC_3_PACKET						18
#define CRC_4_PACKET						19
#define EXECUTE_PACKET						20
#define PENDING_PAYLOAD_DATA_PKT			21
/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
uint8_t gSmartphoneHandshake = 0;
//uint8_t gEANativeSendTestFinish = 0;
uint8_t gEANativePortActive = 0;
uint8_t gbaRxBuffer[EA_DEMO_BUFF_SIZE];
//uint32_t gdwBufferOffset = 0;

uint8_t gbaSendBuffer[100] = {0};

sLIMPacketHeader stLIMPacketHeader;//Global instance
uint8_t  LIMBuffer[16384] = {0};
uint32_t Calculatedcrc_32c = 0x00000000;
uint32_t Receivedcrc_32c = 0x00000000;
uint16_t BuffSizeWithOutCRC = 0;
uint16_t PendingBytes = 0;
uint16_t RemainingDataLength = 0;

uint8_t TransmitBufferForIOS[16384] = {0};//To Transmit data
struct libusb_device_handle *glumify;
extern struct libusb_device_handle* GetUSBDevicehandle();
extern OsaMutex Mutex_Handle; 
extern S_BatteryInfo_t stBatteryInfo;//Battery related information

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
//static uint8_t firstTime = 1;
static uint8_t status = CLOSE;
static uint16_t Counter = 0;//TBD Subbu

/*************************************************************************************************/
/*                                 Volatile Variables Section                                    */
/*************************************************************************************************/
volatile uint32_t gdwDataReceivedCounter = 0;

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
//void iAP2_Power_Features_bfnNativeEndpointsTest(uint16_t wMessage);

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/


/*************************************************************************************************
void Serializer(sLIMPacketHeader *pstLIMPacketHeader,uint8_t *pDataBuffer, uint16_t BufferSize)
Serializer Functionality
**************************************************************************************************/

void Serializer(sLIMPacketHeader *pstLIMPacketHeader,uint8_t *pDataBuffer, uint16_t BufferSize)
{
	//printf("Serializer Function Entry \n");
	int32_t MutexLockStatus; 
    int32_t MutexUnlockStatus; 
	unsigned long crc_32;
	uint16_t BuffCount = 0; 
	uint16_t TotalBuffSize = (LIMPKTHEADERSIZE + (BufferSize));
	//int count = 0;
	TransmitBufferForIOS[BuffCount++]=pstLIMPacketHeader->EndPointNum;
	//printf("Sending : EndPoint Number = %d \n",TransmitBufferForIOS[BuffCount-1]);
	TransmitBufferForIOS[BuffCount++]=pstLIMPacketHeader->DataDirection;
	//printf("Sending : DataDirection = %d \n",TransmitBufferForIOS[BuffCount-1]);
	TransmitBufferForIOS[BuffCount++]=pstLIMPacketHeader->ReqType;
	//printf("Sending : ReqType = %d \n",TransmitBufferForIOS[BuffCount-1]);
	TransmitBufferForIOS[BuffCount++]=pstLIMPacketHeader->Request;
	//printf("Sending : Request = %d \n",TransmitBufferForIOS[BuffCount-1]);
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->value) & 0xFF00) >> 8;
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->value)&0x00FF);	
	//printf("Sending : value = %d \n",pstLIMPacketHeader->value);			
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->Index)&0xFF00) >> 8;
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->Index)&0x00FF);
	//printf("Sending : Index = %d \n",pstLIMPacketHeader->Index);
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->Timeout)&0xFF00) >> 8;
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->Timeout)&0x00FF);
	//printf("Sending : Timeout = %d \n",pstLIMPacketHeader->Timeout);
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->Reserved)&0xFF00) >> 8;
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->Reserved)&0x00FF);
	//printf("Sending : Reserved = %d \n",pstLIMPacketHeader->Reserved);
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->DataLength)&0xFF00) >> 8;
	TransmitBufferForIOS[BuffCount++]=((pstLIMPacketHeader->DataLength)&0x00FF);
	//printf("Sending : DataLength = %d \n",pstLIMPacketHeader->DataLength);	
			
	memcpy(&TransmitBufferForIOS[BuffCount], pDataBuffer, sizeof(BufferSize));
	#if 0
	for(count = 0; count < (pstLIMPacketHeader->DataLength); count++)
	printf(" Buff Data[%d] = %d \n",count, pDataBuffer[count]);
	#endif
	crc_32 = calculate_crc32c((uint32_t)0xffffffff,TransmitBufferForIOS, TotalBuffSize);
	printf("Sending Calculated CRC = %lx \n",crc_32);

	BuffCount = (BuffCount + (pstLIMPacketHeader->DataLength));

	TransmitBufferForIOS[BuffCount++] = (crc_32 & 0xFF000000)>>24;
    TransmitBufferForIOS[BuffCount++] = (crc_32 & 0x00FF0000)>>16;
    TransmitBufferForIOS[BuffCount++] = (crc_32 & 0x0000FF00)>>8;
    TransmitBufferForIOS[BuffCount]   = (crc_32 & 0x000000FF);

	MutexLockStatus = osa_mutex_lock(&Mutex_Handle); 
	if(MutexLockStatus != 0)
    	printf("Command Mutex unable to lock Error = %d \n", MutexLockStatus);

	if(iAP2_Feat_NEA_bfnSendData(TransmitBufferForIOS, (TotalBuffSize + CRCLength)) != 0) 
               printf("Failed to send data to iPhone %d \n",__LINE__);
	else
				printf("sent data to iPhone Successful \n");


	MutexUnlockStatus = osa_mutex_unlock(&Mutex_Handle); 
	if(MutexUnlockStatus != 0)
    printf("Command Mutex unable to Unlock Error = %d \n", MutexUnlockStatus);
}

/*************************************************************************************************
void EndPointMapping(sLIMPacketHeader* psLIMPacketHeader)
EndPoint Mapping Functionality
**************************************************************************************************/
void EndPointMapping(sLIMPacketHeader* psLIMPacketHeader)
{
	#if 0
	if(glumify == NULL)
	{
		lumify_open();//TBD- This only for protection purpose during softwrae development
		glumify = GetUSBDevicehandle();
		printf("USB Device handle opened in EndpointMapping() \n");
	}
	#endif

	switch(psLIMPacketHeader->EndPointNum)
	{
		case 0:
		{	printf("EP Mapping Case 0 \n");
			int result = 0;
			// Control Endpoint 0
			// Read Data from Control Endpoint of Transducer
			//stLIMPacketHeader.DataDirection = 0, then Read data from Control Endpoint
			if(0 == psLIMPacketHeader->DataDirection) 
			{
				printf("Reading data from Control Endpont : %d \n",psLIMPacketHeader->EndPointNum);
				
				if(glumify != NULL)
				{
				
				result = libusb_control_transfer(glumify,
							//LIBUSB_RECIPIENT_DEVICE | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_ENDPOINT_IN,
							psLIMPacketHeader->ReqType,//TBD
 							psLIMPacketHeader->Request,
							psLIMPacketHeader->value, 
							psLIMPacketHeader->Index,
							(unsigned char*)(&LIMBuffer),                                			    					psLIMPacketHeader->DataLength,
							psLIMPacketHeader->Timeout);
				
				}
				else
				{
				printf("USB handle not Available \n");
				}

				if (result < 0) {
           			 printf("Read control failed %d ",__LINE__);
					stLIMPacketHeader.Reserved = (0x00)<<8;//00 For Failure
				}
				else
				{
					stLIMPacketHeader.Reserved = (0x01)<<8;//01 For Scccess
				}

			//Form a packet and send to iphone
			Serializer(&stLIMPacketHeader,LIMBuffer,stLIMPacketHeader.DataLength);

			
			}
			else if(1 == psLIMPacketHeader->DataDirection)
			{
				int result = 0;
				// Write Data to Control Endpoint of Transducer
				// sLIMPacketHeader.DataDirection = 1, then Write data to control Endpoint
				printf("Writing data to Control Endpont : %d \n",psLIMPacketHeader->EndPointNum);
				if(glumify != NULL)
				{		
				result = libusb_control_transfer(glumify,
							//LIBUSB_RECIPIENT_DEVICE | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_ENDPOINT_OUT,
							psLIMPacketHeader->ReqType,//TBD 
							psLIMPacketHeader->Request,
							psLIMPacketHeader->value, 
							psLIMPacketHeader->Index,
							(unsigned char*)(&LIMBuffer),
							psLIMPacketHeader->DataLength,
							psLIMPacketHeader->Timeout);

				}

				if (result < 0) {
           			 printf("Read control failed %d ",__LINE__);
					stLIMPacketHeader.Reserved = (0x00)<<8;//00 For Failure
				}
				else
				{
					stLIMPacketHeader.Reserved = (0x01)<<8;//01 For Scccess
				}

				//Form a packet and send to iphone
				Serializer(&stLIMPacketHeader,LIMBuffer,stLIMPacketHeader.DataLength);
			}
		}
		break;
		case 2:
		{
			printf("Writing data to bulk EP 2 \n");

			//Writing bulk data to EP 2
			int bytesTransferred = 0;
			int result = 0;
			int count = 0;

			for(count = 0; count < (psLIMPacketHeader->DataLength); count++)
			printf(" Bulk write Buff Data[%d] = %d \n",count, LIMBuffer[count]);

			if(glumify != NULL)
			{
			result = libusb_bulk_transfer(glumify,
							LIBUSB_ENDPOINT_OUT | 2, 
							(unsigned char*)(LIMBuffer), 
							psLIMPacketHeader->DataLength,
							&bytesTransferred, 
							psLIMPacketHeader->Timeout);
			}
			if(result < 0) {
        			printf("Error during Bulk write to EP-2 \n");
				stLIMPacketHeader.Reserved = (0x00)<<8;//00 For Failure
			}
			else
			{
				stLIMPacketHeader.Reserved = (0x01)<<8;//01 For Scccess
			}
			printf("write data to bulk EP 2 = %d \n",bytesTransferred);
			//Form a packet and send to iphone
			Serializer(&stLIMPacketHeader,LIMBuffer,stLIMPacketHeader.DataLength);
			
		}
		break;
		case 4:
		{
			//Read Data from Endpoint 4
			printf("Reading data from bulk EP 4 \n");
			int bytesTransferred = 0;
			int result = 0;
			if(glumify != NULL)
			{
			result = libusb_bulk_transfer(glumify,
						 	LIBUSB_ENDPOINT_IN | 4, 
							(unsigned char*)(LIMBuffer),
							//psLIMPacketHeader->DataLength, 
							512,//subbu
							&bytesTransferred, 
							psLIMPacketHeader->Timeout);
			}
			if(result < 0) {
        			printf("Error during Bulk Read from EP-4 \n");
				printf("Error Bulk Read result = %d\n",result);
				stLIMPacketHeader.Reserved = (0x00)<<8;//00 For Failure
			}
			else
			{
				stLIMPacketHeader.Reserved = (0x01)<<8;//01 For Scccess
			}
			
			printf("Read data from bulk EP 4 = %d \n",bytesTransferred);
			
			//Form a packet and send to iphone
			Serializer(&stLIMPacketHeader,LIMBuffer,stLIMPacketHeader.DataLength);
		}
		break;
		case 6:
		{
			//Read Data from Endpoint 6
			//Usually Scan data reading from EP6
			//Asynchronous USB API (Non Blocking calls)
			//Right now not using
		}
		break;
		case 8:
		{
			//Module Controller Interface
			printf(" Reading Battery Related info \n");
			if(stBatteryInfo.BatteryPercentage == 0)
				stLIMPacketHeader.Reserved = (0x00)<<8;//00 For Failure
			else
				stLIMPacketHeader.Reserved = (0x01)<<8;//01 For Scccess

			//Will use some locking mechanism here for threadsafe. will improve
			memcpy(&LIMBuffer, &stBatteryInfo,11);//Will improve

			//Form a packet and send to iphone
			Serializer(&stLIMPacketHeader,LIMBuffer,11);//Will improve in coming days
		}
		break;
		case 0x0A:
		{
			printf("Open Transducer in EP\n");
			//To Open Transducer
			if( status == CLOSE ) 
			{
				printf("Log-1\n");
				if(lumify_open() < 0) 
				{
					printf("Failed to open Transducer \n");
					return ;
				}
					status = OPEN;
			} 
			else 
					printf("Transducer already opened \n");
		}
		break;
		case 0x0B:
		{
			printf("Start Scan in EP\n");
			//To Start Scanning
			if((status == OPEN) || ( status == STOP)) 
			{
				printf("Log-2\n");
				if(scan() < 0) 
				{
					printf("Failed to put Transducer in scan mode \n");
					return ;
				}

					status = SCAN;
			} 
			else 
					printf("Transducer not opened or scanning is already in progress \n");
		}
		break;
		case 0x0C:
		{
			printf("Stop Scan Command Received \n");
			//To Stop Scanning
			if(status == SCAN) 
			{
				stop_scan();
				status = STOP;
			} else 
				printf("Scan is not started \n");
		}
		break;
		case 0x0D:
		{
			printf("Close transducer command Received \n");
			//To Close Transducer
			if((status == OPEN)||(status == STOP)) 
			{
				lumify_close();
				status = CLOSE;
			}
			else 
				printf("Transducer is not opened or scan is in progress \n");
		}
		break;
		default:
		break;
	}//End of Switch
}//End of EndPointMapping()

/*************************************************************************************************
void DeSerializer(uint8_t *pDataBuffer, uint16_t wDataSize)
DeSerializer Functionality
**************************************************************************************************/
void DeSerializer(uint8_t *pDataBuffer, uint16_t wDataSize)
			{	
				uint16_t count = 0;
				static uint8_t SerialFlow = DEFAULT_SERIAL_FLOW_ENDPOINT_PACKET;
				
				//printf("Total Buffer Size from iPhone = %d \n",wDataSize);
				for(count = 0; count < wDataSize; count++)
				{//Begin for loop
				 //Big Endian data from iPhone
				 switch(SerialFlow)
				 {
					case DEFAULT_SERIAL_FLOW_ENDPOINT_PACKET:
					{	
						Counter++;//TBD
						stLIMPacketHeader.EndPointNum = pDataBuffer[count];
						//printf("Received : EndPoint Number = %d \n",stLIMPacketHeader.EndPointNum);
						SerialFlow = DATA_DIRECTION_PACKET;
					}
					break;
					case DATA_DIRECTION_PACKET:
					{
						stLIMPacketHeader.DataDirection = pDataBuffer[count];
						//printf("Received : DataDirection = %d \n",stLIMPacketHeader.DataDirection);
						SerialFlow = REQUEST_TYPE_PACKET;
					}
					break;
					case REQUEST_TYPE_PACKET:
					{
						stLIMPacketHeader.ReqType = pDataBuffer[count];
						//printf("Received : ReqType = %d \n",stLIMPacketHeader.ReqType);
						SerialFlow = REQUEST_PACKET;
					}
					break;
					case REQUEST_PACKET:
					{
						stLIMPacketHeader.Request = pDataBuffer[count];
						//printf("Received : Request = %d \n",stLIMPacketHeader.Request);
						SerialFlow = VALUE_1_PACKET;
					}
					break;
					case VALUE_1_PACKET:
					{
						stLIMPacketHeader.value |= pDataBuffer[count]<<8;
						SerialFlow = VALUE_2_PACKET;
					}
					break;
					case VALUE_2_PACKET:
					{
						stLIMPacketHeader.value |= pDataBuffer[count];
						//printf("Received : value = %d \n",stLIMPacketHeader.value);
						SerialFlow = INDEX_1_PACKET;
					}
					break;
					case INDEX_1_PACKET:
					{
						stLIMPacketHeader.Index |= pDataBuffer[count]<<8;
						SerialFlow = INDEX_2_PACKET;
					}
					break;
					case INDEX_2_PACKET:
					{
						stLIMPacketHeader.Index |= pDataBuffer[count];
						//printf("Received : Index = %d \n",stLIMPacketHeader.Index);
						SerialFlow = TIMEOUT_1_PACKET;
					}
					break;
					case TIMEOUT_1_PACKET:
					{
						stLIMPacketHeader.Timeout |= pDataBuffer[count]<<8;
						SerialFlow = TIMEOUT_2_PACKET;
					}
					break;
					case TIMEOUT_2_PACKET:
					{
						stLIMPacketHeader.Timeout |= pDataBuffer[count];
						//printf("Received : Timeout = %d \n",stLIMPacketHeader.Timeout);
						SerialFlow = RESERVED_1_PACKET;
					}
					break;
					case RESERVED_1_PACKET:
					{
						stLIMPacketHeader.Reserved |= pDataBuffer[count]<<8;
						SerialFlow = RESERVED_2_PACKET;
					}
					break;
					case RESERVED_2_PACKET:
					{
						stLIMPacketHeader.Reserved |= pDataBuffer[count];
						//printf("Received : Reserved = %d \n",stLIMPacketHeader.Reserved);
						SerialFlow = PAYLOAD_DATA_LENGTH_PKT_1;
					}
					break;
					case PAYLOAD_DATA_LENGTH_PKT_1:
					{

						stLIMPacketHeader.DataLength |= pDataBuffer[count]<<8;
						SerialFlow = PAYLOAD_DATA_LENGTH_PKT_2;
					}
					break;
					case PAYLOAD_DATA_LENGTH_PKT_2:
					{
						stLIMPacketHeader.DataLength |= pDataBuffer[count];
						//printf("Received : DataLength = %d \n",stLIMPacketHeader.DataLength);
						//If Data length is present and writing to EP then go to process payload packet
						if(((stLIMPacketHeader.DataLength) > 0) && (1 == stLIMPacketHeader.DataDirection))
							SerialFlow = PAYLOAD_DATA_PROCESS_PKT;
						else
						{
							SerialFlow = CRC_1_PACKET;
						}
					}
					break;
					case PAYLOAD_DATA_PROCESS_PKT:
					{
						RemainingDataLength = (wDataSize - (count+1));
						if(RemainingDataLength >= (stLIMPacketHeader.DataLength))
						{
							//printf(" Multiple Packets/only 1 packet received in One Buffer \n");
							memcpy(&LIMBuffer, &pDataBuffer[count],stLIMPacketHeader.DataLength);
							count += ((stLIMPacketHeader.DataLength) - 1);
							//printf(" Count Value = %d \n",count);
							SerialFlow = CRC_1_PACKET;
						}
						else if(RemainingDataLength < (stLIMPacketHeader.DataLength))
						{
							//printf(" InComplete packet received \n");
							memcpy(&LIMBuffer, &pDataBuffer[count],RemainingDataLength);
							count = 0;
							SerialFlow = PENDING_PAYLOAD_DATA_PKT;
						}
					}
					break;
					case CRC_1_PACKET:
					{
						//printf(" Enter CRC Decoding \n");
						Receivedcrc_32c |= pDataBuffer[count]<<24;
						SerialFlow = CRC_2_PACKET;
					}
					break;
					case CRC_2_PACKET:
					{
						Receivedcrc_32c |= pDataBuffer[count]<<16;
						SerialFlow = CRC_3_PACKET;
					}
					break;
					case CRC_3_PACKET:
					{
						Receivedcrc_32c |= pDataBuffer[count]<<8;
						SerialFlow = CRC_4_PACKET;
					}
					break;
					case CRC_4_PACKET:
					{
						BuffSizeWithOutCRC = stLIMPacketHeader.DataLength + LIMPKTHEADERSIZE;	
						Receivedcrc_32c |= pDataBuffer[count];
						if(1 == stLIMPacketHeader.DataDirection)
						Calculatedcrc_32c = calculate_crc32c((uint32_t)0xffffffff,
											pDataBuffer,BuffSizeWithOutCRC);
						else
						Calculatedcrc_32c = calculate_crc32c((uint32_t)0xffffffff,
											pDataBuffer,LIMPKTHEADERSIZE);

						//printf("Received CRC = %d \n",Receivedcrc_32c);
						//printf("Calculated CRC = %d \n",Calculatedcrc_32c);
						printf("Received CRC = %x \n",Receivedcrc_32c);
						printf("Calculated CRC = %x \n",Calculatedcrc_32c);

						if(Receivedcrc_32c != Calculatedcrc_32c)
						{
							printf(" ------ iPhone Received Data CRC Not matching !!!! ----- \n"); 
							memset (&stLIMPacketHeader,0,sizeof(sLIMPacketHeader));
							Receivedcrc_32c = 0x00000000;
							Calculatedcrc_32c = 0x00000000;						
							memset (&LIMBuffer,0,16384);
						SerialFlow = DEFAULT_SERIAL_FLOW_ENDPOINT_PACKET; //TBD-Subbu- Move to next packet
						}
						else
						{
							printf(" +++++++ iPhone Received Data CRC Matching ++++++++\n"); 
							//Map data to corresponding Transducer Endpoint or Module Controller
							printf("Complete Packets Received = %d \n", Counter);
							if(Counter >= 16383)
								Counter = 0;
							EndPointMapping(&stLIMPacketHeader);

  							memset (&stLIMPacketHeader,0,sizeof(sLIMPacketHeader));
							Receivedcrc_32c = 0x00000000;
							Calculatedcrc_32c = 0x00000000;						}
							memset (&LIMBuffer,0,16384);
						SerialFlow = DEFAULT_SERIAL_FLOW_ENDPOINT_PACKET;
					}
					break;
					case PENDING_PAYLOAD_DATA_PKT:
					{
						printf("Processing Incomplete Packet \n");
						PendingBytes = ((stLIMPacketHeader.DataLength) - RemainingDataLength);
						memcpy(&LIMBuffer, &pDataBuffer[RemainingDataLength],PendingBytes);
						count += PendingBytes;
						SerialFlow = CRC_1_PACKET;
					}
					break;

				 }//End of Switch SerialFlow
				}//End of For loop

			}
/*************************************************************************************************
* @name     vfnMFi_EADemo_iAP2Callback
*
* @brief    Callback function that is called when an Native External Accessory Event Occurs
*
* @param    event : Event that made the call. The possible events are:
*                 IAP2_FEAT_NEA_PROT_ACTIVE         The NEA session was opened
*                 IAP2_FEAT_NEA_PROT_INACTIVE       The NEA session was closed
*                 IAP2_FEAT_NEA_PROT_DATA_RECV      NEA data was received
*                 IAP2_FEAT_NEA_PROT_RECV_NO_MEM    NEA data was received but it was lost because 
*                                                   the RX buffer was full
*                 IAP2_FEAT_NEA_PROT_SEND_COMPLETE  All the data in the TX buffer was sent
*
*           wSize : Data size
*
* @return   void
*
**************************************************************************************************/
void vfnMFi_EADemo_iAP2Callback(uint8_t bEvent, uint16_t wSize)
{
	switch(bEvent)
	{
	case IAP2_FEAT_NEA_PROT_ACTIVE:
		printf("\n IAP2_FEAT_NEA_PROT_ACTIVE \n");
		iAP2_Feat_NEA_bfnRead(&gbaRxBuffer[0]);
		gbaSendBuffer[0] = 0x04;
		gbaSendBuffer[1] = 0x00;
		gbaSendBuffer[2] = 0x01;
		gbaSendBuffer[3] = 0x00;
		gbaSendBuffer[4] = 0x01;
		iAP2_Feat_NEA_bfnSendData(gbaSendBuffer,EA_DEMO_HANDSHAKE_SIZE);
		break;

	case IAP2_FEAT_NEA_PROT_INACTIVE:
		printf("\n IAP2_FEAT_NEA_PROT_INACTIVE \n");
		gSmartphoneHandshake = 0;
		break;

	case IAP2_FEAT_NEA_PROT_DATA_RECV:
		printf("EA_DEMO_BANDWIDTH_TEST_ACTIVE Data received from iPhone : %d \n",gSmartphoneHandshake);
		if(gSmartphoneHandshake)
		{
			iAP2_Feat_NEA_bfnRead(&gbaRxBuffer[0]);
			if(wSize > LIMPKTHEADERSIZE)
			DeSerializer(&gbaRxBuffer[0], wSize);
			
		}
		else
		{
			iAP2_Feat_NEA_bfnRead(&gbaRxBuffer[0]);
			gSmartphoneHandshake = 1;
		}
		break;

	case IAP2_FEAT_NEA_PROT_RECV_NO_MEM:
		break;

	case IAP2_FEAT_NEA_PROT_SEND_COMPLETE:

		break;

	default:
		break;
	}
}

/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
