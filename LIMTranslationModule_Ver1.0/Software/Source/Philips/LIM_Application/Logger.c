/*HEADER******************************************************************************************

************************************************************************************************** 
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
**************************************************************************************************
*
* Comments:
*       File Name : Logger.c
*
**END********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "Logger.h"

/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/




/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/





/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
//static FILE *fptr;
//static char buffer[100];
//static char LimTestBuff[] = "This is Philips Lumify project";
void Test_Write(void);
/*************************************************************************************************/
/*                                 Volatile Variables Section                                    */
/*************************************************************************************************/




/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/



/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

void Init_Logging_System()
{
   /* Open file for both reading and writing */
   //if((fptr = fopen("LumifyLogger.txt","w+")) == NULL)
   //"/sys/class/gpio/gpio%d/value"
   #if 0
   if((fptr = fopen("/usr/LumifyLogger.txt","w+")) == NULL)
   {
       printf("Error! opening file \n");
   }
   else
   {
    printf("LumifyLogger File created successfully. \n");
   }

    //Write_Log();
    
#endif
Test_Write();
}
#if 0
int Write_Log(void)
{
    printf(" Write_Log Entry \n");
    /* Write data to the file */
    if(fptr != NULL)
    {
        fwrite(LimTestBuff, strlen(LimTestBuff) + 1, 1, fptr);
        printf(" Write_Log Success!!!!  \n");
    }
    return 0;
}
#endif
 void Test_Write(void)
 {
     FILE *fp;
   char c[] = "This is Philips Lumify Application ";
 
     printf(" Test Write function testing \n");
   /* Open file for both reading and writing */
   fp = fopen("file.txt", "w+");

   /* Write data to the file */
   fwrite(c, strlen(c) + 1, 1, fp);

 }
 #if 0
int Read_Log(void)
{
    /*
    for(n = 1; n < 5; ++n)
   {
      fread(&num, sizeof(struct threeNum), 1, fptr); 
      printf("n1: %d\tn2: %d\tn3: %d", num.n1, num.n2, num.n3);
   }
   */
    printf(" Read_Log Entry \n");

    /* Seek to the beginning of the file */
    fseek(fptr, 0, SEEK_SET);

    /* Read and display data */
    fread(buffer, strlen(LimTestBuff)+1, 1, fptr);
    printf("%s\n", buffer);

    return 0;
}
#endif
/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
