/*HEADER******************************************************************************************
*
* Copyright 2018 L&T Technology Services Limited, Mysore- India.
*
**************************************************************************************************

**************************************************************************************************
*
* Comments:
*       File Name : Module_Controller.h 
*
**END********************************************************************************************/

#ifndef MODULE_CONTROLLER_H_
#define MODULE_CONTROLLER_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
//Supporting commands b/n Module controller and LIM
#define  BOOT_COMPLETE				1
#define  MFI_FAILURE				2
#define  BATTERY_INFORMATION        3
#define  MISCELLANEOUS_CMD			4
#define  FIRMWARE_UPGRADE           5

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
#if defined(__cplusplus)
extern "C" {
#endif

typedef struct SbatteryInfo
{
    uint8_t BatteryPercentage;  // 0 - 100 % of battery percentage
    uint8_t ChargerConnStatus;  // 1 = charger connected. 0 = charger disconnected
    uint8_t BattFaults;         // Battery related Error
    uint16_t BattTemperature;   //Battery Temperature in kelvin
    uint16_t BattFullChargeCapacity; //Battery Full charge Capacity in mAH
    uint16_t BattDesignCapacity; //Battery Design Capacity in mAH
    uint16_t BattChargeCycles; //Battery Charge Discharge Cycles
    uint16_t BattVoltage;       //Battery voltage
} S_BatteryInfo_t;

typedef struct moduleControllerProtocol
{
    uint8_t StartOfPacket;      // Start of Packet
	uint8_t CmdType;   		    // Command type received from microController
	uint8_t DataLen; 		    // Length of the data 
    uint8_t PayloadData[20];    // Payload data, size will recheck?
	uint8_t RecCheckSum;        // Received CheckSum
}S_ModuleControllerProtocol_t;

//Public Functions
void Initialize_Module_Controller();
int WriteToModuleController(const uint8_t* parrucData, uint8_t ucNoOfBytes );
int WriteProtocolData(uint8_t Cmd, const uint8_t* parrucData, uint8_t ucNoOfBytes );


#if defined(__cplusplus)
}
#endif /* __cplusplus*/

#endif /* MODULE_CONTROLLER_H_ */
/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
