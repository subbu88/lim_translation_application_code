//
//  ProbeInitializer.h
//  PiUsbTest
//
//  Provides methods for initializing the probe and to start scanning.
//
//  Created by Alex Hunt on 4/25/18.
//  Copyright © 2018 Philips Electronics North America Corporation. All rights reserved.
//

#ifndef ProbeInitializer_h
#define ProbeInitializer_h

#include <libusb-1.0/libusb.h>

#if 0
class libusb_device_handle;

class ProbeInitializer {
public:
    static void initProbe(libusb_device_handle* usbDevice);
    static void startScanning(libusb_device_handle* usbDevice);
};
#endif
void initProbe(libusb_device_handle* );
void startScanning(libusb_device_handle* );

int lumify_open(void);

int scan();

void stop_scan();

void lumify_close();

extern void temp_voltage_monitor();

#endif /* ProbeInitializer_h */
