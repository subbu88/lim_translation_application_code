 /*HEADER******************************************************************************************
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
*
**************************************************************************************************
**************************************************************************************************
*
* Comments:
*       File Name : miscFunctions.h
*
**END********************************************************************************************/
 
#ifndef MISCFUNCTIONS_H_
#define MISCFUNCTIONS_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "MFi_Config.h"
#include "iAP2_Host.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define USB_ROLL_SWITCH_DONE   		 (5)

#define INVALID_TEST_NO              (-2)
#define NO_ACTIVE_TEST               (-1)
#define ACTIVE_DISPLAY_CONFIG		 (0)
#define ACTIVE_SELF_TEST             (1)
#define ACTIVE_EA_TEST				 (2)
#define ACTIVE_FILE_TRANSFER_TEST    (3)
#define ACTIVE_MEDIA_LIBRARY_TEST    (4)
#define ACTIVE_ASSISTIVE_TOUCH_TEST	 (5)
#define ACTIVE_MEDIA_PLAYER_TEST     (6)
#define ACTIVE_EA_NATIVE_TEST     (7)
#define ALL_TEST_END				 (8)

#define PRINTF printf
#define SCANF scanf

#define TEST_STATUS_IDLE				(0)
#define TEST_STATUS_BUSY				(1)

#define DEMO_TYPE_MFI 	1			/* actual mfi tests will be performed.*/
#define DEMO_TYPE_SIM	0			/* Simulation of Demo tests, no actual MFi test performed */ /* for developer only*/

#define CONVERT_TO_ASCII(x)   		  	(x + 0x30)

#define CLEAR   						(0)
#define DISABLE  						(0)

#define ON 								(1)
#define OFF 							(0)
#define PLAY   							(1)
#define PAUSE  							(0)
#define MUTE   							(1)
#define UNMUTE 							(0)

#if MFI_IAP2_ENABLE
#define iAP2_ACTIVE 					(iAP2_Host_gwStatus & (1 << IAP2H_IAP2_DETECTED))
#define iAP1_ACTIVE 					(iAP2_Host_gwStatus & (1 << IAP2H_IAP1_DETECTED))
#elif MFI_IAP1_ENABLE
#define iAP2_ACTIVE 					(FALSE)
#define iAP1_ACTIVE 					(TRUE)
#endif


#define MULTI_TEST_TITLE_SIZE       	(19)
#define MULTI_TEST_RESULT_SIZE      	(40)

#define LED1_TOG  						GPIOA_PTOR |= (1 << 11);
#define LED2_TOG  						GPIOA_PTOR |= (1 << 28);
#define LED3_TOG  						GPIOA_PTOR |= (1 << 29);
#define LED4_TOG  						GPIOA_PTOR |= (1 << 10);


#define CABLE_CHAR_INIT       			(7)
#define CABLE_CHAR_NUM        			(20)
#define TRANSPORT_CHAR_INIT    			(15)
#define TRANSPORT_CHAR_NUM    			(15)

#define NO_CABLE_DETECTED_ID        	(0xFF)
enum
{
    USB_DEVICE_COMM,
    USB_HOST_COMM,
    UART_COMM,
};

/*************************************************************************************************/
/*                                      Typedef & enum Section                                   */
/*************************************************************************************************/

/*Following defines are used to set flag events to make changes on the screen*/
enum
{
    TS_WAIT_FOR_IOS_DEVICE_STATE,		/*00*/
    TS_DETECT_IAP_PROTOCOL_STATE,
    TS_DETECT_TRANSPORT_STATE,
    TS_DETECT_CABLE_STATE,				/*03*/
    TS_IDLE_STATE,
    TS_CLEAR_INIT_SCREEN_STATE,
    EA_WAIT_FOR_IOS_APP,				/*06*/
    EA_WAIT_FOR_EVENT,
};

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/
extern uint8_t TestSuite_res_en_Cable[];
extern uint8_t TestSuite_res_en_Communication[];
extern uint8_t TestSuite_res_en_Protocol[];
extern uint8_t TestSuite_res_en_MFi_Version[];
extern sSM gTestSuite_SM;      /*Control the Test Suite States*/
extern uint32_t	gEaTestNo;
extern uint8_t gu8OnGoingTest;
extern uint8_t 	gu8BackUpCharBuffer[];
extern uint32_t	g_CurrentTestId;
extern uint32_t	g_TestStatus;
extern uint8_t 	gu8MediaLibraryReaded;
extern uint32_t	g_DemoModeType;

#if (MFI_TRANSPORT_UART == TRUE) || (MFI_TRANSPORT_USB_HOST_MODE == TRUE) || (MFI_TRANSPORT_USB_DEVICE_MODE == TRUE)
extern uint8_t TestSuite_res_en_Title[];
#endif

extern const uint8_t gau8empty[21];

extern const uint8_t gau8USBDeviceTrans[16];
extern const uint8_t gau8USBRollSwitchTrans[16];
extern const uint8_t gau8USBHostTrans[16];
extern const uint8_t gau8UartTrans[16];

extern const uint8_t gau8USBLigthingHostCable[21]; /*ID0*/
extern const uint8_t gau8USBLigthingDeviceCable[21]; /*ID1*/
extern const uint8_t gau8USBLigthingSerialCable[21]; /*ID0 ID1 */
extern const uint8_t gau8USB30PinCable[21];          /*ID2*/
extern const uint8_t gau8AppleStandardCable[21];  /*NC ID*/

extern const uint8_t gau8SelfTest[19];
extern const uint8_t gau8MedLibTest[19];
extern const uint8_t gau8FileTransferTest[19];

extern const uint8_t gau8iAPError[];
extern const uint8_t gau8StartTest[];
extern const uint8_t gau8TestInProgress[];
extern const uint8_t gauTrack[];

extern const uint8_t gau8WaitingMessage[];

extern const uint8_t gau8IDTranfer[];
extern const uint8_t gau8PackageSize[];
extern const uint8_t gau8TransProgress[];
extern const uint8_t gau8Error[];
extern const uint8_t gau8Cancel[];
extern const uint8_t gau8Done[];
extern const uint8_t gau8zero[];
extern const uint8_t gau8FisrtAndOnly[];
extern uint16_t g_wUSBHostStatus;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
#if defined(__cplusplus)
extern "C" {
#endif

void vfnLED_Init(void);                 /*test*/
void vfnCopyBuffInfo(uint8_t* pu8Source, uint8_t* pu8Destiny, uint8_t u8Size);
void vfnUnSizeCopyBuff(uint8_t* pu8Source, uint8_t* pu8Destiny);
void vfnGetMFiTransport(void);

#if defined(__cplusplus)
}
#endif /* __cplusplus*/

#endif /* MISCFUNCTIONS_H_ */
/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
