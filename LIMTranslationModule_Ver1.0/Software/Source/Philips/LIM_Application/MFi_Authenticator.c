/*HEADER******************************************************************************************
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
*
**************************************************************************************************
**************************************************************************************************
*
* Comments:
*       File Name : MFi_Authenticato.c 
*
**END********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "MFi_Authenticator.h"
#include "stdio.h"
#include "miscFunctions.h"
/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
sSM gTestSuite_SM;      /*Control the Test Suite States*/
uint8_t gu8iOSDeviceDetectedFlag = FALSE;
uint8_t gu8ActualIDCable = NO_CABLE_DETECTED_ID;
uint8_t gu8OnGoingTest = FALSE;
uint8_t gu8StillWaitingForDevice = 0;
uint16_t gu16TrackCounterPrv;
int32_t g_retry = 0;
uint32_t gSubTestNo = 0;
uint32_t gEaTestNo = 0;
uint32_t g_InfoFlag = 0;
/*************************************************************************************************/
/*                                   Extern Variables Section                                    */
/*************************************************************************************************/
extern uint16_t gu16TrackCounter;
extern uint8_t *gu8pMLName;

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/
/* Function Pointer to Machine states */
void (*const vptrfnTestSuiteStateMachine[])(void) =      /* Test Suite State Machine */
{
    vfnWaitForAppleDevice,		/*0*/
    vfnDetectiAPProtocol,		/*1*/
    vfnDetectCommunication,
    vfnDetectCable,				/*3*/
    vfnIdle,
    vfnClearInitScreen,			/*5*/
};

/****************************************************************************************************/
/*                                         Functions Section                                        */
/****************************************************************************************************/



/*************************************************************************************************
* @name     vfnDetectiAPProtocol
*
* @brief    Set an event to actualise information logging with the current protocol       
*           
* @param   	None
*
* @return  	None  
*
**************************************************************************************************/
void vfnDetectiAPProtocol(void)
{
    TestSuite_res_en_Protocol[10] = 'i';
    TestSuite_res_en_Protocol[11] = 'A';
    TestSuite_res_en_Protocol[12] = 'P';                         

#if MFI_IAP2_ENABLE
    if(iAP2_ACTIVE)
    {
        TestSuite_res_en_Protocol[13] = '2';
    }
    else
#endif
    {
        TestSuite_res_en_Protocol[13] = '1';		
    }

    gTestSuite_SM.ActualState = TS_DETECT_TRANSPORT_STATE;
    gTestSuite_SM.PrevState = TS_DETECT_IAP_PROTOCOL_STATE;
}

/*************************************************************************************************
* @name     vfnWaitForAppleDevice
*
* @brief    Wait until an apple device is attached    
*           
* @param   	None
*
* @return  	None  
*
**************************************************************************************************/
void vfnWaitForAppleDevice(void)
{
#if MFI_IAP2_ENABLE && MFI_IAP1_ENABLE
    if((IAP_HOST_CHECK_STATUS_REGISTER(IAPH_APP_CMD_ALLOWED)) || (IAP2_HOST_MSG_ALLOWED))
#else
#if MFI_IAP2_ENABLE
    if(IAP2_HOST_MSG_ALLOWED)
#else /*MFI_IAP1_ENABLE*/
    if(IAP_HOST_CHECK_STATUS_REGISTER(IAPH_APP_CMD_ALLOWED))
#endif
#endif
    {
        gu8iOSDeviceDetectedFlag = TRUE;
        gTestSuite_SM.ActualState = TS_DETECT_IAP_PROTOCOL_STATE;
    }
    else
    {
        if (gu8StillWaitingForDevice == 0)
        {
            PRINTF("\r\n\r\nWaiting for Apple device connection...\r\n\r\n");
            gu8StillWaitingForDevice = 1;
        }
    }
    gTestSuite_SM.PrevState = TS_WAIT_FOR_IOS_DEVICE_STATE;
}


/*************************************************************************************************
* @name     vfnDetectCommunication
*
* @brief    Ask for the current transport and set event to actualise screen
*           
* @param   	None
*
* @return  	None
*
**************************************************************************************************/
void vfnDetectCommunication(void)
{
    vfnGetMFiTransport();

    gTestSuite_SM.ActualState = TS_DETECT_CABLE_STATE;
    gTestSuite_SM.PrevState = TS_DETECT_TRANSPORT_STATE;
}

/*************************************************************************************************
* @name     vfnDetectCable
*
* @brief    If no custom cable is detected, notify that Standard Cable is used and set event 
*           to actualise information logging
*
* @param   	None
*
* @return  	None
*
**************************************************************************************************/
void vfnDetectCable(void)
{
    if(gu8ActualIDCable == NO_CABLE_DETECTED_ID)
    {
        vfnCopyBuffInfo((uint8_t*) &gau8AppleStandardCable[0],
        (uint8_t*) &TestSuite_res_en_Cable[CABLE_CHAR_INIT], CABLE_CHAR_NUM);
    }
    
    gTestSuite_SM.ActualState = TS_IDLE_STATE;

    gTestSuite_SM.PrevState = TS_DETECT_CABLE_STATE;
}


/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
