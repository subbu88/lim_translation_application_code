/*HEADER******************************************************************************************

************************************************************************************************** 
*
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
* Source Code provided by NXP - INDIA
**************************************************************************************************
*
* Comments:
*       File Name : main.c 
*
**END********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "stdio.h"
#include "string.h"
#include "stdbool.h"
#include "ProjectTypes.h"
#include "iAP2_Host.h"
#include "Module_Controller.h"
#include "iAP2_MediaLibraryAccess_Feat.h"
#include "GPIO_lib.h"
#include "Transducer_Detect_Logic.h"
#include "Logger.h"
#include <libusb-1.0/libusb.h>//subbu_test
/*************************************************************************************************/
/*                                   Extern Functions Section                                    */
/*************************************************************************************************/

extern bool iAP_init_failed;
/*************************************************************************************************/
/*                                   Type Define Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Stactic Variabe Section                                    */
/*************************************************************************************************/
static bool iap1_ready;

/*************************************************************************************************/
/*                                   Define Section                                    */
/*************************************************************************************************/
#define MFI_AUTH_I2C_ADDRESS 0x11
#define MFI_AUTH_I2C_DEVICE "/dev/i2c-1"
#define MFI_AUTH_HID_DEVICE "/dev/usb/hiddev0"
#define IAP_INIT_MAX_WAIT_TIME 200
#define IAP_INIT_FAILED (iAP_init_failed)

#define LIM_Translation_Application_Ver 1.01

/*************************************************************************************************/
/*                                      Global Variable Section                              */
/*************************************************************************************************/
int32_t g_waittime = 0; 
OsaMutex Mutex_Handle;
/*************************************************************************************************/
/*                                      Prototype Functions Section                              */
/*************************************************************************************************/
int32_t mfi_init();

/*************************************************************************************************/
/*                                      Extern Functions Section                              */
/*************************************************************************************************/
extern int32_t LIM_Translation_Application(void);
extern uint8_t iap_host_register_device_ready_callback(void(*Callback)(void));
//extern void vfnWaitForTestToEnd();

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

static void _iap1_ready(void)
{
    iap1_ready = true;
}
/*************************************************************************************************
* @name     main
*
* @brief    LIM Translation App main entry level function.
*
* @param   	none     
*
* @return  	SUCCESS
*
**************************************************************************************************/
int32_t main(void)
{

    printf("\n LIM Translation Application Start \n");
	printf("\n Version : %.2f \n", LIM_Translation_Application_Ver);
    
    (void)mfi_init();
    
    /* LIM Translation Application Start */
    LIM_Translation_Application();
    
    /* Never leave main */
    return 0;
}
//Hot_plug_USB_notification_implementation Test_start

int done = 0;
libusb_device_handle *handle = NULL;
static OsaThread  Detection_thread_id;

static int LIBUSB_CALL hotplug_callback(libusb_context *ctx, libusb_device *dev, libusb_hotplug_event event, void *user_data)
{
	struct libusb_device_descriptor desc;
	int rc;

	rc = libusb_get_device_descriptor(dev, &desc);
	if (LIBUSB_SUCCESS != rc) {
		fprintf (stderr, "Philips Lumify Error getting device descriptor\n");
	}

	printf ("Philips Lumify Device attached: %04x:%04x\n", desc.idVendor, desc.idProduct);

	if (handle) {
		libusb_close (handle);
		handle = NULL;
	}

	rc = libusb_open (dev, &handle);
	if (LIBUSB_SUCCESS != rc) {
		fprintf (stderr, "Philips Lumify Error opening device\n");
	}

	done++;

	return 0;
}

static int LIBUSB_CALL hotplug_callback_detach(libusb_context *ctx, libusb_device *dev, libusb_hotplug_event event, void *user_data)
{
	printf ("Philips Lumify Device detached\n");

	if (handle) {
		libusb_close (handle);
		handle = NULL;
	}

	done++;

	return 0;
}
void Detect_Transducer()
{
	while (1) {
		if(libusb_handle_events(NULL) < 0) {
			printf("Hotplug Detection Error");
		}
	}
}

void Test_hot_plug()
{
	libusb_hotplug_callback_handle hp[2];
	int product_id, vendor_id, class_id;
	int rc;

	vendor_id  = 0x0F36;
	product_id = 0x9006;
	class_id   = LIBUSB_HOTPLUG_MATCH_ANY;

	rc = libusb_init (NULL);
	if (rc < 0)
	{
		printf("Philips Lumify failed to initialise libusb: %s\n", libusb_error_name(rc));
	}

	if (!libusb_has_capability (LIBUSB_CAP_HAS_HOTPLUG)) {
		printf ("Philips Lumify Hotplug capabilites are not supported on this platform\n");
	}

	rc = libusb_hotplug_register_callback (NULL, LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED, 0, vendor_id,
		product_id, class_id, hotplug_callback, NULL, &hp[0]);
	if (LIBUSB_SUCCESS != rc) {
		fprintf (stderr, "Philips Lumify Error registering callback 0\n");
	}

	rc = libusb_hotplug_register_callback (NULL, LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT, 0, vendor_id,
		product_id,class_id, hotplug_callback_detach, NULL, &hp[1]);
	if (LIBUSB_SUCCESS != rc) {
		fprintf (stderr, "Philips Lumify Error registering callback 1\n");
	}

#if 0
	while (done < 2) {
		rc = libusb_handle_events (NULL);
		if (rc < 0)
			printf("libusb_handle_events() failed: %s\n", libusb_error_name(rc));
	}
#endif
	if(osa_thread_create(&Detection_thread_id, NULL, Detect_Transducer, NULL) != 0) {
		printf("Error while starting Transducer hotplug detection thread ");
		
	}

}

//Hot_plug_USB_notification_implementation Test_End
/*************************************************************************************************
* @name     mfi_init
*
* @brief    MFi Stack Initialization.
*
* @param   	none     
*
* @return  	SUCCESS
*
**************************************************************************************************/
int32_t mfi_init()
{
    Initialize_Module_Controller();
	Init_Transducer_Detection_Logic();
	Test_hot_plug();//subbu_test
    MFIHostConfig config;
	int32_t status;
	uint32_t init_wait_time = 0;
    int32_t MutexStatus; 


	iap1_ready = false;
	printf("\n iap_host_register_device_ready_callback called \n");
	iap_host_register_device_ready_callback(_iap1_ready);

	strncpy(config.hid_dev, MFI_AUTH_HID_DEVICE, 32);
	strncpy(config.i2c_dev, MFI_AUTH_I2C_DEVICE, 32);
	config.i2c_addr = MFI_AUTH_I2C_ADDRESS;

    //MutexStatus = osa_mutex_create(&Mutex_Handle, true);//Mutex Recrussive
    MutexStatus = osa_mutex_create(&Mutex_Handle, false);
    if(MutexStatus != 0)
        printf(" Not able to create Mutex handle - error = %d \n", MutexStatus);

    //osa_mutex_destroy(&Mutex_Handle);   //subbu_mutex - TBD

	/* Logger Test Start */
	Init_Logging_System();
	/* Logger Test End */

	//Gpio test Start
	#if 0
	if(0 != gpio_export(4))
		printf(" GPIO 4 export Failed\n");
		//pin num, direction  (1 = out, 0 = in)
	if( 0 != gpio_direction(4, 1))
		printf(" GPIO 4 set out direction Failed");

	
	if(0 != gpio_export(0))
		printf(" GPIO 4 export Failed\n");
		//pin num, direction
	if( 0 != gpio_direction(0, 0))
		printf(" GPIO 4 set in direction Failed");

	#endif
	//Gpio test end
      
    printf("\niAP2_Host_bfnHostInitialization called\n");
	status = iAP2_Host_bfnHostInitialization(&config);
	if (status)
		return 0x10;

	/* Wait for IAP Authentication and Identication to complete. */
	while (!IAP2_HOST_MSG_ALLOWED && !IAP_INIT_FAILED && !iap1_ready) {
		osa_time_delay(100);
		init_wait_time++;
		if (init_wait_time > IAP_INIT_MAX_WAIT_TIME)
		{
			/*
				extern uint16_t iAP2_Host_gwStatus;
				extern bool iAP_init_failed;
			*/
			printf("iAP2_Host_gwStatus = %d \n",iAP2_Host_gwStatus);
			printf("iAP_init_failed = %d \n",iAP_init_failed);
			WriteProtocolData(MFI_FAILURE, 0, 0);
			return 0x10;
		}
	}

	if (IAP_INIT_FAILED) {
		WriteProtocolData(MFI_FAILURE, 0, 0);
		printf("iAP Host Initialization failed\n");
		return 0x10;
	}

	printf("iAP Host Initialization complete\n");

	return 0;
}


/*************************************************************************************************/
/*                                      Stub Functions Section                                        */
/*************************************************************************************************/
void iAP2_DA_Callback(uint16_t sample_rate)
{
    /* Stub */
}

void User_NowPlayingUpdate_Callback(uint16_t MessageID)
{
    /* Stub */
}

void iAP_TrackNewAttributes(uint32_t sample_rate)
{
    /* Stub */
}

void AssistiveTouch_UpdateCallback(uint8_t bAssistiveTouchStatus)
{
	/* Stub */
}

void vfnFileTransferStatus(uint8_t bFileTransferId, uint8_t bTransferStatus, uint8_t* bpUpperLayerStatus)
{
	/* Stub */
}
void vfnFileTransferSetUp(uint32_t dwFileTransferSize, uint8_t bFileTransferId)
{
	/* Stub */
}

void vfnFileTransferData(uint16_t wFileTransPacketSize, uint8_t wFileTransferID, void** bppStoreData, 
                uint16_t *wpArrayMaxCapacity)
{
	/* Stub */
}

void vfnTS_MediaLibrary_UpdateCallback(void)
{
	/* Stub */
}
void vfnTS_MediaLibrary_InfoCallback(iAP2_MediaLibraryInfoData *spMediaLibrayInfo,
                  iAP2_MediaLibraryInfoSize *spMediaLibrayInfoSize)
{
	/* Stub */
}

/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
