/*HEADER******************************************************************************************
*
* Copyright 2018 L&T Technology Services Limited, Mysore- India.
*
**************************************************************************************************

**************************************************************************************************
*
* Comments:
*       File Name : GPIO_lib.h 
*
**END********************************************************************************************/

#ifndef _GPIO_LIB_H_
#define _GPIO_LIB_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
#if defined(__cplusplus)
extern "C" {
#endif

/* returns -1 or the file descriptor of the gpio value file */
int gpio_export(unsigned int gpio);
/* Set direction to 2 = high output, 1 low output, 0 input */
int gpio_direction(unsigned int gpio,unsigned int dir);
/* Release the GPIO to be claimed by other processes or a kernel driver */
void gpio_unexport(unsigned int gpio);
/* Single GPIO read */
int gpio_read(unsigned int gpio);
/* Set GPIO to val (1 = high) */
int gpio_write(unsigned int gpio,unsigned int val);

#if defined(__cplusplus)
}
#endif /* __cplusplus*/

#endif /* _GPIO_LIB_H_ */







/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
