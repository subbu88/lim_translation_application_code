/*******************************************************************************************
*
* Copyright 2018 L&T Technology Services Limited, Mysore India
*
**************************************************************************************************
**************************************************************************************************
*
* Comments:
*       File Name : Module_Controller.c
*
**END********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <fcntl.h>   /* File Control Definitions           */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions 	   */ 
#include <errno.h>   /* ERROR Number Definitions           */
#include "Module_Controller.h"
#include "osa_common.h"
#include "crc8.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/

#define DEFAULT_ 				0
#define START_OF_PACKET     	1
#define COMMAND_TYPE_PACKET     2
#define DATA_LENGTH_PACKET		3
#define PAYLOAD_DATA_PACKET		4
#define CHECKSUM_PACKET			5

#define SOP_VALUE 					0xFF

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

S_BatteryInfo_t stBatteryInfo;


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static int fd1;/* File Descriptor for UATR2 */
static OsaThread  monitor_thread_id;
static S_ModuleControllerProtocol_t	st_ModuleControllerProtocol;


/*************************************************************************************************/
/*                                 Volatile Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
 
static void SerialReceive( uint8_t* pDataBuffer, uint8_t ucNoOfBytes);
void (*void_fun_ptr)(uint8_t* , uint8_t); 
static int Module_Controller_Read_thread();
static void ReadFromModuleControllerThread();

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
void Initialize_Module_Controller()
{
	
	printf("\n +----------------------------------+ \n");
	printf("\n UART2 Serial Port Initializing \n");
	printf("\n +----------------------------------+ \n");

	/*------------------------------- Opening the Serial Port -------------------------------*/

	/* Open /dev/ttyS2 Communication port */
	fd1 = open("/dev/ttymxc1",O_RDWR | O_NOCTTY | O_NDELAY);/* ttymxc1 is the UART2 Comm Port   */
								/* O_RDWR Read/Write access to serial port*/
								/* O_NOCTTY - No terminal will control the process */
								/* O_NDELAY -Non Blocking Mode,Does not care about-  */
					/* -the status of DCD line,Open() returns immediatly */                                     
									
	if(fd1 == -1)	/* Error Checking */
   	 printf("\n  Error! in Opening Module Controller UART port-ttymxc1  ");
	else
   	 printf("\n  Module Controller UARTt port tymxc1 Opened Successfully ");

	/*---------- Setting the Attributes of the serial port using termios structure --------- */
		
		struct termios SerialPortSettings;		/* Create the structure                          */
		tcgetattr(fd1, &SerialPortSettings);		/* Get the current attributes of the Serial port */
		cfsetispeed(&SerialPortSettings,B115200); /* Set Read  Speed as 115200                       */
		cfsetospeed(&SerialPortSettings,B115200); /* Set Write Speed as 115200                      */
		SerialPortSettings.c_cflag &= ~PARENB;   /* Disables the Parity Enable bit(PARENB),So No Parity   */
		SerialPortSettings.c_cflag &= ~CSTOPB;   /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
		SerialPortSettings.c_cflag &= ~CSIZE;	 /* Clears the mask for setting the data size             */
		SerialPortSettings.c_cflag |=  CS8;      /* Set the data bits = 8                                 */
		
		SerialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
		SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */ 
		
		SerialPortSettings.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
                                 INLCR | PARMRK | INPCK | ISTRIP | IXON);

		SerialPortSettings.c_oflag  = 0;/*No Output Processing*/

		 SerialPortSettings.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

		/* Setting Time outs */ //subbu_test and fix
		SerialPortSettings.c_cc[VMIN] = 5; /* Read at least 10 characters */
		SerialPortSettings.c_cc[VTIME] = 0; /* Wait indefinetly */

		if((tcsetattr(fd1,TCSANOW,&SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
		    printf("\n  ERROR ! in Module Controller UART Setting attributes");
		else
            printf("\n  BaudRate = 115200 \n  StopBits = 1 \n  Parity   = none");

 	tcflush(fd1, TCIFLUSH);   /* Discards old data in the rx buffer            */					
		
	if(fd1 != -1)	/* Error Checking */
	{
		void_fun_ptr = &SerialReceive;	        
		Module_Controller_Read_thread();
	}

	//InitCrc8();//Initialize CRC8 lookup table
	//close(fd1);/* Close the Serial port */ //subbu take care
	printf("Moodule controller UART Init End \n");
}


int WriteProtocolData(uint8_t Cmd, const uint8_t* parrucData, uint8_t ucNoOfBytes )
{	
	int count = 0;
	uint8_t CalcCRC8 = 0;
	uint8_t SendBuffer[10] = {0};
	SendBuffer[0] = SOP_VALUE;
	SendBuffer[1] = Cmd;
	SendBuffer[2] = ucNoOfBytes;
	for(count = 0; count < ucNoOfBytes; count++)
	{
		SendBuffer[(3+count)] = parrucData[count];
	}

	CalcCRC8 = crc8(0, SendBuffer, (ucNoOfBytes + 3));
	printf(" UART sending CRC8 = %d \n", CalcCRC8);
	//ComputeCrc8(SendBuffer, (ucNoOfBytes + 3), &CalcCRC8);
	SendBuffer[(3 + ucNoOfBytes)] = CalcCRC8;

#if 0
	for(int ik = 0; (ik < (ucNoOfBytes+4)); ik++)
	{
	printf("Suma UART Debug value = %d \n", SendBuffer[ik]);
	}
#endif

	printf("Writing data to Module Controller \n");
	/*------------------------------- Write data to serial port -----------------------------*/
	
	//char write_buffer[] = {0xff,0x01,0x02,0x76,0x77,0xff};
	int  bytes_written  = 0;  	/* Value for storing the number of bytes written to the port */ 
	bytes_written = write(fd1,SendBuffer,(ucNoOfBytes + 4 ));/* use write() to send data to port */
								     /* "fd1"-file descriptor pointing to the opened serial port */
								     /*"parrucData"- address of the buffer containing data	*/
								     /* "ucNoOfBytes" - No of bytes to write */	
	
	printf("\n  %d Bytes written to Module Controller UART \n", bytes_written);
	return (bytes_written);
}



//Basic Writing Functionality
int WriteToModuleController( const uint8_t* parrucData, uint8_t ucNoOfBytes )
{
	//printf("Writing data to Module Controller \n");
	/*------------------------------- Write data to serial port -----------------------------*/
	
	//char write_buffer[] = {0xff,0x01,0x02,0x76,0x77,0xff};
	int  bytes_written  = 0;  	/* Value for storing the number of bytes written to the port */ 
	bytes_written = write(fd1,parrucData,ucNoOfBytes);/* use write() to send data to port */
								     /* "fd1"-file descriptor pointing to the opened serial port */
								     /*"parrucData"- address of the buffer containing data	*/
								     /* "ucNoOfBytes" - No of bytes to write */	
	
	//printf("\n  %d Bytes written to Module Controller UART", bytes_written);
	return (bytes_written);
}

int ReadFromModuleController( uint8_t* parrucData, uint8_t ucNoOfBytes )
{
  /*------------------------------- Read data from serial port -----------------------------*/
 		tcflush(fd1, TCIFLUSH);   /* Discards old data in the rx buffer            */
		int  bytes_read = 0;    /* Number of bytes read by the read() system call */
 		int i = 0;
 		bytes_read = read(fd1,&parrucData,ucNoOfBytes); /* Read the data                   */
			
		printf("\n\n  Bytes Rxed -%d", bytes_read); /* Print the number of bytes read */
		printf("\n\n  ");
 		for(i=0;i<bytes_read;i++)	 /*printing only the received characters*/
		    printf("%c",parrucData[i]);
return bytes_read;

}

/************* Update Information ***************/
void UpdateBatteryInfo(S_ModuleControllerProtocol_t * psModuleControllerProtocol)
{
	printf(" UpdateBatteryInfo Entry \n ");
	switch(psModuleControllerProtocol->CmdType)
	{
		case BATTERY_INFORMATION:
		{
			memcpy(&stBatteryInfo, &psModuleControllerProtocol->PayloadData[0], sizeof (psModuleControllerProtocol->DataLen));
		} 
	 		break;
		default:
			break;
	}
}

static void SerialReceive(uint8_t* pDataBuffer, uint8_t ucNoOfBytes)
{
	printf("MC Serial Receive Entry \n");
	#if 0
	int i;
	for(i = 0; i < ucNoOfBytes; i++)
	{
		printf("UART rec Data = %d \n",*pDataBuffer);
		kparrucData++;
	}
	#endif
/********** Module Controller Deserializer Code Start*******************/

	uint16_t count = 0;
	static uint8_t SerialFlow = START_OF_PACKET;
	uint8_t ucComputedCRC = 0x00;
				
	for(count = 0; count < ucNoOfBytes; count++)
	{//Begin for loop
		switch(SerialFlow)
		{
			case START_OF_PACKET:
			{
				st_ModuleControllerProtocol.StartOfPacket = pDataBuffer[count];
				if(SOP_VALUE == pDataBuffer[count])
					SerialFlow = COMMAND_TYPE_PACKET;
				else
					SerialFlow = START_OF_PACKET;
			}
				break;
			case COMMAND_TYPE_PACKET:
			{	
				st_ModuleControllerProtocol.CmdType = pDataBuffer[count];
				SerialFlow = DATA_LENGTH_PACKET;
			}
				break;
			case DATA_LENGTH_PACKET:
			{
				st_ModuleControllerProtocol.DataLen = pDataBuffer[count];
				SerialFlow = PAYLOAD_DATA_PACKET;
			}
				break;
			case PAYLOAD_DATA_PACKET:
			{
				memcpy(&(st_ModuleControllerProtocol.PayloadData), &pDataBuffer[count],st_ModuleControllerProtocol.DataLen);
				count += ((st_ModuleControllerProtocol.DataLen) - 1);
				SerialFlow = CHECKSUM_PACKET;
			}
				break;
			case CHECKSUM_PACKET:
			{
				#if 0
				ComputeCrc8(pDataBuffer,
						((st_ModuleControllerProtocol.DataLen) + 3),
						&ucComputedCRC);
				#endif
				ucComputedCRC = crc8(0, pDataBuffer, ((st_ModuleControllerProtocol.DataLen) + 3));
				 printf(" UART received calc - CRC8 = %d \n", ucComputedCRC);
				st_ModuleControllerProtocol.RecCheckSum = pDataBuffer[count];

				if(ucComputedCRC == st_ModuleControllerProtocol.RecCheckSum)
				{
					printf(" +++++ crc8 Matching ++++++ \n");
					//Update information in local buffer based on the commands received
					UpdateBatteryInfo(&st_ModuleControllerProtocol);
				}
				else
					printf("crc8 mismatch while received packet from microcontroller \n");

				memset (&st_ModuleControllerProtocol,0,sizeof(S_ModuleControllerProtocol_t));
				SerialFlow = START_OF_PACKET;
			}
			break;
			default:
				break;

		}//End of switch
	}//End of for loop
/********** Deserializer code end ********************/
}//End of SerialReceive()

static void ReadFromModuleControllerThread()
{
	int ucNoOfBytesRead = 0;
	uint8_t read_buffer[32];   /* Buffer to store the data received              */
	uint8_t ucNoOfBytes = 32;//Verify_TBD
	printf(" UART2 Read Thread Entry \n" );

	while(fd1)
	{
		ucNoOfBytesRead = 0;
		
		//read from the device
		ucNoOfBytesRead = read(fd1,&read_buffer,ucNoOfBytes); /* Read the data   */ 
		if(ucNoOfBytesRead > 0)
		{
			printf("Bytes received from Moudule Controller UART = %d \n", ucNoOfBytesRead);
			tcflush(fd1, TCIFLUSH);   /* Discards old data in the rx buffer */
			if(void_fun_ptr)
			{
				//Call Call Back Function to read and decode as per the protocol
				(*void_fun_ptr) ( (uint8_t*)read_buffer, ucNoOfBytesRead );
			
				if( !ucNoOfBytes )
				{
				//Sleep(m_nTimeOutInMilliSec); 
				}	 
			}
			else
			{
				printf("\n UARTCommn Callback Function NULL Pointer\n"); 
			}
			memset (&read_buffer,0,ucNoOfBytes);
		}
		//usleep( 300 * 1000);//300 milli sec sleep
		//usleep(1000);
		osa_time_delay(300);//300 milli sec thread
	}//End of while(1)
}//End of ReadFromModuleControllerThread()



static int Module_Controller_Read_thread()
{
    printf("Starting Module Controller Read thread \n ");
	if(osa_thread_create(&monitor_thread_id, NULL, ReadFromModuleControllerThread, NULL) != 0) {
		printf("Error while starting Module Controller Read thread ");
		return -1;
	}
	return 0;
}

/******* Module Controller UART Application End  ********/

