/*HEADER******************************************************************************************
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
************************************************************************************************** 
**************************************************************************************************
*
* Comments:
*       File Name : MFi_TestSuite_Demo.h 
*
**END********************************************************************************************/

#ifndef MFI_TESTSUITE_DEMO_H_
#define MFI_TESTSUITE_DEMO_H_

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus*/

/* initial setup APIs*/
void vfnWaitForAppleDevice(void);
void vfnDeactivateVoiceOver(void);
void vfnDetectCable(void);
void vfnDetectiAPProtocol(void);
void vfnDetectCommunication(void);
void vfnIdle(void);
void vfnClearInitScreen(void);

/* application state machine for various tests*/
void PerformLIMTranslationAppTasks(void);


#if defined(__cplusplus)
}
#endif /* __cplusplus*/

#endif /* MFI_TESTSUITE_DEMO_H_ */
/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
