/*******************************************************************************************
*
* Copyright 2018 L&T Technology Services Limited, Mysore India
*
**************************************************************************************************
**************************************************************************************************
*
* Comments:
*       File Name : Module_Controller.c
*
**END********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/stat.h>
#include "GPIO_lib.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/



/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/




/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                 Volatile Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
 


/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

int gpio_direction(unsigned int gpio, unsigned int dir)
{
	int ret = 0;
	char buf[50];
	sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio);
	int gpiofd = open(buf, O_WRONLY);
	if(gpiofd < 0) 
	{
		printf("Couldn't open IRQ file");
		ret = -1;
	}
	if(dir == 2 && gpiofd)
	{
		if (3 != write(gpiofd, "high", 3)) 
		{
			printf("In high Couldn't set GPIO direction to out \n");
			ret = -2;
		}
	}

	if(dir == 1 && gpiofd)
	{
		if (3 != write(gpiofd, "out", 3)) 
		{
			printf("Couldn't set GPIO direction to out \n");
			ret = -3;
		}
	}
	else if(gpiofd) 
	{
		if(2 != write(gpiofd, "in", 2))
		{
			printf("Couldn't set GPIO directio to in \n");
			ret = -4;
		}
	}
	close(gpiofd);
	return ret;
}

int gpio_export(unsigned int gpio)
{
	int efd;
	char buf[50];
	//int gpiofd; 
	int ret;

	/* Quick test if it has already been exported */
	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
	efd = open(buf, O_WRONLY);
	if(efd != -1) 
	{
		close(efd);
		return 0;
	}
	efd = open("/sys/class/gpio/export", O_WRONLY);
	if(efd != -1) 
	{
		sprintf(buf, "%d", gpio);
		ret = write(efd, buf, strlen(buf));
		if(ret < 0) 
		{
			printf("Gpio %d Export failed \n",gpio);
			return -2;
		}
		close(efd);
	} 
	else 
	{
		// If we can't open the export file, we probably
		// dont have any gpio permissions
		return -1;
	}
	return 0;
}

void gpio_unexport(unsigned int gpio)
{
	int gpiofd;
	int ret;
	char buf[50];
	gpiofd = open("/sys/class/gpio/unexport", O_WRONLY);
	sprintf(buf, "%d", gpio);
	ret = write(gpiofd, buf, strlen(buf));
	if(ret)
	{}
	close(gpiofd);
}

int gpio_read(unsigned int gpio)
{
	char in[3] = {0, 0, 0};
	char buf[50];
	int nread, gpiofd;
	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
	gpiofd = open(buf, O_RDWR);
	if(gpiofd < 0) 
	{
		fprintf(stderr, "Failed to open gpio %d value\n", gpio);
		printf("gpio failed");
	}
	do 
	{
		nread = read(gpiofd, in, 1);
	} while (nread == 0);
	if(nread == -1)
	{
		printf("GPIO Read failed");
		return -1;
	}
	close(gpiofd);
	return atoi(in);
}

int gpio_write(unsigned int gpio,unsigned int val)
{
	char buf[50];
	int ret, gpiofd;
	//int nread;
	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
	gpiofd = open(buf, O_RDWR);
	if(gpiofd > 0) 
	{
		snprintf(buf, 2, "%d", val);
		ret = write(gpiofd, buf, 2);
		if(ret < 0)
		{
			printf("failed to set gpio \n");
			return 1;
		}
		close(gpiofd);
		if(ret == 2) 
			return 0;
	}
	return 1;
}

/******* Module Controller UART Application End  ********/

