/*HEADER******************************************************************************************
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
**************************************************************************************************
*
* Comments:
*       File Name : Packet_Manager.h 
*
**END********************************************************************************************/
#ifndef PACKET_MANAGER_H_
#define PACKET_MANAGER_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define EA_DEMO_BUFF_SIZE         2048 * 8//2048
#define EA_DEMO_HANDSHAKE_SIZE    5
#define EA_DEMO_TESTS_ACTIVE      1

typedef struct
{
	uint8_t EndPointNum;   		// endpoint number where this transfer will be sent
	uint8_t DataDirection; 		// i believe ReqType itself data direction TBD
	uint8_t ReqType;       	   	// The request type field for the setup packet 
	uint8_t Request;	   		// The request field for the setup packet
	uint16_t value;		   		// The value field for the setup packet 
	uint16_t Index;		   		// The index field for the setup packet 
	uint16_t Timeout;	   		/* Timeout (in millseconds) that this function should wait 						before giving up due to no response being received. 
								For an unlimited timeout, use value 0. */
	uint16_t Reserved;	   		// For Future USE
	uint16_t DataLength;   		/* The length field for the setup packet. The data buffer 						should be at least this size */
}sLIMPacketHeader;

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
#if defined(__cplusplus)
extern "C" {
#endif




#if defined(__cplusplus)
}
#endif /* __cplusplus*/

#endif /* PACKET_MANAGER_H_ */
/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
