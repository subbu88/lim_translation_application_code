/*HEADER******************************************************************************************
*
* Copyright 2016 Freescale Semiconductor, Inc.
*
**************************************************************************************************
*
* Comments:
*       File Name : rootWindow.h 
*
**END********************************************************************************************/

#ifndef __ROOTWINDOW_H__
#define __ROOTWINDOW_H__

#ifndef KSDK_SUPPORT
/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "MFi_Config.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define TRANSPORT_CHAR_INIT    			(15)
#define TRANSPORT_CHAR_NUM    			(15)
#define CABLE_CHAR_INIT       			(7)
#define CABLE_CHAR_NUM        			(20)

#define RELEASE_BUTTON_EVENT    		(0x4001)
#define TOUCH_BUTTON_EVENT      		(0x4000)

#define TIMER_ID 						(1)
#define PRESS_TIME_MASK					(60)
#define SCAN_QUANTITY 					(10000)
#define SCAN_DELAY 						(20)

#define TOUCH_TRIGGER_NUM    			(260)  /* [number * 150us = 40ms] */

#define USB_ROLL_SWITCH_DONE   			(5)

#define I2C_PCA9672_SLAVE_ADRESS 		(0x23)

#define LIGHTNING_TO_USB_DEVICE_ID  	(0xF7)
#define LIGHTNING_TO_USB_HOST_ID    	(0xFB)
#define LIGHTNING_TO_SERIAL_ID      	(0xF3)
#define THIRTY_PIN_CONNECTOR_ID     	(0xEF)
#define NO_CABLE_DETECTED_ID        	(0xFF)

#define RW_CR_TOUCH_ADRESS 				(0x49)

/*************************************************************************************************/
/*                                   Typedef & enum Section                                      */
/*************************************************************************************************/
/*Following defines are used to set flag events to make changes on the screen*/
enum
{
    TS_IAP_PROTOCOL_EVENT,
    TS_TRANSPORT_EVENT,	
    TS_CABLE_EVENT,
    TS_WRONG_CABLE_EVENT,
};

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern unsigned char TestSuite_res_en_Communication[];
extern unsigned char TestSuite_res_en_Cable[];
extern unsigned char TestSuite_res_en_Protocol[];
extern char TestSuite_res_en_mTestTitle[]; 
extern char TestSuite_res_en_resultPrompt[80];
extern char TestSuite_res_en_EAText[256];
extern unsigned char TestSuite_res_en_MFi_Version[];

#if (MFI_TRANSPORT_UART == TRUE) || (MFI_TRANSPORT_USB_HOST_MODE == TRUE) || (MFI_TRANSPORT_USB_DEVICE_MODE == TRUE)
extern char TestSuite_res_en_Title[];
#endif

extern sSM gTestSuite_SM;   

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Declaration                                         */
/*************************************************************************************************/
#if defined(__cplusplus)
extern "C" {
#endif

int32_t displayMediaPlayerTestMenu();

#if defined(__cplusplus)
}
#endif

#endif /* KSDK_SUPPORT    */
#endif /* __ROOTWINDOW_H__ */

/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
