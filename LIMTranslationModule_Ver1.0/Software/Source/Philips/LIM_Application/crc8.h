#ifndef CRC8_H_	
#define CRC8_H_

#if defined(__cplusplus)
extern "C" {
#endif

#if 0
void InitCrc8();
int ComputeCrc8(const uint8_t* pAddress, const uint8_t ucLength,
				 uint8_t* PComputedCRC);
#endif

unsigned char crc8(unsigned crc, unsigned char const *data, size_t len);				 

#if defined(__cplusplus)
}
#endif /* __cplusplus*/

#endif

