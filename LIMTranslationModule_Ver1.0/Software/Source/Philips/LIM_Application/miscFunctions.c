/*HEADER******************************************************************************************
* Philips Lumify Interface Module(LIM) Translation Application
* Code modified by L&T Technology Services, Mysore INDIA
* Modified Date : 06-Sep-2018
*
**************************************************************************************************

**************************************************************************************************
*
* Comments:
*       File Name : miscFunctions.c
*
**END********************************************************************************************/


/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "iAP2_Host.h"
//#include "playerwindow.h"
#include "miscFunctions.h"


#define STRING_MAX_SIZE          	(64)
/*************************************************************************************************/
/*                                   Constant Variables Section                                    */
/*************************************************************************************************/
const uint8_t gau8empty[21] = {"                    "};

const uint8_t gau8USBDeviceTrans[16]={"USB DEVICE     "};
const uint8_t gau8USBRollSwitchTrans[16]={"USB ROLL SWITCH"};
const uint8_t gau8USBHostTrans[16]={"USB HOST       "};
const uint8_t gau8UartTrans[16]={"UART           "};

const uint8_t gau8USBLigthingHostCable[21] = {"Lightning USB Host  "}; 	/*ID0*/
const uint8_t gau8USBLigthingDeviceCable[21] = {"Lightning USB Device"}; /*ID1*/
const uint8_t gau8USBLigthingSerialCable[21] = {"Lightning Serial    "}; /*ID0 ID1 */
const uint8_t gau8USB30PinCable[21] = {"30-pin Connector    "};          /*ID2*/
const uint8_t gau8AppleStandardCable[21] = {" Standard USB Cable "};  	/*NC ID*/

const uint8_t gau8SelfTest[19]         ={"    SELF  TEST    "};
const uint8_t gau8MedLibTest[19]       ={"MEDIA LIBRARY TEST"};
const uint8_t gau8FileTransferTest[19] ={"FILE TRANSFER TEST"};

const uint8_t gau8iAPError[] = {"Not Available for iAP1"};
const uint8_t gau8StartTest[] ={"Please Press Start"};
const uint8_t gau8TestInProgress[] ={"Test in Progress"};
const uint8_t gauTrack[]={" tracks"};

const uint8_t gau8WaitingMessage[] = {"Waiting iOS App Message"};

const uint8_t gau8IDTranfer[] = {"Transfer ID: "};
const uint8_t gau8PackageSize[]= {" Package Size: "};
const uint8_t gau8TransProgress[]= {" Progress: "};
const uint8_t gau8Error[]= {"  _ERROR_  "};
const uint8_t gau8Cancel[]= {"Transfer cancelled"};
const uint8_t gau8Done[]= {"Test Done"};
const uint8_t gau8zero[]={"0%"};

const uint8_t gau8FisrtAndOnly[]={"No data Available"};
/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
#if(MFI_TRANSPORT_USB_DEVICE_MODE)
uint8_t gu8CurrentTransport = USB_DEVICE_COMM;
#elif(MFI_TRANSPORT_USB_HOST_MODE)
uint8_t gu8CurrentTransport = USB_HOST_COMM;
#elif(MFI_TRANSPORT_UART)
uint8_t gu8CurrentTransport = UART_COMM;
#endif

uint8_t TestSuite_res_en_Track[STRING_MAX_SIZE+1] =
{   0x0046, 0x0053, 0x004c, 0x0020, 0x0054, 0x0065, 0x0073, 0x0074,
    0x0020, 0x0053, 0x0075, 0x0069, 0x0074, 0x0065, 0};
uint8_t TestSuite_res_en_Artist[STRING_MAX_SIZE+1] =
{   0x0046, 0x0072, 0x0065, 0x0065, 0x0073, 0x0063, 0x0061, 0x006c,
    0x0065, 0};
uint8_t TestSuite_res_en_Album[STRING_MAX_SIZE+1] =
{   0x004d, 0x0046, 0x0069, 0x0020, 0x0053, 0x0074, 0x0061, 0x0063,
    0x006b, 0x0020, 0x0054, 0x0057, 0x0052, 0x002d, 0x0044, 0x004f,
    0x0043, 0x004b, 0x0032, 0};
uint8_t TestSuite_res_en_Enlapsed[] =
{   0x0030, 0x0030, 0x003a, 0x0030, 0x0030, 0};
uint8_t TestSuite_res_en_TotalTime[] =
{   0x0030, 0x0030, 0x003a, 0x0030, 0x0030, 0};
uint8_t TestSuite_res_en_EAText[256] =
{   0x0057, 0x0061, 0x0069, 0x0074, 0x0069, 0x006e, 0x0067, 0x0020,
    0x0066, 0x006f, 0x0072, 0x0020, 0x006d, 0x0065, 0x0073, 0x0073,
    0x0061, 0x0067, 0x0065, 0x0073, 0};

/* NMFi-26 PlaybackAppBundleId is required. "com.freescale.EADemo" */
uint8_t TestSuite_res_en_PlaybackAppName[STRING_MAX_SIZE] =
{   0x0063, 0x006f, 0x006d, 0x002e, 0x0066, 0x0072, 0x0065, 0x0065,
    0x0073, 0x0063, 0x0061, 0x006c, 0x0065, 0x002e, 0x0045, 0x0041,
    0x0044, 0x0065, 0x006d, 0x006f, 0x0000};

uint8_t TestSuite_res_en_Cable[] =
{   0x0043, 0x0061, 0x0062, 0x006c, 0x0065, ':', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0};
uint8_t TestSuite_res_en_Communication[] =
{   0x0043, 0x006f, 0x006d, 0x006d, 0x0075, 0x006e, 0x0069, 0x0063,
    0x0061, 0x0074, 0x0069, 0x006f, 0x006e, ':', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  0};
uint8_t TestSuite_res_en_Protocol[] =
{   'P', 'r', 'o', 't', 'o', 'c', 'o', 'l', ':', ' ',' ', ' ', ' ', ' ', 0};
uint8_t TestSuite_res_en_resultPrompt[80] =
{   'P', 'l', 'e', 'a', 's', 'e', ' ', 'P', 'r', 'e', 's', 's', ' ',
	'S', 't', 'a', 'r', 't', ' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ',
	' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ' ,' ', ' ', 0};
uint8_t TestSuite_res_en_MFi_Version[] =
{   'M','F','i',' ','S','D','K',' ','2','.','0',' ','T','e','s','t',' ','S','u','i','t',
    'e',' ', 'D','e','m','o', 0};

#if(MFI_TRANSPORT_USB_DEVICE_MODE)
    #ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
        /* ROMDATA PEGCHAR TestSuite_res_en_Title[] = */
        uint8_t TestSuite_res_en_Title[] =
        {   0x0020, 0x0054, 0x0045, 0x0053, 0x0054,
	        0x0020, 0x0053, 0x0055, 0x0049, 0x0054, 0x0045, ' ', '(', 'U', 'S', 'B',
	        ' ', 'R', 'o', 'l','e', ' ', 'S','w', 'i', 't', 'c','h', ')' ,0};
    #else
        /* ROMDATA PEGCHAR TestSuite_res_en_Title[] = */
        uint8_t TestSuite_res_en_Title[] =
        {   0x0020, 0x0054, 0x0045, 0x0053, 0x0054,
            0x0020, 0x0053, 0x0055, 0x0049, 0x0054, 0x0045, ' ', '(', 'U', 'S', 'B',
            ' ', 'D', 'e', 'v','i', 'c', 'e', ')' ,0};
    #endif
#elif(MFI_TRANSPORT_USB_HOST_MODE)
    /* ROMDATA PEGCHAR TestSuite_res_en_Title[] = */
    uint8_t TestSuite_res_en_Title[] =
    {   0x0020, 0x0054, 0x0045, 0x0053, 0x0054,
        0x0020, 0x0053, 0x0055, 0x0049, 0x0054, 0x0045, ' ', '(', 'U', 'S', 'B',
        ' ', 'H', 'o', 's','t',')' ,0};
#elif(MFI_TRANSPORT_UART)
    /* ROMDATA PEGCHAR TestSuite_res_en_Title[] = */
    uint8_t TestSuite_res_en_Title[] =
    {   0x0020, 0x0054, 0x0045, 0x0053, 0x0054,
	    0x0020, 0x0053, 0x0055, 0x0049, 0x0054, 0x0045, ' ', '(', 'U', 'A', 'R',
	    'T', ')' ,0};
#endif
/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/*************************************************************************************************
* @name     vfnCopyBuffInfo
*
* @brief    Copy information with a defined size from one buffer to other 
*           
* @param   	uint8_t* pu8Source    Pointer to Source buffer
*           uint8_t* pu8Destiny   Pointer to destiny buffer
*           uint8_t u8Size        Size of the package to be copied
*
* @return  	None  
*
**************************************************************************************************/
void vfnCopyBuffInfo(uint8_t* pu8Source, uint8_t* pu8Destiny, uint8_t u8Size)
{
    uint8_t u8CharCounter = 0;

    for(u8CharCounter = 0; u8CharCounter < u8Size; u8CharCounter++)
    {
        pu8Destiny[u8CharCounter]  = pu8Source[u8CharCounter]; 
    } 
}

/*************************************************************************************************
* @name     vfnUnSizeCopyBuff
*
* @brief    Copy data from one buffer to other until the NULL character is detected
*           
* @param   	uint8_t* pu8Source   Pointer to source buffer 
*           uint8_t* pu8Destiny  Pointer to destiny buffer
*
* @return  	None  
*
**************************************************************************************************/
void vfnUnSizeCopyBuff(uint8_t* pu8Source, uint8_t* pu8Destiny)
{
    uint8_t u8CharCounter = 0;

    do
    {
        pu8Destiny[u8CharCounter] =  pu8Source[u8CharCounter];
    }while(pu8Source[u8CharCounter++] != '\0');
}

/*************************************************************************************************
* @name     vfnGetMFiTransport
*
* @brief    Once the device is detected actualise the buffer to show in the screen
*           
* @param   	None
*
* @return  	None
*
**************************************************************************************************/
void vfnGetMFiTransport(void)
{
    switch(gu8CurrentTransport)
    {
        case USB_DEVICE_COMM:
        #ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
            //if(g_wUSBHostStatus &(1<<USB_ROLL_SWITCH_DONE))
            {
                vfnCopyBuffInfo((uint8_t*)&gau8USBDeviceTrans[0], 
                (uint8_t*)&TestSuite_res_en_Communication[TRANSPORT_CHAR_INIT], TRANSPORT_CHAR_NUM);
            }
            //else
            //{
            //    vfnCopyBuffInfo((uint8_t*)&gau8USBHostTrans[0], 
            //   (uint8_t*)&TestSuite_res_en_Communication[TRANSPORT_CHAR_INIT], TRANSPORT_CHAR_NUM);
            //}
        #else
            vfnCopyBuffInfo((uint8_t*)&gau8USBDeviceTrans[0], 
            (uint8_t*)&TestSuite_res_en_Communication[TRANSPORT_CHAR_INIT], TRANSPORT_CHAR_NUM);
        #endif
            break;
        case USB_HOST_COMM:
            vfnCopyBuffInfo((uint8_t*)&gau8USBHostTrans[0], 
            (uint8_t*)&TestSuite_res_en_Communication[TRANSPORT_CHAR_INIT], TRANSPORT_CHAR_NUM);
            break;
        case UART_COMM:
            vfnCopyBuffInfo((uint8_t*)&gau8UartTrans[0], 
            (uint8_t*)&TestSuite_res_en_Communication[TRANSPORT_CHAR_INIT], TRANSPORT_CHAR_NUM);
            break;
        default:
            vfnCopyBuffInfo((uint8_t*)&gau8empty[0], 
            (uint8_t*)&TestSuite_res_en_Communication[TRANSPORT_CHAR_INIT], TRANSPORT_CHAR_NUM);
            break;
    }
}


/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
