/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Note: This software/document contains information restricted to MFi licensees and subject to the 
 *       MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *		This header file is the interface between the SI5351.c Module and the 
 * 		DigitalAudioDriver.c module.
 *
 *END*********************************************************************************************/
 
#ifndef SI5351_H_
#define SI5351_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/


/*
 * Defines SYS_MCLK frequency
 * */
#define SI5351_SYS_MCLK		(27000000) 



/*
 * Enables SI5351 internal oscillator to be used for the zero cross detectors
 * 
 */
//#define _SI5351_INT_OSC					

/***************************************************************************************************/
/* * Register values. */
#define SI5351_REGISTER0	        (0)
#define SI5351_REGISTER1	        (1)
#define SI5351_REGISTER2	        (2)
#define SI5351_REGISTER3	        (3)
#define SI5351_REGISTER9	        (9)
#define SI5351_REGISTER15			(15)
#define SI5351_REGISTER16			(16)
#define SI5351_REGISTER17			(17)
#define SI5351_REGISTER18			(18)
#define SI5351_REGISTER19			(19)
#define SI5351_REGISTER20			(20)
#define SI5351_REGISTER21			(21)
#define SI5351_REGISTER22			(22)
#define SI5351_REGISTER23			(23)
#define SI5351_REGISTER24			(24)
#define SI5351_REGISTER25			(25)
#define SI5351_REGISTER26			(26)
#define SI5351_REGISTER27			(27)
#define SI5351_REGISTER28			(28)
#define SI5351_REGISTER29			(29)
#define SI5351_REGISTER30			(30)
#define SI5351_REGISTER31			(31)
#define SI5351_REGISTER32			(32)
#define SI5351_REGISTER33			(33)
#define SI5351_REGISTER34			(34)
#define SI5351_REGISTER35			(35)
#define SI5351_REGISTER36			(36)
#define SI5351_REGISTER37			(37)
#define SI5351_REGISTER38			(38)
#define SI5351_REGISTER39			(39)
#define SI5351_REGISTER40			(40)
#define SI5351_REGISTER41			(41)
#define SI5351_REGISTER42			(42)
#define SI5351_REGISTER43			(43)
#define SI5351_REGISTER44			(44)
#define SI5351_REGISTER45			(45)
#define SI5351_REGISTER46			(46)
#define SI5351_REGISTER47			(47)
#define SI5351_REGISTER48			(48)
#define SI5351_REGISTER49			(49)
#define SI5351_REGISTER50			(50)
#define SI5351_REGISTER51			(51)
#define SI5351_REGISTER52			(52)
#define SI5351_REGISTER53			(53)
#define SI5351_REGISTER54			(54)
#define SI5351_REGISTER55			(55)
#define SI5351_REGISTER56			(56)
#define SI5351_REGISTER57			(57)
#define SI5351_REGISTER58			(58)
#define SI5351_REGISTER59			(59)
#define SI5351_REGISTER60			(60)
#define SI5351_REGISTER61			(61)
#define SI5351_REGISTER62			(62)
#define SI5351_REGISTER63			(63)
#define SI5351_REGISTER64			(64)
#define SI5351_REGISTER65			(65)
#define SI5351_REGISTER66			(66)
#define SI5351_REGISTER67			(67)
#define SI5351_REGISTER68			(68)
#define SI5351_REGISTER69			(69)
#define SI5351_REGISTER70			(70)
#define SI5351_REGISTER71			(71)
#define SI5351_REGISTER72			(72)
#define SI5351_REGISTER73			(73)
#define SI5351_REGISTER74			(74)
#define SI5351_REGISTER75			(75)
#define SI5351_REGISTER76			(76)
#define SI5351_REGISTER77			(77)
#define SI5351_REGISTER78			(78)
#define SI5351_REGISTER79			(79)
#define SI5351_REGISTER80			(80)
#define SI5351_REGISTER81			(81)
#define SI5351_REGISTER82			(82)
#define SI5351_REGISTER83			(83)
#define SI5351_REGISTER84			(84)
#define SI5351_REGISTER85			(85)
#define SI5351_REGISTER86			(86)
#define SI5351_REGISTER87			(87)
#define SI5351_REGISTER88			(88)
#define SI5351_REGISTER89			(89)
#define SI5351_REGISTER90			(90)
#define SI5351_REGISTER91			(91)
#define SI5351_REGISTER92			(92)
#define SI5351_REGISTER93			(93)
#define SI5351_REGISTER94			(94)
#define SI5351_REGISTER95			(95)
#define SI5351_REGISTER96			(96)
#define SI5351_REGISTER97			(97)
#define SI5351_REGISTER98			(98)
#define SI5351_REGISTER99			(99)
#define SI5351_REGISTER100			(100)
#define SI5351_REGISTER101			(101)
#define SI5351_REGISTER102			(102)
#define SI5351_REGISTER103			(103)
#define SI5351_REGISTER104			(104)
#define SI5351_REGISTER105			(105)
#define SI5351_REGISTER106			(106)
#define SI5351_REGISTER107			(107)
#define SI5351_REGISTER108			(108)
#define SI5351_REGISTER109			(109)
#define SI5351_REGISTER110			(110)
#define SI5351_REGISTER111			(111)
#define SI5351_REGISTER112			(112)
#define SI5351_REGISTER113			(113)
#define SI5351_REGISTER114			(114)
#define SI5351_REGISTER115			(115)
#define SI5351_REGISTER116			(116)
#define SI5351_REGISTER117			(117)
#define SI5351_REGISTER118			(118)
#define SI5351_REGISTER119			(119)
#define SI5351_REGISTER120			(120)
#define SI5351_REGISTER121			(121)
#define SI5351_REGISTER122			(122)
#define SI5351_REGISTER123			(123)
#define SI5351_REGISTER124			(124)
#define SI5351_REGISTER125			(125)
#define SI5351_REGISTER126			(126)
#define SI5351_REGISTER127			(127)
#define SI5351_REGISTER128			(128)
#define SI5351_REGISTER129			(129)
#define SI5351_REGISTER130			(130)
#define SI5351_REGISTER131			(131)
#define SI5351_REGISTER132			(132)
#define SI5351_REGISTER133			(133)
#define SI5351_REGISTER134			(134)
#define SI5351_REGISTER135			(135)
#define SI5351_REGISTER136			(136)
#define SI5351_REGISTER137			(137)
#define SI5351_REGISTER138			(138)
#define SI5351_REGISTER139			(139)
#define SI5351_REGISTER140			(140)
#define SI5351_REGISTER141			(141)
#define SI5351_REGISTER142			(142)
#define SI5351_REGISTER143			(143)
#define SI5351_REGISTER144			(144)
#define SI5351_REGISTER145			(145)
#define SI5351_REGISTER146			(146)
#define SI5351_REGISTER147			(147)
#define SI5351_REGISTER148			(148)
#define SI5351_REGISTER149			(149)
#define SI5351_REGISTER150			(150)
#define SI5351_REGISTER151			(151)
#define SI5351_REGISTER152			(152)
#define SI5351_REGISTER153			(153)
#define SI5351_REGISTER154			(154)
#define SI5351_REGISTER155			(155)
#define SI5351_REGISTER156			(156)
#define SI5351_REGISTER157			(157)
#define SI5351_REGISTER158			(158)
#define SI5351_REGISTER159			(159)
#define SI5351_REGISTER160			(160)
#define SI5351_REGISTER161			(161)
#define SI5351_REGISTER162			(162)
#define SI5351_REGISTER163			(163)
#define SI5351_REGISTER164			(164)
#define SI5351_REGISTER165			(165)
#define SI5351_REGISTER166			(166)
#define SI5351_REGISTER167			(167)
#define SI5351_REGISTER168			(168)
#define SI5351_REGISTER169			(169)
#define SI5351_REGISTER170			(170)
#define SI5351_REGISTER177			(177)
#define SI5351_REGISTER183			(183)
#define SI5351_REGISTER187			(187)


#define SI514_REGISTER26_MSNA_P3_15_8_MASK				(0xFFu)
#define SI514_REGISTER26_MSNA_P3_15_8_SHIFT				(8)
#define SI514_REGISTER26_MSNA_P3_15_8(x)				(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER26_MSNA_P3_15_8_SHIFT))&SI514_REGISTER26_MSNA_P3_15_8_MASK)

#define SI514_REGISTER27_MSNA_P3_7_0_MASK				(0xFFu)
#define SI514_REGISTER27_MSNA_P3_7_0_SHIFT				(0)
#define SI514_REGISTER27_MSNA_P3_7_0(x)					(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER27_MSNA_P3_7_0_SHIFT))&SI514_REGISTER27_MSNA_P3_7_0_MASK)

#define SI514_REGISTER28_MSNA_P1_17_16_MASK				(0x3u)
#define SI514_REGISTER28_MSNA_P1_17_16_SHIFT			(16)
#define SI514_REGISTER28_MSNA_P1_17_16(x)				(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER28_MSNA_P1_17_16_SHIFT))&SI514_REGISTER28_MSNA_P1_17_16_MASK)

#define SI514_REGISTER29_MSNA_P1_15_8_MASK				(0xFFu)
#define SI514_REGISTER29_MSNA_P1_15_8_SHIFT				(8)
#define SI514_REGISTER29_MSNA_P1_15_8(x)				(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER29_MSNA_P1_15_8_SHIFT))&SI514_REGISTER29_MSNA_P1_15_8_MASK)

#define SI514_REGISTER30_MSNA_P1_7_0_MASK				(0xFFu)
#define SI514_REGISTER30_MSNA_P1_7_0_SHIFT				(0)
#define SI514_REGISTER30_MSNA_P1_7_0(x)					(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER30_MSNA_P1_7_0_SHIFT))&SI514_REGISTER30_MSNA_P1_7_0_MASK)

#define SI514_REGISTER31_MSNA_P3_19_16_MASK				(0xF0u)
#define SI514_REGISTER31_MSNA_P3_19_16_SHIFT			(12)
#define SI514_REGISTER31_MSNA_P3_19_16(x)				(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER31_MSNA_P3_19_16_SHIFT))&SI514_REGISTER31_MSNA_P3_19_16_MASK)
#define SI514_REGISTER31_MSNA_P2_19_16_MASK				(0xFu)
#define SI514_REGISTER31_MSNA_P2_19_16_SHIFT			(16)
#define SI514_REGISTER31_MSNA_P2_19_16(x)				(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER31_MSNA_P2_19_16_SHIFT))&SI514_REGISTER31_MSNA_P2_19_16_MASK)

#define SI514_REGISTER32_MSNA_P2_15_8_MASK				(0xFFu)
#define SI514_REGISTER32_MSNA_P2_15_8_SHIFT				(8)
#define SI514_REGISTER32_MSNA_P2_15_8(x)				(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER32_MSNA_P2_15_8_SHIFT))&SI514_REGISTER32_MSNA_P2_15_8_MASK)

#define SI514_REGISTER33_MSNA_P2_7_0_MASK				(0xFFu)
#define SI514_REGISTER33_MSNA_P2_7_0_SHIFT				(0)
#define SI514_REGISTER33_MSNA_P2_7_0(x)					(((uint8_t)(((uint32_t)(x))>>SI514_REGISTER33_MSNA_P2_7_0_SHIFT))&SI514_REGISTER33_MSNA_P2_7_0_MASK)


/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
/*!
    \enum eSI5351Status
    \brief SI5351 driver status
*/
typedef enum
{
	SI5351_STATUS_TCO_READY=0,
	SI5351_STATUS_CONFIGURED,
	SI5351_STATUS_I2C_ERROR,
}eSI5351Status;

typedef enum
{
	SI5351_STATUS_BUSY = 0,
	SI5351_STATUS_IDLE,
}eSI5351I2CStatus;


/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void SI5351_vfnInit(void);
uint8_t SI5351_vfnCorrectFrequency(uint64_t dwFreqRatioValue);
uint8_t SI5351_vfnResetFrequency(void);
void SI5351_vfnDriver(void);
/*************************************************************************************************/

#endif /* SI5351_H_ */
