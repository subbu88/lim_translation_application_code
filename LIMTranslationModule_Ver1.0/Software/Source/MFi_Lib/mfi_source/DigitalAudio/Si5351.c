/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Note: This software/document contains information restricted to MFi licensees and subject to the 
 *       MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *		This module contains all the functions needed to communicate with the SI5351 device.
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "Events.h"
#include "Si5351.h"
#include "iAP_Timer.h"
#include "AudioClkSyncDriver.h"

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void SI5351_vfnWriteRegister(uint8_t bRegister, uint8_t *bRegisterData);
static void SI5351_vfnReadRegister(uint8_t bRegister, uint8_t *bpRegisterData);
static void SI5351_vfnABC2MSNARegs(uint32_t dwA, uint32_t dwB ,uint32_t dwC);
/* State functions */
static void SI5351_vfnSetConfiguration_State(void);
static void SI5351_vfnStartCorrectFrequency_state(void);
static void SI5351_vfnReadMSValues_State(void);
static void SI5351_vfnWriteMSValues_State(void);
static void SI5351_vfnWaitComm_State(void);
static void SI5351_vfnWaitReading_State(void);
static void SI5351_vfnIDLE(void);

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
/*!
    \def SI5351_I2C_ADDRESS
    \brief I2C Slave address of the SI5351
*/
#define SI5351_I2C_ADDRESS						(0x6F)  
/*!
    \def SI5351_CONTROL_BUFFER_SIZE
    \brief Size to be used by the buffer for the I2C communication
*/
#define SI5351_I2C_ADDRESSING_SIZE				(1)
#define SI5351_MS_REGS_SIZE						(8)
#define SI5351_CONTROL_BUFFER_SIZE				(SI5351_I2C_ADDRESSING_SIZE + SI5351_MS_REGS_SIZE)

#define SI5351_INIT_SIZE							(12)

#define SI5351_48000HZ							(48000)
#define SI5351_44100HZ							(44100)
#define SI5351_32000HZ							(32000)

#define SI5351_FRACTION_BITS					(20)
#define SI5351_CONST_FRACTION_BITS				(31)
#define SI5351_A_SHIFTING_BITS					(SI5351_CONST_FRACTION_BITS + ACSD_FRACTION_BITS)
#define SI5351_B_SHIFTING_BITS					(SI5351_CONST_FRACTION_BITS + ACSD_FRACTION_BITS-SI5351_FRACTION_BITS)
#define SI5351_SHIFT_TO_OUTPUT_PRECISION		(SI5351_CONST_FRACTION_BITS + ACSD_FRACTION_BITS-SI5351_FRACTION_BITS)
#define SI5351_OUTPUT_OSCILLATION				(24576000)
#define SI5351_INPUT_OSCILLATION				(27000000)
#define SI5351_MULTISYNTH_A						(30)
#define SI5351_MULTISYNTH_B						(0)
#define SI5351_MULTISYNTH_C						(1)

#define SI5351_PLLA_A		(27)
#define SI5351_PLLA_B		(321563)
#define SI5351_PLLA_C		(0xFFFFF)

#define SI5351_DELTAFREQ_GOOD_VALUE						(819)


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

typedef enum
{
	SI5351_SET_CONFIGURATION_STATE = 0,
	SI5351_START_CORRECT_FREQUENCY_STATE,
	SI5351_READ_MS_VALUES_STATE,
	SI5351_WRITE_MS_VALUES_STATE,
	SI5351_CONTROL_STATE_WAIT_COMM,
	SI5351_CONTROL_STATE_WAIT_READING,
	SI5351_CONTROL_STATE_IDLE
}eSI5351ControlStates; /**< used for SI5351 driver states */

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
uint8_t SI5351_gbDriverStatus = 0;  /**< used to identify SI5351 Driver status */
sSM SI5351_gslStateMachine;  /**< Stores the states of the state machines */

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

static uint8_t SI5351_gabControlOutputBuffer[SI5351_CONTROL_BUFFER_SIZE];  /**< Buffer for write I2C commands */

static uint32_t gdwA=SI5351_PLLA_A;
static uint32_t gdwB=SI5351_PLLA_B;

static uint8_t gbBlockSize = 1;
static int32_t Si5351_gdwDeltaFreqValue;
static uint64_t Si5351_gdwFreqRatioValue;
static uint32_t Si5351_gdwSampleFreq;
static uint8_t *SI5351_gbpReadData;	 /**< Pointer to the I2C read data */

static uint8_t SI5351_gbIICBusStatus; /**< Save IIC bus status */

static uint8_t SI5351_gabMSNAArray[SI5351_MS_REGS_SIZE]; /**  */

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*!
    \var SI5351_gawInitRegisterSequenceInitRegisterSequence
    \brief Registers definitions for the initialization
*/
static const uint16_t SI5351_gawInitRegisterSequence[SI5351_INIT_SIZE] =
{
		SI5351_REGISTER3, // Disable Outputs
		SI5351_REGISTER16, //Powerdown all output drivers
		SI5351_REGISTER17,
		SI5351_REGISTER18,
		SI5351_REGISTER19,
		SI5351_REGISTER20,
		SI5351_REGISTER21,
		SI5351_REGISTER22,
		SI5351_REGISTER23,
		SI5351_REGISTER2,
		//SI5351_REGISTER  /*Registers 15-92 and 149-170 */
		SI5351_REGISTER177, //Apply PLLA and PLLB soft reset
		SI5351_REGISTER3 //Enable desired outputs
};

/*!
    \var SI5351_gawInitRegisterValuesSequence
    \brief Registers values for the initialization ***** TBD *****.
*/
static const uint8_t SI5351_gawInitRegisterValuesSequence[SI5351_INIT_SIZE] =
{
	0x0,//SI5351_REGISTER3	Disable Outputs
	0x0,//SI5351_REGISTER16	Powerdown all output drivers
	0x0,//SI5351_REGISTER17
	0x0,//SI5351_REGISTER18
	0x0,//SI5351_REGISTER19
	0x0,//SI5351_REGISTER20
	0x0,//SI5351_REGISTER21
	0x0,//SI5351_REGISTER22
	0x0,//SI5351_REGISTER23
	0x0,//SI5351_REGISTER2
	//0x0,//SI5351_REGISTER /* Registers 15-92 and 149-170 */
	0x0,//SI5351_REGISTER177	Apply PLLA and PLLB soft reset
	0x0//SI5351_REGISTER3	Enable desired outputs
};


void (* const SI5351_vfnapStateMachine[]) (void) =
{
	SI5351_vfnSetConfiguration_State,
	SI5351_vfnStartCorrectFrequency_state,
	SI5351_vfnReadMSValues_State,
	SI5351_vfnWriteMSValues_State,
	SI5351_vfnWaitComm_State,
	SI5351_vfnWaitReading_State,
	SI5351_vfnIDLE	
};

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/* Global functions */

/** ***********************************************************************************************
*
* @brief    starts SI5351 initialization, further init is done on the INIT state
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
void SI5351_vfnInit(void)
{
	/* Initialize physical layer */
	//(void)iAPI2C1_Enable(iAPI2C1_DeviceData);
	/* Set the driver as busy */
	SI5351_gbDriverStatus |= (1 << SI5351_STATUS_BUSY);
	/* Clear configured flag since will be configured again */
	//SI5351_gbDriverStatus &= ~(1 << SI5351_STATUS_CONFIGURED);
	//SI5351_gslStateMachine.ActualState = SI5351_SET_CONFIGURATION_STATE;
	SI5351_gslStateMachine.ActualState = SI5351_CONTROL_STATE_IDLE;
	SI5351_gslStateMachine.NextState = SI5351_CONTROL_STATE_IDLE;

}

/** ***********************************************************************************************
 *
 * @brief    Start state machine to correct frequency
 * 
 * @param   	none 
 * 
 * @return  	none
 *          
 **************************************************************************************************/
uint8_t SI5351_vfnCorrectFrequency(uint64_t dwFreqRatioValue)
{
	uint8_t bResultValue=SI5351_STATUS_BUSY;
	if(!(SI5351_gbDriverStatus & (1 << SI5351_STATUS_BUSY)))
	{
		/* Set the driver as busy */
		SI5351_gbDriverStatus |= (1 << SI5351_STATUS_BUSY);
		Si5351_gdwFreqRatioValue = dwFreqRatioValue;
		bResultValue=SI5351_STATUS_IDLE;
		SI5351_gslStateMachine.ActualState = SI5351_START_CORRECT_FREQUENCY_STATE;
	}
	return bResultValue;
}


 /** ***********************************************************************************************
 *
 * @brief    Reset the Si5351 output frequency to default value.
 * 
 * @param   	dwSampleFreq: Actual Sample rate used 
 * 
 * @return  	none
 *          
 **************************************************************************************************/
uint8_t SI5351_vfnResetFrequency(void)
{
	uint8_t bResultValue=SI5351_STATUS_BUSY;
	if(!(SI5351_gbDriverStatus & (1 << SI5351_STATUS_BUSY)))
	{
		/* Set the driver as busy */
		SI5351_gbDriverStatus |= (1 << SI5351_STATUS_BUSY);
		SI5351_vfnABC2MSNARegs(SI5351_PLLA_A, SI5351_PLLA_B ,SI5351_PLLA_C);
		gdwA=SI5351_PLLA_A;
		gdwB=SI5351_PLLA_B;
		bResultValue=SI5351_STATUS_IDLE;
		SI5351_gslStateMachine.ActualState = SI5351_WRITE_MS_VALUES_STATE;
		SI5351_gslStateMachine.NextState = SI5351_CONTROL_STATE_IDLE;
	}
	return bResultValue;
}

/** ***********************************************************************************************
*
* @brief    Si5351 driver function.
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
void SI5351_vfnDriver(void)
{
	SI5351_vfnapStateMachine[SI5351_gslStateMachine.ActualState]();	
}

/** ***********************************************************************************************
*
* @brief    Provides the ability to write SI5351 control registers 
* 
* @param   	wRegister: Register to be written
* @param	wRegisterData: Value of the desired register
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnWriteRegister(uint8_t bRegister, uint8_t *bRegisterData)
{
	uint8_t i=SI5351_I2C_ADDRESSING_SIZE;
	/* set the driver as busy and the next state to wait physical layer driver */
	SI5351_gbDriverStatus |= (1 << SI5351_STATUS_BUSY);
	/* Flag IIC driver as busy */
	IIC_SET_DRIVER_BUSY;
	IIC_SET_RESERVE;
	IIC_SET_RELEASE_ON_COMP;

	/* Set the command buffer to send the data thru physical layer */
	SI5351_gabControlOutputBuffer[0] = bRegister;
	while(i <= gbBlockSize)
	{
		SI5351_gabControlOutputBuffer[i++] = *bRegisterData++;
	}
	/* Select I2C slave address */
	(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,SI5351_I2C_ADDRESS);
	/* Call the PE Write Block HAL I2C function */
	(void)iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, &SI5351_gabControlOutputBuffer[0], \
										SI5351_I2C_ADDRESSING_SIZE+gbBlockSize, LDD_I2C_SEND_STOP);
	
	/* Change state machine current state to SI5351_CONTROL_STATE_WAIT_COMM */
	SI5351_gslStateMachine.ActualState = SI5351_CONTROL_STATE_WAIT_COMM;
}

/** ***********************************************************************************************
*
* @brief    Provides the ability to read a control register. Internally it is 
      	  	 a wrapper of a a static (module) function
* 
* @param   	wRegister: Register to be read
* @param	bpRegisterData: Pointer to where the read data will be stored
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnReadRegister(uint8_t bRegister, uint8_t *bpRegisterData)
{
	/* Set the driver as busy and the next state logic */
	SI5351_gbDriverStatus |= (1 << SI5351_STATUS_BUSY);
	
	/* Set the I2C Driver busy flag */
	IIC_SET_DRIVER_BUSY;
	IIC_SET_RESERVE;
	IIC_CLEAR_RELEASE_ON_COMP;
	/* Setup control buffer */
	SI5351_gabControlOutputBuffer[0] = bRegister;
	
	/* Setup pointer to the reception buffer */
	SI5351_gbpReadData = bpRegisterData;

	/* Select I2C slave address */
	(void)(iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData, LDD_I2C_ADDRTYPE_7BITS, SI5351_I2C_ADDRESS));

	/* Set the logical address to be read */
	(void)(iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, &SI5351_gabControlOutputBuffer[0], 1, LDD_I2C_SEND_STOP));
	
	/* Sets the current and next state to finish the reading process */
	SI5351_gslStateMachine.ActualState = SI5351_CONTROL_STATE_WAIT_COMM;
	SI5351_gslStateMachine.NextState = SI5351_CONTROL_STATE_WAIT_READING;
}

/** ***********************************************************************************************
*
* @brief    Reset the Si5351 output frequency to default value.
* 
* @param   	dwSampleFreq: Actual Sample rate used 
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnABC2MSNARegs(uint32_t dwA, uint32_t dwB ,uint32_t dwC)
{
	uint32_t dwP1;
	uint32_t dwP2;
	
	dwP1=128*dwA + (128*dwB/dwC) - 512;
	dwP2=128*dwB - dwC*(128*dwB/dwC);

	SI5351_gabMSNAArray[0]=SI514_REGISTER26_MSNA_P3_15_8(dwC);
	SI5351_gabMSNAArray[1]=SI514_REGISTER27_MSNA_P3_7_0(dwC);
	SI5351_gabMSNAArray[2]=SI514_REGISTER28_MSNA_P1_17_16(dwP1);
	SI5351_gabMSNAArray[3]=SI514_REGISTER29_MSNA_P1_15_8(dwP1);
	SI5351_gabMSNAArray[4]=SI514_REGISTER30_MSNA_P1_7_0(dwP1);
	SI5351_gabMSNAArray[5]=SI514_REGISTER31_MSNA_P3_19_16(dwC)|SI514_REGISTER31_MSNA_P2_19_16(dwP2);
	SI5351_gabMSNAArray[6]=SI514_REGISTER32_MSNA_P2_15_8(dwP2);
	SI5351_gabMSNAArray[7]=SI514_REGISTER33_MSNA_P2_7_0(dwP2);
}

/* State functions */

/** ***********************************************************************************************
*
* @brief   Set SI5351 Configuration
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnSetConfiguration_State(void)
{
	static uint8_t bInitiStep = 0;

	/*retries if the I2C driver reported an error*/
	if(SI5351_gbDriverStatus&(1<<SI5351_STATUS_I2C_ERROR))
	{
		/* Decrease the SGTL write index register ro write the same register again */
		bInitiStep--;
		/* Clear the SGTL500 I2C error flag */
		SI5351_gbDriverStatus&= ~(1<<SI5351_STATUS_I2C_ERROR);
	}
	/* Check if there is any other write registers */
	if(bInitiStep < SI5351_INIT_SIZE)
	{
		if(!IIC_RESERVED)
		{
			/* if driver is NOT busy */
			/** @todo Function Return */
			(void)IIC_CHECK_BUS(SI5351_gbIICBusStatus);
			if(SI5351_gbIICBusStatus)
			{
				gbBlockSize=1;
				/* Send register and the data to be written */
				SI5351_vfnWriteRegister(SI5351_gawInitRegisterSequence[bInitiStep],(uint8_t*)&SI5351_gawInitRegisterValuesSequence[bInitiStep]);
				/* Keep state machine in the control init state by setting current state for next state */
				SI5351_gslStateMachine.NextState = SI5351_SET_CONFIGURATION_STATE;
				/* Increase the SGTL index */
				bInitiStep++;
			}
		}
	}
	else /* If there are no more register to be written */
	{
		/* Clear the write index register */
		bInitiStep = 0;
		/* Set the SGTL already configured flag */
		SI5351_gbDriverStatus |= (1 << SI5351_STATUS_CONFIGURED);
		/* Go to idle state */
		SI5351_gslStateMachine.ActualState = SI5351_CONTROL_STATE_IDLE;
		SI5351_gslStateMachine.NextState = SI5351_CONTROL_STATE_IDLE;
	}
}

/** ***********************************************************************************************
*
* @brief    Sets state machine to the change frequency state
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnStartCorrectFrequency_state(void)
{
	/** Convert (a + b/c) to Q45. This due from gdwDeltaFreqValue calculation in Q14 and 
	 *  from SI5351_CONSTANT_FOR_48000HZ in Q31 => Q14 + Q31 = Q45 )*/
	/** convert gdwA to Q45*/				/**gdwB already at Q20, convert it to Q45 shifting only 25 to left */
	uint64_t qwNewMS = (uint64_t)((gdwA<<20) | gdwB );
	qwNewMS*=Si5351_gdwFreqRatioValue;
	
	qwNewMS>>=32;


	gdwA= (uint32_t)(qwNewMS >> 20);
	gdwB= (uint32_t)( qwNewMS & SI5351_PLLA_C);
	
	SI5351_vfnABC2MSNARegs(gdwA, gdwB ,SI5351_PLLA_C);
	
	SI5351_gslStateMachine.ActualState = SI5351_WRITE_MS_VALUES_STATE;
	SI5351_gslStateMachine.NextState = SI5351_CONTROL_STATE_IDLE;
}


/** ***********************************************************************************************
*
* @brief    
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnReadMSValues_State(void)
{
	if(!IIC_RESERVED)
	{
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(SI5351_gbIICBusStatus);
		if(SI5351_gbIICBusStatus)
		{
			/* Read register all M factor registers and after finishing read go to 
			   SI5351_CORRECTING_FREQUENCY_STATE state */
			gbBlockSize=SI5351_MS_REGS_SIZE;
			SI5351_vfnReadRegister(SI5351_REGISTER26, &SI5351_gabMSNAArray[0]);
		}
	}
}

/** ***********************************************************************************************
*
* @brief    
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnWriteMSValues_State(void)
{
	/* Retries if the I2C driver reported an error*/
	if(SI5351_gbDriverStatus&(1<<SI5351_STATUS_I2C_ERROR))
	{
		SI5351_gbDriverStatus&= ~(1<<SI5351_STATUS_I2C_ERROR);
	}

	if(!IIC_RESERVED)
	{
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(SI5351_gbIICBusStatus);
		if(SI5351_gbIICBusStatus)
		{
			/* Read register all M factor registers and after finishing read go to 
			   SI5351_CORRECTING_FREQUENCY_STATE state */
			gbBlockSize=SI5351_MS_REGS_SIZE;
			SI5351_vfnWriteRegister(SI5351_REGISTER26, &SI5351_gabMSNAArray[0]);

		}
	}
}

/** ***********************************************************************************************
*
* @brief    Waits for the physical layer driver to finish its process
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnWaitComm_State(void)
{
	if(!IIC_DRIVER_BUSY)
	{
		/*Check for the physical layer driver to be free*/
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(SI5351_gbIICBusStatus);
		if(SI5351_gbIICBusStatus)
		{
			/* Confirm if there is no error */
			if(IIC_DRIVER_ERROR)
			{
				IIC_CLEAR_DRIVER_ERROR;
				SI5351_gbDriverStatus |= (1<<SI5351_STATUS_I2C_ERROR);
			}
			else
			{
				if(IIC_GET_RELEASE_ON_COMP)
				{
					/* Unreserve module if the intended operation was a write */
					IIC_CLEAR_RESERVE;
				}

				SI5351_gslStateMachine.ActualState = SI5351_gslStateMachine.NextState;
			}
		}
	}
}


/** ***********************************************************************************************
*
* @brief   Waits for the physical layer driver to finish its process
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnWaitReading_State(void)
{
	if(!IIC_DRIVER_BUSY)
	{
		/*Check for the physical layer driver to be free*/
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(SI5351_gbIICBusStatus);
		if(SI5351_gbIICBusStatus)
		{
			/* Set to busy the I2C driver flag */
			IIC_SET_DRIVER_BUSY;
			IIC_SET_RELEASE_ON_COMP;
			
			/* Select I2C slave address */
			(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,SI5351_I2C_ADDRESS);
			/* Call the PE Receive Block HAL function */
			(void)iAPI2C1_MasterReceiveBlock(iAPI2C1_DeviceData, SI5351_gbpReadData, \
					gbBlockSize, LDD_I2C_SEND_STOP );
			/* Set the state machine to the wait state */
			SI5351_gslStateMachine.ActualState = SI5351_CONTROL_STATE_WAIT_COMM;
		}
	}
}

/** ***********************************************************************************************
*
* @brief    Idle state
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SI5351_vfnIDLE(void)
{
	SI5351_gbDriverStatus &= ~(1 << SI5351_STATUS_BUSY);
}
