/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *		This header file is the interface between the SGTL5000.c Module and the 
 * 		DigitalAudioDriver.c module.
 *
 *END*********************************************************************************************/

#ifndef SGTL5000_H_
#define SGTL5000_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/


/*Select the word length */
//#define _16_BIT_WORD
//#define _20_BIT_WORD
//#define _24_BIT_WORD
#define _32_BIT_WORD

//#define I2S_RIGHT_JUSTIFIED_MODE
/*
 * Defines SYS_MCLK frequency
 * */
#define SGTL5000_SYS_MCLK		(24576000) // B36502



/*
 * Enables SGTL5000 internal oscillator to be used for the zero cross detectors
 * 
 */
//#define _SGTL5000_INT_OSC					

/***************************************************************************************************/
/* * Register values. */
#define SGTL5000_CHIP_ID		        	(0x0000)
#define SGTL5000_CHIP_DIG_POWER				(0x0002)
#define SGTL5000_CHIP_CLK_CTRL				(0x0004)
#define SGTL5000_CHIP_I2S_CTRL				(0x0006)
#define SGTL5000_CHIP_SSS_CTRL				(0x000a)
#define SGTL5000_CHIP_ADCDAC_CTRL			(0x000e)
#define SGTL5000_CHIP_DAC_VOL				(0x0010)
#define SGTL5000_CHIP_PAD_STRENGTH			(0x0014)
#define SGTL5000_CHIP_ANA_ADC_CTRL			(0x0020)
#define SGTL5000_CHIP_ANA_HP_CTRL			(0x0022)
#define SGTL5000_CHIP_ANA_CTRL				(0x0024)
#define SGTL5000_CHIP_LINREG_CTRL			(0x0026)
#define SGTL5000_CHIP_REF_CTRL				(0x0028)
#define SGTL5000_CHIP_MIC_CTRL				(0x002a)
#define SGTL5000_CHIP_LINE_OUT_CTRL			(0x002c)
#define SGTL5000_CHIP_LINE_OUT_VOL			(0x002e)
#define SGTL5000_CHIP_ANA_POWER				(0x0030)
#define SGTL5000_CHIP_PLL_CTRL				(0x0032)
#define SGTL5000_CHIP_CLK_TOP_CTRL			(0x0034)
#define SGTL5000_CHIP_ANA_STATUS			(0x0036)
#define SGTL5000_CHIP_SHORT_CTRL			(0x003c)
#define SGTL5000_CHIP_ANA_TEST2				(0x003a)
#define SGTL5000_DAP_BASS_ENHANCE          	(0x0104)
#define SGTL5000_DAP_BASS_ENHANCE_CTRL     	(0x0106)

#define SGTL5000_DAP_AUDIO_EQ             	(0x0108)
#define SGTL5000_DAP_AUDIO_EQ_BASS_BAND0   	(0x0116)
#define SGTL5000_DAP_AUDIO_EQ_BAND1        	(0x0118)
#define SGTL5000_DAP_AUDIO_EQ_BAND2        	(0x011A)
#define SGTL5000_DAP_AUDIO_EQ_BAND3        	(0x011C)
#define SGTL5000_DAP_AUDIO_EQ_TREBLE_BAND4  (0x011E)

#define SGTL5000_DAP_AVC_CTRL              	(0x0124)
#define SGTL5000_DAP_AVC_THRESHOLD         	(0x0126)
#define SGTL5000_DAP_AVC_ATTACK            	(0x0128)
#define SGTL5000_DAP_AVC_DECAY              (0x012A)
#define SGTL5000_DAP_MAIN_CHAN             	(0x0120)
#define SGTL5000_DAP_CONTROL                (0x0100)

#define SGTL5000_CHECK_STATUS(x)	(SGTL5000_gbDriverStatus&((1 << x)))
#define SGTL5000_CONTROL_BUSY		(SGTL5000_gbDriverStatus&((1 << SGTL5000_STATUS_BUSY)))
#define SGTL5000_CONFIGURED			(SGTL5000_gbDriverStatus&((1 << SGTL5000_STATUS_CONFIGURED)))

#define SGTL5000_FREQ_96KHZ		(96000) 
#define SGTL5000_FREQ_48KHZ     (48000)   
#define SGTL5000_FREQ_44_1KHZ   (44100)   
#define SGTL5000_FREQ_32KHZ		(32000) 

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
/*!
    \enum eSGTL5000Status
    \brief SGTL5000 driver status
*/
typedef enum
{
	SGTL5000_STATUS_BUSY = 0,
	SGTL5000_STATUS_CONFIGURED,
	SGTL5000_STATUS_I2C_ERROR
}eSGTL5000Status;

/*!
    \enum eSGTL5000GEQBands
    \brief SGTL5000 5 GEQ bands
*/
typedef enum
{
	SGTL5000_BAND_0 = 0,
	SGTL5000_BAND_1,
	SGTL5000_BAND_2,
	SGTL5000_BAND_3,
	SGTL5000_BAND_4
}eSGTL5000GEQBands;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern uint8_t SGTL5000_gbDriverStatus;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void SGTL5000_vfnInit(void);
uint8_t SGTL5000_bfnChangeFrequency(uint32_t dwNewFreqValue);
void SGTL5000_vfnDriver(void);
/*************************************************************************************************/

#endif /* SGTL5000_H_ */
