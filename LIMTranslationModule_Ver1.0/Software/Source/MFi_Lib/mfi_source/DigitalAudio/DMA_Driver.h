/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_DMA_H_
#define IAP_DMA_H_

/**********************************************************************************************************************/
/*                                 				    Includes Section                                         		  */
/**********************************************************************************************************************/
#include "ProjectTypes.h"

/**********************************************************************************************************************/
/*                                 				    Typedef Section              		                              */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                             				    Defines & Macros Section            		                          */
/**********************************************************************************************************************/

/* Following defines are needed as workaround since processor expert does not provides methods to do so */

#define DMA_GET_CURRENT_DEST_ADDR			((uint32_t)DMA_DADDR_REG(DMA_BASE_PTR,0))
#define DMA_GET_CURRENT_SRC_ADDR			((uint32_t)DMA_SADDR_REG(DMA_BASE_PTR,1))

/* K60, K70 */
#define DMA_ENABLE_TX_FIFO_REQUEST			I2S0_TCSR |= I2S_TCSR_FRDE_MASK
#define DMA_ENABLE_RX_FIFO_REQUEST			I2S0_RCSR |= I2S_RCSR_FRDE_MASK
#define DMA_TX_PERIPHERAL_DEVICE			I2S0_TDR0
#define DMA_RX_PERIPHERAL_DEVICE			I2S0_RDR0

/* K40 */
//#define DMA_ENABLE_TX_FIFO_REQUEST			I2S0_IER |= I2S_IER_TDMAE_MASK
//#define DMA_ENABLE_RX_FIFO_REQUEST			I2S0_IER |= I2S_IER_RDMAE_MASK
//#define DMA_TX_PERIPHERAL_DEVICE			I2S0_TX0
//#define DMA_RX_PERIPHERAL_DEVICE			I2S0_RX0

#define DMA_BYTE_COUNT						8

/**********************************************************************************************************************/
/*                           			     Function-like Macros Section           		                          */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                            				   Extern Constants Section              			                      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                               			   Extern Variables Section         		                              */
/**********************************************************************************************************************/
extern uint16_t DMA_gwTransferCount;

/**********************************************************************************************************************/
/*                            			 	  Function Prototypes Section                                 			  */
/**********************************************************************************************************************/
void iAP_DMA_vfnInit(void);
void iAP_DMA_vfnStartCopyTransfer(void);
void iAP_DMA_vfnSetSourceAddressForCopyTransfer(uint32_t dwSourceAddress);
void iAP_DMA_vfnSetDestinationAddressForCopyTransfer(uint32_t dwDestinationAddress);
void iAP_DMA_vfnSetTransferSizeForCopy(uint32_t dwReceiveSize);

/**********************************************************************************************************************/

#endif /* IAP_DMA_H_ */
