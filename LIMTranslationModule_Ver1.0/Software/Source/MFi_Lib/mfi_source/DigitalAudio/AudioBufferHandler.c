/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/**********************************************************************************************************************/
/*       			                                Includes Section                                         		  */
/**********************************************************************************************************************/
#include "AudioBufferHandler.h"
#include "iAP_Timer.h"
#include "DigitalAudioDriver.h"
/**********************************************************************************************************************/
/*                        				            Typedef Section                                         		  */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                           			      Function Prototypes Section            			                      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                     				            Defines & Macros Section                                    		  */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                                			    Global Variables Section                                 			  */
/**********************************************************************************************************************/

/** Buffer handler status variable */
uint8_t AudioBufferHandler_gbStatus = 0;
/** Global status of digital audio driver*/
extern uint8_t DAD_gbStatus;
/** Array to be implemented as ring buffer for Tx */
#if (AUDIO_BUFFER_SAMPLE_SIZE == 4)
	#if defined(__CWCC__) || defined(__GNUC__) || defined(__CC_ARM)
		uint32_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE] __attribute__((aligned (AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE)));
	#elif defined(__IAR_SYSTEMS_ICC__)
		#pragma data_alignment = AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE
		uint32_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE];
	#endif
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 3)      
	#if defined(__CWCC__) || defined(__GNUC__) || defined(__CC_ARM)
		uint32_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE] __attribute__((aligned (AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE)));
	#elif defined(__IAR_SYSTEMS_ICC__)
		#pragma data_alignment = AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE
		uint32_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE];
	#endif
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 2)
	#if defined(__CWCC__) || defined(__GNUC__) || defined(__CC_ARM)
		uint16_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE] __attribute__((aligned (AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE)));
	#elif defined(__IAR_SYSTEMS_ICC__)
		#pragma data_alignment = AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE
		uint16_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE];
	#endif
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 1)
	#if defined(__CWCC__) || defined(__GNUC__) || defined(__CC_ARM)
		uint8_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE] __attribute__((aligned (AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE)));
	#elif defined(__IAR_SYSTEMS_ICC__)
		#pragma data_alignment = AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE
		uint8_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE];
	#endif
#endif

#ifndef IAPI_INTERFACE_USB_DEV
//uint8_t g_baAudioRecvBuffer[255];
#endif
/**********************************************************************************************************************/
/*                         			      	    Static Variables Section                		                      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                             			        Global Constants Section                     			              */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                             				    Static Constants Section             	                 		      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                                   			   Functions Section           			                              */
/**********************************************************************************************************************/

/** ***********************************************************************************************
*
* @brief    Function to initialize the Audio Buffer Handler module. This routine configures the 
* 			I2S shifter register to work with 8, 16 and 24 bits
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
void AudioBufferHandler_vfnInit(void)
{
	AudioBufferHandler_gbStatus = 0;
	AUDIO_BUFFER_SET_I2S_SHIFTER;
}

/** ***********************************************************************************************
*
* @brief    Function to fill the audio buffer up the half of its capacity.
* 
* @param   	dwReceiveSize indicates the buffer level in a time instant
* 
* @return  	void
*          
**************************************************************************************************/
void AudioBufferHandler_vfnFillHalfBuffer(uint32_t dwDiffSentRcvBuffer)
{
	/* If OutRingBuffer is half filled, start SSI transfer */
	if((AUDIO_BUFFER_MID_PERCENT)< dwDiffSentRcvBuffer)
	{
		uint8_t bIndex = 0;
		/* When the buffer is already approximately at 50% then*/
		/* Set flag that indicates audio buffer Rx is already at 50%*/
		AUDIO_BUFFER_HANDLER_SET_STATUS(AUDIO_BUFFER_HANDLER_RX_STARTED);
		/* Clear SSI TX FIFO*/
		while(bIndex++ < AUDIO_IIS_FIFO_SIZE)
		{
			DMA_TX_PERIPHERAL_DEVICE = 0x00;
		}
		/* Enable SSI module to start transfer*/
		(void)iAPSSI1_EnableTransfer(iAPSSI1_DeviceData,LDD_SSI_TRANSMITTER);
		/* Enable timers to detect audio communication when never expired*/
		IAP_TIMER_ENABLE_CHN(DAD_TIMEOUT_CHN);
		/* Start descompensation rate monitoring */
		DAD_gbStatus |= (1 << DAD_COMPENSATION_RATE_STARTED);

		IAP_TIMER_ENABLE_CHN(TCO_TIMEOUT_CHN);
	}
}

/** ***********************************************************************************************
*
* @brief    Calculates the audio buffer occupancy based on the write and read pointer.
* 
* @param   	DestAddr Address where the writing pointer is pointing
*
* @param   	SrcAddr Address where the reading pointer is pointing
* 
* @return  	absolute difference indicates amount of bytes into the audio buffer
*          
**************************************************************************************************/
uint32_t AudioBufferHandler_dwfnCalculateOccupancy(uint32_t dwDestAddr, uint32_t dwSrcAddr)
{
    uint32_t dwResult;
    /* This calculate the address differences in bytes units, and mask by the max buffer value in bytes*/
	dwResult = ((uint32_t)(dwDestAddr) - (uint32_t)(dwSrcAddr))&((uint32_t)(AUDIO_OUTPUT_RING_BUFFER_SIZE*SAMPLE_SIZE_FOR_CALCULATE_OCCUPANCY)-1);
	/* And divide the result by SAMPLE_SIZE to convert the result in samples units*/
	dwResult /= SAMPLE_SIZE_FOR_CALCULATE_OCCUPANCY;
    
    return(dwResult);
}
