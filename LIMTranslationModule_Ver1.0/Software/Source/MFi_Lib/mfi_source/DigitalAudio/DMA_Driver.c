/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/**********************************************************************************************************************/
/*       			                                Includes Section                                         		  */
/**********************************************************************************************************************/
#include "DMA_Driver.h"

/**********************************************************************************************************************/
/*                        				            Typedef Section                                         		  */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                           			      Function Prototypes Section            			                      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                     				            Defines & Macros Section                                    		  */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                                			    Global Variables Section                                 			  */
/**********************************************************************************************************************/
uint16_t DMA_gwTransferCount;				/**< Configures the mayor loop count uses to transmit data over IIS */

/**********************************************************************************************************************/
/*                         			      	    Static Variables Section                		                      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                             			        Global Constants Section                     			              */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                             				    Static Constants Section             	                 		      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                                   			   Functions Section           			                              */
/**********************************************************************************************************************/

/** **********************************************************************************************
*
* @brief    Initialize one DMA channel to copy data from memory to memory, this channel is software
* 			triggered. Initialize a second DMA channel to transfer data from memory to IIS, this
* 			channel is triggered by IIS peripheral. 
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
void iAP_DMA_vfnInit(void)
{	
	(void)iAPDMACopy1_AllocateChannel(iAPDMACopy1_DeviceData);
	(void)iAPDMACopy1_EnableChannel(iAPDMACopy1_DeviceData);
	(void)iAPDMATx1_AllocateChannel(iAPDMATx1_DeviceData);
	(void)iAPDMATx1_EnableChannel(iAPDMATx1_DeviceData);
	DMA_ENABLE_TX_FIFO_REQUEST;
}

/** **********************************************************************************************
*
* @brief    Set source address used by the copy from memory to memory DMA transfer. 
* 
* @param   	dwSourceAddress Address at the beginning of the buffer where the data to be copied is
* 			allocated.
* 
* @return  	void
*          
**************************************************************************************************/
void iAP_DMA_vfnSetSourceAddressForCopyTransfer(uint32_t dwSourceAddress)
{
	(void)iAPDMACopy1_SetSourceAddress(iAPDMACopy1_DeviceData, dwSourceAddress);
}

/** **********************************************************************************************
*
* @brief    Set destination address used by the copy from memory to memory DMA transfer. 
* 
* @param   	dwDestinationAddress Address at the beginning of the buffer where the data to be copied is
* 			allocated.
* 
* @return  	void
*          
**************************************************************************************************/
void iAP_DMA_vfnSetDestinationAddressForCopyTransfer(uint32_t dwDestinationAddress)
{
	(void)iAPDMACopy1_SetDestinationAddress(iAPDMACopy1_DeviceData, dwDestinationAddress);
}

/** **********************************************************************************************
*
* @brief    Set size used by the copy from memory to memory DMA transfer
* 
* @param   	dwReceiveSize Amount of bytes to be transfered
* 
* @return  	void
*          
**************************************************************************************************/
void iAP_DMA_vfnSetTransferSizeForCopy(uint32_t dwReceiveSize)
{
	(void)iAPDMACopy1_SetByteCount(iAPDMACopy1_DeviceData, dwReceiveSize);
}

/** **********************************************************************************************
*
* @brief    Request a software trigger to start DMA transfer
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
void iAP_DMA_vfnStartCopyTransfer(void)
{
	(void)iAPDMACopy1_StartTransfer(iAPDMACopy1_DeviceData);
}

