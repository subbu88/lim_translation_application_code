/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *		This module contains all the functions needed to communicate with the TCO device.
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/

#include "ProjectTypes.h"
#include "MFi_Config.h"
#include "AudioClkSyncDriver.h"
#include "iAPTimer1.h"
#include "iAP_Timer.h"
#include "AudioBufferHandler.h"
#include "AudioClkSyncTimer.h"
#include "iAP_USB_Audio_Interface.h"
#include "Si5351.h"
/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/

/** ****************Async Feedback functions**************************/
#ifdef USB_ASYNC	
static void ACSD_vfnSetFeedback(uint32_t dwFreq);
static void ACSD_vfnCorrectFeedback(uint32_t dwCurrentSampleFreq, int32_t dwDeltaFreq);
static void ACSD_vfnSendFeedback(void);
#endif
static uint64_t ACSD_qwfnCalculateDeltaFeedback(void);


/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/

#define ACSD_MILLISECS_PER_TICKS_SAMPLE				(20)
#define ACSD_TIMER_FREQUENCY						(CPU_BUS_CLK_HZ)
#define ACSD_MILLISECS_PER_SEC						(1000)
#define ACSD_AVERAGE_THRESHOLD						(100)
#define ACSD_TIMER_TICKS_PER_MS						(ACSD_TIMER_FREQUENCY/ACSD_MILLISECS_PER_SEC)

#define ACSD_LPF_COEFF								(64)
#define ACSD_LPF_Q_SHIFT							(8)
#define ACSD_LPF_NOISE_THLD							(2 * (int32_t)ACSD_TIMER_FREQUENCY / 1000000) /* 2us */

/** There are some concurrencies between DMA and USB interrupts that might generate
 *  measure errors with period calculation in both parts so that some boundaries has 
 *  been defined to avoid measure process when concurrencies happen.   */
/* USB and Interrupts Periods are phase shift constantly and there are 2 position that are in concurrency
 *  When DMA period is delayed by 300us as well as USB period is delayed by 700us approximately
 *  and when both period are in phase*/

#define ACSD_USB_MID_THR_FACTOR				(1389)		/* 72% */
#define ACSD_DMA_MID_THR_FACTOR				(3571)		/* 28% */

#define ACSD_LATERAL_THR_FACTOR				(28571)	    /* 3.5% */


/*                    _________           ________     */
/*             USB __/         \_________/        \___ */
/*  (DMA thresholds)| |--|_____|__           ________  */
/*             DMA _L_R__/     |  \_________/        \ */
/*                      | |----| (USB thresholds)      */
/*                      L R                            */
#define ACSD_USB_TICKS_RIGHT_THRESHOLD			( (ACSD_TIMER_FREQUENCY/ACSD_USB_MID_THR_FACTOR) - \
													(ACSD_TIMER_FREQUENCY/ACSD_LATERAL_THR_FACTOR) ) 
#define ACSD_USB_TICKS_LEFT_THRESHOLD			( (ACSD_TIMER_FREQUENCY/ACSD_USB_MID_THR_FACTOR) + \
													(ACSD_TIMER_FREQUENCY/ACSD_LATERAL_THR_FACTOR) )
#define ACSD_DMA_TICKS_RIGHT_THRESHOLD			( (ACSD_TIMER_FREQUENCY/ACSD_DMA_MID_THR_FACTOR) - \
													(ACSD_TIMER_FREQUENCY/ACSD_LATERAL_THR_FACTOR) ) 
#define ACSD_DMA_TICKS_LEFT_THRESHOLD			( (ACSD_TIMER_FREQUENCY/ACSD_DMA_MID_THR_FACTOR) + \
													(ACSD_TIMER_FREQUENCY/ACSD_LATERAL_THR_FACTOR) ) 
														
/*USB     ________          ________     */
/*    ___/        \________/        \	 */
/*   DMA  ________          ________     */
/*  _____/        \________/        \    */
/*      |-|      |-|                     */
#define ACSD_USBDMA_TICKS_THRESHOLD				(ACSD_TIMER_FREQUENCY/ACSD_LATERAL_THR_FACTOR/2)   
                                                       	

#define ACSD_FREQUENCY_48KHZ 					(48000)
#define ACSD_FREQUENCY_44_1KHZ 					(44100)
#define ACSD_FREQUENCY_32KHZ 					(32000)

#define ACSD_COMPENSATOR_FREQ					(0x2000) //0.5Hz Q14

#define ABS(a)			((a<0)?-a:a)

#define ACSD_PERIODS_CONCURRENCY_FREE			(1)
#define ACSD_PERIODS_IN_CONCURRENCY 			(0)

/* Deviation in PPM to be injected in the clock to compensate from the limits of the buffer to 50% */
#define ACSD_COMP_PPM				(10)
#define ACSD_COMP_UP_RATIO			(((uint64_t)1000000 + ACSD_COMP_PPM) << ACSD_RATIO_Q_SHIFT) / 1000000
#define ACSD_COMP_DOWN_RATIO		(((uint64_t)1000000 - ACSD_COMP_PPM) << ACSD_RATIO_Q_SHIFT) / 1000000

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

typedef enum
{
	ACSD_STATUS_BUSY = 0
}eACSDStatus;

typedef enum
{
	ACSD_DMA_PERIOD_MEASURE_READY = 0,
	ACSD_USB_PERIOD_MEASURE_READY,
	ACSD_MEASURE_READY,
	ACSD_STATUS_FREQ_UP,
	ACSD_STATUS_FREQ_DOWN,
	ACSD_WAIT_MEASURE_READY_STATE
}eACSDMeasureStatus;

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
uint8_t ACSD_gbMeasureReadyStatus=0;

#ifdef USB_ASYNC
extern uint32_t DAD_gdwCurrentFrequency;
#endif

sSM ACSD_gslStateMachine;

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static uint64_t ACSD_qwTargetRatio;
static uint64_t ACSD_qwCompRatio;
static int32_t ACSD_dwUSBAvarageTicks;
static int32_t ACSD_dwDMAAvarageTicks;

#ifdef USB_ASYNC
static int32_t ACSD_gdwFeedback; 			/**< Amount of feedback to maintain the audio buffer stable */
#endif

volatile static uint32_t ACSD_dwDMATimerSample;
volatile static uint32_t ACSD_dwUSBTimerSample;
/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

#ifdef USB_ASYNC	
/** ***********************************************************************************************
*
* @brief    
* 
* @param   	
* 
* @return  	
*          
**************************************************************************************************/
void ACSD_vfnResetFeedback(void)
{
	ACSD_vfnSetFeedback(DAD_gdwCurrentFrequency);
}

/** ***********************************************************************************************
*
* @brief    
* 
* @param   	
* 
* @return  	
*          
**************************************************************************************************/
static void ACSD_vfnSetFeedback(uint32_t dwFreq)
{
	ACSD_gdwFeedback=(int32_t)(dwFreq<<ACSD_FRACTION_BITS)/ACSD_MILLISECS_PER_SEC;
}



/** ***********************************************************************************************
*
* @brief    
* 
* @param   	
* 
* @return  	
*          
**************************************************************************************************/
static void ACSD_vfnCorrectFeedback(uint32_t dwCurrentSampleFreq, int32_t dwDeltaFreq)
{
//	int32_t dwFeedBack = gdwFeedback;
	ACSD_gdwFeedback  = (int32_t)dwCurrentSampleFreq << ACSD_FRACTION_BITS;
	ACSD_gdwFeedback -= dwDeltaFreq;
	ACSD_gdwFeedback /= ACSD_MILLISECS_PER_SEC;

}

/** ***********************************************************************************************
*
* @brief    
* 
* @param   	
* 
* @return  	
*          
**************************************************************************************************/
static void ACSD_vfnSendFeedback(void)
{
	if(g_bUSBAudioStatus & (1 << USB_FEEDBACK_SENT))
	{
		g_bUSBAudioStatus &= ~(1 << USB_FEEDBACK_SENT);
		(void)USB_Class_Audio_Send_Data(0, 5, (uint8_t *)&ACSD_gdwFeedback,3);
	}

}

#endif

void ACSD_vfnInit(void)
{
#ifdef DAD_TCO
	SI5351_vfnInit();
#else	
	ACSD_vfnSetFeedback(ACSD_FREQUENCY_44_1KHZ);
#endif
	ACSD_gbMeasureReadyStatus=0;
	/** @todo Function Return */
	(void)AudioClkSyncTimer_SetPeriodTicks(AudioClkSyncTimer_DeviceData, 0xFFFFFFFF);
}

/** ***********************************************************************************************
*
* @brief    
* 
* @param   	
* 
* @return  	
*          
**************************************************************************************************/
void ACSD_vfnInitVariables(void)
{
	ACSD_gbMeasureReadyStatus=0;
	ACSD_qwCompRatio = 1 << ACSD_RATIO_Q_SHIFT;
#ifdef USB_ASYNC
	ACSD_vfnResetFeedback();
#else
	/** @todo Function Return */
	(void)SI5351_vfnResetFrequency();
#endif
}

/** ***********************************************************************************************
*
* @brief    
* 
* @param   	
* 
* @return  	
*          
**************************************************************************************************/
void ACSD_vfnDriver(uint32_t dwBufferDifference)
{ 
	uint64_t qwFreqRatio; 
#ifndef USB_ASYNC	
	uint8_t bSi5351Status;
#endif
	if(AUDIO_BUFFER_HANDLER_CHECK_STATUS(AUDIO_BUFFER_HANDLER_RX_STARTED))
	{
		if( !(ACSD_gbMeasureReadyStatus & (1<<ACSD_STATUS_FREQ_UP)) && \
				dwBufferDifference < AUDIO_BUFFER_DOWN_PERCENT)  
		{
#ifdef USB_ASYNC
			ACSD_vfnCorrectFeedback(dwCurrSampleFreq, -ACSD_COMPENSATOR_FREQ);
#else
			ACSD_qwCompRatio = ACSD_COMP_UP_RATIO;
#endif
			ACSD_gbMeasureReadyStatus |= 1<<ACSD_STATUS_FREQ_UP;
			ACSD_gbMeasureReadyStatus &= ~(1<<ACSD_STATUS_FREQ_DOWN);
		}
		else if( !( ACSD_gbMeasureReadyStatus & (1<<ACSD_STATUS_FREQ_DOWN)) && \
				dwBufferDifference > AUDIO_BUFFER_UP_PERCENT )
		{
#ifdef USB_ASYNC
			ACSD_vfnCorrectFeedback(dwCurrSampleFreq, ACSD_COMPENSATOR_FREQ);
#else
			ACSD_qwCompRatio = ACSD_COMP_DOWN_RATIO;
#endif
			ACSD_gbMeasureReadyStatus |= 1<<ACSD_STATUS_FREQ_DOWN;
			ACSD_gbMeasureReadyStatus &= ~(1<<ACSD_STATUS_FREQ_UP);
		}
        if( ((ACSD_gbMeasureReadyStatus & (1<<ACSD_STATUS_FREQ_UP)) && (dwBufferDifference > AUDIO_BUFFER_MID_PERCENT)) \
        		|| ((ACSD_gbMeasureReadyStatus & (1<<ACSD_STATUS_FREQ_DOWN)) && (dwBufferDifference < AUDIO_BUFFER_MID_PERCENT)) )
		{
        	ACSD_qwCompRatio = 1 << ACSD_RATIO_Q_SHIFT;
        	ACSD_gbMeasureReadyStatus &= ~((1<<ACSD_STATUS_FREQ_UP) | (1<<ACSD_STATUS_FREQ_DOWN));
		}

		if (IAP_TIMER_CHECK_CHN_FLAG(TCO_TIMEOUT_CHN))
		{
			ACSD_gbMeasureReadyStatus |= (1<<ACSD_WAIT_MEASURE_READY_STATE);
			ACSD_gbMeasureReadyStatus &= ~((1<<ACSD_MEASURE_READY) | (1<<ACSD_USB_PERIOD_MEASURE_READY) \
											| (1<<ACSD_DMA_PERIOD_MEASURE_READY));
			IAP_TIMER_DISABLE_CHN(TCO_TIMEOUT_CHN);
		}
		
		if ((ACSD_gbMeasureReadyStatus & (1<<ACSD_WAIT_MEASURE_READY_STATE)) && \
				(ACSD_gbMeasureReadyStatus & (1<<ACSD_MEASURE_READY)))
		{
			qwFreqRatio=ACSD_qwfnCalculateDeltaFeedback();
#ifdef USB_ASYNC
			ACSD_vfnCorrectFeedback(dwCurrSampleFreq, dwDeltaFreq);
#else
			bSi5351Status=SI5351_vfnCorrectFrequency(qwFreqRatio);
			if(SI5351_STATUS_IDLE == bSi5351Status)
#endif
			{
				ACSD_gbMeasureReadyStatus &= ~(1<<ACSD_WAIT_MEASURE_READY_STATE);
				IAP_TIMER_ENABLE_CHN(TCO_TIMEOUT_CHN);
				IAP_TIMER_SET_PERIOD(TCO_TIMEOUT_CHN,IAP_TIMEOUT_10SEC_PERIOD);
			}
		}
	}
	
#ifdef USB_ASYNC	
	ACSD_vfnSendFeedback();
#endif
}	

/** ***********************************************************************************************
*
* @brief    Provides the ability to Monitor DMA interrupt period 
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
void ACSD_vfnDMAFrameMonitor(void)
{
	static uint8_t bFrameNum=0;
	static uint8_t bAverageCount=0;
	static uint32_t dwDMAInitalTimerSample;
	static int32_t lDeltaSum;
	static uint8_t bPDeltaCount;
	static uint8_t bNDeltaCount;
	static int32_t dwDMAAverage=0;
	int32_t lDelta;
	
	ACSD_dwDMATimerSample=AudioClkSyncTimer_GetCounterValue(AudioClkSyncTimer_DeviceData);
	
	if ((ACSD_gbMeasureReadyStatus & ((1<<ACSD_USB_PERIOD_MEASURE_READY) | \
										(1<<ACSD_DMA_PERIOD_MEASURE_READY))) == ((1<<ACSD_USB_PERIOD_MEASURE_READY) | \
										(1<<ACSD_DMA_PERIOD_MEASURE_READY)))
	{
		ACSD_gbMeasureReadyStatus |= 1<<ACSD_MEASURE_READY;
	}

	if (!(ACSD_gbMeasureReadyStatus&((1<<ACSD_MEASURE_READY) | (1<<ACSD_DMA_PERIOD_MEASURE_READY))))
	{
		if (bFrameNum == 0)
		{
			dwDMAInitalTimerSample = ACSD_dwDMATimerSample;
			bFrameNum++;
		}
		else if(bFrameNum > 0)
		{
			bFrameNum++;
			if (bFrameNum > ACSD_MILLISECS_PER_TICKS_SAMPLE)
			{
				ACSD_dwDMATimerSample = (dwDMAInitalTimerSample -  ACSD_dwDMATimerSample) << ACSD_LPF_Q_SHIFT;

				lDelta = (int32_t)ACSD_dwDMATimerSample - dwDMAAverage;

				if(lDelta > (ACSD_LPF_NOISE_THLD << ACSD_LPF_Q_SHIFT))
				{
					lDeltaSum += ACSD_dwDMATimerSample;
					bNDeltaCount = 0;
					bPDeltaCount++;
					if(bPDeltaCount == 4)
					{
						dwDMAAverage = lDeltaSum/4;
						bAverageCount = 0;
						lDeltaSum = 0;
						bPDeltaCount = 0;
					}
				}
				else if(lDelta < -(ACSD_LPF_NOISE_THLD << ACSD_LPF_Q_SHIFT))
				{
					lDeltaSum += ACSD_dwDMATimerSample;
					bPDeltaCount = 0;
					bNDeltaCount++;
					if(bNDeltaCount == 4)
					{
						dwDMAAverage = lDeltaSum/4;
						bAverageCount = 0;
						lDeltaSum = 0;
						bNDeltaCount = 0;
					}
				}
				else
				{
					lDeltaSum = 0;
					bPDeltaCount = 0;
					bNDeltaCount = 0;

					dwDMAAverage = dwDMAAverage + lDelta/ACSD_LPF_COEFF;
					if (bAverageCount == ACSD_AVERAGE_THRESHOLD - 1)
					{
						ACSD_dwDMAAvarageTicks = dwDMAAverage >> ACSD_LPF_Q_SHIFT;
						bAverageCount=0;
						ACSD_gbMeasureReadyStatus |= (1<<ACSD_DMA_PERIOD_MEASURE_READY);
					}
					else
					{
						bAverageCount++;
					}
				}

				bFrameNum = 0;
			}	
		}	
	}	
}

/** ***********************************************************************************************
*
* @brief    Provides the ability to Monitor USB interrupt period 
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
void ACSD_vfnUSBFrameMonitor(void)
{
	static uint8_t bFrameNum=0;
	static uint8_t bAverageCount=0;
	static uint32_t dwUSBInitalTimerSample;
	static int32_t lDeltaSum;
	static uint8_t bPDeltaCount;
	static uint8_t bNDeltaCount;
	static int32_t dwUSBAverage=0;
	int32_t lDelta;
	
	ACSD_dwUSBTimerSample = AudioClkSyncTimer_GetCounterValue(AudioClkSyncTimer_DeviceData);
	
	if ((ACSD_gbMeasureReadyStatus & ((1<<ACSD_USB_PERIOD_MEASURE_READY) | \
										(1<<ACSD_DMA_PERIOD_MEASURE_READY))) == ((1<<ACSD_USB_PERIOD_MEASURE_READY) | \
										(1<<ACSD_DMA_PERIOD_MEASURE_READY)))
	{
		ACSD_gbMeasureReadyStatus |= 1<<ACSD_MEASURE_READY;
	}

	if (!(ACSD_gbMeasureReadyStatus&((1<<ACSD_MEASURE_READY) | (1<<ACSD_USB_PERIOD_MEASURE_READY))))
	{
		if (bFrameNum == 0)
		{
			dwUSBInitalTimerSample = ACSD_dwUSBTimerSample;
			bFrameNum++;
		}
		else if(bFrameNum > 0)
		{
			bFrameNum++;
			if (bFrameNum > ACSD_MILLISECS_PER_TICKS_SAMPLE)
			{
				ACSD_dwUSBTimerSample = (dwUSBInitalTimerSample -  ACSD_dwUSBTimerSample) << ACSD_LPF_Q_SHIFT;

				lDelta = (int32_t)ACSD_dwUSBTimerSample - dwUSBAverage;

				if(lDelta > (ACSD_LPF_NOISE_THLD << ACSD_LPF_Q_SHIFT))
				{
					lDeltaSum += ACSD_dwUSBTimerSample;
					bNDeltaCount = 0;
					bPDeltaCount++;
					if(bPDeltaCount == 4)
					{
						dwUSBAverage = lDeltaSum/4;
						bAverageCount = 0;
						lDeltaSum = 0;
						bPDeltaCount = 0;
					}
				}
				else if(lDelta < -(ACSD_LPF_NOISE_THLD << ACSD_LPF_Q_SHIFT))
				{
					lDeltaSum += ACSD_dwUSBTimerSample;
					bPDeltaCount = 0;
					bNDeltaCount++;
					if(bNDeltaCount == 4)
					{
						dwUSBAverage = lDeltaSum/4;
						bAverageCount = 0;
						lDeltaSum = 0;
						bNDeltaCount = 0;
					}
				}
				else
				{
					lDeltaSum = 0;
					bPDeltaCount = 0;
					bNDeltaCount = 0;

					dwUSBAverage = dwUSBAverage + lDelta/ACSD_LPF_COEFF;
					if (bAverageCount == ACSD_AVERAGE_THRESHOLD - 1)
					{
						ACSD_dwUSBAvarageTicks = dwUSBAverage >> ACSD_LPF_Q_SHIFT;
						bAverageCount=0;
						ACSD_gbMeasureReadyStatus |= (1<<ACSD_USB_PERIOD_MEASURE_READY);
					}
					else
					{
						bAverageCount++;
					}
				}

				bFrameNum = 0;
			}	
		}	
	}	
}

/** ***********************************************************************************************
*
* @brief    Provides the ability to Calculate delta frequency between DMA interrupt period and 
* 				USB interrupt period
* 
* @param   	uint32_t: Sample rate used
* 
* @return  	int64_t: Q14 Delta Frequency
*          
**************************************************************************************************/
static uint64_t ACSD_qwfnCalculateDeltaFeedback(void)
{
	uint64_t qwFreqRatio;

	qwFreqRatio  = (uint64_t)ACSD_dwDMAAvarageTicks<<32;
	qwFreqRatio /= ACSD_dwUSBAvarageTicks;
	qwFreqRatio = (qwFreqRatio << ACSD_RATIO_Q_SHIFT) / ACSD_qwTargetRatio;
	qwFreqRatio = (qwFreqRatio << ACSD_RATIO_Q_SHIFT) / ACSD_qwCompRatio;
	
	return qwFreqRatio;
}

void ACSD_vfnSetRatio(uint64_t qwRatio)
{
	ACSD_qwTargetRatio = qwRatio;
}
