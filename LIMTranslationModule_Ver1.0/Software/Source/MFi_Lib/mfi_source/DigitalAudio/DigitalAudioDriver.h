/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef DIGITALAUDIODRIVER_H_
#define DIGITALAUDIODRIVER_H_

/**********************************************************************************************************************/
/*                                 				    Includes Section                                         		  */
/**********************************************************************************************************************/
#include "ProjectTypes.h"
#include "MFi_Config.h"
#include "SGTL5000.h"
#include "AudioBufferHandler.h"
#include "DMA_Driver.h"
#ifdef IAPI_INTERFACE_USB_DEV
//#include "usb_descriptor.h"
#include "iAP_USBDevice.h"
#endif
#include "iAP_Timer.h"
#include "iAP_USB_Audio_Interface.h"

/**********************************************************************************************************************/
/*                             				    Defines & Macros Section            		                          */
/**********************************************************************************************************************/
/** Default audio sampling frequency */
#define DAD_FREQUENCY_44_1KHZ				(44100)
/** Audio channels left and right */
#define DAD_AUDIO_CHANNEL_NUMBER			(2)

#define DAD_TRANSFER_COUNT_FACTOR_NUM			(AUDIO_BUFFER_SAMPLE_SIZE*AUDIO_CHANNEL_NUMBER)
#define DAD_TRANSFER_COUNT_FACTOR_DEN			(DMA_BYTE_COUNT*DAD_10_14_FORMAT_DIV_FACT)


/******************** Macros for Feedback method ***********************/
/** Specify period in milliseconds when the feedback must be issued */
#define DAD_FEEDBACK_PERIOD_MS				(8)

/** 10.14 format is required by USB audio class */
#define DAD_FEEDBACK_44_1_SAMPLES 			(0x0B0666)
#define DAD_FEEDBACK_48_SAMPLES 			(0x0C0000)
#define DAD_FEEDBACK_96_SAMPLES 			(0x180000)
/** Multiplier factor for 10.14 format */
#define DAD_10_14_FORMAT_MULT_FACT			(16384)
/** Divider factor for 10.14 format */
#define DAD_10_14_FORMAT_DIV_FACT			(1000)
/** 1/8 of sample in 10.14 format */
#define DAD_ONE_SAMPLE_10_14_FORMAT			(DAD_10_14_FORMAT_MULT_FACT / DAD_FEEDBACK_PERIOD_MS)

/***************** Macros for SRC and Interpolation **********************/
#define DAD_SRC
/** Number of samples used to interpolate*/
#define DAD_SRC_WINDOWS_SAMPLES				(400)
/** Delay in milliseconds*/
#define DAD_PADLOCK_COMPENSATION_DELAY		(1000)
/** Macro to define the Q format to use in linear interpolation method*/
#define DAD_QSRC_INT_MASK 					(0xFFF00000)
#define DAD_QSRC_FRAC_MASK					(0x000FFFFF)
#define DAD_QSRC_FRAC_SHIFTER 				(20)
#define DAD_QSRC_INT_SHIFTER 				(12)
/** Macro to Convert the Qxx to Q0.31*/
#define DAD_QSRC_TO_Q31_SHIFTER 			(DAD_QSRC_INT_SHIFTER-1)
/** Define a 1.0 value in Q0.31 format*/
#define DAD_Q31_ONE							(0x7FFFFFFF)
/** Define a 0.5 value*/
#define DAD_HALF_UNITY_QSRC_FORMAT				(1<<(DAD_QSRC_FRAC_SHIFTER-1))
#define DAD_ONE_UNITY_QSRC_FORMAT				(1<<(DAD_QSRC_FRAC_SHIFTER))
#define DAD_UP_FRACTION_QSRC_FORMAT				((DAD_SRC_WINDOWS_SAMPLES<<DAD_QSRC_FRAC_SHIFTER)/(DAD_SRC_WINDOWS_SAMPLES+1))
#define DAD_DOWN_FRACTION_QSRC_FORMAT			((DAD_SRC_WINDOWS_SAMPLES<<DAD_QSRC_FRAC_SHIFTER)/(DAD_SRC_WINDOWS_SAMPLES-1))
/** Due fraction errors the threshold may be is not reached exactly in its accumulation so subtract 0.5 is helpful in order to be reached*/
#define DAD_THRESHOLD_QSRC_FORMAT				((DAD_SRC_WINDOWS_SAMPLES<<DAD_QSRC_FRAC_SHIFTER)-DAD_HALF_UNITY_QSRC_FORMAT) 

#define DAD_CHANNELS_OFFSET						AUDIO_CHANNEL_NUMBER
#define DAD_SRC_CONVERSION_DONE					(1)
#define DAD_SRC_CONVERSION_NOT_READY			(0)
#define DAD_PREPARE_TO_QFMT_SHIFTER				(32)
/** This macro represent 5% (1/20) useful for descompensation rate*/
#define DAD_DEN_FACTOR_PERCENT_FOR_DESC_RATE	(20) 
#define DAD_CNT_INC				(1)
#define DAD_EXTRAS_SAMPLES		((DAD_CNT_INC*DMA_BYTE_COUNT)/(AUDIO_BUFFER_SAMPLE_SIZE*AUDIO_CHANNEL_NUMBER))
/**********************************************************************************************************************/
/*                                 				    Typedef Section              		                              */
/**********************************************************************************************************************/
#if defined(USB_SYNC) && !defined(DAD_TCO)
#if (AUDIO_BUFFER_SAMPLE_SIZE >= 3)
typedef int32_t qFmt;
typedef union{
	struct 
	{
		int32_t hi;
	}word;
	int32_t dwData;
}WDIV; 
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 2)
typedef int16_t qFmt;			
typedef union{
	struct 
	{
		int16_t lo;
		int16_t hi;
	}word;
	int32_t dwData;
}WDIV; 
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 1)
typedef int8_t qFmt;			
typedef union{
	struct 
	{
		int8_t lo;
		int8_t mdlo;
		int8_t mdhi;
		int8_t hi;
	}word;
	int32_t dwData;
}WDIV; 
#endif
#endif

typedef enum
{
	DAD_WAIT_COMPENSATION = 0,		/**< used to control the compensation of the feedback */
	DAD_COMPENSATION_RATE_READY,
	DAD_COMPENSATION_RATE_STARTED
}_DAD_Status;
/**********************************************************************************************************************/
/*                           			     Function-like Macros Section           		                          */
/**********************************************************************************************************************/
#if (MFI_SGTL_ENABLE == _TRUE_)
	#define DAD_AUDIO_CODEC_INIT				SGTL5000_vfnInit()
	#define DAD_CHANGE_FREQUENCY(Freq)			SGTL5000_bfnChangeFrequency(Freq)
	#define DAD_AUDIO_CODEC_CALLBACK			SGTL5000_vfnDriver()
	#ifdef USB_ASYNC
		#define DAD_ACSD_INIT						ACSD_vfnInit()
		#define DAD_ACSD_CALLBACK					NOP
	#else
		#ifdef DAD_TCO
			#define DAD_ACSD_INIT						ACSD_vfnInit()
			#define DAD_ACSD_CALLBACK					SI5351_vfnDriver()
		#else
			#define DAD_ACSD_INIT						NOP
			#define DAD_ACSD_CALLBACK					NOP
		#endif
	#endif
#else
	#define DAD_AUDIO_CODEC_INIT				NOP
	#define DAD_CHANGE_FREQUENCY(Freq)			NOP
	#define DAD_AUDIO_CODEC_CALLBACK			NOP
	#define DAD_ACSD_CALLBACK					NOP
	#define DAD_ACSD_INIT						NOP
#endif

#define DAD_AUDIO_BUFFER_INIT				AudioBufferHandler_vfnInit()
#define DAD_AUDIO_DMA_INIT 					iAP_DMA_vfnInit()

/**********************************************************************************************************************/
/*                            				   Extern Constants Section              			                      */
/**********************************************************************************************************************/
#ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
	extern uint8_t u8rollSwitchResult;
#endif

/**********************************************************************************************************************/
/*                               			   Extern Variables Section         		                              */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                            			 	  Function Prototypes Section                                 			  */
/**********************************************************************************************************************/
void DAD_vfnInitialization(void);
void DAD_vfnDigitalAudioDriver(void);
void DAD_vfnDigitalAudioNewFrequency(uint16_t wNewFrequency);

/**********************************************************************************************************************/

#endif /* DIGITALAUDIODRIVER_H_ */
