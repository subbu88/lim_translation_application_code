/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/**********************************************************************************************************************/
/*       			                                Includes Section                                         		  */
/**********************************************************************************************************************/
#include "ProjectTypes.h"
#include "DigitalAudioDriver.h"
#if defined(USB_ASYNC) || defined(DAD_TCO)
#include "AudioClkSyncDriver.h"
#endif

#ifdef IAPI_INTERFACE_USB_HOST
#include "iAP_USB_Audio_Interface.h"

#ifdef MFI_IAP1
#include "iAP_Host.h"
#endif

#ifdef MFI_IAP2
#include "iAP2_Host.h"
#include "iAP2_DigitalAudio_Feat.h"
#endif

#endif
/**********************************************************************************************************************/
/*                           			      Function Prototypes Section            			                      */
/**********************************************************************************************************************/
#if defined(USB_SYNC) && !defined(DAD_TCO)	
static void iAP_vfnBufferLevelMonitor(uint32_t dwBufferDifference, uint32_t *dwpAudioRecv, uint16_t *wpAudioRecvSize);
static void DAD_vfnSenseDescompensationRate(uint32_t dwBufferDifference);
#ifdef DAD_SRC
static uint8_t iAP_bfnLinearSampleRateCompensator(qFmt *Input,volatile uint16_t *wInBytesBlockSize);
#else
static uint8_t iAP_bfSingleCompensation(qFmt *dwInput, int8_t *bOffsetBytes);
#endif
#endif
static void DAD_vfnInitVariables(void);

/**********************************************************************************************************************/
/*                     				            Defines & Macros Section                                    		  */
/**********************************************************************************************************************/
#ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
#define USB_HOST_MODE	0
#define USB_DEVICE_MODE	1
#endif

/**********************************************************************************************************************/
/*                        				            Typedef Section                                         		  */
/**********************************************************************************************************************/
typedef enum
{
	DAD_NO_COMPENSATION =0,
	DAD_UP_COMPENSATION,
	DAD_DOWN_COMPENSATION
}_eCompensationStatus;
/**********************************************************************************************************************/
/*                                			    Global Variables Section                                 			  */
/**********************************************************************************************************************/
uint8_t DAD_gbStatus;						/**< Digital Audio Driver status variable */
volatile uint32_t dwCompensationDelay;


#ifdef IAPI_INTERFACE_USB_HOST
#ifdef MFI_IAP2
iAP2_Digital_Audio_Feat_InfomationData sAudioAttributes;
#endif
#endif

#if defined(USB_SYNC) && !defined(DAD_TCO)	
uint8_t DAD_gbCompesationStatus;
uint8_t DAD_gbSRCConversionStatus;
#endif

/**********************************************************************************************************************/
/*                         			      	    Static Variables Section                		                      */
/**********************************************************************************************************************/

uint32_t DAD_gdwCurrentFrequency;		/**< Frequency at which audio will be received */
#if defined(USB_SYNC) && !defined(DAD_TCO)	
static uint16_t DAD_gw1msDelayCounter;
static int32_t DAD_gdwAccFrac;
static int32_t DAD_gdwFrac;
#endif



/**********************************************************************************************************************/
/*                             			        Global Constants Section                     			              */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                             				    Static Constants Section             	                 		      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                                   			   Functions Section           			                              */
/**********************************************************************************************************************/


/** ***********************************************************************************************
*
* @brief    Function to initialize the DigitalAudioDriver module. This routine configures the 
* 			AudioBufferHandler, DMA_Driver and AudioBufferHandler layers. There is no need to 
* 			call other initialization routine.
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
void DAD_vfnInitialization(void)
{
	/* Configure lower layers */
	DAD_AUDIO_CODEC_INIT;
	DAD_ACSD_INIT;
	DAD_AUDIO_BUFFER_INIT;
	DAD_AUDIO_DMA_INIT;
	
#ifdef SAI_MASTER
	iAPSSIMasterMode_Init();
#endif
	
	/* Initial Frequency */
	DAD_gdwCurrentFrequency = DAD_FREQUENCY_44_1KHZ;
	/* Initial value of DMA transfers */
	DMA_gwTransferCount = DAD_FREQUENCY_44_1KHZ * DAD_TRANSFER_COUNT_FACTOR_NUM / DAD_TRANSFER_COUNT_FACTOR_DEN;
	DAD_vfnInitVariables();
#ifdef USB_ASYNC
	/* Set feedback flag */
	g_bUSBAudioStatus |= (1 << USB_FEEDBACK_SENT);
#endif
}

/** ***********************************************************************************************
*
* @brief    Function to initialize the DigitalAudioDriver variables.
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
static void DAD_vfnInitVariables(void)
{
	/* If we are here is because there is not audio data therefore I2S is disabled*/
	(void)iAPSSI1_DisableTransfer(iAPSSI1_DeviceData,LDD_SSI_TRANSMITTER);
	/* Once the timer has timeout it is not needed until audio communication is started again and buffer filled to 50%*/
	IAP_TIMER_DISABLE_CHN(DAD_TIMEOUT_CHN);
	IAP_TIMER_SET_PERIOD(DAD_TIMEOUT_CHN,IAP_TIMEOUT_4MS_PERIOD);
	/* TCO */
	IAP_TIMER_DISABLE_CHN(TCO_TIMEOUT_CHN);
	IAP_TIMER_SET_PERIOD(TCO_TIMEOUT_CHN,IAP_TIMEOUT_2SEC_PERIOD);
	/* Initialize Src and Dst Audio buffer Pointers, set the 2 pointer with the same value */
	iAP_DMA_vfnSetDestinationAddressForCopyTransfer(DMA_GET_CURRENT_SRC_ADDR);
	/* clear Rx started flag for 50% buffer fill*/
	AUDIO_BUFFER_HANDLER_CLEAR_STATUS(AUDIO_BUFFER_HANDLER_RX_STARTED);
	/* Init Variables */
	DAD_gbStatus = 0;
	
#if defined(USB_ASYNC) || defined(DAD_TCO)
	ACSD_vfnInitVariables();
#endif

#if defined(USB_SYNC) && !defined(DAD_TCO)	
    /* descompensation rate timer */		
	IAP_TIMER_DISABLE_CHN(IAP_DESCOMPENSATION_SENSE_TIMER);
	/* Clear interpolation flags*/
	DAD_gbCompesationStatus=DAD_NO_COMPENSATION;
	DAD_gbSRCConversionStatus=DAD_SRC_CONVERSION_NOT_READY;
	/* Clear status flags (wait_compensation and compensation_rate_ready)*/
	DAD_gdwAccFrac=0;
#endif

}

#if defined(USB_SYNC) && !defined(DAD_TCO)	
/** ***********************************************************************************************
*
* @brief    Function useful to calculate the descompensation rate through 2 samples of buffer label   
* 			gotten in different time (windows time), calculate the differences between the 2 samples and divided
* 			by the windows time to get the descompensation rate. 
* 
* @param   	uint32_t dw2ndBufferDiff is a 2nd sample of buffer level 
* 
* @return  	void
*          
**************************************************************************************************/

static void DAD_vfnSenseDescompensationRate(uint32_t dw2ndBufferDiff)
{
	static uint32_t dw1stBufferDiff;

	if (DAD_gbStatus & (1 << DAD_COMPENSATION_RATE_STARTED))
	{
		DAD_gbStatus &= ~(1 << DAD_COMPENSATION_RATE_STARTED);
		/* Set period timer to sense descompensation rate*/
		IAP_TIMER_SET_PERIOD(IAP_DESCOMPENSATION_SENSE_TIMER,IAP_TIMEOUT_10SEC_PERIOD);
		/* Enable timer to get descompensation rate when will be expired */
		IAP_TIMER_ENABLE_CHN(IAP_DESCOMPENSATION_SENSE_TIMER);
		/* Save the 1st buffer level sample to sense descompensation rate */
		dw1stBufferDiff=dw2ndBufferDiff;
	}
	else if(IAP_TIMER_CHECK_CHN_FLAG(IAP_DESCOMPENSATION_SENSE_TIMER) || (dw2ndBufferDiff < AUDIO_BUFFER_DOWN_PERCENT) || (dw2ndBufferDiff > AUDIO_BUFFER_UP_PERCENT))
	{
		/* Get the absolute difference of buffer level from 1st instant and 2nd instant of time*/
		if (dw2ndBufferDiff >= dw1stBufferDiff)
		{
			dwCompensationDelay=dw2ndBufferDiff-dw1stBufferDiff;
		}
		else
		{
			dwCompensationDelay=dw1stBufferDiff-dw2ndBufferDiff;
		}
		if ((dw2ndBufferDiff < AUDIO_BUFFER_DOWN_PERCENT) || (dw2ndBufferDiff > AUDIO_BUFFER_UP_PERCENT)){
			/* convert samples number difference in milliseconds delay for each compensation applied*/
			dwCompensationDelay=(IAP_TIMEOUT_10SEC_PERIOD - IAP_TIMER_GET_VALUE(IAP_DESCOMPENSATION_SENSE_TIMER))/dwCompensationDelay; 
		}
		else
		{
			/* convert samples number difference in milliseconds delay for each compensation applied*/
			dwCompensationDelay=IAP_TIMEOUT_10SEC_PERIOD/dwCompensationDelay; 			
		}
		/* Subtract the 5% to ensure the compensation by SRC will be a little bit faster than descompensation*/
		dwCompensationDelay=dwCompensationDelay-dwCompensationDelay/DAD_DEN_FACTOR_PERCENT_FOR_DESC_RATE; 
		/* Compensation delay protection, this will be the maximum compensation delay*/
		if (dwCompensationDelay > DAD_PADLOCK_COMPENSATION_DELAY)
		{
			dwCompensationDelay=DAD_PADLOCK_COMPENSATION_DELAY;
		}
		/* At this point the compensation delay sensor is ready*/
		DAD_gbStatus |= (1 << DAD_COMPENSATION_RATE_READY);
		/* Disable timer to get descompensation rate when will be expired */
		IAP_TIMER_DISABLE_CHN(IAP_DESCOMPENSATION_SENSE_TIMER);

	}
}


#ifdef DAD_SRC

/** ***********************************************************************************************
*
* @brief    Function which compensate one sample more or one sample less applying a Sample Rate Converter  
* 			using linear interpolation method, this function will be called many times to complete the 
* 			the compensation of a determined block defined by DAD_SRC_WINDOWS_SAMPLES.
* 
* @param   	dwInput audio buffer
* 
* @return  	uint8_t Compensation done
*          
**************************************************************************************************/

static uint8_t iAP_bfnLinearSampleRateCompensator(qFmt *dwInput,volatile uint16_t *wInBytesBlockSize)
{
	static qFmt dwSampleLeftConnect;			/* Left last sample of the last frame */
	static qFmt dwSampleRightConnect;			/* Right last sample of the last frame */
	WDIV y;                                   /* Interpolation result */
	WDIV y0, y1;                                /* Nearest output values */
    uint32_t dwFract;                                 /* fractional part */
    uint32_t dwAccBlockFrac;							/* Local Fractional Accumulator*/
    uint8_t bReturnValue;
    uint8_t bInIndex;                             	 /* Input Index to read nearest output values */
    uint8_t bInBlockSize;
    uint8_t bOutIndex=0;                              /* Output Index to read nearest output values */

    y.dwData=0;
    y0.dwData=0;
    y1.dwData=0;
    bInBlockSize=(uint8_t)(*wInBytesBlockSize / AUDIO_BUFFER_SAMPLE_SIZE);
    /* dwFract  dwAccBlockFrac is in 12.20 format */
    /* 12 bits for the table index */
    /* Index value calculation */
    if (0==DAD_gdwAccFrac)
    {
    	/* Move the first audio data to the extra elements index zero and one*/
    	dwInput[0]=dwInput[2];
    	dwInput[1]=dwInput[3];
        bOutIndex=DAD_CHANNELS_OFFSET;
    	DAD_gdwAccFrac = DAD_ONE_UNITY_QSRC_FORMAT + DAD_gdwFrac;
        dwAccBlockFrac = DAD_gdwAccFrac;
    }
    else
    {
    	/* use the last elements of previous processing and put them in the extra elements*/
    	dwInput[0]=dwSampleLeftConnect;
    	dwInput[1]=dwSampleRightConnect;
        dwAccBlockFrac = (DAD_gdwAccFrac & DAD_QSRC_FRAC_MASK);
    }

	bInIndex = (uint8_t)(dwAccBlockFrac  >> DAD_QSRC_FRAC_SHIFTER);
	bInIndex*=DAD_CHANNELS_OFFSET;

	/* Linear interpolation loop */
    while ((bInIndex < bInBlockSize) && (DAD_gdwAccFrac < DAD_THRESHOLD_QSRC_FORMAT))
    {
    	
   		dwFract = (dwAccBlockFrac & DAD_QSRC_FRAC_MASK) << DAD_QSRC_TO_Q31_SHIFTER;
   		/*********LEFT CHANNEL***********/
	    /* Read two nearest output values from the index in 1.31(q31) format */
   		y0.word.hi = dwInput[bInIndex];
   		y1.word.hi = dwInput[bInIndex + DAD_CHANNELS_OFFSET];

	    /* Calculation of y0 * (1-fract) and y is in 2.30 format */
	    y.dwData = ((int32_t) ((int64_t) y0.dwData * (DAD_Q31_ONE - dwFract) >> DAD_PREPARE_TO_QFMT_SHIFTER));

	    /* Calculation of y0 * (1-fract) + y1 *fract and y is in 2.30 format */
	    y.dwData += ((int32_t) (((int64_t) y1.dwData * dwFract) >> DAD_PREPARE_TO_QFMT_SHIFTER));

	    /* Convert y to 1.31 format */
	    dwInput[bOutIndex++]=y.word.hi << 1u;
	    /*********RIGHT CHANNEL***********/
	    bInIndex++;
	    y0.word.hi = dwInput[bInIndex];
	    y1.word.hi = dwInput[bInIndex + DAD_CHANNELS_OFFSET];

	    /* Calculation of y0 * (1-fract) and y is in 2.30 format */
	    y.dwData = ((int32_t) ((int64_t) y0.dwData * (DAD_Q31_ONE - dwFract) >> DAD_PREPARE_TO_QFMT_SHIFTER));

	    /* Calculation of y0 * (1-fract) + y1 *fract and y is in 2.30 format */
	    y.dwData += ((int32_t) (((int64_t) y1.dwData * dwFract) >> DAD_PREPARE_TO_QFMT_SHIFTER));

	    /* Convert y to 1.31 format */
	    dwInput[bOutIndex++]=y.word.hi << 1u;

	    DAD_gdwAccFrac += DAD_gdwFrac;
	    dwAccBlockFrac += DAD_gdwFrac;
   		bInIndex = (uint8_t)((dwAccBlockFrac & DAD_QSRC_INT_MASK) >> DAD_QSRC_FRAC_SHIFTER);
	    /* 20 bits for the fractional part */
	    /* shift left by 11 to keep fract in 1.31 format */
   		bInIndex*=DAD_CHANNELS_OFFSET;

	}
    if (DAD_gdwAccFrac > DAD_THRESHOLD_QSRC_FORMAT)
    {
    	/*Due fraction errors may be the max value no was reached exactly so it must to be rounded even it was reached or not*/
	    dwAccBlockFrac += DAD_HALF_UNITY_QSRC_FORMAT;
	    /* Get the index integer part */
		bInIndex = (uint8_t)(dwAccBlockFrac >> DAD_QSRC_FRAC_SHIFTER);
		/* Convert the single index to 2 channel index */
   		bInIndex*=DAD_CHANNELS_OFFSET;
   		/* increase the block with channel offset to complete the last data */
    	bInBlockSize+=DAD_CHANNELS_OFFSET;
    	/* At last processing If compensation is up or down there will be a different behavior results*/
    	if (DAD_gdwFrac & DAD_QSRC_INT_MASK) /*down compensation*/
    	{
			while (bInIndex < bInBlockSize)
			{
				/* move the remaining elements */
				dwInput[bOutIndex++]=dwInput[bInIndex++]; 
			}
    	}
    	else /*up compensation*/
    	{
    		/* In this case the last data already are in its correct position just will be needed the boutindex */
    		bOutIndex+=bInBlockSize-bOutIndex;
    	}
    	/* init global acc fraction for the next compensation*/
	    DAD_gdwAccFrac=0;
	    bReturnValue=DAD_SRC_CONVERSION_DONE;
    }
    else
    {
    	dwSampleLeftConnect=dwInput[bInIndex];
    	dwSampleRightConnect=dwInput[bInIndex + 1 ];
    	bReturnValue=DAD_SRC_CONVERSION_NOT_READY;
    }
	*wInBytesBlockSize= bOutIndex * AUDIO_BUFFER_SAMPLE_SIZE;
	return bReturnValue;
}

#else
/** ***********************************************************************************************
*
* @brief    Function which compensate one sample more or one sample less applying a single interpolation of 
* 			one sample at the beginning of the buffer, this function just introduce or delete one samlpe to 
* 			compensate, its processing is very very low but the quality is affected, a slight click can be notable.  
* 
* @param   	dwInput audio buffer, int8_t offset to apply in audio buffer input.
* 
* @return  	uint8_t Compensation done
*          
**************************************************************************************************/
static uint8_t iAP_bfSingleCompensation(qFmt *dwInput, int8_t *bOffsetBytes)
{
	uint8_t bReturnValue=DAD_SRC_CONVERSION_DONE;

	if (DAD_gbCompesationStatus == DAD_UP_COMPENSATION)
	{
		/*************LEFT CHANNEL****************/	
		/** Calculating average between 2 samples*/
		/** In the case of USB SYNC we always have 2 extra element in the beginning of
		 *  the input buffer (left and right) in order to use them to interpolation */ 
		dwInput[0]=dwInput[2];
		dwInput[2]/=2;
		dwInput[2]+=dwInput[4]/2;
		/*************RIGTH CHANNEL****************/
		dwInput[1]=dwInput[3];
		dwInput[3]/=2;
		dwInput[3]+=dwInput[5]/2;
		*bOffsetBytes=AUDIO_BUFFER_SAMPLE_SIZE*AUDIO_CHANNEL_NUMBER;
	}
	else //if (DAD_gbCompesationStatus == DAD_DOWN_COMPENSATION)
	{
		/*************LEFT CHANNEL****************/		
		/** Calculating average between 2 samples*/
		dwInput[4]/=2;
		dwInput[4]+=dwInput[2]/2;
		/*************RIGTH CHANNEL****************/
		dwInput[5]/=2;
		dwInput[5]+=dwInput[3]/2;
		*bOffsetBytes=-AUDIO_BUFFER_SAMPLE_SIZE*AUDIO_CHANNEL_NUMBER;
	}
	return bReturnValue;

}
#endif

/** ***********************************************************************************************
*
* @brief    Function in charge of monitor the buffer level, there are 2 level which will be monitored, below and above of 50% 
* 			Approximately, if those level are crossed a compensation is applied to return to 50%.
* 
* @param   	uint32_t dwBufferDifference (buffer level), uint32_t *dwpAudioRecv(pointer to buffer input address), 
* 			uint16_t *wpAudioRecvSize (no. bytes of audio coming)
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP_vfnBufferLevelMonitor(uint32_t dwBufferDifference, uint32_t *dwpAudioRecv, uint16_t *wpAudioRecvSize){

	*dwpAudioRecv=(uint32_t)&g_baAudioRecvBuffer[AUDIO_BUFFER_SAMPLE_SIZE*AUDIO_CHANNEL_NUMBER];
	*wpAudioRecvSize=g_wAudioRecvSize;

	if(AUDIO_BUFFER_HANDLER_CHECK_STATUS(AUDIO_BUFFER_HANDLER_RX_STARTED) && (DAD_gbStatus & (1 << DAD_COMPENSATION_RATE_READY)))
	{
		if(!(DAD_gbStatus & (1 << DAD_WAIT_COMPENSATION)))
		{
			if(dwBufferDifference < AUDIO_BUFFER_DOWN_PERCENT)  /*Audio buffer occupancy is less than 49% */
			{
					/* Set flag to wait for compensation  */
					DAD_gbCompesationStatus=DAD_UP_COMPENSATION;
					DAD_gdwFrac=DAD_UP_FRACTION_QSRC_FORMAT;
			}
			else if(dwBufferDifference > AUDIO_BUFFER_UP_PERCENT)
			{
					DAD_gbCompesationStatus=DAD_DOWN_COMPENSATION;						
					DAD_gdwFrac=DAD_DOWN_FRACTION_QSRC_FORMAT;	    			
			} 
			if (DAD_gbCompesationStatus != DAD_NO_COMPENSATION)
			{
				/* Set flag to wait for compensation  */
				DAD_gbStatus |= (1 << DAD_WAIT_COMPENSATION);
				DAD_gw1msDelayCounter=dwCompensationDelay;
			}
		}
		else
		{	
			if ((DAD_gbCompesationStatus != DAD_NO_COMPENSATION) && (DAD_gw1msDelayCounter==dwCompensationDelay))
			{		
#ifdef DAD_SRC
				DAD_gbSRCConversionStatus=iAP_bfnLinearSampleRateCompensator((qFmt*)&g_baAudioRecvBuffer[0], &g_wAudioRecvSize);
				*dwpAudioRecv=(uint32_t)&g_baAudioRecvBuffer[0];
				*wpAudioRecvSize=g_wAudioRecvSize;
#else
				int8_t bOffsetByte;
				DAD_gbSRCConversionStatus=iAP_bfSingleCompensation((qFmt*)&g_baAudioRecvBuffer[0], &bOffsetByte);
				*dwpAudioRecv=(uint32_t)&g_baAudioRecvBuffer[(AUDIO_BUFFER_SAMPLE_SIZE*AUDIO_CHANNEL_NUMBER) - bOffsetByte];
				*wpAudioRecvSize=g_wAudioRecvSize + bOffsetByte;
#endif
				if (DAD_gbSRCConversionStatus)
				{
					DAD_gw1msDelayCounter=0;
				}
			}else
			{
				DAD_gw1msDelayCounter++;
			}
			
			if((DAD_gbCompesationStatus == DAD_UP_COMPENSATION) && (DAD_gbSRCConversionStatus == DAD_SRC_CONVERSION_DONE) && (dwBufferDifference > AUDIO_BUFFER_MID_PERCENT))
			{
					/* Clear flag because compensation is done */
					DAD_gbStatus &= ~(1 << DAD_WAIT_COMPENSATION);
					DAD_gbCompesationStatus=DAD_NO_COMPENSATION;
					DAD_gbSRCConversionStatus=DAD_SRC_CONVERSION_NOT_READY;
			}
			else if((DAD_gbCompesationStatus == DAD_DOWN_COMPENSATION) && (DAD_gbSRCConversionStatus == DAD_SRC_CONVERSION_DONE) && (dwBufferDifference < AUDIO_BUFFER_MID_PERCENT))
			{
					/* Clear flag because compensation is done */
					DAD_gbStatus &= ~(1 << DAD_WAIT_COMPENSATION);
					DAD_gbCompesationStatus=DAD_NO_COMPENSATION;
					DAD_gbSRCConversionStatus=DAD_SRC_CONVERSION_NOT_READY;
			}
		}
	}

}
#endif
/** ***********************************************************************************************
*
* @brief    Function that continuously checks if a change of frequency is received. It also checks if
* audio was received and starts a DMA transference. When the transference is over it computes the 
* occupancy of the buffer, calculates and sends the feedback. Finally, if no audio was received and
* a time out it fills the audio buffer with zeros. 
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
void DAD_vfnDigitalAudioDriver(void)
{
	uint32_t dwBufferDifference;
#if defined(USB_SYNC) && !defined(DAD_TCO)	
	uint32_t bAudioRecvAddress;
	uint16_t wAuxAudioRecvSize;    
#endif

#ifdef IAPI_INTERFACE_USB_HOST
	static uint8_t bAudioEnabled = _FALSE_;
#ifdef MFI_IAP2
	if(iAP2_Host_gwStatus & (1 << IAP2H_ID_ACCEPTED) )
	{
		if(_FALSE_ == bAudioEnabled)
		{
			bAudioEnabled = _TRUE_;
#ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
			if ( USB_HOST_MODE == u8rollSwitchResult)
#endif
			{
				/** @todo Function Return */
				(void)iAP_USBHostAudioEnable();
#ifndef IAPI_INTERFACE_USB_ROLE_SWITCH
				iAP2_DigitalAudio_Feat_bfnStartUSBDeviceModeAudio(&sAudioAttributes);
#endif
			}
		}
	}
	else
	{
		bAudioEnabled = _FALSE_;
	}
#endif
#endif
		
	if(gu8NewFreqFlag == _TRUE_)
	{
		gu8NewFreqFlag = _FALSE_;

		DAD_gdwCurrentFrequency = (uint32_t)g_cur_sampling_frequency[2] << 16;
		DAD_gdwCurrentFrequency |= (uint32_t)g_cur_sampling_frequency[1] << 8;
		DAD_gdwCurrentFrequency |= (uint32_t)g_cur_sampling_frequency[0];
		/** @todo Function Return */
		(void)DAD_CHANGE_FREQUENCY(DAD_gdwCurrentFrequency);
		
		DMA_gwTransferCount = DAD_CNT_INC + (uint32_t)DAD_gdwCurrentFrequency *((uint32_t)DAD_TRANSFER_COUNT_FACTOR_NUM) / ((uint32_t)DAD_TRANSFER_COUNT_FACTOR_DEN);
#if defined(USB_ASYNC) || defined(DAD_TCO)
		ACSD_vfnSetRatio((((uint64_t)DMA_gwTransferCount << ACSD_RATIO_Q_SHIFT)* 2 * 1000) / (uint64_t)DAD_gdwCurrentFrequency);
#endif
		/* Force timer to zero to initialize variables below*/
		IAP_TIMER_SET_PERIOD(DAD_TIMEOUT_CHN,0);
		IAP_TIMER_ENABLE_CHN(DAD_TIMEOUT_CHN);
	}
	
	if(IAP_TIMER_CHECK_CHN_FLAG(DAD_TIMEOUT_CHN))
	{
		DAD_vfnInitVariables();
	}
	
	if(g_bUSBAudioStatus & (1 << USB_AUDIO_RECEIVED))
	{
		dwBufferDifference = AudioBufferHandler_dwfnCalculateOccupancy(DMA_GET_CURRENT_DEST_ADDR, DMA_GET_CURRENT_SRC_ADDR);

		if(!AUDIO_BUFFER_HANDLER_CHECK_STATUS(AUDIO_BUFFER_HANDLER_RX_STARTED))
		{
			AudioBufferHandler_vfnFillHalfBuffer(dwBufferDifference);
		}
#if defined(USB_SYNC) && !defined(DAD_TCO)
		else if(!(DAD_gbStatus & (1 << DAD_COMPENSATION_RATE_READY)))
		{
			DAD_vfnSenseDescompensationRate(dwBufferDifference);
		}
		/* Verify buffer levels and apply SRC if buffer level is out of boundaries*/
    	iAP_vfnBufferLevelMonitor(dwBufferDifference, &bAudioRecvAddress, &wAuxAudioRecvSize);
    	/* Set DMA source address at the begining of the buffer */
        iAP_DMA_vfnSetSourceAddressForCopyTransfer(bAudioRecvAddress);
		/* Set DMA transfer size equal to receive size */
        iAP_DMA_vfnSetTransferSizeForCopy(wAuxAudioRecvSize);
#else
		/* Set DMA source address at the begining of the buffer */
		iAP_DMA_vfnSetSourceAddressForCopyTransfer((uint32_t)(&g_baAudioRecvBuffer[0]));
		/* Set DMA transfer size equal to receive size */
		iAP_DMA_vfnSetTransferSizeForCopy(g_wAudioRecvSize);
#endif
		/* Start DMA transfer, this copies data from endpoint buffer to OutRingBuffer */
		iAP_DMA_vfnStartCopyTransfer();
		/* Clean audio data received flag*/
		g_bUSBAudioStatus &= ~(1 << USB_AUDIO_RECEIVED);
		/* Timer useful when data coming from device has stopped*/
		IAP_TIMER_SET_PERIOD(DAD_TIMEOUT_CHN,IAP_TIMEOUT_4MS_PERIOD);
#ifdef DAD_TCO	
		ACSD_vfnDriver(dwBufferDifference);
#endif
	}
	DAD_ACSD_CALLBACK;
	DAD_AUDIO_CODEC_CALLBACK;
}

#ifdef IAPI_INTERFACE_USB_HOST
/** ***********************************************************************************************
*
* @brief    Function called by iAP2_DigitalAudio_Feat when a song is playing with 
* 			different frequency 
* 			 
* @param   	wNewFrequency Frequency in the last song
* 
* @return  	void
*          
**************************************************************************************************/
void DAD_vfnDigitalAudioNewFrequency(uint16_t wNewFrequency)
{
	g_cur_sampling_frequency[0]= (uint8_t)(0x00FF & wNewFrequency);
	if(gu8FreqBackUp != g_cur_sampling_frequency[0])
	{
		g_cur_sampling_frequency[1]= (uint8_t)(wNewFrequency>>8);
		g_cur_sampling_frequency[2]=0;
		gu8NewFreqFlag = _TRUE_;
		gu8FreqBackUp = g_cur_sampling_frequency[0];
		/** @todo Function Return */
		(void)iAP_USBHostNewTrackAttributes();
	}
}
#endif
