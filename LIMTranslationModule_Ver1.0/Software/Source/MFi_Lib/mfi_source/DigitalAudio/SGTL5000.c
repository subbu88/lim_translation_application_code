/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *		This module contains all the functions needed to communicate with the SGTL5000 device. 
 * 		It also handles change the SGTL sample rate frequency and change the GEQ band volume. 
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "Events.h"
#include "SGTL5000.h"
#include "SAI_Master_Driver.h"

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void SGTL5000_vfnWriteRegister(uint16_t wRegister, uint16_t wRegisterData);
static void SGTL5000_vfnReadRegister(uint16_t wRegister, uint8_t *bpRegisterData);

static void SGTL5000_vfnReadConfiguration_State(void);
static void SGTL5000_vfnCheckConfiguration_State(void);
static void SGTL5000_vfnSetConfiguration_State(void);
static void SGTL5000_vfnReadPLLStatus_State(void);
static void SGTL5000_vfnChangeFrequency_State(void);
static void SGTL5000_vfnWriteFreqSequence_State(void);
static void SGTL5000_vfnWaitComm_State(void);
static void SGTL5000_vfnWaitReading_State(void);
static void SGTL5000_vfnIDLE(void);

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
/*!
    \def SGTL5000_I2C_ADDRESS
    \brief I2C Slave address of the SGTL5000
*/
#define SGTL5000_I2C_ADDRESS				(0x0A)  
/*!
    \def SGTL5000_CONTROL_BUFFER_SIZE
    \brief Size to be used by the buffer for the I2C communication
*/
#define SGTL5000_CONTROL_BUFFER_SIZE		(4)
/*!
    \def SGTL5000_GEQ_MAX_VOL
    \brief Max allowed volume value for the GEQ equivalent to +12dB
*/
#define SGTL5000_GEQ_MAX_VOL				(0x5F)
/*!
    \def SGTL5000_GEQ_MIN_VOL
    \brief Min allowed volume value for the GEQ equivalent to -12dB
*/
#define SGTL5000_GEQ_MIN_VOL				(0x00)
/*!
    \def SGTL5000_INIT_SIZE
    \brief The total size of the initialization arrays
*/
#define SGTL5000_INIT_SIZE					(21)
/*!
    \def SGTL5000_CHIP_ANA_PWR_RST_HIGH_VAL
    \brief CHIP_ANA_POWER reset value most significant byte
*/
#define SGTL5000_CHIP_ANA_PWR_RST_HIGH_VAL	(0x70)
/*!
    \def SGTL5000_CHIP_ANA_PWR_RST_LOW_VAL
    \brief CHIP_ANA_POWER reset value less significant byte
*/
#define SGTL5000_CHIP_ANA_PWR_RST_LOW_VAL	(0x60)
/*!
    \def SGTL5000_CHIP_ANA_PWR_REG_SIZE
    \brief Chip Ana Power Register size
*/
#define SGTL5000_CHIP_ANA_PWR_REG_SIZE		(0x02)
/*!
    \def I2S_WORD_LENGTH
    \brief Word length selection for initialization
*/
#ifdef SAI_MASTER
	#define CHIP_CLK_CTRL_MS						(0x00)
	#define CHIP_CLK_CTRL_SYS_FS_INIT				(0x08)
	#define CHIP_CLK_CTRL_MCLK_FREQ_INIT			(0x00)
#else
	#define CHIP_CLK_CTRL_MS						(0x80)
	#define CHIP_CLK_CTRL_SYS_FS_INIT				(0x04)
	#define CHIP_CLK_CTRL_MCLK_FREQ_INIT			(0x03)
#endif

#ifdef _16_BIT_WORD
	#define I2S_WORD_LENGTH					(0x30)
#elif defined(_20_BIT_WORD)
	#define I2S_WORD_LENGTH					(0x20)
#elif defined(_24_BIT_WORD)
	#define I2S_WORD_LENGTH					(0x10)
#else
	#define I2S_WORD_LENGTH					(0x00)
#endif

#ifdef I2S_RIGHT_JUSTIFIED_MODE
	#define I2S_MODE						(0x4)
	#define I2S_LRALIGN						(0x2)
	#define I2S_LRPOL						(0x1)
#else
	#define I2S_MODE						(0x0)
	#define I2S_LRALIGN						(0x0)
	#define I2S_LRPOL						(0x0)
#endif


#if (SGTL5000_SYS_MCLK > 17000000)
		/* SYS_MCL /2  */
	#define SGTL5000_CHIP_CLK_TOP_CTRL_INPUT_FREQ_DIV2	(0x0008) 

		/*INT_DIVISOR = FLOOR(PLL_OUTPUT_FREQ/INPUT_FREQ)*/
	#define SGTL5000_PLL_INT_DIV_NO_44_1K						(0x10)
	#define SGTL5000_PLL_INT_DIV_44_1K							(0x0E)

		/*FRAC_DIVISOR = ((PLL_OUTPUT_FREQ/INPUT_FREQ) - INT_DIVISOR)*2048*/
	#define SGTL5000_PLL_FRAC_DIV_NO_44_1K						(0x00)
	#define SGTL5000_PLL_FRAC_DIV_44_1K							(0x59A)

#else
	/* SYS_MCL pass through	
		@TODO : Calculate values according to the frequency	
	*/ 
	#define SGTL5000_CHIP_CLK_TOP_CTRL_INPUT_FREQ_DIV2  (0x0000)  
 
	/*INT_DIVISOR = FLOOR(PLL_OUTPUT_FREQ/INPUT_FREQ)*/
	#define SGTL5000_PLL_INT_DIV_NO_44_1K						(0x10)
	#define SGTL5000_PLL_INT_DIV_44_1K							(0x0E)

	/*FRAC_DIVISOR = ((PLL_OUTPUT_FREQ/INPUT_FREQ) - INT_DIVISOR)*2048*/
	#define SGTL5000_PLL_FRAC_DIV_NO_44_1K						(0x00)
	#define SGTL5000_PLL_FRAC_DIV_44_1K							(0x599)
#endif

/*!	
    \def SGTL5000_PLL_CTRL_INT_DIVISOR
    \brief Integer bits position into CHIP_PLL_CTRL
 */
#define SGTL5000_PLL_CTRL_INT_DIVISOR					(0xB)

/*!
    \def I2S_WORD_LENGTH
    \brief Enables an internal oscillator to be used for the zero cross detectors
*/
#ifdef _SGTL5000_INT_OSC
	#define SGTL5000_CHIP_CLK_TOP_CTRL_ENABLE_INT_OSC   (0x0800)
#else
	#define SGTL5000_CHIP_CLK_TOP_CTRL_ENABLE_INT_OSC   (0x0000)
#endif
	
/*!	
    \def SGTL5000_FS_MULTIPLE_256
    \brief Identifies incoming SYS_MCLK 256 multiple
 */
#define SGTL5000_FS_MULTIPLE_256 	(256)
/*!	
    \def SGTL5000_FS_MULTIPLE_384
    \brief Identifies incoming SYS_MCLK 384 multiple
 */
#define SGTL5000_FS_MULTIPLE_384 	(384)
/*!	
    \def SGTL5000_FS_MULTIPLE_512
    \brief Identifies incoming SYS_MCLK 512 multiple
 */
#define SGTL5000_FS_MULTIPLE_512 	(512)

/*!
    \def SGTL5000_VCOAMP_POWERUP
    \brief Value to set the �Power up the PLL VCO amplifier� bit to 1 for CHIP_ANA_POWER
    		register
 */
#define SGTL5000_VCOAMP_POWERUP (0x0400)
/*!
    \def SGTL5000_VCOAMP_POWERUP
    \brief Value to set the �PLL Power Up� bit to 1 for CHIP_ANA_POWER register
 */
#define SGTL5000_PLL_POWERUP (0x0100)
/*!
    \def SGTL5000_PLL_POWERUP
    \brief Value to set the required bits to turn the PLL on
 */
#define SGTL5000_TURN_ON_PLL (SGTL5000_VCOAMP_POWERUP | SGTL5000_PLL_POWERUP)
/*!	
    \def SGTL5000_TURN_OFF_PLL
    \brief Value to set the required bits to turn the PLL off
 */
#define SGTL5000_TURN_OFF_PLL (~SGTL5000_TURN_ON_PLL)
/*!	
    \def SGTL5000_CHANGE_FREQ_SIZE
    \brief Size of array used to change register values for frequency change
 */
#define SGTL5000_CHANGE_FREQ_SIZE (0x04) 

/*!	
    \def SGTL5000_CHIP_CLK_CTRL_SYS_FS	
    \brief Sample rate bits position into CHIP_CLK_CTRL
 */
#define SGTL5000_CHIP_CLK_CTRL_SYS_FS			(0x02)


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/
typedef enum
{
	SGTL5000_FS_MULTIPLE_OF_256 = 0x0,
	SGTL5000_FS_MULTIPLE_OF_384,
	SGTL5000_FS_MULTIPLE_OF_512,
	SGTL5000_FS_NO_MULTIPLE
}eSgtl5000FsMultiples;  /**< used to identify FS possible multiple */

/*!
    \enum eSgtl5000SysFss
    \brief SGTL5000 Sample Rate
*/
typedef enum
{
	SGTL5000_SYS_FS_32K = 0x0,
	SGTL5000_SYS_FS_44_1K ,
	SGTL5000_SYS_FS_48K,
	SGTL5000_SYS_FS_96K
}eSgtl5000SysFs;  /**< used to identify Sys FS */

typedef enum
{
	SGTL5000_READ_CONFIGURATION_STATE = 0,
	SGTL5000_CHECK_CONFIGURATION_STATE,
	SGTL5000_SET_CONFIGURATION_STATE,
	SGTL5000_READ_PLL_STATUS_STATE,
	SGTL5000_CHANGE_FREQUENCY_STATE,
	SGTL5000_WRITE_FREQ_SEQUENCE_STATE,
	SGTL5000_CONTROL_STATE_WAIT_COMM,
	SGTL5000_CONTROL_STATE_WAIT_READING,
	SGTL5000_CONTROL_STATE_IDLE
}eSGTL5000ControlStates; /**< used for SGTL5000 driver states */

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

uint8_t SGTL5000_gbDriverStatus = 0;  /**< used to identify SGTL5000 Driver status */


sSM SGTL5000_gslStateMachine;  /**< Stores the states of the state machines */

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

static uint8_t SGTL5000_gabControlOutputBuffer[SGTL5000_CONTROL_BUFFER_SIZE];  /**< Buffer for write I2C commands */
static uint8_t SGTL5000_gabChipAnaPowerVal[SGTL5000_CHIP_ANA_PWR_REG_SIZE]; /**< This byte array holds the Chip Ana Power register value at Init State */
static uint8_t SGTL5000_gubMclkFreqMul=0x3;  /**< This byte is used to save FS multiple or PLL */
static uint8_t SGTL5000_gbSysFs=0; /**< This byte is used to save internal sample rate */
static uint8_t  SGTL5000_gbPLLIntDivisor; /**< This byte is used to save integer part for PLL */
static uint16_t SGTL5000_gwPLLFracDivisor; /**< This  is used to save fractional part for PLL */
static uint8_t SGTL5000_gbChangeFreqSize; /**< Used to control how many register must be configured during a change frequency */
static uint8_t *SGTL5000_gbpReadData;	 /**< Pointer to the I2C read data */
static uint32_t SGTL5000_gdwNewFrequencyValue; /**< This variable stores the new frequency value */
static uint8_t SGTL5000_gbNextStateAfterRead = 0;  /**< used to store the next state after reading a register  */
static uint16_t SGTL5000_gawChangeFreqRegisterValuesSequence[SGTL5000_CHANGE_FREQ_SIZE];  /**< Registers values to change frequency */

static uint8_t bRegStep = 0; /*Used to indicate order for register configuration*/

static uint16_t SGTL5000_gawChangeFreqRegisterSequence[SGTL5000_CHANGE_FREQ_SIZE] = /**< Registers definitions to change frequency */
{
	SGTL5000_CHIP_PLL_CTRL,  
	SGTL5000_CHIP_ANA_POWER,
	SGTL5000_CHIP_CLK_CTRL,
	SGTL5000_CHIP_ANA_POWER,
};

uint8_t SGTL5000_gbIICBusStatus; /**< Save IIC bus status */

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*!
    \var SGTL5000_gawInitRegisterSequenceInitRegisterSequence
    \brief Registers definitions for the initialization
*/
static const uint16_t SGTL5000_gawInitRegisterSequence[SGTL5000_INIT_SIZE] =
{
	SGTL5000_CHIP_LINREG_CTRL,
	SGTL5000_CHIP_ANA_POWER,
	SGTL5000_CHIP_LINREG_CTRL,
	SGTL5000_CHIP_REF_CTRL,
	SGTL5000_CHIP_LINE_OUT_CTRL,
	SGTL5000_CHIP_SHORT_CTRL,
	SGTL5000_CHIP_ANA_POWER ,
	SGTL5000_CHIP_DIG_POWER,
	SGTL5000_CHIP_LINE_OUT_VOL,
	SGTL5000_CHIP_CLK_TOP_CTRL ,
	SGTL5000_CHIP_PLL_CTRL,
	SGTL5000_CHIP_ANA_POWER,
	SGTL5000_CHIP_CLK_CTRL,
	SGTL5000_CHIP_I2S_CTRL,
	SGTL5000_CHIP_SSS_CTRL,
	SGTL5000_CHIP_ANA_ADC_CTRL,
	SGTL5000_CHIP_DAC_VOL,
	SGTL5000_CHIP_LINE_OUT_VOL,
	SGTL5000_CHIP_ANA_HP_CTRL,
	SGTL5000_CHIP_ADCDAC_CTRL,
	SGTL5000_CHIP_ANA_CTRL
};

/*!
    \var SGTL5000_gawInitRegisterValuesSequence
    \brief Registers values for the initialization.
*/
static const uint16_t SGTL5000_gawInitRegisterValuesSequence[SGTL5000_INIT_SIZE] =
{
	0x0008, //SGTL5000_CHIP_LINREG_CTRL
	0x7260, //SGTL5000_CHIP_ANA_POWER
	0x0068, //SGTL5000_CHIP_LINREG_CTRL
	0x01EE, //SGTL5000_CHIP_REF_CTRL
	0x0F22, //SGTL5000_CHIP_LINE_OUT_CTRL
	0x1106, //SGTL5000_CHIP_SHORT_CTRL
	0x72FB, //SGTL5000_CHIP_ANA_POWER
	0x0063, //SGTL5000_CHIP_DIG_POWER
	0x0F0F, //SGTL5000_CHIP_LINE_OUT_VOL
	SGTL5000_CHIP_CLK_TOP_CTRL_ENABLE_INT_OSC|SGTL5000_CHIP_CLK_TOP_CTRL_INPUT_FREQ_DIV2 , //SGTL5000_CHIP_CLK_TOP_CTRL
	0x759A, //SGTL5000_CHIP_PLL_CTRL
	0x77FB, //SGTL5000_CHIP_ANA_POWER
	0x0000|CHIP_CLK_CTRL_SYS_FS_INIT|CHIP_CLK_CTRL_MCLK_FREQ_INIT, //SGTL5000_CHIP_CLK_CTRL
	0x0000|CHIP_CLK_CTRL_MS|I2S_WORD_LENGTH|I2S_MODE|I2S_LRALIGN|I2S_LRPOL, //SGTL5000_CHIP_I2S_CTRL
	0x0010, //SGTL5000_CHIP_SSS_CTRL
	0x0000, //SGTL5000_CHIP_ANA_ADC_CTRL
	0x3C3C, //SGTL5000_CHIP_DAC_VOL
	0x0C0C, //SGTL5000_CHIP_LINE_OUT_VOL
	0x3030, //SGTL5000_CHIP_ANA_HP_CTRL 
	0x0000, //SGTL5000_CHIP_ADCDAC_CTRL
	0x0004  //SGTL5000_CHIP_ANA_CTRL
};
/*!
    \var SGTL5000_gawGEQRegisters
    \brief Registers definitions for the GEQ
    \note This array is currently not used, but is declared for future support.
*/
static const uint16_t SGTL5000_gawGEQRegisters[5] =
{
		SGTL5000_DAP_AUDIO_EQ_BASS_BAND0,
		SGTL5000_DAP_AUDIO_EQ_BAND1,
		SGTL5000_DAP_AUDIO_EQ_BAND2,
		SGTL5000_DAP_AUDIO_EQ_BAND3,
		SGTL5000_DAP_AUDIO_EQ_TREBLE_BAND4
};

void (* const SGTL5000_vfnapStateMachine[]) (void) =
{
	SGTL5000_vfnReadConfiguration_State,
	SGTL5000_vfnCheckConfiguration_State,
	SGTL5000_vfnSetConfiguration_State,
	SGTL5000_vfnReadPLLStatus_State,
	SGTL5000_vfnChangeFrequency_State,
	SGTL5000_vfnWriteFreqSequence_State,
	SGTL5000_vfnWaitComm_State,
	SGTL5000_vfnWaitReading_State,
	SGTL5000_vfnIDLE	
};

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/* Global functions */

/** ***********************************************************************************************
*
* @brief    starts CODEC initialization, further init is done on the INIT state
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
void SGTL5000_vfnInit(void)
{
	/* Initialize physical layer */
	(void)iAPI2C1_Enable(iAPI2C1_DeviceData);
	/* Set the driver as busy */
	SGTL5000_gbDriverStatus |= (1 << SGTL5000_STATUS_BUSY);
	SGTL5000_gslStateMachine.ActualState = SGTL5000_READ_CONFIGURATION_STATE;
	SGTL5000_gslStateMachine.NextState = SGTL5000_READ_CONFIGURATION_STATE;
}

/** ***********************************************************************************************
*
* @brief    Sets state machine to the change frequency state
* 
* @param   	dwNewFreqValue: New frequency value to be changed 
* 
* @return  	_ERROR_ state machine is busy
* 			_OK_    state machine accepts change frequency request
*          
**************************************************************************************************/
uint8_t SGTL5000_bfnChangeFrequency(uint32_t dwNewFreqValue)
{
	uint32_t dwMultiple;
	uint8_t bStatus = _ERROR_;
	
	if(!(SGTL5000_gbDriverStatus & (1 << SGTL5000_STATUS_BUSY)))
	{
		/* Set the driver as busy */
		SGTL5000_gbDriverStatus |= (1 << SGTL5000_STATUS_BUSY);
		/* Clear configured flag since will be configured again */
		SGTL5000_gbDriverStatus &= ~(1 << SGTL5000_STATUS_CONFIGURED);
		/* Stores the new frequency value to be use by the state machine */
		if(SGTL5000_FREQ_48KHZ == dwNewFreqValue ||  SGTL5000_FREQ_44_1KHZ == dwNewFreqValue \
				|| SGTL5000_FREQ_32KHZ == dwNewFreqValue ||  SGTL5000_FREQ_96KHZ == dwNewFreqValue )
		{	
			if(SGTL5000_FREQ_44_1KHZ == dwNewFreqValue)
			{
				SGTL5000_gbSysFs= SGTL5000_SYS_FS_44_1K;
				SGTL5000_gbPLLIntDivisor= SGTL5000_PLL_INT_DIV_44_1K;
				SGTL5000_gwPLLFracDivisor= SGTL5000_PLL_FRAC_DIV_44_1K;
			}
			else 
			{
				if(SGTL5000_FREQ_48KHZ == dwNewFreqValue)
				{
					SGTL5000_gbSysFs= SGTL5000_SYS_FS_48K;
				}
				else if (SGTL5000_FREQ_96KHZ == dwNewFreqValue)
				{
					SGTL5000_gbSysFs= SGTL5000_SYS_FS_96K;
				}
				else 
				{
					SGTL5000_gbSysFs= SGTL5000_SYS_FS_32K;			
				}
				SGTL5000_gbPLLIntDivisor= SGTL5000_PLL_INT_DIV_NO_44_1K;
				SGTL5000_gwPLLFracDivisor= SGTL5000_PLL_FRAC_DIV_NO_44_1K;
			}
			SGTL5000_gdwNewFrequencyValue = dwNewFreqValue;
			
			/*Identify if FS is multiple of SYS_MCLK*/
#ifdef SAI_MASTER
			SGTL5000_gubMclkFreqMul=SGTL5000_FS_MULTIPLE_OF_256;
			iAPSSIMaster_ChangeFreq(dwNewFreqValue);
#else
			dwMultiple=(SGTL5000_SYS_MCLK/dwNewFreqValue);	
			if(SGTL5000_FS_MULTIPLE_256 == dwMultiple)
			{
				SGTL5000_gubMclkFreqMul=SGTL5000_FS_MULTIPLE_OF_256;
			}
			else if (SGTL5000_FS_MULTIPLE_384 == dwMultiple)
			{		 
				SGTL5000_gubMclkFreqMul=SGTL5000_FS_MULTIPLE_OF_384;
		
			}
			else if (SGTL5000_FS_MULTIPLE_512 == dwMultiple)
			{
				SGTL5000_gubMclkFreqMul=SGTL5000_FS_MULTIPLE_OF_512;
			}
			else
			{
				SGTL5000_gubMclkFreqMul=SGTL5000_FS_NO_MULTIPLE;
			}
#endif
			SGTL5000_gslStateMachine.ActualState = SGTL5000_READ_PLL_STATUS_STATE;
			SGTL5000_gslStateMachine.NextState = SGTL5000_READ_PLL_STATUS_STATE;
		}
		else
		{
			SGTL5000_gbDriverStatus |= (1 << SGTL5000_STATUS_CONFIGURED);
			SGTL5000_gslStateMachine.ActualState = SGTL5000_CONTROL_STATE_IDLE;
			SGTL5000_gslStateMachine.NextState = SGTL5000_CONTROL_STATE_IDLE;
		}
		bStatus = _OK_;
	}
	return(bStatus);
}

void SGTL5000_vfnDriver(void)
{
	SGTL5000_vfnapStateMachine[SGTL5000_gslStateMachine.ActualState]();	
}

/** ***********************************************************************************************
*
* @brief    Provides the ability to write SGTL5000 control registers 
* 
* @param   	wRegister: Register to be written
* @param	wRegisterData: Value of the desired register
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnWriteRegister(uint16_t wRegister, uint16_t wRegisterData)
{
	/* set the driver as busy and the next state to wait physical layer driver */
	SGTL5000_gbDriverStatus |= (1 << SGTL5000_STATUS_BUSY);
	/* Flag IIC driver as busy */
	IIC_SET_DRIVER_BUSY;
	IIC_SET_RESERVE;
	IIC_SET_RELEASE_ON_COMP;

	/* Set the command buffer to send the data thru physical layer */
	SGTL5000_gabControlOutputBuffer[0] = (uint8_t)(wRegister>>8);
	SGTL5000_gabControlOutputBuffer[1] = (uint8_t)(wRegister & 0xFF);
	SGTL5000_gabControlOutputBuffer[2] = (uint8_t)(wRegisterData>>8);
	SGTL5000_gabControlOutputBuffer[3] = (uint8_t)(wRegisterData & 0xFF);
	
	/* Select I2C slave address */
	(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,SGTL5000_I2C_ADDRESS);
	/* Call the PE Write Block HAL I2C function */
	(void)iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, &SGTL5000_gabControlOutputBuffer[0], 4, LDD_I2C_SEND_STOP);
	
	/* Change state machine current state to SGTL5000_CONTROL_STATE_WAIT_COMM */
	SGTL5000_gslStateMachine.ActualState = SGTL5000_CONTROL_STATE_WAIT_COMM;
}

/** ***********************************************************************************************
*
* @brief    Provides the ability to read a control register. Internally it is 
      	  	 a wrapper of a a static (module) function
* 
* @param   	wRegister: Register to be read
* @param	bpRegisterData: Pointer to where the read data will be stored
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnReadRegister(uint16_t wRegister, uint8_t *bpRegisterData)
{
	/* Set the driver as busy and the next state logic */
	SGTL5000_gbDriverStatus |= (1 << SGTL5000_STATUS_BUSY);
	
	/* Set the I2C Driver busy flag */
	IIC_SET_DRIVER_BUSY;
	IIC_SET_RESERVE;
	IIC_CLEAR_RELEASE_ON_COMP;
	
	/* Setup control buffer */
	SGTL5000_gabControlOutputBuffer[0] = (uint8_t)(wRegister>>8);
	SGTL5000_gabControlOutputBuffer[1] = (uint8_t)(wRegister & 0xFF);
	
	/* Setup pointer to the reception buffer */
	SGTL5000_gbpReadData = bpRegisterData;

	/* Select I2C slave address */
	(void)(iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData, LDD_I2C_ADDRTYPE_7BITS, SGTL5000_I2C_ADDRESS));

	/* Set the logical address to be read */
	(void)(iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, &SGTL5000_gabControlOutputBuffer[0], 2, LDD_I2C_SEND_STOP));
	
	/* Sets the current and next state to finish the reading process */
	SGTL5000_gslStateMachine.ActualState = SGTL5000_CONTROL_STATE_WAIT_COMM;
	SGTL5000_gslStateMachine.NextState = SGTL5000_CONTROL_STATE_WAIT_READING;
}

/** ***********************************************************************************************
*
* @brief    Read the CHIP_ANA_POWER values to know if the SGTL5000 must be configured
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
void SGTL5000_vfnReadConfiguration_State(void)
{
	if(!IIC_RESERVED)
	{
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(SGTL5000_gbIICBusStatus);
		if(SGTL5000_gbIICBusStatus)
		{
			SGTL5000_gslStateMachine.PrevState =  SGTL5000_READ_CONFIGURATION_STATE;
			/* Read SGTL Register to determine if is currently configured */
			SGTL5000_vfnReadRegister(SGTL5000_CHIP_ANA_POWER, &SGTL5000_gabChipAnaPowerVal[0]);
				
			/* Set next state after reading */
			SGTL5000_gbNextStateAfterRead = SGTL5000_CHECK_CONFIGURATION_STATE;
		}
	}
}

/** ***********************************************************************************************
*
* @brief   This state changes determines if the SGTL device needs to be configured
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnCheckConfiguration_State(void)
{
	/* Compares Ana Power Register to determine if SGTL is already configured or not */
	if((SGTL5000_CHIP_ANA_PWR_RST_HIGH_VAL == SGTL5000_gabChipAnaPowerVal[0]) && 
			(SGTL5000_CHIP_ANA_PWR_RST_LOW_VAL == SGTL5000_gabChipAnaPowerVal[1]))
	{
		/* If not configured go to Control Init State */
		SGTL5000_gslStateMachine.ActualState = SGTL5000_SET_CONFIGURATION_STATE;
	}
	else
	{
		/* Since its already configured set the configured flag */
		SGTL5000_gbDriverStatus |= (1 << SGTL5000_STATUS_CONFIGURED);
		/* Go to idle state */
		SGTL5000_gslStateMachine.ActualState = SGTL5000_CONTROL_STATE_IDLE;
		SGTL5000_gslStateMachine.NextState = SGTL5000_CONTROL_STATE_IDLE;
	}
}

/** ***********************************************************************************************
*
* @brief   Set SGTL5000 Configuration
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnSetConfiguration_State(void)
{
	
	/*retries if the I2C driver reported an error*/
	if(SGTL5000_gbDriverStatus&(1<<SGTL5000_STATUS_I2C_ERROR))
	{
		/* Decrease the SGTL write index register ro write the same register again */
		bRegStep--;
		/* Clear the SGTL500 I2C error flag */
		SGTL5000_gbDriverStatus&= ~(1<<SGTL5000_STATUS_I2C_ERROR);
	}
	/* Check if there is any other write registers */
	if(bRegStep < SGTL5000_INIT_SIZE)
	{
		if(!IIC_RESERVED)
		{
			/* if driver is NOT busy */
			/** @todo Function Return */
			(void)IIC_CHECK_BUS(SGTL5000_gbIICBusStatus);
			if(SGTL5000_gbIICBusStatus)
			{
				SGTL5000_gslStateMachine.PrevState = SGTL5000_SET_CONFIGURATION_STATE;
				/* Send register and the data to be written */
				SGTL5000_vfnWriteRegister(SGTL5000_gawInitRegisterSequence[bRegStep],SGTL5000_gawInitRegisterValuesSequence[bRegStep]);
				/* Keep state machine in the control init state by setting current state for next state */
				SGTL5000_gslStateMachine.NextState = SGTL5000_SET_CONFIGURATION_STATE;
				/* Increase the SGTL index */
				bRegStep++;
			}
		}
	}
	else /* If there are no more register to be written */
	{
		/* Clear the write index register */
		bRegStep = 0;
		/* Set the SGTL already configured flag */
		SGTL5000_gbDriverStatus |= (1 << SGTL5000_STATUS_CONFIGURED);
		/* Go to idle state */
		SGTL5000_gslStateMachine.ActualState = SGTL5000_CONTROL_STATE_IDLE;
		SGTL5000_gslStateMachine.NextState = SGTL5000_CONTROL_STATE_IDLE;
	}
}

/** ***********************************************************************************************
*
* @brief    Read the current PLL status on/off
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
void SGTL5000_vfnReadPLLStatus_State(void)
{
	if(!IIC_RESERVED)
	{
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(SGTL5000_gbIICBusStatus);
		if(SGTL5000_gbIICBusStatus)
		{
			SGTL5000_gslStateMachine.PrevState =  SGTL5000_READ_PLL_STATUS_STATE;
			/* Read register SGTL5000_CHIP_ANA_POWER and after finishing read go to 
			   SGTL5000_CHANGE_FREQUENCY_STATE state */
			SGTL5000_vfnReadRegister(SGTL5000_CHIP_ANA_POWER, &SGTL5000_gabChipAnaPowerVal[0]);
			
			/* Set next state after reading */
			SGTL5000_gbNextStateAfterRead = SGTL5000_CHANGE_FREQUENCY_STATE;
		}
	}
}

/** ***********************************************************************************************
*
* @brief   This state changes SGTL5000 configuration according to the needed sample rate
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnChangeFrequency_State(void)
{
	if(SGTL5000_FS_NO_MULTIPLE == SGTL5000_gubMclkFreqMul) /*The PLL must be set */
	{
		/* Set register write order */
		SGTL5000_gawChangeFreqRegisterSequence[0] = SGTL5000_CHIP_ANA_POWER;
		SGTL5000_gawChangeFreqRegisterSequence[1] = SGTL5000_CHIP_PLL_CTRL;
		SGTL5000_gawChangeFreqRegisterSequence[2] = SGTL5000_CHIP_ANA_POWER;
		SGTL5000_gawChangeFreqRegisterSequence[3] = SGTL5000_CHIP_CLK_CTRL;
		
		/* Arrange read data from register SGTL5000_CHIP_ANA_POWER from two 8 bit variables 
							 * into a 16 bit one */
							 
		SGTL5000_gawChangeFreqRegisterValuesSequence[0] = (uint16_t)(SGTL5000_gabChipAnaPowerVal[0]<<0x08);
		SGTL5000_gawChangeFreqRegisterValuesSequence[0]|= (uint16_t)(SGTL5000_gabChipAnaPowerVal[1]);
		
		/* Prepare data to set register to turn PLL off */
		SGTL5000_gawChangeFreqRegisterValuesSequence[0] &= SGTL5000_TURN_OFF_PLL;
						
		/*Configure PLL integer and fractional numbers*/
		SGTL5000_gawChangeFreqRegisterValuesSequence[1] = ((SGTL5000_gbPLLIntDivisor << SGTL5000_PLL_CTRL_INT_DIVISOR)|SGTL5000_gwPLLFracDivisor);
				
		/* Prepare data to set register to turn PLL on */
		SGTL5000_gawChangeFreqRegisterValuesSequence[2]= SGTL5000_gawChangeFreqRegisterValuesSequence[0] |= SGTL5000_TURN_ON_PLL;
		
		/* Prepare data to set register to set system sample rate and if the PLL is used*/
		SGTL5000_gawChangeFreqRegisterValuesSequence[3] = ((SGTL5000_gbSysFs<< SGTL5000_CHIP_CLK_CTRL_SYS_FS)|SGTL5000_gubMclkFreqMul);
					
		SGTL5000_gbChangeFreqSize = 4;
	}
	else /*The PLL must not be set */
	{
		/* Set register write order */
		SGTL5000_gawChangeFreqRegisterSequence[0] = SGTL5000_CHIP_CLK_CTRL;
		SGTL5000_gawChangeFreqRegisterSequence[1] = SGTL5000_CHIP_ANA_POWER;
		
		/* Arrange read data from register SGTL5000_CHIP_ANA_POWER from two 8 bit variables 
				 * into a 16 bit one */
		SGTL5000_gawChangeFreqRegisterValuesSequence[1] = (uint16_t)(SGTL5000_gabChipAnaPowerVal[0]<<0x08);
		SGTL5000_gawChangeFreqRegisterValuesSequence[1]|= (uint16_t)(SGTL5000_gabChipAnaPowerVal[1]);

		/* Prepare data to set register to set system sample rate and if the PLL is not used*/
		SGTL5000_gawChangeFreqRegisterValuesSequence[0] = ((SGTL5000_gbSysFs<< SGTL5000_CHIP_CLK_CTRL_SYS_FS)|SGTL5000_gubMclkFreqMul);
		
		/* Prepare data to set register to turn PLL off */
		SGTL5000_gawChangeFreqRegisterValuesSequence[1] &= SGTL5000_TURN_OFF_PLL;
				
		SGTL5000_gbChangeFreqSize=2;	
	}
	
	SGTL5000_gslStateMachine.ActualState = SGTL5000_WRITE_FREQ_SEQUENCE_STATE;
	SGTL5000_gslStateMachine.NextState = SGTL5000_WRITE_FREQ_SEQUENCE_STATE;
}

/** ***********************************************************************************************
*
* @brief   This state sends a write I2C command to change the SGTL5000 frequency
* 
* @param   	void
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnWriteFreqSequence_State(void)
{
	

	/* Retries if the I2C driver reported an error*/
	if(SGTL5000_gbDriverStatus&(1<<SGTL5000_STATUS_I2C_ERROR))
	{
		bRegStep--;
		SGTL5000_gbDriverStatus&= ~(1<<SGTL5000_STATUS_I2C_ERROR);
	}

	/* Check if there is any other write registers */
	if(bRegStep < SGTL5000_gbChangeFreqSize)
	{
		if(!IIC_RESERVED)
		{
			SGTL5000_gslStateMachine.NextState = SGTL5000_WRITE_FREQ_SEQUENCE_STATE;
			/** @todo Function Return */
			(void)IIC_CHECK_BUS(SGTL5000_gbIICBusStatus);
			if(SGTL5000_gbIICBusStatus)
			{
				
				SGTL5000_gslStateMachine.PrevState = SGTL5000_WRITE_FREQ_SEQUENCE_STATE;
				/* Read the pre-configured data (to change frequency and turn on/off PLL) and write it 
				 * to the corresponding register */
				SGTL5000_vfnWriteRegister(SGTL5000_gawChangeFreqRegisterSequence[bRegStep], \
						SGTL5000_gawChangeFreqRegisterValuesSequence[bRegStep]);	
				bRegStep++;
			}
		}
	}
	else
	{
	/* When finished, set IDLE state to Actual and Next states */
		bRegStep = 0;
		SGTL5000_gbDriverStatus |= (1 << SGTL5000_STATUS_CONFIGURED);
		SGTL5000_gslStateMachine.ActualState = SGTL5000_CONTROL_STATE_IDLE;
		SGTL5000_gslStateMachine.NextState = SGTL5000_CONTROL_STATE_IDLE;
	}
}

/** ***********************************************************************************************
*
* @brief    Waits for the physical layer driver to finish its process
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnWaitComm_State(void)
{
	if(!IIC_DRIVER_BUSY)
	{
		/*Check for the physical layer driver to be free*/
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(SGTL5000_gbIICBusStatus);
		if(SGTL5000_gbIICBusStatus)
		{
			/* Confirm if there is no error */
			if(IIC_DRIVER_ERROR)
			{
				IIC_CLEAR_DRIVER_ERROR;
				
				SGTL5000_gbDriverStatus |= (1<<SGTL5000_STATUS_I2C_ERROR);

			}
			else if(IIC_NAK_ERROR_FLAG)
			{
				IIC_CLEAR_NAK_ERROR_FLAG;
				IIC_CLEAR_RESERVE;
				
				if(bRegStep)
				{
					bRegStep--;
				}
				
				SGTL5000_gslStateMachine.ActualState = SGTL5000_gslStateMachine.PrevState;
				SGTL5000_gslStateMachine.NextState = SGTL5000_gslStateMachine.PrevState;
			}
			else
			{
				if(IIC_GET_RELEASE_ON_COMP)
				{
					/* Unreserve module if the intended operation was a write */
					IIC_CLEAR_RESERVE;
				}
				
				SGTL5000_gslStateMachine.ActualState = SGTL5000_gslStateMachine.NextState;
			}
		}
	}
}

/** ***********************************************************************************************
*
* @brief   Waits for the physical layer driver to finish its process
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnWaitReading_State(void)
{
	if(!IIC_DRIVER_BUSY)
	{
		/*Check for the physical layer driver to be free*/
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(SGTL5000_gbIICBusStatus);
		if(SGTL5000_gbIICBusStatus)
		{
			/* Set to busy the I2C driver flag */
			IIC_SET_DRIVER_BUSY;
			IIC_SET_RELEASE_ON_COMP;
			
			/* Select I2C slave address */
			(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,SGTL5000_I2C_ADDRESS);
			/* Call the PE Receive Block HAL function */
			(void)iAPI2C1_MasterReceiveBlock(iAPI2C1_DeviceData, SGTL5000_gbpReadData, \
					2, LDD_I2C_SEND_STOP );
			/* Set the state machine to the wait state */
			SGTL5000_gslStateMachine.ActualState = SGTL5000_CONTROL_STATE_WAIT_COMM;
			
			/* Go to configuration state */
			SGTL5000_gslStateMachine.NextState = SGTL5000_gbNextStateAfterRead;
		}
	}
}

/** ***********************************************************************************************
*
* @brief    Idle state
* 
* @param   	none
* 
* @return  	none
*          
**************************************************************************************************/
static void SGTL5000_vfnIDLE(void)
{
	SGTL5000_gbDriverStatus &= ~(1 << SGTL5000_STATUS_BUSY);
}
