/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef AudioClkSyncDriver_H_
#define AudioClkSyncDriver_H_

/**********************************************************************************************************************/
/*                                 				    Includes Section                                         		  */
/**********************************************************************************************************************/
#include "ProjectTypes.h"

/**********************************************************************************************************************/
/*                                 				    Typedef Section              		                              */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                             				    Defines & Macros Section            		                          */
/**********************************************************************************************************************/
#define ACSD_FRAME20_FOR_44100HZ			(19)
#define ACSD_FREQ_32000HZ					(32000)
#define ACSD_FREQ_44100HZ					(44100)
#define ACSD_FREQ_48000HZ					(48000)

#define ACSD_FRACTION_BITS				(14)
#define ACSD_RATIO_Q_SHIFT				(24)

#define TCO_MEASURE_NOT_READY			(0)
/**********************************************************************************************************************/
/*                           			     Function-like Macros Section           		                          */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                            				   Extern Constants Section              			                      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                               			   Extern Variables Section         		                              */
/**********************************************************************************************************************/

/**********************************************************************************************************************/
/*                            			 	  Function Prototypes Section                                 			  */
/**********************************************************************************************************************/

void ACSD_vfnResetFeedback(void);

void ACSD_vfnInitVariables(void);
void ACSD_vfnDriver(uint32_t dwBufferDifference);
void ACSD_vfnDMAFrameMonitor(void);
void ACSD_vfnUSBFrameMonitor(void);
void ACSD_vfnSetRatio(uint64_t qwRatio);
/**********************************************************************************************************************/

#endif 

