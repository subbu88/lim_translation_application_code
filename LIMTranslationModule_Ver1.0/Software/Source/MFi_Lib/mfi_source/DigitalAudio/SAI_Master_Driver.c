/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *		This module contains all the functions needed to communicate with the I2S Module just
 * 		for master mode.
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/

#include "SAI_Master_Driver.h"

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/



#ifdef CORE_CLK_144MHZ
	#define SR_48K_FRACT		(31)
	#define SR_48K_DIVIDE		(374)
	#define SR_44_1K_FRACT		(48)
	#define SR_44_1K_DIVIDE		(624)
	#define SR_32K_FRACT		(63)
	#define SR_32K_DIVIDE		(1124)
#elif defined(CORE_CLK_120MHZ)
	#define SR_48K_FRACT		(63) 
	#define SR_48K_DIVIDE		(624)
	#define SR_44_1K_FRACT		(293)  
	#define SR_44_1K_DIVIDE		(3124)
	#define SR_32K_FRACT		(127)      
	#define SR_32K_DIVIDE		(1874)
#elif defined(CORE_CLK_96MHZ)
	#define SR_48K_FRACT		(15)
	#define SR_48K_DIVIDE		(124)
	#define SR_44_1K_FRACT		(146)
	#define SR_44_1K_DIVIDE		(1249)
	#define SR_32K_FRACT		(31)
	#define SR_32K_DIVIDE		(374)
#elif defined(CORE_CLK_72MHZ)
	#define SR_48K_FRACT		(63)
	#define SR_48K_DIVIDE		(374)
	#define SR_44_1K_FRACT		(97)
	#define SR_44_1K_DIVIDE		(624)
	#define SR_32K_FRACT		(127)
	#define SR_32K_DIVIDE		(1124)
#elif defined(CORE_CLK_48MHZ)
	#define SR_48K_FRACT		(31)
	#define SR_48K_DIVIDE		(124)
	#define SR_44_1K_FRACT		(146)
	#define SR_44_1K_DIVIDE		(624)
	#define SR_32K_FRACT		(63)
	#define SR_32K_DIVIDE		(374)
#elif defined(CORE_CLK_24MHZ)
	#define SR_48K_FRACT		(63)
	#define SR_48K_DIVIDE		(124)
	#define SR_44_1K_FRACT		(150)  	/* just for this case error 0.00106% */
	#define SR_44_1K_DIVIDE		(320)	/* just for this case error 0.00106% */
	#define SR_32K_FRACT		(127)
	#define SR_32K_DIVIDE		(374)
#endif
/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/** ***********************************************************************************************
*
* @brief    starts I2S Master initialization, just set up the registers needed for master mode.
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/

void iAPSSIMasterMode_Init(void)
{
	I2S0_MCR &= (uint32_t)~(uint32_t)(I2S_MCR_MOE_MASK); /* Disable MCLK divider */
	while((I2S0_MCR & I2S_MCR_MOE_MASK) != 0U){} /* Wait for MCLK disable*/
	   
	/* MCLK OUT */
	PORTE_PCR6 |= PORT_PCR_MUX(0x04)|PORT_PCR_DSE_MASK;
	/* Select PLL as clock input */
	I2S0_MCR |=  I2S_MCR_MICS(3);
	/* Set 44100 sample frequency by default */
	iAPSSIMaster_ChangeFreq(44100);
	/* Tx setting for master mode */	
	/* Enable master clk output at SSI module */
	I2S0_MCR |= I2S_MCR_MOE_MASK;
	/* Enable bit clk as output, PLL clk input and set bit clock divisor  */	
	I2S0_TCR2 |= I2S_TCR2_BCD_MASK|I2S_TCR2_MSEL(0x01)|I2S_TCR2_DIV(1);
	/* Enable Frame select as output*/
	I2S0_TCR4 |=  I2S_TCR4_FSD_MASK;
}

/** ***********************************************************************************************
*
* @brief    This function change PLL setting corresponding to desired sample rate .
* 
* @param   	uint32_t dwNewFreq - New desired Sample Rate.
* 
* @return  	void
*          
**************************************************************************************************/


void iAPSSIMaster_ChangeFreq(uint32_t dwNewFreq)
{
    if(44100 == dwNewFreq)
    {
        /* 44.1kHz * 64 * 4 */
        I2S0_MDR = I2S_MDR_FRACT(SR_44_1K_FRACT)|I2S_MDR_DIVIDE(SR_44_1K_DIVIDE);
    }
    else if (48000 == dwNewFreq)
    {
        /* 48kHz * 64 * 4 */
        I2S0_MDR = I2S_MDR_FRACT(SR_48K_FRACT)|I2S_MDR_DIVIDE(SR_48K_DIVIDE);
    }
    else
    {
        /*  32kHz * 64 * 4 */
        I2S0_MDR = I2S_MDR_FRACT(SR_32K_FRACT)|I2S_MDR_DIVIDE(SR_32K_DIVIDE);
    }
}
