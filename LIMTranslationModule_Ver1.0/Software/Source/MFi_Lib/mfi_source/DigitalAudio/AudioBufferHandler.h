/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef AUDIOBUFFERHANDLER_H_
#define AUDIOBUFFERHANDLER_H_

/**********************************************************************************************************************/
/*                                 				    Includes Section                                         		  */
/**********************************************************************************************************************/
#include "ProjectTypes.h"


/**********************************************************************************************************************/
/*                                 				    Typedef Section              		                              */
/**********************************************************************************************************************/
typedef enum
{
	AUDIO_BUFFER_HANDLER_TX_STARTED = 0,
	AUDIO_BUFFER_HANDLER_RX_STARTED
}_AudioBufferHandlerStatus;

/**********************************************************************************************************************/
/*                             				    Defines & Macros Section            		                          */
/**********************************************************************************************************************/
/** Compute the buffer size with the sampling rate, channels and amount of audio to buffer */
#define AUDIO_OUTPUT_RING_BUFFER_SIZE				(1024)
#define AUDIO_BUFFER_30_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 3) / 10)
#define AUDIO_BUFFER_40_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 4) / 10)
#define AUDIO_BUFFER_45_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 45) / 100)
#define AUDIO_BUFFER_49_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 49) / 100)
#define AUDIO_BUFFER_50_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 50) / 100)
#define AUDIO_BUFFER_51_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 51) / 100)
#define AUDIO_BUFFER_55_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 55) / 100)
#define AUDIO_BUFFER_60_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 6) / 10)
#define AUDIO_BUFFER_68_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 68) / 100)
#define AUDIO_BUFFER_70_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 70) / 100)
#define AUDIO_BUFFER_72_PERCENT						((AUDIO_OUTPUT_RING_BUFFER_SIZE * 72) / 100)

/***Used for USB_SYNC*****/
#define AUDIO_BUFFER_UP_PERCENT							(AUDIO_BUFFER_70_PERCENT)
#define AUDIO_BUFFER_MID_PERCENT						(AUDIO_BUFFER_50_PERCENT)
#define AUDIO_BUFFER_DOWN_PERCENT						(AUDIO_BUFFER_30_PERCENT)

#define AUDIO_CHANNEL_NUMBER						(2)
/********/

#define AUDIO_BUFFER_HANDLER_CHECK_STATUS(x)		(AudioBufferHandler_gbStatus & (1 << (x)) )
#define AUDIO_BUFFER_HANDLER_SET_STATUS(x)			(AudioBufferHandler_gbStatus |= (1 << (x)) )
#define AUDIO_BUFFER_HANDLER_CLEAR_STATUS(x)		(AudioBufferHandler_gbStatus &= ~(1 << (x)) )

#define AUDIO_BUFFER_SAMPLE_SIZE					(2)
#define AUDIO_IIS_FIFO_SIZE							(8)
#define AUDIO_BUFFER_CLEAR_I2S_SHIFTER				I2S0_TCR5 &= ~0x00001F00
//#define I2S_RIGHT_JUSTIFIED_MODE

#ifdef I2S_RIGHT_JUSTIFIED_MODE
#define TCR5_FBT_SIZE_4		(0x00001F00)
#define TCR5_FBT_SIZE_3		(0x00001F00)
#define TCR5_FBT_SIZE_2		(0x00001F00)
#define TCR5_FBT_SIZE_1		(0x00001700)
#else
#define TCR5_FBT_SIZE_4		(0x00001F00)
#define TCR5_FBT_SIZE_3		(0x00001700)
#define TCR5_FBT_SIZE_2		(0x00000F00)
#define TCR5_FBT_SIZE_1		(0x00000700)
#endif
#if (AUDIO_BUFFER_SAMPLE_SIZE == 4)
	#define AUDIO_BUFFER_SET_I2S_SHIFTER				AUDIO_BUFFER_CLEAR_I2S_SHIFTER; I2S0_TCR5 |= TCR5_FBT_SIZE_4
#define SAMPLE_SIZE_FOR_CALCULATE_OCCUPANCY				4
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 3)
	#define AUDIO_BUFFER_SET_I2S_SHIFTER				AUDIO_BUFFER_CLEAR_I2S_SHIFTER; I2S0_TCR5 |= TCR5_FBT_SIZE_3
#define SAMPLE_SIZE_FOR_CALCULATE_OCCUPANCY				4
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 2)
#define AUDIO_BUFFER_SET_I2S_SHIFTER				AUDIO_BUFFER_CLEAR_I2S_SHIFTER; I2S0_TCR5 |= TCR5_FBT_SIZE_2
#define SAMPLE_SIZE_FOR_CALCULATE_OCCUPANCY				2
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 1)
	#define AUDIO_BUFFER_SET_I2S_SHIFTER				AUDIO_BUFFER_CLEAR_I2S_SHIFTER; I2S0_TCR5 |= TCR5_FBT_SIZE_1
#define SAMPLE_SIZE_FOR_CALCULATE_OCCUPANCY				1
#endif	

/**********************************************************************************************************************/
/*                           			     Function-like Macros Section           		                          */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                            				   Extern Constants Section              			                      */
/**********************************************************************************************************************/


/**********************************************************************************************************************/
/*                               			   Extern Variables Section         		                              */
/**********************************************************************************************************************/
extern uint8_t AudioBufferHandler_gbStatus;

#if (AUDIO_BUFFER_SAMPLE_SIZE == 4)
	#if defined(__CWCC__) || defined(__GNUC__) || defined(__CC_ARM)
		extern uint32_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE] __attribute__((aligned (AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE)));
	#elif defined(__IAR_SYSTEMS_ICC__)
		#pragma data_alignment = AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE
		extern uint32_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE];
	#endif
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 3)      
	#if defined(__CWCC__) || defined(__GNUC__) || defined(__CC_ARM)
		extern uint32_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE] __attribute__((aligned (AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE)));
	#elif defined(__IAR_SYSTEMS_ICC__)
		#pragma data_alignment = AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE
		extern uint32_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE];
	#endif
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 2)
	#if defined(__CWCC__) || defined(__GNUC__) || defined(__CC_ARM)
		extern uint16_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE] __attribute__((aligned (AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE)));
	#elif defined(__IAR_SYSTEMS_ICC__)
		#pragma data_alignment = AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE
		extern uint16_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE];
	#endif
#elif (AUDIO_BUFFER_SAMPLE_SIZE == 1)
	#if defined(__CWCC__) || defined(__GNUC__) || defined(__CC_ARM)
		extern uint8_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE] __attribute__((aligned (AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE)));
	#elif defined(__IAR_SYSTEMS_ICC__)
		#pragma data_alignment = AUDIO_OUTPUT_RING_BUFFER_SIZE*AUDIO_BUFFER_SAMPLE_SIZE
		extern uint8_t AudioBufferHandler_gdwaOutRingBuffer[AUDIO_OUTPUT_RING_BUFFER_SIZE];
	#endif
#endif

/**********************************************************************************************************************/
/*                            			 	  Function Prototypes Section                                 			  */
/**********************************************************************************************************************/
void AudioBufferHandler_vfnInit(void);
void AudioBufferHandler_vfnFillHalfBuffer(uint32_t dwDiffSentRcvBuffer);
uint32_t AudioBufferHandler_dwfnCalculateOccupancy(uint32_t DestAddr, uint32_t SrcAddr);

/**********************************************************************************************************************/

#endif /* AUDIOBUFFERHANDLER_H_ */
