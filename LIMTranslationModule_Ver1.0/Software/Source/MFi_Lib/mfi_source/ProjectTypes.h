/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef PROJECTTYPES_H_
#define PROJECTTYPES_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include <stdint.h>
#include <stdlib.h>

#include "mfi_cfg.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/

#ifndef _TRUE_
  #define  _TRUE_  0x01u
#endif

#ifndef _FALSE_
  #define  _FALSE_   0x00u
#endif

#ifndef _ERROR_
	#define _ERROR_ 0x80u
#endif
#ifndef _OK_
	#define _OK_    0x00u
#endif
 
#ifndef NULL
  #define  NULL   0x00u
#endif

#ifndef ARRAY_N_ELEMENTS
	#define ARRAY_N_ELEMENTS(x)         (sizeof(x) / sizeof(x[0]))
#endif

#define U32_TO_U8_SPLIT(x)			(uint8_t)(x >> 24),(uint8_t)(x >> 16), (uint8_t)(x >> 8),(uint8_t)(x)

#define BUS_CLOCK					(25000000)
#define BIG_ENDIAN_CORE				(_FALSE_)
#define HARDWARE_SWAP				(_FALSE_)
#define MEM_UNALIGNMENT 			(_TRUE_)	/* MCU supports memory unalignment */


/* Do NOT use this function like macro in code, instead use BYTESWAP32_MEM_ALIG(...)
 * BYTESWAP32_MEM_ALIG_CORE(...): does a 32 bit byte swap compatible with dereferenced
 * unaligned data in memory (i.e. dereferencing pointers to unaligned structure elements).
 */
#define BYTESWAP32_MEM_ALIG_CORE(dwDataSource, dwDataDest) ({	\
		uint8_t baBackValue[2]; \
		uDWordByte_t *upSwapTmpIn = (uDWordByte_t*)&dwDataSource; \
		uDWordByte_t *upSwapTmpOut = (uDWordByte_t*)&dwDataDest; \
		baBackValue[0] = upSwapTmpIn->baDWordByte[0]; \
		baBackValue[1] = upSwapTmpIn->baDWordByte[1]; \
		upSwapTmpOut->baDWordByte[0] = upSwapTmpIn->baDWordByte[3]; \
		upSwapTmpOut->baDWordByte[1] = upSwapTmpIn->baDWordByte[2]; \
		upSwapTmpOut->baDWordByte[2] = baBackValue[1]; \
		upSwapTmpOut->baDWordByte[3] = baBackValue[0]; })

/* Do NOT use this function like macro in code, instead use BYTESWAP16_MEM_ALIG(...)
 * BYTESWAP16_MEM_ALIG_CORE(...): does a 16 bit byte swap compatible with dereferenced
 * unaligned data in memory (i.e. dereferencing pointers to unaligned structure elements).
 */
#define BYTESWAP16_MEM_ALIG_CORE(wDataSource, wDataDest) ({	\
	uint8_t baBackValue; \
	uWordByte_t *upSwapTmpIn = (uWordByte_t*)&wDataSource; \
	uWordByte_t *upSwapTmpOut = (uWordByte_t*)&wDataDest; \
	baBackValue = upSwapTmpIn->baWordByte[0] ; \
	upSwapTmpOut->baWordByte[0] = upSwapTmpIn->baWordByte[1] ; \
	upSwapTmpOut->baWordByte[1] = baBackValue; })

#if(_FALSE_ == BIG_ENDIAN_CORE)
	#if (HARDWARE_SWAP == _TRUE_)
		#if defined	(__GNUC__) || defined(__IAR_SYSTEMS_ICC__) || defined(__CC_ARM)
			extern unsigned long Reverse32bit(unsigned long);
			extern unsigned short Reverse16bit(unsigned short);
		#elif defined(__CWCC__)
			asm unsigned long Reverse32bit(unsigned long);
			asm unsigned short Reverse16bit(unsigned short);
		#else
			#error "Compiler not supported."
		#endif
	
		#define BYTESWAP32(dwDataSource, dwDataDest)	dwDataDest = Reverse32bit(dwDataSource)
		#define BYTESWAP16(wDataSource, wDataDest) 		wDataDest = Reverse16bit(wDataSource)
	
		#if (MEM_UNALIGNMENT == _TRUE_)
			#define BYTESWAP32_MEM_ALIG(dwDataSource, dwDataDest)	BYTESWAP32(dwDataSource, dwDataDest)
			#define BYTESWAP16_MEM_ALIG(wDataSource, wDataDest)		BYTESWAP16(wDataSource, wDataDest)
		#else
			#define BYTESWAP32_MEM_ALIG(dwDataSource, dwDataDest)	BYTESWAP32_MEM_ALIG_CORE(dwDataSource, dwDataDest)
			#define BYTESWAP16_MEM_ALIG(wDataSource, wDataDest)		BYTESWAP16_MEM_ALIG_CORE(wDataSource, wDataDest)
		#endif
	#else
		#if (MEM_UNALIGNMENT == _TRUE_)
			#define BYTESWAP32(dwDataSource, dwDataDest)	dwDataDest = (uint32_t)((((dwDataSource) & 0xFF000000) >> 24) \
																| (((dwDataSource) & 0x00FF0000) >> 8) \
																| (((dwDataSource) & 0x0000FF00) << 8) \
																| (((dwDataSource) & 0x000000FF) << 24))															
			#define BYTESWAP16(wDataSource, wDataDest)		wDataDest = (uint16_t)((((wDataSource) & 0xFF00) >> 0x08) \
																| (((wDataSource) & 0x00FF) << 0x8))

			#define BYTESWAP32_MEM_ALIG(dwDataSource, dwDataDest)	BYTESWAP32_MEM_ALIG_CORE(dwDataSource, dwDataDest)
			#define BYTESWAP16_MEM_ALIG(wDataSource, wDataDest)		BYTESWAP16_MEM_ALIG_CORE(wDataSource, wDataDest)
		#else
			#define BYTESWAP32(dwDataSource, dwDataDest)	BYTESWAP32_MEM_ALIG_CORE(dwDataSource, dwDataDest)
			#define BYTESWAP16(wDataSource, wDataDest)		BYTESWAP16_MEM_ALIG_CORE(wDataSource, wDataDest)
			
			#define BYTESWAP32_MEM_ALIG(dwDataSource, dwDataDest)	BYTESWAP32_MEM_ALIG_CORE(dwDataSource, dwDataDest)
			#define BYTESWAP16_MEM_ALIG(wDataSource, wDataDest)		BYTESWAP16_MEM_ALIG_CORE(wDataSource, wDataDest)	
		#endif
	#endif
#else
	#define BYTESWAP32(dwDataSource, dwDataDest)		(dwDataDest = dwDataSource;)
	#define BYTESWAP16(wDataSource, wDataDest)		(wDataDest = wDataSource;)
#endif

#if (defined(__CWCC__) || defined(__IAR_SYSTEMS_ICC__) || defined(__GNUC__))
	#define NOP		asm("nop");
#elif defined (__CC_ARM)
	#define NOP		__nop();
#else
	#error "Compiler not supported."
#endif	   

//#define MFI_DEBUG

#ifdef MFI_DEBUG
#define dprint(...) printf(__VA_ARGS__)
#else
#define dprint(...) do{ } while (false)
#endif


//#define TRANSDUCER_BUF_SIZE	16398
#define TRANSDUCER_BUF_SIZE		16402 //16k+14+4


/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
			
//! Type for status return. 
typedef uint32_t status_t;
#if 0			
typedef unsigned char			uint8_t;				//!< UNSIGNED 8 BIT DEFINITION
typedef unsigned short			uint16_t;			//!< UNSIGNED 16 BIT DEFINITION
typedef unsigned long			uint32_t;			//!< UNSIGNED 32 BIT DEFINITION
typedef unsigned long long		uint64_t;			//!< UNSIGNED 64 BIT DEFINITION
typedef signed char				int8_t;				//!< SIGNED 8 BIT DEFINITION
typedef signed short			int16_t;			//!< SIGNED 16 BIT DEFINITION
typedef signed long				int32_t;			//!< SIGNED 32 BIT DEFINITION
typedef signed long	long		int64_t;			//!< SIGNED 64 BIT DEFINITION
#endif
typedef union{
	uint32_t dwWord;
	uint8_t baDWordByte[4];
}uDWordByte_t;

typedef union{
	uint16_t wWord;
	uint8_t baWordByte[2];
}uWordByte_t;

/* State Machine */
  typedef struct
  {
    uint8_t ActualState;
    uint8_t PrevState;
    uint8_t NextState;
    uint8_t ErrorState;
  }sSM;
/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

typedef struct {
    char hid_dev[32];
    char i2c_dev[32];
    uint8_t i2c_addr;
} MFIHostConfig;

extern MFIHostConfig g_hostConfig;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/

#endif /* PROJECTTYPES_H_ */
