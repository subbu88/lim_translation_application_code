/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *		This file is used to configure the overall stack parameters, like transports.
 *
 *END*********************************************************************************************/

#ifndef MFI_CONFIG_H_
#define MFI_CONFIG_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/

#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
/* iAP version selection */
#define MFI_IAP1_ENABLE                     (_TRUE_)
#define MFI_IAP2_ENABLE                     (_TRUE_)

/* iAP Transport configuration. Enabling USB host mode and USB device mode enables Role Switch by default */
#define MFI_TRANSPORT_USB_HOST_MODE         (_FALSE_)
#define MFI_TRANSPORT_USB_DEVICE_MODE       (_TRUE_)
#define MFI_TRANSPORT_UART                  (_FALSE_)

/* Digital Audio configuration */
#define MFI_DIGITAL_AUDIO_ENABLE            (_FALSE_)
#define MFI_AUDIO_SYNCHRONOUS_MODE          (_FALSE_)      /* _FALSE_ selects Asynchronous audio */
#define MFI_AUDIO_TCO_MODE                  (_FALSE_)     /* _FALSE_ selects Sample Rate Converter */
#define MFI_SGTL_ENABLE                     (_TRUE_)

#define AUDIO_CLASS_2_0			            (_TRUE_)

/* Microcontroller selection */
#define MFI_KL25_ENABLE                     (_FALSE_)

/* Device charging configuration */
#define MFI_CHARGE_CURRENT_AVAILABLE    	2100
#define MFI_CHARGE__TRUE_					(_TRUE_)
#define MFI_CHARGE_ENABLED                   (_TRUE_)

/* Number of allowed communication applications */
#define MFI_MAX_APPS_NUMBER                   (2)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION

/* Internal macros definition based on configuration options */
#if (MFI_IAP1_ENABLE == _TRUE_)
#define MFI_IAP1
#endif

#if (MFI_IAP2_ENABLE == _TRUE_)
#define MFI_IAP2
#endif

#if (MFI_TRANSPORT_USB_HOST_MODE == _TRUE_)
#define IAPI_INTERFACE_USB_DEV

/* Macros required by the USB Device stack for Kinetis processors */
#define __MK_xxx_H__
#endif

#if (MFI_TRANSPORT_USB_DEVICE_MODE == _TRUE_)
#define IAPI_INTERFACE_USB_HOST
#endif



#if (MFI_TRANSPORT_UART == _TRUE_)
#define IAPI_INTERFACE_UART
#endif

#if (MFI_TRANSPORT_USB_HOST_MODE == _TRUE_) && (MFI_TRANSPORT_USB_DEVICE_MODE == _TRUE_)
#define IAPI_INTERFACE_USB_ROLE_SWITCH
#endif

#if (MFI_TRANSPORT_UART == _TRUE_) && ((MFI_TRANSPORT_USB_HOST_MODE == _TRUE_) || (MFI_TRANSPORT_USB_DEVICE_MODE == _TRUE_))
#error "Concurrent USB and UART transports are not supported."
#endif

#if (MFI_DIGITAL_AUDIO_ENABLE == _TRUE_)
#define MFI_AUDIO_SUPPORT

#if (MFI_AUDIO_SYNCHRONOUS_MODE == _TRUE_)
#define USB_SYNC
#else
#define USB_ASYNC
#endif

#if (MFI_KL25_ENABLE == _TRUE_)
#define MFI_KL25
#endif

#if ((MFI_AUDIO_TCO_MODE == _TRUE_) || defined(USB_ASYNC))
#define DAD_TCO
#endif

#if (MFI_TRANSPORT_USB_HOST_MODE == _FALSE_) && (MFI_AUDIO_SYNCHRONOUS_MODE == _FALSE_)
#error "Asynchronous Audio is only supported in USB Host mode."
#endif

#if MFI_TRANSPORT_UART == _TRUE_
#error "Digital Audio may not be enabled when communicating through UART."
#endif

#if ((MFI_AUDIO_SYNCHRONOUS_MODE == _FALSE_) && (MFI_AUDIO_TCO_MODE == _TRUE_))
#error "Tunable Clock Oscillator can only be used with Synchronous audio mode."
#endif

#endif

//@todo Check the max number of application iPod supports
#if MFI_MAX_APPS_NUMBER > 6
#error "Amount of max applications allowed is x"
#endif

/*
#if MFI_MAX_APPS_NUMBER >= 1
#define IAP1_APPLICATION_DATA_TRANSFER_CALLBACK_1
#endif
#if MFI_MAX_APPS_NUMBER >= 2
#define IAP1_APPLICATION_DATA_TRANSFER_CALLBACK_2
#endif
#if MFI_MAX_APPS_NUMBER >= 3
#define IAP1_APPLICATION_DATA_TRANSFER_CALLBACK_3
#endif
#if MFI_MAX_APPS_NUMBER >= 4
#define IAP1_APPLICATION_DATA_TRANSFER_CALLBACK_4
#endif
#if MFI_MAX_APPS_NUMBER >= 5
#define IAP1_APPLICATION_DATA_TRANSFER_CALLBACK_5
#endif
*/

/*! Application callbacks ID's */
typedef enum
{
    kIapaCallbackId1 = 0,
    kIapaCallbackId2,
    kIapaCallbackId3,
    kIapaCallbackId4,
    kIapaCallbackId5,
    kIapaCallbackId6
}iapaCallbacksIds_t;
/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/

#endif /* MFI_CONFIG_H_ */
