/**************************************************************************************************
 *
 * Freescale Semiconductor Inc.
 * (c) Copyright 2004-2010 Freescale Semiconductor, Inc.
 * ALL RIGHTS RESERVED.
 *
***************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
 * IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
***********************************************************************************************//*!
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * @file iAP_Authentication.h
 *
 * @author B24509
 *
 * @date Mar 8, 2012
 *
 * @brief
**************************************************************************************************/

#ifndef IAP_AUTHENTICATION_H_
#define IAP_AUTHENTICATION_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

//#include "Events.h"
//#include "iAP_Timer.h"


/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP_ACC_AUTHENTICATION_ENABLE                (_TRUE_)
#define IAP_IOS_AUTHENTICATION_ENABLE                (_FALSE_)

#define IAP_AUTH_APPLE_CERT_RESULT_OK                (0x00u) 
#define IAP_AUTH_APPLE_SIGN_RESULT_OK                (0x00u)
#define IAP_AUTH_APPLE_CERT_RESULT_ERR                (0xFFu)
#define IAP_AUTH_APPLE_SIGN_RESULT_ERR                (0xFFu)

#define IAP_AUTH_OK                                        0
#define IAP_AUTH_ERROR                                    0xFF

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef enum  errorCodes
{
    kAuthenticationNoError = 0,
    kAuthenticationInvalidRegisterForRead,
    kAuthenticationInvalidRegisterForWrite,
    kAuthenticationInvalidSignatureLength,
    kAuthenticationInvalidChallengeLength,
    kAuthenticationInvalidCertificateLength,
    kAuthenticationInternalErrorDuringSignatureGeneration,
    kAuthenticationInternalErrorDuringChallengeGeneration,
    kAuthenticationInternalErrorDuringSignatureVerification,
    kAuthenticationInternalErrorDuringCertificateVerification,
    kAuthenticationInvalidProcessControl,
    kAuthenticationProcessControlOutOfSequence,
    KAuthenticationBusy,            
    KAuthenticationTimeoutError,                   
    KAuthenticationCommandComplete
} authenticationCopressesorErrorCode;


typedef struct auth_config
{
    void (*events_callback)(uint8_t);
    uint8_t i2c_addr;
    char i2c_dev[32];
}auth_config_parameters;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/
#define IAP_AUTH_CP_BUSY                    (AuthenticationStatus & (1 << KAuthenticationBusy))
#define IAP_AUTH_CP_ERROR                    (AuthenticationStatus & (1 << KAuthenticationError))

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern uint16_t authenticationCertificateSize;
extern uint8_t iAP_Auth_gbaRxBufferForCP[128];            /**< Buffer used to read and write data from-to CP */
extern uint8_t authenticationCoprocessorVersionAndID[6];            /**< Buffer to read current CP version and Device ID*/
extern uint8_t iAP_Auth_CPCertificate[1284];

/*************************************************************************************************/
/*                                Function Prototypes Section                                   */
/*************************************************************************************************/
uint8_t iAP_Auth_bfnInit(auth_config_parameters * config_parameters);

uint8_t iAP_Auth_bfnDeInit(void);

void iAP_Auth_vfnCancel(void);

void iAP_Auth_vfnReadProtVerAndDevID(uint8_t *pu8DataBuffer);

void iAP_Auth_vfnReadAccessoryCertificate(uint8_t *pu8RegisterData, uint16_t *wpCertificateLength);

void iAP_Auth_vfnAccessoryChallenge(uint8_t *pu8ChallengeData, uint8_t *puint8_tSignatureData, 
                                    uint16_t wChallangeLenght, uint16_t *wpSignatureLength);

#if (_TRUE_ == IAP_IOS_AUTHENTICATION_ENABLE)
void iAP_Auth_vfnAppleCertificateVal(uint8_t *bpAppleCertificateData, 
                                     uint8_t *bpAppleCertificateResult, 
                                     uint16_t wAppleCertificateLenght);

void iAP_Auth_vfnAppleChallengeGen(uint8_t *bpAppleChallangeData, 
                                                   uint16_t *wpAppleChallangeLength);


void iAP_Auth_vfnAppleSignatureVal(uint8_t *bpAppleChallangeData, uint8_t *bpAppleSignature, 
                                   uint16_t wAppleChallengeLength, uint16_t wAppleSignatureLength,
                                           uint8_t *bAppleSignaturResult);
#endif

/*************************************************************************************************/

#endif /* IAP_AUTHENTICATION_H_ */
