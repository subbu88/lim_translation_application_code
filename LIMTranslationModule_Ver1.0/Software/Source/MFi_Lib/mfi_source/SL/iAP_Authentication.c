/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP_Authentication.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/

#define SOFT_ADDRESS_SIZE				(0x01u)
#define CERTIFICATE_LENGTH_SIZE			(0x02u)
#define CHALLENGE_LENGTH_SIZE			(0x02u)
#define SIGNATURE_lENGTH_SIZE			(0x02u)
#define APPLE_CERTIFICATE_LENGTH_SIZE	(0x02u)
#define CP_CTRL_STATUS_REG_SIZE			(0x01u)
#define CP_CTRL_STATUS_ERROR_MASK		(0x80u)
#define CP_SC_RESULT_MASK               (0x70u)
#define CP_SC_RESULT_SHIFT				(0x04u)

#define IAP_AUTH_WRITE_LENGTH_SIZE		(0x03u)
#define IAP_AUTH_PAGE_SIZE				(0x80u)

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

enum AUTH_CP_CTRL_VALUES
{
	AUTH_CP_NO_OP1 = 0,
	AUTH_CP_START_SIGNATURE_GEN,
	AUTH_CP_START_CHALLENGE_GEN,
	AUTH_CP_START_SIGNATURE_VAL,
	AUTH_CP_START_CERTIFICATE_VAL,
	AUTH_CP_NO_OP2
};

enum AUTH_CP_RESULT_VALUES
{
	AUTH_CP_NO_VALID_RESULT = 0,			/* 0x00 */
	AUTH_CP_SIGNATURE_GEN_OK,				/* 0x01 */
	AUTH_CP_CHALLENGE_GEN_OK,				/* 0x02 */
	AUTH_CP_APPLE_SIGNATURE_OK,				/* 0x03 */
	AUTH_CP_APPLE_CERTIFICATE_OK			/* 0x04 */
};

typedef struct
{
	uint8_t 	*bpWriteDataBuffer;
	uint8_t  *bpReadDataBuffer;
	uint16_t wWRSize;
	uint16_t wRDSize;
	uint8_t 	bCPSoftAddress;
}IAP_AUTH_COMM;

typedef struct
{
	uint8_t 	*bpCertificateData;
	uint8_t 	*bpSignatureData;
	uint8_t 	*bpChallengeData;
	uint8_t 	*bpAppleResult;
	uint16_t *wpLengthReturn;
	uint16_t wAppleChallengeLength;
	uint8_t  bWriteCPCtrlStatusReg;
	uint8_t 	bReadCPCtrlStatusReg;
}IAP_AUTH_MAIN;

typedef enum
{
	IAP_AUTH_READ_CERTIFICATE_DATA_STATE = 0,
	IAP_AUTH_WRITE_CHALLENGE_DATA_STATE,
	IAP_AUTH_WRITE_START_SIGNATURE_GEN_STATE,
	IAP_AUTH_READ_SIGNATURE_GEN_STATUS_STATE,
	IAP_AUTH_READ_SIGNATURE_LENGTH_STATE,
	IAP_AUTH_READ_SIGNATURE_DATA_STATE,
#if (IAP_IOS_AUTHENTICATION_ENABLE)
	IAP_AUTH_WRITE_APPLE_CERTIFICATE_DATA_STATE,
	IAP_AUTH_WRITE_START_APPLE_CERT_VAL_STATE,
	IAP_AUTH_READ_APPLE_CERT_VAL_STATUS_STATE,
	IAP_AUTH_END_APPLE_CERTIFICATE_VAL_STATE,
	IAP_AUTH_READ_APPLE_CHALLENGE_GEN_STATUS_STATE,
	IAP_AUTH_READ_APPLE_CHALLENGE_LENGTH_STATE,
	IAP_AUTH_READ_APPLE_CHALLENGE_DATA_STATE,
	IAP_AUTH_WRITE_APPLE_SIGNATURE_DATA_STATE,
	IAP_AUTH_WRITE_APPLE_CHALLENGE_LENGTH_STATE,
	IAP_AUTH_WRITE_APPLE_CHALLENGE_DATA_STATE,
	IAP_AUTH_WRITE_START_APPLE_SIGNATURE_VAL_STATE,
	IAP_AUTH_READ_APPLE_SIGNATURE_VAL_STATUS_STATE,
	IAP_AUTH_END_APPLE_SIGNATURE_VALIDATION_STATE,
#endif
	IAP_AUTH_READ_CP_ERROR_CODE_REG_STATE,
	IAP_AUTH_WAIT_HAL_STATE,
	IAP_AUTH_DELAY_STATE,
	IAP_AUTH_RETRY_STATE,
	IAP_AUTH_IDLE_STATE,
	IAP_AUTH_COMM_RETRY_WRITE_STATE,
	IAP_AUTH_COMM_RETRY_READ_STATE
}IAP_AUTH_STATES;

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void iAP_Auth_vfnReadCertificateData_State(void);
static void iAP_Auth_vfnWriteChallengeData_State(void);
static void iAP_Auth_vfnWriteStartSignatureGen_State(void);
static void iAP_Auth_vfnReadSignatureGenStatus_State(void);
static void iAP_Auth_vfnReadSignatureLength_State(void);
static void iAP_Auth_vfnReadSignatureData_State(void);
#if (IAP_IOS_AUTHENTICATION_ENABLE)
static void iAP_Auth_vfnWriteAppleCertificateData_State(void);
static void iAP_Auth_vfnWriteStartAppleCertiVal_State(void);
static void iAP_Auth_vfnReadAppleCertificateValStatus_State(void);
static void iAP_Auth_vfnEndAppleCertificateVal_State(void);
static void iAP_Auth_vfnReadAppleChallengeGenStatus_State(void);
static void iAP_Auth_vfnReadAppleChallengeLength_State(void);
static void iAP_Auth_vfnReadAppleChallengeData_State(void);
static void iAP_Auth_vfnWriteAppleSignatureData_State(void);
static void iAP_Auth_vfnWriteAppleChallengeLength_State(void);
static void iAP_Auth_vfnWriteAppleChallengeData_State(void);
static void iAP_Auth_vfnWriteStartAppleSignatureVal_State(void);
static void iAP_Auth_vfnReadAppleSignatureValStatus_State(void);
static void iAP_Auth_vfnEndAppleSignatureValidation_State(void);
#endif
static void iAP_Auth_vfnReadCPErrorCodeRegister_State(void);
static void iAP_Auth_vfnWaitHAL_State(void);
static void iAP_Auth_vfnDelay_State(void);
static void iAP_Auth_vfnRetry_State(void);
static void iAP_Auth_vfnIdle_State(void);
static void iAP_Auth_vfnRetryWrite_State(void);
static void iAP_Auth_vfnRetryRead_State(void);


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
static void (*const iAP_Auth_vfnMainStateMachine[])(void) = 
{
	iAP_Auth_vfnReadCertificateData_State,
	iAP_Auth_vfnWriteChallengeData_State,
	iAP_Auth_vfnWriteStartSignatureGen_State,
	iAP_Auth_vfnReadSignatureGenStatus_State,
	iAP_Auth_vfnReadSignatureLength_State,
	iAP_Auth_vfnReadSignatureData_State,
#if (IAP_IOS_AUTHENTICATION_ENABLE)
	iAP_Auth_vfnWriteAppleCertificateData_State,
	iAP_Auth_vfnWriteStartAppleCertiVal_State,
	iAP_Auth_vfnReadAppleCertificateValStatus_State,
	iAP_Auth_vfnEndAppleCertificateVal_State,
	iAP_Auth_vfnReadAppleChallengeGenStatus_State,
	iAP_Auth_vfnReadAppleChallengeLength_State,
	iAP_Auth_vfnReadAppleChallengeData_State,
	iAP_Auth_vfnWriteAppleSignatureData_State,
	iAP_Auth_vfnWriteAppleChallengeLength_State,
	iAP_Auth_vfnWriteAppleChallengeData_State,
	iAP_Auth_vfnWriteStartAppleSignatureVal_State,
	iAP_Auth_vfnReadAppleSignatureValStatus_State,
	iAP_Auth_vfnEndAppleSignatureValidation_State,
#endif
	iAP_Auth_vfnReadCPErrorCodeRegister_State,
	iAP_Auth_vfnWaitHAL_State,
	iAP_Auth_vfnDelay_State,
	iAP_Auth_vfnRetry_State,
	iAP_Auth_vfnIdle_State,
	iAP_Auth_vfnRetryWrite_State,
	iAP_Auth_vfnRetryRead_State
};

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
uint32_t iAP_Auth_gu32Status;
uint16_t iAP_Auth_gwCertificateSize;
uint8_t iAP_Auth_gbaRxBufferForCP[128];			/**< Buffer used to read and write data from-to CP */
uint8_t iAP_Auth_gbaCP_VersionAndID[6];			/**< Buffer to read current CP version and Device ID*/
uint8_t iAP_Auth_CPCertificate[1284];

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static sSM iAP_Auth_sMainSM;
static IAP_AUTH_COMM iAP_Auth_sCommData;
static IAP_AUTH_MAIN iAP_Auth_sMainData;
static uint8_t iAP_Auth_baWriteBuffer[IAP_AUTH_WRITE_LENGTH_SIZE];
static uint8_t iAP_Auth_bRetryCounter;

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

void iAP_Auth_vfnInit(void)
{
	/* Set Auth main state machine to initial state */
	iAP_Auth_sMainSM.ActualState = IAP_AUTH_IDLE_STATE;
	iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
	/* Clear Authentication Status Flags */
	iAP_Auth_gu32Status = 0;
	/* Enable Timer channel */
	IAP_TIMER_ENABLE_CHN(IAP_AUTH_WAIT_CP_TIMEOUT_CHN);
	/* Enables the I2C module */
	(void)iAPI2C1_Enable(iAPI2C1_DeviceData);
}

void iAP_Auth_vfnCancelCommand(void)
{
	iAP_Auth_gu32Status = 0;
	iAP_Auth_sMainSM.ActualState = IAP_AUTH_IDLE_STATE;
}

void iAP_Auth_vfnReadProtVerAndDevID(uint8_t *pu8DataBuffer)
{
	if(!(iAP_Auth_gu32Status & (1 << AUTH_BUSY)))
	{
		iAP_Auth_vfnReadRegister(AUTHENTICATION_PROTOCOL_MAJOR_VERSION,(uint8_t*)pu8DataBuffer,6);
		iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
	}
}

void iAP_Auth_vfnReadAccessoryCertificate(uint8_t *pu8RegisterData, uint16_t *wpCertificateLength)
{
	if(!(iAP_Auth_gu32Status & (1 << AUTH_BUSY)))
	{
		/* Save the location where certificate must be stored */
		iAP_Auth_sMainData.bpCertificateData = pu8RegisterData;
		/* Save location to where the size of the certificate must be stored for the host */
		iAP_Auth_sMainData.wpLengthReturn = wpCertificateLength;		
		/* Start by reading the length certificate */
		iAP_Auth_vfnReadRegister(ACCESSORY_CERTIFICATE_DATA_LENGTH, \
				(uint8_t *)&iAP_Auth_gwCertificateSize,(uint16_t)CERTIFICATE_LENGTH_SIZE);
		
		/* Initialize the main state machine to read certificate and length certificate */
		iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_CERTIFICATE_DATA_STATE;
	}
}

void iAP_Auth_vfnAccessoryChallenge(uint8_t *pu8ChallengeData, uint8_t *pu8SignatureData, uint16_t wChallangeLenght, uint16_t *wpSignatureLength)
{	
	if(!(iAP_Auth_gu32Status & (1 << AUTH_BUSY)))
	{
//		uint16_t *wpLengthBufferPointer = (uint16_t *)&iAP_Auth_baWriteBuffer[1];//@todo fix-kl DONE
	
		/* Save the location where signature must be stored */
		iAP_Auth_sMainData.bpSignatureData = pu8SignatureData;
		/* Save location to where the size of the Signature Data must be stored for the host */
		iAP_Auth_sMainData.wpLengthReturn = wpSignatureLength;
		/* Save the location where challenge data is stored */
		iAP_Auth_sMainData.bpChallengeData = pu8ChallengeData;
		/* Save the Challenge length in the length global variable */
		iAP_Auth_gwCertificateSize = wChallangeLenght;
    		//@todo This change was made to support KL, but won't work with big endian devices
		/* Save Challenge length into a static buffer with the MSB first */
        	iAP_Auth_baWriteBuffer[1] = (uint8_t)((wChallangeLenght-1) >> 0x08);
        	iAP_Auth_baWriteBuffer[2] = (uint8_t)((wChallangeLenght-1));
		
		/* Start by writing the challenge data length in the CP */
		iAP_Auth_vfnWriteRegister(CHALLENGE_DATA_LENGTH,(uint8_t *)&iAP_Auth_baWriteBuffer[0], \
				(uint16_t)(CHALLENGE_LENGTH_SIZE+1));
		/* Initialize the main state machine to write Data challenge */
		iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_CHALLENGE_DATA_STATE;
	}
}

#if (IAP_IOS_AUTHENTICATION_ENABLE)
void iAP_Auth_vfnAppleCertificateVal(uint8_t *bpAppleCertificateData, uint8_t *bpAppleCertificateResult, \
		uint16_t wAppleCertificateLenght)
{	
	if(!(iAP_Auth_gu32Status & (1 << AUTH_BUSY)))
	{
		uint16_t *wpLengthBufferPointer = (uint16_t *)&iAP_Auth_baWriteBuffer[1];
	
		/* Save the location where result must be stored */
		iAP_Auth_sMainData.bpAppleResult = bpAppleCertificateResult;
		/* Save the location where Apple Certificate data is stored */
		iAP_Auth_sMainData.bpCertificateData = bpAppleCertificateData;
		/* Save the Certificate length in the length global variable */
		iAP_Auth_gwCertificateSize = wAppleCertificateLenght;
	#if(FALSE == BIG_ENDIAN_CORE)	
		/* Convert Certificate length to big-endian and save it in a static buffer */
		BYTESWAP16(wAppleCertificateLenght,*wpLengthBufferPointer);
	#else
		*wpLengthBufferPointer = (wAppleCertificateLenght);
	#endif
		/* Starts by writing the challenge data length in the CP */
		iAP_Auth_vfnWriteRegister(APPLE_DEVICE_CERTIFICATE_DATA_LENGTH, \
				(uint8_t *)&iAP_Auth_baWriteBuffer[0], (uint16_t)(APPLE_CERTIFICATE_LENGTH_SIZE+1));
		/* Initialize the main state machine to write Apple Certificate Data  */
		iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_APPLE_CERTIFICATE_DATA_STATE;
	}
}

void iAP_Auth_vfnAppleChallengeGen(uint8_t *bpAppleChallangeData, uint16_t *wpAppleChallangeLength)
{
	if(!(iAP_Auth_gu32Status & (1 << AUTH_BUSY)))
	{
		uint8_t *wpWriteRegisterBufferPointer = &iAP_Auth_baWriteBuffer[1];
	
		/* Stores the address where the apple challenge must be stored */
		iAP_Auth_sMainData.bpChallengeData = bpAppleChallangeData;
		/* Stores the address where the apple challenge length must be stored */
		iAP_Auth_sMainData.wpLengthReturn = wpAppleChallangeLength;
		/* Load the start apple challenge generation value in the byte to write buff */
		*wpWriteRegisterBufferPointer = AUTH_CP_START_CHALLENGE_GEN;
		/* Write the start of apple Challenge Generation value */
		iAP_Auth_vfnWriteRegister(AUTHENTICATION_CONTROL_AND_STATUS, \
				(uint8_t*)&iAP_Auth_baWriteBuffer[0], CP_CTRL_STATUS_REG_SIZE+1);
		/* Set the main SM to execute the Read Apple Challenge Generation Status State */
		iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_APPLE_CHALLENGE_GEN_STATUS_STATE;
	}
}

void iAP_Auth_vfnAppleSignatureVal(uint8_t *bpAppleChallangeData, uint8_t *bpAppleSignatureData, \
		uint16_t wAppleChallengeLength, uint16_t wAppleSignatureLength, uint8_t *bAppleSignaturResult)
{
	if(!(iAP_Auth_gu32Status & (1 << AUTH_BUSY)))
	{
		/* Temporal pointer to store the length data in the buffer to transmit */
		uint16_t *wpLengthBufferPointer = (uint16_t *)&iAP_Auth_baWriteBuffer[1];
	
		/* Save location to where the Apple Challenge is stored */
		iAP_Auth_sMainData.bpChallengeData = bpAppleChallangeData;
		/* Save location to where the Apple Signature is stored */
		iAP_Auth_sMainData.bpSignatureData = bpAppleSignatureData;
		/* Save location to where the result will be stored for upper layer */
		iAP_Auth_sMainData.bpAppleResult = bAppleSignaturResult;
		/* Save the Signature data length */
		iAP_Auth_gwCertificateSize = wAppleSignatureLength;
		/* Save the Challenge data length */
		iAP_Auth_sMainData.wAppleChallengeLength = wAppleChallengeLength;
	#if(FALSE == BIG_ENDIAN_CORE)	
		/* Convert Signature length to big-endian and save it in the buffer to transmit */
		BYTESWAP16(wAppleSignatureLength,*wpLengthBufferPointer);
	#else
		*wpLengthBufferPointer = (wAppleSignatureLength);
	#endif
		/* Start by writing the Signature length */
		iAP_Auth_vfnWriteRegister(SIGNATURE_DATA_LENGTH ,(uint8_t *)&iAP_Auth_baWriteBuffer[0], \
					(uint16_t)(SIGNATURE_lENGTH_SIZE+1));
		/* Initialize the main state machine to write the apple signature data */
		iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_APPLE_SIGNATURE_DATA_STATE;
	}
}
#endif

void iAP_Auth_vfnReadRegister(uint8_t u8Register, uint8_t *pu8DataBuffer, uint16_t u16Datasize)
{	
	LDD_I2C_TBusState Status;
	
	/* Store the Reading information into a struct for the State machine to use it */
	iAP_Auth_sCommData.bCPSoftAddress = u8Register;
	iAP_Auth_sCommData.bpReadDataBuffer = pu8DataBuffer;
	iAP_Auth_sCommData.wRDSize = u16Datasize;
	
	/** @todo Function Return */
	(void)IIC_CHECK_BUS(Status);
	
	if((!IAP_AUTH_COMM_BUSY) && (LDD_I2C_IDLE == Status))
	{	
		/* Select IIC slave address */
		/** @todo Function Return */
		(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,IAP_AUTH_SLAVE_ADDRESS);
		/* Sets the Authentication busy and Authentication Read Flags */
		iAP_Auth_gu32Status |= (1 << AUTH_BUSY) | (1 << AUTH_WRITE) | (1 << AUTH_READ);
		iAP_Auth_bRetryCounter = 0;

		IAP_AUTH_SET_COMM_BUSY;
		IIC_SET_RESERVE;
		IIC_CLEAR_RELEASE_ON_COMP;


		/* Call the PE Write Block HAL IIC function */
		//	iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, (uint8_t *)&iAP_Auth_sCommData.bCPSoftAddress, SOFT_ADDRESS_SIZE, LDD_I2C_NO_SEND_STOP);
		/** @todo Function Return */
		(void)iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, (uint8_t *)&iAP_Auth_sCommData.bCPSoftAddress, SOFT_ADDRESS_SIZE, LDD_I2C_SEND_STOP);

		/* Set Auth Communication state machine to write state */
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_WAIT_HAL_STATE;
		iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
	}
	else
	{
		iAP_Auth_sMainSM.PrevState = iAP_Auth_sMainSM.ActualState;
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_COMM_RETRY_READ_STATE;
	}
}

void iAP_Auth_vfnWriteRegister(uint8_t u8Register, uint8_t *pu8RegisterData, uint16_t u16Datasize)
{	
	LDD_I2C_TBusState Status;
	
	/* Store the bytes to write pointer within the comm struct */
	iAP_Auth_sCommData.bCPSoftAddress = u8Register;
	iAP_Auth_sCommData.bpWriteDataBuffer = pu8RegisterData;
	iAP_Auth_sCommData.wWRSize = u16Datasize;
	
	/** @todo Function Return */
	(void)IIC_CHECK_BUS(Status);
	
	if((!IAP_AUTH_COMM_BUSY) && (LDD_I2C_IDLE == Status))
	{
		/* Select IIC slave address */
		/** @todo Function Return */
		(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,IAP_AUTH_SLAVE_ADDRESS);
		/* Sets the Authentication busy and Authentication Read Flags */
		iAP_Auth_gu32Status |= (1 << AUTH_BUSY) | (1 << AUTH_WRITE);
		iAP_Auth_bRetryCounter = 0;

		IAP_AUTH_SET_COMM_BUSY;
		IIC_SET_RESERVE;
		IIC_SET_RELEASE_ON_COMP;

		/* Store the Register Address in the Write Data Buffer */
		*pu8RegisterData = u8Register;

		/* Call the PE Write Block HAL IIC function */
		/** @todo Function Return */
		(void)iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, iAP_Auth_sCommData.bpWriteDataBuffer, \
				iAP_Auth_sCommData.wWRSize, LDD_I2C_SEND_STOP);

		/* Set the Write state to be executed next */
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_WAIT_HAL_STATE;
		iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
	}
	else
	{
		iAP_Auth_sMainSM.PrevState = iAP_Auth_sMainSM.ActualState;
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_COMM_RETRY_WRITE_STATE;
	}
}

static void iAP_Auth_vfnReadCertificateData_State(void)
{
	/* The state machine has finished reading certificate length */

	uint8_t *bpLengthReturnTmp = (uint8_t*)iAP_Auth_sMainData.wpLengthReturn;//@todo fix-kl DONE

#if(FALSE == BIG_ENDIAN_CORE)		
	/* Converts bytes received from big-endian to little-endian */
	BYTESWAP16(iAP_Auth_gwCertificateSize,iAP_Auth_gwCertificateSize);
#endif		
	/* Save certificate length for host layer */
//        *iAP_Auth_sMainData.wpLengthReturn = iAP_Auth_gwCertificateSize;
        /* @todo if porting to an architecture with different endianness
        check following byte order: */
        bpLengthReturnTmp[1] = (uint8_t)(iAP_Auth_gwCertificateSize >> 0x08);
        bpLengthReturnTmp[0] = (uint8_t)(iAP_Auth_gwCertificateSize);
	/* Reads the certificate data */
	iAP_Auth_vfnReadRegister(ACCESSORY_CERTIFICATE_DATA_P1, \
			iAP_Auth_sMainData.bpCertificateData, iAP_Auth_gwCertificateSize);
	/* Sets the Read certificate data as the following state */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
}

static void iAP_Auth_vfnWriteChallengeData_State(void)
{
	/* The state machine has finished writing the challenge length */
	
	/* Write challenge data into the CP */
	iAP_Auth_vfnWriteRegister(CHALLENGE_DATA, iAP_Auth_sMainData.bpChallengeData, \
			iAP_Auth_gwCertificateSize);
	/* Set the Start signature generation state to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_START_SIGNATURE_GEN_STATE;
}

static void iAP_Auth_vfnWriteStartSignatureGen_State(void)
{
	/* The state machine has finished writing the challenge data */
	
	uint8_t *wpWriteRegisterBufferPointer = &iAP_Auth_baWriteBuffer[1];

	/* Load the start signature generation value in the byte to write */
	*wpWriteRegisterBufferPointer = AUTH_CP_START_SIGNATURE_GEN;
	/* Write the start of signature generation value */
	iAP_Auth_vfnWriteRegister(AUTHENTICATION_CONTROL_AND_STATUS, \
			(uint8_t*)&iAP_Auth_baWriteBuffer[0], CP_CTRL_STATUS_REG_SIZE+1);
	/* Set the Read Signature Generation Status State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_SIGNATURE_GEN_STATUS_STATE;
}

static void iAP_Auth_vfnReadSignatureGenStatus_State(void)
{
	/* The state machine has finished writing the start of signature generation cmd */
	
	/* Read the Status and control register */
	iAP_Auth_vfnReadRegister(AUTHENTICATION_CONTROL_AND_STATUS, \
			(uint8_t*)&iAP_Auth_sMainData.bReadCPCtrlStatusReg, CP_CTRL_STATUS_REG_SIZE);
	/* Set the Read Signature Length State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_SIGNATURE_LENGTH_STATE;
}

static void iAP_Auth_vfnReadSignatureLength_State(void)
{
	/* Temporal variable to store the result from the CP executed cmd */
	uint8_t bProcResult;
	
	/* If Result is available */
	if(iAP_Auth_sMainData.bReadCPCtrlStatusReg)
	{
		/* If there was NO error during the signature generation */
		if (!(CP_CTRL_STATUS_ERROR_MASK == iAP_Auth_sMainData.bReadCPCtrlStatusReg))
		{
			/* Keep only the result fields */
			bProcResult = (uint8_t)(iAP_Auth_sMainData.bReadCPCtrlStatusReg & CP_SC_RESULT_MASK);
			/* Shift the data to the less significant nibble */
			bProcResult = (uint8_t)(bProcResult >> CP_SC_RESULT_SHIFT);
			/* If the signature successfully generated flag is set */
			if (AUTH_CP_SIGNATURE_GEN_OK == bProcResult)
			{
				/* Read the signature length */
				iAP_Auth_vfnReadRegister(SIGNATURE_DATA_LENGTH,(uint8_t *)&iAP_Auth_gwCertificateSize, \
						SIGNATURE_lENGTH_SIZE);
				/* Set the Read Signature State to be executed next */
				iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_SIGNATURE_DATA_STATE;
			}
			else /* If any other result is reported */
			{
				/* Set the signature generation error */	
				iAP_Auth_gu32Status |= (uint32_t)(1 << AUTH_SIGNATURE_ERROR);
				/* End the state machine by setting the main idle state */
				iAP_Auth_sMainSM.ActualState = IAP_AUTH_IDLE_STATE;
			}
		}
		else /* If there was an error in the signature generation */
		{
			/* Set the signature generation error */	
			iAP_Auth_gu32Status |= (uint32_t)(1 << AUTH_SIGNATURE_ERROR);
			/* End the state machine by setting the main idle state */
			iAP_Auth_sMainSM.ActualState = IAP_AUTH_IDLE_STATE;
		}
	}
	else /* if CP has not generated the signature */
	{
		/* Set the Read Signature Generation Status State to be executed again */
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_READ_SIGNATURE_GEN_STATUS_STATE;
	}
}

static void iAP_Auth_vfnReadSignatureData_State(void)
{
	/* The state machine has finished reading the signature length */
//	uint16_t* wpTempLength = iAP_Auth_sMainData.wpLengthReturn;
	uint8_t* bpTempLength = (uint8_t*)iAP_Auth_sMainData.wpLengthReturn;
	
	#if(FALSE == BIG_ENDIAN_CORE)		
		/* Convert Signature length to little-endian */
		BYTESWAP16(iAP_Auth_gwCertificateSize,iAP_Auth_gwCertificateSize);
	#endif		
		
	/* Save certificate length for host layer */
//	*wpTempLength = (uint16_t)iAP_Auth_gwCertificateSize;
        /* @todo if porting to an architecture with different endianness
        check following byte order: */
		bpTempLength[1] = (uint8_t)(iAP_Auth_gwCertificateSize >> 0x08);
		bpTempLength[0] = (uint8_t)(iAP_Auth_gwCertificateSize);
	/* Read the signature data */
	iAP_Auth_vfnReadRegister(SIGNATURE_DATA, \
			iAP_Auth_sMainData.bpSignatureData, iAP_Auth_gwCertificateSize);
	/* Set the End Accessory Challenge State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
}

#if (IAP_IOS_AUTHENTICATION_ENABLE)
static void iAP_Auth_vfnWriteAppleCertificateData_State(void)
{
	/* The state machine has finished writing the apple certificate length */
	
	static uint8_t bCertificatePagesNumber;
	static uint16_t wCertificateDataOffset;
	
	/* If there are still multiple sections to write */
	if(IAP_AUTH_PAGE_SIZE < iAP_Auth_gwCertificateSize )
	{
		/* Write apple certificate data into the CP */
		iAP_Auth_vfnWriteRegister(APPLE_DEVICE_CERTIFICATE_DATA_P1 + bCertificatePagesNumber, \
				iAP_Auth_sMainData.bpCertificateData + wCertificateDataOffset, \
				IAP_AUTH_PAGE_SIZE + 1);
		/* Subtract the apple certificate data bytes already written */
		iAP_Auth_gwCertificateSize -= IAP_AUTH_PAGE_SIZE;
		/* Move the offset one page size to write on the next Register */
		wCertificateDataOffset += IAP_AUTH_PAGE_SIZE; 
		/* Increment the page number */
		bCertificatePagesNumber++;
		iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_APPLE_CERTIFICATE_DATA_STATE;
	}	
	/* if the last section to write */
	else if (iAP_Auth_gwCertificateSize)
	{
		/* Write the last apple certificate section into the CP */
		iAP_Auth_vfnWriteRegister(APPLE_DEVICE_CERTIFICATE_DATA_P1 + bCertificatePagesNumber, \
				iAP_Auth_sMainData.bpCertificateData + wCertificateDataOffset, \
				iAP_Auth_gwCertificateSize + 1);
		/* Set the Start signature apple certificate validation state to be executed next */
		iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_START_APPLE_CERT_VAL_STATE;
		
		/* Clear static variables for next apple authentication */
		wCertificateDataOffset = (uint16_t)FALSE; 
		bCertificatePagesNumber = FALSE;
	}
	else
	{
		/* Do nothing */
	}
}

static void iAP_Auth_vfnWriteStartAppleCertiVal_State(void)
{
	/* The state machine has finished writing the challenge data */
	
	uint8_t *wpWriteRegisterBufferPointer = &iAP_Auth_baWriteBuffer[1];
	
	/* Load the start signature generation value in the byte to write */
	*wpWriteRegisterBufferPointer = AUTH_CP_START_CERTIFICATE_VAL;
	/* Write the start of apple certificate validation value */
	iAP_Auth_vfnWriteRegister(AUTHENTICATION_CONTROL_AND_STATUS, \
			(uint8_t*)&iAP_Auth_baWriteBuffer[0], CP_CTRL_STATUS_REG_SIZE+1);
	/* Set the Read Apple Certificate Validation Status State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_APPLE_CERT_VAL_STATUS_STATE;
}

static void iAP_Auth_vfnReadAppleCertificateValStatus_State(void)
{
	/* The state machine has finished writing the start apple cert validation cmd */
	
	/* Read the Status and control register */
	iAP_Auth_vfnReadRegister(AUTHENTICATION_CONTROL_AND_STATUS, \
			(uint8_t *)&iAP_Auth_sMainData.bReadCPCtrlStatusReg, CP_CTRL_STATUS_REG_SIZE);
	/* Set the Read Signature Length State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_END_APPLE_CERTIFICATE_VAL_STATE;
}

static void iAP_Auth_vfnEndAppleCertificateVal_State(void)
{
	/* Temporal variable to store the result from the CP executed cmd */
	uint8_t bProcResult;
	
	/* The state machine has finished reading the CP Ctrl&Status Register */

	/* If Result is available */
	if(iAP_Auth_sMainData.bReadCPCtrlStatusReg)
	{
		/* If there was NO error during the signature generation */
		if (!(CP_CTRL_STATUS_ERROR_MASK == iAP_Auth_sMainData.bReadCPCtrlStatusReg))
		{
			/* Keep only the result fields */
			bProcResult = (uint8_t)(iAP_Auth_sMainData.bReadCPCtrlStatusReg & CP_SC_RESULT_MASK);
			/* Shift the data to the less significant nibble */
			bProcResult = (uint8_t)(bProcResult >> CP_SC_RESULT_SHIFT);				
			/* End the SM by setting the main idle state to be executed next */
			iAP_Auth_sMainSM.ActualState = IAP_AUTH_IDLE_STATE;			
			/* If the signature successfully generated flag is set */
			if (AUTH_CP_APPLE_CERTIFICATE_OK == bProcResult)
			{
				/* Stores the certificate validation result */
				*iAP_Auth_sMainData.bpAppleResult = IAP_AUTH_APPLE_CERT_RESULT_OK;
			}
			else /* If any other result is reported */
			{
				/* Set the signature generation error */	
				iAP_Auth_gu32Status |= (uint32_t)(1 << AUTH_SIGNATURE_ERROR);
				/* Stores the certificate validation result 0xFF == Error */
				*iAP_Auth_sMainData.bpAppleResult = IAP_AUTH_APPLE_CERT_RESULT_ERR;
			}
		}
		else /* If there was an error in the signature generation */
		{
			/* Set the signature generation error */	
			iAP_Auth_gu32Status |= (uint32_t)(1 << AUTH_SIGNATURE_ERROR);
			
			/* Read the error code register */
			iAP_Auth_vfnReadRegister(ERROR_CODE, (uint8_t *)&iAP_Auth_sMainData.bReadCPCtrlStatusReg, \
					CP_CTRL_STATUS_REG_SIZE);
			/* Go to error state */
			iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_CP_ERROR_CODE_REG_STATE;
		}
	}
	else /* if CP has not generated the result */
	{
		/* Set the Read apple Cert validation Status State to be executed again */
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_READ_APPLE_CERT_VAL_STATUS_STATE;
	}
}

static void iAP_Auth_vfnReadAppleChallengeGenStatus_State(void)
{
	/* The state machine has finished writing the start of apple challenge gen cmd */

	/* Read the Status and control register */
	iAP_Auth_vfnReadRegister(AUTHENTICATION_CONTROL_AND_STATUS, \
			(uint8_t*)&iAP_Auth_sMainData.bReadCPCtrlStatusReg, CP_CTRL_STATUS_REG_SIZE);
	/* Set the Read Apple Challenge Length State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_APPLE_CHALLENGE_LENGTH_STATE;
}

static void iAP_Auth_vfnReadAppleChallengeLength_State(void)
{
	/* Temporal variable to store the result from the CP executed cmd */
	uint8_t bProcResult;
	
	/* The state machine has finished reading the CP Ctrl&Status Register */
	
	/* If Result is available */
	if(iAP_Auth_sMainData.bReadCPCtrlStatusReg)
	{
		/* If there was NO error during the apple challenge generation */
		if (!(CP_CTRL_STATUS_ERROR_MASK == iAP_Auth_sMainData.bReadCPCtrlStatusReg))
		{
			/* Keep only the result fields */
			bProcResult = (uint8_t)(iAP_Auth_sMainData.bReadCPCtrlStatusReg & CP_SC_RESULT_MASK);
			/* Shift the data to the less significant nibble */
			bProcResult = (uint8_t)(bProcResult >> CP_SC_RESULT_SHIFT);
			/* If the apple challenge successfully generated flag is set */
			if (AUTH_CP_CHALLENGE_GEN_OK == bProcResult)
			{
				/* Read the signature length */
				iAP_Auth_vfnReadRegister(CHALLENGE_DATA_LENGTH,(uint8_t *)&iAP_Auth_gwCertificateSize, \
						SIGNATURE_lENGTH_SIZE);
				/* Set the Read Apple Challenge State to be executed next */
				iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_APPLE_CHALLENGE_DATA_STATE;	
			}
			else /* If any other result is reported */
			{
				/* Set the signature generation error */	
				iAP_Auth_gu32Status |= (uint32_t)(1 << AUTH_SIGNATURE_ERROR);
				/* End the state machine by setting the main idle state */
				iAP_Auth_sMainSM.ActualState = IAP_AUTH_IDLE_STATE;
			}
		}
		else /* If there was an error in the signature generation */
		{
			/* Read the error code register */
			iAP_Auth_vfnReadRegister(ERROR_CODE, (uint8_t*)&iAP_Auth_sMainData.bReadCPCtrlStatusReg, \
					CP_CTRL_STATUS_REG_SIZE);
			/* Go to error state */
			iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_CP_ERROR_CODE_REG_STATE;
			/* Set the signature generation error */	
			iAP_Auth_gu32Status |= (uint32_t)(1 << AUTH_SIGNATURE_ERROR);
		}
	}
	else /* if CP has not generated the Apple Challenge */
	{
		/* Set the Read Apple Challenge Gen Status State to be executed again */
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_READ_APPLE_CHALLENGE_GEN_STATUS_STATE;
	}	
}

static void iAP_Auth_vfnReadAppleChallengeData_State(void)
{
	
	/* The state machine has finished reading the apple challenge length */
	
	#if(FALSE == BIG_ENDIAN_CORE)
		/* Convert Apple Challenge length to little-endian */
		BYTESWAP16(iAP_Auth_gwCertificateSize,iAP_Auth_gwCertificateSize);
	#endif		
	/* Stores the Apple Challenge length in the Address provided by host */
	*iAP_Auth_sMainData.wpLengthReturn = iAP_Auth_gwCertificateSize;
	/* Read the Apple Challenge data */
	iAP_Auth_vfnReadRegister(CHALLENGE_DATA, \
			iAP_Auth_sMainData.bpChallengeData, iAP_Auth_gwCertificateSize);
	/* Set the End Accessory Challenge State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
}

static void iAP_Auth_vfnWriteAppleSignatureData_State(void)
{
	/* The state machine has finished writing the apple signature length */

	/* Write apple Signature data into the CP */
	iAP_Auth_vfnWriteRegister(SIGNATURE_DATA, \
			iAP_Auth_sMainData.bpSignatureData, iAP_Auth_gwCertificateSize + 1);
	/* Set the Start signature apple certificate validation state to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_APPLE_CHALLENGE_LENGTH_STATE;
}

static void iAP_Auth_vfnWriteAppleChallengeLength_State(void)
{
	/* The state machine has finished writing the apple signature length */
	
	/* Temporal pointer to store the length value in the buffer to transmit */
	uint16_t *wpLengthBufferPointer = (uint16_t *)&iAP_Auth_baWriteBuffer[1];
	
	#if(FALSE == BIG_ENDIAN_CORE)		
		/* Convert Apple Challenge length to big-endian and save it in the buffer to transmit */
		BYTESWAP16(iAP_Auth_sMainData.wAppleChallengeLength,*wpLengthBufferPointer);
	#else
		*wpLengthBufferPointer = (iAP_Auth_sMainData.wAppleChallengeLength);
	#endif		
	/* Write apple Challenge data into the CP */
	iAP_Auth_vfnWriteRegister(CHALLENGE_DATA_LENGTH ,(uint8_t *)&iAP_Auth_baWriteBuffer[0], \
				(uint16_t)(CHALLENGE_LENGTH_SIZE+1));
	/* Set the Start signature apple certificate validation state to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_APPLE_CHALLENGE_DATA_STATE;
}

static void iAP_Auth_vfnWriteAppleChallengeData_State(void)
{
	/* The state machine has finished writing the apple Challenge length */

	/* Write apple Challenge data into the CP */
	iAP_Auth_vfnWriteRegister(CHALLENGE_DATA, iAP_Auth_sMainData.bpChallengeData, \
			iAP_Auth_sMainData.wAppleChallengeLength + 1);
	/* Set the Start signature apple certificate validation state to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_WRITE_START_APPLE_SIGNATURE_VAL_STATE;
}

static void iAP_Auth_vfnWriteStartAppleSignatureVal_State(void)
{
	/* The state machine has finished writing the apple challenge data */
	
	/* Temporal pointer of the buffer to transmit */
	uint8_t *wpWriteRegisterBufferPointer = &iAP_Auth_baWriteBuffer[1];
	
	/* Load the start apple signature validation value in the buffer to send */
	*wpWriteRegisterBufferPointer = AUTH_CP_START_SIGNATURE_VAL;
	/* Write the start of signature validation value */
	iAP_Auth_vfnWriteRegister(AUTHENTICATION_CONTROL_AND_STATUS, \
			(uint8_t*)&iAP_Auth_baWriteBuffer[0], CP_CTRL_STATUS_REG_SIZE+1);
	/* Set the Read Apple Signature Validation Status State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_APPLE_SIGNATURE_VAL_STATUS_STATE;
}

static void iAP_Auth_vfnReadAppleSignatureValStatus_State(void)
{
	/* The state machine has finished writing the start of signature validation cmd */

	/* Read the Status and control register */
	iAP_Auth_vfnReadRegister(AUTHENTICATION_CONTROL_AND_STATUS, \
			(uint8_t*)&iAP_Auth_sMainData.bReadCPCtrlStatusReg, CP_CTRL_STATUS_REG_SIZE);
	/* Set the End Apple Signature Validation State to be executed next */
	iAP_Auth_sMainSM.NextState = IAP_AUTH_END_APPLE_SIGNATURE_VALIDATION_STATE;
}

static void iAP_Auth_vfnEndAppleSignatureValidation_State(void)
{
	/* Temporal variable to store the result from the CP executed cmd */
	uint8_t bProcResult;
	
	/* The state machine has finished reading the CP Ctrl&Status Register */

	/* If Result is available */
	if(iAP_Auth_sMainData.bReadCPCtrlStatusReg)
	{
		/* If there was NO error during the signature generation */
		if (!(CP_CTRL_STATUS_ERROR_MASK == iAP_Auth_sMainData.bReadCPCtrlStatusReg))
		{
			/* Keep only the result fields */
			bProcResult = (uint8_t)(iAP_Auth_sMainData.bReadCPCtrlStatusReg & CP_SC_RESULT_MASK);
			/* Shift the data to the less significant nibble */
			bProcResult = (uint8_t)(bProcResult >> CP_SC_RESULT_SHIFT);				
			/* End the SM by setting the main idle state to be executed next */
			iAP_Auth_sMainSM.ActualState = IAP_AUTH_IDLE_STATE;			
			/* If the signature successfully generated flag is set */
			if (AUTH_CP_APPLE_SIGNATURE_OK == bProcResult)
			{
				/* Stores the certificate validation result */
				*iAP_Auth_sMainData.bpAppleResult = IAP_AUTH_APPLE_SIGN_RESULT_OK;
			}
			else /* If any other result is reported */
			{
				/* Set the signature generation error */	
				iAP_Auth_gu32Status |= (uint32_t)(1 << AUTH_SIGNATURE_ERROR);
				/* Stores the certificate validation result 0xFF == Error */
				*iAP_Auth_sMainData.bpAppleResult = IAP_AUTH_APPLE_SIGN_RESULT_ERR;
			}
		}
		else /* If there was an error in the signature generation */
		{
			/* Read the error code register */
			iAP_Auth_vfnReadRegister(ERROR_CODE, (uint8_t*)&iAP_Auth_sMainData.bReadCPCtrlStatusReg, \
					CP_CTRL_STATUS_REG_SIZE);
			/* Go to error state */
			iAP_Auth_sMainSM.NextState = IAP_AUTH_READ_CP_ERROR_CODE_REG_STATE;
			/* Set the signature generation error */	
			iAP_Auth_gu32Status |= (uint32_t)(1 << AUTH_SIGNATURE_ERROR);
		}
	}
	else /* if CP has not generated the result */
	{
		/* Set the Read apple Signature validation Status State to be executed again */
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_READ_APPLE_SIGNATURE_VAL_STATUS_STATE;
	}
}
#endif

static void iAP_Auth_vfnReadCPErrorCodeRegister_State(void)
{
	/* The state machine has finished reading the Error code register cmd */

	/* Stores the error code in the result variable */
	*iAP_Auth_sMainData.bpAppleResult = iAP_Auth_sMainData.bReadCPCtrlStatusReg;
	/* End the SM by setting the main idle state to be executed next */
	iAP_Auth_sMainSM.ActualState = IAP_AUTH_IDLE_STATE;
}

static void iAP_Auth_vfnWaitHAL_State(void)
{
	/* If data has been sent */
	if(!IIC_DRIVER_BUSY)
	{
		uint8_t bIICStatus;
		/** @todo Function Return */
		(void)IIC_CHECK_BUS(bIICStatus);
		if(bIICStatus)
		{
			/* If no error was detected */
			if(!IAP_AUTH_CHECK_COMM_ERROR)
			{
				/* If was a reading instruction */
				if (iAP_Auth_gu32Status & (1 << AUTH_READ))
				{
					if(iAP_Auth_gu32Status & (1 << AUTH_WRITE))
					{
						uint8_t bReceiveBlockError;
	
						/* Select IIC slave address */
						/** @todo Function Return */
						(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,IAP_AUTH_SLAVE_ADDRESS);
						/* Call the PE Receive Block HAL function */
						bReceiveBlockError = iAPI2C1_MasterReceiveBlock(iAPI2C1_DeviceData, iAP_Auth_sCommData.bpReadDataBuffer, \
								iAP_Auth_sCommData.wRDSize, LDD_I2C_SEND_STOP );
						
						if(ERR_OK == bReceiveBlockError)
						{
							iAP_Auth_gu32Status &= ~(1 << AUTH_WRITE);
							IAP_AUTH_SET_COMM_BUSY;
							IIC_SET_RELEASE_ON_COMP;
						}
					}
					else
					{
						IIC_CLEAR_RESERVE;
						IIC_CLEAR_RELEASE_ON_COMP;
						iAP_Auth_gu32Status &= ~(1 << AUTH_READ);
						IAP_TIMER_SET_PERIOD(IAP_AUTH_WAIT_CP_TIMEOUT_CHN,IAP_TIMEOUT_2MS_PERIOD);
						iAP_Auth_sMainSM.ActualState = IAP_AUTH_DELAY_STATE;				
					}
				}
				else /* If was a writing instruction */
				{
					IIC_CLEAR_RESERVE;
					iAP_Auth_gu32Status &= ~(1 << AUTH_WRITE);
					IAP_TIMER_SET_PERIOD(IAP_AUTH_WAIT_CP_TIMEOUT_CHN,IAP_TIMEOUT_2MS_PERIOD);
					iAP_Auth_sMainSM.ActualState = IAP_AUTH_DELAY_STATE;
				}	
			}
			else  /* If an error was detected */
			{	
				IAP_AUTH_CLEAR_COMM_ERROR;
				IIC_CLEAR_RESERVE;
				
				/* Set channel for waiting 1ms before retry */
				IAP_TIMER_SET_PERIOD(IAP_AUTH_WAIT_CP_TIMEOUT_CHN,IAP_TIMEOUT_2MS_PERIOD);
				iAP_Auth_sMainSM.ActualState = IAP_AUTH_RETRY_STATE;
			}
		}
	}
}

static void iAP_Auth_vfnDelay_State(void)
{
	if(IAP_TIMER_CHECK_CHN_FLAG(IAP_AUTH_WAIT_CP_TIMEOUT_CHN))
	{
		iAP_Auth_sMainSM.ActualState = iAP_Auth_sMainSM.NextState;
	}
}

static void iAP_Auth_vfnRetry_State(void)
{
	if(IAP_TIMER_CHECK_CHN_FLAG(IAP_AUTH_WAIT_CP_TIMEOUT_CHN))
	{
		if(!IAP_AUTH_COMM_BUSY)
		{
			uint8_t bIICStatus;
			/** @todo Function Return */
			(void)IIC_CHECK_BUS(bIICStatus);
			if(bIICStatus)
			{
				iAP_Auth_bRetryCounter++;
				if(200 < iAP_Auth_bRetryCounter)
				{
					/* maximum retries reached */
					iAP_Auth_bRetryCounter = 0;
				}
				else
				{
					/* Try again */
					iAP_Auth_sMainSM.ActualState = IAP_AUTH_WAIT_HAL_STATE;
					IAP_AUTH_SET_COMM_BUSY;
					IIC_SET_RESERVE;
					
					/* Select IIC slave address */
					/** @todo Function Return */
					(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,IAP_AUTH_SLAVE_ADDRESS);
					
					if(iAP_Auth_gu32Status &(1 << AUTH_READ))
					{
						if(iAP_Auth_gu32Status & (1 << AUTH_WRITE))
						{
							IIC_CLEAR_RELEASE_ON_COMP;
							/* Call the PE Write Block HAL IIC function */
	//						iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, (uint8_t *)&iAP_Auth_sCommData.bCPSoftAddress, SOFT_ADDRESS_SIZE, LDD_I2C_NO_SEND_STOP);
							/** @todo Function Return */
							(void)iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, (uint8_t *)&iAP_Auth_sCommData.bCPSoftAddress, SOFT_ADDRESS_SIZE, LDD_I2C_SEND_STOP);
						}
						else
						{
							uint8_t bReceiveBlockError;
	
							IIC_SET_RELEASE_ON_COMP;
							/* Call the PE Receive Block HAL function */
							bReceiveBlockError = iAPI2C1_MasterReceiveBlock(iAPI2C1_DeviceData, iAP_Auth_sCommData.bpReadDataBuffer, \
									iAP_Auth_sCommData.wRDSize, LDD_I2C_SEND_STOP );
							
							if(ERR_OK != bReceiveBlockError)
							{
								iAP_Auth_sMainSM.ActualState = IAP_AUTH_RETRY_STATE;
							}
						}
					}
					else
					{
						IIC_SET_RELEASE_ON_COMP;
						
						/* Call the PE Write Block HAL IIC function */
						/** @todo Function Return */
						(void)iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, iAP_Auth_sCommData.bpWriteDataBuffer, \
							iAP_Auth_sCommData.wWRSize, LDD_I2C_SEND_STOP);
					}
				}
			}
		}
	}
}

static void iAP_Auth_vfnIdle_State(void)
{
	/* Clears busy flag */
	iAP_Auth_gu32Status &= ~(1 << AUTH_BUSY);
}

void iAP_Auth_vfnStateMachine(void)
{
	/* Execute main Authentication state machine */
	iAP_Auth_vfnMainStateMachine[iAP_Auth_sMainSM.ActualState]();
}

static void iAP_Auth_vfnRetryRead_State(void)
{
	LDD_I2C_TBusState Status;
		
	/** @todo Function Return */
	(void)IIC_CHECK_BUS(Status);	
	if((!IAP_AUTH_COMM_BUSY) && (LDD_I2C_IDLE == Status))
	{
		/* Select IIC slave address */
		/** @todo Function Return */
		(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,IAP_AUTH_SLAVE_ADDRESS);
		/* Sets the Authentication busy and Authentication Read Flags */
		iAP_Auth_gu32Status |= (1 << AUTH_BUSY) | (1 << AUTH_WRITE) | (1 << AUTH_READ);
		iAP_Auth_bRetryCounter = 0;

		IAP_AUTH_SET_COMM_BUSY;
		IIC_SET_RESERVE;
		IIC_CLEAR_RELEASE_ON_COMP;

		/* Call the PE Write Block HAL IIC function */
	//	iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, (uint8_t *)&iAP_Auth_sCommData.bCPSoftAddress, SOFT_ADDRESS_SIZE, LDD_I2C_NO_SEND_STOP);
		/** @todo Function Return */
		(void)iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, (uint8_t *)&iAP_Auth_sCommData.bCPSoftAddress, SOFT_ADDRESS_SIZE, LDD_I2C_SEND_STOP);
		
		/* Set Auth Communication state machine to write state */
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_WAIT_HAL_STATE;
		//iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
	}
}

static void iAP_Auth_vfnRetryWrite_State(void)
{
	LDD_I2C_TBusState Status;

	/** @todo Function Return */
	(void)IIC_CHECK_BUS(Status);
	if((!IAP_AUTH_COMM_BUSY) && (LDD_I2C_IDLE == Status))
	{
		/* Select IIC slave address */
		/** @todo Function Return */
		(void)iAPI2C1_SelectSlaveDevice(iAPI2C1_DeviceData,LDD_I2C_ADDRTYPE_7BITS,IAP_AUTH_SLAVE_ADDRESS);
		/* Sets the Authentication busy and Authentication Read Flags */
		iAP_Auth_gu32Status |= (1 << AUTH_BUSY) | (1 << AUTH_WRITE);
		iAP_Auth_bRetryCounter = 0;

		IAP_AUTH_SET_COMM_BUSY;

		IIC_SET_RESERVE;
		IIC_SET_RELEASE_ON_COMP;
		/* Store the Register Address in the Write Data Buffer */
		*iAP_Auth_sCommData.bpWriteDataBuffer = iAP_Auth_sCommData.bCPSoftAddress;

		/* Call the PE Write Block HAL IIC function */
		/** @todo Function Return */
		(void)iAPI2C1_MasterSendBlock(iAPI2C1_DeviceData, iAP_Auth_sCommData.bpWriteDataBuffer, \
				iAP_Auth_sCommData.wWRSize, LDD_I2C_SEND_STOP);

		/* Set the Write state to be executed next */
		iAP_Auth_sMainSM.ActualState = IAP_AUTH_WAIT_HAL_STATE;
		//iAP_Auth_sMainSM.NextState = IAP_AUTH_IDLE_STATE;
	}
}
