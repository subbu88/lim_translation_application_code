/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale _PROJECT_ Software License. See  
* the FREESCALE_PROJECT_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/
///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Includes Section                        
///////////////////////////////////////////////////////////////////////////////////////////////////
#include "i2c_commands.h"

#include "bsp.h"
#include "i2c.h"
#include "assert.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                   Defines & Macros Section                   
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>
#define I2C_IOCTL_OK                                        (0x00)

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                       Typedef Section                        
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>

//! <brief description>
        //!< <brief description of each element in a enum/structure>

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                   Global Constants Section                   
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                   Static Constants Section                   
///////////////////////////////////////////////////////////////////////////////////////////////////
//! <brief description>

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                   Global Variables Section                   
///////////////////////////////////////////////////////////////////////////////////////////////////
//! <brief description>

    
///////////////////////////////////////////////////////////////////////////////////////////////////
//                                   Static Variables Section                   
///////////////////////////////////////////////////////////////////////////////////////////////////
//! <brief description>
OsaMutex transferLock;
OsaMutex openLock;
OsaMutex closeLock;

static OsaEvent i2cCommandsTimer;
static bool timerDone = false;

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                  Function Prototypes Section                 
///////////////////////////////////////////////////////////////////////////////////////////////////
/*!
 * @brief brief function description
 *
 * @param param1 param1 brief description
 * @param param2 param2 brief description
 * @return brief description of return value
 * @retval retval1 retval1 brief description
 * 
 * 
 */

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Functions Section                       
///////////////////////////////////////////////////////////////////////////////////////////////////

static void _link_event_cb(uint32_t id, void *data, uint32_t notused,
                           uint32_t notused2)
{
    bool *done = (bool*) data;
    *done = true;
}

/*************************************************************************************************
 *
 * @brief    Initialize locks needed for any i2c operation. This function must be called
 * 			once before i2c_commands_open()
 * 
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
uint8_t i2c_commands_init(void)
{
	uint8_t i2cInitStatus = kStatus_I2C_Success;
	
	osa_mutex_create(&openLock, false);
	osa_mutex_create(&transferLock, false);
	osa_mutex_create(&closeLock, false);
	
    osa_timeout_create(&i2cCommandsTimer, _link_event_cb, (void*) &timerDone);
	
	return i2cInitStatus;
}

/*************************************************************************************************
 *
 * @brief     De-initialize locks needed for any i2c operation. This function must be called if  
 * 			  i2c_commands functions are not needed after i2c_commands_close()
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
uint8_t i2c_commands_deinit(void)
{
	uint8_t i2cDeinitStatus = kStatus_I2C_Success;
	osa_mutex_destroy(&openLock);
	osa_mutex_destroy(&transferLock);
	osa_mutex_destroy(&closeLock);
    osa_timeout_destroy(&i2cCommandsTimer);
	return i2cDeinitStatus;
}

/*************************************************************************************************
 *
 * @brief    Open i2c file pointer according to parameter passed. 
 * 
 * @param    master_name [IN]	A string whit the i2c file pointer name in the OS.
 * 
 * 			 device [OUT]	In this parameter the function will return the corresponding file pointer 
 * 			 to i2c bus	if passed master_name is found as file pointer in OS
 * 			 
 * 			 adapter_nr [IN] Currently this parameter is not used in MQX.
 * 			 
 * @return   kStatus_I2C_Success:  The i2c file pointer with the name passed in master_name has been
 * 								  opened correctly.
 * 			
 * 			kStatus_I2C_Failure: The i2c file pointer with the name passed in master_name could not 
 * 								  been opened correctly.
 *          
 **************************************************************************************************/
uint8_t i2c_commands_open(const char *master_name,
                           uint32_t **device,
                           uint32_t adapter_nr)
{
    FILE * file;
    uint32_t ret;
    uint32_t i2cBaudrate = I2C_BUS_BAUDRATE;

	printf(" subbu2- i2c_commands_open \n ");

    int lockStatus;
    /* Protect I2C transaction in multitask environment */
    lockStatus = osa_mutex_lock(&openLock);

    if(ERRCODE_NO_ERROR == lockStatus)
    {

        ret = kStatus_I2C_Success;

        file = fopen(master_name,NULL);

        if (NULL != file) 
        {
            if (I2C_IOCTL_OK == ioctl (file, IO_IOCTL_I2C_SET_MASTER_MODE, NULL)) 
            {

                if (I2C_IOCTL_OK == ioctl(file,IO_IOCTL_I2C_SET_BAUD,&i2cBaudrate)) 
                {
                    *(*device) = (uint32_t)file;
                }
                else
                {
                    ret = kStatus_I2C_Failure;
                }
            } 
            else 
            {
                ret = kStatus_I2C_Failure;
            }
        } 
        else
        {
            ret = kStatus_I2C_Failure;
        }
        /* Release the transaction transferLock */
        lockStatus = osa_mutex_unlock(&openLock);
        if(ERRCODE_NO_ERROR != lockStatus )
        {
            ret = kStatus_I2C_Failure;
        }
    }
    else
    {
        ret = kStatus_I2C_Failure;
    }
    return ret;
}

/*************************************************************************************************
 *
 * @brief    Close a previously i2c file pointer.
 * 
 * @param    devide [IN] 		Indicate the identifier for the file pointer that should be closed.
 * 
 * @return   kStatus_I2C_Success:  The i2c file pointer with the name passed in master_name has been
 * 								  closed correctly.
 * 			
 * 			kStatus_I2C_Failure: The i2c file pointer with the name passed in master_name could not 
 * 								  been closed correctly.
 *          
 **************************************************************************************************/
uint8_t i2c_commands_close(uint32_t device)
{
    uint32_t ret;
    FILE *file;
 
    int lockStatus;
    /* Protect I2C transaction in multitask environment */
    lockStatus = osa_mutex_lock(&closeLock);

    if(ERRCODE_NO_ERROR == lockStatus)
    {

    file = (FILE *)(device);
    
    ret = kStatus_I2C_Success;
    if (NULL != file) 
    {
        fclose(file);
    }
    else 
    {
        ret = kStatus_I2C_Failure;
    }
    /* Release the transaction transferLock */
        lockStatus = osa_mutex_unlock(&closeLock);
        if(ERRCODE_NO_ERROR != lockStatus )
        {
            ret = kStatus_I2C_Failure;
        }
    }
    else
    {
        ret = kStatus_I2C_Failure;
    }
    return (ret);
}

/*************************************************************************************************
 *
 * @brief    Try to execute an i2c write operation in the bus corresponding to passed file pointer.
 * 
 * @param    master_handle [IN]: Identifier for the i2c file pointer.
 * 				
 * 			 slave_addr [IN]:: I2C slave address used in the i2c write transaction.
 * 			
 * 			 regNo[IN]: :	Pointer to desired register to be written 
 * 			
 * 			 regLen [IN]:   Indicate the number of bytes for register address
 * 			 
 * 		     buf [IN]:		Pointer with the data to be written.
 *
 * 			 len [IN]:    	Indicate the number of bytes to be written.
 * 		    	
 * @return   kStatus_I2C_Success:  The i2c write operation has been completed correctly.
 * 			
 * 			 kStatus_I2C_Failure: The i2c write operation could not been completed correctly.
 *          
 **************************************************************************************************/
uint8_t i2c_commands_write(uint32_t master_handle,
                            uint8_t  slave_addr,
                            uint8_t   *regNo,
                            uint8_t   regLen,
                            uint8_t *buf,
                            uint32_t len,
                            uint32_t timeOut)
{
    OsaTimeval timeval;
    uint8_t i2cTransferStatus;
    uint32_t i2cSlaveAddress;
    uint32_t ioctlParam;
    uint32_t result = 0;
    uint32_t i2cByteReadWrite;
    int timerStatus;     /* @todo i2c - EM: Why is this static? */
    FILE *file;
  
    int lockStatus;

    lockStatus = osa_mutex_lock(&transferLock);

    if(ERRCODE_NO_ERROR == lockStatus)
    {  

    	file = (FILE *)(master_handle);
    	i2cSlaveAddress = slave_addr;
    	i2cTransferStatus = kStatus_I2C_Success;

    	if (NULL ==  buf) 
    	{
    		printf("WriteRegisterI2C:paramter error!\n");
    		i2cTransferStatus = kStatus_I2C_Failure;
    	}
    	else
    	{
    		/* Set the destination address */
    		if (I2C_IOCTL_OK != ioctl(file, IO_IOCTL_I2C_SET_DESTINATION_ADDRESS, 
    				&i2cSlaveAddress)) 
    		{
    			i2cTransferStatus = kStatus_I2C_Failure;
    		}
    		else
    		{
                timeval.tv_sec = 0;
                timeval.tv_usec = (timeOut * 1000);
                timerDone = false;
                osa_timeout_start(&i2cCommandsTimer, &timeval);

    		    do{
    				i2cTransferStatus = kStatus_I2C_Success;
    				/* Write desired register and then the data */
    				i2cByteReadWrite = fwrite (regNo,1,regLen,file);
    				if (regLen != i2cByteReadWrite)
    				{
    					i2cTransferStatus = kStatus_I2C_Failure;
    					(void)ioctl(file, IO_IOCTL_I2C_STOP,&ioctlParam);
    				} 
    				//Verify timer status to know if a time out occur.
                    timerStatus = (int) timerDone;

    			}while( (i2cTransferStatus == kStatus_I2C_Failure) && (!timerStatus));
    			if(timerStatus)
    			{
    				i2cTransferStatus = kStatus_I2C_Timeout;
    			}
    			else
    			{
    				fflush(file);

                    timeval.tv_sec = 0;
                    timeval.tv_usec = (timeOut * 1000);
                    timerDone = false;
                    osa_timeout_start(&i2cCommandsTimer, &timeval);

    				do{
    					/* Write nbytes data to iAP auth chip */
    					i2cByteReadWrite = fwrite(buf,1,len,file);
    					if (len != i2cByteReadWrite)
    					{
    						i2cTransferStatus = kStatus_I2C_Failure;
    						(void)ioctl(file, IO_IOCTL_I2C_STOP,&ioctlParam);
    					} 
    					//Verify timer status to know if a time out occur.
                        timerStatus = (int) timerDone;

    				}while( (i2cTransferStatus == kStatus_I2C_Failure) && (!timerStatus));
    				/* @todo i2c - EM: There is no validation for the timeout in this case */
#if 0
    				/* Wait for device write cycle finish - acknowledge */
    				result = 0;
    				do 
    				{  /* Sends just I2C bus address, returns acknowledge bit and stops */
    					// fwrite (&result, 1, 0, file);

    					if (I2C_OK != ioctl (file, IO_IOCTL_FLUSH_OUTPUT, &ioctlParam))
    					{
    						printf ("  ERROR during wait (flush)\n");
    					}

    					if (I2C_OK != ioctl (file, IO_IOCTL_I2C_STOP, NULL))
    					{
    						printf ("  ERROR during wait (stop)\n");
    					}
    					result++;
    				} while ((ioctlParam & 1) || (result <= 1));
#endif
    			}
    		}
    		/* Stop I2C transfer */
    		if (I2C_OK != ioctl (file, IO_IOCTL_I2C_STOP, NULL))
    		{
    			i2cTransferStatus = kStatus_I2C_Failure;
    		}
    		
    		
    		/* Release the transaction transferLock */
    		lockStatus = osa_mutex_unlock(&transferLock);
    		if(ERRCODE_NO_ERROR != lockStatus )
    		{
    			i2cTransferStatus = kStatus_I2C_Failure;
    		}
    	}
    }
    else
    {
        i2cTransferStatus = kStatus_I2C_Timeout;
    }
    osa_timeout_cancel(&i2cCommandsTimer);
    return i2cTransferStatus;
}

/*************************************************************************************************
 *
 * @brief    Try to execute an i2c write operation in the bus corresponding to passed file pointer.
 * 
 * @param    master_handle [IN]: Identifier for the i2c file pointer.
 * 				
 * 			 slave_addr [IN]:: I2C slave address used in the i2c read transaction.
 * 			
 * 			 regNo[IN]: :	Pointer to desired register to be read
 * 			
 * 			 regLen [IN]:   Indicate the number of bytes  for register address
 * 
 * 		     buf [IN]:		Pointer where read data will be stored.
 * 		     
 * 			 len [IN]:    	Indicate the number of bytes to be read.
 * 		    	
 * @return   kStatus_I2C_Success:  The i2c read operation has been completed correctly.
 * 			
 * 			 kStatus_I2C_Failure: The i2c read operation could not been completed correctly.
 *          
 **************************************************************************************************/
uint8_t i2c_commands_read(uint32_t master_handle,
                           uint8_t  slave_addr,
                           uint8_t  * regNo,
                           uint8_t regLen,
                           uint8_t *buf,
                           uint32_t len,
                           uint32_t timeOut)
{
    OsaTimeval timeval;
	uint32_t i2cSlaveAddress;
    uint8_t i2cTransferStatus;
    uint32_t i2cByteReadWrite;
    uint32_t ioctlParam;
    FILE *file;
    int timerStatus;
     
    int lockStatus;

    lockStatus = osa_mutex_lock(&transferLock);
    
    if(ERRCODE_NO_ERROR == lockStatus)
    {
    	file = (FILE *)(master_handle);
    	i2cSlaveAddress = slave_addr;
    	i2cTransferStatus = kStatus_I2C_Success;

    	if (NULL == buf) 
    	{        printf("ReadRegisterI2C:paramter error!\n");
    	return kStatus_I2C_Failure;
    	}

    	//   Start condition
    	//   Device address with the R/W bit cleared to indicate write
    	//   Send one bytes for the 8 bit register address
    	//   Stop Condition followed by start condition (or a single restart condition)
    	//   Device address with the R/W bit set to indicate read
    	//   Read one bytes from the addressed register
    	//   Stop condition


    	/* I2C bus address also contains memory block index */
    	if (I2C_OK != ioctl (file, IO_IOCTL_I2C_SET_DESTINATION_ADDRESS, &i2cSlaveAddress))
    	{
    		i2cTransferStatus = kStatus_I2C_Failure;
    	}
    	else
    	{
    		if (I2C_OK == ioctl (file, IO_IOCTL_FLUSH_OUTPUT, &ioctlParam))
    		{
                timeval.tv_sec = 0;
                timeval.tv_usec = (timeOut * 1000);
                timerDone = false;
                osa_timeout_start(&i2cCommandsTimer, &timeval);

    		    do { 
    				i2cTransferStatus = kStatus_I2C_Success;

    				/* Write address within memory block */
    				i2cByteReadWrite = fwrite (regNo, 1, regLen, file);

    				if (regLen != i2cByteReadWrite)
    				{
    					i2cTransferStatus = kStatus_I2C_Failure;
    					(void)ioctl (file, IO_IOCTL_I2C_STOP, &ioctlParam);
    				}
    				//Verify timer status to know if a time out occur.
                    timerStatus = (int) timerDone;

    			}while( (i2cTransferStatus == kStatus_I2C_Failure) && (!timerStatus));
    			if(timerStatus)
    			{
    				i2cTransferStatus = kStatus_I2C_Timeout;
    			}
    			else
    			{

    				/* Wait for completion */
    				ioctlParam = fflush (file);
    				if (MQX_OK != ioctlParam)
    				{
    					i2cTransferStatus = kStatus_I2C_Failure;
    				}
    				else 
    				{
    					/* Restart I2C transfer for reading */
    					if (I2C_OK != ioctl (file, IO_IOCTL_I2C_REPEATED_START, NULL))
    					{
    						i2cTransferStatus = kStatus_I2C_Failure;
    					}
    					else
    					{
                            timeval.tv_sec = 0;
                            timeval.tv_usec = (timeOut * 1000);
                            timerDone = false;
                            osa_timeout_start(&i2cCommandsTimer, &timeval);

    					    do {
    							ioctlParam = len;
    							i2cTransferStatus = kStatus_I2C_Success;
    							/* Set number of byte to read request */
    							if (I2C_OK != ioctl (file, IO_IOCTL_I2C_SET_RX_REQUEST, &ioctlParam))
    							{
    								i2cTransferStatus = kStatus_I2C_Failure; 
    							}
    							else
    							{
    									/* Read all data */
    									i2cByteReadWrite = fread (buf, 1, ioctlParam, file);
    									if (i2cByteReadWrite != len)
    									{
    										i2cTransferStatus = kStatus_I2C_Failure;
    									}
    							}
    							if(kStatus_I2C_Failure == i2cTransferStatus)
    							{
    								(void)ioctl (file, IO_IOCTL_I2C_STOP, NULL);
    								osa_time_delay(1);
    							}

    							//Verify timer status to know if a time out occur.
                                timerStatus = (int) timerDone;

    						}while( (i2cTransferStatus == kStatus_I2C_Failure) && (!timerStatus));
    					}
    				}
    			}        

    		}
    		else
    		{
    			i2cTransferStatus = kStatus_I2C_Failure;
    		}
    	}
		/* Stop I2C transfer */
		if (I2C_OK != ioctl (file, IO_IOCTL_I2C_STOP, NULL))
		{
			i2cTransferStatus = kStatus_I2C_Failure;
		}
		/* Release the transaction transferLock */
		lockStatus = osa_mutex_unlock(&transferLock);

		if(ERRCODE_NO_ERROR != lockStatus )
		{
			i2cTransferStatus = kStatus_I2C_Failure;
		}
		else if(ERRCODE_TIMED_OUT == timerStatus)
		{
			i2cTransferStatus = kStatus_I2C_Timeout; 
		}  	
    }
    else
    {
        i2cTransferStatus = kStatus_I2C_Timeout;
    }
    osa_timeout_cancel(&i2cCommandsTimer);
    return i2cTransferStatus;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// EOF
///////////////////////////////////////////////////////////////////////////////////////////////////
