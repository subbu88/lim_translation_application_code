/**************************************************************************************************
 *
 * Freescale Semiconductor Inc.
 * (c) Copyright 2004-2010 Freescale Semiconductor, Inc.
 * ALL RIGHTS RESERVED.
 *
 **************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 **********************************************************************************************//*!
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * @file iAP_Authentication.c
 *
 * @author B24509
 *
 * @date Mar 1, 2012
 *
 * @brief
 *************************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
//#include "fas_cfg.h"
#include "osa_common.h"
#include "iAP_Blocking_Authentication.h"
#include "i2c_commands.h"
#include "iAP2_Host.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/

#define SOFT_ADDRESS_SIZE                (0x01u)
#define CERTIFICATE_LENGTH_SIZE            (0x02u)
#define CHALLENGE_LENGTH_SIZE            (0x02u)
#define SIGNATURE_LENGTH_SIZE            (0x02u)
#define ERROR_LENGTH_SIZE               (0x01u)
#define APPLE_CERTIFICATE_LENGTH_SIZE    (0x02u)
#define CP_CTRL_STATUS_REG_SIZE            (0x01u)
#define CP_CTRL_STATUS_ERROR_MASK        (0x80u)
#define CP_SC_RESULT_MASK               (0x70u)
#define CP_SC_RESULT_SHIFT                (0x04u)
#define IAP_AUT_INVALID_CERT_PAGES      (0xFF)

#define IAP_AUTH_WRITE_LENGTH_SIZE        (0x03u)
#define IAP_AUTH_PAGE_SIZE                (0x80u)


#define IAP_AUTH_I2C_SLAVE_ADDRESS                  (g_hostConfig.i2c_addr)
#define IAP_AUTH_REGISTER_ADDRESS_SIZE                (0x01)
#define IAP_AUTH_I2C_TIMEOUT                        (0)
#define IAP_AUTH_I2C_MAXIMUM_RETRIES                (30)
#define IAP_AUTH_I2C_DELAY_BETWEEN_TRANSFERS        (1)

#define IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS          (50)
#define IAP_AUTH_TIME_OUT_2S                        (2000)

#define IAP2_AUTHENTICATION_STATEMACHINE_TASK_PRIORITY        9

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

enum
{
    kAuthenticationNoFirstOp = 0,
    kAuthenticationStartSignatureGeneration,
    kAuthenticationStartChallengeGeneration,
    kAuthenticationStartSignatureValue,
    kAuthenticationStartCertificateValue,
    kAuthenticationNoSecondOp
}authenticationControlValue_t;

enum
{
    kAuthenticationCoprocessorNoValidResult= 0,         /* 0x00 */
    kAuthenticationSignatureGenerationOk,               /* 0x01 */
    kAuthenticationCoprocessorChallengeOk,                /* 0x02 */
    kAuthenticationCoprocessorSignatureOk,                /* 0x03 */
    kAuthenticationCoprocessorCertificateOk                /* 0x04 */
}authenticationCoprocessorResulValues_t;




typedef struct
{
    uint8_t  * writeDataBufferPtr;
    uint8_t  * readDataBufferPtr;
    uint16_t  writeBufferSize;
    uint16_t readBufferSize;
    uint8_t  coprocessorSoftAddress;
}iapAuthenticationCommunication_t;

typedef struct
{
    uint8_t * certificateDataPtr;
    uint8_t * signatureDataPtr;
    uint8_t * challengeDataPtr;
    uint8_t * appleResultPtr;
    uint16_t * lengthReturnPtr;
    uint16_t appleChallengeLength;
    uint8_t  writeCoprocessorControlStatus;
    uint8_t readCoprocessorControlStatus;
}iapAuthenticationMain_t;

typedef enum
{
    kiapAuthenticationReadCProtVerAndDevCommand= 0,
    kiapAuthenticationReadAccessoryCertificateCommand,
    kiapAuthenticationReadAccessoryChallengeCommand,
#if (_TRUE_ == IAP_IOS_AUTHENTICATION_ENABLE)
    kiapAuthenticationAppleCertificateValueCommand,
    kiapAuthenticationAppleChallengeGenerationCommand,
    kiapAuthenticationAppleSignatureValueCommand,
#endif
    kiapAuthenticationNoCommand
}iapAuthenticationStates_t;

typedef enum eAppleCPRegisters
{
    kdeviceVersion = 0,                                    /*0x00*/
    kfirmwareVersion,                                    /*0x01*/
    kauthenticationProtocolMajorVersion,                /*0x02*/
    kauthenticationProtocolMinorVersion,                /*0x03*/
    kdeviceId,                                            /*0x04*/
    kerrorCode,                                            /*0x05*/
    knotUsed06,                                        /*0x06*/
    knotUsed07,                                        /*0x07*/
    knotUsed08,                                        /*0x08*/
    knotUsed09,                                        /*0x09*/
    knotUsed0A,                                        /*0x0A*/
    knotUsed0B,                                        /*0x0B*/
    knotUsed0C,                                        /*0x0C*/
    knotUsed0D,                                        /*0x0D*/
    knotUsed0E,                                        /*0x0E*/
    knotUsed0F,                                        /*0x0F*/
    kAuthenticationControlAndStatus,                    /*0x10*/
    ksignatureDataLength,                                /*0x11*/
    ksignatureData,                                        /*0x12*/
    knotUsed13,                                        /*0x13*/
    knotUsed14,                                        /*0x14*/
    knotUsed15,                                        /*0x15*/
    knotUsed16,                                        /*0x16*/
    knotUsed17,                                        /*0x17*/
    knotUsed18,                                        /*0x18*/
    knotUsed19,                                        /*0x19*/
    knotUsed1A,                                        /*0x1A*/
    knotUsed1B,                                        /*0x1B*/
    knotUsed1C,                                        /*0x1C*/
    knotUsed1D,                                        /*0x1D*/
    knotUsed1E,                                        /*0x1E*/
    knotUsed1F,                                        /*0x1F*/
    kchallengeDataLength,                                /*0x20*/
    kchallengeData,                                        /*0x21*/
    knotUsed22,                                        /*0x22*/
    knotUsed23,                                        /*0x23*/
    knotUsed24,                                        /*0x24*/
    knotUsed25,                                        /*0x25*/
    knotUsed26,                                        /*0x26*/
    knotUsed27,                                        /*0x27*/
    knotUsed28,                                        /*0x28*/
    knotUsed29,                                        /*0x29*/
    knotUsed2A,                                        /*0x2A*/
    knotUsed2B,                                        /*0x2B*/
    knotUsed2C,                                        /*0x2C*/
    knotUsed2D,                                        /*0x2D*/
    knotUsed2E,                                        /*0x2E*/
    knotUsed2F,                                        /*0x2F*/
    kAccessoryCertificateDataLength,                    /*0x30*/
    kAccessoryCertificateDataP1,                        /*0x31*/
    kAccessoryCertificateDataP2,                        /*0x32*/
    kAccessoryCertificateDataP3,                        /*0x33*/
    kAccessoryCertificateDataP4,                        /*0x34*/
    kAccessoryCertificateDataP5,                        /*0x35*/
    kAccessoryCertificateDataP6,                        /*0x36*/
    kAccessoryCertificateDataP7,                        /*0x37*/
    kAccessoryCertificateDataP8,                        /*0x38*/
    kAccessoryCertificateDataP9,                        /*0x39*/
    kAccessoryCertificateDataP10,                        /*0x3A*/
    knotUsed3B,                                        /*0x3B*/
    knotUsed3C,                                        /*0x3C*/
    knotUsed3D,                                        /*0x3D*/
    knotUsed3E,                                        /*0x3E*/
    knotUsed3F,                                        /*0x3F*/
    kselfTestControlAndStatus,                        /*0x40*/
    kreserved41,                                        /*0x41*/
    kreserved42,                                        /*0x42*/
    kreserved43,                                        /*0x43*/
    kreserved44,                                        /*0x44*/
    kreserved45,                                        /*0x45*/
    kreserved46,                                        /*0x46*/
    kreserved47,                                        /*0x47*/
    kreserved48,                                        /*0x48*/
    kreserved49,                                        /*0x49*/
    kreserved4A,                                        /*0x4A*/
    kreserved4B,                                        /*0x4B*/
    kreserved4C,                                        /*0x4C*/
    ksystemEventCounter,                                /*0x4D*/
    kreserved4E,                                        /*0x4E*/
    kreserved4F,                                         /*0x4F*/
    kAppleDeviceCertificateDataLength,                /*0x50*/
    kAppleDeviceCertificateDataP1,                    /*0x51*/
    kAppleDeviceCertificateDataP2,                    /*0x52*/
    kAppleDeviceCertificateDataP3,                    /*0x53*/
    kAppleDeviceCertificateDataP4,                    /*0x54*/
    kAppleDeviceCertificateDataP5,                    /*0x55*/
    kAppleDeviceCertificateDataP6,                    /*0x56*/
    kAppleDeviceCertificateDataP7,                    /*0x57*/
    kAppleDeviceCertificateDataP8                    /*0x58*/
}appleCoprocessorRegisters_t;

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/


static void iAP_Auth_vfnReadProtVerAndDevIDCommand(uint8_t *pu8DataBuffer);

static void iAP_Auth_vfnReadAccessoryCertificateCommand(uint8_t *pu8RegisterData, uint16_t *wpCertificateLength);

static void iAP_Auth_vfnAccessoryChallengeCommand(uint8_t *pu8ChallengeData, uint8_t *puint8_tSignatureData,
                                    uint16_t wChallangeLenght, uint16_t *wpSignatureLength);

#if (_TRUE_ == IAP_IOS_AUTHENTICATION_ENABLE)
static void iAP_Auth_vfnAppleCertificateValueCommand(uint8_t *bpAppleCertificateData,
                                     uint8_t *bpAppleCertificateResult,
                                     uint16_t wAppleCertificateLenght);

static void iAP_Auth_vfnAppleChallengeGenCommand(uint8_t *bpAppleChallangeData,
                                                   uint16_t *wpAppleChallangeLength);


static void iAP_Auth_vfnAppleSignatureValCommand(uint8_t *bpAppleChallangeData, uint8_t *bpAppleSignature,
                                   uint16_t wAppleChallengeLength, uint16_t wAppleSignatureLength,
                                           uint8_t *bAppleSignaturResult);
#endif


static status_t iap_authentication_readRegister(uint8_t registerToRead,
        uint8_t * dataBufferPtr, uint32_t dataSize,uint32_t timeOut);

static status_t iap_authentication_writeRegister(uint8_t registerToRead,
        uint8_t *dataBufferPtr, uint32_t dataSize,uint32_t timeOut);

void iAP_Auth_vfnStateMachine(void * param);



/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
uint16_t authenticationCertificateSize;
uint8_t iAP_Auth_gbaRxBufferForCP[128];            /**< Buffer used to read and write data from-to CP */
uint8_t authenticationCoprocessorVersionAndID[6];            /**< Buffer to read current CP version and Device ID*/
uint8_t iAP_Auth_CPCertificate[1284];

uint8_t *bpOutputBuffer;
uint8_t *bpOutputBufferTwo;
uint8_t *bpOutputBufferThree;

uint16_t *wpWordOutputBuffer;

uint16_t wDataBufferSize;
uint16_t wDataBufferTwoSize;


/*************************************************************************************************/

/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static iapAuthenticationCommunication_t iAP_Auth_sCommData;
static iapAuthenticationMain_t iAP_Auth_sMainData;
static uint8_t iapAuthenticationWriteBuffer[IAP_AUTH_WRITE_LENGTH_SIZE];
static uint8_t iapAuthenticationRetryCounter;
static uint32_t timeToStartTransfer;
static uint32_t timeInTransfer;
static uint32_t i2cBusHandler;
static uint32_t *i2cBusHandlerPtr=&i2cBusHandler;
static uint32_t **i2cBusHandlerFilePtr = &i2cBusHandlerPtr;
static uint8_t g_authenticationCommand;
static uint8_t g_authenticationCommandCancelled;
static OsaThread iAP_Auth_vfnStateMachine_TaskId; 
static bool iAP_Auth_vfnStateMachine_End;
static void (*iAP_Auth_EventsCallback)(uint8_t);

OsaSem iAP_AuthenticationTaskEvent;
/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/


uint8_t iAP_Auth_bfnDeInit(void)
{
    if (iAP_Auth_vfnStateMachine_End)
        return 0;

    dprint("iAP_Auth_bfnDeInit start\n");

    iAP_Auth_vfnStateMachine_End = true;
    osa_sem_post(&iAP_AuthenticationTaskEvent);

    i2c_commands_close(i2cBusHandler);
    i2c_commands_deinit();

    return ERRCODE_NO_ERROR;
}

uint8_t iAP_Auth_bfnInit(auth_config_parameters * config_parameters)
{
    OsaThreadAttr attr;
    int status;
    uint8_t i2cTransferStatus;

    dprint("iAP_Auth_bfnInit start\n");

    uint8_t bAuthInitStatus = IAP_AUTH_ERROR;
    g_authenticationCommand= kiapAuthenticationNoCommand;
    g_hostConfig.i2c_addr=config_parameters->i2c_addr;
    g_authenticationCommandCancelled = FALSE;

    iAP_Auth_vfnStateMachine_End = false;

    i2c_commands_init();

    osa_sem_create(&iAP_AuthenticationTaskEvent, 0);

    i2cTransferStatus = i2c_commands_open(config_parameters->i2c_dev,
                                          i2cBusHandlerFilePtr,
                                          0);
    if (kStatus_I2C_Success == i2cTransferStatus){
        bAuthInitStatus = IAP_AUTH_OK;
        iAP_Auth_EventsCallback = config_parameters->events_callback;
    }
    
    osa_thread_attr_init(&attr);
    osa_thread_attr_set_stack_size(&attr, 2000);

    status = osa_thread_create(&iAP_Auth_vfnStateMachine_TaskId,
                               &attr,
                               iAP_Auth_vfnStateMachine,
                               NULL);
    osa_thread_set_name(iAP_Auth_vfnStateMachine_TaskId, "iAP Auth Task");

    osa_thread_attr_destroy(&attr);

    return bAuthInitStatus;
}

void iAP_Auth_vfnCancel(void)
{
    g_authenticationCommandCancelled = TRUE;
}

void iAP_Auth_vfnReadProtVerAndDevID(uint8_t *pu8DataBuffer)
{
    dprint("iAP_Auth_vfnReadProtVerAndDevID start\n");

    bpOutputBuffer = pu8DataBuffer;
    g_authenticationCommand =kiapAuthenticationReadCProtVerAndDevCommand; 
    osa_sem_post(&iAP_AuthenticationTaskEvent);
}

void iAP_Auth_vfnReadAccessoryCertificate(uint8_t *pu8RegisterData, uint16_t *wpCertificateLength)
{
    dprint("iAP_Auth_vfnReadAccessoryCertificate start\n");

    bpOutputBuffer = pu8RegisterData;
    wpWordOutputBuffer = wpCertificateLength;
    g_authenticationCommand = kiapAuthenticationReadAccessoryCertificateCommand;
    osa_sem_post(&iAP_AuthenticationTaskEvent);
}

void iAP_Auth_vfnAccessoryChallenge(uint8_t *pu8ChallengeData, uint8_t *puint8_tSignatureData,
                                    uint16_t wChallangeLenght, uint16_t *wpSignatureLength)
{
    dprint("iAP_Auth_vfnAccessoryChallenge start\n");

    bpOutputBuffer = pu8ChallengeData;
    bpOutputBufferTwo = puint8_tSignatureData;
    wDataBufferSize = wChallangeLenght;
    wpWordOutputBuffer = wpSignatureLength;
    g_authenticationCommand = kiapAuthenticationReadAccessoryChallengeCommand;
    osa_sem_post(&iAP_AuthenticationTaskEvent);
}

#if (_TRUE_ == IAP_IOS_AUTHENTICATION_ENABLE)
void iAP_Auth_vfnAppleCertificateVal(uint8_t *bpAppleCertificateData,
                                     uint8_t *bpAppleCertificateResult,
                                     uint16_t wAppleCertificateLenght)
{

    bpOutputBuffer = bpAppleCertificateData;
    bpOutputBufferTwo = bpAppleCertificateResult;
    wDataBufferSize = wAppleCertificateLenght;
    g_authenticationCommand = kiapAuthenticationAppleCertificateValueCommand;
    osa_sem_post(&iAP_AuthenticationTaskEvent);
}


void iAP_Auth_vfnAppleChallengeGen(uint8_t *bpAppleChallangeData,
                                                   uint16_t *wpAppleChallangeLength)
{
    bpOutputBuffer = bpAppleChallangeData;
    wpWordOutputBuffer = wpAppleChallangeLength;
    g_authenticationCommand = kiapAuthenticationAppleChallengeGenerationCommand;
    osa_sem_post(&iAP_AuthenticationTaskEvent);
}


void iAP_Auth_vfnAppleSignatureVal(uint8_t *bpAppleChallangeData, uint8_t *bpAppleSignature,
                                   uint16_t wAppleChallengeLength, uint16_t wAppleSignatureLength,
                                           uint8_t *bAppleSignaturResult)
{
    bpOutputBuffer = bpAppleChallangeData;
    bpOutputBufferTwo = bpAppleSignature;
    bpOutputBufferThree = bAppleSignaturResult;
    wDataBufferSize = wAppleChallengeLength;
    wDataBufferTwoSize = wAppleSignatureLength;
    g_authenticationCommand = kiapAuthenticationAppleSignatureValueCommand;
    osa_sem_post(&iAP_AuthenticationTaskEvent);
}
#endif



static void iAP_Auth_vfnReadProtVerAndDevIDCommand(uint8_t *dataBufferPtr)
{
    status_t i2cTransferStatus;

    do
    {
        //Read coprocessor register to get Version and ID.
        i2cTransferStatus = iap_authentication_readRegister(kauthenticationProtocolMajorVersion,
                dataBufferPtr,6,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

    }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

    if(FALSE  == g_authenticationCommandCancelled)
    {
        //report command complete event to iAP2Host
        iAP_Auth_EventsCallback(KAuthenticationCommandComplete);
    }
    g_authenticationCommandCancelled = FALSE;
}


static void iAP_Auth_vfnReadAccessoryCertificateCommand(uint8_t *registerDataPtr,
        uint16_t *certificateLengthPtr)
{

    status_t i2cTransferStatus;
    uint8_t *lengthReturnTmpPtr;

    dprint("iAP_Auth_vfnReadAccessoryCertificateCommand start\n");

    /* Save the location where certificate must be stored */
    iAP_Auth_sMainData.certificateDataPtr = registerDataPtr;
    /* Save location to where the size of the certificate must be stored for the host */
    iAP_Auth_sMainData.lengthReturnPtr = certificateLengthPtr;

    do
    {
        i2cTransferStatus = iap_authentication_readRegister(kAccessoryCertificateDataLength, \
                (uint8_t *)&authenticationCertificateSize,(uint32_t)CERTIFICATE_LENGTH_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

    }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

    if(FALSE  == g_authenticationCommandCancelled)
    {
        lengthReturnTmpPtr = (uint8_t*)iAP_Auth_sMainData.lengthReturnPtr;

#if(_FALSE_ == BIG_ENDIAN_CORE)
        /* Converts bytes received from big-endian to little-endian */
        BYTESWAP16(authenticationCertificateSize,authenticationCertificateSize);
#endif
        /* Save certificate length for host layer */
        /* @todo if porting to an architecture with different endianness
            check following byte order: */
        lengthReturnTmpPtr[1] = (uint8_t)(authenticationCertificateSize >> 0x08);
        lengthReturnTmpPtr[0] = (uint8_t)(authenticationCertificateSize);

        do
        {
            i2cTransferStatus = iap_authentication_readRegister(kAccessoryCertificateDataP1, \
                    iAP_Auth_sMainData.certificateDataPtr, authenticationCertificateSize,IAP_AUTH_TIME_OUT_2S);

        }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

        if(FALSE  == g_authenticationCommandCancelled)
        {
            //report command complete event to iAP2Host
            dprint("iAP_Auth_ReadAccCertifacte done\n");
            iAP_Auth_EventsCallback(KAuthenticationCommandComplete);
        }
    }
    g_authenticationCommandCancelled = FALSE;
}

static void iAP_Auth_vfnAccessoryChallengeCommand(uint8_t *challengeDataPtr, uint8_t *signatureDataPtr,
        uint16_t challangeLenght, uint16_t * signatureLengthPtr)
{
    status_t i2cTransferStatus;
    uint8_t *writeRegisterBufferPointer;
    uint8_t* tempLengthPtr;
    /* Temporal variable to store the result from the CP executed cmd */
    uint8_t coprocessorResult;

    dprint("iAP_Auth_vfnAccessoryChallengeCommand start\n");

    /* Save the location where signature must be stored */
    iAP_Auth_sMainData.signatureDataPtr = signatureDataPtr;
    /* Save location to where the size of the Signature Data must be stored for the host */
    iAP_Auth_sMainData.lengthReturnPtr = signatureLengthPtr;
    /* Save the location where challenge data is stored */
    iAP_Auth_sMainData.challengeDataPtr = challengeDataPtr;
    /* Save the Challenge length in the length global variable */
    authenticationCertificateSize = challangeLenght;

    //@todo This change was made to support KL, but won't work with big endian devices
    /* Save Challenge length into a static buffer with the MSB first */
    iapAuthenticationWriteBuffer[1] = (uint8_t)((challangeLenght) >> 0x08);
    iapAuthenticationWriteBuffer[2] = (uint8_t)((challangeLenght));

    do{
        // Start by writing the challenge data length in the CP */
        i2cTransferStatus= iap_authentication_writeRegister(kchallengeDataLength,
                (uint8_t *)&iapAuthenticationWriteBuffer[1], \
                (uint16_t)(CHALLENGE_LENGTH_SIZE),IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

    }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

    if(FALSE  == g_authenticationCommandCancelled)
    {
        do
        {
            // Write challenge data into the CP
            i2cTransferStatus= iap_authentication_writeRegister(kchallengeData,
                    iAP_Auth_sMainData.challengeDataPtr,  challangeLenght, IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

        }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

        if(FALSE  == g_authenticationCommandCancelled)
        {
            writeRegisterBufferPointer = &iapAuthenticationWriteBuffer[1];
            do
            {
                // Load the start signature generation value in the byte to write
                *writeRegisterBufferPointer = kAuthenticationStartSignatureGeneration;
                /* Write the start of signature generation value */
                i2cTransferStatus = iap_authentication_writeRegister(kAuthenticationControlAndStatus, \
                        (uint8_t*)&iapAuthenticationWriteBuffer[1], CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_2S);


            }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

            if(FALSE  == g_authenticationCommandCancelled)
            {
                do
                {
                    // Read the Status and control register
                    i2cTransferStatus = iap_authentication_readRegister(kAuthenticationControlAndStatus, \
                            (uint8_t*)&iAP_Auth_sMainData.readCoprocessorControlStatus, CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

                }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                if(FALSE  == g_authenticationCommandCancelled)
                {
                    /* If there was NO error during the signature generation */
                    if (!(CP_CTRL_STATUS_ERROR_MASK == iAP_Auth_sMainData.readCoprocessorControlStatus))
                    {
                        /* Keep only the result fields */
                        coprocessorResult = (uint8_t)(iAP_Auth_sMainData.readCoprocessorControlStatus & CP_SC_RESULT_MASK);
                        /* Shift the data to the less significant nibble */
                        coprocessorResult = (uint8_t)(coprocessorResult >> CP_SC_RESULT_SHIFT);
                        /* If the signature successfully generated flag is set */
                        if (kAuthenticationSignatureGenerationOk == coprocessorResult)
                        {
                            do
                            {
                                // Read the signature length
                                i2cTransferStatus =  iap_authentication_readRegister(ksignatureDataLength,
                                        (uint8_t *)&authenticationCertificateSize, SIGNATURE_LENGTH_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

                            }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                            if(FALSE  == g_authenticationCommandCancelled)
                            {
                                /* The state machine has finished reading the signature length */
                                //  uint16_t* wpTempLength = iAP_Auth_sMainData.lengthReturnPtr;
                                tempLengthPtr = (uint8_t*)iAP_Auth_sMainData.lengthReturnPtr;

#if(_FALSE_ == BIG_ENDIAN_CORE)
                                /* Convert Signature length to little-endian */
                                BYTESWAP16(authenticationCertificateSize,authenticationCertificateSize);
#endif

                                /* Save certificate length for host layer */
                                //  *wpTempLength = (uint16_t)authenticationCertificateSize;
                                /* @todo if porting to an architecture with different endianness
                           check following byte order: */
                                tempLengthPtr[1] = (uint8_t)(authenticationCertificateSize >> 0x08);
                                tempLengthPtr[0] = (uint8_t)(authenticationCertificateSize);

                                do
                                {
                                    // Read the signature data
                                    i2cTransferStatus = iap_authentication_readRegister(ksignatureData, \
                                            iAP_Auth_sMainData. signatureDataPtr, authenticationCertificateSize,IAP_AUTH_TIME_OUT_2S);

                                }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                                if(FALSE  == g_authenticationCommandCancelled)
                                {
                                    iAP_Auth_EventsCallback(KAuthenticationCommandComplete);
                                }
                            }
                        }
                        else /* If any other result is reported */
                        {
                            do
                            {
                                // Read the signature length
                                i2cTransferStatus =  iap_authentication_readRegister(kerrorCode,
                                        &coprocessorResult, ERROR_LENGTH_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

                            }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                            if(FALSE  == g_authenticationCommandCancelled)
                            {
                                /* Set the signature generation error */
                                iAP_Auth_EventsCallback(KAuthenticationCommandComplete);
                            }
                        }
                    }
                }
            }
        }
    }
    g_authenticationCommandCancelled = FALSE;
}

#if (_TRUE_ == IAP_IOS_AUTHENTICATION_ENABLE)

static void iAP_Auth_vfnAppleCertificateValueCommand(uint8_t *appleCertificateDataPtr,
        uint8_t *appleCertificateResultPtr, \
        uint16_t appleCertificateLenght)
{
    status_t i2cTransferStatus;
    uint16_t *lengthBufferPointer;
    uint8_t certificatePagesNumber;
    uint16_t certificateDataOffset;
    uint8_t *writeRegisterBufferPointer;
    uint8_t coprocessorResult;

    lengthBufferPointer = (uint16_t *)&iapAuthenticationWriteBuffer[1];

    certificateDataOffset = 0;
    certificatePagesNumber = 0;

    /* Save the location where result must be stored */
    iAP_Auth_sMainData.appleResultPtr = appleCertificateResultPtr;
    /* Save the location where Apple Certificate data is stored */
    iAP_Auth_sMainData.certificateDataPtr = appleCertificateDataPtr;
    /* Save the Certificate length in the length global variable */
    authenticationCertificateSize = appleCertificateLenght;
#if(_FALSE_ == BIG_ENDIAN_CORE)
    /* Convert Certificate length to big-endian and save it in a static buffer */
    BYTESWAP16(appleCertificateLenght,*lengthBufferPointer);
#else
    *lengthBufferPointer = (appleCertificateLenght);
#endif

    do
    {
        /* Starts by writing the challenge data length in the CP */
        i2cTransferStatus = iap_authentication_writeRegister(kAppleDeviceCertificateDataLength, \
                (uint8_t *)&iapAuthenticationWriteBuffer[1], (uint16_t)(APPLE_CERTIFICATE_LENGTH_SIZE),IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

    }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

    if(FALSE  == g_authenticationCommandCancelled)
    {
        certificatePagesNumber = 0;
        while(authenticationCertificateSize)
        {
            if(IAP_AUTH_PAGE_SIZE < authenticationCertificateSize)
            {
                do
                {
                    /* Write apple certificate data into the CP */
                    i2cTransferStatus = iap_authentication_writeRegister(
                            kAppleDeviceCertificateDataP1 + certificatePagesNumber, \
                            iAP_Auth_sMainData.certificateDataPtr + certificateDataOffset, \
                            IAP_AUTH_PAGE_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);
                }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

            }
            else
            {
                do
                {
                    /* Write the last apple certificate section into the CP */
                    i2cTransferStatus = iap_authentication_writeRegister(
                            kAppleDeviceCertificateDataP1 + certificatePagesNumber, \
                            iAP_Auth_sMainData.certificateDataPtr + certificateDataOffset, \
                            authenticationCertificateSize ,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);
                }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );
            }

            if(FALSE  == g_authenticationCommandCancelled)
            {
                if(i2cTransferStatus != kStatus_I2C_Success)
                {
                    iAP_Auth_EventsCallback(KAuthenticationTimeoutError);
                    //Put certificate size in 0 to end loop
                    authenticationCertificateSize = 0;
                    //Put invalid value to identify an error has happend
                    certificatePagesNumber = IAP_AUT_INVALID_CERT_PAGES;
                }
                else
                {
                    /* Subtract the apple certificate data bytes already written */
                    authenticationCertificateSize -= IAP_AUTH_PAGE_SIZE;
                    /* Move the offset one page size to write on the next Register */
                    certificateDataOffset += IAP_AUTH_PAGE_SIZE;
                    /* Increment the page number */
                    certificatePagesNumber++;
                }
            }
            else
            {
                //Put certificate size in 0 to end loop
                authenticationCertificateSize = 0;
                //Put invalid value to identify an error has happend
                certificatePagesNumber = IAP_AUT_INVALID_CERT_PAGES;
            }
        }

        if( IAP_AUT_INVALID_CERT_PAGES != certificatePagesNumber)
        {
            /* Clear static variables for next apple authentication */
            certificateDataOffset = (uint16_t)_FALSE_;
            certificatePagesNumber = _FALSE_;

            writeRegisterBufferPointer = &iapAuthenticationWriteBuffer[1];

            /* Load the start signature generation value in the byte to write */
            *writeRegisterBufferPointer = kAuthenticationStartSignatureGeneration;

            do
            {
                /* Write the start of signature generation value */
                i2cTransferStatus = iap_authentication_writeRegister(kAuthenticationControlAndStatus, \
                        (uint8_t*)&iapAuthenticationWriteBuffer[1], CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

            }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );
            if(FALSE  == g_authenticationCommandCancelled)
            {
                /* Read the Status and control register */
                i2cTransferStatus = iap_authentication_readRegister(kAuthenticationControlAndStatus, \
                        (uint8_t*)&iAP_Auth_sMainData.readCoprocessorControlStatus, CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);


                if(i2cTransferStatus != kStatus_I2C_Success)
                {
                    iAP_Auth_EventsCallback(KAuthenticationTimeoutError);
                }
                else
                {
                    /* If there was NO error during the signature generation */
                    if (!(CP_CTRL_STATUS_ERROR_MASK == iAP_Auth_sMainData.readCoprocessorControlStatus))
                    {
                        /* Keep only the result fields */
                        coprocessorResult = (uint8_t)(iAP_Auth_sMainData.readCoprocessorControlStatus & CP_SC_RESULT_MASK);
                        /* Shift the data to the less significant nibble */
                        coprocessorResult = (uint8_t)(coprocessorResult >> CP_SC_RESULT_SHIFT);

                        /* If the signature successfully generated flag is set */
                        if (kAuthenticationCoprocessorCertificateOk  == coprocessorResult)
                        {
                            /* Stores the certificate validation result */
                            *iAP_Auth_sMainData.appleResultPtr = IAP_AUTH_APPLE_CERT_RESULT_OK;
                            iAP_Auth_EventsCallback(KAuthenticationCommandComplete);
                        }
                        //                        else /* If any other result is reported */
                        //                        {
                        //                            /* Set the signature generation error */
                        //                            iAP_Auth_EventsCallback(KAuthenticationSignatureError);
                        //                            /* Stores the certificate validation result 0xFF == Error */
                        //                            *iAP_Auth_sMainData.appleResultPtr = IAP_AUTH_APPLE_SIGN_RESULT_ERR;
                        //                        }
                    }
                    else /* If there was an error in the signature generation */
                    {

                        do
                        {

                            /* Read the error code register */
                            i2cTransferStatus =  iap_authentication_readRegister(kerrorCode, (uint8_t*)&iAP_Auth_sMainData.readCoprocessorControlStatus, \
                                    CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);


                        }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                        if(FALSE  == g_authenticationCommandCancelled)
                        {
                            /* Stores the error code in the result variable */
                            *iAP_Auth_sMainData. appleResultPtr = iAP_Auth_sMainData.readCoprocessorControlStatus;
                            /* Set the signature generation error */
                            iAP_Auth_EventsCallback(iAP_Auth_sMainData.readCoprocessorControlStatus);
                        }
                    }
                }
            }
        }
    }
    g_authenticationCommandCancelled = FALSE;
}

static void iAP_Auth_vfnAppleChallengeGenCommand(uint8_t *appleChallangeDataPtr,
        uint16_t *appleChallangeLengthPtr)
{
    status_t i2cTransferStatus;
    uint8_t coprocessorResult;

    uint8_t *writeRegisterBufferPointer = &iapAuthenticationWriteBuffer[1];



    /* Stores the address where the apple challenge must be stored */
    iAP_Auth_sMainData.challengeDataPtr = appleChallangeDataPtr;
    /* Stores the address where the apple challenge length must be stored */
    iAP_Auth_sMainData.lengthReturnPtr = appleChallangeLengthPtr;
    /* Load the start apple challenge generation value in the byte to write buff */
    *writeRegisterBufferPointer = kAuthenticationStartChallengeGeneration;


    do
    {

        /* Write the start of apple Challenge Generation value */
        i2cTransferStatus = iap_authentication_writeRegister(kAuthenticationControlAndStatus, \
                (uint8_t*)&iapAuthenticationWriteBuffer[1], CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

    }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

    if(FALSE  == g_authenticationCommandCancelled)
    {

        do
        {
            /* Read the Status and control register */
            i2cTransferStatus = iap_authentication_readRegister(kAuthenticationControlAndStatus, \
                    (uint8_t*)&iAP_Auth_sMainData.readCoprocessorControlStatus, CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);
        }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

        if(FALSE  == g_authenticationCommandCancelled)
        {
            /* If there was NO error during the apple challenge generation */
            if (!(CP_CTRL_STATUS_ERROR_MASK == iAP_Auth_sMainData.readCoprocessorControlStatus))
            {
                /* Keep only the result fields */
                coprocessorResult = (uint8_t)(iAP_Auth_sMainData.readCoprocessorControlStatus & CP_SC_RESULT_MASK);
                /* Shift the data to the less significant nibble */
                coprocessorResult = (uint8_t)(coprocessorResult >> CP_SC_RESULT_SHIFT);
                /* If the apple challenge successfully generated flag is set */
                if (kAuthenticationCoprocessorChallengeOk == coprocessorResult)
                {


                    do
                    {
                        /* Read the signature length */
                        i2cTransferStatus =  iap_authentication_readRegister(kchallengeDataLength,
                                (uint8_t *)&authenticationCertificateSize,  SIGNATURE_LENGTH_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

                    }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                    if(FALSE  == g_authenticationCommandCancelled)
                    {

                        /* The state machine has finished reading the apple challenge length */

#if(_FALSE_ == BIG_ENDIAN_CORE)
                        /* Convert Apple Challenge length to little-endian */
                        BYTESWAP16(authenticationCertificateSize,authenticationCertificateSize);
#endif
                        /* Stores the Apple Challenge length in the Address provided by host */
                        *iAP_Auth_sMainData.lengthReturnPtr = authenticationCertificateSize;


                        do
                        {
                            /* Read the Apple Challenge data */
                            i2cTransferStatus =  iap_authentication_readRegister(kchallengeData, \
                                    iAP_Auth_sMainData.challengeDataPtr, authenticationCertificateSize,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);
                        }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                        if(FALSE  == g_authenticationCommandCancelled)
                        {
                            iAP_Auth_EventsCallback(KAuthenticationCommandComplete);
                        }
                    }
                }
                //                else /* If any other result is reported */
                //                {
                //                    /* Set the signature generation error */
                //                    iAP_Auth_EventsCallback(KAuthenticationSignatureError);
                //                }
            }
            else /* If there was an error in the signature generation */
            {
                do
                {
                    /* Read the error code register */
                    i2cTransferStatus = iap_authentication_readRegister(kerrorCode, (uint8_t*)&iAP_Auth_sMainData.readCoprocessorControlStatus, \
                            CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);
                }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                if(FALSE  == g_authenticationCommandCancelled)
                {
                    /* Stores the error code in the result variable */
                    *iAP_Auth_sMainData. appleResultPtr = iAP_Auth_sMainData.readCoprocessorControlStatus;
                    /* Set the signature generation error */
                    iAP_Auth_EventsCallback(iAP_Auth_sMainData.readCoprocessorControlStatus);
                }
            }
        }
    }
    g_authenticationCommandCancelled = FALSE;
}

static void iAP_Auth_vfnAppleSignatureValCommand(uint8_t *  appleChallangeDataPtr, uint8_t *appleSignatureDataPtr,
        uint16_t appleChallengeLength, uint16_t appleSignatureLength,
        uint8_t *appleSignaturResultPtr)
{
    status_t i2cTransferStatus;

    uint16_t *lengthBufferPointer;
    uint8_t coprocessorResult;
    uint8_t *writeRegisterBufferPointer;

    /* Temporal pointer to store the length data in the buffer to transmit */
    lengthBufferPointer = (uint16_t *)(&iapAuthenticationWriteBuffer[1]);

    /* Save location to where the Apple Challenge is stored */
    iAP_Auth_sMainData.challengeDataPtr = appleChallangeDataPtr;
    /* Save location to where the Apple Signature is stored */
    iAP_Auth_sMainData.signatureDataPtr = appleSignatureDataPtr;
    /* Save location to where the result will be stored for upper layer */
    iAP_Auth_sMainData. appleResultPtr = appleSignaturResultPtr;
    /* Save the Signature data length */
    authenticationCertificateSize = appleSignatureLength;
    /* Save the Challenge data length */
    iAP_Auth_sMainData.appleChallengeLength = appleSignatureLength;
#if(_FALSE_ == BIG_ENDIAN_CORE)
    /* Convert Signature length to big-endian and save it in the buffer to transmit */
    BYTESWAP16(appleSignatureLength,*lengthBufferPointer);
#else
    *lengthBufferPointer = (appleSignatureLength);
#endif


    do {

    /* Start by writing the Signature length */
    i2cTransferStatus =iap_authentication_writeRegister(ksignatureDataLength ,(uint8_t *)&iapAuthenticationWriteBuffer[1], \
            (uint16_t)(SIGNATURE_LENGTH_SIZE),IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

    }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

    if(FALSE == g_authenticationCommandCancelled)
    {

        do
        {
        /* The state machine has finished writing the apple signature length */
        /* Write apple Signature data into the CP */
        i2cTransferStatus = iap_authentication_writeRegister(ksignatureData, \
                iAP_Auth_sMainData. signatureDataPtr, authenticationCertificateSize ,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);
        }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

        if(FALSE  == g_authenticationCommandCancelled)
        {

            /* Temporal pointer to store the length value in the buffer to transmit */
            lengthBufferPointer = (uint16_t *)&iapAuthenticationWriteBuffer[1];

#if(_FALSE_ == BIG_ENDIAN_CORE)
            /* Convert Apple Challenge length to big-endian and save it in the buffer to transmit */
            BYTESWAP16(iAP_Auth_sMainData.appleChallengeLength,*lengthBufferPointer);
#else
            *lengthBufferPointer = (iAP_Auth_sMainData.appleChallengeLength);
#endif

            do
            {
            /* Write apple Challenge data into the CP */
            i2cTransferStatus = iap_authentication_writeRegister(kchallengeDataLength ,(uint8_t *)&iapAuthenticationWriteBuffer[1], \
                    (uint16_t)(CHALLENGE_LENGTH_SIZE),IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);
            //Verify timer status to know if a time out occur.
            }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

            if(FALSE  == g_authenticationCommandCancelled)
            {
                do
                {
                /* Write apple Challenge data into the CP */
                i2cTransferStatus = iap_authentication_writeRegister(kchallengeData,
                        iAP_Auth_sMainData.challengeDataPtr,
                        iAP_Auth_sMainData.appleChallengeLength,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);
                }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                if(FALSE  == g_authenticationCommandCancelled)
                {
                    /* The state machine has finished writing the apple challenge data */

                    /* Temporal pointer of the buffer to transmit */
                    writeRegisterBufferPointer = &iapAuthenticationWriteBuffer[1];

                    /* Load the start apple signature validation value in the buffer to send */
                    *writeRegisterBufferPointer = kAuthenticationStartSignatureValue;

                    do
                    {
                    /* Write the start of signature validation value */
                    i2cTransferStatus = iap_authentication_writeRegister(kAuthenticationControlAndStatus, \
                            (uint8_t*)&iapAuthenticationWriteBuffer[1], CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

                    }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                    if(FALSE  == g_authenticationCommandCancelled)
                    {

                        do
                        {
                        /* Read the Status and control register */
                        i2cTransferStatus = iap_authentication_readRegister(kAuthenticationControlAndStatus, \
                                (uint8_t*)&iAP_Auth_sMainData.readCoprocessorControlStatus, CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

                        }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                        if(FALSE  == g_authenticationCommandCancelled)
                        {
                            /* If there was NO error during the apple challenge generation */
                            if (!(CP_CTRL_STATUS_ERROR_MASK == iAP_Auth_sMainData.readCoprocessorControlStatus))
                            {
                                /* Keep only the result fields */
                                coprocessorResult = (uint8_t)(iAP_Auth_sMainData.readCoprocessorControlStatus & CP_SC_RESULT_MASK);
                                /* Shift the data to the less significant nibble */
                                coprocessorResult = (uint8_t)(coprocessorResult >> CP_SC_RESULT_SHIFT);

                                /* If the signature successfully generated flag is set */
                                if (kAuthenticationCoprocessorSignatureOk == coprocessorResult)
                                {
                                        /* Stores the certificate validation result */
                                        *iAP_Auth_sMainData.appleResultPtr = IAP_AUTH_APPLE_SIGN_RESULT_OK;
                                        iAP_Auth_EventsCallback(KAuthenticationCommandComplete);

                                }
//                                else /* If any other result is reported */
//                                {
//
//                                    /* Set the signature generation error */
//                                    iAP_Auth_EventsCallback(KAuthenticationSignatureError);
//
//                                }
                            }
                            else /* If there was an error in the signature generation */
                            {
                                do
                                {
                                /* Read the error code register */
                                i2cTransferStatus = iap_authentication_readRegister(kerrorCode, (uint8_t*)&iAP_Auth_sMainData.readCoprocessorControlStatus, \
                                        CP_CTRL_STATUS_REG_SIZE,IAP_AUTH_TIME_OUT_I2C_TRANSACTIONS);

                                }while((kStatus_I2C_Success !=i2cTransferStatus) && (g_authenticationCommandCancelled != TRUE) );

                                if(FALSE  == g_authenticationCommandCancelled)
                                {
                                    /* Stores the error code in the result variable */
                                    *iAP_Auth_sMainData. appleResultPtr = iAP_Auth_sMainData.readCoprocessorControlStatus;
                                    /* Set the signature generation error */
                                    iAP_Auth_EventsCallback(iAP_Auth_sMainData.readCoprocessorControlStatus);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    g_authenticationCommandCancelled = FALSE;
}
#endif

static status_t iap_authentication_readRegister(uint8_t registerToRead,
        uint8_t * dataBufferPtr, uint32_t dataSize,uint32_t timeOut)
{

    status_t i2cTransferStatus;

    /* Store the Reading information into a structure for the State machine to use it */
    iAP_Auth_sCommData.coprocessorSoftAddress = registerToRead;
    iAP_Auth_sCommData.readDataBufferPtr = dataBufferPtr;
    iAP_Auth_sCommData.readBufferSize = dataSize;

    i2cTransferStatus = i2c_commands_read(i2cBusHandler,IAP_AUTH_I2C_SLAVE_ADDRESS,
            &iAP_Auth_sCommData.coprocessorSoftAddress,IAP_AUTH_REGISTER_ADDRESS_SIZE,
            iAP_Auth_sCommData.readDataBufferPtr, iAP_Auth_sCommData.readBufferSize,timeOut);

    return i2cTransferStatus;
}


static status_t iap_authentication_writeRegister(uint8_t registerToWrite, uint8_t *dataBufferPtr,
        uint32_t dataSize,uint32_t timeOut)
{

    status_t i2cTransferStatus;

    /* Store the bytes to write pointer within the comm struct */
    iAP_Auth_sCommData.coprocessorSoftAddress = registerToWrite;
    iAP_Auth_sCommData.writeDataBufferPtr = dataBufferPtr;
    iAP_Auth_sCommData.writeBufferSize = dataSize;

    i2cTransferStatus = i2c_commands_write(i2cBusHandler, IAP_AUTH_I2C_SLAVE_ADDRESS,
            &iAP_Auth_sCommData.coprocessorSoftAddress,IAP_AUTH_REGISTER_ADDRESS_SIZE,
            iAP_Auth_sCommData.writeDataBufferPtr,(uint32_t)iAP_Auth_sCommData.writeBufferSize , timeOut);

    return i2cTransferStatus;

}

void iAP_Auth_vfnStateMachine(void * param)
{
    static bool executeCommand = false;
    int syncStatus;

        dprint("iAP_Auth_vfnStateMachine started\n");
    printf("iAP_Auth_vfnStateMachine started\n");

    do {
        if (executeCommand) {
            if (iAP_Auth_vfnStateMachine_End)
                break;

            dprint("iAP_Auth_vfnStateMachine execute command %d\n",
                   g_authenticationCommand);

            switch(g_authenticationCommand)
            {
                case kiapAuthenticationReadCProtVerAndDevCommand:
                    iAP_Auth_vfnReadProtVerAndDevIDCommand(bpOutputBuffer);
                    break;

                case kiapAuthenticationReadAccessoryCertificateCommand:
                    iAP_Auth_vfnReadAccessoryCertificateCommand(bpOutputBuffer,wpWordOutputBuffer);
                            break;

                case kiapAuthenticationReadAccessoryChallengeCommand:
                    iAP_Auth_vfnAccessoryChallengeCommand(bpOutputBuffer, bpOutputBufferTwo, wDataBufferSize,wpWordOutputBuffer);
                            break;


#if (_TRUE_ == IAP_IOS_AUTHENTICATION_ENABLE)
                case kiapAuthenticationAppleCertificateValueCommand:
                    iAP_Auth_vfnAppleCertificateValueCommand(bpOutputBuffer,bpOutputBufferTwo,wDataBufferSize);
                            break;

                case kiapAuthenticationAppleChallengeGenerationCommand:
                    iAP_Auth_vfnAppleChallengeGenCommand(bpOutputBufferTwo,wpWordOutputBuffer);
                            break;
                case kiapAuthenticationAppleSignatureValueCommand:
                    iAP_Auth_vfnAppleSignatureValCommand(bpOutputBuffer,bpOutputBufferTwo,wDataBufferSize,wDataBufferTwoSize,bpOutputBufferThree);
                            break;
#endif

                default:
                    break;
            }
            executeCommand = false;
            g_authenticationCommand= kiapAuthenticationNoCommand;

        }
        else
        {
            dprint("iAP_Auth_vfnStateMachine about to sem_wait\n");
            syncStatus = osa_sem_wait(&iAP_AuthenticationTaskEvent, 0);
            dprint("iAP_Auth_vfnStateMachine after sem_wait\n");
            executeCommand = true;
        }

    } while(true);

    osa_sem_destroy(&iAP_AuthenticationTaskEvent);

    dprint("iAP_Auth_vfnStateMachine exiting\n");
}

