
#include <string.h>
#include <errno.h>

#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>

#include "i2c_commands.h"

static OsaMutex transferLock;

uint8_t i2c_commands_init(void)
{
    osa_mutex_create(&transferLock, false);

    return kStatus_I2C_Success;
}

uint8_t i2c_commands_deinit(void)
{
    osa_mutex_destroy(&transferLock);

    return kStatus_I2C_Success;
}

uint8_t i2c_commands_open(const char *master_name,
                          uint32_t **device,
                          uint32_t adapter_nr)
{
    int fd, ret = kStatus_I2C_Success;

    osa_mutex_lock(&transferLock);

    fd = open(master_name, O_RDWR);
    if (fd < 0)
    {
        ret = kStatus_I2C_Failure;
    }

    osa_mutex_unlock(&transferLock);

    *(*device) = (uint32_t) fd;

    return ret;
}

uint8_t i2c_commands_close(uint32_t device)
{
    int ret = kStatus_I2C_Success;
    int fd = (int) device;

    osa_mutex_lock(&transferLock);

    ret = close(fd);
    if (ret < 0)
        ret = kStatus_I2C_Failure;

    osa_mutex_unlock(&transferLock);

    return ret;
}

uint8_t i2c_commands_write(uint32_t master_handle,
                           uint8_t slave_addr,
                           uint8_t *regNo,
                           uint8_t regLen,
                           uint8_t *buf,
                           uint32_t len,
                           uint32_t timeOut)
{
    uint8_t i2cTransferStatus;
    int ret, fd = (int) master_handle;

    struct i2c_rdwr_ioctl_data msg_queue;
    struct i2c_msg msg;
    uint8_t *newbuf;

    i2cTransferStatus = kStatus_I2C_Success;

    osa_mutex_lock(&transferLock);

    newbuf = malloc(len + 1);
    newbuf[0] = *regNo;
    osa_memcpy(&newbuf[1], buf, len);

    msg_queue.nmsgs = 1;
    msg_queue.msgs = &msg;

    msg.len = len + 1;
    msg.addr = slave_addr;
    msg.buf = newbuf;
    msg.flags = 0;

    ret = ioctl(fd, I2C_RDWR, &msg_queue);
    if (ret < 0)
        i2cTransferStatus = kStatus_I2C_Failure;

    osa_free(newbuf);

    osa_mutex_unlock(&transferLock);

    return i2cTransferStatus;
}

uint8_t i2c_commands_read(uint32_t master_handle,
                           uint8_t slave_addr,
                           uint8_t *regNo,
                           uint8_t regLen,
                           uint8_t *buf,
                           uint32_t len,
                           uint32_t timeOut)
{
    uint8_t i2cTransferStatus;
    int ret, fd = (int) master_handle;

    struct i2c_rdwr_ioctl_data msg_queue;
    struct i2c_msg msg;

    i2cTransferStatus = kStatus_I2C_Success;

    osa_mutex_lock(&transferLock);

    msg_queue.nmsgs = 1;
    msg_queue.msgs = &msg;

    msg.len = regLen;
    msg.addr = slave_addr;
    msg.buf = regNo;
    msg.flags = 0;

    ret = ioctl(fd, I2C_RDWR, &msg_queue);
    if (ret < 0) {
        i2cTransferStatus = kStatus_I2C_Failure;

        osa_mutex_unlock(&transferLock);
        return i2cTransferStatus;
    }

    msg.len = len;
    msg.addr = slave_addr;
    msg.buf = buf;
    msg.flags = I2C_M_RD;

    usleep(10000);

    ret = ioctl(fd, I2C_RDWR, &msg_queue);
    if (ret < 0)
        i2cTransferStatus = kStatus_I2C_Failure;

    osa_mutex_unlock(&transferLock);

    return i2cTransferStatus;
}

