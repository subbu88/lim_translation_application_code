/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef CIRCULARBUFFERS_H_
#define CIRCULARBUFFERS_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "MFi_Config.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/* Macros that should be configured by the user */
/*************************************************************************************************/

/** Number of circular buffers; shouldn't be more than 20 */
#define CIRCULAR_BUFFERS_AMOUNT						4

/* Definition of each circular buffer parameter:
 * CIRCULAR_BUFFER_x_SIZE - Size in number of CIRCULAR_BUFFER_0_DATA_TYPE elements of the buffer.
 * CIRCULAR_BUFFER_0_DATA_TYPE - Data type for the buffer elements. Only uint8_t, uint16_t and uint32_t shall be used.
 * CIRCULAR_BUFFER_0_TYPE - Random access (CIRCULAR_BUFFER_RANDOM) or FIFO (CIRCULAR_BUFFER_FIFO) buffer definition.
 * CIRCULAR_BUFFER_0_RELEASE_QUEUE_SIZE - Size of the support buffer containing pending memory release requests. This
 * 										  is only valid for Random buffers. FIFO buffers should be left with 0 size. 
 */

#if CIRCULAR_BUFFERS_AMOUNT >= 1
#define CIRCULAR_BUFFER_0_SIZE                    (65525)//(512 * 8)
#define CIRCULAR_BUFFER_0_DATA_TYPE				  uint8_t
#define CIRCULAR_BUFFER_0_TYPE				  	  CIRCULAR_BUFFER_RANDOM
#define CIRCULAR_BUFFER_0_RELEASE_QUEUE_SIZE	  16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 2
#define CIRCULAR_BUFFER_1_SIZE                    (65525)//(512 * 8)
#define CIRCULAR_BUFFER_1_DATA_TYPE				  uint8_t
#define CIRCULAR_BUFFER_1_TYPE				  	  CIRCULAR_BUFFER_RANDOM
#define CIRCULAR_BUFFER_1_RELEASE_QUEUE_SIZE	  16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 3
#define CIRCULAR_BUFFER_2_SIZE                    16
#define CIRCULAR_BUFFER_2_DATA_TYPE				  uint32_t
#define CIRCULAR_BUFFER_2_TYPE				  	  CIRCULAR_BUFFER_FIFO
#define CIRCULAR_BUFFER_2_RELEASE_QUEUE_SIZE	  0
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 4
#define CIRCULAR_BUFFER_3_SIZE                    16
#define CIRCULAR_BUFFER_3_DATA_TYPE				  uint32_t
#define CIRCULAR_BUFFER_3_TYPE				  	  CIRCULAR_BUFFER_FIFO
#define CIRCULAR_BUFFER_3_RELEASE_QUEUE_SIZE	  0
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 5
#define CIRCULAR_BUFFER_4_SIZE                    16
#define CIRCULAR_BUFFER_4_DATA_TYPE				  uint32_t
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 6
#define CIRCULAR_BUFFER_5_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 7
#define CIRCULAR_BUFFER_6_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 8
#define CIRCULAR_BUFFER_7_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 9
#define CIRCULAR_BUFFER_8_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 10
#define CIRCULAR_BUFFER_9_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 11
#define CIRCULAR_BUFFER_10_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 12
#define CIRCULAR_BUFFER_11_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 13
#define CIRCULAR_BUFFER_12_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 14
#define CIRCULAR_BUFFER_13_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 15
#define CIRCULAR_BUFFER_14_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 16
#define CIRCULAR_BUFFER_15_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 17
#define CIRCULAR_BUFFER_16_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 18
#define CIRCULAR_BUFFER_17_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 19
#define CIRCULAR_BUFFER_18_SIZE                    16
#endif
#if CIRCULAR_BUFFERS_AMOUNT >= 20
#define CIRCULAR_BUFFER_19_SIZE                    16
#endif

/*************************************************************************************************/
/* Pre-defined macros for the module operation, macros below this line shouldn't be changed */
/*************************************************************************************************/

#if (CIRCULAR_BUFFERS_AMOUNT == 0 || CIRCULAR_BUFFERS_AMOUNT > 20)
#error "Amount of circular buffers declared has to be more than 0 and equal to or less than 20"
#endif

/** Circular buffer types */
#define CIRCULAR_BUFFER_RANDOM			0U
#define CIRCULAR_BUFFER_FIFO			1U

/** Macros defined to be used as masks with the bit numbers coming from the ENUM */
#define CIRCULAR_BUFFER_OK            		(0)
#define CIRCULAR_BUFFER_EMPTY               (1<<CIRCULAR_BUFFER_EMPTY_BIT)
#define CIRCULAR_BUFFER_FULL                (1<<CIRCULAR_BUFFER_FULL_BIT)
#define CIRCULAR_BUFFER_OVERFLOW            (1<<CIRCULAR_BUFFER_OVERFLOW_BIT)
#define CIRCULAR_BUFFER_DATA_READY          (1<<CIRCULAR_BUFFER_DATA_READY_BIT)
#define CIRCULAR_BUFFER_ERROR_OUT_OF_RANGE  (1<<CIRCULAR_BUFFER_ERROR_OUT_OF_RANGE_BIT)
#define CIRCULAR_BUFFER_NOT_RANDOM_BUFF  	(1<<CIRCULAR_BUFFER_NOT_RANDOM_BUFF_BIT)
#define CIRCULAR_BUFFER_LOCK_RELEASE		(1<<CIRCULAR_BUFFER_LOCK_RELEASE_BIT)

#define CIRCULAR_BUFFER_ELEM_SIZE(x)		(sizeof(x) / 2)
#define CIRCULAR_BUFFER_BYTE				CIRCULAR_BUFFER_ELEM_SIZE(uint8_t)
#define CIRCULAR_BUFFER_WORD				CIRCULAR_BUFFER_ELEM_SIZE(uint16_t)
#define CIRCULAR_BUFFER_LONG				CIRCULAR_BUFFER_ELEM_SIZE(uint32_t)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
/** Struct used to get information from each Buffer declarated */
typedef struct
{
	uint16_t wReadIndex;
	uint16_t wWriteIndex;
	uint8_t  bReleaseCount;  /* Holds the count of pending releases for Random buffers */
	uint8_t  bStatus;		/* Contains buffer full, empty or overflown flags */
}sCircularBufferInformation;

typedef struct
{
	uint16_t wSize;
	uint16_t wIndex;
}sCircularBufferReleaseRecord;

/** Status Bits for each Circular Buffer*/
typedef enum
{
	CIRCULAR_BUFFER_EMPTY_BIT = 0,
	CIRCULAR_BUFFER_FULL_BIT,
	CIRCULAR_BUFFER_OVERFLOW_BIT,
	CIRCULAR_BUFFER_DATA_READY_BIT,
	CIRCULAR_BUFFER_ERROR_OUT_OF_RANGE_BIT,
	CIRCULAR_BUFFER_NOT_RANDOM_BUFF_BIT,
	CIRCULAR_BUFFER_LOCK_RELEASE_BIT
}CIRCULAR_BUFFER_STATUS_BITS;

#ifndef IAPI_INCOMING_MAX_PAYLOAD_SIZE
#define IAPI_INCOMING_MAX_PAYLOAD_SIZE			128
#endif

typedef union
{
#ifdef MFI_IAP2
  #ifdef MFI_IAP1
    #if (CIRCULAR_BUFFER_0_SIZE + CIRCULAR_BUFFER_1_SIZE + CIRCULAR_BUFFER_2_SIZE + CIRCULAR_BUFFER_3_SIZE) > ((IAPI_INCOMING_MAX_PAYLOAD_SIZE + 12) * 3)
        uint8_t baCommMemory[CIRCULAR_BUFFER_0_SIZE + CIRCULAR_BUFFER_1_SIZE + CIRCULAR_BUFFER_2_SIZE + CIRCULAR_BUFFER_3_SIZE];
    #else
        uint8_t baCommMemory[(IAPI_INCOMING_MAX_PAYLOAD_SIZE + 12) * 3];
    #endif
  #else
        uint8_t baCommMemory[CIRCULAR_BUFFER_0_SIZE + CIRCULAR_BUFFER_1_SIZE + CIRCULAR_BUFFER_2_SIZE + CIRCULAR_BUFFER_3_SIZE];
  #endif
#else
    uint8_t baCommMemory[(IAPI_INCOMING_MAX_PAYLOAD_SIZE + 12) * 3];        
#endif

#ifdef MFI_IAP2
	struct
	{
		CIRCULAR_BUFFER_0_DATA_TYPE CircularBuffer0[CIRCULAR_BUFFER_0_SIZE];
		CIRCULAR_BUFFER_1_DATA_TYPE CircularBuffer1[CIRCULAR_BUFFER_1_SIZE];
		CIRCULAR_BUFFER_2_DATA_TYPE CircularBuffer2[CIRCULAR_BUFFER_2_SIZE];
		CIRCULAR_BUFFER_3_DATA_TYPE CircularBuffer3[CIRCULAR_BUFFER_3_SIZE];
	}iAP2_Buffers;
#endif

#ifdef MFI_IAP1
	struct
	{
		uint8_t iAP1_gbaReceptionBufferEven[IAPI_INCOMING_MAX_PAYLOAD_SIZE + 12];
		uint8_t iAP1_gbaReceptionBufferOdd[IAPI_INCOMING_MAX_PAYLOAD_SIZE + 12];
		uint8_t iAP1_gbaTransmitBuffer[IAPI_INCOMING_MAX_PAYLOAD_SIZE + 12];
	}iAP1_Buffers;
#endif
}MFI_COMM_BUFFERS;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern MFI_COMM_BUFFERS	MFI_CommBuffers;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
#ifdef MFI_IAP2
void CircularBuffersInit(void);

/* FIFO Buffer Interface */
uint8_t CircularBufferFIFOReadData(void* vpReadPtr, uint8_t bBufferIndex);
uint8_t CircularBufferFIFOWriteData(void *vpWriteData, uint8_t bBufferIndex);

/* Random buffer interface */
uint8_t CircularBufferRandomReadData(void* vpReadPtr, uint16_t *wReadIndex, uint8_t bBufferIndex);
uint8_t CircularBufferRandomWriteData(void *vpWriteData, uint16_t *wWriteIndex, uint8_t bBufferIndex);
uint8_t CircularBufferRandomReadBuffer(void* vpReadPtr,uint16_t *wReadIndex ,uint16_t wReadSize, uint8_t bBufferIndex);
uint8_t CircularBufferRandomWriteBuffer(void* vpWritePtr,uint16_t *wWriteIndex, uint16_t wWritesize, uint8_t bBufferIndex);
uint8_t CircularBufferGetWriteIndex(uint16_t *wpWriteIndex, uint8_t bBufferIndex);
uint8_t CircularBufferGetReadIndex(uint16_t *wpReadIndex, uint8_t bBufferIndex);
uint8_t CircularBufferGetPtr(void **bppBufferPtr, uint8_t bBufferIndex, uint16_t wIndex);

uint16_t CircularBufferGetSpaceAvailable(uint8_t bBufferIndex);
uint16_t CircularBufferGetBufferSize(uint8_t bBufferIndex);

uint8_t CircularBufferReserveSpace(uint8_t bBufferIndex, uint16_t wSize);
uint8_t CircularBufferReleaseSpace(uint8_t bBufferIndex, uint16_t wIndex, uint16_t wSize);
uint8_t CircularBufferDiscardData(uint16_t wSize, uint8_t bBufferIndex);

void CircularBufferSetReleaseLockFlag(uint8_t bBufferIndex);
void CircularBufferClearReleaseLockFlag(uint8_t bBufferIndex);
#endif
/*************************************************************************************************/

#endif /* CIRCULARBUFFERS_H_ */
