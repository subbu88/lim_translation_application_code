/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_TIMER_H_
#define IAP_TIMER_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"


/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP_TIMER_MAX_CHN                   (11)
#define IAP_TIMER_SET_PERIOD(a,b)			iAP_Timer_gwaAuxTimers[a] = (uint16_t)(b)
#define IAP_TIMER_GET_VALUE(a)				iAP_Timer_gwaAuxTimers[a]
#define IAP_TIMER_CHECK_CHN_FLAG(x)			((!iAP_Timer_gwaAuxTimers[x]) \
											&& (iAP_Timer_ChnEnabler & (1 << x)))
#define IAP_TIMER_HAS_EXPIRED               IAP_TIMER_CHECK_CHN_FLAG
#define IAP_TIMER_ENABLE_CHN(x)				(iAP_Timer_ChnEnabler |= (1 << x))
#define IAP_TIMER_DISABLE_CHN(x)			(iAP_Timer_ChnEnabler &= ~(1 << x))
#define IAP_TIMER_CHN_ENABLER				(iAP_Timer_ChnEnabler)
#define IAP_TIMER_IS_CHN_ENABLED(x)			(iAP_Timer_ChnEnabler & (1<<x))

#define IAP_TIMER_TICK						(1)		/* 1, 2, 5, or 10 milliseconds */

#define IAP_TIMEOUT_1MS_PERIOD				(1)
#if(1 == IAP_TIMER_TICK)
#define IAP_TIMEOUT_2MS_PERIOD				(2)
#else
#define IAP_TIMEOUT_2MS_PERIOD				(1)
#endif
#if(5 <= IAP_TIMER_TICK)
#define IAP_TIMEOUT_4MS_PERIOD				(1)
#else
#define IAP_TIMEOUT_4MS_PERIOD				(4 / IAP_TIMER_TICK)
#endif
#define IAP_TIMEOUT_10MS_PERIOD				(10 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_14MS_PERIOD				(14 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_20MS_PERIOD				(20 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_30MS_PERIOD				(30 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_40MS_PERIOD				(40 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_50MS_PERIOD				(50 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_100MS_PERIOD			(100 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_200MS_PERIOD            (200 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_500MS_PERIOD			(500 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_550MS_PERIOD            (550 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_1SEC_PERIOD				(1000 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_2SEC_PERIOD				(2000 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_5SEC_PERIOD				(5050 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_10SEC_PERIOD			(10000 / IAP_TIMER_TICK)
#define IAP_TIMEOUT_65SEC_PERIOD			(65000 / IAP_TIMER_TICK)

#define IAPH_IOS_DETECT_CHN					(0)
#define IAPH_CMD_ACK_TIMEOUT_CHN			(1)
#define IAPH_CMD_REPEAT_PERIOD_CHN			(2)
#define IAPH_COPROCESSOR_TIMEOUT_CHN		(3)
#define IAPH_COMPLETE_FRAME_TIMEOUT_CHN		(4)
#define IAPH_WAITING_IOS_COMM_CHN			(5)
#define DAD_TIMEOUT_CHN						(6)
#define IAP2_LINK_SENT_ACK_TIMER			(7)
#define IAP_DESCOMPENSATION_SENSE_TIMER		(8)
#define IAP_WAIT_LINK_INIT_TIMER            (9)
#define TCO_TIMEOUT_CHN					(10)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern uint16_t iAP_Timer_gwaAuxTimers[];
extern uint16_t iAP_Timer_gwGeneralTimer;

#if (IAP_TIMER_MAX_CHN <= 8)
	extern uint8_t iAP_Timer_ChnEnabler;
#else
	#if (IAP_TIMER_MAX_CHN <= 16)
		extern uint16_t iAP_Timer_ChnEnabler;
	#else
		#if (IAP_TIMER_MAX_CHN <= 32)
			extern uint32_t iAP_Timer_ChnEnabler;
		#else
			#error "Cannot handle more than 32 auxiliary timer channels"
		#endif
	#endif
#endif

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void iAP_Timer_vfnRefresh(void);

/*************************************************************************************************/

#endif /* IAP_TIMER_H_ */
