/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_AUTHENTICATION_H_
#define IAP_AUTHENTICATION_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "Events.h"
#include "iAP_Timer.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP_ACC_AUTHENTICATION_ENABLE				(_TRUE_)
#define IAP_IOS_AUTHENTICATION_ENABLE				(_FALSE_)

#define IAP_AUTH_SLAVE_ADDRESS						(0x11)
#define IAP_AUTH_COMM_BUSY							(IIC_RESERVED)
#define IAP_AUTH_CHECK_COMM_ERROR					(IIC_DRIVER_ERROR || IIC_NAK_ERROR_FLAG)
#define IAP_AUTH_CLEAR_COMM_ERROR					IIC_CLEAR_DRIVER_ERROR; IIC_CLEAR_NAK_ERROR_FLAG 
#define IAP_AUTH_SET_COMM_BUSY						IIC_SET_DRIVER_BUSY

#define IAP_AUTH_APPLE_CERT_RESULT_OK				(0x00u) 
#define IAP_AUTH_APPLE_SIGN_RESULT_OK				(0x00u)
#define IAP_AUTH_APPLE_CERT_RESULT_ERR				(0xFFu)
#define IAP_AUTH_APPLE_SIGN_RESULT_ERR				(0xFFu)

#define IAP_AUTH_WAIT_CP_TIMEOUT_CHN				(0)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/

typedef enum eAppleCPRegisters
{
	DEVICE_VERSION = 0,									/*0x00*/
	FIRMWARE_VERSION,									/*0x01*/
	AUTHENTICATION_PROTOCOL_MAJOR_VERSION,				/*0x02*/
	AUTHENTICATION_PROTOCOL_MINOR_VERSION,				/*0x03*/
	DEVICE_ID,											/*0x04*/
	ERROR_CODE,											/*0x05*/
	NOT_USED_06,										/*0x06*/
	NOT_USED_07,										/*0x07*/
	NOT_USED_08,										/*0x08*/
	NOT_USED_09,										/*0x09*/	
	NOT_USED_0A,										/*0x0A*/	
	NOT_USED_0B,										/*0x0B*/
	NOT_USED_0C,										/*0x0C*/
	NOT_USED_0D,										/*0x0D*/
	NOT_USED_0E,										/*0x0E*/
	NOT_USED_0F,										/*0x0F*/
	AUTHENTICATION_CONTROL_AND_STATUS,					/*0x10*/
	SIGNATURE_DATA_LENGTH,								/*0x11*/
	SIGNATURE_DATA,										/*0x12*/
	NOT_USED_13,										/*0x13*/
	NOT_USED_14,										/*0x14*/
	NOT_USED_15,										/*0x15*/
	NOT_USED_16,										/*0x16*/
	NOT_USED_17,										/*0x17*/
	NOT_USED_18,										/*0x18*/
	NOT_USED_19,										/*0x19*/
	NOT_USED_1A,										/*0x1A*/
	NOT_USED_1B,										/*0x1B*/
	NOT_USED_1C,										/*0x1C*/
	NOT_USED_1D,										/*0x1D*/
	NOT_USED_1E,										/*0x1E*/
	NOT_USED_1F,										/*0x1F*/
	CHALLENGE_DATA_LENGTH,								/*0x20*/
	CHALLENGE_DATA,										/*0x21*/
	NOT_USED_22,										/*0x22*/
	NOT_USED_23,										/*0x23*/
	NOT_USED_24,										/*0x24*/
	NOT_USED_25,										/*0x25*/
	NOT_USED_26,										/*0x26*/
	NOT_USED_27,										/*0x27*/
	NOT_USED_28,										/*0x28*/
	NOT_USED_29,										/*0x29*/
	NOT_USED_2A,										/*0x2A*/
	NOT_USED_2B,										/*0x2B*/
	NOT_USED_2C,										/*0x2C*/
	NOT_USED_2D,										/*0x2D*/
	NOT_USED_2E,										/*0x2E*/
	NOT_USED_2F,										/*0x2F*/
	ACCESSORY_CERTIFICATE_DATA_LENGTH,					/*0x30*/
	ACCESSORY_CERTIFICATE_DATA_P1,						/*0x31*/
	ACCESSORY_CERTIFICATE_DATA_P2,						/*0x32*/
	ACCESSORY_CERTIFICATE_DATA_P3,						/*0x33*/
	ACCESSORY_CERTIFICATE_DATA_P4,						/*0x34*/
	ACCESSORY_CERTIFICATE_DATA_P5,						/*0x35*/
	ACCESSORY_CERTIFICATE_DATA_P6,						/*0x36*/
	ACCESSORY_CERTIFICATE_DATA_P7,						/*0x37*/
	ACCESSORY_CERTIFICATE_DATA_P8,						/*0x38*/
	ACCESSORY_CERTIFICATE_DATA_P9,						/*0x39*/
	ACCESSORY_CERTIFICATE_DATA_P10,						/*0x3A*/
	NOT_USED_3B,										/*0x3B*/
	NOT_USED_3C,										/*0x3C*/
	NOT_USED_3D,										/*0x3D*/
	NOT_USED_3E,										/*0x3E*/
	NOT_USED_3F,										/*0x3F*/
	SELF_TEST_CONTROL_AND_STATUS,						/*0x40*/
	RESERVED_41,										/*0x41*/
	RESERVED_42,										/*0x42*/
	RESERVED_43,										/*0x43*/
	RESERVED_44,										/*0x44*/
	RESERVED_45,										/*0x45*/
	RESERVED_46,										/*0x46*/
	RESERVED_47,										/*0x47*/
	RESERVED_48,										/*0x48*/
	RESERVED_49,										/*0x49*/
	RESERVED_4A,										/*0x4A*/
	RESERVED_4B,										/*0x4B*/
	RESERVED_4C,										/*0x4C*/
	SYSTEM_EVENT_COUNTER,								/*0x4D*/
	RESERVED_4E,										/*0x4E*/
	RESERVED_4F, 										/*0x4F*/
	APPLE_DEVICE_CERTIFICATE_DATA_LENGTH,				/*0x50*/
	APPLE_DEVICE_CERTIFICATE_DATA_P1,					/*0x51*/
	APPLE_DEVICE_CERTIFICATE_DATA_P2,					/*0x52*/
	APPLE_DEVICE_CERTIFICATE_DATA_P3,					/*0x53*/
	APPLE_DEVICE_CERTIFICATE_DATA_P4,					/*0x54*/
	APPLE_DEVICE_CERTIFICATE_DATA_P5,					/*0x55*/
	APPLE_DEVICE_CERTIFICATE_DATA_P6,					/*0x56*/
	APPLE_DEVICE_CERTIFICATE_DATA_P7,					/*0x57*/
	APPLE_DEVICE_CERTIFICATE_DATA_P8					/*0x58*/
}eAppleCPRegisters;

enum AUTH_STATUS_FLAGS
{
	AUTH_BUSY = 0,				/* 0x00 */
	AUTH_ERROR,					/* 0x01 */
	AUTH_WRITE,					/* 0x02 */
	AUTH_READ,					/* 0x03 */
	AUTH_SIGNATURE_ERROR		/* 0x04 */
};

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/
#define IAP_AUTH_CP_BUSY					(iAP_Auth_gu32Status & (1 << AUTH_BUSY))
#define IAP_AUTH_CP_ERROR					(iAP_Auth_gu32Status & (1 << AUTH_ERROR))

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

extern uint32_t iAP_Auth_gu32Status;
extern uint16_t iAP_Auth_gwCertificateSize;
extern uint8_t iAP_Auth_gbaRxBufferForCP[128];			/**< Buffer used to read and write data from-to CP */
extern uint8_t iAP_Auth_gbaCP_VersionAndID[6];			/**< Buffer to read current CP version and Device ID*/
extern uint8_t iAP_Auth_CPCertificate[1284];

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void iAP_Auth_vfnInit(void);
void iAP_Auth_vfnCancelCommand(void);
void iAP_Auth_vfnReadProtVerAndDevID(uint8_t *pu8DataBuffer);
void iAP_Auth_vfnReadAccessoryCertificate(uint8_t *pu8RegisterData, uint16_t *wpCertificateLength);
void iAP_Auth_vfnAccessoryChallenge(uint8_t *pu8ChallengeData, uint8_t *pu8SignatureData, uint16_t wChallangeLenght, uint16_t *wpSignatureLength);
void iAP_Auth_vfnAppleCertificateVal(uint8_t *bpAppleCertificateData, uint8_t *bpAppleCertificateResult, uint16_t wAppleCertificateLenght);
void iAP_Auth_vfnAppleChallengeGen(uint8_t *bpAppleChallangeData, uint16_t *wpAppleChallangeLength);
void iAP_Auth_vfnAppleSignatureVal(uint8_t *bpAppleChallangeData, uint8_t *bpAppleSignature, uint16_t wAppleChallengeLength, uint16_t wAppleSignatureLength, uint8_t *bAppleSignaturResult);
void iAP_Auth_vfnReadRegister(uint8_t u8Register, uint8_t *pu8DataBuffer, uint16_t u16Datasize);
void iAP_Auth_vfnWriteRegister(uint8_t u8Register, uint8_t *pu8RegisterData, uint16_t u16Datasize);
void iAP_Auth_vfnStateMachine(void);
/*************************************************************************************************/

#endif /* IAP_AUTHENTICATION_H_ */
