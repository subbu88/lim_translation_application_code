/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
* FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
* file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

#ifndef BUFFERSALLOC_H_
#define BUFFERSALLOC_H_

/*************************************************************************************************
*                                      Includes Section                                         
*************************************************************************************************/
#include <stdint.h>

/* @addtogroup services_layer Services Layer Components
    @{ 

    @addtogroup buffers_alloc Buffers Memory Allocation service
    @brief Module designed to reserve and release memory buffers as a service for other software
           layers that require to store information. 
    @details This module provides similar functionality as a malloc and free with the difference
             that its performance will be deterministic and similar across architectures and 
             compilers. It uses a global memory buffer for each requested buffer divided into
             a set of sub-buffers (msgs) if needed. If not, sub buffers amount shall be set to 1.  
    @{
*/

/*************************************************************************************************
*                                  Defines & Macros Section                                     
*************************************************************************************************/
/* this parameter is defined into IAP_Interface.h */

#define BUFFERS_EMPTY                   (0x00)  /* empty value for every buffer */

#define BUFFERS_AMOUNT                  (2)     /* number of buffers to use */
/* Definition of each buffer parameter:
 * BUFFER_X_SIZE        - Size of the buffer
 * BUFFER_X_FULL        - Maximum value of packets counted into a byte (like bit flags)
 * BUFFER_X_MSG_NUM     - Number of subbuffers created into buffer x
 * BUFFER_X_MSG_SIZE    - size of each subbuffer
 */

#if (BUFFERS_AMOUNT == 0 || BUFFERS_AMOUNT > 4)
#error "Amount of buffers declared has to be more than 0 and equal to or less than 4"
#endif

#if BUFFERS_AMOUNT >= 1
#define BUFFER_0_SIZE                   (65535)//(4096)
#define BUFFER_0_FULL                   ((1<<BUFFER_0_SUBBUFFERS_AMOUNT) - 1)
#define BUFFER_0_SUBBUFFERS_AMOUNT      (4)
#define BUFFER_0_MSG_SIZE               (BUFFER_0_SIZE / BUFFER_0_SUBBUFFERS_AMOUNT)
#endif
#if BUFFERS_AMOUNT >= 2
#define BUFFER_1_SIZE                   (65535)//(4096)
#define BUFFER_1_FULL                   ((1<<BUFFER_1_SUBBUFFERS_AMOUNT) - 1)
#define BUFFER_1_SUBBUFFERS_AMOUNT      (4)
#define BUFFER_1_MSG_SIZE               (BUFFER_1_SIZE / BUFFER_1_SUBBUFFERS_AMOUNT)
#endif
#if BUFFERS_AMOUNT >= 3
#define BUFFER_2_SIZE                   (512)
#define BUFFER_2_FULL                   ((1<<BUFFER_2_SUBBUFFERS_AMOUNT) - 1)
#define BUFFER_2_SUBBUFFERS_AMOUNT      (4)
#define BUFFER_2_MSG_SIZE               (BUFFER_2_SIZE / BUFFER_2_SUBBUFFERS_AMOUNT)
#endif
#if BUFFERS_AMOUNT >= 4
#define BUFFER_3_SIZE                   (2024)
#define BUFFER_3_FULL                   ((1<<BUFFER_3_SUBBUFFERS_AMOUNT) - 1)
#define BUFFER_3_SUBBUFFERS_AMOUNT      (4)
#define BUFFER_3_MSG_SIZE               (BUFFER_3_SIZE / BUFFER_3_SUBBUFFERS_AMOUNT)
#endif

/*************************************************************************************************
*                                      Typedef Section                                          
*************************************************************************************************/
/*! Buffer Status */
typedef enum
{
    /*! Space is available in Buffer */
    kBufferOk = 0,
    /*! Buffer is empty */
    kBufferEmpty,
    /*! Space to be erased is out of range */
    kBufferErrorOutOfRange,
    /*! If any address is not a start of a reserved block */
    kBufferErrorPointer
}bufferStatus_t;


/*! Information of each buffer */
typedef struct
{
    /*! Pointer of buffer's available address  */
    uint8_t*    availablePosition;
    /*! Value of buffer's status  */
    uint8_t        bufferStatus;
    /*! Flag of buffer's busy and available message  */
#if ((BUFFER_0_SUBBUFFERS_AMOUNT <= 8)\
        && (BUFFER_1_SUBBUFFERS_AMOUNT <= 8)\
        && (BUFFER_2_SUBBUFFERS_AMOUNT <= 8)\
        && (BUFFER_3_SUBBUFFERS_AMOUNT <= 8))
    uint8_t        msgStatusRegister;
#elif ((BUFFER_0_SUBBUFFERS_AMOUNT <= 16)\
        && (BUFFER_1_SUBBUFFERS_AMOUNT <= 16)\
        && (BUFFER_2_SUBBUFFERS_AMOUNT <= 16)\
        && (BUFFER_3_SUBBUFFERS_AMOUNT <= 16))
    uint16_t    msgStatusRegister;
#elif ((BUFFER_0_SUBBUFFERS_AMOUNT <= 32)\
        && (BUFFER_1_SUBBUFFERS_AMOUNT <= 32)\
        && (BUFFER_2_SUBBUFFERS_AMOUNT <= 32)\
        && (BUFFER_3_SUBBUFFERS_AMOUNT <= 32))
    uint32_t     msgStatusRegister;
#else
#error "invalid number of message. The maximum value should be 32"
#endif
}bufferInformation_t;

typedef struct
{
#if BUFFERS_AMOUNT >= 1
    uint8_t    buffer0[BUFFER_0_SIZE];
#endif
#if BUFFERS_AMOUNT >= 2
    uint8_t    buffer1[BUFFER_1_SIZE];
#endif
#if BUFFERS_AMOUNT >= 3
    uint8_t buffer2[BUFFER_2_SIZE];
#endif
#if BUFFERS_AMOUNT >= 4
    uint8_t buffer3[BUFFER_3_SIZE];
#endif
}iAP1_buffers_t;

extern iAP1_buffers_t       g_iapInterfaceBuffers;

/*************************************************************************************************
*                                Function Prototypes Section                                    
*************************************************************************************************/

/*!
* * @brief Reserve a message space to save the incoming or outgoing frame.
* @param    BufferIndex        Buffer number [0,1 - ReceptionBuffer or TransmissionBuffer] 
*                                 from where the message will be saved.
* @param    SizeOfMessage    Size of message to save.
* @return                    Pointer from where space is available to save the message.
*/
uint8_t*    buffersAlloc_reserveSpace(uint8_t bufferIndex, uint16_t sizeOfMessage);

/*!
* @brief    Releases message space from a buffer indicated. 
* @param    BufferIndex:    Buffer number [0,1 - ReceptionBuffer or TransmissionBuffer] 
*                                 from where the message will be released.
* @param    ReleaseIndex:    Pointer from where the message will be released; 
*                                 Normally returned by Buffer_ReserveSpace function.
* @return    Status of buffer.
* @retval    kBufferEmpty    Doesn't exist any message to release.
* @retval    BufferOk        Exist space available in buffer, 
*                           but the buffer has still message to release.
* @retval    kBufferErrorOutOfRange    The pointer not is in range of buffer indicated.
*/
uint8_t     buffersAlloc_releaseSpace(uint8_t bufferIndex, uint8_t* releaseAddress);

/*!
* @brief    Clear a specific buffer status flags to release all occupied messages and 
*           reset the buffer pointer availability
* @param    BufferIndex     Buffer number [0,1, ... BUFFERS_AMOUNT] 
*                               from where the message will be saved.
* @return   Status if the reset status was successful or not
* @retval   BufferOk        Buffer status flags were cleared OK
* @retval   kBufferErrorOutOfRange  Buffer requested doesn't exist
*/
uint8_t buffersAlloc_resetBufferStatus(uint8_t bufferIndex);

/*!
* @brief    Clear buffer status flags to release all occupied messages for all buffers
*           and reset the buffer pointer availability
*/
void buffersAlloc_resetAllBuffersStatus(void);

/*! @}  Buffers Alloc group */

/*! @} Services layer group */
#endif /* MALLOCBUFFERS_H_ */
