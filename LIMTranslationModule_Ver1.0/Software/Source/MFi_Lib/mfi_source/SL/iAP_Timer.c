/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP_Timer.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
uint16_t iAP_Timer_gwaAuxTimers[IAP_TIMER_MAX_CHN];
uint16_t iAP_Timer_gwGeneralTimer=0;
#if (IAP_TIMER_MAX_CHN <= 8)
	uint8_t iAP_Timer_ChnEnabler = 0;
#else
	#if (IAP_TIMER_MAX_CHN <= 16)
		uint16_t iAP_Timer_ChnEnabler = 0;
	#else
		#if (IAP_TIMER_MAX_CHN <= 32)
			uint32_t iAP_Timer_ChnEnabler = 0;
		#else
			#error "Cannot handle more than 32 auxiliary timer channels"
		#endif
	#endif
#endif


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

void iAP_Timer_vfnRefresh(void)
{
    uint8_t lbIndex = IAP_TIMER_MAX_CHN;
    iAP_Timer_gwGeneralTimer++;
    while(lbIndex--)
    {
    	if(iAP_Timer_ChnEnabler & (1 << lbIndex))
    	{
			if(iAP_Timer_gwaAuxTimers[lbIndex])
			{
				iAP_Timer_gwaAuxTimers[lbIndex]--;
			}
    	}
    }
}
