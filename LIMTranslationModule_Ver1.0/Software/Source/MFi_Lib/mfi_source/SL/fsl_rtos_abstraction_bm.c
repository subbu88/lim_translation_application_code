/*
 * Copyright (c) 2013, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************************* 
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 

 */

#include <assert.h>
#include "fsl_rtos_abstraction_bm.h"
#include "sw_timer.h"

//! @addtogroup rtos_irq_sync
//! @{

////////////////////////////////////////////////////////////////////////////////
// Declarations
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// API
////////////////////////////////////////////////////////////////////////////////

//TODO change false for FALSE
//TODO  change true for TRUE

// ----------- Sync services --------------
fsl_rtos_status __sync_create(sync_object_t *obj) //TODO added __
{
    obj->semCount = 0;
    obj->isWaiting = FALSE;
    obj->timerId = sw_timer_reserve_channel();
    
    return kSuccess;
}

fsl_rtos_status sync_wait(sync_object_t *obj, uint32_t timeout)
{
    fsl_rtos_status retVal = kIdle;
    
    /* Always check for the event first. Deal with timeout only if not already set */
    if(obj->semCount)
    {
        /* Event set. Clear it and return success */
        interrupt_disable_global();
        obj->semCount = 0;
        obj->isWaiting = FALSE;
        interrupt_enable_global();
        retVal = kSuccess;
    }
    else
    {
        if(obj->isWaiting)
        {
            /* Check for timeout */
            uint32_t status;
            
            status = sw_timer_get_channel_status(obj->timerId);
            if(status == kSwTimerChannelExpired)
            {
                interrupt_disable_global();
                obj->isWaiting = FALSE;
                interrupt_enable_global();
                retVal = kTimeout;
            }
        }
        else
        {
            /* Start the timeout counter */
            interrupt_disable_global();
            obj->isWaiting = TRUE;
            interrupt_enable_global();
            sw_timer_start_channel(obj->timerId, timeout);
        }
    }
    
    return retVal;
}

fsl_rtos_status sync_poll(sync_object_t *obj)
{
    fsl_rtos_status retVal;
        
    /* Always check for the event first. Deal with timeout only if not already set */
    if(obj->semCount)
    {
        /* Event set. Clear it and return success */
        interrupt_disable_global();
        obj->semCount = 0;
        obj->isWaiting = FALSE;
        interrupt_enable_global();
        retVal = kSuccess;
    }
    else
    {
        retVal = kTimeout;
    }
    
    return retVal;
}

fsl_rtos_status sync_signal(sync_object_t *obj)
{
    interrupt_disable_global();
    ++obj->semCount;
    interrupt_enable_global();
    
    return kSuccess;
}

// ----------- Lock services --------------
void __lock_create(lock_object_t *obj)
{
    obj->isLocked = FALSE;
    obj->isWaiting = FALSE;
    obj->timerId = sw_timer_reserve_channel();
}

fsl_rtos_status lock_wait(lock_object_t *obj, uint32_t timeout)
{
    fsl_rtos_status retVal;
    
    /* Always check for the event first. Deal with timeout only if not already set */
    if(obj->isLocked == FALSE)
    {
        /* Event set. Clear it and return success */
        interrupt_disable_global();
        obj->isLocked = TRUE;
        obj->isWaiting = FALSE;
        interrupt_enable_global();
        retVal = kSuccess;
    }
    else
    {
        retVal = kIdle;
        
        if(obj->isWaiting)
        {
            /* Check for timeout */
            uint32_t status;
            
            status = sw_timer_get_channel_status(obj->timerId);
            if(status == kSwTimerChannelExpired)
            {
                interrupt_disable_global();
                obj->isWaiting = FALSE;
                interrupt_enable_global();
                retVal = kTimeout;
            }
        }
        else
        {
            /* Start the timeout counter */
            interrupt_disable_global();
            obj->isWaiting = TRUE;
            interrupt_enable_global();
            sw_timer_start_channel(obj->timerId, timeout);
        }
    }
    
    return retVal;
}

fsl_rtos_status lock_poll(lock_object_t *obj)
{
    fsl_rtos_status retVal;
        
    /* Always check for the event first. Deal with timeout only if not already set */
    if(obj->isLocked == FALSE)
    {
        /* Event set. Clear it and return success */
        interrupt_disable_global();
        obj->isLocked = TRUE;
        obj->isWaiting = FALSE;
        interrupt_enable_global();
        retVal = kSuccess;
    }
    else
    {
        retVal = kTimeout;
    }
    
    return retVal;
}

fsl_rtos_status lock_release(lock_object_t *obj)
{
    interrupt_disable_global();
    obj->isLocked = FALSE;
    interrupt_enable_global();
    
    return kSuccess;
}

// ----------- Event services --------------

fsl_rtos_status event_create(event_object_t *obj, event_clear_type clearType)
{
    (void)sync_create(obj->eventSync);  // TODO remove &
    obj->flags = 0;
    obj->clearType = clearType;
    
    return kSuccess;
}

fsl_rtos_status event_wait(event_object_t *obj, uint32_t timeout, event_group_t *setFlags)
{
    fsl_rtos_status retVal;
    
    retVal = sync_wait(&obj->eventSync, timeout);
    
    if(retVal == kSuccess)
    {
        interrupt_disable_global();
        *setFlags = obj->flags;
        
        if(kEventAutoClr == obj->clearType)
        {
            obj->flags = 0;
        }
        interrupt_enable_global();
    }
    
    return retVal;
}

fsl_rtos_status event_set(event_object_t *obj, event_group_t flags)
{
    /* Set flags ensuring atomic operation */
    interrupt_disable_global();
    obj->flags |= flags;
    interrupt_enable_global();
    
    return sync_signal(&obj->eventSync);
}

fsl_rtos_status event_clear(event_object_t *obj, event_group_t flags)
{
    /* Clear flags ensuring atomic operation */
    interrupt_disable_global();
    obj->flags &= ~flags;
    interrupt_enable_global();
    
    return kSuccess;
}
// TODO comment this out because of redefinition error
//// ---------- Message Queues ---------------
//msg_queue_handler_t __msg_queue_create(msg_queue_t *queue, uint16_t number, uint16_t size, void * mem)
//{
//    queue->queueMem = mem;
//    queue->number = number;
//    queue->size = size;
//    queue->head = 0;
//    queue->tail = 0;
//    queue->isEmpty = TRUE;
//    
//    sync_create(queue->queueSync);
//                                                  
//    return queue;
//}
//
//fsl_rtos_status msg_queue_put(msg_queue_handler_t handler, msg_queue_item_t item)
//{
//    fsl_rtos_status retVal = kError;
//    
//    /* Check that there is room in the queue */
//    if((handler->tail != handler->head) || handler->isEmpty)
//    {
//        rtos_enter_critical();
//#if (__FSL_RTOS_MSGQ_COPY_MSG__)
//        uint32_t *src = (uint32_t*)item;
//        uint32_t *dst = &handler->queueMem[handler->tail * handler->size];
//        uint16_t msgSize;
//        
//        /* Copy entire message into the queue, based on the size configured at creation */
//        msgSize = handler->size;
//        while(msgSize--)
//        {
//           *dst++ = *src++; 
//        }
//#else
//        /* Store the pointer to the message in the queue */
//        handler->queueMem[handler->tail] = item;
//#endif
//        
//        /* Adjust tail pointer and wrap in case the end of the buffer is reached */
//        ++handler->tail;
//        if(handler->tail == handler->number)
//        {
//            handler->tail = 0;
//        }
//        
//        /* If queue was empty, clear the empty flag and signal that it is not empty anymore */
//        if(handler->isEmpty)
//        {
//            handler->isEmpty = FALSE;
//            sync_signal(handler->queueSync);
//        }
//        rtos_exit_critical();
//
//        retVal = kSuccess;
//    }
//
//    return retVal;
//}
//
//fsl_rtos_status msg_queue_get(msg_queue_handler_t handler, msg_queue_item_t *item, uint32_t timeout)
//{
//    fsl_rtos_status retVal = kError;
//    
//    rtos_enter_critical();
//    /* Check if the queue is not empty */
//    if(!handler->isEmpty)
//    {
//#if (__FSL_RTOS_MSGQ_COPY_MSG__)
//        uint32_t *src = &handler->queueMem[handler->head * handler->size];
//        uint32_t *dst = (uint32_t*)item;
//        uint16_t msgSize;
//        
//        /* Copy entire message into the queue, based on the size configured at creation */
//        msgSize = handler->size;
//        while(msgSize--)
//        {
//           *dst++ = *src++; 
//        }
//#else
//        /* Store the pointer to the message in the queue */
//        *item = handler->queueMem[handler->head];
//#endif
//        
//        /* Adjust head pointer and wrap in case the end of the buffer is reached */
//        ++handler->head;
//        if(handler->head == handler->number)
//        {
//            handler->head = 0;
//        }
//        
//        /* If queue was empty, clear the empty flag and signal that it is not empty anymore */
//        if(handler->head == handler->tail)
//        {
//            handler->isEmpty = TRUE;
//        }
//        rtos_exit_critical();
//        
//        /* Poll the sync object to consume it in case it has been signalled before */
//        (void)sync_poll(handler->queueSync);
//
//        retVal = kSuccess;
//    
//    }
//    else
//    {
//        /* Wait for the semaphore if the queue was empty */
//        retVal = sync_wait(&handler->queueSync, timeout);
//        rtos_exit_critical();
//    }
//    
//    return retVal;
//}
//
//static inline fsl_rtos_status msg_queue_flush(msg_queue_handler_t handler)
//{
//    /* Reset indexes, set status to empty and consume semaphore in case it was set before */
//    handler->head = 0;
//    handler->tail = 0;
//    handler->isEmpty = TRUE;
//    sync_poll(&handler->queueSync);
//    
//    return kSuccess;
//}

////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
