/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale _PROJECT_ Software License. See  
* the FREESCALE_PROJECT_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

#ifndef I2C_COMMANDS_H_
#define I2C_COMMANDS_H_

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Includes Section                                         
///////////////////////////////////////////////////////////////////////////////////////////////////
#include "osa_common.h"

#if (FSL_OS_SELECTED == FSL_OS_MQX)
#include "bsp.h"
#include "i2c.h"
#endif

//! @addtogroup SoftwareLayer(HAL,HIL,SL or Application)
//! @brief SoftwareLayer brief description 
//!                         (This brief must be written only in one file per software layer)
//! @{ 

//! @addtogroup ModuleName
//! @brief Module brief description
//! @{


///////////////////////////////////////////////////////////////////////////////////////////////////
//                                  Defines & Macros Section                                     
///////////////////////////////////////////////////////////////////////////////////////////////////
#define I2C_BUS_BAUDRATE       (10000)

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Typedef Section                                          
///////////////////////////////////////////////////////////////////////////////////////////////////


//! <brief description>

//! <brief description>
        //!< <brief description of each element in a enum/structure>



//! Errors that can be returned by the I2C master driver.
typedef enum _i2c_commands_errors
{
    kStatus_I2C_Success,        //!< The master performed a transfer. 
                                //! @todo AN: Validate correct enum for success case.
    kStatus_I2C_Busy,           //!< The master is already performing a transfer.
    kStatus_I2C_ReceivedNak,    //!< The slave device sent a NAK in response to a byte.
    kStatus_I2C_ArbitLostError,  //!< The master device received an arbitration lost error.
    kStatus_I2C_Timeout,
    kStatus_I2C_Failure
}i2c_commands_errors_t;

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                Function-like Macros Section                                   
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                  Extern Constants Section                                     
///////////////////////////////////////////////////////////////////////////////////////////////////
//! <brief description>

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                  Extern Variables Section                                     
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                Function Prototypes Section                                    
///////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(__cplusplus)
extern "C" {
#endif // __cplusplus

/*!
 * @brief brief function description
 *
 * @param param1 param1 brief description
 * @param param2 param2 brief description
 * @return brief description of return value
 * @retval retval1 retval1 brief description
 */
// ... your functions here ..


uint8_t i2c_commands_open(const char *master_name,
                           uint32_t **device,
                           uint32_t adapter_nr);

uint8_t i2c_commands_close(uint32_t device);

uint8_t i2c_commands_read(uint32_t master_handle,
                           uint8_t  slave_addr,
                           uint8_t*  regNo,
                           uint8_t regLengh,
                           uint8_t *buf,
                           uint32_t len,
                           uint32_t timeOut);


uint8_t i2c_commands_write(uint32_t master_handle,
                            uint8_t  slave_addr,
                            uint8_t*  regNo,
                            uint8_t regLengh,
                            uint8_t *buf,
                            uint32_t len,
                            uint32_t timeOut);

uint8_t i2c_commands_init(void);

uint8_t i2c_commands_deinit(void);


#if defined(__cplusplus)
}
#endif // __cplusplus
//! @}
//Group ModuleName

//! @}
//Group SoftwareLayer
#endif /* I2C_OPERATIONS_H_ */
///////////////////////////////////////////////////////////////////////////////////////////////////
// EOF
///////////////////////////////////////////////////////////////////////////////////////////////////
