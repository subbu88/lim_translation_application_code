/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFI Software License.   
 * See the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may    
 * not use this file except in compliance with the License.
 *
 **************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 **END********************************************************************************************/
/*************************************************************************************************
 *                                      Includes Section                        
 *************************************************************************************************/
#include "RandomAccessArray.h"

/*************************************************************************************************
 *                                   Defines & Macros Section                                      
 *************************************************************************************************/

/*! Macro to check if the position in the array is available */
#define IS_POS_AVALIABLE(arrayInfo, index)      !((1 << index) & (arrayInfo->avaliablePositions))

/*! Set the corresponding bit of the variable avaliablePositions to indicate the position
 * in the array is reserved*/
#define SET_POS_AS_RESERVED(arrayInfo, index)   (arrayInfo->avaliablePositions |= (1 << index))

/*! Clear the corresponding bit of the variable avaliablePositions to indicate the position
 * in the array is available*/
#define SET_POS_AS_AVAILABLE(arrayInfo, index)  (arrayInfo->avaliablePositions &= ~(1 << index))

/*************************************************************************************************
 *                                      Typedef Section                        
 *************************************************************************************************/

/*************************************************************************************************
 *                                  Function Prototypes Section                 
 *************************************************************************************************/

/*************************************************************************************************
 *                                   Global Constants Section                   
 *************************************************************************************************/

/*************************************************************************************************
 *                                   Static Constants Section                   
 *************************************************************************************************/

/*************************************************************************************************
 *                                   Global Variables Section                   
 *************************************************************************************************/

/*************************************************************************************************
 *                                   Static Variables Section                   
 *************************************************************************************************/

/*************************************************************************************************
 *                                      Functions Section                       
 *************************************************************************************************/

/* See RandomAccessArray.h for documentation of this function. */
void RandomAccessArray_flush_array(random_access_array_information_t* ramdomAccessArrayInfo)
{
    ramdomAccessArrayInfo->avaliablePositions = 0x00;
    ramdomAccessArrayInfo->lastAvaliableIndex = 0x00;
    ramdomAccessArrayInfo->lastInsertIndex = 0x00;
}

/* See RandomAccessArray.h for documentation of this function. */
free_array_element_status_t RandomAccessArray_free_position(random_access_array_information_t* ramdomAccessArrayInfo, uint8_t arrayIndex)
{
    free_array_element_status_t freeStatus = kFreeIndexOutOfRangeError;

    /* If the given index is in the range of the array free that element */
    if(arrayIndex < ramdomAccessArrayInfo->numberOfElements)
    {
        SET_POS_AS_AVAILABLE(ramdomAccessArrayInfo, arrayIndex);
        ramdomAccessArrayInfo->lastAvaliableIndex = arrayIndex;
        freeStatus = kFreeIndexSuccess;
    }

    return freeStatus;
}

/* See RandomAccessArray.h for documentation of this function. */
uint8_t RandomAccessArray_get_next_avaliable_position(random_access_array_information_t* ramdomAccessArrayInfo)
{
    uint8_t searchIndex;
    uint8_t newReservedIndex;

    /* Start searching from last last free index to search faster */
    searchIndex = ramdomAccessArrayInfo->lastAvaliableIndex;

    newReservedIndex = kNotAvailableIndex;

    do {
        /* The position is free */
        if(IS_POS_AVALIABLE(ramdomAccessArrayInfo, searchIndex))
        {
            /* Set index as reserved */
            newReservedIndex = searchIndex;
            SET_POS_AS_RESERVED(ramdomAccessArrayInfo, newReservedIndex);

            /* Backup last inserted index in global variable */
            ramdomAccessArrayInfo->lastInsertIndex = newReservedIndex;

            /* Update last free index with the next index */
            ramdomAccessArrayInfo->lastAvaliableIndex = newReservedIndex + 1;

            /* Make it circular */
            if (ramdomAccessArrayInfo->lastAvaliableIndex == ramdomAccessArrayInfo->numberOfElements)
            {
                ramdomAccessArrayInfo->lastAvaliableIndex = 0;
            }
        }
        /* The position is already reserved */
        else
        {
            searchIndex = (searchIndex + 1) % ramdomAccessArrayInfo->numberOfElements;
        }
        /* Search for next index until all positions are checked and newReservedIndex keeps being unavailable */
    }while( (searchIndex != ramdomAccessArrayInfo->lastAvaliableIndex ) && (kNotAvailableIndex == newReservedIndex) );

    return newReservedIndex;
}


