/*
 * Copyright (c) 2013, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************************* 
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 

 */

#if !defined(__FSL_RTOS_ABSTRACTION_BM_H__)
#define __FSL_RTOS_ABSTRACTION_BM_H__

#include <stdint.h>
#include <stdbool.h>
#include "sw_timer.h"
//#include "Cpu.h"
//#include "PE_Types.h"

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#if defined __CC_ARM
#define inline      __inline
#endif

#define __FSL_RTOS_MSGQ_COPY_MSG__ 1
//! @addtogroup rtos_irq_sync
//! @{

////////////////////////////////////////////////////////////////////////////////
// Declarations
////////////////////////////////////////////////////////////////////////////////
//! @brief Type for an interrupt synchronization object.
typedef struct
{
    bool isWaiting;
    uint8_t semCount;
    uint8_t timerId;
}sync_object_t;

//! @brief Type for a resource locking object.
typedef struct
{
    bool isWaiting;
    bool isLocked;
    uint8_t timerId;
}lock_object_t;

//! @brief Type for an event flags group. Bit 32 is reserved.
typedef uint32_t event_group_t;

/*! @brief The event flags are cleared automatically or manually.*/
typedef enum
{
    kEventAutoClr = 0,
    kEventManualClr
}event_clear_type;

//! @brief Type for an event group object.
typedef struct
{
    sync_object_t   eventSync;
    event_group_t   flags;
    event_clear_type clearType;
}event_object_t;

//! @brief Type for a task pointer.
typedef void (* task_t)(void);

//! @brief Type for a task handler, returned by the task_create function.
typedef uint32_t task_handler_t;

//! @brief Type for a task stack.
typedef uint32_t task_stack_t;

//! @brief Type for a message queue declaration and creation.
typedef struct
{
#if (__FSL_RTOS_MSGQ_COPY_MSG__)
    uint32_t *queueMem;    /* Points to the queue memory */ // TODO change uint32_t queueMem[]; for uint32_t* queueMem;
#else
    void **queueMem;       /* Points to the queue memory */ // TODO change void *queueMem[]; for void **queueMem;  
#endif
    uint16_t number;        /* Stores the elements in the queue */
    uint16_t size;          /* Stores the size in words of each element */
    uint16_t head;          /* Index of the next element to be read */
    uint16_t tail;          /* Index of the next place to write to */
    sync_object_t queueSync;
    bool isEmpty;
}msg_queue_t;

//! @brief Type for a message queue declaration and creation.
typedef msg_queue_t*  msg_queue_handler_t;
typedef void*  msg_queue_item_t; //TODO add msg_queue_item_t definition

//! @brief Status values to be returned by functions.
typedef enum
{
    kSuccess = 0,
    kError,
    kTimeout,
    kIdle
}fsl_rtos_status;

/*! @brief The event flags are set or not.*/
typedef enum
{
    kFlagNotSet = 0,
    kFlagSet
}event_status;

enum fsl_rtos_timeouts
{
    //! @brief Constant to pass as timeout value in order to wait indefinitely.
    kSyncWaitForever = 0xFFFF
};

//! @brief Macro passed to the task_destroy function to destroy the current task.
#define FSL_RTOS_CURRENT_TASK   ((task_handler_t)0)

////////////////////////////////////////////////////////////////////////////////
// API
////////////////////////////////////////////////////////////////////////////////
#if defined(__cplusplus)
extern "C" {
#endif

// TODO change from EOF file to BOF
static inline void interrupt_disable_global(void)
{
	//EnterCritical();
}

static inline void interrupt_enable_global(void)
{
	//ExitCritical();
}


//! @name Interrupt handler synchronization
//@{

/*!
 * @brief Create the synchronization object. To be used instead of a standard
 *      declaration.
 *
 * @param obj The sync object to create.
 */
#define sync_object_declare(obj)    sync_object_t obj

/*!
 * @brief Initialize a synchronization object to a given state.
 *
 * @param obj The sync object to initialize.
 */
#define sync_create(obj)        __sync_create(&obj)

/* Internal function to initialize a sync object. */
fsl_rtos_status __sync_create(sync_object_t *obj); // TODO change void for fsl_rtos_status

/*!
 * @brief Wait for a synchronization object to be signalled.
 *
 * @param obj The synchronization object.
 * @param timeout The maximum number of milliseconds to wait for the object to be signalled.
 *      Pass the #kSyncWaitForever constant to wait indefinitely for someone to signal the object.
 *      0 should not be passed to this function. Instead, use sync_poll for a non blocking check.
 *
 * @retval kSuccess The object was signalled.
 * @retval kTimeout A timeout occurred.
 * @retval kError An incorrect parameter was passed.
 * @retval kIdle The object has not been signalled.
 */
fsl_rtos_status sync_wait(sync_object_t *obj, uint32_t timeout);

/*!
 * @brief Checks if a synchronization object has been signalled. This function returns instantly.
 *
 * @param obj The synchronization object.
 *
 * @retval kSuccess The object was signalled.
 * @retval kTimeout The object was not signaled.
 */
fsl_rtos_status sync_poll(sync_object_t *obj);

/*!
 * @brief Signal for someone waiting on the syncronization object to wake up. This function
 *        should not be called from an ISR.
 *
 * @param obj The synchronization object to signal.
 * 
 * @retval kSuccess The object was successfully signalled.
 */
fsl_rtos_status sync_signal(sync_object_t *obj);

/*!
 * @brief Signal for someone waiting on the syncronization object to wake up. This function
 *        should only be called from an ISR.
 *
 * @param obj The synchronization object to signal.
 * 
 * @retval kSuccess The object was successfully signalled.
 */
static inline fsl_rtos_status sync_signal_from_isr(sync_object_t *obj)
{
    ++obj->semCount;
    
    return kSuccess;
}

/*!
 * @brief Destroy a previously created synchronization object.
 *
 * @param obj The synchronization object to destroy.
 * 
 * @retval kSuccess The object was successfully destroyed.
 */
static inline fsl_rtos_status sync_destroy(sync_object_t *obj)
{
    uint32_t status;
  
    status = sw_timer_release_channel(obj->timerId);
    
    if(status == kSwTimerStatusSuccess)
    {
        return kSuccess;
    }
    else
    {
        return kError;
    }
}

//! @name Resource locking
//@{

/*!
 * @brief Create the locking object. To be used instead of a standard
 *      declaration.
 *
 * @param obj The lock object to create.
 */
#define lock_object_declare(obj)    lock_object_t obj

/*!
 * @brief Initialize a locking object to a given state.
 *
 * @param obj The lock object to initialize.
 */
#define lock_create(obj)	__lock_create(&obj)

void __lock_create(lock_object_t *obj);

/*!
 * @brief Wait for the object to be unlocked and lock it.
 *
 * @param obj The locking object.
 * @param timeout The maximum number of milliseconds to wait for the mutex.
 *      Pass the #kSyncWaitForever constant to wait indefinitely for someone to signal the object.
 *      0 should not be passed to this function. Instead, use sync_poll for a non blocking check.
 *
 * @retval kSuccess The lock was obtained.
 * @retval kTimeout A timeout occurred.
 * @retval kError An incorrect parameter was passed.
 */
fsl_rtos_status lock_wait(lock_object_t *obj, uint32_t timeout);

/*!
 * @brief Checks if a locking object can be locked and locks it if possible.
 *      This function returns instantly.
 *
 * @param obj The locking object.
 *
 * @retval kSuccess The object was succesfully locked.
 */
fsl_rtos_status lock_poll(lock_object_t *obj);

/*!
 * @brief Unlock a previously locked object.
 *
 * @param obj The locking object to unlock.
 * 
 * @retval kSuccess The object was successfully unlocked.
 */
fsl_rtos_status lock_release(lock_object_t *obj);

/*!
 * @brief Destroy a previously created locking object.
 *
 * @param obj The locking object to destroy.
 * 
 * @retval kSuccess The object was successfully destroyed.
 */
static inline fsl_rtos_status lock_destroy(lock_object_t *obj)
{
    uint32_t status;
  
    status = sw_timer_release_channel(obj->timerId);
    
    if(status == kSwTimerStatusSuccess)
    {
        return kSuccess;
    }
    else
    {
        return kError;
    }
}

//@}

//! @name Event signaling
//@{

/*!
 * @brief Initializes the event object to a known state.
 *
 * @param obj Pointer to the event object to initialize.
 */
fsl_rtos_status event_create(event_object_t *obj, event_clear_type clearType); // TODO change return type from void to fsl_rtos_status

/*!
 * @brief Wait for any event to be set.
 *
 * @param obj The event object.
 * @param timeout The maximum number of milliseconds to wait for the event.
 *      Pass the #kSyncWaitForever constant to wait indefinitely. 0 should not be passed 
 *      to this function.
 * @param setFlags Pointer to receive the flags that were set.
 *
 * @retval kSuccess An event was set.
 * @retval kTimeout A timeout occurred.
 * @retval kError An incorrect parameter was passed.
 */
fsl_rtos_status event_wait(event_object_t *obj, uint32_t timeout, event_group_t *setFlags);

/*!
 * @brief Set one or more events of an event object. This function should not be called from an ISR.
 *
 * @param obj The event object.
 * @param setFlags Event flags to be set.
 *
 * @retval kSuccess The flags were succesfully set.
 * @retval kError An incorrect parameter was passed.
 */
fsl_rtos_status event_set(event_object_t *obj, event_group_t flags);

/*!
 * @brief Set one or more events of an event object. This function should only be called from an ISR.
 *
 * @param obj The event object.
 * @param setFlags Event flags to be set.
 *
 * @retval kSuccess The flags were succesfully set.
 * @retval kError An incorrect parameter was passed.
 */
static inline fsl_rtos_status event_set_from_isr(event_object_t *obj, event_group_t flags)
{
    obj->flags |= flags;
    return sync_signal_from_isr(&obj->eventSync);
}

/*!
 * @brief Clear one or more events of an event object. This function should not be called from an ISR.
 *
 * @param obj The event object.
 * @param flags Event flags to be clear.
 *
 * @retval kSuccess The flags were succesfully clear.
 * @retval kError An incorrect parameter was passed.
 */
fsl_rtos_status event_clear(event_object_t *obj, event_group_t flags);

/*!
 * @brief Check the bits are set or not.
 *
 * @param obj The event object.
 * @param flag The flag to check, only one bit can be checked.
 *
 * @retval kFlagsSet The flag checked are set.
 * @retval kFlagsNotSet The flag checked are not set or got an error.
 */
static inline event_status event_check_flags(event_object_t *obj, event_group_t flag)
{
    if(obj->flags & flag)
    {
        return kFlagSet;
    }
    else
    {
        return kFlagNotSet;
    }
}

/*!
 * @brief Destroy a previously created event object.
 *
 * @param obj The event object to destroy.
 * 
 * @retval kSuccess The object was successfully destroyed.
 */
static inline fsl_rtos_status event_destroy(event_object_t *obj)
{
    return sync_destroy(&obj->eventSync);
}
//@}


//! @name Message queues
//@{

/*!
 * @brief This macro statically reserves the memory required for the queue.
 * @note The queue will store pointers to the elements, and no the elements themselves. The
 *       element must continue to exist in memory for the receiving end to properly get the
 *       contents.
 *
 * @param name Identifier for the memory region.
 * @param number Number of elements in the queue. 
 * @param size Size of every element in words.
 */
#if (__FSL_RTOS_MSGQ_COPY_MSG__)
#define MSG_QUEUE_DECLARE(name, number, size) msg_queue_t name; \
												uint32_t queueMem##name[number * size]
#else
#define MSG_QUEUE_DECLARE(name, number, size) msg_queue_t name; \
												void * queueMem##name[number]
#endif

/*!
 * @brief Initialize the message queue.
 *
 * @param queue The queue declared through the MSG_QUEUE_DECLARE macro.
 * @param number The number of elements in the queue.
 * @param size Size of every elements in words.
 * 
 * @retval Handler to access the queue for put and get operations.
 */
#define msg_queue_create(queue, number, size)       __msg_queue_create(&queue, number, size, queueMem##queue) //TODO change queueMem##name for queueMem##queue
                                                                      
static inline msg_queue_handler_t __msg_queue_create(msg_queue_t *queue, uint16_t number, uint16_t size, void * mem)
{
    queue->queueMem = mem;
    queue->number = number;
    queue->size = size;
    queue->head = 0;
    queue->tail = 0;
    queue->isEmpty = TRUE;
    
    sync_create(queue->queueSync);
                                                  
    return queue;
}

/*!
 * @brief Introduce an element at the tail of the queue.
 *
 * @param handler Queue handler returned by the msg_queue_create function.
 * @param item Pointer to the element to be introduced in the queue.
 * 
 * @retval kSuccess Element succesfully introduced in the queue.
 * @retval kError The queue was full or an invalid parameter was passed.
 */
static inline fsl_rtos_status msg_queue_put(msg_queue_handler_t handler, msg_queue_item_t item)
{
    fsl_rtos_status retVal = kError;
    // TODO change declaration to beginning of file
    uint32_t *src;
    uint32_t *dst;
    uint16_t msgSize;
    
    /* Check that there is room in the queue */
    if((handler->tail != handler->head) || handler->isEmpty)
    {
    	interrupt_disable_global(); // TODO change rtos_enter_critical() for interrupt_disable_global()
#if (__FSL_RTOS_MSGQ_COPY_MSG__)
        src = (uint32_t*)item;
        dst = &handler->queueMem[handler->tail * handler->size];

        
        /* Copy entire message into the queue, based on the size configured at creation */
        msgSize = handler->size;
        while(msgSize--)
        {
           *dst++ = *src++; 
        }
#else
        /* Store the pointer to the message in the queue */
        handler->queueMem[handler->tail] = item;
#endif
        
        /* Adjust tail pointer and wrap in case the end of the buffer is reached */
        ++handler->tail;
        if(handler->tail == handler->number)
        {
            handler->tail = 0;
        }
        
        /* If queue was empty, clear the empty flag and signal that it is not empty anymore */
        if(handler->isEmpty)
        {
            handler->isEmpty = FALSE;
            sync_signal(&handler->queueSync); // TODO added &
        }
        interrupt_enable_global(); // TODO change rtos_exit_critical() for interrupt_enable_global()

        retVal = kSuccess;
    }

    return retVal;
}

/*!
 * @brief Read and remove an element at the head of the queue.
 *
 * @param handler Queue handler returned by the msg_queue_create function.
 * @param item Pointer to store a pointer to the element of the queue.
 * @param timeout In case the queue is empty, the number of milliseconds to
 *        wait for an element to be introduced into the queue. Use 0 to return
 *        immediately or #kSyncWaitForever to wait indefinitely.
 * 
 * @retval kSuccess Element successfully obtained from the queue.
 * @retval kTimeout If a timeout was specified, the queue remained empty after timeout.
 * @retval kError The queue was empty or the handler was invalid.
 * @retval kIdle The queue is empty and the timeout has not expired.
 */
static inline fsl_rtos_status msg_queue_get(msg_queue_handler_t handler, msg_queue_item_t *item, uint32_t timeout)
{
    fsl_rtos_status retVal = kError;
    
	interrupt_disable_global(); // TODO change rtos_enter_critical() for interrupt_disable_global()
    /* Check if the queue is not empty */
    if(!handler->isEmpty)
    {
#if (__FSL_RTOS_MSGQ_COPY_MSG__)
        uint32_t *src = &handler->queueMem[handler->head * handler->size];
        uint32_t *dst = (uint32_t*)item;
        uint16_t msgSize;
        
        /* Copy entire message into the queue, based on the size configured at creation */
        msgSize = handler->size;
        while(msgSize--)
        {
           *dst++ = *src++; 
        }
#else
        /* Store the pointer to the message in the queue */
        *item = handler->queueMem[handler->head];
#endif
        
        /* Adjust head pointer and wrap in case the end of the buffer is reached */
        ++handler->head;
        if(handler->head == handler->number)
        {
            handler->head = 0;
        }
        
        /* If queue was empty, clear the empty flag and signal that it is not empty anymore */
        if(handler->head == handler->tail)
        {
            handler->isEmpty = TRUE;
        }
        interrupt_enable_global(); // TODO change rtos_exit_critical() for interrupt_enable_global()
        
        /* Poll the sync object to consume it in case it has been signalled before */
        (void)sync_poll(&handler->queueSync); //TODO add &

        retVal = kSuccess;
    
    }
    else
    {
        /* Wait for the semaphore if the queue was empty */
        retVal = sync_wait(&handler->queueSync, timeout);
        interrupt_enable_global(); // TODO change rtos_exit_critical() for interrupt_enable_global()
    }
    
    return retVal;
}

/*!
 * @brief Discards all elements in the queue and leaves the queue empty.
 *
 * @param handler Queue handler returned by the msg_queue_create function.
 * 
 * @retval kSuccess Queue successfully emptied.
 */
static inline fsl_rtos_status msg_queue_flush(msg_queue_handler_t handler)
{
    /* Reseat indexes, set status to empty and consume semaphore in case it was set before */
    handler->head = 0;
    handler->tail = 0;
    handler->isEmpty = TRUE;
    sync_poll(&handler->queueSync);
    
    return kSuccess;
}

/*!
 * @brief Destroy a previously created queue.
 *
 * @param handler Queue handler returned by the msg_queue_create function.
 * 
 * @retval kSuccess The queue was successfully destroyed.
 */
static inline fsl_rtos_status msg_queue_destroy(msg_queue_handler_t handler)
{
    return sync_destroy(&handler->queueSync);
}

//@}




#if defined(__cplusplus)
}
#endif

//! @}

#endif // __FSL_RTOS_ABSTRACTION_BM_H__
////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////

