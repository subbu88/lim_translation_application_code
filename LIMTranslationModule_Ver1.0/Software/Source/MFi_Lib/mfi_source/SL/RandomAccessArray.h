/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
* Freescale Confidential Proprietary. Licensed under the Freescale MFI Software License.   
* See the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may    
* not use this file except in compliance with the License.
 *
 **************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 **END********************************************************************************************/
#ifndef RANDOMACCESSARRAY_H_
#define RANDOMACCESSARRAY_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include <stdint.h>

/*!@addtogroup services_layer Services Layer Components
 * @brief 
 * 
 * @addtogroup RandomAccessArray Random access array driver
 * @brief Service layer to manipulate random access arrays
 * @{
 */

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
/*! Number of elements of the array, define modified by the user of the driver */ 
#define NUMBER_OF_ELEMENTS_OF_THE_ARRAY      (16)

/*! Macro to check if a position in the array is reserved */
#define IS_POSITION_RESERVED(arrayInfo, index)     ((1 << index) & (arrayInfo.avaliablePositions))


/*!  Macro to check if the array is empty */
#define IS_ARRAY_EMPTY(arrayInfo)   (arrayInfo.avaliablePositions == 0x00L)
 
/************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
#if(NUMBER_OF_ELEMENTS_OF_THE_ARRAY <= 8)
/*! Number of available positions type*/
typedef uint8_t avaliable_positions_t;
#elif (NUMBER_OF_ELEMENTS_OF_THE_ARRAY <= 16)
typedef uint16_t avaliable_positions_t;
#elif (NUMBER_OF_ELEMENTS_OF_THE_ARRAY <= 32)
typedef uint32_t avaliable_positions_t;
#else
#error "Invalid number of array elements. The maximum value should be 32"
#endif

/*! Array information */
typedef struct {
    /*! Last available index in array */
    uint8_t lastAvaliableIndex;
    /*! Last inserted index in array */
    uint8_t lastInsertIndex; 
    /*! Variable that stores the available positions*/
    avaliable_positions_t avaliablePositions;
    /*! Number of elements in the array */ 
    uint8_t numberOfElements; 
}random_access_array_information_t;

/*! Return constant for RandomAccessArray_get_next_avaliable_position function */
typedef enum
{
     /*! Error reserving element in the array */
     kNotAvailableIndex = 0xFF
}get_array_element_constants_t;

/*! Return constants for RandomAccessArray_free_position function */
typedef enum
{
    /*! The given element was freed successfully  */
    kFreeIndexSuccess = 0,
    /*! Error freeing element in the array */
    kFreeIndexOutOfRangeError
}free_array_element_status_t;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
/*!
 * Free the given index of the array
 * @param ramdomAccessArrayInfo Pointer that contains the information of the array.
 * */
void RandomAccessArray_flush_array(random_access_array_information_t* ramdomAccessArrayInfo);

/*!
 * Free the given index of the array
 * @param ramdomAccessArrayInfo Pointer that contains the information of the array.
 * @param arrayIndex index of the array that will be freed.
 * @return If the given index was successfully freed or not
 * @retval kFreeIndexSuccess OK the index was freed 
 * @retval kFreeIndexOutOfRangeError ERROR  the given index can not be freed
 */
free_array_element_status_t RandomAccessArray_free_position(random_access_array_information_t* ramdomAccessArrayInfo, uint8_t arrayIndex);

/*!
 * Get the next available position in the array to be used
 * @param ramdomAccessArrayInfo Pointer that contains the information of the array.
 * @return An index to be used to write in the array 
 * @retval newReservedIndex A valid index to be used 
 * @retval kNotAvailableIndex The array is full
 */
uint8_t RandomAccessArray_get_next_avaliable_position(random_access_array_information_t* ramdomAccessArrayInfo);

/*@} Random Access Array group */

/*! @} Services layer group */

#endif /* RANDOMACCESSARRAY_H_ */
