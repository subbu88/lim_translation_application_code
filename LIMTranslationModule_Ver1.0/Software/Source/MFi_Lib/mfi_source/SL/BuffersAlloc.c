/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 **************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 **END********************************************************************************************/

/*************************************************************************************************
*                                      Includes Section                        
*************************************************************************************************/
#include "BuffersAlloc.h"
#include "ProjectTypes.h"
#include <stdbool.h>

/*************************************************************************************************
*                                   Defines & Macros Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                      Typedef Section                        
*************************************************************************************************/

/*************************************************************************************************
*                                  Function Prototypes Section                 
*************************************************************************************************/

/*************************************************************************************************
*                                   Global Constants Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                   Global Variables Section                   
*************************************************************************************************/
iAP1_buffers_t        g_iapInterfaceBuffers;

/* Control structure used for each buffer */
static bufferInformation_t    s_BufferInfo[BUFFERS_AMOUNT];


/*************************************************************************************************
*                                   Static Constants Section                   
*************************************************************************************************/
/*! Initial address of each buffer*/
static uint8_t * const kBuffersInitialPosition[BUFFERS_AMOUNT] =
{
    g_iapInterfaceBuffers.buffer0,
#if (BUFFERS_AMOUNT >= 2)
    g_iapInterfaceBuffers.buffer1,
#endif
#if (BUFFERS_AMOUNT >= 3)
    g_iapInterfaceBuffers.buffer2,
#endif
#if (BUFFERS_AMOUNT >= 4)
    g_iapInterfaceBuffers.buffer3,
#endif
};

/*! Final address of each buffer, used to create protection mechanisms to avoid memory overwriting */
static uint8_t * const kBuffersFinalPosition[BUFFERS_AMOUNT] =
{
    (g_iapInterfaceBuffers.buffer0 + (BUFFER_0_SIZE - 1)),
#if (BUFFERS_AMOUNT >= 2)    
    (g_iapInterfaceBuffers.buffer1 + (BUFFER_1_SIZE - 1)),
#endif
#if (BUFFERS_AMOUNT >= 3)    
    (g_iapInterfaceBuffers.buffer2 + (BUFFER_2_SIZE - 1)),
#endif
#if (BUFFERS_AMOUNT >= 4)    
    (g_iapInterfaceBuffers.buffer3 + (BUFFER_3_SIZE - 1)),
#endif
};

/*! Constant array to store the size of each buffer in memory */
static const uint16_t kBuffersMsgSize[BUFFERS_AMOUNT] =
{
    BUFFER_0_MSG_SIZE,
#if (BUFFERS_AMOUNT >= 2)
    BUFFER_1_MSG_SIZE,
#endif
#if (BUFFERS_AMOUNT >= 3)
    BUFFER_2_MSG_SIZE,
#endif
#if (BUFFERS_AMOUNT >= 4)
    BUFFER_3_MSG_SIZE,
#endif
};

/*! Constant array to indicate which flags signal that a buffer is full of messages */
#if ((BUFFER_0_SUBBUFFERS_AMOUNT <= 8)\
        && (BUFFER_1_SUBBUFFERS_AMOUNT <= 8)\
        && (BUFFER_2_SUBBUFFERS_AMOUNT <= 8)\
        && (BUFFER_3_SUBBUFFERS_AMOUNT <= 8))
static const uint8_t kBuffersFull[BUFFERS_AMOUNT] = 
#elif ((BUFFER_0_SUBBUFFERS_AMOUNT <= 16)\
        && (BUFFER_1_SUBBUFFERS_AMOUNT <= 16)\
        && (BUFFER_2_SUBBUFFERS_AMOUNT <= 16)\
        && (BUFFER_3_SUBBUFFERS_AMOUNT <= 16))
static const uint16_t kBuffersFull[BUFFERS_AMOUNT] =
#elif ((BUFFER_0_SUBBUFFERS_AMOUNT <= 32)\
        && (BUFFER_1_SUBBUFFERS_AMOUNT <= 32)\
        && (BUFFER_2_SUBBUFFERS_AMOUNT <= 32)\
        && (BUFFER_3_SUBBUFFERS_AMOUNT <= 32))
static const uint32_t kBuffersFull[BUFFERS_AMOUNT] =
#endif
{
    BUFFER_0_FULL,
#if (BUFFERS_AMOUNT >= 2)
    BUFFER_1_FULL
#endif
#if (BUFFERS_AMOUNT >= 3)
    BUFFER_2_FULL
#endif
#if (BUFFERS_AMOUNT >= 4)
    BUFFER_3_FULL
#endif
};

/*************************************************************************************************
*                                   Static Variables Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                      Functions Section                       
*************************************************************************************************/

/* See MallocBuffers.h for documentation of this function */
uint8_t* buffersAlloc_reserveSpace(uint8_t bufferIndex, uint16_t sizeOfMessage)
{
    uint8_t* returnPointer = NULL;
    bool posFound = false;
    uint8_t counterMsgPos = 0;
    
    if(bufferIndex < BUFFERS_AMOUNT)
    {
        /* Verify that sizeOfMessage required is a correct value */
        if((sizeOfMessage > 0) && (sizeOfMessage <= kBuffersMsgSize[bufferIndex]))
        {
            /* Check buffer status */
            if(BUFFERS_EMPTY != s_BufferInfo[bufferIndex].msgStatusRegister)
            {
                if(kBuffersFull[bufferIndex] != s_BufferInfo[bufferIndex].msgStatusRegister)
                {
                    /* searches the first bit as 0 (means that exists this space available)
                     * and set this bit with 1 (means that this space was taken)
                     * Only until a 0 is found this FOR will be completed.
                     * This FOR is sure because only can enters here 
                     * if the bMsgStatusReg isn't empty nor full */
                    for(counterMsgPos = 0; false == posFound; counterMsgPos++)
                    {
                        if(!(s_BufferInfo[bufferIndex].msgStatusRegister & (1 << counterMsgPos)))
                        {
                            posFound = true;
                            s_BufferInfo[bufferIndex].msgStatusRegister |= (1<<counterMsgPos);
                            s_BufferInfo[bufferIndex].availablePosition = 
                                    (kBuffersInitialPosition[bufferIndex] + 
                                            (kBuffersMsgSize[bufferIndex] * counterMsgPos));
                            returnPointer = s_BufferInfo[bufferIndex].availablePosition;
                        }
                    }
                }
                /* if buffer is full (this full value is variable according 
                 * to the number of possible message into buffer) returns a NULL value */
                else
                {
                    returnPointer = NULL;
                }
            }
            /* if buffer is empty assigns from initial position in buffer 
             * & set the first bit in bMsgStatusReg to indicate that this buffer already in use */
            else
            {
                s_BufferInfo[bufferIndex].msgStatusRegister |= 1;
                s_BufferInfo[bufferIndex].availablePosition = kBuffersInitialPosition[bufferIndex];
                returnPointer = s_BufferInfo[bufferIndex].availablePosition;
            }
        }
    }
    return (returnPointer);
}


/* See MallocBuffers.h for documentation of this function */
uint8_t buffersAlloc_releaseSpace(uint8_t bufferIndex, uint8_t* releaseAddress)
{
    uint8_t bReturnStatus = kBufferErrorOutOfRange;
    uint8_t bMsgPosition;
#if ((BUFFER_0_SIZE <= 255)\
        && (BUFFER_1_SIZE <= 255)\
        && (BUFFER_2_SIZE <= 255)\
        && (BUFFER_3_SIZE <= 255))
    uint8_t offsetBetweenStartAndReleaseAddress;
#elif ((BUFFER_0_SIZE <= 65535)\
        && (BUFFER_1_SIZE <= 65535)\
        && (BUFFER_2_SIZE <= 65535)\
        && (BUFFER_3_SIZE <= 65535))
    uint16_t offsetBetweenStartAndReleaseAddress;
#endif
    
    /* Verify that buffer is a valid buffer */
    if(bufferIndex < BUFFERS_AMOUNT)
    {
        /* checks that pointer is a valid value */
        if((releaseAddress >= kBuffersInitialPosition[bufferIndex]) 
                && (releaseAddress <= kBuffersFinalPosition[bufferIndex]))
        {
            offsetBetweenStartAndReleaseAddress = releaseAddress - kBuffersInitialPosition[bufferIndex]; 
            /* Verify that the address points to the start of some block */
            if(!(offsetBetweenStartAndReleaseAddress % kBuffersMsgSize[bufferIndex]))
            {
                /* Check buffer status */
                /* if buffer isn't empty:
                 * calculates the position of space that wants to be released
                 * and clears this bit to indicate that is a space that can be occupied again */
                if(BUFFERS_EMPTY != s_BufferInfo[bufferIndex].msgStatusRegister)
                {
                    bMsgPosition = (offsetBetweenStartAndReleaseAddress / kBuffersMsgSize[bufferIndex]);
                    s_BufferInfo[bufferIndex].msgStatusRegister &= ~(1 << bMsgPosition);
                    bReturnStatus = kBufferOk;
                }
                /* if buffer is empty, just returns a EMPTY status */
                else
                {
                    bReturnStatus = kBufferEmpty;
                }
            }
        }
        else
        {
            bReturnStatus = kBufferErrorPointer;
        }
    }
    return (bReturnStatus);
}

uint8_t buffersAlloc_resetBufferStatus(uint8_t bufferIndex) 
{
    uint8_t returnValue = kBufferErrorOutOfRange; 
    if (bufferIndex < BUFFERS_AMOUNT)
    {
        s_BufferInfo[bufferIndex].msgStatusRegister = 0; 
        s_BufferInfo[bufferIndex].availablePosition = kBuffersInitialPosition[bufferIndex];
        returnValue = kBufferOk; 
    }
    return (returnValue); 
}

void buffersAlloc_resetAllBuffersStatus(void) 
{
    uint8_t bufferIndex = 0;  
    while (bufferIndex < BUFFERS_AMOUNT)
    {
        s_BufferInfo[bufferIndex].msgStatusRegister = 0; 
        s_BufferInfo[bufferIndex].availablePosition = kBuffersInitialPosition[bufferIndex];
        bufferIndex++; 
    }
}
