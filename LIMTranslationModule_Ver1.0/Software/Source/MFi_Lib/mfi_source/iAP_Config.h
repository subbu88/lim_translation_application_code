/*HEADER******************************************************************************************
 *
 * Copyright 2015 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_CONFIG_H_
#define IAP_CONFIG_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "mfi_cfg.h"
#include "GeneralLingo.h"
#include "iAP2_Ext_Acc_Feat.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/ 

/*************************************************************************************************/
/* iAP2 Accessory Configuration Strings */
/*************************************************************************************************/ 
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
/*-----------------------------------------------------------------------------------------------*/
/* Name */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_NAME_STRING_DATA                         "Freescale Accessory"
/*-----------------------------------------------------------------------------------------------*/
/* Model identifier */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_MODEL_ID_STRING_DATA                     "ABCD1234"
/*-----------------------------------------------------------------------------------------------*/
/* Manufacturer */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_MANUFACTURER_STRING_DATA                 "Freescale"
/*-----------------------------------------------------------------------------------------------*/
/* Serial number */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_SERIAL_NUM_STRING_DATA                   "123456789"
/*-----------------------------------------------------------------------------------------------*/
/* Firmware version */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_FW_VERSION_STRING_DATA                   "001"
/*-----------------------------------------------------------------------------------------------*/
/* Hardware version */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_HW_VERSION_STRING_DATA                   "001"
/*-----------------------------------------------------------------------------------------------*/
#endif
/*************************************************************************************************/
/* iAP2 External Accessory Configuration Strings */
/*************************************************************************************************/ 
/*-----------------------------------------------------------------------------------------------*/
/* Supported External Accessory Protocol */
/*-----------------------------------------------------------------------------------------------*/

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL == _TRUE_)
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 0)
#define IAP2_FEAT_EA_PROT0_CALLBACK                         vfnTestSuite_EACallback//iAP2_EA_Callback
#define IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA0                (0x00)
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION 
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA0              "com.freescale.EADemo"
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA0             (0x00)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID0        (_TRUE_)
//#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID0 == _TRUE_)
/* NOTE: Must refer to the transport component id of a declared USB Host Transport component */
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA0   (0x0001) 
//#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 1)
#define IAP2_FEAT_EA_PROT1_CALLBACK                         NULL
#define IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA1                (0x01)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA1              "Ext Acc"
#define IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA1             (0x00)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID1        (_FALSE_)
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID1 == _TRUE_)
/* NOTE: Must refer to the transport component id of a declared USB Host Transport component */
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA1   (0x0001) 
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 2)
#define IAP2_FEAT_EA_PROT2_CALLBACK                         NULL
#define IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA2                (0x02)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA2              "Ext Acc"
#define IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA2             (0x00)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID2        (_FALSE_)
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID2 == _TRUE_)
/* NOTE: Must refer to the transport component id of a declared USB Host Transport component */
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA2   (0x0001) 
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 3)
#define IAP2_FEAT_EA_PROT3_CALLBACK                         NULL
#define IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA3                (0x03)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA3              "Ext Acc"
#define IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA3             (0x00)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID3        (_FALSE_)
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID3 == _TRUE_)
/* NOTE: Must refer to the transport component id of a declared USB Host Transport component */
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA3   (0x0001) 
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 4)
#define IAP2_FEAT_EA_PROT4_CALLBACK                         NULL
#define IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA4                (0x04)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA4              "Ext Acc"
#define IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA4             (0x00)
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID4        (_FALSE_)
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID4 == _TRUE_)
/* NOTE: Must refer to the transport component id of a declared USB Host Transport component */
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA4   (0x0001) 
#endif
#endif
#endif

/*-----------------------------------------------------------------------------------------------*/
/* Preferred App Bundle Seed Identifier */
/*-----------------------------------------------------------------------------------------------*/
#if (IAP2_IDENT_PREF_APP_BUNDLE_SEED_ID == _TRUE_)
#define IAP2_IDENT_PREF_APP_BUNDLE_SEED_ID_DATA             "5E8NLJSRZ"
#endif


/*************************************************************************************************/
/* iAP1 Configuration Strings */
/*************************************************************************************************/ 
/*-----------------------------------------------------------------------------------------------*/
/* Supported External Accessory Protocol */
/*-----------------------------------------------------------------------------------------------*/
/* External accessory string must be defined in this format (character by character separated by commas)*/
#define EA_PROTOCOL_STRING_DATA                   'c', 'o', 'm', '.', 'f', 'r', 'e', 'e', 's', \
                                                  'c', 'a', 'l', 'e', '.', 'E', 'A', 'D', 'e', \
                                                  'm', 'o', '\0'
/* Size must be modified appropriately to match the size of the previous string*/
#define EA_PROTOCOL_STRING_SIZE                    0x15

/* Bundle seed string must be defined in this format (character by character separated by commas)*/
#define BUNDLE_SEED_ID_STRING_DATA                '5', 'E', '8', 'N', 'L', 'J', '5', 'R', 'Z', '2','\0'
/* Size must be modified appropriately to match the size of the previous string*/
#define BUNDLE_SEED_ID_STRING_SIZE                 0x0B

/*! EA Protocol Metadata Token Configuration. Valid options: 
 * kEaProtHideInformation: The device doesn't try to find a matching app nor displays a button to do it
 * kEaProtAppStoreDefault: The device tries to find a matching app.
 * kEaProtAppStoreNeverMatch: The device doesn't try to find a matching app but displays a button to do it.
 */
#define DEFAULT_EA_PROTOCOL_METADATA               (kEaProtAppStoreNeverMatch)

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/* iAP2 Function prototypes  */
/*************************************************************************************************/ 
void IAP2_FEAT_EA_PROT0_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus);
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 1)
void IAP2_FEAT_EA_PROT1_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus);
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 2)
void IAP2_FEAT_EA_PROT2_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus);
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 3)
void IAP2_FEAT_EA_PROT3_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus);
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 4)
void IAP2_FEAT_EA_PROT4_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus);
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 5)
#error "Maximum number of External Accessory instances is 5" 
#endif

#endif /* IAP_CONFIG_H_ */
