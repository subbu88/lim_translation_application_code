/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_TRANSPORT_H_
#define IAP_TRANSPORT_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef void (* IAP_TRANSPORT_RECV_CALLBACK)(uint8_t *pbData, uint16_t wDataSize);
typedef void (* IAP_TRANSPORT_SENT_CALLBACK)(void);
typedef uint16_t (* IAP_TRANSPORT_SEND_DATA)(uint8_t *pbData, uint16_t wSize);
typedef void (* IAP_TRANSPORT_RECEIVE_DATA)(void);
typedef void (* IAP_TRANSPORT_NATIVE_RECEIVE_DATA)(uint8_t *pbData);
typedef uint8_t (* IAP_TRANSPORT_STATUS)(void);

typedef enum
{
	IAP_TRANSPORT_USB_DEVICE = 0,
	IAP_TRANSPORT_USB_HOST,
	IAP_TRANSPORT_UART,
	IAP_TRANSPORT_COUNT
}IAP_TRANSPORTS;

typedef enum
{
	IAP_TRANS_OK = 0,
	IAP_TRANS_NOT_AVAILABLE,
	IAP_TRANS_ILEGAL_TRANSPORT,
	IAP_TRANS_BUSY
}IAP_TRANS_STAUTS;
/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern IAP_TRANSPORT_SEND_DATA iAP_Transport_SendData;
extern IAP_TRANSPORT_RECEIVE_DATA iAP_Transport_DataReceive;
extern IAP_TRANSPORT_STATUS iAP_Transport_Status;
extern IAP_TRANSPORT_SEND_DATA iAP_Transport_NEASendData;
extern IAP_TRANSPORT_NATIVE_RECEIVE_DATA iAP_Transport_NEARcvData;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
uint8_t iAP_Transport_SelectTranspot(uint8_t bTransID);
void iAP_Transport_SetRecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback);
void iAP_Transport_SetSentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback);
void iAP_Transport_Init(void);
void iAP_Transport_UnInit(void);

/*************************************************************************************************/

#endif /* IAP_TRANSPORT_H_ */
