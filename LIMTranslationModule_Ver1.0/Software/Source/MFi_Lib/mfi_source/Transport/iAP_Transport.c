/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "MFi_Config.h"
#include "iAP_Transport.h"
#ifdef IAPI_INTERFACE_UART
#include "Events.h"
#endif

#ifdef IAPI_INTERFACE_USB_DEV
#include "iAP_USBDevice.h"
#endif
#ifdef IAPI_INTERFACE_USB_HOST
#include "iAP_USBHost.h"
#endif
#include <stdio.h>

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
#if defined(IAPI_INTERFACE_USB_HOST)
IAP_TRANSPORT_SEND_DATA iAP_Transport_SendData = iAP_USBHost_Transmit_Message;
IAP_TRANSPORT_RECEIVE_DATA iAP_Transport_DataReceive = iAP_USBHost_DataReceive;
IAP_TRANSPORT_STATUS iAP_Transport_Status = (uint8_t (*)(void))iAP_USBHost_Status;
#elif defined(IAPI_INTERFACE_USB_DEV)
IAP_TRANSPORT_SEND_DATA iAP_Transport_SendData = iAP_USBDev_Transmit_Message;
IAP_TRANSPORT_RECEIVE_DATA iAP_Transport_DataReceive = iAP_USBDev_DataReceive;
IAP_TRANSPORT_STATUS iAP_Transport_Status = iAP_USBDev_Status;
IAP_TRANSPORT_SEND_DATA iAP_Transport_NEASendData = iAP_USBDev_NEATransmit;
IAP_TRANSPORT_NATIVE_RECEIVE_DATA iAP_Transport_NEARcvData = iAP_USBDev_NEADataReceive;
#elif defined(IAPI_INTERFACE_UART)
IAP_TRANSPORT_SEND_DATA iAP_Transport_SendData = UART_Transmit_Message;
IAP_TRANSPORT_RECEIVE_DATA iAP_Transport_DataReceive = iAP_UART_DummyDataReceive;
IAP_TRANSPORT_STATUS iAP_Transport_Status = iAP_UART_Status;
#endif

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
/* Declare and initialize active transport variable in order of transport priority */
#if defined(IAPI_INTERFACE_USB_HOST)
static uint8_t iAP_Transp_bActiveTransport = IAP_TRANSPORT_USB_HOST;
#elif defined(IAPI_INTERFACE_USB_DEV)
static uint8_t iAP_Transp_bActiveTransport = IAP_TRANSPORT_USB_DEVICE;
#elif defined(IAPI_INTERFACE_UART)
static uint8_t iAP_Transp_bActiveTransport = IAP_TRANSPORT_UART;
#endif

static IAP_TRANSPORT_RECV_CALLBACK iAP_Trans_RecvCbk;
static IAP_TRANSPORT_SENT_CALLBACK iAP_Trans_SentCbk;
static IAP_TRANSPORT_RECV_CALLBACK iAP_Trans_NEARecvCbk;
static IAP_TRANSPORT_SENT_CALLBACK iAP_Trans_NEASentCbk;
/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
uint8_t iAP_Transport_SelectTranspot(uint8_t bTransID)
{
	uint8_t bStatus;
	
	bStatus = IAP_TRANS_OK;
	if(IAP_TRANSPORT_USB_DEVICE == bTransID)
	{
#if defined(IAPI_INTERFACE_USB_DEV)
		iAP_Transport_SendData = iAP_USBDev_Transmit_Message;
		iAP_Transport_DataReceive = iAP_USBDev_DataReceive;
		iAP_Transport_Status = iAP_USBDev_Status;
		iAP_Transport_NEASendData = iAP_USBDev_NEATransmit;
		iAP_Transport_NEARcvData = iAP_USBDev_NEADataReceive;
		iAP_Transp_bActiveTransport = bTransID;
		iAP_USBDev_SetSentCallback(iAP_Trans_SentCbk);
		iAP_USBDev_SetRecvCallback(iAP_Trans_RecvCbk);
		/* native endpoints */
		iAP_USBDev_SetNEASentCallback(iAP_Trans_NEASentCbk);
		iAP_USBDev_SetNEARecvCallback(iAP_Trans_NEARecvCbk);		
#else
		bStatus = IAP_TRANS_NOT_AVAILABLE;
#endif
	}
	else if(IAP_TRANSPORT_USB_HOST == bTransID)
	{
#if defined(IAPI_INTERFACE_USB_HOST)
		iAP_Transport_SendData = iAP_USBHost_Transmit_Message;
		iAP_Transport_DataReceive = iAP_USBHost_DataReceive;
		iAP_Transport_Status = (uint8_t (*)(void))iAP_USBHost_Status;
		iAP_Transp_bActiveTransport = bTransID;
		iAP_USBHost_SetSentCallback(iAP_Trans_SentCbk);
		iAP_USBHost_SetRecvCallback(iAP_Trans_RecvCbk);
#else
		bStatus = IAP_TRANS_NOT_AVAILABLE;
#endif
	}
	else if(IAP_TRANSPORT_UART == bTransID)
	{
#if defined(IAPI_INTERFACE_UART)
		iAP_Transport_SendData = UART_Transmit_Message;
        iAP_Transport_DataReceive = iAP_UART_DummyDataReceive;
		iAP_Transport_Status = iAP_UART_Status;
		iAP_Transp_bActiveTransport = bTransID;
		iAP_UART_SetSentCallback(iAP_Trans_SentCbk);
		iAP_UART_SetRecvCallback(iAP_Trans_RecvCbk);
#else
		bStatus = IAP_TRANS_NOT_AVAILABLE;
#endif
	}
	else
	{
		bStatus = IAP_TRANS_ILEGAL_TRANSPORT;
	}
	
	return bStatus;
}

void iAP_Transport_SetSentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback)
{
#if (defined(IAPI_INTERFACE_USB_HOST) || defined(IAPI_INTERFACE_UART)) && defined(IAPI_INTERFACE_USB_DEV)
	if(IAP_TRANSPORT_USB_DEVICE == iAP_Transp_bActiveTransport)
#endif
	{
#if defined(IAPI_INTERFACE_USB_DEV)
		iAP_USBDev_SetSentCallback(fpCallback);
#endif
	}

#if (defined(IAPI_INTERFACE_USB_DEV) || defined(IAPI_INTERFACE_UART)) && defined(IAPI_INTERFACE_USB_HOST)
	if(IAP_TRANSPORT_USB_HOST == iAP_Transp_bActiveTransport)
#endif
	{
#if defined(IAPI_INTERFACE_USB_HOST)
		iAP_USBHost_SetSentCallback(fpCallback);
#endif
	}
	
#if (defined(IAPI_INTERFACE_USB_DEV) || defined(IAPI_INTERFACE_USB_HOST)) && defined(IAPI_INTERFACE_UART)
	if(IAP_TRANSPORT_USB_DEVICE == iAP_Transp_bActiveTransport)
#endif
	{
#if defined(IAPI_INTERFACE_UART)
		iAP_UART_SetSentCallback(fpCallback);
#endif
	}
	
	/* Store the value to be able to set it when a transport change is made */
	iAP_Trans_SentCbk = fpCallback;
}

void iAP_Transport_SetRecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback)
{
#if (defined(IAPI_INTERFACE_USB_HOST) || defined(IAPI_INTERFACE_UART)) && defined(IAPI_INTERFACE_USB_DEV)
	if(IAP_TRANSPORT_USB_DEVICE == iAP_Transp_bActiveTransport)
#endif
	{
#if defined(IAPI_INTERFACE_USB_DEV)
		iAP_USBDev_SetRecvCallback(fpCallback);
#endif
	}

#if (defined(IAPI_INTERFACE_USB_DEV) || defined(IAPI_INTERFACE_UART)) && defined(IAPI_INTERFACE_USB_HOST)
	if(IAP_TRANSPORT_USB_HOST == iAP_Transp_bActiveTransport)
#endif
	{
#if defined(IAPI_INTERFACE_USB_HOST)
		iAP_USBHost_SetRecvCallback(fpCallback);
#endif
	}
	
#if (defined(IAPI_INTERFACE_USB_DEV) || defined(IAPI_INTERFACE_USB_HOST)) && defined(IAPI_INTERFACE_UART)
	if(IAP_TRANSPORT_USB_DEVICE == iAP_Transp_bActiveTransport)
#endif
	{
#if defined(IAPI_INTERFACE_UART)
		iAP_UART_SetRecvCallback(fpCallback);
#endif
	}
	
	/* Store the value to be able to set it when a transport change is made */
	iAP_Trans_RecvCbk = fpCallback;
}

void iAP_Transport_SetNEASentCallback(IAP_TRANSPORT_SENT_CALLBACK fpCallback)
{
	{
		iAP_USBDev_SetNEASentCallback(fpCallback);
	}

	{
	}

	{
	}
//printf("\r\n\t track-2\r\n");
	/* Store the value to be able to set it when a transport change is made */
	iAP_Trans_NEASentCbk = fpCallback;
}

void iAP_Transport_SetNEARecvCallback(IAP_TRANSPORT_RECV_CALLBACK fpCallback)
{
	{
		iAP_USBDev_SetNEARecvCallback(fpCallback);
	}

	{
	}

	{
	}
 //printf("\r\n\t track-1\r\n");

	/* Store the value to be able to set it when a transport change is made */
	iAP_Trans_NEARecvCbk = fpCallback;
}

void iAP_Transport_Init(void)
{

#if defined(IAPI_INTERFACE_USB_HOST)
	/** @todo Function Return */
	(void)iAP_USBHost_Init();
#elif defined(IAPI_INTERFACE_USB_DEV)
	iAP_USBDev_Init();
#elif defined(IAPI_INTERFACE_UART)
	/* Receive UART data */
	(void)iAPUart1_ReceiveBlock(iAPUart1_DeviceData,(void *)&g_bUARTReceivedData,UART_NUM_RX_BYTES);
#endif
}

void iAP_Transport_UnInit(void)
{
#if defined(IAPI_INTERFACE_USB_HOST)
        (void)iAP_USBHost_UnInit();
#elif defined(IAPI_INTERFACE_USB_DEV)
        (void)iAP_USBDev_UnInit();
#elif defined(IAPI_INTERFACE_UART)
#endif
}
