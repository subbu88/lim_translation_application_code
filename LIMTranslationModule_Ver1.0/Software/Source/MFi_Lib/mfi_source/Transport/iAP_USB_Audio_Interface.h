/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_USB_AUDIO_INTERFACE_H_
#define IAP_USB_AUDIO_INTERFACE_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "MFi_Config.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#ifdef MFI_AUDIO_SUPPORT 
#define AUDIO_MAX_iSIZE				192
#define AUDIO_MAX_SIZE				384
#define AUDIO_SAMPLE_SIZE				(2)		/* Sample size in bytes */
#define AUDIO_SAMPLE_RESOLUTION			(16)	/* Significant bits per sample */
#define AUDIO_FREQ_COUNT				(2)		/* Number of supported freq */
#define AUDIO_RCV_CHANNELS				(0x02)
#endif 

typedef enum
{
	USB_AUDIO_RECEIVED = 0,
	USB_FEEDBACK_SENT
}_eUsbAudioStatus;
#ifdef MFI_AUDIO_SUPPORT 
typedef enum _eUsbTransportStatus
{
	USB_AUDIO_ASYNC_FLAG,   /*Audio asynchronous or synchronous*/
	USB_AUDIO_IDEV_FLAG   /*Connected device is iPod,iPad or iPhone*/
}_eUsbTransportStatus;
#endif
/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern volatile uint8_t g_bUSBAudioStatus;
#ifdef MFI_AUDIO_SUPPORT 
extern uint16_t g_wAudioRecvSize;
extern uint8_t g_baAudioRecvBuffer[];
extern uint8_t gu8NewFreqFlag;
extern uint8_t g_cur_sampling_frequency[];
extern uint8_t g_bAudioBufferFlags;
extern uint8_t gu8FreqBackUp;
#endif

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/

#endif /* IAP_USB_AUDIO_INTERFACE_H_ */
