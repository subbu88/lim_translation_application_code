/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_NowPlayingUpdates_Feat.h"
#include "iAP2_Sessions_Control.h"
#include "iAP2_Features.h"
#include "iAP2_Host.h"
/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/

#define iAP2_NOW_PLAYING_UPDATES_START_MAX_MESSAGE_SIZE		(0x7A)	
#define iAP2_NOW_PLAYING_UPDATES_STOP_MAX_MESSAGE_SIZE	    (0x06)


/*Possible ID parameter for NowPlayingFeature*/


#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_TITTLE_ID				            (1)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PLAYBACK_DURATION_ID		            (4)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_TITLE_ID			            (6)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_TRACK_NUMBER_ID	            (7)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_TRACK_COUNT_ID		            (8)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_DISC_NUMBER_ID		            (9)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_DISC_COUNT			            (10)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ARTIST_ID				            (12)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_GENRE_ID					            (16)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_COMPOSER_ID				            (18)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PROPERTY_IS_LIKE_SUPPORTED           (21)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PROPERTY_IS_BAN_SUPPORTED            (22)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PROPERTY_IS_LIKED                    (23)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PROPERTY_IS_BANNED                   (24)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ARTWORK_FILE_TRANSFER_ID	            (26)
#define iAP2_NOW_PLAYING_UPDATES_BIGGER_MEDIA_PARAM_ID                                      (27)

#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_ID						        (0)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_ELAPSED_TIME_ID			        (1)		
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_QUEUE_INDEX_ID			        (2)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_QUEUE_COUNT_ID			        (3)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_QUEUE_CHAPTER_INDEX_ID	        (4)		
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_SUFFLE_MODE_ID			        (5)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_REAPET_MODE_ID			        (6)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_APP_NAME_ID				        (7)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_MEDIA_LIBRARY_UNIQUE_ID                (8)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_PB_ITUNES_RADIO_AD                     (9)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_PB_ITUNES_RADIO_STATION_NAME           (10)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_PB_ITUNES_RADIO_STATION_PERSISTENT_ID  (11)
#define iAP2_NOW_PLAYING_UPDATES_BIGGER_PLAYBACK_PARAM_ID                                   (12)


#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ID							(0)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_ID								(1)

#define iAP2_NOW_PLAYING_UPDATES_INVALID_MEDIA_ITEM_UPDATE_FLAG						(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_MAX_VALID_PARAMETER)
#define iAP2_NOW_PLAYING_UPDATES_INVALID_PLAYBACK_STATUS_UPDATE_FLAG				(1<<iAP2_NOW_PLAYING_PLAYBACK_MAX_VALID_PARAMETER)
				
/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void iAP2_NowPlayingUpdates_vfnNewRequestCaller(void(*fnPtr)(uint16_t),uint16_t MessageID);
static uint8_t iAP2_NowPlayingUpdates_vfnGetParameter(void);

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/

/*Pointers to functions to be executed according to received ParameterID*/
void (* const NowPlayingUpdates_vfnapReceivedMediaItemParameters[]) (void* subparamvalue,uint8_t maxValue, uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame) =
{
		iAP2_FEATURES_UPDATES_InvalidParameter, //0
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8,
		iAP2_FEATURES_UPDATES_InvalidParameter,
		iAP2_FEATURES_UPDATES_InvalidParameter,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32, 
		iAP2_FEATURES_UPDATES_InvalidParameter, //5
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16,//10
		iAP2_FEATURES_UPDATES_InvalidParameter,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8,
		iAP2_FEATURES_UPDATES_InvalidParameter,
		iAP2_FEATURES_UPDATES_InvalidParameter,
		iAP2_FEATURES_UPDATES_InvalidParameter, //15
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8,
		iAP2_FEATURES_UPDATES_InvalidParameter,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8,
		iAP2_FEATURES_UPDATES_InvalidParameter,
		iAP2_FEATURES_UPDATES_InvalidParameter, //20
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum,
        iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum,
        iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum,
        iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum,
        iAP2_FEATURES_UPDATES_InvalidParameter, //25
        iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 
};

/*Pointers to functions to be executed according to received ParameterID*/
void (* const NowPlayingUpdates_vfnapReceivedPlaybackStatusParameters[])(void* subparamvalue,uint8_t maxValue, uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame) =
{
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum, //0
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum,//5
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum, 
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8,//10
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64
};


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static iAP2_NowPlayingUpdatesData NowPlayingUpdates; 				/**< Keep the last NowPlayingUpdates*/
static iAP2_NowPlayingUpdatesData *NowPlayingUpdatesDataToUpdate;  /**< Keep pointer where NowPlayingUpdates will be updated*/

/** Temporal arrays where some updates are saved until a complete message was validated */
static uint8_t gabMediaItemMediaTitle[iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE];
static uint8_t gabMediaItemAlbumTitle[iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE];
static uint8_t gabMediaItemArtist[iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE];
static uint8_t gabMediaItemGenre[iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE];
static uint8_t gabMediaItemComposer[iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE];	
static uint8_t gabPlaybackAppName[iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE];
static uint8_t gabPBiTunesRadioStationName[iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE];
static uint8_t gabPBMediaLibraryUniqueIdentifier[iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE];

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*Pointers to variables where received info will be stored*/
static void * const iAP2_NOW_PLAYING_UPDATES_Media_Item_SubparamVal[]=
{
        NULL, //0
        &(NowPlayingUpdates.pbMediaItemMediaTitle),
        NULL,
        NULL,
        &(NowPlayingUpdates.dwMediaItemPlaybackDuration),
        NULL, //5
        &(NowPlayingUpdates.pbMediaItemAlbumTitle),
        &(NowPlayingUpdates.wMediaItemAlbumTrackNumber),
        &(NowPlayingUpdates.wMediaItemAlbumTrackCount),
        &(NowPlayingUpdates.wMediaItemAlbumDiscNumber),
        &(NowPlayingUpdates.wMediaItemAlbumDiscCount), //10
        NULL,
        &(NowPlayingUpdates.pbMediaItemArtist),
        NULL,
        NULL,
        NULL,//15
        &(NowPlayingUpdates.pbMediaItemGenre),
        NULL,
        &(NowPlayingUpdates.pbMediaItemComposer),
        NULL,
        NULL,//20
        &(NowPlayingUpdates.bMediaItemPropertyIsLikeSupported),
        &(NowPlayingUpdates.bMediaItemPropertyIsBanSupported),
        &(NowPlayingUpdates.bMediaItemPropertyIsLiked),
        &(NowPlayingUpdates.bMediaItemPropertyIsBanned),
        NULL,//25
        &(NowPlayingUpdates.bMediaItemArtworkFileTransferIdentifier)
};
      
/*Pointers to variables where received info will be stored*/
static void * iAP2_NOW_PLAYING_UPDATES_Playback_Status_SubparamVal[] =
{
		&(NowPlayingUpdates.bPlaybackStatus),
		&(NowPlayingUpdates.dwPlaybackElapsedTime),
	    &(NowPlayingUpdates.dwPlaybackQueueIndex),
	    &(NowPlayingUpdates.dwPlaybackQueueCount),
	    &(NowPlayingUpdates.dwPlaybackQueueChapterIndex),
	    &(NowPlayingUpdates.bPlaybackSuffleMode), //5  
	    &(NowPlayingUpdates.bPlaybackStatusRepeatMode),
	    &(NowPlayingUpdates.pbPlaybackAppName),
        &(NowPlayingUpdates.pbPBMediaLibraryUniqueIdentifier),  
        &(NowPlayingUpdates.bPBiTunesRadioAd),
        &(NowPlayingUpdates.pbPBiTunesRadioStationName), //10
        &(NowPlayingUpdates.qwPBiTunesRadioStationMediaPlaylistPersistentID), 
};                                                 
                                                                    
/*Pointers to maximum possible size for received data*/
static uint8_t * const iAP2_NOW_PLAYING_UPDATES_Media_Item_SubparaMaxSizes[] =
{
        NULL, 
        (&(NowPlayingUpdates.bMediaItemMediaTitleSize)),	 
        NULL,
        NULL,
        NULL,
        NULL, //5
        (&(NowPlayingUpdates.bMediaItemAlbumTitleSize)),
        NULL,
        NULL,
        NULL,
        NULL, //10
        NULL,
        (&(NowPlayingUpdates.bMediaItemArtistSize)),
        NULL,
        NULL,
        NULL,//15
        (&(NowPlayingUpdates.bMediaItemGenreSize)),
        NULL,
        (&(NowPlayingUpdates.bMediaItemComposerSize)),
        NULL,
        NULL, //20
        NULL,
        NULL,
        NULL,
        NULL, 
        NULL, //25
        NULL
};

 /*Flag values for each received parameter */
static uint16_t const iAP2_NOW_PLAYING_UPDATES_Media_Item_subParamFlag[] =
{
        0, 
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_TITLE, 
        0,
        0,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PLAYBACK_DURATION,
        0, //5
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_TITLE,	
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_TRACK_NUMBER,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_TRACK_COUNT,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_DISC_NUMBER,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_DISC_COUNT, //10
        0,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ARTIST,
        0,
        0,
        0, //15
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_GENRE,
        0,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_COMPOSER,
        0,
        0, //20
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PROPERTY_IS_LIKE_SUPPORTED, 
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PROPERTY_IS_BAN_SUPPORTED,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PROPERTY_IS_LIKED,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PROPERTY_IS_BANNED,
        0, //25
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ARTWORK_FILE_TRANSFER_IDENTIFIER
};

 /*Maximum values for data parameters*/
static uint8_t  iAP2_NOW_PLAYING_UPDATES_Playback_Status_MaxEnumVal[]= 
 {
		 4,
		 2,
		 1,
 };

/*Pointers to maximum possible size for received data*/
static uint8_t * const iAP2_NOW_PLAYING_UPDATES_Playback_Status_subparamMaxValue[] =
{
        &(iAP2_NOW_PLAYING_UPDATES_Playback_Status_MaxEnumVal[0]), 
        NULL,	 
        NULL,
        NULL,
        NULL,
        &(iAP2_NOW_PLAYING_UPDATES_Playback_Status_MaxEnumVal[1]), //5
        &(iAP2_NOW_PLAYING_UPDATES_Playback_Status_MaxEnumVal[1]),
        &(NowPlayingUpdates.bPlaybackAppNameSize),
        &(NowPlayingUpdates.bPBMediaLibraryUniqueIdentifierSize),
        &(iAP2_NOW_PLAYING_UPDATES_Playback_Status_MaxEnumVal[2]),
        &(NowPlayingUpdates.bPBiTunesRadioStationNameSize), //10
        NULL,

};

 /*Pointers where flag should be set*/
static uint32_t * const iAP2_NOW_PLAYING_UPDATES_FlagToSet[] =
 {
		 &(NowPlayingUpdates.dwMediaItemAttributesUpdated),
		 &(NowPlayingUpdates.dwPlaybackAttributesUpdated),
 };

  /*Flag values for each received parameter */
static uint16_t const iAP2_NOW_PLAYING_UPDATES_Playback_Status_subParamFlag[] =
{
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_STATUS,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_ELAPSED_TIME, 
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_QUEUE_INDEX,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_QUEUE_COUNT,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_QUEUE_CHAPTER_INDEX,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_SHUFFLE_MODE, //5
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_REPEAT_MODE,	
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_APP_NAME,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_MEDIA_LIBRARY_UNIQUE_ID,              
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_PB_ITUNES_RADIO_AD,              
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_PB_ITUNES_RADIO_STATION_NAME,          
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_PB_ITUNES_RADIO_STATION_PERSISTENT_ID
};
 
/*Sub parameters ID to be sent */
static const uint8_t iAP2_NowPlayingUpdates_gabMediaItemAttributes[]=
{
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_TITTLE_ID,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PLAYBACK_DURATION_ID,        
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_TRACK_NUMBER_ID,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_TITLE_ID,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_TRACK_COUNT_ID,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_DISC_NUMBER_ID,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ALBUM_DISC_COUNT,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ARTIST_ID,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_GENRE_ID,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_COMPOSER_ID,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PROPERTY_IS_LIKE_SUPPORTED,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PROPERTY_IS_BAN_SUPPORTED,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PROPERTY_IS_LIKED,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_PROPERTY_IS_BANNED,
        iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ARTWORK_FILE_TRANSFER_ID 
};

/*Sub parameters ID to be sent */
static const uint8_t iAP2_NowPlayingUpdates_gaPlaybackAttributesID[]=
{
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_ELAPSED_TIME_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_QUEUE_INDEX_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_QUEUE_COUNT_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_QUEUE_CHAPTER_INDEX_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_SUFFLE_MODE_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_REAPET_MODE_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_STATUS_APP_NAME_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_MEDIA_LIBRARY_UNIQUE_ID,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_PB_ITUNES_RADIO_AD,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_PB_ITUNES_RADIO_STATION_NAME,
        iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_PB_ITUNES_RADIO_STATION_PERSISTENT_ID 
};

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
static uint16_t gwDataSize;					/**< Data Size for current parameter*/
static uint8_t gbMessageCurrentStatus;  		/**< Current message status*/

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/** ***********************************************************************************************
*
* @brief    Function used to update NowPlayingUpdates structure from Application with the new values.
* 			Moreover the validation of the correct data size is done.
* 
* @param   	* NowPlayingUpdatesDataToUpdate where will be save the updates.
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_NowPlayingUpdates_vfnUpdateAllData(iAP2_NowPlayingUpdatesData * NowPlayingUpdatesDataToUpdate)
{		
	
	NowPlayingUpdatesDataToUpdate->dwMediaItemPlaybackDuration= NowPlayingUpdates.dwMediaItemPlaybackDuration;
	NowPlayingUpdatesDataToUpdate->dwPlaybackElapsedTime= NowPlayingUpdates.dwPlaybackElapsedTime;
	NowPlayingUpdatesDataToUpdate->dwPlaybackQueueIndex= NowPlayingUpdates.dwPlaybackQueueIndex;
	NowPlayingUpdatesDataToUpdate->dwPlaybackQueueCount= NowPlayingUpdates.dwPlaybackQueueCount;
	NowPlayingUpdatesDataToUpdate->dwPlaybackQueueChapterIndex= NowPlayingUpdates.dwPlaybackQueueChapterIndex;
	NowPlayingUpdatesDataToUpdate->wMediaItemAlbumTrackNumber= NowPlayingUpdates.wMediaItemAlbumTrackNumber;
	NowPlayingUpdatesDataToUpdate->wMediaItemAlbumTrackCount= NowPlayingUpdates.wMediaItemAlbumTrackCount;
	NowPlayingUpdatesDataToUpdate->wMediaItemAlbumDiscNumber= NowPlayingUpdates.wMediaItemAlbumDiscNumber;
	NowPlayingUpdatesDataToUpdate->wMediaItemAlbumDiscCount= NowPlayingUpdates.wMediaItemAlbumDiscCount;
	NowPlayingUpdatesDataToUpdate->dwMediaItemAttributesUpdated= NowPlayingUpdates.dwMediaItemAttributesUpdated;
	NowPlayingUpdatesDataToUpdate->bMediaItemArtworkFileTransferIdentifier= NowPlayingUpdates.bMediaItemArtworkFileTransferIdentifier;
	NowPlayingUpdatesDataToUpdate->bPlaybackStatus= NowPlayingUpdates.bPlaybackStatus;
	NowPlayingUpdatesDataToUpdate->bPlaybackSuffleMode= NowPlayingUpdates.bPlaybackSuffleMode;	
	NowPlayingUpdatesDataToUpdate->bPlaybackStatusRepeatMode= NowPlayingUpdates.bPlaybackStatusRepeatMode;
	NowPlayingUpdatesDataToUpdate->dwPlaybackAttributesUpdated= NowPlayingUpdates.dwPlaybackAttributesUpdated;
	
	NowPlayingUpdatesDataToUpdate->bMediaItemPropertyIsLikeSupported = NowPlayingUpdates.bMediaItemPropertyIsLikeSupported;
	NowPlayingUpdatesDataToUpdate->bMediaItemPropertyIsBanSupported = NowPlayingUpdates.bMediaItemPropertyIsBanSupported;
	NowPlayingUpdatesDataToUpdate->bMediaItemPropertyIsLiked = NowPlayingUpdates.bMediaItemPropertyIsLiked;
	NowPlayingUpdatesDataToUpdate->bMediaItemPropertyIsBanned = NowPlayingUpdates.bMediaItemPropertyIsBanned;   
	NowPlayingUpdatesDataToUpdate->bPBiTunesRadioAd = NowPlayingUpdates.bPBiTunesRadioAd;  
	NowPlayingUpdatesDataToUpdate->qwPBiTunesRadioStationMediaPlaylistPersistentID = NowPlayingUpdates.qwPBiTunesRadioStationMediaPlaylistPersistentID;  
	
	/** @todo Function Return */
	(void)memcpy(NowPlayingUpdatesDataToUpdate->pbMediaItemMediaTitle,NowPlayingUpdates.pbMediaItemMediaTitle,NowPlayingUpdates.bMediaItemMediaTitleSize);
	/** @todo Function Return */
	(void)memcpy(NowPlayingUpdatesDataToUpdate->pbMediaItemAlbumTitle,NowPlayingUpdates.pbMediaItemAlbumTitle,NowPlayingUpdates.bMediaItemAlbumTitleSize);
	
	/** @todo Function Return */
	(void)memcpy(NowPlayingUpdatesDataToUpdate->pbMediaItemArtist,NowPlayingUpdates.pbMediaItemArtist,NowPlayingUpdates.bMediaItemArtistSize);
	/** @todo Function Return */
	(void)memcpy(NowPlayingUpdatesDataToUpdate->pbMediaItemGenre,NowPlayingUpdates.pbMediaItemGenre,NowPlayingUpdates.bMediaItemGenreSize);
	
	/** @todo Function Return */
	(void)memcpy(NowPlayingUpdatesDataToUpdate->pbMediaItemComposer,NowPlayingUpdates.pbMediaItemComposer,NowPlayingUpdates.bMediaItemComposerSize);
	/** @todo Function Return */
	(void)memcpy(NowPlayingUpdatesDataToUpdate->pbPlaybackAppName,NowPlayingUpdates.pbPlaybackAppName,NowPlayingUpdates.bPlaybackAppNameSize);
	
    /** @todo Function Return */
    (void)memcpy(NowPlayingUpdatesDataToUpdate->pbPBMediaLibraryUniqueIdentifier,NowPlayingUpdates.pbPBMediaLibraryUniqueIdentifier,NowPlayingUpdates.bPBMediaLibraryUniqueIdentifierSize);
    
    /** @todo Function Return */
    (void)memcpy(NowPlayingUpdatesDataToUpdate->pbPBiTunesRadioStationName,NowPlayingUpdates.pbPBiTunesRadioStationName,NowPlayingUpdates.bPBiTunesRadioStationNameSize);
	
}


/**************************************************************************************************
*
* @brief    Function used to when a Message was received
*
* @param    fnPtr Pointer to the function to be called once received message was validated
*			wMessageID	MesssageID 
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_NowPlayingUpdates_vfnNewRequestCaller(void(*fnPtr)(uint16_t),uint16_t wMessageID)
{
	static iAP2_NowPlayingUpdatesData NowPlayingUpdatesTmp;
	uint8_t bStatus;

	if(iAP2_FEATURES_UPDATES_MESSAGE_NEW == gbMessageCurrentStatus)
	{
		/*Initialize tmp variables*/
		NowPlayingUpdates.dwMediaItemAttributesUpdated = 0;
		NowPlayingUpdates.dwPlaybackAttributesUpdated = 0;
		/** @todo Function Return */
		(void)memcpy(&NowPlayingUpdatesTmp,&NowPlayingUpdates,sizeof(iAP2_NowPlayingUpdatesData));
		NowPlayingUpdates.pbMediaItemMediaTitle = &gabMediaItemMediaTitle[0];
		NowPlayingUpdates.pbMediaItemAlbumTitle = &gabMediaItemAlbumTitle[0];
		NowPlayingUpdates.pbMediaItemArtist = &gabMediaItemArtist[0];
		NowPlayingUpdates.pbMediaItemGenre =&gabMediaItemGenre[0];
		NowPlayingUpdates.pbMediaItemComposer = &gabMediaItemComposer[0];
		NowPlayingUpdates.pbPlaybackAppName = &gabPlaybackAppName[0];	
		NowPlayingUpdates.pbPBMediaLibraryUniqueIdentifier = &gabPBMediaLibraryUniqueIdentifier[0];
		NowPlayingUpdates.pbPBiTunesRadioStationName = &gabPBiTunesRadioStationName[0];	
	}

	/*Get all parameter from current frame*/
	bStatus = iAP2_NowPlayingUpdates_vfnGetParameter();
	if(gbMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED )
	{
		/*Return to last valid values*/
		/** @todo Function Return */
		(void)memcpy(&NowPlayingUpdates,&NowPlayingUpdatesTmp,sizeof(iAP2_NowPlayingUpdatesData));
		gbMessageCurrentStatus = iAP2_FEATURES_UPDATES_MESSAGE_NEW;
	}
	else
	{
		if(IAP2_SESS_CTRL_MESSAGE_COMPLETE == bStatus)
		{
		/*Copy received and validate information to Application structure*/
			iAP2_NowPlayingUpdates_vfnUpdateAllData(NowPlayingUpdatesDataToUpdate);		
			gbMessageCurrentStatus = iAP2_FEATURES_UPDATES_MESSAGE_NEW;
			fnPtr(wMessageID);
		}
		else
		{
			gbMessageCurrentStatus |= iAP2_FEATURES_UPDATES_MESSAGE_INCOMPLETE;
		}
	}
}


/**************************************************************************************************
*
* @brief    Function called to get Parameters and subparameter from a NowPlayingUpdate message.
* 
* @param   	void
* 
* @return  	Message status 
* 			IAP2_SESS_CTRL_PARAM_COMPLETE 
* 			IAP2_SESS_CTRL_GROUP_COMPLETE 
* 			IAP2_SESS_CTRL_MESSAGE_COMPLETE 
* 			IAP2_SESS_CTRL_DATA_PENDING
*          
**************************************************************************************************/
static uint8_t iAP2_NowPlayingUpdates_vfnGetParameter(void)
{
	static uint16_t wParamID;
	static uint16_t wSubParamId;
	uint8_t bDataStatus = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
	uint8_t bHeaderStatus;
	uint8_t bSubParamLevel;
    uint8_t maxSize = 0;

	do
	{
		bHeaderStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID,&gwDataSize);	
		bSubParamLevel = 0;

		if(IAP2_SESS_CTRL_OK == bHeaderStatus)
		{
			if(iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ID == wParamID)
			{		
				do
				{
					if(0 == bSubParamLevel)
					{
						bSubParamLevel = 1;
						bHeaderStatus = iAP2_Sessions_bfnCtrlMsgGroupStart();
					}

					bHeaderStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wSubParamId,&gwDataSize);
					if(IAP2_SESS_CTRL_OK == bHeaderStatus)
					{
						if(wSubParamId<iAP2_NOW_PLAYING_UPDATES_BIGGER_MEDIA_PARAM_ID)
						{
                            if (iAP2_NOW_PLAYING_UPDATES_Media_Item_SubparaMaxSizes[wSubParamId])
                                maxSize = *iAP2_NOW_PLAYING_UPDATES_Media_Item_SubparaMaxSizes[wSubParamId];
                            else
                                maxSize = 0;
							NowPlayingUpdates_vfnapReceivedMediaItemParameters[wSubParamId] \
								 	 ( iAP2_NOW_PLAYING_UPDATES_Media_Item_SubparamVal[wSubParamId],
								 	   maxSize,
									   iAP2_NOW_PLAYING_UPDATES_Media_Item_subParamFlag[wSubParamId],
									   iAP2_NOW_PLAYING_UPDATES_FlagToSet[0], 
									   gwDataSize,&gbMessageCurrentStatus,&bDataStatus);							
						}
						else
						{
							/* Set status to message complete to discard all the message */
							bDataStatus = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
						}
					}			
				}while((IAP2_SESS_CTRL_OK == bHeaderStatus) && (IAP2_SESS_CTRL_PARAM_COMPLETE == bDataStatus));
			}
			else
			{
				if(iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_ID == wParamID)
				{
					do
					{
						if(0 == bSubParamLevel)
						{
							bSubParamLevel = 1;
							bHeaderStatus = iAP2_Sessions_bfnCtrlMsgGroupStart();
						}
						
						bHeaderStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wSubParamId,&gwDataSize);	
						if(IAP2_SESS_CTRL_OK == bHeaderStatus)
						{
							if(wSubParamId<iAP2_NOW_PLAYING_UPDATES_BIGGER_PLAYBACK_PARAM_ID)
							{
                                if (iAP2_NOW_PLAYING_UPDATES_Playback_Status_subparamMaxValue[wSubParamId])
                                    maxSize = *iAP2_NOW_PLAYING_UPDATES_Playback_Status_subparamMaxValue[wSubParamId];
                                else
                                    maxSize = 0;
								NowPlayingUpdates_vfnapReceivedPlaybackStatusParameters[wSubParamId] \
													(iAP2_NOW_PLAYING_UPDATES_Playback_Status_SubparamVal[wSubParamId],
													 maxSize,
													 iAP2_NOW_PLAYING_UPDATES_Playback_Status_subParamFlag[wSubParamId],
													 iAP2_NOW_PLAYING_UPDATES_FlagToSet[1], 
													 gwDataSize,&gbMessageCurrentStatus,&bDataStatus);
							}
							else
							{
								/* Set status to message complete to discard all the message */
								bDataStatus = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
							}
						}
					}while((IAP2_SESS_CTRL_OK == bHeaderStatus) && (IAP2_SESS_CTRL_PARAM_COMPLETE == bDataStatus));
				}
			}
		}
	}while((IAP2_SESS_CTRL_OK == bHeaderStatus) && (IAP2_SESS_CTRL_MESSAGE_COMPLETE != bDataStatus ) && (IAP2_SESS_CTRL_DATA_PENDING != bDataStatus ));
	
	return bDataStatus;
}

/** ***********************************************************************************************
*
* @brief    Function called when a Message for NowPlayingUpdates is received .
* 
* @param   	wMessageID Identifies messageID
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_Feat_NowPlay_vfnCallBack(uint16_t wMessageID)
{
	if(iAP2_NOW_PLAYING_UPDATES == wMessageID)
	{
		iAP2_NowPlayingUpdates_vfnNewRequestCaller(&iAP2_CALLBACK_FROM_NOW_PLAYING_UPDATE,wMessageID); /*@TODO: Call correct callback*/
	}
}

/**************************************************************************************************
*
* @brief    Function called to start receiving NowPlayingUpdates messages from the iPod, iPad or
* 			iPhone device.
* 
* @param   	wMediaItemAttributes:	indicates which MediaItem updates is wanted to be received, use defines flags for each wanted to 
* 									received Playback Status parameter
* 			bPlaybackAttributes:	indicates which PlaybackStatus updates want to receive, Flags for each wanted to 
* 									received Playback Status parameter
* 			NowPlayingAppDataPtr:	the pointer to the application structure where the updates will be 
* 									saved. The application should initialize the pointer and its
* 									corresponding size for each UTF8 parameter should be set into the
* 									this structure. In case a type of Now Playing update parameter
* 									is not wanted to be received, then the related structure's elements
* 									should be set to NULL (pointers) or zero (size) accordingly.
* 									Into this structure the fields bPlaybackAttributesUpdated and 
* 									wMediaItemAttributesUpdated will indicated the received updates.
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*			iAP2_NOW_PLAYING_UPDATES_INVALID_DATA_PARAMETER 0xFF
**************************************************************************************************/
uint8_t iAP2_NowPlayingUpdates_bfnStartPlayingUpdates(uint16_t wMediaItemAttributes, uint16_t wPlaybackAttributes,iAP2_NowPlayingUpdatesData * NowPlayingAppDataPtr)
{
	uint8_t bSessionStatus;
	uint8_t bCurrentSubParam;

	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{
		gbMessageCurrentStatus = 0;
		NowPlayingUpdatesDataToUpdate=NowPlayingAppDataPtr;
		/** @todo Function Return */
		(void)memcpy(&NowPlayingUpdates,NowPlayingAppDataPtr,sizeof(iAP2_NowPlayingUpdatesData));
		if( (wMediaItemAttributes<iAP2_NOW_PLAYING_UPDATES_INVALID_MEDIA_ITEM_UPDATE_FLAG) && 
				(wPlaybackAttributes<iAP2_NOW_PLAYING_UPDATES_INVALID_PLAYBACK_STATUS_UPDATE_FLAG))
		{

			bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(iAP2_NOW_PLAYING_UPDATES_START, NULL, 0,iAP2_NOW_PLAYING_UPDATES_START_MAX_MESSAGE_SIZE);
			/*Add Media Item Attributes Sub parameters*/
			bCurrentSubParam=0;
			while((iAP2_NOW_PLAYING_UPDATES_ERROR_OK == bSessionStatus) && (bCurrentSubParam<iAP2_NOW_PLAYING_MEDIA_ITEM_MAX_VALID_PARAMETER))
			{
				if(wMediaItemAttributes&0x01)
				{
					bSessionStatus= iAP2_Sessions_bfnAddCtrlMsgParam(iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ATTRIBUTES_ID,iAP2_NowPlayingUpdates_gabMediaItemAttributes[bCurrentSubParam], NULL, 0);
				}
				wMediaItemAttributes/=2;
				bCurrentSubParam++;
			}
			if(iAP2_NOW_PLAYING_UPDATES_ERROR_OK == bSessionStatus)
			{
				/*Add Playback Status Sub parameters*/
				bCurrentSubParam=0;
				while((iAP2_NOW_PLAYING_UPDATES_ERROR_OK == bSessionStatus) && (bCurrentSubParam<iAP2_NOW_PLAYING_PLAYBACK_MAX_VALID_PARAMETER))
				{
					if(wPlaybackAttributes&0x01)
					{
						bSessionStatus= iAP2_Sessions_bfnAddCtrlMsgParam(iAP2_NOW_PLAYING_UPDATES_PLAYBACK_ATTRIBUTES_ID,iAP2_NowPlayingUpdates_gaPlaybackAttributesID[bCurrentSubParam], NULL, 0);
					}
					wPlaybackAttributes/=2;
					bCurrentSubParam++;	
				}

				if(iAP2_NOW_PLAYING_UPDATES_ERROR_OK == bSessionStatus)
				{
					bSessionStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
				}
				else
				{
					iAP2_Sessions_vfnResetMessage();
				}
			}
			else
			{
				iAP2_Sessions_vfnResetMessage();
			}
		}
		else
		{
			bSessionStatus=iAP2_NOW_PLAYING_UPDATES_INVALID_DATA_PARAMETER;
		}
		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}
	return bSessionStatus;
}

/** ***********************************************************************************************
*
* @brief    Function called to stop receiving NowPlayingUpdates from iPod, iPad or
* 			iPhone device.
* 
* @param   void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_NowPlayingUpdates_bfnStopPlayingUpdates(void)
{
	uint8_t bSessionStatus;

	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{
		gbMessageCurrentStatus = 0;
		bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(iAP2_NOW_PLAYING_UPDATES_STOP, NULL, 0, 0);


		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}
	return bSessionStatus;
}


/*Unit testing purposes*/
void iAP2_NowPlayingUpdates_vfnDummy(uint16_t MessageID)
{
	(void)MessageID;
}
