/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_MediaLibraryAccess_Feat.h"
#include "iAP2_Features.h"
#include "iAP2_Sessions_Control.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define VALID_MEDIA_ITEM_PROPERTIES_BITS 		(0x7FFFFFF)
#define VALID_MEDIA_PLAYLIST_PROPERTIES_BITS 	(0x7F)
#define MEDIA_LIBRARY_START_UPDATES_MAX_SIZE	(0xFF)
#define MEDIA_LIBRARY_STOP_UPDATES_MAX_SIZE		(0xFF)
#define PLAY_COLLECTION_MAX_SIZE				(0x5E)	
#define COLLECTION_PERSISTENT_ID_SIZE			(0x08)
#define U64_MAX_SIZE							(0x08)
#define NO_DATA									(0x00)
#define MAX_COLLECTION_TYPE_VALUE 				(0x05)

/* Flags */

#define FULL_DATABASE_FLAG						(0x20)
#define INVALID_MSG_FLAG						(0x40)
#define MEDIA_LIBRARY_CORRUPTED_DATA_MASK		(0x80)

/* Error Messages */
#define MEDIA_LIBRARY_INFO_DATA_CORRUPTED       (0x04)

/* Start Media Library Updates */
#define UPDATES_MEDIA_LIBRRY_UNIQUE_ID			(0x00)
#define UPDATES_LAST_KNOWN_MEDIA_LIBRARY_REV	(0x01)
#define UPDATES_MEDIA_ITEM_PROPERTIES			(0x02)
#define UPDATES_MEDIA_PLAYLIST_PROPERTIES		(0x03)
#define UPDATES_MEDIA_UPDATE_PROGRESS			(0x04)
#define UPDATES_MEDIA_HIDING_REMOTE_ITEMS		(0x05)

/* Stop Media Library Updates */
#define MEDIA_LIBRARY_STOP_UPDATE_NEW_MSG		(0x00)
#define MEDIA_LIBRARY_STOP_UPDATE_ADDED_PARAM	(0x01)
#define STOP_UPDATE_MEDIA_LIBRRY_UNIQUE_ID		(0x00)

/* Media Library Information Message */
#define MEDIA_LIBRARY_INFO_ID					(0x00)
#define MEDIA_LIBRARY_INFO_MAX_SUBPARAM_VALUE	(0x02)

/* Play Media Library Items Message */
#define PLAY_ITEM_PERSISTENT_IDS				(0x00)
#define PLAY_ITEMS_STARTING_INDEX				(0x01)
#define PLAY_MEDIA_LIBRARY_UNIQUE_ID			(0x02)

/* Play Media Library Collection Message */
#define COLLECTION_PERSISTENT_ID				(0x00)
#define COLLECTION_TYPE							(0x01)
#define COLLECTION_STARTING_INDEX				(0x02)
#define COLLECTION_MEDIA_LIBRARY_UNIQUE_ID		(0x03)

/* Media Library Updates Messages */
#define MEDIA_LIBRARY_UPDATES_UNIQUE_ID				(0x00)
#define MEDIA_LIBRARY_UPDATES_REVISION				(0x01)
#define MEDIA_LIBRARY_UPDATES_ITEM 					(0x02)
#define MEDIA_LIBRARY_UPDATES_PLAYLIST				(0x03)
#define MEDIA_LIBRARY_UPDATES_DELETED_ITEM_ID		(0x04)
#define MEDIA_LIBRARY_UPDATES_DELETED_PLAYLIST_ID	(0x05)
#define MEDIA_LIBRARY_UPDATES_RESET					(0x06)
#define MEDIA_LIBRARY_UPDATE_PROGRESS				(0x07)
#define MEDIA_LIBRARY_IS_HIDING_REMOTE_ITEMS	    (0x08)

#define MEDIA_LIBRARY_UPDATES_READ_NO_GROUP_PARAM 	(0x00)
#define MEDIA_LIBRARY_UPDATES_READ_GROUP_PARAM 		(0x01)





/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

iAP2_MediaLibraryPlaylistUpdateType iAP2_MediaLibraryPlaylistUpdate;
iAP2_MediaLibraryItemUpdateType iAP2_MediaLibraryItemUpdate;

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static uint8_t iAP2_MediaLibrary_gbStopUpdateSequence;
static uint8_t iAP2_MediaLibrary_gbFrameStatus;
static uint8_t iAP2_MediaLibrary_gbStatusFlags;
static uint8_t iAP2_MediaLibrary_gbaInfoName[UFT8_MAX_SIZE];
static uint8_t iAP2_MediaLibrary_gbaInfoUniqueID[UFT8_MAX_SIZE];
static uint8_t iAP2_MediaLibrary_gbaUpdateIniqueID[UFT8_MAX_SIZE];
static uint8_t iAP2_MediaLibrary_gbInfoType;
static uint16_t iAP2_MediaLibrary_gbaInfoNameSize;
static uint16_t iAP2_MediaLibrary_gbaInfoUniqueIDSize;
static uint16_t iAP2_MediaLibrary_gbaUpdateIniqueIDSize;
static uint16_t iAP2_MediaLibrary_gbInfoTypeSize;
static uint32_t iAP2_MediaLibrary_dwUpdatedProperties;

static uint8_t iAP2_MediaLibrary_gbaInfoName1[UFT8_MAX_SIZE];
static uint8_t iAP2_MediaLibrary_gbaInfoUniqueID1[UFT8_MAX_SIZE];
static uint8_t iAP2_MediaLibrary_gbInfoType1;


static iAP2_MediaLibraryInfoData iAP2_MediaLibrary_gsInfo = 
{
		&iAP2_MediaLibrary_gbaInfoName[0],
		&iAP2_MediaLibrary_gbaInfoUniqueID[0],
		&iAP2_MediaLibrary_gbInfoType
};

static iAP2_MediaLibraryInfoSize iAP2_MediaLibrary_gsInfoSize = 
{
		&iAP2_MediaLibrary_gbaInfoNameSize,
		&iAP2_MediaLibrary_gbaInfoUniqueIDSize,
		&iAP2_MediaLibrary_gbInfoTypeSize
};

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

static void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML(void * vpsubparamVal,uint8_t bsubparamMaxSize,
		uint32_t wsubParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);

static void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum_ML(void* vpsubparamvalue, uint8_t bmaxValue,
		uint32_t wsubParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, 
		uint8_t * bpCurrentFrame);

/* Pointers to functions to be executed according to received ParameterID when a media library 
 * information message arrives */
void (* const iAP_MediaLibrary_vfnapInfo[]) (void* subparamvalue,
		uint8_t maxValue, uint32_t subParamFlag, uint32_t* wpFlagToSet, uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, 
		uint8_t * bpCurrentFrame) =
		{
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML, 	/* 0x00 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML,	/* 0x01*/
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum	/* 0x02*/
		};

/* Pointers to functions to be executed according to received ParameterID when a media library 
 * update message arrives */
void (* const iAP_MediaLibrary_vfnapUpdate[]) (void* subparamvalue,
		uint8_t maxValue, uint32_t subParamFlag, uint32_t* wpFlagToSet, uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, 
		uint8_t * bpCurrentFrame) =
		{		
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML, /* 0x00 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML,	/* 0x01 */
				iAP2_FEATURES_UPDATES_InvalidParameter,			/* 0x02 */
				iAP2_FEATURES_UPDATES_InvalidParameter,			/* 0x03 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64, 	/* 0x04 */ 
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64, 	/* 0x05 */ 
				iAP2_FEATURES_UPDATES_InvalidParameter, 		/* 0x06 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 		/* 0x07 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8		/* 0x08 */
		};

/* Pointers to functions to be executed according to received Media Item SubParameterID when a 
 * media library update message arrives */
void (* const iAP_MediaLibrary_vfnapUpdateItem[]) (void* subparamvalue,
		uint8_t maxValue, uint32_t subParamFlag, uint32_t *wpFlagToSet, uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, 
		uint8_t * bpCurrentFrame) =
		{		
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64,		/* 0x00 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML,	/* 0x01 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum_ML,	/* 0x02 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8,		/* 0x03 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32, 	/* 0x04 */ 
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64, 	/* 0x05 */ 
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML, 		/* 0x06 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16, 		/* 0x07 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16,	/* 0x08 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16,			/* 0x09 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16,			/* 0x0A */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64, 	/* 0x0B */ 
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML, 	/* 0x0C */ 
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64, 		/* 0x0D */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML, 		/* 0x0E */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64,	/* 0x0F */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML,			/* 0x10 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64,			/* 0x11 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML, 	/* 0x12 */ 
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 	/* 0x13 */ 
				iAP2_FEATURES_UPDATES_InvalidParameter,/* 0x14 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 	/* 0x15 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 	/* 0x16 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 	/* 0x17 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 	/* 0x18 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 	/* 0x19 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8 	/* 0x19 */
		};

/* Pointers to functions to be executed according to received media playlist SubParameterID when a 
 * media library update message arrives */
void (* const iAP_MediaLibrary_vfnapUpdatePlaylist[]) (void* subparamvalue,
		uint8_t maxValue, uint32_t subParamFlag, uint32_t* wpFlagToSet, uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, 
		uint8_t * bpCurrentFrame) =
		{		
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64,		/* 0x00 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML,	/* 0x01 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64,	/* 0x02 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8,		/* 0x03 */
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 	/* 0x04 */ 
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8, 	/* 0x05 */ 
				iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8 	/* 0x06 */ 
		};

/* Pointers to variables where received info will be stored */
static void * const iAP2_MediaLibrary_vapInfoAddress[2][3]=
{
		{
            &iAP2_MediaLibrary_gbaInfoName[0],
            &iAP2_MediaLibrary_gbaInfoUniqueID[0],
            &iAP2_MediaLibrary_gbInfoType
		},
        {
            &iAP2_MediaLibrary_gbaInfoName1[0],
            &iAP2_MediaLibrary_gbaInfoUniqueID1[0],
            &iAP2_MediaLibrary_gbInfoType1
        }
};

/* Pointers to variables where received updates will be stored */
static void * const iAP2_MediaLibrary_vapUpdateItemAddress[]=
{
		&iAP2_MediaLibraryItemUpdate.qwPersistentID,		/* 0x00 */
#if iAP2_MEDIA_LIBRARY_ITEM_TITLE == _TRUE_		
		&iAP2_MediaLibraryItemUpdate.baTitle,	/* 0x01 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_MEDIA_TYPE == _TRUE_
		&iAP2_MediaLibraryItemUpdate.baType,	/* 0x02 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_RAITING == _TRUE_		
		&iAP2_MediaLibraryItemUpdate.bRating,		/* 0x03 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_DURATION_MS == _TRUE_
		&iAP2_MediaLibraryItemUpdate.dwPlaybackDuration, 	/* 0x04 */ 
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ALBUM_PERSISTENT_ID == _TRUE_	
		&iAP2_MediaLibraryItemUpdate.qwAlbumPersistentID, 	/* 0x05 */ 
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ALBUM_TITLE == _TRUE_		
		&iAP2_MediaLibraryItemUpdate.baAlbumTitle, 		/* 0x06 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ALBUM_TRACK_NUM == _TRUE_		
		&iAP2_MediaLibraryItemUpdate.wAlbumTrackNum, 		/* 0x07 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ALBUM_TRACK_CNT == _TRUE_		
		&iAP2_MediaLibraryItemUpdate.wAlbumTrackCnt,	/* 0x08 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ALBUM_DISC_NUM == _TRUE_
		&iAP2_MediaLibraryItemUpdate.wAlbumDiscNum,			/* 0x09 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ALBUM_DISC_CNT == _TRUE_
		&iAP2_MediaLibraryItemUpdate.wAlbumDiscCnt,			/* 0x0A */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ARTIST_PERSISTENT_ID == _TRUE_
		&iAP2_MediaLibraryItemUpdate.qwArtistPersistentID, 	/* 0x0B */ 
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ARTIST == _TRUE_		
		&iAP2_MediaLibraryItemUpdate.baArtist, 	/* 0x0C */ 
#else
		NULL,
#endif	
#if iAP2_MEDIA_LIBRARY_ITEM_ALBUM_ARTIST_PERSISTENT_ID == _TRUE_
		&iAP2_MediaLibraryItemUpdate.qwAlbumArtistPersistentID, 		/* 0x0D */
#else
		NULL,
#endif		
#if iAP2_MEDIA_LIBRARY_ITEM_ALBUM_ARTIST == _TRUE_
		&iAP2_MediaLibraryItemUpdate.baAlbumArtist, 		/* 0x0E */
#else
		NULL,
#endif	
#if iAP2_MEDIA_LIBRARY_ITEM_GENRE_PERSISTENT_ID == _TRUE_
		&iAP2_MediaLibraryItemUpdate.qwGenrePersistentID,	/* 0x0F */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_GENRE == _TRUE_
		&iAP2_MediaLibraryItemUpdate.baGenre,			/* 0x10 */
#else
		NULL,
#endif	
#if iAP2_MEDIA_LIBRARY_ITEM_COMPOSER_PERSISTENT_ID == _TRUE_
		&iAP2_MediaLibraryItemUpdate.ComposerPersistentID,			/* 0x11 */
#else
		NULL,
#endif	
#if iAP2_MEDIA_LIBRARY_ITEM_COMPOSER == _TRUE_
		&iAP2_MediaLibraryItemUpdate.baComposer, 	/* 0x12 */ 
#else
		NULL,
#endif			
#if iAP2_MEDIA_LIBRARY_ITEM_IS_PART_OF_COMPILATION == _TRUE_
		&iAP2_MediaLibraryItemUpdate.bPartOfCompilation, 	/* 0x13 */ 
#else
		NULL,
#endif
		NULL,					/*0x14*/
#if iAP2_MEDIA_LIBRARY_ITEM_IS_LIKE_SUPPORTED == _TRUE_
		&iAP2_MediaLibraryItemUpdate.bIsLikeSupported, 		/* 0x15 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_IS_BAN_SUPPORTED == _TRUE_
		&iAP2_MediaLibraryItemUpdate.bIsBanSupported, 		/* 0x16 */
#else
		NULL,
#endif	
#if iAP2_MEDIA_LIBRARY_ITEM_IS_LIKED == _TRUE_
		&iAP2_MediaLibraryItemUpdate.bIsLiked, 		/* 0x17 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_IS_BANNED == _TRUE_
		&iAP2_MediaLibraryItemUpdate.bIsBanned, 		/* 0x18 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_IS_RESIDENT_ON_DEVICE == _TRUE_
		&iAP2_MediaLibraryItemUpdate.bIsResidentOnDevice, 		/* 0x19 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_ITEM_ARTWORK_FILE_TRANSFER_ID == _TRUE_
		&iAP2_MediaLibraryItemUpdate.bArtworkFileTransferID/* 0x1A */
#else
		NULL
#endif
};

/* Pointers to variables where received updates will be stored */
static void * const iAP2_MediaLibrary_vapUpdatePlaylistAddress[]=
{
		&iAP2_MediaLibraryPlaylistUpdate.qwPersistentID,		/* 0x00 */
#if iAP2_MEDIA_LIBRARY_PLAYLIST_NAME == _TRUE_
		&iAP2_MediaLibraryPlaylistUpdate.baName,	/* 0x01 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_PLAYLIST_PARENT_PERSISTENT_ID == _TRUE_
		&iAP2_MediaLibraryPlaylistUpdate.qwParentPersistentID,	/* 0x02 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_PLAYLIST_GENIUS_MIX == _TRUE_
		&iAP2_MediaLibraryPlaylistUpdate.bGeniusMix,		/* 0x03 */
#else
		NULL,
#endif
#if iAP2_MEDIA_LIBRARY_PLAYLIST_IS_FOLDER == _TRUE_
		&iAP2_MediaLibraryPlaylistUpdate.bListIsFolder, 	/* 0x04 */ 
#else
		NULL,
#endif	
#if iAP2_MEDIA_LIBRARY_PLAYLIST_CONTAINED_ITEMS == _TRUE_
		&iAP2_MediaLibraryPlaylistUpdate.bContaindedMediaItemFileTransferID, 	/* 0x05 */ 
#else
		NULL,
#endif	
#if iAP2_MEDIA_LIBRARY_PLAYLIST_iTUNES_RADIO_STATION == _TRUE_
		&iAP2_MediaLibraryPlaylistUpdate.bIsiTunesRadioStation 	/* 0x06 */ 
#else
		NULL
#endif	
};

/* Array containing max subparameters allowed sizes */
static uint8_t const iAP2_MediaLibrary_vapInfoSubParamSize[]=
{
		UFT8_MAX_SIZE,
		UFT8_MAX_SIZE,
		sizeof(uint8_t)
};

/* Array containing max parameters allowed sizes from media update message */
static uint8_t const iAP2_MediaLibrary_vapUpdateParamSize[]=
{
		(uint8_t)UFT8_MAX_SIZE, //0x0
		(uint8_t)UFT8_MAX_SIZE, //0x1
		0, 		//0x2
		0,		//0x3
		(uint8_t)U64_MAX_SIZE, //0x4
		(uint8_t)U64_MAX_SIZE, //0x5
		0,			//0x6
		(uint8_t)sizeof(uint8_t),		//0x7
		(uint8_t)sizeof(uint8_t)		//0x8
};


/* Array containing max parameters allowed sizes from media item group of the media 
 * library update */
static uint8_t const iAP2_MediaLibrary_vapUpdateItemSubParamSize[]=
{
		(uint8_t)U64_MAX_SIZE,		/* 0x00 */
		(uint8_t)UFT8_MAX_SIZE,	/* 0x01 */
		(uint8_t)sizeof(uint8_t),	/* 0x02 */
		(uint8_t)sizeof(uint8_t),		/* 0x03 */
		(uint8_t)sizeof(uint32_t), 	/* 0x04 */ 
		(uint8_t)U64_MAX_SIZE, 	/* 0x05 */ 
		(uint8_t)UFT8_MAX_SIZE, 		/* 0x06 */
		(uint8_t)sizeof(uint16_t), 		/* 0x07 */
		(uint8_t)sizeof(uint16_t),	/* 0x08 */
		(uint8_t)sizeof(uint16_t),			/* 0x09 */
		(uint8_t)sizeof(uint16_t),			/* 0x0A */
		(uint8_t)U64_MAX_SIZE, 	/* 0x0B */ 
		(uint8_t)UFT8_MAX_SIZE, 	/* 0x0C */ 
		(uint8_t)U64_MAX_SIZE, 		/* 0x0D */
		(uint8_t)UFT8_MAX_SIZE, 		/* 0x0E */
		(uint8_t)U64_MAX_SIZE,	/* 0x0F */
		(uint8_t)UFT8_MAX_SIZE,			/* 0x10 */
		(uint8_t)U64_MAX_SIZE,			/* 0x11 */
		(uint8_t)UFT8_MAX_SIZE, 	/* 0x12 */ 
		(uint8_t)sizeof(uint8_t), 	/* 0x13 */ 
		0, 	/* 0x14 */ 
		(uint8_t)sizeof(uint8_t), 		/* 0x15 */
		(uint8_t)sizeof(uint8_t), 		/* 0x16 */
		(uint8_t)sizeof(uint8_t), 		/* 0x17 */
		(uint8_t)sizeof(uint8_t), 		/* 0x18 */
		(uint8_t)sizeof(uint8_t)   		/* 0x19 */
};

/* Array containing max parameters allowed sizes from media item group of the media 
 * library update */
static uint8_t const iAP2_MediaLibrary_vapUpdatePlaylistSubParamSize[]=
{
		(uint8_t)U64_MAX_SIZE,		/* 0x00 */
		(uint8_t)UFT8_MAX_SIZE,	/* 0x01 */
		(uint8_t)U64_MAX_SIZE,	/* 0x02 */
		(uint8_t)sizeof(uint8_t),		/* 0x03 */
		(uint8_t)sizeof(uint8_t), 	/* 0x04 */ 
		(uint8_t)sizeof(uint8_t), 	/* 0x05 */ 
		(uint8_t)sizeof(uint8_t) 		/* 0x06 */ 
};

/* Array containing flags from media item group of the media library update */
static uint32_t const iAP2_MediaLibrary_vapUpdateItemSubParamUpdateFlag[]=
{
#if iAP2_MEDIA_LIBRARY_ITEM_PROPERTIES_ENABLE == _TRUE_
		iAP2_MEDIA_LIBRARY_ITEM_PERSISTENT_ID_MASK,				/* 0x00 */
		iAP2_MEDIA_LIBRARY_ITEM_TITLE_MASK, 						/* 0x01 */
		iAP2_MEDIA_LIBRARY_ITEM_MEDIA_TYPE_MASK,					 /* 0x02 */
		iAP2_MEDIA_LIBRARY_ITEM_RAITING_MASK,						/* 0x03 */
		iAP2_MEDIA_LIBRARY_ITEM_DURATION_MS_MASK,					/* 0x04 */ 
		iAP2_MEDIA_LIBRARY_ITEM_ALBUM_PERSISTENT_ID_MASK,			/* 0x05 */ 
		iAP2_MEDIA_LIBRARY_ITEM_ALBUM_TITLE_MASK,						/* 0x06 */
		iAP2_MEDIA_LIBRARY_ITEM_ALBUM_TRACK_NUM_MASK,				/* 0x07 */
		iAP2_MEDIA_LIBRARY_ITEM_ALBUM_TRACK_CNT_MASK,				/* 0x08 */
		iAP2_MEDIA_LIBRARY_ITEM_ALBUM_DISC_NUM_MASK	,					/* 0x09 */
		iAP2_MEDIA_LIBRARY_ITEM_ALBUM_DISC_CNT_MASK,				/* 0x0A */
		iAP2_MEDIA_LIBRARY_ITEM_ARTIST_PERSISTENT_ID_MASK,		/* 0x0B */ 
		iAP2_MEDIA_LIBRARY_ITEM_ARTIST_MASK,						/* 0x0C */ 
		iAP2_MEDIA_LIBRARY_ITEM_ALBUM_ARTIST_PERSISTENT_ID_MASK,		/* 0x0D */
		iAP2_MEDIA_LIBRARY_ITEM_ALBUM_ARTIST_MASK,					/* 0x0E */
		iAP2_MEDIA_LIBRARY_ITEM_GENRE_PERSISTENT_ID_MASK,			/* 0x0F */
		iAP2_MEDIA_LIBRARY_ITEM_GENRE_MASK,							/* 0x10 */
		iAP2_MEDIA_LIBRARY_ITEM_COMPOSER_PERSISTENT_ID_MASK	,			/* 0x11 */
		iAP2_MEDIA_LIBRARY_ITEM_COMPOSER_MASK,					/* 0x12 */ 
		iAP2_MEDIA_LIBRARY_ITEM_IS_PART_OF_COMPILATION_MASK,		/* 0x13 */ 
		0,														/*0x14*/
		iAP2_MEDIA_LIBRARY_ITEM_IS_LIKE_SUPPORTED_MASK,				/*0x15*/
		iAP2_MEDIA_LIBRARY_ITEM_IS_BAN_SUPPORTED_MASK,				/*0x16*/		
		iAP2_MEDIA_LIBRARY_ITEM_IS_LIKED_MASK,						/*0x17*/
		iAP2_MEDIA_LIBRARY_ITEM_IS_BANNED_MASK,						/*0x18*/
		iAP2_MEDIA_LIBRARY_ITEM_IS_RESIDENT_ON_DEVICE_MASK,			/*0x19*/
		iAP2_MEDIA_LIBRARY_ITEM_ARTWORK_FILE_TRANSFER_ID_MASK       /*0x1A*/
#else
		0,		/* 0x00 */
		0, 		/* 0x01 */
		0,		/* 0x02 */
		0,		/* 0x03 */
		0,		/* 0x04 */ 
		0,		/* 0x05 */ 
		0,		/* 0x06 */
		0,		/* 0x07 */
		0,		/* 0x08 */
		0,		/* 0x09 */
		0,		/* 0x0A */
		0,		/* 0x0B */ 
		0,		/* 0x0C */ 
		0,		/* 0x0D */
		0,		/* 0x0E */
		0,		/* 0x0F */
		0,		/* 0x10 */
		0,       /* 0x11 */
		0,		/* 0x12 */ 
		0,		/* 0x13 */ 
		0,		/* 0x14 */
		0,		/* 0x15 */ 
		0,		/* 0x16 */
		0,		/* 0x17 */ 
		0,		/* 0x18 */
		0		/* 0x19 */ 
#endif
};

/* Array containing flags from media playlist group of the media library update */
static uint32_t const iAP2_MediaLibrary_vapUpdatePlaylistSubParamUpdateFlag[]=
{
#if iAP2_MEDIA_LIBRARY_PLAYLIST_PROPERTIES_ENABLE == _TRUE_
		iAP2_MEDIA_LIBRARY_ITEM_PERSISTENT_ID_MASK ,                     /* 0x00 */
		iAP2_MEDIA_LIBRARY_PLAYLIST_NAME_MASK,				           /* 0x01 */
		iAP2_MEDIA_LIBRARY_PLAYLIST_PARENT_PERSISTENT_ID_MASK,           /* 0x02 */
		iAP2_MEDIA_LIBRARY_PLAYLIST_GENIUS_MIX_MASK,			           /* 0x03 */
		iAP2_MEDIA_LIBRARY_PLAYLIST_IS_FOLDER_MASK,			           /* 0x04 */ 
		iAP2_MEDIA_LIBRARY_PLAYLIST_CONTAINED_ITEMS_MASK,	           /* 0x05 */ 
		iAP2_MEDIA_LIBRARY_PLAYLIST_iTUNES_RADIO_STATION_MASK			/*0x06*/
#else
		0,                     /* 0x00 */
		0,				           /* 0x01 */
		0,           /* 0x02 */
		0,			           /* 0x03 */
		0,			           /* 0x04 */ 
		0,	           /* 0x05 */
		0			 /* 0x06 */
#endif
};

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static uint8_t bfnStringLength(uint8_t *bpString);
static void iAP2_MediaLibrary_vfnUpdates(void(*fnPtr)(uint16_t, uint8_t*, uint16_t), uint16_t wMessageID);
static void iAP2_MediaLibrary_vfnInformation(void(*fnPtr)(iAP2_MediaLibraryInfoData *,
		iAP2_MediaLibraryInfoSize *), uint16_t wMessageID);
static void iAP2_FEATURES_UPDATES_DataUtf8_ML(uint8_t * bpData, uint8_t bmaxSize,uint16_t wDataSize,
		uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
static void iAP2_FEATURES_UPDATES_Data8_Enum_ML(uint8_t * bpData,uint8_t bmaxValue,uint16_t wDataSize,
		uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
/*************************************************************************************************/
/*                                    Global Functions Section                                   */
/*************************************************************************************************/

/** ***********************************************************************************************
 *
 * @brief    Function called when a Message for MediaLibrary is received.
 * 
 * @param   	wMessageID: Message ID
 * 
 * @return  	void
 *          
 **************************************************************************************************/
void iAP2_Feat_MediaLib_vfnCallBack(uint16_t wMessageID)
{

	if(IAP2_MEDIA_LIBRARY_INFO == wMessageID)
	{
		iAP2_MediaLibrary_vfnInformation(&iAP2_MEDIA_LIBRARY_INFO_CALLBACK, wMessageID);
	}
	else if(IAP2_MEDIA_LIBRARY_UPDATE == wMessageID)
	{
		iAP2_MEDIA_LIBRARY_UPDATES_CALLBACK();
	}
	else
	{
		/* Do nothing */
	}
}

/** ***********************************************************************************************
 *
 * @brief    Function called to start receiving media library information from device.
 * 
 * @param   	void
 * 
 * @return  	0x00  MEDIA_LIBRARY_OK.  	Message was sent to the lower layers in order to be 
 * 										transmitted.
 * 
 * 			0x01  MEDIA_LIBRARY_ERR_TRY_LATER. Message could not be sent to the lower layers 
 * 												and will not be transmitted. Application 
 * 												should try sending message later.
 * 
 *			0x02  MEDIA_LIBRARY_ERR_INTERNAL. 	An internal error has occurred. Application 
 *												should try sending message later.
 *												
 *			0x03  MEDIA_LIBRARY_ERR_FATAL. An error has occurred and message could not be 
 *											sent and wont be able to do so.
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_bfnStartInfo(void)
{
	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = IAP2_MEDIA_LIBRARY_RET_OK;
	bMediaLibraryRetStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_START_MEDIA_LIBRARY_INFO, NULL, 0, 0);
	if(IAP2_MEDIA_LIBRARY_RET_OK != bMediaLibraryRetStatus)
	{
		/* Message could not be form correctly and will be discarded */
		iAP2_Sessions_vfnResetMessage();
	}
	return bMediaLibraryRetStatus;
}


/** ***********************************************************************************************
 *
 * @brief    Function called to stop receiving media library information from device.
 * 
 * @param   void
 * 
 * @return  	0x00  MEDIA_LIBRARY_OK.  	Message was sent to the lower layers in order to be 
 * 										transmitted.
 * 
 * 			0x01  MEDIA_LIBRARY_ERR_TRY_LATER. Message could not be sent to the lower layers 
 * 												and will not be transmitted. Application 
 * 												should try sending message later.
 * 
 *			0x02  MEDIA_LIBRARY_ERR_INTERNAL. 	An internal error has occurred. Application 
 *												should try sending message later.
 *												
 *			0x03  MEDIA_LIBRARY_ERR_FATAL. An error has occurred and message could not be 
 *											sent and wont be able to do so.
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_bfnStopInfo(void)
{
	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = IAP2_MEDIA_LIBRARY_RET_OK;
	bMediaLibraryRetStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_STOP_MEDIA_LIBRARY_INFO, NULL, 0, 0);
	if(IAP2_MEDIA_LIBRARY_RET_OK != bMediaLibraryRetStatus)
	{
		/* Message could not be form correctly and will be discarded */
		iAP2_Sessions_vfnResetMessage();
	}
	return bMediaLibraryRetStatus;
}

/** ***********************************************************************************************
 *
 * @brief    Function called to start receiving media library updates from device. This message 
 * 			must not be send more than once per iAP2 connection
 * 
 * @param   void
 * 
 * @return  	If the message could be sent
 *			SESSIONS_OK 		0
 *			SESSIONS_BUSY 		1
 *			SESSIONS_BUSY 		2
 *			SESSIONS_MEM_FULL	3
 *			SESSIONS_WRONG_INIT 4
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_bfnStartUpdates(uint8_t *bpMediaLibrarytUniqueID, uint8_t *bpLastKnownMediaLibraryRev,
		uint32_t dwMediaItemProperties, uint8_t bMediaPlaylistProperties, uint8_t bMediaLibrayFlags)
{
	uint16_t wMediaLibrarytUniqueIDSize;
	uint16_t wLastKnownMediaLibraryRevSize;

	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = IAP2_MEDIA_LIBRARY_RET_OK;

	uint8_t bSubParameterCnt = 0x00;

	/* Keep only valid data from variables received */
	dwMediaItemProperties &= VALID_MEDIA_ITEM_PROPERTIES_BITS;
	bMediaPlaylistProperties &=  VALID_MEDIA_PLAYLIST_PROPERTIES_BITS;

	/* Retrieve strings size */
	wMediaLibrarytUniqueIDSize = bfnStringLength(bpMediaLibrarytUniqueID);
	wLastKnownMediaLibraryRevSize = bfnStringLength(bpLastKnownMediaLibraryRev);

	/* if pointer receive for media library unique identifier is NOT NULL */
	if(0 != wMediaLibrarytUniqueIDSize)
	{
		/* If the message will only have one parameter */
		if((0 == wLastKnownMediaLibraryRevSize)&&(0 == dwMediaItemProperties)&&
				(0 == bMediaPlaylistProperties))
		{
			//			/* If first time requesting full database */
			//			if(!(iAP2_MediaLibrary_gbStatusFlags & FULL_DATABASE_FLAG))
			//			{
			//				/* Set the already requested full update flag */
			//				iAP2_MediaLibrary_gbStatusFlags |= FULL_DATABASE_FLAG;
			/* Call sessions layer to start forming message */
			bMediaLibraryRetStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_START_MEDIA_LIBRARY_UPDATES, 
					bpMediaLibrarytUniqueID, wMediaLibrarytUniqueIDSize, 
					wMediaLibrarytUniqueIDSize + CTRL_MSG_PARAMHEADER_SIZE + CTRL_MSG_HEADER_SIZE);
			//			}
		}
		else /* If more than one parameter will be sent */
		{
			/* Call sessions layer to start forming message */
			bMediaLibraryRetStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_START_MEDIA_LIBRARY_UPDATES,
					NULL, NO_DATA, MEDIA_LIBRARY_START_UPDATES_MAX_SIZE);

			/* If no error while starting message */
			if(IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus)
			{	
				/* Set parameter */
				bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(UPDATES_MEDIA_LIBRRY_UNIQUE_ID,
						IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, bpMediaLibrarytUniqueID, 
						wMediaLibrarytUniqueIDSize);
				/* If no error while sending media library unique id */
				if(IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus)
				{
					/* If last known media library revision parameter is present */
					if (0 != wLastKnownMediaLibraryRevSize)
					{
						bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(UPDATES_LAST_KNOWN_MEDIA_LIBRARY_REV,
								IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, bpLastKnownMediaLibraryRev,
								wLastKnownMediaLibraryRevSize);
					}
					else /* If last known media library revision parameter is not present */
					{
						//					/* If NOT first time requesting full database */
						//					if(iAP2_MediaLibrary_gbStatusFlags & FULL_DATABASE_FLAG)
						//					{
						//						/* Invalid Message due to accessory cannot request a full database more than once 
						//						 * during an iAP2 connection */
						//						iAP2_MediaLibrary_gbStatusFlags |= INVALID_MSG_FLAG;
						//						bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
						//					}
						//					else
						//					{
						//						iAP2_MediaLibrary_gbStatusFlags |= FULL_DATABASE_FLAG;
						//					}
					}
					if(IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus)
					{
						/* If any media item property will be set */
						if(0 != dwMediaItemProperties)
						{
							/* Set media item persist identifier property since is not optional */
							dwMediaItemProperties |= 0x01;
							/* While no error and sub-parameter variable is below max sub-parameter */
							while((IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus) && 
									(bSubParameterCnt < iAP2_MEDIA_MAX_MEDIA_ITEM_PROPERTY_SUBPARAM))
							{
								/* If property is present */
								if(dwMediaItemProperties & (0x01 << bSubParameterCnt))
								{
									/* Add media item properties sub-parameters */
									bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(UPDATES_MEDIA_ITEM_PROPERTIES,
											bSubParameterCnt, NULL, NO_DATA);
								}
								/* Increase sub-parameter count */
								bSubParameterCnt++;
							}
						}
					}
					if(IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus)
					{
						/* If any media play-list property will be set */
						if(0 != bMediaPlaylistProperties)
						{
							/* clear sub-parameter count */
							bSubParameterCnt = 0x00;
							/* Set media play-list persistent identifier property since is not optional */
							bMediaPlaylistProperties |= 0x01;
							/* While no error and sub-parameter variable is below max sub-parameter */
							while((IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus) && 
									(bSubParameterCnt < iAP2_MAX_MEDIA_PLAYLIST_PROPERTY_SUBPARAM))
							{
								/* If property is present */
								if(bMediaPlaylistProperties & (0x01 << bSubParameterCnt))
								{
									/* Add media item properties sub-parameters */
									bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(UPDATES_MEDIA_PLAYLIST_PROPERTIES,
											bSubParameterCnt, NULL, NO_DATA);
								}
								/* Increase sub-parameter count */
								bSubParameterCnt++;
							}
						}
					}	
					if(IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus)
					{
						/* If MediaLibraryUpdateProgres has been set */
						if((iAP2_MEDIA_LIBRARY_UPDATES_UPDATE_PROGRESS) & bMediaLibrayFlags)
						{

							bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(UPDATES_MEDIA_UPDATE_PROGRESS,
									IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, NULL,0);

						}
						if(IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus)
						{
							/* If MediaLibraryIsHidingRemoteItems has been set */
							if((iAP2_MEDIA_LIBRARY_UPDATES_IS_HIDING_REMOTE_ITEMS) & bMediaLibrayFlags)
							{
								bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(UPDATES_MEDIA_HIDING_REMOTE_ITEMS,
										IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, NULL,0);
							}
						}
					}
					if(IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus)
					{
						/* Send Start Updates Message */
						bMediaLibraryRetStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
					}		
				}
			}
		}
	}
	else
	{
		bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
	}
	if(IAP2_MEDIA_LIBRARY_RET_OK != bMediaLibraryRetStatus)
	{
		/* Message could not be form correctly and will be discarded */
		iAP2_Sessions_vfnResetMessage();
	}
	return bMediaLibraryRetStatus;
}

/**************************************************************************************************
 *
 * @brief    Function must be executed by the upper layer to retreive the data from the 
 * 			MediaLibraryUpdates message.
 *
 * @param    
 * 
 * @return  	void
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_vfnUpdateGetParameterData(uint8_t *bpParameterData, uint16_t *wpParameterID, uint16_t *wSize, 
		uint32_t *dwpUpdatedProperties)
{
	uint16_t wParamID;
	uint16_t gwDataSize;
	uint16_t wSubParamId;
	uint8_t bSubparameterLevel = _FALSE_;
	uint8_t bParameterHeaderReturn = MEDIA_LIBRARY_UPDATE_GET_PARAM_OK;
	uint8_t bSubParameterHeaderReturn = MEDIA_LIBRARY_UPDATE_GET_PARAM_OK;
	uint8_t bGetParameterDataReturn = MEDIA_LIBRARY_UPDATE_GET_PARAM_OK;


	/* Read parameter header */
	bParameterHeaderReturn = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID, &gwDataSize);

	/* If no errors while reading */
	if (IAP2_SESS_CTRL_OK == bParameterHeaderReturn)
	{
		/* If parameter is a group parameter */
		if((MEDIA_LIBRARY_UPDATES_ITEM  == wParamID)||
				(MEDIA_LIBRARY_UPDATES_PLAYLIST  == wParamID))
		{

#if iAP2_MEDIA_LIBRARY_ITEM_MEDIA_TYPE == _TRUE_
			/* Clear Type counter from item sub-parameter update */
            if(!(iAP2_MediaLibrary_dwUpdatedProperties & 
               iAP2_MediaLibrary_vapUpdateItemSubParamUpdateFlag[iAP2_MEDIA_ITEM_PROPERTY_MEDIA_TYPE_BIT]))
            {
			    iAP2_MediaLibraryItemUpdate.bTypeCnt = 0x00;
            }
#endif
			do
			{
				/* if is the first group parameter */
				if(_FALSE_ == bSubparameterLevel)
				{
					/* Set sub-parameter level */
					bSubparameterLevel = _TRUE_;
					/* Read Sub-parameter */
					bSubParameterHeaderReturn = iAP2_Sessions_bfnCtrlMsgGroupStart();	
				}

				/* Read Sub-parameter */
				bSubParameterHeaderReturn = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wSubParamId,
						&gwDataSize);

				/* If no errors while reading */
				if (IAP2_SESS_CTRL_OK == bSubParameterHeaderReturn)
				{
					/* If is a item subparameter */
					if(MEDIA_LIBRARY_UPDATES_ITEM  == wParamID)
					{
						if(iAP2_MEDIA_LIBRARY_ITEM_MAX_ID > wSubParamId)
						{
							/* Read media item Sub-parameter data */
							iAP_MediaLibrary_vfnapUpdateItem[wSubParamId](iAP2_MediaLibrary_vapUpdateItemAddress[wSubParamId], 
									iAP2_MediaLibrary_vapUpdateItemSubParamSize[wSubParamId],
									iAP2_MediaLibrary_vapUpdateItemSubParamUpdateFlag[wSubParamId], 
									&iAP2_MediaLibrary_dwUpdatedProperties, gwDataSize, &iAP2_MediaLibrary_gbFrameStatus,
									&bGetParameterDataReturn);	
#if (_TRUE_ == iAP2_MEDIA_LIBRARY_ITEM_MEDIA_TYPE)
							/* if is a item type */
							if((iAP2_MEDIA_LIBRARY_UPDATES_ITEM_ID == wSubParamId) &&
                               (bGetParameterDataReturn != IAP2_SESS_CTRL_DATA_PENDING))
							{
								/* Increase item type counter  */
								iAP2_MediaLibraryItemUpdate.bTypeCnt++;
							}
#endif	
						}
						else
						{
							bGetParameterDataReturn = IAP2_MEDIA_LIBRARY_INVALID_PARAMETER;
						}
					}
					else
					{
						if(iAP2_MEDIA_LIBRARY_PLAYLIST_MAX_ID > wSubParamId)
						{
							/* Read media item Sub-parameter data */
							iAP_MediaLibrary_vfnapUpdatePlaylist[wSubParamId](iAP2_MediaLibrary_vapUpdatePlaylistAddress[wSubParamId], 
									iAP2_MediaLibrary_vapUpdatePlaylistSubParamSize[wSubParamId],
									iAP2_MediaLibrary_vapUpdatePlaylistSubParamUpdateFlag[wSubParamId], 
									&iAP2_MediaLibrary_dwUpdatedProperties, gwDataSize, &iAP2_MediaLibrary_gbFrameStatus,
									&bGetParameterDataReturn);	
						}
						else
						{
							bGetParameterDataReturn = IAP2_MEDIA_LIBRARY_INVALID_PARAMETER;
						}
					}
				}
				/* If there was an error */
				else
				{
					bGetParameterDataReturn = IAP2_MEDIA_LIBRARY_RET_ERR;
				}
			}while(IAP2_SESS_CTRL_PARAM_COMPLETE == bGetParameterDataReturn);
			/* if  last group sub-parameter */
			if((IAP2_SESS_CTRL_GROUP_COMPLETE == bGetParameterDataReturn)||
					(IAP2_SESS_CTRL_MESSAGE_COMPLETE == bGetParameterDataReturn))
			{
				if (IAP2_SESS_CTRL_GROUP_COMPLETE == bGetParameterDataReturn)
				{
					bGetParameterDataReturn = IAP2_MEDIA_LIBRARY_RET_OK;
				}
				/* Return read values */
				*wpParameterID = wParamID; 
				*wSize = 0x00;
                *dwpUpdatedProperties = iAP2_MediaLibrary_dwUpdatedProperties;
                iAP2_MediaLibrary_dwUpdatedProperties = 0; /* Clear for next message */
			}	
		}
		/* If a no group parameter */
		else if (MEDIA_LIBRARY_IS_HIDING_REMOTE_ITEMS >= wParamID)
		{
			
			/* Read Parameter Data */
			iAP_MediaLibrary_vfnapUpdate[wParamID](bpParameterData, 
					iAP2_MediaLibrary_vapUpdateParamSize[wParamID],
					0, 0, gwDataSize, &iAP2_MediaLibrary_gbFrameStatus,
					&bGetParameterDataReturn);	
			/* if no errors while reading parameters */
			if(IAP2_SESS_CTRL_DATA_PENDING != bGetParameterDataReturn)
			{
				/* Return read values */
				*wpParameterID = wParamID; 
				if(UFT8_MAX_SIZE < gwDataSize )
				{
					gwDataSize = UFT8_MAX_SIZE;
				}
				*wSize = gwDataSize;
			}
		}
		else
		{
			bGetParameterDataReturn = IAP2_MEDIA_LIBRARY_INVALID_PARAMETER;
		}
	}
	else
	{
		bGetParameterDataReturn = IAP2_MEDIA_LIBRARY_RET_ERR;
	}

	return bGetParameterDataReturn;
}


/** ***********************************************************************************************
 *
 * @brief    Function called to stop receiving media library updates from device. This message 
 * 			must not be send more than once per iAP2 connection
 * 
 * @param   void
 * 
 * @return  	If the message could be sent
 *			SESSIONS_OK 		0
 *			SESSIONS_BUSY 		1
 *			SESSIONS_BUSY 		2
 *			SESSIONS_MEM_FULL	3
 *			SESSIONS_WRONG_INIT 4
 *			INVALID_PARAMETER   5 
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_bfnStopUpdatesAddUniqueID(uint8_t *bpMediaLibraryIniqueID)
{
	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = IAP2_MEDIA_LIBRARY_RET_OK;
	uint16_t wMediaLibraryUniqueIDSize;
	/* If a new message */
	if((MEDIA_LIBRARY_STOP_UPDATE_NEW_MSG == iAP2_MediaLibrary_gbStopUpdateSequence)&&
			(NULL != bpMediaLibraryIniqueID))
	{
		/* Call sessions layer to start forming message */
		bMediaLibraryRetStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_STOP_MEDIA_LIBRARY_UPDATES,
				NULL, NO_DATA, MEDIA_LIBRARY_STOP_UPDATES_MAX_SIZE);

		/* If no error while starting the message */
		if (IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus)
		{
			/* set new sequence status */
			iAP2_MediaLibrary_gbStopUpdateSequence = MEDIA_LIBRARY_STOP_UPDATE_ADDED_PARAM;
		}
	}
	/* Retrieve string size */
	wMediaLibraryUniqueIDSize = bfnStringLength(bpMediaLibraryIniqueID);

	/* If parameter pointer was null */
	if((0 != wMediaLibraryUniqueIDSize)&&(IAP2_MEDIA_LIBRARY_RET_OK == bMediaLibraryRetStatus))
	{
		/* Call lower layer to add parameter to the message */
		bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(STOP_UPDATE_MEDIA_LIBRRY_UNIQUE_ID,
				IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, bpMediaLibraryIniqueID, 
				wMediaLibraryUniqueIDSize);
	}
	else
	{
		/* Set wrong parameter error */
		bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
	}
	if(IAP2_MEDIA_LIBRARY_RET_OK != bMediaLibraryRetStatus)
	{
		/* Message could not be form correctly and will be discarded */
		iAP2_Sessions_vfnResetMessage();
	}
	return bMediaLibraryRetStatus;
}

/** ***********************************************************************************************
 *
 * @brief    Function called to stop receiving media library updates from device. This message 
 * 			must not be send more than once per iAP2 connection
 * 
 * @param   void
 * 
 * @return  	If the message could be sent
 *			SESSIONS_OK 		0
 *			SESSIONS_BUSY 		1
 *			SESSIONS_BUSY 		2
 *			SESSIONS_MEM_FULL	3
 *			SESSIONS_WRONG_INIT 4
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_bfnStopUpdatesSend(void)
{
	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = MEDIA_LIBRARY_OK;
	/* If add media library unique ID has been previously called */
	if (MEDIA_LIBRARY_STOP_UPDATE_ADDED_PARAM == iAP2_MediaLibrary_gbStopUpdateSequence)
	{
		/* Call lower layer to send message */
		bMediaLibraryRetStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
		/* Reset sequence variable */
		iAP2_MediaLibrary_gbStopUpdateSequence = MEDIA_LIBRARY_STOP_UPDATE_NEW_MSG;
	}
	else /* if add media library update unique ID has not been sent */
	{
		/* set wrong parameter error */
		bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
	}
	if(IAP2_MEDIA_LIBRARY_RET_OK != bMediaLibraryRetStatus)
	{
		/* Message could not be form correctly and will be discarded */
		iAP2_Sessions_vfnResetMessage();
	}
	return bMediaLibraryRetStatus;
}

/** ***********************************************************************************************
 *
 * @brief    Function called to start receiving media library updates from device.
 * 
 * @param   void
 * 
 * @return  	If the message could be sent
 *			SESSIONS_OK 		0
 *			SESSIONS_BUSY 		1
 *			SESSIONS_BUSY 		2
 *			SESSIONS_MEM_FULL	3
 *			SESSIONS_WRONG_INIT 4
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_bfnPlayCurrentSelection(uint8_t* bpMediaLibraryUniqueID)
{
	uint16_t wMediaLibraryUniqueIDSize;

	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = MEDIA_LIBRARY_OK;

	/* Retrieve string size */
	wMediaLibraryUniqueIDSize = bfnStringLength(bpMediaLibraryUniqueID);

	/* if pointer receive for media library unique identifier is NOT NULL */
	if(0 != wMediaLibraryUniqueIDSize)
	{
		/* Call sessions layer to send a one parameter message */
		bMediaLibraryRetStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_PLAY_MEDIA_LIBRARY_CURRENT_SEL, 
				bpMediaLibraryUniqueID, wMediaLibraryUniqueIDSize, 
				wMediaLibraryUniqueIDSize + CTRL_MSG_PARAMHEADER_SIZE + CTRL_MSG_HEADER_SIZE);
	}
	else /* If pointer is NULL */
	{
		/* Set the error return value */
		bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
	}
	if(IAP2_MEDIA_LIBRARY_RET_OK != bMediaLibraryRetStatus)
	{
		/* Message could not be form correctly and will be discarded */
		iAP2_Sessions_vfnResetMessage();
	}
	return bMediaLibraryRetStatus;
}

/** ***********************************************************************************************
 *
 * @brief    Function called to start receiving media library updates from device.
 * 
 * @param   void
 * 
 * @return  	If the message could be sent
 *			SESSIONS_OK 		0
 *			SESSIONS_BUSY 		1
 *			SESSIONS_BUSY 		2
 *			SESSIONS_MEM_FULL	3
 *			SESSIONS_WRONG_INIT 4
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_bfnPlayItems(uint8_t *bpItemPersistentIDs, uint16_t wItemPersistentIDsSize, 
		uint32_t dwItemStartingIndex, uint8_t *bpMediaLibraryUniqueID)
{

	uint16_t wMediaLibrarytUniqueIDSize;

	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = MEDIA_LIBRARY_OK;

	/* Retrieve strings size */
	wMediaLibrarytUniqueIDSize = bfnStringLength(bpMediaLibraryUniqueID);

	/* Call sessions layer to start forming message */
	bMediaLibraryRetStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_PLAY_MEDIA_LIBRARY_ITEMS,
			NULL, NO_DATA, MEDIA_LIBRARY_START_UPDATES_MAX_SIZE);

	/* If media item persistent ID is NOT NULL */
	if((NULL != bpItemPersistentIDs)&&(wItemPersistentIDsSize >= sizeof(uint64_t)))
	{
		/* Set parameter */
		bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(PLAY_ITEM_PERSISTENT_IDS,
				IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, bpItemPersistentIDs, 
				wItemPersistentIDsSize);
		/* If no error while sending media library unique id */
		if(MEDIA_LIBRARY_OK == bMediaLibraryRetStatus)
		{
			/* If starting index is not zero */
			if (0 != dwItemStartingIndex)
			{
				/* Change endianess */
				/* @Todo: AC - Add support for big endian core */
#if BIG_ENDIAN_CORE == _FALSE_
				BYTESWAP32(dwItemStartingIndex,dwItemStartingIndex);
#endif
				bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(PLAY_ITEMS_STARTING_INDEX,
						IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, (uint8_t*)&dwItemStartingIndex,
						sizeof(uint32_t));
			}
			if(MEDIA_LIBRARY_OK == bMediaLibraryRetStatus)
			{
				/* If media unique ID was NOT NULL  */
				if(0 != wMediaLibrarytUniqueIDSize)
				{

					/* Add media item properties sub-parameters */
					bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(PLAY_MEDIA_LIBRARY_UNIQUE_ID,
							IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, bpMediaLibraryUniqueID, 
							wMediaLibrarytUniqueIDSize);
				}
				else
				{
					/* Set error message */
					bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
				}
			}
			if(MEDIA_LIBRARY_OK == bMediaLibraryRetStatus)
			{
				bMediaLibraryRetStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
			}
		}
	}
	else
	{
		/* Set error message */
		bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
	}
	if(IAP2_MEDIA_LIBRARY_RET_OK != bMediaLibraryRetStatus)
	{
		/* Message could not be form correctly and will be discarded */
		iAP2_Sessions_vfnResetMessage();
	}
	return bMediaLibraryRetStatus;
}

/** ***********************************************************************************************
 *
 * @brief    Function called to start receiving media library updates from device.
 * 
 * @param   void
 * 
 * @return  	If the message could be sent
 *			SESSIONS_OK 		0
 *			SESSIONS_BUSY 		1
 *			SESSIONS_BUSY 		2
 *			SESSIONS_MEM_FULL	3
 *			SESSIONS_WRONG_INIT 4
 *          
 **************************************************************************************************/
uint8_t iAP2_MediaLibrary_bfnPlayCollection(uint8_t *bpCollectionPersistantID, uint8_t bCollectionType, 
		uint32_t dwCollectionStartIndex, uint8_t *bpMediaLibraryUniqueID)
{
	uint16_t wMediaLibrarytUniqueIDSize;

	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = MEDIA_LIBRARY_OK;

	/* Retrieve strings size */
	wMediaLibrarytUniqueIDSize = bfnStringLength(bpMediaLibraryUniqueID);

	/* Call sessions layer to start forming message */
	bMediaLibraryRetStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_PLAY_MEDIA_LIBRARY_COLLECTION,
			NULL, NO_DATA, PLAY_COLLECTION_MAX_SIZE);

	/* If collection persistent ID is NOT NULL */
	if(NULL != bpCollectionPersistantID)
	{
		/* Set parameter */
		bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(COLLECTION_PERSISTENT_ID,
				IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, bpCollectionPersistantID, 
				COLLECTION_PERSISTENT_ID_SIZE);
		/* If no error while sending media library unique id */
		if(MEDIA_LIBRARY_OK == bMediaLibraryRetStatus)
		{
			/* If collection type is within the expected valaues */
			if (bCollectionType < MAX_COLLECTION_TYPE_VALUE)
			{
				/* Add collection type parameter to the message */
				bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(COLLECTION_TYPE,
						IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, &bCollectionType,
						sizeof(uint8_t));
				/* If no errors while adding collection type */
				if(MEDIA_LIBRARY_OK == bMediaLibraryRetStatus)
				{
					/* If there is a collection start index parameter present */
					if(0 != dwCollectionStartIndex)
					{
#if BIG_ENDIAN_CORE == _FALSE_	
						/* Change the 32bit value to big endian */
						BYTESWAP32(dwCollectionStartIndex,dwCollectionStartIndex);
#endif
						/* Add collection start index parameter to the message */
						bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(COLLECTION_STARTING_INDEX,
								IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, (uint8_t*)&dwCollectionStartIndex, 
								sizeof(uint32_t));
					}
					/* If no errors while adding collection type */
					if(MEDIA_LIBRARY_OK == bMediaLibraryRetStatus)
					{
						/* If media library unique ID does NOT contain a null pointer */
						if(0 != wMediaLibrarytUniqueIDSize)
						{
							/* Add media library unique ID parameter to the message */
							bMediaLibraryRetStatus = iAP2_Sessions_bfnAddCtrlMsgParam(COLLECTION_MEDIA_LIBRARY_UNIQUE_ID,
									IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, bpMediaLibraryUniqueID, 
									wMediaLibrarytUniqueIDSize);
						}
						else
						{
							/* Set error message */
							bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
						}
					}
					/* If no errors while adding collection type */
					if(MEDIA_LIBRARY_OK == bMediaLibraryRetStatus)
					{
						bMediaLibraryRetStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
					}
				}
			}
			else
			{
				/* Set error message */
				bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
			}
		}

	}
	else
	{
		bMediaLibraryRetStatus = MEDIA_LIBRARY_ERR_WRONG_PARAM;
	}
	if(IAP2_MEDIA_LIBRARY_RET_OK != bMediaLibraryRetStatus)
	{
		/* Message could not be form correctly and will be discarded */
		iAP2_Sessions_vfnResetMessage();
	}
	return bMediaLibraryRetStatus;
}

#if defined(__IAR_SYSTEMS_ICC__)
__weak
#else
__attribute__ ((weak))
#endif
void iAP2_MediaLibrary_vfnInfoRecv(iAP2_MediaLibraryInfoData *spMediaLibrayInfo,
		iAP2_MediaLibraryInfoSize *spMediaLibrayInfoSize)
{
	(void)spMediaLibrayInfo;
	(void)spMediaLibrayInfoSize;
}

#if defined(__IAR_SYSTEMS_ICC__)
__weak
#else
__attribute__ ((weak))
#endif
void iAP2_MediaLibrary_vfnUpdateRecv(void)
{

}
/*************************************************************************************************/
/*                                    Static Functions Section                                   */
/*************************************************************************************************/

/**************************************************************************************************
 *
 * @brief    Function executed when a Media library Information message is received.
 *
 * @param    fnPtr Pointer to the function to be called once received message was validated
 *			wMessageID	Message Identifier 
 * 
 * @return  	void
 *          
 **************************************************************************************************/
static void iAP2_MediaLibrary_vfnInformation(void(*fnPtr)(iAP2_MediaLibraryInfoData *,
		iAP2_MediaLibraryInfoSize *), uint16_t wMessageID)
{
	uint16_t gwDataSize;
	uint16_t wParamID;
	uint16_t wSubParamId;
	uint8_t bSubParameterDataReturn;
	uint8_t bSubParamLevel = _FALSE_;
	/* Local variable to store return value*/
	uint8_t bMediaLibraryRetStatus = IAP2_MEDIA_LIBRARY_RET_OK;
    uint8_t count = 0;

	(void)wMessageID;

	do 
	{
		/* Read Parameter Header */
		bMediaLibraryRetStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID,&gwDataSize);

        if(IAP2_SESS_CTRL_OK == bMediaLibraryRetStatus)
        {
            /* If correct expected values for parameter Id and parameter size */
            if((MEDIA_LIBRARY_INFO_ID == wParamID) && (FALSE != gwDataSize))
            {
                /* while readings to header return OK and readings to data return ask for next 
                 * parameter */
                do
                {
                    /* If first sub-parameter reading */
                    if(FALSE == bSubParamLevel)
                    {
                        bSubParamLevel = TRUE;
                        bMediaLibraryRetStatus = iAP2_Sessions_bfnCtrlMsgGroupStart();
                    }									  
                    
                    bMediaLibraryRetStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wSubParamId, &gwDataSize);	
    
                    if( IAP2_SESS_CTRL_OK == bMediaLibraryRetStatus)
                    {
                        /* If sub-parameter is within the expected limits */
                        if(MEDIA_LIBRARY_INFO_MAX_SUBPARAM_VALUE >= wSubParamId)
                        {
                            iAP_MediaLibrary_vfnapInfo[wSubParamId] \
                            (iAP2_MediaLibrary_vapInfoAddress[count][wSubParamId], (uint8_t)iAP2_MediaLibrary_vapInfoSubParamSize[wSubParamId], \
                                    NULL, NULL, gwDataSize, &iAP2_MediaLibrary_gbFrameStatus, \
                                    &bSubParameterDataReturn);	
                            if (0x00 == wSubParamId)
                            {
                                *iAP2_MediaLibrary_gsInfoSize.wpNameSize = bfnStringLength(iAP2_MediaLibrary_vapInfoAddress[count][wSubParamId]);
                            }
                            else if(0x01 == wSubParamId)
                            {
                                *iAP2_MediaLibrary_gsInfoSize.wpUniqueIDSize = bfnStringLength(iAP2_MediaLibrary_vapInfoAddress[count][wSubParamId]);
                            }
                            else
                            {
                                *iAP2_MediaLibrary_gsInfoSize.wpTypeSize = iAP2_MediaLibrary_vapInfoSubParamSize[wSubParamId];
                            }
                        }
                        else
                        {
                            /* Set the corrupted data flag */
                            iAP2_MediaLibrary_gbStatusFlags |= MEDIA_LIBRARY_CORRUPTED_DATA_MASK;
                            bSubParameterDataReturn = MEDIA_LIBRARY_INFO_DATA_CORRUPTED;
                        }
                    }
                }while((IAP2_SESS_CTRL_OK == bMediaLibraryRetStatus) && 
                        (IAP2_SESS_CTRL_PARAM_COMPLETE == bSubParameterDataReturn));
    
                if ((IAP2_SESS_CTRL_GROUP_COMPLETE == bSubParameterDataReturn)||
                        (IAP2_SESS_CTRL_MESSAGE_COMPLETE == bSubParameterDataReturn))
                {
                    /* Reset subparameter level */
                    bSubParamLevel = FALSE;
                    /* Execute upper layer callback function */
                    fnPtr(&iAP2_MediaLibrary_gsInfo, &iAP2_MediaLibrary_gsInfoSize);
                    count++;
                }
            }
            else
            {
                /* Set the corrupted data flag */
                iAP2_MediaLibrary_gbStatusFlags |= MEDIA_LIBRARY_CORRUPTED_DATA_MASK;
                bSubParameterDataReturn = MEDIA_LIBRARY_INFO_DATA_CORRUPTED;
            }
		}
    }while((IAP2_SESS_CTRL_OK == bMediaLibraryRetStatus) && 
            (IAP2_SESS_CTRL_MESSAGE_COMPLETE != bSubParameterDataReturn ) &&
            (IAP2_SESS_CTRL_DATA_PENDING != bSubParameterDataReturn ));	

}

static uint8_t bfnStringLength(uint8_t *bpString)
{
	uint8_t bIndex = 0x00;
	if (bpString != NULL)
	{
		do
		{
			bIndex ++;
		}while((bpString[bIndex]!= 0)&&(bIndex < UFT8_MAX_SIZE - 1));	
		bpString[bIndex]= 0;
		bIndex = bIndex + 1;
	}
	return (bIndex);
}

/** ***********************************************************************************************
 *
 * @brief    Function used to save utf8 data from a message, the validation of the correct data size according to
 *  			information from original structure is done too.
 * 
 * @param   	wpData pointer where utf8 will be saved
 * 			bmaxValue it is used to know how long the string could be
 * 			bpMessageCurrentStatus pointer used to know the current message status
 * 			bpCurrentFrame pointer used to know current frame status
 * 
 * @return  	void
 *          
 **************************************************************************************************/
static void iAP2_FEATURES_UPDATES_DataUtf8_ML(uint8_t * bpData, uint8_t bmaxSize,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	(void)bpMessageCurrentStatus;

	if(wDataSize > bmaxSize)
	{
		wDataSize = bmaxSize;
	}
	wDataSize--; /*Only read data size - 1 to add null character at the end of the utf8 */
	*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(bpData,wDataSize);
	bpData[wDataSize]=0;
}


/** ***********************************************************************************************
 *
 * @brief    Function used to save utf8 data from a message.
 * 
 * @param   	vpsubParameter pointer where utf8 will be saved
 * 			bmaxValue it is used to know utf8 data length.
 * 			bsubParamFlag Indicates the flag to be set once the value was validated and saved.
 * 			wpFlagToSet Flag to be set after copy data operation
 * 			bpMessageCurrentStatus pointer used to know the current message status
 * 			bpCurrentFrame pointer used to know current frame status
 * 
 * @return  	void
 *          
 **************************************************************************************************/
static void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8_ML(void * vpsubparamVal,uint8_t bsubparamMaxSize ,uint32_t wsubParamFlag, uint32_t *wpFlagToSet,uint16_t wDataSize,
		uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	iAP2_FEATURES_UPDATES_DataUtf8_ML((uint8_t *)vpsubparamVal,bsubparamMaxSize,wDataSize,bpMessageCurrentStatus,bpCurrentFrame);
	if(!(*bpMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED ))
	{
		if(NULL != wpFlagToSet)
		{
			(*wpFlagToSet) |= wsubParamFlag;
		}
	}
}


/** ***********************************************************************************************
 *
 * @brief    Function used to save enum (uint8_t) data from a message.
 * 
 * @param   	*vpsubParameter pointer where enum will be saved
 * 			bmaxValue the possible maximum value for this enum
 * 			bsubParamFlag Indicates the flag to be set once the value was validated and saved.
 * 			wpFlagToSet Flag to be set after copy data operation
 * 			bpMessageCurrentStatus pointer used to know the current message status
 * 			bpCurrentFrame pointer used to know current frame status

 * 
 * @return  	void
 *          
 **************************************************************************************************/
static void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum_ML(void* vpsubparamvalue, uint8_t bmaxValue,
		uint32_t wsubParamFlag,uint32_t *wpFlagToSet,uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, 
		uint8_t * bpCurrentFrame)
{
#if iAP2_MEDIA_LIBRARY_ITEM_MEDIA_TYPE == _TRUE_
	iAP2_FEATURES_UPDATES_Data8_Enum_ML(((uint8_t *)vpsubparamvalue) + iAP2_MediaLibraryItemUpdate.bTypeCnt,
			bmaxValue,wDataSize, bpMessageCurrentStatus,bpCurrentFrame);
#else
	iAP2_FEATURES_UPDATES_Data8_Enum_ML((uint8_t *)vpsubparamvalue,
			bmaxValue,wDataSize, bpMessageCurrentStatus,bpCurrentFrame);
#endif
	if(!(*bpMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED ))
	{
		if(NULL != wpFlagToSet)
		{
			(*wpFlagToSet) |= wsubParamFlag;
		}
	}
}

/** ***********************************************************************************************
 *
 * @brief    Function used to save enum data from a message, the validation of the correct data size is done too.
 * 
 * @param    wpData pointer where enum will be saved
 * 			wbMaxValue indicates bigger enum value
 * 			bpMessageCurrentStatus pointer used to know the current message status
 * 			bpCurrentFrame pointer used to know current frame status
 * 
 * @return  	void
 *          
 **************************************************************************************************/
static void iAP2_FEATURES_UPDATES_Data8_Enum_ML(uint8_t * bpData,uint8_t bmaxValue,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	uint8_t tmpValueData;

	(void)bmaxValue;
	
	if((wDataSize != iAP2_FEATURES_UPDATES_BYTE))
	{
		*bpMessageCurrentStatus |=  iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED;
		*bpCurrentFrame = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
	}
	else
	{
		*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(&tmpValueData,iAP2_FEATURES_UPDATES_BYTE);
		*bpData=tmpValueData;
	}
}

