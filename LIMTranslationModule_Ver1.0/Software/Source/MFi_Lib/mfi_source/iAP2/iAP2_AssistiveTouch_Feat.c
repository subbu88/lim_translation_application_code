/*HEADER******************************************************************************************
*
* Copyright 2016 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
* FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
* file except in compliance with the License.
*
*************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
*END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "stdio.h"
#include "iAP2_AssistiveTouch_Feat.h"
#include "iAP2_Features.h"
#include "iAP2_Sessions_Control.h"
/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/



/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/

void iAP2_Feat_AssistTouch_bfnDummy(uint8_t bAssistiveTouchStatus);

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/



/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
/**************************************************************************************************
* @name     iAP2_AssistiveTouch_Feat_bfnStartAssistiveTouch
*
* @brief    Function called to start Assistive touch from accessory.
* 
* @param   	void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_AssistiveTouch_Feat_bfnStartAssistiveTouch(void)
{	
    uint8_t bSessionStatus;
    bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_ASSISTIVE_TOUCH_FEAT_START, NULL,0,0);
    
    if(iAP2_ASSISTIVE_TOUCH_FEATURE_OK != bSessionStatus)
    {
        iAP2_Sessions_vfnResetMessage();
    }	

    return(bSessionStatus);
}
/**************************************************************************************************
* @name     iAP2_AssistiveTouch_Feat_bfnStartAssistiveTouchInformation
*
* @brief    Function called to start Assistive touch Information from accessory.
* 
* @param   	void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_AssistiveTouch_Feat_bfnStartAssistiveTouchInformation(void)
{
    uint8_t bSessionStatus;
        
    bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_ASSISTIVE_TOUCH_FEAT_START_INFO, NULL,0,0);
    
    if(iAP2_ASSISTIVE_TOUCH_FEATURE_OK != bSessionStatus)
    {
        iAP2_Sessions_vfnResetMessage();
    }
    
    return(bSessionStatus);
}
/**************************************************************************************************
* @name     iAP2_AssistiveTouch_Feat_bfnStopAssistiveTouch
*
* @brief    Function called to stop Assistive touch from accessory.
* 
* @param   	void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_AssistiveTouch_Feat_bfnStopAssistiveTouch(void)
{
    uint8_t bSessionStatus;
        
    bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_ASSISTIVE_TOUCH_FEAT_STOP, NULL,0,0);
    
    if(iAP2_ASSISTIVE_TOUCH_FEATURE_OK != bSessionStatus)
    {
        iAP2_Sessions_vfnResetMessage();
    }	
    
    return(bSessionStatus);
}
/**************************************************************************************************
* @name     iAP2_AssistiveTouch_Feat_bfnStopAssistiveTouchInformation
*
* @brief    Function called to stop Assistive touch information from accessory.
* 
* @param   	void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_AssistiveTouch_Feat_bfnStopAssistiveTouchInformation(void)
{
    uint8_t bSessionStatus;
        
    bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_ASSISTIVE_TOUCH_FEAT_STOP_INFO, NULL,0,0);
    
    if(iAP2_ASSISTIVE_TOUCH_FEATURE_OK != bSessionStatus)
    {
        iAP2_Sessions_vfnResetMessage();
    }
    
    return(bSessionStatus);
}
/**************************************************************************************************
* @name     iAP2_Feat_AssistTouch_vfnCallBack
*
* @brief    Assistive touch callback
* 
* @param   	wMessageID : Message ID
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_Feat_AssistTouch_vfnCallBack(uint16_t wMessageID)
{
    uint8_t bSessionStatus;
    uint16_t wParamID;
    uint16_t wDataSize = 0;
    uint8_t bCurrentDataStatus = 0;
    uint8_t bAssistiveTouchStatus = 0;

    if(IAP2_ASSISTIVE_TOUCH_FEAT_INFORMATION == wMessageID)
    {
        bSessionStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID,&wDataSize);
        
        if(IAP2_SESS_CTRL_OK == bSessionStatus)
        {
            bCurrentDataStatus = iAP2_Sessions_bfnReadCtrlMsgParamData(&bAssistiveTouchStatus,wDataSize);
            
            if(IAP2_SESS_CTRL_MESSAGE_COMPLETE == bCurrentDataStatus)
            {
                IAP2_ASSISTIVE_TOUCH_CALLBACK(bAssistiveTouchStatus);
            }	
        }			
    }
}
/**************************************************************************************************
* @name     iAP2_Feat_AssistTouch_bfnDummy
*
* @brief    Assistive touch dummy function
* 
* @param   	bAssistiveTouchStatus : Assistive Touch status
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_Feat_AssistTouch_bfnDummy(uint8_t bAssistiveTouchStatus)
{
    (void)bAssistiveTouchStatus;
}

/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
