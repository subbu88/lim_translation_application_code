/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Identification_Feat.h"
#include "iAP2_Ext_Acc_Feat.h"
#include "iAP2_Config.h"
#include "iAP2_Sessions_Control.h"
#include "iAP2_Sessions_Ext_Acc.h"
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static iAP2_EASessionType EASessionArray[IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES] = 
{
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 0)
    {
        0x0000,
        IAP2_FEAT_EA_PROT_INACTIVE,
    },
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 1)
    {
        0x0000,
        IAP2_FEAT_EA_PROT_INACTIVE,
    },
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 2)
    {
        0x0000,
        IAP2_FEAT_EA_PROT_INACTIVE,
    },
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 3)
    {
        0x0000,
        IAP2_FEAT_EA_PROT_INACTIVE,
    },
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 4)
    {
        0x0000,
        IAP2_FEAT_EA_PROT_INACTIVE,
    }
#endif  
};

static uint16_t gwEATransferID = 0;
static uint8_t  gbEAProtocolID = 0;

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/


/*************************************************************************************************
*
* @brief    This function is called every time an External Message arrives. The protocol information
*           is actualized in the EASessionArray after that an application callback will be execute
*           to inform upper layers. 
* 
* @param   	wMessageID		Message of External Accessory feature.
*           *fnPtr          Function pointer to Application callback 
*           
* @return  	void
*          
**************************************************************************************************/
void iAP2_Feat_EA_vfnNewRequestCaller(uint16_t wMessageID)
{
    uint16_t wParamID;
    uint16_t wDataSize;
    uint8_t  bSessionStatus;
    uint8_t  bEACurrentFrame;
    uint8_t  bProtCount = 0;

    if(wMessageID == IAP2_EXT_ACC_FEAT_START_PROT_SESS)
    {
        do
        {
            /* Get first Start EA parameter ExternalAccessoryProtocolIdentifier */
            bSessionStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID, &wDataSize);
            if(IAP2_SESS_CTRL_OK == bSessionStatus)
            {
                if(wParamID == IAP2_EXT_ACC_START_PROTOCOL_IDENTIFIER_ID)
                {
                    /* Read and store first Start EA parameter ExternalAccessoryProtocolIdentifier */
                    bEACurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(&gbEAProtocolID, wDataSize);   

                }
                else if(wParamID == IAP2_EXT_ACC_START_PROTOCOL_SESSION_IDENTIFIER_ID)
                {
                    /* Read and store Second Start EA parameter ExternalAccessoryProtocolSessionIdentifier */
                    bEACurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(&gwEATransferID, wDataSize);                   
                }

            }
        }while((IAP2_SESS_CTRL_OK == bSessionStatus) && (IAP2_SESS_CTRL_MESSAGE_COMPLETE != bEACurrentFrame ) && (IAP2_SESS_CTRL_DATA_PENDING != bEACurrentFrame));

        if(bEACurrentFrame == IAP2_SESS_CTRL_MESSAGE_COMPLETE)
        {
            if(gbEAProtocolID < IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES)
            {
#if(BIG_ENDIAN_CORE ==_FALSE_)
                /* Swap the information in case of Little endian core */
                BYTESWAP16(gwEATransferID,EASessionArray[gbEAProtocolID].wEATransferID);
#else
                EASessionArray[gbEAProtocolID].wEATransferID = gwEATransferID;
#endif

                /* Update status for the specific session */
                EASessionArray[gbEAProtocolID].bEASessionStatus = IAP2_FEAT_EA_PROT_ACTIVE;
                iAP2_Feat_EA_ProtCallbacks[gbEAProtocolID](NULL, gbEAProtocolID, IAP2_FEAT_EA_PROT_ACTIVE);
            }
            else
            {
                /** @todo EM - ExtAccFeat: Protocol ID not presented in identification. Report to the application? */
            }
        }

    }
    else if(wMessageID == IAP2_EXT_ACC_FEAT_STOP_PROT_SESS)
    {
        /*Get Stop EA parameter ExternalAccessoryProtocolSessionIdentifier */
        bSessionStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID,&wDataSize);
        if((IAP2_SESS_CTRL_OK == bSessionStatus) && (wParamID == IAP2_EXT_ACC_STOP_PROTOCOL_SESSION_IDENTIFIER_ID))
        {
            /*Read and store Stop EA parameter ExternalAccessoryProtocolSessionIdentifier */
            bSessionStatus = iAP2_Sessions_bfnReadCtrlMsgParamData(&gwEATransferID, wDataSize);
            /*Swap the information in case of Little endian core */
#if(BIG_ENDIAN_CORE == _FALSE_)
            BYTESWAP16(gwEATransferID,gwEATransferID);
#endif

            /* Find the structure with the correct session */
            while((gwEATransferID != EASessionArray[bProtCount].wEATransferID) && (bProtCount < IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES))
            {
                bProtCount++;
            }
            /* If Session ID was found actualize protocol parameter, if not send and error */
            if(bProtCount < IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES )
            {
                /* Clear the Transfer ID and update status for the specific session */  
                EASessionArray[bProtCount].wEATransferID = 0x0000;
                EASessionArray[bProtCount].bEASessionStatus = IAP2_FEAT_EA_PROT_INACTIVE;
                iAP2_Feat_EA_ProtCallbacks[bProtCount](NULL, bProtCount, IAP2_FEAT_EA_PROT_INACTIVE);
            }
            else
            {
                /** @todo EM - ExtAccFeat: Protocol ID not presented in identification. Report to the application? */
            }
        }
    }
    else
    {
        /* Invalid message ID, ignore. */
    }
}

uint8_t iAP2_Feat_EA_bfnSendData_temp(uint8_t *pBuff, uint16_t wSize, uint16_t wEAProtID)
{
	uint8_t  bStatus; 

	bStatus = iAP2_Session_EA_SendData(pBuff, EASessionArray[wEAProtID].wEATransferID, wSize);

	if(IAP2_SESSIONS_EA_SEND_COMPLETE == bStatus)
	{
		bStatus = IAP2_FEAT_EA_PROT_SEND_COMPLETE;
	}
	else if(IAP2_SESSIONS_EA_SEND_NO_MEM == bStatus)
	{
		bStatus = IAP2_FEAT_EA_PROT_SEND_NO_MEM;
	}
	else
	{
		bStatus = IAP2_FEAT_EA_PROT_SEND_PENDING;
	}

	return bStatus;
}


uint8_t iAP2_Feat_EA_bfnSendData(uint8_t *pBuff, uint16_t wSize, uint16_t wEAProtID)
{
	uint8_t  bStatus; 
	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_EXTERNAL_ACC_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{

		if(wEAProtID < IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES)
		{	
			if(IAP2_FEAT_EA_PROT_ACTIVE == EASessionArray[wEAProtID].bEASessionStatus)
			{
				bStatus = iAP2_Session_EA_SendData(pBuff, EASessionArray[wEAProtID].wEATransferID, wSize);

				if(IAP2_SESSIONS_EA_SEND_COMPLETE == bStatus)
				{
					bStatus = IAP2_FEAT_EA_PROT_SEND_COMPLETE;
				}
				else if(IAP2_SESSIONS_EA_SEND_NO_MEM == bStatus)
				{
					bStatus = IAP2_FEAT_EA_PROT_SEND_NO_MEM;
				}
				else
				{
					bStatus = IAP2_FEAT_EA_PROT_SEND_PENDING;
				}
			}
			else
			{
				bStatus = EASessionArray[wEAProtID].bEASessionStatus;
			}
		}
		else
		{
			bStatus = IAP2_FEAT_EA_INVALID_PROT_ID;
		}

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_EXTERNAL_ACC_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bStatus = SESSIONS_LOCK_FAIL;
	}
	return bStatus;
}

uint8_t iAP2_Feat_EA_bfnReadData(uint8_t *pbBuff, uint16_t wSize, uint16_t wReadIndex)
{
	uint8_t bStatus;
	
	bStatus = iAP2_Session_EA_ReadData(pbBuff, wSize, wReadIndex);
	
	return bStatus; 
}

void iAP2_Feat_EA_vfnDataCallback(uint16_t wEATranID, uint16_t wReadIndex, uint16_t wSize, uint8_t bStatus)
{
	IAP2_FEAT_EA_RX_DATA DataRxInfo;
	uint8_t bProtCount = 0;
	
	/* Find the structure with the correct session */
	while((wEATranID != EASessionArray[bProtCount].wEATransferID) && (bProtCount < IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES))
	{
		bProtCount++;
	}
	
	/* If Session ID was found actualize protocol parameter, if not send and error */
	if(bProtCount < IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES)
	{
		if(IAP2_SESSIONS_EA_DATA_RECV == bStatus)
		{

			DataRxInfo.wReadIndex = wReadIndex;
			DataRxInfo.wSize = wSize;
			iAP2_Feat_EA_ProtCallbacks[bProtCount](&DataRxInfo, bProtCount, IAP2_FEAT_EA_PROT_DATA_RECV);
		}
		else if(IAP2_SESSIONS_EA_SEND_COMPLETE == bStatus)
		{
			iAP2_Feat_EA_ProtCallbacks[bProtCount](NULL, bProtCount, IAP2_FEAT_EA_PROT_SEND_COMPLETE);
		}
	}
	else
	{
		/* Transfer ID not found. Ignore data */
	}
}

