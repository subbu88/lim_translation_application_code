/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_EXT_ACC_FEAT_H_
#define IAP2_EXT_ACC_FEAT_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "mfi_cfg.h"
/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/ 
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL               (_TRUE_)
#endif
#define IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES     (0x01)

#define IAP2_EXT_ACC_FEAT_START_PROT_SESS		(0xEA00)
#define IAP2_EXT_ACC_FEAT_STOP_PROT_SESS		(0xEA01)

#define IAP2_EXT_ACC_START_PROTOCOL_IDENTIFIER_ID             (0) 
#define IAP2_EXT_ACC_START_PROTOCOL_SESSION_IDENTIFIER_ID     (1) 

#define IAP2_EXT_ACC_STOP_PROTOCOL_SESSION_IDENTIFIER_ID      (0) 

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef struct
{
	uint16_t wReadIndex;
	uint16_t wSize;
}IAP2_FEAT_EA_RX_DATA;

typedef void (* const IAP2_FEAT_EA_CBK)(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus);

typedef struct{
	uint16_t wEATransferID;
	uint8_t  bEASessionStatus;
}iAP2_EASessionType;

typedef enum
{
	IAP2_FEAT_EA_PROT_ACTIVE = 0,
	IAP2_FEAT_EA_PROT_INACTIVE,
	IAP2_FEAT_EA_PROT_BUSY,
	IAP2_FEAT_EA_PROT_DATA_RECV,
	IAP2_FEAT_EA_PROT_SEND_NO_MEM,
	IAP2_FEAT_EA_PROT_SEND_COMPLETE,
	IAP2_FEAT_EA_PROT_SEND_PENDING,
	IAP2_FEAT_EA_INVALID_PROT_ID
} IAP2_FEAT_EA_PROT_STATUS;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/

uint8_t iAP2_Feat_EA_bfnSendData(uint8_t *pBuff, uint16_t wSize, uint16_t wEAProtID);
uint8_t iAP2_Feat_EA_bfnSendData_temp(uint8_t *pBuff, uint16_t wSize, uint16_t wEAProtID);
uint8_t iAP2_Feat_EA_bfnReadData(uint8_t *pbBuff, uint16_t wSize, uint16_t wReadIndex);
void iAP2_Feat_EA_vfnNewRequestCaller(uint16_t wMessageID);

/*************************************************************************************************/

#endif /* IAP2_EXT_ACC_FEAT_H_ */
