/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_LINK_H_
#define IAP2_LINK_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "CircularBuffers.h"
#include "osa_common.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP2_LINK_CTRL_CALLBACK			iAP2_Host_LinkCtrlCbk
#define IAP2_LINK_SESSION_CALLBACK		iAP2_Sessions_bfnNewFrameCallback	

#define IAP2_MAX_NUM_OUTSTANDING_PACKET 8

#define IAP2_LINK_STATUS_OK				0
#define IAP2_LINK_ERROR_NO_MEM			1
#define IAP2_LINK_ERROR_MEM_BUSY		2
#define IAP2_LINK_ERROR_BAD_INIT		3

/** Macros to help API user with interpretation of return values from other layers,
 *  i.e. function iAP2_Link_bfnReadData() is based on values returns from functions at
 *  CircularBuffer.h
 *  */
#define IAP2_LINK_CBUFFER_OK						CIRCULAR_BUFFER_OK
#define IAP2_LINK_CBUFFER_EMPTY						CIRCULAR_BUFFER_EMPTY
#define IAP2_LINK_CBUFFER_FULL						CIRCULAR_BUFFER_FULL
#define IAP2_LINK_CBUFFER_OVERFLOW					CIRCULAR_BUFFER_OVERFLOW
#define IPA2_LINK_CBUFFER_DATA_READY				CIRCULAR_BUFFER_DATA_READY
#define IAP2_LINK_CBUFFER_ERROR_OUT_OF_RANGE		CIRCULAR_BUFFER_ERROR_OUT_OF_RANGE
#define IAP2_LINK_CBUFFER_NOT_RANDOM_BUFF			CIRCULAR_BUFFER_NOT_RANDOM_BUFF

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/


typedef struct
{
	uint8_t bSessionId;
	uint8_t bSessionType;
	uint8_t bSessionVer;
}IAP2_LINK_CONFIG_SESSION;

typedef struct
{
	IAP2_LINK_CONFIG_SESSION* spSessionData;
	uint8_t bLinkVersion;
	uint8_t bMaxNumOutstandingPackets;
	uint16_t wMaxPackLen;
	uint16_t wRetTimeOut;
	uint16_t wCumAckTimeOut;
	uint8_t bMaxNumRestransmission;
	uint8_t bMaxCumAck;
	uint8_t bNumSessions;
}IAP2_LINK_SYNCHRONIZATION_PAYLOAD;

typedef enum
{
	IAP2_LINK_CTRL_RST = 0,
	IAP2_LINK_CTRL_SYN,
	IAP2_LINK_CTRL_SLP
}IAP2_LINK_CTRL_PACKET_TYPES;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
uint8_t iAP2_Link_bfnInit(void);
uint8_t iAP2_Link_bfnDeinit(void);
#if (FSL_OS_NONE == FSL_OS_SELECTED)
void iAP2_Link_vfnTask(void);
#else
void iAP2_Link_vfnTask(void* param);
#endif
void iAP2_Link_vfnSendSyncPayload(IAP2_LINK_SYNCHRONIZATION_PAYLOAD *sSyncData);
uint8_t iAP2_Link_bfnSend( uint8_t bSessionId, uint8_t bMemReqId, uint16_t wReSize );
uint8_t iAP2_Link_bfnMemoryRequest( uint8_t **wppPointer, uint16_t *wpDataSize , uint8_t *wMemReqId);
void iAP2_Link_vfnMemoryFlush(uint8_t wMemReqId );
void iAP2_Link_vfnAcceptSyncPayload(void);
void iAP2_Link_vfnLinkConfig(IAP2_LINK_SYNCHRONIZATION_PAYLOAD *spSyncData);
void iAP2_Link_vfnReset(void);
uint8_t iAP2_Link_bfnReadData(void* vpReadPtr, uint32_t *wpReadIndex, uint16_t wSize );
/*************************************************************************************************/

#endif /* IAP2_LINK_H_ */
