/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_POWER_FEATURE_H_
#define IAP2_POWER_FEATURE_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "string.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/*Callback from PowerUpdate Feat to Application*/
#define iAP2_CALLBACK_FROM_POWER_UPDATE				(iAP2_Power_Features_bfnDummy)	


/*Identification MessageID */
#define iAP2_POWER_FEATURE_UPDATES_START				(0xAE00)
#define iAP2_POWER_FEATURE_UPDATES						(0xAE01)
#define iAP2_POWER_FEATURE_UPDATES_STOP					(0xAE02)
#define iAP2_POWER_FEATURE_SOURCE_UPDATE				(0xAE03)

/*Flags for each received parameter */
#define iAP2_POWER_FEATURE_MAX_CURRENT_DRAWN_FROM_ACCESORY					(1)
#define	iAP2_POWER_FEATURE_DEVICE_BATT_WILL_CHARGE_POWER_PRESENT			(1<<1)
#define iAP2_POWER_FEATURE_ACCESORY_POWER_MODE								(1<<2)

#define	iAP2_POWER_FEATURE_SOURCE_AVAILABLE_CURRENT							(1<<3)
#define iAP2_POWER_FEATURE_SOURCE_DEVICE_BATT_SHOULD_CHARGE_POWER_PRESENT	(1<<4)


#define iAP2_POWER_FEATURE_ERROR_OK											0
#define iAP2_POWER_FEATURE_INVALID_DATA_PARAMETER							0xFF

/*Valid values for current parameters*/
#define MAX_CURRENT_DRAWM_FROM_ACCESORY_0				0
#define MAX_CURRENT_DRAWM_FROM_ACCESORY_1000		 	1000
#define MAX_CURRENT_DRAWM_FROM_ACCESORY_2100			2100
#define MAX_CURRENT_DRAWM_FROM_ACCESORY_2400			2400




/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
/*Possible values for AccesoryPoweMode parameter*/
typedef enum
{
	iAP2_POWER_FEATURE_RESERVED = 0,
	iAP2_POWER_FEATURE_LOW_POWER,
	iAP2_POWER_FEATURE_INTERMITTENT_HIGH_POWER 
} iAP2_PowerUpdates_AccesoryPoweMode;

/*Structure where PowerUpdate information would be stored*/
typedef struct 
{
	uint32_t dwPowerUpdatesFlag;  			 /**< Flag to indicate which Parameter was updated*/
	uint16_t wMaxCurrentDrawnFromAcc;    
	uint8_t  bDeviceBattWillChargeIfPowerPresent;
	uint8_t  bAccessoryPowerMode;
} iAP2_PowerUpdateFeature;                /* Device data structure type */

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void iAP2_Feat_Power_vfnCallBack(uint16_t MessageID);
uint8_t iAP2_Power_Features_bfnStartPowerUpdates(uint8_t bPowerUpdatesAttributes,iAP2_PowerUpdateFeature * PowerUpdatesAppData);
uint8_t iAP2_Power_Features_bfnStopPowerUpdates(void);
#if IAP2_IDENT_PWR_SOURCE_TYPE_DATA == IAP2_PWR_TYPE_SELF
uint8_t iAP2_Power_Features_bfnPowerSourceUpdate(uint16_t wAvailableCurrentForDevice, uint8_t bDeviceBattShouldChargeIfPowerPresent);
#endif

/*Call back for Power Update message*/
extern void iAP2_CALLBACK_FROM_POWER_UPDATE(uint16_t MessageID); 


/*************************************************************************************************/

#endif /* IAP2_POWER_FEATURE_H_ */
