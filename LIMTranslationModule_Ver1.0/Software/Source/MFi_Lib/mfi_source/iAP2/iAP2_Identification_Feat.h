/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_IDENTIFICATION_H_
#define IAP2_IDENTIFICATION_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "mfi_cfg.h"
/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/*Identification MessageID */
#define IAP2_IDENTIFICATION_START_IDENTIFICATION				(0x1D00)
#define IAP2_IDENTIFICATION_IDENTIFICATION_INFO					(0x1D01)
#define IAP2_IDENTIFICATION_ACCEPTED							(0x1D02)
#define IAP2_IDENTIFICATION_REJECTED							(0x1D03)
#define IAP2_IDENTIFICATION_CANCELL								(0x1D05)
#define IAP2_IDENTIFICATION_INFO_UPDATE							(0x1D06)

/*Flag for each rejected parameter from Information Identification */
#define IAP2_IDENTIFICATION_NAME_REJECTED						(1)
#define IAP2_IDENTIFICATION_MODELL_REJECTED						(1<<1)
#define IAP2_IDENTIFICATION_MANUFACTURER_REJECTED				(1<<2)
#define IAP2_IDENTIFICATION_SERIAL_NUMBER_REJECTED				(1<<3)
#define IAP2_IDENTIFICATION_FIRMWARE_VER_REJECTED				(1<<4)
#define IAP2_IDENTIFICATION_HARDWARE_VER_REJECTED				(1<<5)
#define IAP2_IDENTIFICATION_MESSAGE_SENT_REJECTED				(1<<6)
#define IAP2_IDENTIFICATION_MESSAGE_RECEIVED_REJECTED			(1<<7)
#define IAP2_IDENTIFICATION_POWER_SOURCE_REJECTED				(1<<8)
#define IAP2_IDENTIFICATION_MAX_CURRENT_FROM_DEVICE_REJECTED	(1<<9)
#define IAP2_IDENTIFICATION_EXTERNAL_ACCESORY_REJECTED			(1<<10)
#define IAP2_IDENTIFICATION_APP_BUNDLE_SEED_REJECTED			(1<<11)
#define IAP2_IDENTIFICATION_CURRENT_LANGUAGE_REJECTED			(1<<12)
#define IAP2_IDENTIFICATION_SUPPORTED_LANGUAGE_REJECTED			(1<<13)
#define IAP2_IDENTIFICATION_SERIAL_TRANSPORT_COMPONENT_REJECTED	(1<<14)
#define IAP2_IDENTIFICATION_USB_DEVICE_TRANSPORT_REJECTED		(1<<15)
#define IAP2_IDENTIFICATION_USB_HOST_TRANSPORT_REJECTED			(1<<16)
#define IAP2_IDENTIFICATION_BLUETOOTH_TRANSPORT_REJECTED		(1<<17)
#define IAP2_IDENTIFICATION_IAP2_HID_COMPONENT_REJECTED			(1<<18)
#define IAP2_IDENTIFICATION_USB_HOST_HID_COMPONENT_REJECTED		(1<<19)


#define IAP2_MAX_POSSIBLE_MESSAGES_ID							(74)

#define IAP2_IDENTIFICATION_FEATURE_ERROR_OK					(0x00)
#define IAP2_IDENTIFICATION_INVALID_DATA_PARAMETER				(0xFF)

/* Power Source Type */
/*-----------------------------------------------------------------------------------------------*/
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_PWR_TYPE_SELF				0x02
#define IAP2_PWR_TYPE_NONE				0x00

#define IAP2_IDENT_PWR_SOURCE_TYPE_DATA		 				(IAP2_PWR_TYPE_NONE)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION 
/*-----------------------------------------------------------------------------------------------*/
/* Maximum Current Drawn From Device */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_MAX_CURRENT_DRAWN_DATA		 			(0x0000)
/*-----------------------------------------------------------------------------------------------*/
/* Messages sent by accessory and received from device */
/*-----------------------------------------------------------------------------------------------*/

/* Features Supported */
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_CTRL_MSG_ACCESSORY_AUTHENTICATION				(_FALSE_)
#define IAP2_CTRL_MSG_ACCESSORY_IDENTIFICATION				(_FALSE_)
#define IAP2_CTRL_MSG_APP_LAUNCH							(_FALSE_)
#define IAP2_CTRL_MSG_ASSISTIVE_TOUCH						(_TRUE_)
#define IAP2_CTRL_MSG_BLUETOOTH_STATUS						(_FALSE_)
#define IAP2_CTRL_MSG_DEVICE_AUTHENTICATION					(_FALSE_)
#define IAP2_CTRL_MSG_EXT_ACC_PROTOCOL						(_TRUE_)
#define IAP2_CTRL_MSG_HUMAN_INTERFACE_DEVICE				(_TRUE_)
#define IAP2_CTRL_MSG_LOCATION								(_FALSE_)
#if (MFICFG_ENABLE_METADATA)
#define IAP2_CTRL_MSG_MEDIA_LIBRARY_ACCESS					(_TRUE_)
#else
#define IAP2_CTRL_MSG_MEDIA_LIBRARY_ACCESS                  (_FALSE_)
#endif
#define IAP2_CTRL_MSG_NOW_PLAYING							(_TRUE_)
#define IAP2_CTRL_MSG_POWER									(_FALSE_)
#define IAP2_CTRL_MSG_USB_DEVICE_MODE_AUDIO					(_TRUE_)
#define IAP2_CTRL_MSG_VOICE_OVER							(_FALSE_)
#define IAP2_CTRL_MSG_WIFI_INFO_SHARING						(_FALSE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION
/* Message IDs Supported */

#define IAP2_CTRL_MSG_AUTH_CERTIFICATE						(0xAA01) /* Acc */
#define IAP2_CTRL_MSG_AUTH_RESPONSE							(0xAA03) /* Acc */
#define IAP2_CTRL_MSG_REQ_AUTH_CERTIFICATE					(0xAA00) /* Dev */
#define IAP2_CTRL_MSG_REQ_AUTH_CHALLENGE_RESPONSE			(0xAA02) /* Dev */
#define IAP2_CTRL_MSG_AUTH_FAILED							(0xAA04) /* Dev */
#define IAP2_CTRL_MSG_AUTH_SUCCEEDED						(0xAA05) /* Dev */
#if (IAP2_CTRL_MSG_ACCESSORY_AUTHENTICATION == _TRUE_)
#define IAP2_AUTH_ACC_MESSAGES								(0x02)
#define IAP2_AUTH_DEV_MESSAGES								(0x04)
#else
#define IAP2_AUTH_ACC_MESSAGES								(0x00)
#define IAP2_AUTH_DEV_MESSAGES								(0x00)
#endif

#define IAP2_CTRL_MSG_START_IDENTIFICATION					(0x1D00) /* Dev */
#define IAP2_CTRL_MSG_IDENTIFICATION_INFO					(0x1D01) /* Acc */
#define IAP2_CTRL_MSG_IDENTIFICATION_ACCEPTED				(0x1D02) /* Dev */
#define IAP2_CTRL_MSG_IDENTIFICATION_REJECTED				(0x1D03) /* Dev */
#define IAP2_CTRL_MSG_IDENTIFICATION_CANCELL				(0x1D05) /* Acc */
#define IAP2_CTRL_MSG_IDENTIFICATION_INFO_UPDATE			(0x1D06) /* Acc */
#if (IAP2_CTRL_MSG_ACCESSORY_IDENTIFICATION == _TRUE_)
#define IAP2_IDENTIFICATION_ACC_MESSAGES					(0x03)
#define IAP2_IDENTIFICATION_DEV_MESSAGES					(0x03)
#else
#define IAP2_IDENTIFICATION_ACC_MESSAGES					(0x00)
#define IAP2_IDENTIFICATION_DEV_MESSAGES					(0x00)
#endif

#define IAP2_CTRL_MSG_REQUEST_APP_LAUNCH					(0xEA02) /* Acc */
#if (IAP2_CTRL_MSG_APP_LAUNCH == _TRUE_)
#define IAP2_APP_LAUNCH_ACC_MESSAGES						(0x01)
#define IAP2_APP_LAUNCH_DEV_MESSAGES						(0x00)
#else
#define IAP2_APP_LAUNCH_ACC_MESSAGES						(0x00)
#define IAP2_APP_LAUNCH_DEV_MESSAGES						(0x00)
#endif

#define IAP2_CTRL_MSG_START_ASSISTIVE_TOUCH					(0x5400) /* Acc */
#define IAP2_CTRL_MSG_STOP_ASSISTIVE_TOUCH					(0x5401) /* Acc */
#define IAP2_CTRL_MSG_START_ASSISTIVE_TOUCH_INFO			(0x5402) /* Acc */
#define IAP2_CTRL_MSG_ASSISTIVE_TOUCH_INFO					(0x5403) /* Dev */
#define IAP2_CTRL_MSG_STOP_ASSISTIVE_TOUCH_INFO 			(0x5404) /* Acc */ 
#if (IAP2_CTRL_MSG_ASSISTIVE_TOUCH == _TRUE_)
#define IAP2_ASSISTIVE_TOUCH_ACC_MESSAGES					(0x04)
#define IAP2_ASSISTIVE_TOUCH_DEV_MESSAGES					(0x01)
#else
#define IAP2_ASSISTIVE_TOUCH_ACC_MESSAGES					(0x00)
#define IAP2_ASSISTIVE_TOUCH_DEV_MESSAGES					(0x00)
#endif

#define IAP2_CTRL_MSG_BLUETOOTH_COMPONENT_INFO				(0x4E01) /* Acc */
#define IAP2_CTRL_MSG_START_BLUETOOTH_CONN_UPDATES			(0x4E03) /* Acc */
#define IAP2_CTRL_MSG_BLUETOOTH_CONN_UPDATE					(0x4E04) /* Dev */
#define IAP2_CTRL_MSG_STOP_BLUETOOTH_CONN_UPDATE			(0x4E05) /* Acc */
#if (IAP2_CTRL_MSG_BLUETOOTH_STATUS == _TRUE_)
#define IAP2_BLUETOOTH_STATUS_ACC_MESSAGES					(0x03)
#define IAP2_BLUETOOTH_STATUS_DEV_MESSAGES					(0x01)
#else
#define IAP2_BLUETOOTH_STATUS_ACC_MESSAGES					(0x00)
#define IAP2_BLUETOOTH_STATUS_DEV_MESSAGES					(0x00)
#endif

#define IAP2_CTRL_MSG_REQ_DEV_AUTH_CERTIFICATE				(0xAA10) /* Acc */
#define IAP2_CTRL_MSG_DEV_AUTH_CERTIFICATE					(0xAA11) /* Dev */
#define IAP2_CTRL_MSG_REQ_DEV_AUTH_CHALLENGE_RESPONSE		(0xAA12) /* Acc */
#define IAP2_CTRL_MSG_DEV_AUTH_RESPONSE						(0xAA13) /* Dev */
#define IAP2_CTRL_MSG_DEV_AUTH_FAILED						(0xAA14) /* Acc */
#define IAP2_CTRL_MSG_DEV_AUTH_SUCCEEDED					(0xAA15) /* Acc */
#if (IAP2_CTRL_MSG_DEVICE_AUTHENTICATION == _TRUE_)
#define IAP2_DEVICE_AUTHENTICATION_ACC_MESSAGES				(0x04)
#define IAP2_DEVICE_AUTHENTICATION_DEV_MESSAGES				(0x02)
#else
#define IAP2_DEVICE_AUTHENTICATION_ACC_MESSAGES				(0x00)
#define IAP2_DEVICE_AUTHENTICATION_DEV_MESSAGES				(0x00)
#endif

#define IAP2_CTRL_MSG_START_EXT_ACC_PROTOCOL_SESSION		(0xEA00) /* Dev */
#define IAP2_CTRL_MSG_STOP_EXT_ACC_PROTOCOL_SESSION			(0xEA01) /* Dev */
#if (IAP2_CTRL_MSG_EXT_ACC_PROTOCOL == _TRUE_)
#define IAP2_EXT_ACC_PROTOCOL_ACC_MESSAGES					(0x00)
#define IAP2_EXT_ACC_PROTOCOL_DEV_MESSAGES					(0x02)
#else
#define IAP2_EXT_ACC_PROTOCOL_ACC_MESSAGES					(0x00)
#define IAP2_EXT_ACC_PROTOCOL_DEV_MESSAGES					(0x00)
#endif

#define IAP2_CTRL_MSG_START_HID								(0x6800) /* Acc */
#define IAP2_CTRL_MSG_DEVICE_HID_REPORT						(0x6801) /* Dev */ 
#define IAP2_CTRL_MSG_ACC_HID_REPORT						(0x6802) /* Acc */
#define IAP2_CTRL_MSG_STOP_HID								(0x6803) /* Acc */
#if (IAP2_CTRL_MSG_HUMAN_INTERFACE_DEVICE == _TRUE_)
#define IAP2_HUMAN_INTERFACE_DEVICE_ACC_MESSAGES			(0x03)
#define IAP2_HUMAN_INTERFACE_DEVICE_DEV_MESSAGES			(0x01)
#else
#define IAP2_HUMAN_INTERFACE_DEVICE_ACC_MESSAGES			(0x00)
#define IAP2_HUMAN_INTERFACE_DEVICE_DEV_MESSAGES			(0x00)
#endif

#define IAP2_CTRL_MSG_START_LOCATION_INFO					(0xFFFA) /* Dev */
#define IAP2_CTRL_MSG_LOCATION_INFO							(0xFFFB) /* Acc */
#define IAP2_CTRL_MSG_STOP_LOCATION_INFO					(0xFFFC) /* Dev */
#if (IAP2_CTRL_MSG_LOCATION == _TRUE_)
#define IAP2_LOCATION_ACC_MESSAGES							(0x01)
#define IAP2_LOCATION_DEV_MESSAGES							(0x02)
#else
#define IAP2_LOCATION_ACC_MESSAGES							(0x00)
#define IAP2_LOCATION_DEV_MESSAGES							(0x00)
#endif

#define IAP2_CTRL_MSG_START_MEDIA_LIBRARY_INFO				(0x4C00) /* Acc */
#define IAP2_CTRL_MSG_MEDIA_LIBRARY_INFO					(0x4C01) /* Dev */
#define IAP2_CTRL_MSG_STOP_MEDIA_LIBRARY_INFO				(0x4C02) /* Acc */
#define IAP2_CTRL_MSG_START_MEDIA_LIBRARY_UPDATES			(0x4C03) /* Acc */
#define IAP2_CTRL_MSG_MEDIA_LIBRARY_UPDATE					(0x4C04) /* Dev */
#define IAP2_CTRL_MSG_STOP_MEDIA_LIBRARY_UPDATES			(0x4C05) /* Acc */
#define IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_CURRENT_SEL		(0x4C06) /* Acc */
#define IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_ITEMS				(0x4C07) /* Acc */
#define IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_COLLECTION			(0x4C08) /* Acc */
#if (IAP2_CTRL_MSG_MEDIA_LIBRARY_ACCESS == _TRUE_)
#define IAP2_MEDIA_LIBRARY_ACCESS_ACC_MESSAGES				(0x07)
#define IAP2_MEDIA_LIBRARY_ACCESS_DEV_MESSAGES				(0x02)
#else
#define IAP2_MEDIA_LIBRARY_ACCESS_ACC_MESSAGES				(0x00)
#define IAP2_MEDIA_LIBRARY_ACCESS_DEV_MESSAGES				(0x00)
#endif

#define IAP2_CTRL_MSG_START_NOW_PLAYING_UPDATES				(0x5000) /* Acc */
#define IAP2_CTRL_MSG_NOW_PLAYING_UPDATE					(0x5001) /* Dev */
#define IAP2_CTRL_MSG_STOP_NOW_PLAYING_UPDATES				(0x5002) /* Acc */
#if (IAP2_CTRL_MSG_NOW_PLAYING == _TRUE_)
#define IAP2_NOW_PLAYING_ACC_MESSAGES						(0x02)
#define IAP2_NOW_PLAYING_DEV_MESSAGES						(0x01)
#else
#define IAP2_NOW_PLAYING_ACC_MESSAGES						(0x00)
#define IAP2_NOW_PLAYING_DEV_MESSAGES						(0x00)
#endif

#define IAP2_CTRL_MSG_START_POWER_UPDATES					(0xAE00) /* Acc */
#define IAP2_CTRL_MSG_POWER_UPDATE							(0xAE01) /* Dev */
#define IAP2_CTRL_MSG_STOP_POWER_UPDATES					(0xAE02) /* Acc */
#define IAP2_CTRL_MSG_POWER_SOURCE_UPDATE					(0xAE03) /* Acc */
#if (IAP2_CTRL_MSG_POWER == _TRUE_)
#if IAP2_IDENT_PWR_SOURCE_TYPE_DATA == IAP2_PWR_TYPE_SELF
#define IAP2_POWER_ACC_MESSAGES								(0x03)
#define IAP2_POWER_DEV_MESSAGES								(0x01)
#else
#define IAP2_POWER_ACC_MESSAGES								(0x02)
#define IAP2_POWER_DEV_MESSAGES								(0x01)
#endif
#else
#define IAP2_POWER_ACC_MESSAGES								(0x00)
#define IAP2_POWER_DEV_MESSAGES								(0x00)
#endif

#define IAP2_CTRL_MSG_START_USB_DEV_MODE_AUDIO				(0xDA00) /* Acc */
#define IAP2_CTRL_MSG_USB_DEV_MODE_AUDIO_INFO				(0xDA01) /* Dev */
#define IAP2_CTRL_MSG_STOP_USB_DEV_MODE_AUDIO				(0xDA02) /* Acc */
#if (IAP2_CTRL_MSG_USB_DEVICE_MODE_AUDIO == _TRUE_)
#define IAP2_USB_DEVICE_MODE_AUDIO_ACC_MESSAGES				(0x02)
#define IAP2_USB_DEVICE_MODE_AUDIO_DEV_MESSAGES				(0x01)
#else
#define IAP2_USB_DEVICE_MODE_AUDIO_ACC_MESSAGES				(0x00)
#define IAP2_USB_DEVICE_MODE_AUDIO_DEV_MESSAGES				(0x00)
#endif

#define IAP2_CTRL_MSG_START_VOICE_OVER						(0x5612) /* Acc */ 
#define IAP2_CTRL_MSG_STOP_VOICE_OVER						(0x5613) /* Acc */
#define IAP2_CTRL_MSG_REQ_VOICE_OVER_MOVE_CURSOR			(0x5601) /* Acc */
#define IAP2_CTRL_MSG_REQ_VOICE_OVER_ACTIVATE_CURSOR		(0x5602) /* Acc */
#define IAP2_CTRL_MSG_REQ_VOICE_OVER_SCROLL_PAGE			(0x5603) /* Acc */
#define IAP2_CTRL_MSG_REQ_VOICE_OVER_SPEAK_TEXT				(0x5606) /* Acc */
#define IAP2_CTRL_MSG_REQ_VOICE_OVER_PAUSE_TEXT				(0x5608) /* Acc */
#define IAP2_CTRL_MSG_REQ_VOICE_OVER_RESUME_TEXT			(0x5609) /* Acc */
#define IAP2_CTRL_MSG_START_VOICE_OVER_UPDATES				(0x560B) /* Acc */
#define IAP2_CTRL_MSG_VOICE_OVER_UPDATE						(0x560C) /* Dev */
#define IAP2_CTRL_MSG_STOP_VOICE_OVER_UPDATES				(0x560D) /* Acc */
#define IAP2_CTRL_MSG_REQ_VOICE_OVER_CONFIG					(0x560E) /* Acc */
#define IAP2_CTRL_MSG_START_VOICE_OVER_CURSOR_UPDATES		(0x560F) /* Acc */
#define IAP2_CTRL_MSG_VOICE_OVER_CURSOR_UPDATE				(0x5610) /* Dev */
#define IAP2_CTRL_MSG_STOP_VOICE_OVER_CURSOR_UPDATES		(0x5611) /* Acc */
#if (IAP2_CTRL_MSG_VOICE_OVER == _TRUE_)
#define IAP2_VOICE_OVER_ACC_MESSAGES						(0x0D)
#define IAP2_VOICE_OVER_DEV_MESSAGES						(0x02)
#else
#define IAP2_VOICE_OVER_ACC_MESSAGES						(0x00)
#define IAP2_VOICE_OVER_DEV_MESSAGES						(0x00)
#endif

#define IAP2_CTRL_MSG_REQ_WIFI_INFO							(0x5700) /* Acc */
#define IAP2_CTRL_MSG_WIFI_INFO								(0x5701) /* Dev */
#if (IAP2_CTRL_MSG_WIFI_INFO_SHARING == _TRUE_)
#define IAP2_WIFI_INFO_SHARING_ACC_MESSAGES					(0x01)
#define IAP2_WIFI_INFO_SHARING_DEV_MESSAGES					(0x01)
#else
#define IAP2_WIFI_INFO_SHARING_ACC_MESSAGES					(0x00)
#define IAP2_WIFI_INFO_SHARING_DEV_MESSAGES					(0x00)
#endif
#define	IAP2_CTRL_MSG_ACCESSORY_ANY_MESSAGE										(IAP2_CTRL_MSG_ACCESSORY_AUTHENTICATION | IAP2_CTRL_MSG_ACCESSORY_IDENTIFICATION | IAP2_CTRL_MSG_APP_LAUNCH | IAP2_CTRL_MSG_ASSISTIVE_TOUCH | IAP2_CTRL_MSG_BLUETOOTH_STATUS | IAP2_CTRL_MSG_DEVICE_AUTHENTICATION | IAP2_CTRL_MSG_EXT_ACC_PROTOCOL | IAP2_CTRL_MSG_HUMAN_INTERFACE_DEVICE | IAP2_CTRL_MSG_LOCATION | IAP2_CTRL_MSG_MEDIA_LIBRARY_ACCESS | IAP2_CTRL_MSG_NOW_PLAYING | IAP2_CTRL_MSG_POWER | IAP2_CTRL_MSG_USB_DEVICE_MODE_AUDIO | IAP2_CTRL_MSG_VOICE_OVER | IAP2_CTRL_MSG_WIFI_INFO_SHARING)

#define IAP2_IDENT_ACC_MESSAGE_SIZE							((IAP2_AUTH_ACC_MESSAGES + \
															IAP2_IDENTIFICATION_ACC_MESSAGES + \
															IAP2_APP_LAUNCH_ACC_MESSAGES + \
															IAP2_ASSISTIVE_TOUCH_ACC_MESSAGES + \
															IAP2_BLUETOOTH_STATUS_ACC_MESSAGES + \
															IAP2_DEVICE_AUTHENTICATION_ACC_MESSAGES + \
															IAP2_EXT_ACC_PROTOCOL_ACC_MESSAGES + \
															IAP2_HUMAN_INTERFACE_DEVICE_ACC_MESSAGES + \
															IAP2_LOCATION_ACC_MESSAGES + \
															IAP2_MEDIA_LIBRARY_ACCESS_ACC_MESSAGES + \
															IAP2_NOW_PLAYING_ACC_MESSAGES + \
															IAP2_POWER_ACC_MESSAGES + \
															IAP2_USB_DEVICE_MODE_AUDIO_ACC_MESSAGES + \
															IAP2_VOICE_OVER_ACC_MESSAGES + \
															IAP2_WIFI_INFO_SHARING_ACC_MESSAGES)*2)

#define IAP2_IDENT_DEV_MESSAGE_SIZE							((IAP2_AUTH_DEV_MESSAGES + \
															IAP2_IDENTIFICATION_DEV_MESSAGES + \
															IAP2_APP_LAUNCH_DEV_MESSAGES + \
															IAP2_ASSISTIVE_TOUCH_DEV_MESSAGES + \
															IAP2_BLUETOOTH_STATUS_DEV_MESSAGES + \
															IAP2_DEVICE_AUTHENTICATION_DEV_MESSAGES + \
															IAP2_EXT_ACC_PROTOCOL_DEV_MESSAGES + \
															IAP2_HUMAN_INTERFACE_DEVICE_DEV_MESSAGES + \
															IAP2_LOCATION_DEV_MESSAGES + \
															IAP2_MEDIA_LIBRARY_ACCESS_DEV_MESSAGES + \
															IAP2_NOW_PLAYING_DEV_MESSAGES + \
															IAP2_POWER_DEV_MESSAGES + \
															IAP2_USB_DEVICE_MODE_AUDIO_DEV_MESSAGES + \
															IAP2_VOICE_OVER_DEV_MESSAGES + \
															IAP2_WIFI_INFO_SHARING_DEV_MESSAGES)*2)

/*-----------------------------------------------------------------------------------------------*/
/* Preferred App Bundle Seed Identifier */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_PREF_APP_BUNDLE_SEED_ID                  (_FALSE_)
/*-----------------------------------------------------------------------------------------------*/
/* Current Language */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_CURRENT_LANGUAGE_DATA		 			"en"
/*-----------------------------------------------------------------------------------------------*/
/* Supported Language */
/*-----------------------------------------------------------------------------------------------*/
#define IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES				(0x02)
#define IAP2_IDENT_SUPPORTED_LANGUAGES_SIZE					(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES * 3)

#define IAP2_IDENT_SUPPORTED_LANGUAGES_DATA0	 			"en"
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 1)
#define IAP2_IDENT_SUPPORTED_LANGUAGES_DATA1		 		"es"
#endif
/*-----------------------------------------------------------------------------------------------*/
/* Serial Transport Component */
/*-----------------------------------------------------------------------------------------------*/
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION 
#define IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT				(_FALSE_)
#define IAP2_IDENT_SERIAL_TRANSPORT_SUPPORTS_IAP2			(_FALSE_)
#endif
#if(IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT == _TRUE_)
#define IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT_ID_DATA		(0x0000) /* All transport Ids must be unique */
#define IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT_NAME_DATA		"Serial FSL"
#endif

/*-----------------------------------------------------------------------------------------------*/
/* USB Device Transport Component */
/*-----------------------------------------------------------------------------------------------*/
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT              (_TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_SUPPORTS_IAP2          (_TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_8000HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_11025HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_12000HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_16000HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_22050HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_24000HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_32000HZ		(_TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_44100HZ		(_TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_48000HZ		(_TRUE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION 

#if (IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT == _TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_ID_DATA 		(0x0000) /* All transport Ids must be unique */
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_NAME_DATA	"USB Dev FSL"
#if (IAP2_IDENT_USB_DEV_TRANSPORT_SUPPORTS_IAP2 == _TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_SUPPORTS_IAP2_SIZE	 	(0x00)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_8000HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR0				(0x00)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_11025HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR1				(0x01)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_12000HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR2				(0x02)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_16000HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR3				(0x03)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_22050HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR4				(0x04)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_24000HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR5				(0x05)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_32000HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR6				(0x06)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_44100HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR7				(0x07)
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_48000HZ == _TRUE_)
#define IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR8				(0x08)
#endif
#endif

/*-----------------------------------------------------------------------------------------------*/
/* USB Host Transport Component */
/*-----------------------------------------------------------------------------------------------*/
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT                         (_FALSE_)
#define IAP2_IDENT_USB_HOST_TRANSPORT_SUPPORTS_IAP2                     (_TRUE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION
#if (IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT == _TRUE_)
#define IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT_ID_DATA		(0x0001) /* All transport Ids must be unique */
#define IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT_NAME_DATA	"USB Host FSL"
#endif

/*-----------------------------------------------------------------------------------------------*/
/* Bluetooth Transport Component */
/*-----------------------------------------------------------------------------------------------*/
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION 
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT   			(_FALSE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES	(0x02)

#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT == _TRUE_)
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 0)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA0	(0x0000)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA0	"Bluetooth FSL"
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP20		(_TRUE_)
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP20 == _TRUE_)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP2_SIZE0	(0x00)
#endif
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA0	{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00}
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 1)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA1	(0x0001)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA1	"Bluetooth FSL"
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP21		(_TRUE_)
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP21 == _TRUE_)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP2_SIZE1	(0x00)
#endif
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA1	{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01}
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 2)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA2	(0x0002)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA2	"Bluetooth FSL"
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP22		(_TRUE_)
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP22 == _TRUE_)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP2_SIZE2	(0x00)
#endif
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA2	{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x02}
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 3)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA3	(0x0003)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA3	"Bluetooth FSL"
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP23		(_TRUE_)
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP23 == _TRUE_)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP2_SIZE3	(0x00)
#endif
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA3	{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x03}
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 4)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA4	(0x0004)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA4	"Bluetooth FSL"
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP24		(_TRUE_)
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP24 == _TRUE_)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP2_SIZE4	(0x00)
#endif
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA4	{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x04}
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 5)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA5	(0x0005)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA5	"Bluetooth FSL"
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP25		(_TRUE_)
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP25 == _TRUE_)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP2_SIZE5	(0x00)
#endif
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA5	{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x05}
#endif
#endif

/*-----------------------------------------------------------------------------------------------*/
/* iAP HID Component */
/*-----------------------------------------------------------------------------------------------*/
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION 
#define IAP2_IDENT_HID_COMPONENT   							(_TRUE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_IDENT_HID_COMPONENT_INSTANCES					(0x02)

#if (IAP2_IDENT_HID_COMPONENT == _TRUE_)
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 0)
#define IAP2_IDENT_HID_COMPONENT_ID_DATA0					(0x0001)
#define IAP2_IDENT_HID_COMPONENT_NAME_DATA0					"iAP HID Media Player"
#define IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA0				(0x01)
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 1)
#define IAP2_IDENT_HID_COMPONENT_ID_DATA1					(0x0002)
#define IAP2_IDENT_HID_COMPONENT_NAME_DATA1					"iAP HID Assistive Touch"
#define IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA1				(0x02)
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 2)
#define IAP2_IDENT_HID_COMPONENT_ID_DATA2					(0x0002)
#define IAP2_IDENT_HID_COMPONENT_NAME_DATA2					"iAP HID FSL"
#define IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA2				(0x00)
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 3)
#define IAP2_IDENT_HID_COMPONENT_ID_DATA3					(0x0003)
#define IAP2_IDENT_HID_COMPONENT_NAME_DATA3					"iAP HID FSL"
#define IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA3				(0x00)
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 4)
#define IAP2_IDENT_HID_COMPONENT_ID_DATA4					(0x0004)
#define IAP2_IDENT_HID_COMPONENT_NAME_DATA4					"iAP HID FSL"
#define IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA4				(0x00)
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 5)
#define IAP2_IDENT_HID_COMPONENT_ID_DATA5					(0x0005)
#define IAP2_IDENT_HID_COMPONENT_NAME_DATA5					"iAP HID FSL"
#define IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA5				(0x00)
#endif
#endif

/*-----------------------------------------------------------------------------------------------*/
/* USB Host HID Component */
/*-----------------------------------------------------------------------------------------------*/
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION 
#define IAP2_IDENT_USB_HOST_HID_COMPONENT   				(_FALSE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES			(0x02)

#if (IAP2_IDENT_USB_HOST_HID_COMPONENT == _TRUE_)
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 0)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA0			(0x0000)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA0		"USB Host HID FSL"
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA0	(0x00)
/* Must refer to a USB Host Transport component ID */
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA0	(0x0000)
/* Must match the Acc  corresponding USB descriptor */
#define IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA0	(0x00)
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 1)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA1			(0x0001)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA1		"USB Host HID FSL"
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA1	(0x00)
/* Must refer to a USB Host Transport component ID */
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA1	(0x0000)
/* Must match the Acc  corresponding USB descriptor */
#define IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA1	(0x00)
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 2)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA2			(0x0002)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA2		"USB Host HID FSL"
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA2	(0x00)
/* Must refer to a USB Host Transport component ID */
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA2	(0x0000)
/* Must match the Acc  corresponding USB descriptor */
#define IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA2	(0x00)
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 3)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA3			(0x0003)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA3		"USB Host HID FSL"
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA3	(0x00)
/* Must refer to a USB Host Transport component ID */
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA3	(0x0000) 
/* Must match the Acc  corresponding USB descriptor */
#define IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA3	(0x00)
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 4)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA4			(0x0004)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA4		"USB Host HID FSL"
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA4	(0x00)
/* Must refer to a USB Host Transport component ID */
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA4	(0x0000)
/* Must match the Acc  corresponding USB descriptor */
#define IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA4	(0x00)
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 5)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA5			(0x0005)
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA5		"USB Host HID FSL"
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA5	(0x00)
/* Must refer to a USB Host Transport component ID */
#define IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA5	(0x0000)
/* Must match the Acc  corresponding USB descriptor */
#define IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA5	(0x00)
#endif
#endif

/** Identification Parameter IDs Enumeration */
enum PARAM_IDS
{
	NAME_PARAM = 0, 								/* 0 */
	MODEL_IDENTIFIER_PARAM, 						/* 1 */
	MANUFACTURER_PARAM,								/* 2 */
	SERIAL_NUMBER_PARAM,							/* 3 */
	FIRMWARE_VERSION_PARAM,							/* 4 */
	HARDWARE_VERSION_PARAM,							/* 5 */
	MESSAGES_SENT_BY_ACC_PARAM,						/* 6 */
	MESSAGES_RECEIVED_FROM_DEV_PARAM,				/* 7 */
	POWER_SOURCE_TYPE_PARAM,						/* 8 */
	MAX_CURRENT_DRAWN_FROM_DEV_PARAM,				/* 9 */
	SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,			/* 10 */
	PREFERRED_APP_BUNDLE_SEED_ID_PARAM,				/* 11 */
	CURRENT_LANGUAGE_PARAM,							/* 12 */
	SUPPORTED_LAGUAGE_PARAM,						/* 13 */
	SERIAL_TRANSPORT_COMPONENT_PARAM,				/* 14 */
	USB_DEV_TRANSPORT_COMPONENT_PARAM,				/* 15 */
	USB_HOST_TRANSPORT_COMPNENT_PARAM,				/* 16 */
	BLUETOOTH_TRANSPORT_COMPONENT_PARAM,			/* 17 */
	IAP_HID_COMPONENT_PARAM,						/* 18 */
	USB_HOST_HID_COMPONENT_PARAM					/* 19 */
};

/** External Accessory Protocol parameter group */
enum EXT_ACC_PROTOCOL_SUBPARAM_IDS
{
	EXT_ACC_PROTOCOL_ID_SUBPARAM = 0, 						/* 0 */
	EXT_ACC_PROTOCOL_NAME_SUBPARAM, 						/* 1 */
	EXT_ACC_PROTOCOL_MATCH_ACTION_SUBPARAM,					/* 2 */
	EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_SUBPARAM				/* 3 */
};

/** Serial Transport Component parameter group */
enum TRANSPORT_SERIAL_COMPONENT_SUBPARAM_IDS
{
	TRANSPORT_SERIAL_COMPONENT_ID_SUBPARAM = 0,				/* 0 */
	TRANSPORT_SERIAL_COMPONENT_NAME_SUBPARAM, 				/* 1 */
	TRANSPORT_SERIAL_COMPONENT_SUPPORTS_IAP2_SUBPARAM		/* 2 */
};

/** USB Device transport component parameter group */
enum USB_DEV_TRANSPORT_COMPONENT_SUBPARAM_IDS
{
	USB_DEV_TRANSPORT_COMPONENT_ID_SUBPARAM = 0,			/* 0 */
	USB_DEV_TRANSPORT_COMPONENT_NAME_SUBPARAM, 				/* 1 */
	USB_DEV_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM,		/* 2 */
	USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM			/* 3 */
};

/** USB Host transport component parameter group */
enum USB_HOST_TRANSPORT_COMPONENT_SUBPARAM_IDS
{
	USB_HOST_TRANSPORT_COMPONENT_ID_SUBPARAM = 0,			/* 0 */
	USB_HOST_TRANSPORT_COMPONENT_NAME_SUBPARAM, 			/* 1 */
	USB_HOST_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM		/* 2 */
};

/** USB Host transport component parameter group */
enum BLUETOOTH_TRANSPORT_COMPONENT_SUBPARAM_IDS
{
	BLUETOOTH_TRANSPORT_COMPONENT_ID_SUBPARAM = 0,			/* 0 */
	BLUETOOTH_TRANSPORT_COMPONENT_NAME_SUBPARAM, 			/* 1 */
	BLUETOOTH_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM,	/* 2 */
	BLUETOOTH_TRANSPORT_MAC_ADDRESS_SUBPARAM				/* 3 */
};

/** iAP HID Component parameter group */
enum IAP_HID_COMPONENT_SUBPARAM_IDS
{
	IAP_HID_COMPONENT_ID_SUBPARAM = 0,						/* 0 */
	IAP_HID_COMPONENT_NAME_SUBPARAM, 						/* 1 */
	IAP_HID_COMPONENT_FUNCTION_SUBPARAM						/* 2 */
};

/** USB Host HID Component parameter group */
enum USB_HOST_HID_COMPONENT_SUBPARAM_IDS
{
	USB_HOST_HID_COMPONENT_ID_SUBPARAM = 0,					/* 0 */
	USB_HOST_COMPONENT_NAME_SUBPARAM, 						/* 1 */
	USB_HOST_COMPONENT_FUNCTION_SUBPARAM,					/* 2 */
	USB_HOST_TRANS_COMPONENT_ID_SUBPARAM,					/* 3 */
	USB_HOST_TRANS_INTERFACE_NUM_SUBPARAM					/* 4 */
};

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef struct
{
	/*Pointers with the new Identification information*/
	uint8_t *pbName;
	uint8_t *pbModeIdentifier;
	uint8_t *pbManufacturer;
	uint8_t *pbSerialNumber;
	uint8_t *pbFirmwareVersion;
	uint8_t *pbHardwareVersion;
	uint8_t *pbCurrentLanguage;
	/*Sizes for each parameter for update information*/
	uint8_t bNameSize;
	uint8_t bModeIdentifierSize;
	uint8_t bManufacturerSize;
	uint8_t bSerialNumberSize;
	uint8_t bFirmwareVersionSize;
	uint8_t bHardwareVersionSize;
	uint8_t bCurrentLanguageSize;
} iAP2_Identification_InformationUpdateData; /**<Structure with data for an IdentificationInformationUpdate*/

typedef struct
{
	uint32_t dwRejectedIdentificationInfo;	/**< used to identify rejected parameters */
	uint16_t *pwMessageReceivedFromDevice;  /**<Rejected ID from iPod,iPad or iPhone*/
	uint16_t *pwMessageSentByAccesory;	   /**<Rejected ID by accessory*/
	uint16_t wMessageReceivedFromDeviceSize; /**<Number of rejected message from iPod,iPad or iPhone*/
	uint16_t wMessageSentByAccesorySize;		/**<Number of rejected message by accessory*/
} iAP2_Identification_RejectedInformation;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void iAP2_Feat_ID_vfnCallBack(uint16_t wMessageID);

uint8_t iAP2_Identification_bfnSendUpdateInformation(iAP2_Identification_InformationUpdateData *InformationUpdatesApp);
uint8_t iAP2_Identification_bfnSendIdentificationInfo(iAP2_Identification_RejectedInformation * eRejectedInfoPtr);
uint8_t iAP2_Identification_bfnSendCancell(void);

/*************************************************************************************************/

#endif /* IAP2_IDENTIFICATION_H_ */

