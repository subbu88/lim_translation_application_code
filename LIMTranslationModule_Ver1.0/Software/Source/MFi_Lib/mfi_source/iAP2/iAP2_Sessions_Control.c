/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Identification_Feat.h"
#include "iAP2_Sessions_Control.h"
#include "iAP2_Link.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define IAP2_CTRL_SESSION_CALLBACK		iAP2_Features_Arbitrator
#define SESSIONS_UNIT_TESTING 			(_FALSE_)

#define CTRL_MSG_MAX_MEMREQ_IDS			(IAP2_MAX_NUM_OUTSTANDING_PACKET*2)

#define CTRL_MSG_MEMREQS_ATTEMPTS		(1) /* Choose value only 1 or 2, but NOT 0*/

/* Maximum number of possible group parameters in a single control message, at:
 * Accessory Identification - IdentificationInformation (0x1D01), has 7 group
 * type parameter.
 */
#define CTRL_MSG_MAX_PARAM_GRPS			(7)
#define CTRL_MSG_PARAM_GRPS_BUFF_CTRL	(CTRL_MSG_MAX_PARAM_GRPS*2)

/* Control Message Sequence Status */
#define CTRL_MSG_API_INIT_IDLE			(1)
#define CTRL_MSG_API_SET_PARAM			(2)

/* Start - Control Message Structure */
#define CTRL_MSG_SOM_MSB				(0x40) /* SOM: Start Of Message */
#define CTRL_MSG_SOM_LSB				(0x40) /* SOM: Start Of Message */

#define CTRL_MSG_SOM_MSB_OFFSET			(0)
#define CTRL_MSG_SOM_LSB_OFFSET			(1)
#define CTRL_MSG_LENGTHM_OFFSET			(2)
#define CTRL_MSG_LENGTHL_OFFSET			(3)
#define CTRL_MSG_ID_MSB_OFFSET			(4)
#define CTRL_MSG_ID_LSB_OFFSET			(5)
#define CTRL_MSG_PARAM_OFFSET			(6)

#define CTRL_MSG_PRMHDR_LENGTHM_OFFSET	(0)
#define CTRL_MSG_PRMHDR_LENGTHL_OFFSET	(1)
#define CTRL_MSG_PRMHDR_ID_MSB_OFFSET	(2)
#define CTRL_MSG_PRMHDR_ID_LSB_OFFSET	(3)
#define CTRL_MSG_PARAM_DATA_OFFSET		(4)

#define CTRL_MSG_SUBPRMHDR_LNGTHM_OFFST	(0)
#define CTRL_MSG_SUBPRMHDR_LNGTHL_OFFST	(1)
#define CTRL_MSG_SUBPRMHDR_ID_MSB_OFFST	(2)
#define CTRL_MSG_SUBPRMHDR_ID_LSB_OFFST	(3)
#define CTRL_MSG_SUBPARAM_DATA_OFFSET	(4)
/* End - Control Message Structure */

#define CTRL_MSG_START_OF_FRAME			(0x40)
#define IAP_FEATURES_CENTRALIZED_FN_CALLBACK(x)

#define CTRL_MSG_UNKNOWN_SIZE			(0)

/* States of FSM in function 'iAP2_Sessions_bfnSetInBuffer(...)' */
#define IAP2_SESSIONS_STATE_AVAILABLE_MEMORY	(0)
#define IAP2_SESSIONS_STATE_ALL_DATA_COPY		(1)
#define IAP2_SESSIONS_STATE_REQ_MEMORY			(2)
#define IAP2_SESSIONS_STATE_PARTIAL_DATA_COPY	(3)


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/
typedef union
{
	uint32_t dwData;
	struct
	{
		uint16_t wSize;
		uint16_t wID;
	}sElem;
}IAP2_SESS_CTRL_HEADER;

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
uint8_t iAP2_Sessions_gbCtrlMsgStatusFlags = 0x00;
/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static IAP2_SESS_CTRL_HEADER iAP2_Sess_Ctrl_Headers[IAP2_SESS_CTRL_NEST_MAX];
static uint16_t iAP2_Sess_Ctrl_waDataRem[IAP2_SESS_CTRL_NEST_MAX];
static uint16_t iAP2_Sess_Ctrl_bCurPktNest;
static uint16_t iAP2_Sess_Ctrl_bCurMsgNest;
static uint16_t iAP2_Sess_Ctrl_wAdjNest;
static uint16_t iAP2_Sess_Ctrl_wAdjSize;
static uint16_t iAP2_Sess_Ctrl_wPktSize;
static uint16_t iAP2_Sess_Ctrl_wPktIndex;

static uint16_t	wLinkBufferSize = 0;
static uint16_t	wLinkBufferFree = 0;
static uint16_t	wFrameTotalBytesIdx = 0;
static uint16_t	wCurrentMemReqIdx = 0;/* stores array index of the last reserved memory chunk in used */
static uint16_t	wParamGroupTotalBytes = 0;
static uint16_t	wTotalSubparametersInGroup = 0;
static uint16_t 	wCurrentParamId = 0;
static uint16_t 	wPreviousParamId = 0;
static uint8_t 		wPrevCompID = 0;
static uint16_t 	iAP2_Sess_Ctrl_wMsgMaxSize;
static uint8_t	baMemRequestIds[CTRL_MSG_MAX_MEMREQ_IDS];
static uint8_t 	bSeqStats = CTRL_MSG_API_INIT_IDLE; /* API Sequence Status */
static uint8_t	*bpLinkBuffer = NULL;
static uint8_t	*bpLinkBufferCtrlMsgStart = NULL; /* The message header will be found here */
static uint8_t	bMemRequestIdsCount = 0;
static uint8_t 	*bpaCurrentGrpParamSizeStorage[2] = {NULL, NULL};


#if (SESSIONS_UNIT_TESTING == _TRUE_)
uint8_t gbaLinkBufferMock[100];

uint16_t iAP_Sessions_gwFrameSize;
uint16_t iAP_Sessions_gwReadIndex;
uint16_t iAP_Sessions_gwMsgLength;
uint16_t iAP_Sessions_gwMsgId;
uint16_t iAP_Sessions_gwParameterLength;
uint16_t iAP_Sessions_gwParameterId;
uint16_t iAP_Sessions_gwSubParameterLength;
uint16_t iAP_Sessions_gwParameterLengthBackUp;
uint16_t iAP_Sessions_gwSubParameterId;
uint16_t iAP_Sessions_gwParameterIdBackUp;
uint16_t iAP_Sessions_gwBytesReadFromParam;
uint16_t iAP_Sessions_gwBytesToReadFromParam;
uint16_t iAP_Sessions_gwParamLengthBackUp;
uint16_t iAP_Sessions_gwMsgLengthBackup;
uint8_t  baCtrlMsgHeaderBuffer[CTRL_MSG_HEADER_SIZE]={0,0,0,0,0,0};
uint8_t  iAP_Sessions_gbaParameterBufer[0x500];
#endif

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void iAP2_Sessions_vfnResetControls(void);
static uint8_t iAP2_Sessions_vfnSetCtrlMsgHeader(uint16_t wCtrlMsgID, uint16_t wSize);
static void iAP2_Sessions_vfnSetCtrlMsgHeaderSize(uint16_t wDataSize);
static uint8_t iAP2_Sessions_bfnSetInBuffer(uint8_t baData[], uint16_t wDataSize, uint8_t* bpaDataStoredAt[]);
static uint8_t iAP2_Sessions_bfnMemoryRequest(uint8_t **bppIndex, uint16_t wDataSize);
static void iAP2_Sessions_vfnFlushRequestedMem(void);
static uint8_t iAP2_Sessions_bfnSetCtrlMsgGrpParamHeader(uint16_t wParamID);
#if defined __CC_ARM
static __inline void iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(uint8_t bCheckPrevParamId);
#else
static inline void iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(uint8_t bCheckPrevParamId);
#endif
static void iAP2_Sessions_vfnWriteDataToLinkBuffer(uint16_t wDataSize, uint8_t* baData, uint8_t* bpaDataStoredAt[], 
																						uint16_t *wpCurrentPartialDataIdx);
static uint8_t iAP2_Sessions_Ctrl_bfnReadData(uint8_t *bpBuff, uint16_t wSize);
static uint8_t iAP2_Sessions_Ctrl_bfnNestAdjust(void);

void IAP2_CTRL_SESSION_CALLBACK(uint16_t wMsgId);

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
void iAP2_Sessions_Ctrl_vfnReset(void)
{
	uint8_t dataRemRst = 0;

	/* Reset any pending flags and set pending message data to 0 */
	iAP2_Sessions_gbCtrlMsgStatusFlags = 0;

	for(dataRemRst = 0; dataRemRst < IAP2_SESS_CTRL_NEST_MAX; dataRemRst++)
	{
		iAP2_Sess_Ctrl_waDataRem[dataRemRst] = 0;
	}
}

/*!
 	 @fn 		uint8_t iAP2_Sessions_bfnSendCtrlMessage(uint16_t wCtrlMsgID, uint8_t baData[], uint16_t wDataSize,
 	 	 	 	 	 uint16_t wMaxDataSize)
 	 @brief		This function has two different modes of use:
 	 	 	 	
 	 	 	 	Mode A: this function alone can be used to send a control message with zero or one
 	 	 	 			parameter, where the parameter ID will be zero. The way the function 

parameters
 	 	 	 			are set will determine if zero or one control message parameters are to 

be
 	 	 	 			send:
 	 	 	 		
 	 	 	 		For zero control message parameters:
 	 	 	 			iAP2_Sessions_bfnSendCtrlMessage(CtrlMsgID,	NULL,		0,		

0);
 	 	 	 		
 	 	 	 		For one control message parameter (where parameter ID will be zero, if required
 	 	 	 		to send a parameter with a different ID use 'Mode B'):
 	 	 	 			iAP2_Sessions_bfnSendCtrlMessage(CtrlMsgID,	ptrData, 	0xYYYY,	

0xZZZZ);
 	 	 	 			
 	 	 	 		where parameters:
 	 	 	 		 	CtrlMsgID:	Control message ID
 	 	 	 		 	ptrData:	uint8_t* - pointer to a uint8_t data type array or to MSB of a 

non-array data
 	 	 	 		 				type, for this last, a cast to (uint8_t*) will be 

required. 
 	 	 	 			0xYYYY:		Size in bytes of the pointed, by param. 'ptrData', data.
 	 	 	 			0xZZZZ:		WARNING: Should NOT be zero when sending ONE control 

message
 	 	 	 						parameter.
 	 	 	 						The value passed through this function parameter 

is the
 	 	 	 						approximated size in bytes of the complete 

control message.
 	 	 	 						The more precision this value holds the more 

efficient the function
 	 	 	 						works (regarding memory usage and CPU cycles). 

Since this value is
 	 	 	 						only an approximation used for efficiency, 

passing any value [other
 	 	 	 						zero] the function will still work.
 	 	 	 	
 	 	 	 	Mode B:	along with the functions:
 	 	 	 		uint8_t iAP2_Sessions_bfnAddCtrlMsgParam(uint16_t wParamID, uint16_t wSubparamId, uint8_t baData[], uint16_t wDataSize)
					and									
					uint8_t iAP2_Sessions_bfnEndSendCtrlMsg(void)
					
					The three functions offer the possibility to send any other type of control
					message (that does not have zero or one parameter -when if one parameter is used
					its parameter ID must not be zero-, for this case use this function�s 'Mode A'
					explained before). The control messages that are able to be send in this mode 

are:
					
					->control messages with two or more parameters
					->one parameter with parameter ID different to zero
					->one or more control message group type parameter
					
					Call sequence:
						1. iAP2_Sessoions_bfnSendCtrlMessage(CtrlMsgID, NULL, 0, 0xZZZZ);
						2. iAP2_Sessions_bfnAddCtrlMsgParam(ParamID, SubparamID, ptrData, 

0xYYYY);
						   
						   == Call this function once for each control message parameter or group
						   subparameter to be send ==
						3. iAP2_Sessions_bfnEndSendCtrlMsg(void);
						
					where parameters:
						CtrlMsgID:	Control message ID
 	 	 	 			0xZZZZ:		The value passed through this function parameter is the
 	 	 	 						approximated size in bytes of the complete 

control message.
 	 	 	 						The more precision this value holds the more 

efficient the function
 	 	 	 						works (regarding memory usage and CPU cycles). 

Since this value is
 	 	 	 						only an approximation used for efficiency, 

passing any value [other
 	 	 	 						zero] the function will still work.
 	 	 	 			ParamID:	Control message parameter ID.
 	 	 	 			SubparamID: In case a group is send this value is the control message 

group type
 	 	 	 						parameter ID; if no group is to be send set this 

value to the defined
 	 	 	 						value 'IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM'.
 	 	 	 		 	ptrData:	uint8_t* - pointer to a uint8_t data type array or to MSB of a 

non-array data
 	 	 	 		 				type, for this last, a cast to (uint8_t*) will be 

required.
 	 	 	 		 	0xYYYY:		Size in bytes of the pointed to data. 
 	 
	@param		wCtrlMsgID: 	Control Message ID.
	@param 		baData:			uint8_t* - pointer to a uint8_t data type array or to MSB of a non-array data
								type, for this last, a cast to (uint8_t*) will be required.
	@param		wDataSize:		Size in bytes of the pointed, by param. 'baData', data.
	@param		wMaxDataSize:	Approximated size in bytes of the complete control message.
 	 	 	 					The more precision this value holds the more efficient 

the function
 	 	 	 					works (regarding memory usage and CPU cycles). Since this 

value is
 	 	 	 					only an approximation used for efficiency, passing any 

value [other
 	 	 	 					zero] the function will still work. Set to '0' when using 

'Mode A' and
 	 	 	 					zero parameters.

 	 @return	SESSIONS_OK:			Initialized sending procedure of control session 
 	 	 	 							message successfully.
				SESSIONS_BUSY:			Unable to reserve initial required memory, try again 
										latter.
				SESSIONS_WRONG_INIT:	The function was called previously already.
				SESSIONS_MEM_FULL:		Fatal error the link layer is unable to reserve the
										required memory and will not be able to 

do it in the
										future. Unable to send the message with 

the current
										accumulated data size and memory 

configuration.
*/
uint8_t iAP2_Sessions_bfnSendCtrlMessage(uint16_t wCtrlMsgID, uint8_t baData[], uint16_t wDataSize, uint16_t wMaxDataSize)
{
	uint16_t wControlMessageHeaderSize = CTRL_MSG_UNKNOWN_SIZE;
	uint8_t bFinalResult;
	uint8_t bIsOneParam = ((NULL != baData) && (0 != wDataSize) && (0 != wMaxDataSize));
	uint8_t bIsZeroPGrpPTwoP = ((NULL == baData) && (0 == wDataSize));
	uint8_t bIsZeroParams = bIsZeroPGrpPTwoP && (wMaxDataSize == 0);
	
	if((CTRL_MSG_API_INIT_IDLE == bSeqStats) && (bIsOneParam || bIsZeroPGrpPTwoP))
	{
		/* Flush possible left overs of previous send sequences with wrong order (by other layers) */
		iAP2_Sessions_vfnFlushRequestedMem();
		/* Reset all variables used for module�s control */
		iAP2_Sessions_vfnResetControls();
				
		if(CTRL_MSG_HEADER_SIZE > wMaxDataSize)
		{
			wMaxDataSize = CTRL_MSG_HEADER_SIZE;
			wControlMessageHeaderSize = CTRL_MSG_HEADER_SIZE;
		}
		
		if(bIsOneParam)
		{
			wControlMessageHeaderSize = CTRL_MSG_HEADER_SIZE + CTRL_MSG_PARAMHEADER_SIZE + wDataSize;
			bSeqStats = CTRL_MSG_API_SET_PARAM;
			
		}

		/* If available the link layer will provide memory size wMaxDataSize */
		bFinalResult = iAP2_Sessions_bfnMemoryRequest(&bpLinkBuffer, wMaxDataSize);
		
		if(SESSIONS_OK == bFinalResult) /* OK */
		{
			/* Store the max size requested initially */
			iAP2_Sess_Ctrl_wMsgMaxSize = wMaxDataSize - wLinkBufferFree;
			
			/* Saving pointer to memory where the message header will be stored at */
			bpLinkBufferCtrlMsgStart = bpLinkBuffer;
			bFinalResult = iAP2_Sessions_vfnSetCtrlMsgHeader(wCtrlMsgID, wControlMessageHeaderSize);
			
			if (SESSIONS_OK == bFinalResult)
			{
				if (bIsOneParam)
				{
					/* One parameter with ID == 0 */
					
					bFinalResult = iAP2_Sessions_bfnAddCtrlMsgParam(0, IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, baData, 
																											wDataSize);
					if(SESSIONS_OK == bFinalResult)
					{
						/* Close send sequence */
						bFinalResult = iAP2_Sessions_bfnEndSendCtrlMsg();						
					}
					else
					{
						/* Flush possible left overs of previous send sequences with wrong order (by other layers) */
						iAP2_Sessions_vfnFlushRequestedMem();
						/* Reset all variables used for module�s control */
						iAP2_Sessions_vfnResetControls();
					}
					bSeqStats = CTRL_MSG_API_INIT_IDLE;
				}
				else
				{	
					/* Zero parameters, one parameter with ID != 0, group type parameter or 2+ parameters */
					
					bSeqStats = CTRL_MSG_API_SET_PARAM;
					
					if(bIsZeroParams)
					{
						bFinalResult = iAP2_Sessions_bfnEndSendCtrlMsg();
					}
				}
			}
			else
			{
				bSeqStats = CTRL_MSG_API_INIT_IDLE;
			}
		}
		/* else
		{*/
			/* Do nothing / function�s return value will be the same as returned value
			 * from previously called function iAP2_Sessions_bfnMemoryRequest()
		}*/
	}
	else if(CTRL_MSG_MAX_MEMREQ_IDS <= bMemRequestIdsCount)
	{ /* Related array size calculated so this case does not happen */
		bFinalResult = SESSIONS_MEM_FULL;
	}
	else
	{
		bFinalResult = SESSIONS_WRONG_INIT;
	}
		
	return bFinalResult;
}

/*!
	@fn 		uint8_t iAP2_Sessions_bfnAddCtrlMsgParam(uint16_t wParamID, uint16_t wSubparamId, uint8_t baData[], 
 	 	 	 		uint16_t wDataSize)
	@brief		Part of the control message send sequence, used to add any type of parameters to
 	 	 	 	the control message; where parameters may also be sub-parameters of groups.
 	 	 	 	
 	 	 	 	This function is used along with these two other functions:
 	 	 	 		iAP2_Sessoions_bfnSendCtrlMessage(...)
 	 	 	 		iAP2_Sessions_bfnEndSendCtrlMsg()
 	 	 	 	
 	 	 	 	For more details see 'Mode B' in iAP2_Sessoions_bfnSendCtrlMessage(...) description. 
						
	@param		wParamID:		Control message parameter ID.
	@param		wSubparamId:	In case a group is send this value is the control message group type
								parameter ID; if no group is to be send set this value to 

the defined
								value 'IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM'.
	@param		baData:			uint8_t* - pointer to a uint8_t data type array or to MSB of a non-array data
								type, for this last, a cast to (uint8_t*) will be required.
	@param		wDataSize:		Size in bytes of the pointed, by param. 'baData', data.
				 
	@return		SESSIONS_OK:			Data was successfully added to the control session 
 	 	 	 							message to be send.
				SESSIONS_BUSY:			Unable to reserve required memory, try again 
										latter.
				SESSIONS_WRONG_INIT:	The function iAP2_Sessoions_bfnSendCtrlMessage(...)
										was not called before.				
				SESSIONS_MEM_FULL:		Fatal error the link layer is unable to reserve the
										required memory and will not be able to 

do it in the
										future. Unable to send the message with 

the current
										accumulated data size and memory 

configuration.
*/
uint8_t iAP2_Sessions_bfnAddCtrlMsgParam(uint16_t wParamID, uint16_t wSubparamId, uint8_t baData[], uint16_t wDataSize)
{
	uint8_t bFinalResult = SESSIONS_WRONG_INIT;
	
	/* Check for parameter formats:
	   -when baData == NULL then wDataSize must be equal to 0
	   or
	   -when wDataSize > 0 then baData must NOT be equal to 0 NULL
	*/
	if(((0 == wDataSize) && (NULL == baData)) || ((0 < wDataSize) && (NULL != baData)))
	{	
		if(CTRL_MSG_API_SET_PARAM == bSeqStats)
		{
			uint8_t baParamHeaderData[CTRL_MSG_PARAMHEADER_SIZE];/* Used parameter and subparameter */
			wCurrentParamId = wParamID; /* to be used in iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize() */
			
			if(IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM == wSubparamId)
			{
				/* If required previous group parameter size will be set */
				iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(_TRUE_);
				
				wPreviousParamId = wParamID;
				
				/* Normal parameter - Not a group type parameter */
			
				baParamHeaderData[CTRL_MSG_PRMHDR_LENGTHM_OFFSET] = (uint8_t)((wDataSize + CTRL_MSG_PARAMHEADER_SIZE)>> 8);
				baParamHeaderData[CTRL_MSG_PRMHDR_LENGTHL_OFFSET] = (uint8_t)(wDataSize + CTRL_MSG_PARAMHEADER_SIZE);
	
				baParamHeaderData[CTRL_MSG_PRMHDR_ID_MSB_OFFSET] =  (uint8_t)(wParamID >> 8);
				baParamHeaderData[CTRL_MSG_PRMHDR_ID_LSB_OFFSET] =  (uint8_t)(wParamID);
	
				/* Add parameter header */
				bFinalResult = iAP2_Sessions_bfnSetInBuffer(&baParamHeaderData[0], CTRL_MSG_PARAMHEADER_SIZE, NULL);
				
				if(SESSIONS_OK == bFinalResult)
				{
					/* Add parameter data */
					bFinalResult = iAP2_Sessions_bfnSetInBuffer(&baData[0], wDataSize, NULL);
				}
			}
			else
			{
                if ( (IAP_HID_COMPONENT_ID_SUBPARAM == wSubparamId) && (wCurrentParamId == IAP_HID_COMPONENT_PARAM) && (wPreviousParamId != wCurrentParamId) )
                {
                	wPrevCompID = baData[1];
                }
           	/* Group type parameter */
                if ( (IAP_HID_COMPONENT_ID_SUBPARAM == wSubparamId) && (wPreviousParamId == wCurrentParamId) &&  ( wPrevCompID != baData[1]))
                    /* Previous group parameter size will not be set */
                    iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(_FALSE_);
               	else
					/* Previous group parameter size will be set */
					iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(_TRUE_);
				
				/* Store current parameter ID for future detection on when to set group length */
				wPreviousParamId = wParamID;
				bFinalResult = SESSIONS_OK; 
				if(0 == wTotalSubparametersInGroup)
				{
					/* Add group type parameter header (same structure as normal parameter) */		

						
					bFinalResult = iAP2_Sessions_bfnSetCtrlMsgGrpParamHeader(wParamID);
					wParamGroupTotalBytes += CTRL_MSG_PARAMHEADER_SIZE;
				}
				
				if(SESSIONS_OK == bFinalResult)
				{
					/* Add subparameter data */
					
					baParamHeaderData[CTRL_MSG_SUBPRMHDR_LNGTHM_OFFST] = 
							(uint8_t)((wDataSize + CTRL_MSG_SUBPARAMHEADER_SIZE)>> 8);
					baParamHeaderData[CTRL_MSG_SUBPRMHDR_LNGTHL_OFFST] = (uint8_t)(wDataSize + CTRL_MSG_SUBPARAMHEADER_SIZE);
		
					baParamHeaderData[CTRL_MSG_SUBPRMHDR_ID_MSB_OFFST] = (uint8_t)(wSubparamId >> 8);
					baParamHeaderData[CTRL_MSG_SUBPRMHDR_ID_LSB_OFFST] = (uint8_t)(wSubparamId);
					
					
					/* Add subparameter header */
					bFinalResult = iAP2_Sessions_bfnSetInBuffer(&baParamHeaderData[0], CTRL_MSG_SUBPARAMHEADER_SIZE, 
																												NULL);
					
					if(SESSIONS_OK == bFinalResult)
					{
						wParamGroupTotalBytes += CTRL_MSG_SUBPARAMHEADER_SIZE;
						
						/* Add subparameter data */
						bFinalResult = iAP2_Sessions_bfnSetInBuffer(&baData[0], wDataSize, NULL);
						
						if(SESSIONS_OK == bFinalResult)
						{
							wParamGroupTotalBytes += wDataSize;
							wTotalSubparametersInGroup++;
						}
					}
				}
			}
		}
		else
		{
			bFinalResult = SESSIONS_WRONG_INIT;
		}
	}
	else
	{
		bFinalResult = SESSIONS_WRONG_INIT;
	}
	
	return bFinalResult;
}

/*!
	@fn 		uint8_t iAP2_Sessions_bfnEndSendCtrlMsg(void)
 	@brief		Part of the control message send sequence, used signal the end of the send sequence
 	 	 	 	and fire the control message send from the link layer.
 	 	 	 	
 	 	 	 	This function is used along with these two other functions:
 	 	 	 		iAP2_Sessoions_bfnSendCtrlMessage(...)
 	 	 	 		iAP2_Sessions_bfnAddCtrlMsgParam(...)
 	 	 	 	
 	 	 	 	For more details see 'Mode B' in iAP2_Sessoions_bfnSendCtrlMessage(...) description. 
						
	@param		void				 
	@return		SESSIONS_OK:			Data was successfully added to the control session 
 	 	 	 							message to be send.
				SESSIONS_BUSY:			Link layer send problem, try again latter.
				SESSIONS_WRONG_INIT:	The function iAP2_Sessoions_bfnSendCtrlMessage(...)
										was not called before.				
*/
uint8_t iAP2_Sessions_bfnEndSendCtrlMsg(void)
{
	uint8_t bFinalResult;
	
	if(CTRL_MSG_API_SET_PARAM == bSeqStats)
	{
		uint8_t bSendStatus = 0;
		uint16_t wLastMemReqId;
		uint16_t wMemReqId;
		uint16_t wLinkBufferUsed;
		
		if(0 != bMemRequestIdsCount)
		{
			wLastMemReqId = bMemRequestIdsCount - 1;
		}
		
		/* If required previous group parameter size will be set */
		iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(_FALSE_);
		
		/* Reset control variable for future detection on when to set group length */
		wPreviousParamId = 0;
		wCurrentParamId = 0;
		
		iAP2_Sessions_vfnSetCtrlMsgHeaderSize(wFrameTotalBytesIdx);
		
		for(wMemReqId = 0; wMemReqId < bMemRequestIdsCount; wMemReqId++)
		{
			wLinkBufferUsed = 0;
			/* Check if the last requested buffer space is not full. If buffer full use wLinkBufferUsed = 0.
			   This assumes all previous requested buffers are full. */
			if((wMemReqId == wLastMemReqId) && (0 != wLinkBufferFree))
			{
				wLinkBufferUsed = wLinkBufferSize - wLinkBufferFree;
			}
			
			bSendStatus |= iAP2_Link_bfnSend(IAP2_SESSIONS_CTRL_MSG_SESSION_ID, baMemRequestIds[wMemReqId], 
																									wLinkBufferUsed);
		}
		
		if(IAP2_LINK_STATUS_OK == bSendStatus)
		{
			bFinalResult = SESSIONS_OK;
			
			/* If all the message has been sent, then reset the pending mem IDs count */
			bMemRequestIdsCount = 0;
		}
		else
		{
			bFinalResult = SESSIONS_BUSY;
		}
	}
	else
	{
		bFinalResult = SESSIONS_WRONG_INIT;
	}
	
	bSeqStats = CTRL_MSG_API_INIT_IDLE;
	
	return bFinalResult;
}

/*!
	@fn 		uint8_t iAP2_Sessions_NewCtrlSessionFrame(uint16_t wReadIndex ,uint16_t wSize)
 	@brief		This function is called by the newFrame function of the sessions.c file This function
 	 	 	 	will be executed every time a new control session packet has arrived. This 
 	 	 	 	function parses the control sessions header and calls the General Features callback fn

	@param		uint16_t wReadIndex: 
	@param		uint16_t wSize:
				
 	@return		SESSIONS_READ_FRAME_CALLBACK_OK (0x00):	Callback Performed Correctly
 	 	 	 	SESSIONS_READ_FRAME_CALLBACK_ERROR (0x01): An error occurred during frame reading
*/
void iAP2_Sessions_Control_vfnRecvCallback(uint16_t wReadIndex, uint16_t wSize)
{
	uint8_t baDataBuff[CTRL_MSG_HEADER_SIZE];
	
	/* Reset current nest for the new packet so that the application can read previous headers */
	iAP2_Sess_Ctrl_bCurPktNest = 1;
	
	/* Determine if there is data that needs to be discarded before the next parameter header */
	if((iAP2_Sessions_gbCtrlMsgStatusFlags & (1 << IAP2_SESSIONS_CTRL_PEND_DATA_ADJ)) == 0)
	{
		/* No data to be discarded, check if this is a new message by checking if there is pending data for a message */
		if(0 == iAP2_Sess_Ctrl_waDataRem[IAP2_SESS_CTRL_NEST_MSG])
		{
			/* New message. Look for Start of Message. It is assumed that the message will always be at the beginning of
			 * the packet. Other mechanisms not supported at this point
			 */
			/** @todo Function Return */
			(void)iAP2_Link_bfnReadData(baDataBuff, &wReadIndex, CTRL_MSG_HEADER_SIZE);
			
			if((CTRL_MSG_START_OF_FRAME == baDataBuff[CTRL_MSG_HEADER_SOM_MSB]) && 
			   (CTRL_MSG_START_OF_FRAME == baDataBuff[CTRL_MSG_HEADER_SOM_LSB]))
			{
				/* Start of message detected. Read Message ID and size. Subtract header size from message size */
				iAP2_Sess_Ctrl_Headers[0].sElem.wID = ((uint16_t)baDataBuff[CTRL_MSG_HEADER_ID_MSB] << 8) |
													  	  baDataBuff[CTRL_MSG_HEADER_ID_LSB];
				iAP2_Sess_Ctrl_Headers[0].sElem.wSize = (((uint16_t)baDataBuff[CTRL_MSG_HEADER_SIZE_MSB] << 8) |
														baDataBuff[CTRL_MSG_HEADER_SIZE_LSB]) - CTRL_MSG_HEADER_SIZE;
				
				iAP2_Sess_Ctrl_waDataRem[IAP2_SESS_CTRL_NEST_MSG] = iAP2_Sess_Ctrl_Headers[0].sElem.wSize;
				iAP2_Sess_Ctrl_wPktSize = wSize - CTRL_MSG_HEADER_SIZE;
				iAP2_Sess_Ctrl_wPktIndex = wReadIndex;
				iAP2_Sess_Ctrl_bCurMsgNest = IAP2_SESS_CTRL_NEST_PARAM;
				IAP2_CTRL_SESSION_CALLBACK(iAP2_Sess_Ctrl_Headers[IAP2_SESS_CTRL_NEST_MSG].sElem.wID);
			}
		}
		else
		{
			/* Packet is part of a message that started in a previous packet, and no data needs to be discarded */
			iAP2_Sess_Ctrl_wPktSize = wSize;
			iAP2_Sess_Ctrl_wPktIndex = wReadIndex;
			IAP2_CTRL_SESSION_CALLBACK(iAP2_Sess_Ctrl_Headers[IAP2_SESS_CTRL_NEST_MSG].sElem.wID);
		}
	}
	else
	{
		/* Determine if there is more data in the packet than the one that needs to be discarded */
		if(iAP2_Sess_Ctrl_wAdjSize <= wSize)
		{
			/* Discard pending data */
			/** @todo Function Return */
			(void)iAP2_Link_bfnReadData(NULL, &wReadIndex, iAP2_Sess_Ctrl_wAdjSize);
			iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_wAdjNest] = 0;
			iAP2_Sessions_gbCtrlMsgStatusFlags &= ~(1 << IAP2_SESSIONS_CTRL_PEND_DATA_ADJ);
			
			if(iAP2_Sess_Ctrl_wAdjNest > IAP2_SESS_CTRL_NEST_MSG)
			{
				/* Not the complete message that was discarded, but determine if after the adjustment there is still
				 * a message to process.
				 */
				if(iAP2_Sess_Ctrl_waDataRem[IAP2_SESS_CTRL_NEST_MSG] != 0)
				{
					iAP2_Sess_Ctrl_wPktSize = wSize - iAP2_Sess_Ctrl_wAdjSize;
					iAP2_Sess_Ctrl_wPktIndex = wReadIndex;
					IAP2_CTRL_SESSION_CALLBACK(iAP2_Sess_Ctrl_Headers[IAP2_SESS_CTRL_NEST_MSG].sElem.wID);
				}
			}
		}
		else
		{
			iAP2_Sess_Ctrl_wAdjSize -= wSize;
		}
	}
}

/*!
	@fn 		uint8_t iAP2_Sessions_vfnGetCtrlMsgParamInfo(uint16_t* wParamID, uint16_t* wParamSize)
	@brief		This function parses the header for each parameter and sends back to 
 	 	 	 	 the features layer the required information.
	@param		uint16_t* wParamID: Pointer to where the parameter ID will be stored.
	@param		uint16_t* wParamSize: Pointer to where the parameter size will be stored.				
 	@return		IAP_SESSIONS_GET_CTRL_MSG_PARAM_INFO_OK (0x00):	Parameter info read correctly.
 	 	 	 	IAP_SESSIONS_GET_CTRL_MSG_PARAM_INFO_ERROR (0x01): The parameter info could not be 
 	 	 	 	read.
*/
uint8_t iAP2_Sessions_bfnGetCtrlMsgParamInfo(uint16_t* wParamID, uint16_t* wParamSize)
{
	static uint8_t iAP2_Sess_Ctrl_HeaderRem = 0;
	uint8_t bStatus = IAP2_SESS_CTRL_OK;
	
	if((iAP2_Sessions_gbCtrlMsgStatusFlags & (1 << IAP2_SESSIONS_CTRL_PEND_DATA_ADJ)) == 0)
	{
		if(iAP2_Sess_Ctrl_bCurPktNest == iAP2_Sess_Ctrl_bCurMsgNest)
		{
			if((iAP2_Sessions_gbCtrlMsgStatusFlags & (1 << IAP2_SESSIONS_CTRL_READ_IN_PROG)) == 0)
			{
				if(0 == iAP2_Sess_Ctrl_HeaderRem)
				{
					iAP2_Sess_Ctrl_HeaderRem = CTRL_MSG_PARAMHEADER_SIZE;
				}
				
				if(iAP2_Sess_Ctrl_HeaderRem > iAP2_Sess_Ctrl_wPktSize)
				{
					/* Header is split between this and the next packet. Read whatever is available and report to the 
					 * application 
					 */
					/** @todo Function Return */
					(void)iAP2_Link_bfnReadData(&iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].dwData, 
											&iAP2_Sess_Ctrl_wPktIndex, iAP2_Sess_Ctrl_wPktSize);
					
					iAP2_Sess_Ctrl_HeaderRem -= iAP2_Sess_Ctrl_wPktSize;
					
					bStatus = IAP2_SESS_CTRL_WAIT_NEXT_PACKET;
				}
				else
				{
					/* Read whatever is left to read from the header */
					/** @todo Function Return */
					(void)iAP2_Link_bfnReadData(&((uint8_t*)&iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].dwData)
													[CTRL_MSG_PARAMHEADER_SIZE - iAP2_Sess_Ctrl_HeaderRem], 
												&iAP2_Sess_Ctrl_wPktIndex, iAP2_Sess_Ctrl_HeaderRem);
					
					iAP2_Sess_Ctrl_wPktSize -= iAP2_Sess_Ctrl_HeaderRem;
					iAP2_Sess_Ctrl_HeaderRem = 0;
					
					#if (BIG_ENDIAN_CORE == _FALSE_)
					/* Adjust endianess if necessary */
						BYTESWAP16(iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wID,iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wID);
						BYTESWAP16(iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wSize, iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wSize);
					#endif
					
					/* Adjust size to discard the size of the header */
					iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wSize -= CTRL_MSG_PARAMHEADER_SIZE;
					iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest] = 
														iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wSize;
					
					*wParamSize = iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wSize;
					*wParamID = iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wID;
				}
			}
			else
			{
				*wParamID = iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurPktNest].sElem.wID;
				*wParamSize = iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurPktNest].sElem.wSize;
			}
		}
		else
		{
			*wParamID = iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurPktNest].sElem.wID;
			*wParamSize = iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurPktNest].sElem.wSize;
		}
	}
	else
	{
		bStatus = IAP2_SESS_CTRL_WAIT_NEXT_PACKET;
	}
	
	return bStatus; 
}

uint8_t iAP2_Sessions_bfnCtrlMsgGroupStart(void)
{
	uint8_t bStatus =  IAP2_SESS_CTRL_OK;
	
	if(iAP2_Sess_Ctrl_bCurPktNest == iAP2_Sess_Ctrl_bCurMsgNest)
	{
		if(iAP2_Sess_Ctrl_bCurMsgNest < (IAP2_SESS_CTRL_NEST_MAX - 1))
		{
			iAP2_Sess_Ctrl_bCurMsgNest++;
			iAP2_Sess_Ctrl_bCurPktNest++;
		}
		else
		{
			bStatus = IAP2_SESS_CTRL_TOO_MANY_GROUPS;
		}
	}
	else
	{
		iAP2_Sess_Ctrl_bCurPktNest++;
	}
	
	return bStatus;
}

/*!
	@fn 		uint8_t iAP2_Sessions_bfnReadCtrlMsgParamData(void* vpParamData, uint16_t wpDataSize)
	@brief		This function is called by the features layer to read the data of the just requested 
 	 	 	 	header information. This function will read parameters and sub-parameters. 
	@param		void* vpParamData: Pointer to where the data read must be stored.
	@param		uint16_t wpDataSize: Amount of bytes to be read.				
	@return		IAP_SESSIONS_ASK_NEXT_PARAM (0x00): Parameter read correctly ask for next parameter.	
 	 	 	 	IAP_SESSIONS_LAST_PARAM_WAIT_NEXT_FRAME (0x01): The parameter requested was not complete.
 	 	 	 	frame is over. wait for next frame and call iAP2_Sessions_vfnGetCtrlMsgParamInfo fn again.
 	 	 	 	IAP_SESSIONS_LAST_SUBPARAM (0x02): There are no more sub-parameters within current parameter.
 	 	 	 	IAP_SESSIONS_LAST_MSG_PARAMETER(0x03): There are no more parameters. Msg is complete.
*/
uint8_t iAP2_Sessions_bfnReadCtrlMsgParamData(void* vpParamData, uint16_t wReadSize)
{
	uint16_t wParamSize;
	uint16_t wParamRem;
	uint8_t bStatus;
	
	iAP2_Sessions_gbCtrlMsgStatusFlags |= (1 << IAP2_SESSIONS_CTRL_READ_IN_PROG);
	
	wParamRem = iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest];
	wParamSize = iAP2_Sess_Ctrl_Headers[iAP2_Sess_Ctrl_bCurMsgNest].sElem.wSize;
	
	bStatus = iAP2_Sessions_Ctrl_bfnReadData(&((uint8_t*)vpParamData)[wParamSize - wParamRem], 
												wParamRem - (wParamSize - wReadSize));
	
	if(IAP2_SESS_CTRL_DATA_PENDING != bStatus)
	{
		/* All data requested has been read, reset read in progress flag. */
		iAP2_Sessions_gbCtrlMsgStatusFlags &= ~(1 << IAP2_SESSIONS_CTRL_READ_IN_PROG);
		
		if(wReadSize < wParamSize)
		{
			/* Read whatever is left of the parameter to adjust buffers */
			bStatus = iAP2_Sessions_Ctrl_bfnReadData(NULL, iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest]);
			
			if(IAP2_SESS_CTRL_DATA_PENDING == bStatus)
			{
				/* Adjustment will be completed in subsequent packets. Set adjustment flag and determine if after the
				 * adjustment the group or message will be completed.
				 */
				iAP2_Sessions_gbCtrlMsgStatusFlags |= (1 << IAP2_SESSIONS_CTRL_PEND_DATA_ADJ);
				iAP2_Sess_Ctrl_wAdjSize = iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest];
				iAP2_Sess_Ctrl_wAdjNest = iAP2_Sess_Ctrl_bCurMsgNest;

				/* NOTE: The current nest remaining data will be left with whatever value it had. This allows for the
				 * condition in the following while to exit if the current nest variable was not decreased. Leaving the
				 * remaining data value different than 0 should not be an issue, since it will be reset once the
				 * adjustment is finished at the packet received callback. 
				 */
				bStatus = iAP2_Sessions_Ctrl_bfnNestAdjust();
			}
		}
	}
	
	return bStatus;
}

static uint8_t iAP2_Sessions_Ctrl_bfnReadData(uint8_t *bpBuff, uint16_t wSize)
{
	uint16_t wBytesToRead;
	uint8_t bStatus;
	
	if(iAP2_Sess_Ctrl_wPktSize > 0)
	{
		if(wSize <= iAP2_Sess_Ctrl_wPktSize)
		{
			/* All data will be read. Call nest adjust function to determine if a group or the message has ended only
			 * if after this read there will be no remaining data for the parameter
			 */
			wBytesToRead = wSize;
			
			if(iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest] == wSize)
			{
				bStatus = iAP2_Sessions_Ctrl_bfnNestAdjust();
			}
			else
			{
				iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest] -= wSize;
				bStatus = IAP2_SESS_CTRL_PARAM_COMPLETE;
			}
		}
		else
		{
			/* Read whatever is left in this packet and signal that there is still pending data */
			wBytesToRead = iAP2_Sess_Ctrl_wPktSize;
			iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest] -= iAP2_Sess_Ctrl_wPktSize;
			
			bStatus = IAP2_SESS_CTRL_DATA_PENDING;
		}
		/** @todo Function Return */
		(void)iAP2_Link_bfnReadData(bpBuff, &iAP2_Sess_Ctrl_wPktIndex, wBytesToRead);
		iAP2_Sess_Ctrl_wPktSize -= wBytesToRead;
	}
	else
    {
	    if((wSize == 0) && (iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest] == 0))
	    {
	        bStatus = iAP2_Sessions_Ctrl_bfnNestAdjust();
	    }
	    else
	    {
	        bStatus = IAP2_SESS_CTRL_DATA_PENDING;
	    }
    }
	
	return bStatus;
}

static uint8_t iAP2_Sessions_Ctrl_bfnNestAdjust(void)
{
	uint8_t bNestIndex;
	uint8_t bStatus;
	
	bNestIndex = iAP2_Sess_Ctrl_bCurMsgNest;
	do
	{
		/* Adjust for size of the header which is not considered in the current nest, but it is in the outer */
		bNestIndex--;
		iAP2_Sess_Ctrl_waDataRem[bNestIndex] -=	
										iAP2_Sess_Ctrl_Headers[bNestIndex + 1].sElem.wSize + CTRL_MSG_PARAMHEADER_SIZE;
	}while((iAP2_Sess_Ctrl_waDataRem[bNestIndex] == 0) && (bNestIndex > 0));
	
	/* If data remaining in the outer nest is 0, then at least one group was ended */
	if(iAP2_Sess_Ctrl_waDataRem[iAP2_Sess_Ctrl_bCurMsgNest - 1] == 0)
	{
		if(iAP2_Sess_Ctrl_waDataRem[0] != 0)	/* There is still more data in the message */
		{
			/* NOTE: There is no current way of indicating how many nested groups have been completed. This
			 * limits the implementation to only one group nested in the message, but not a group nested in
			 * another group.
			 */
			
			/* Adjust packet nest and message nest to the first nest that has remaining data */
			iAP2_Sess_Ctrl_bCurPktNest = bNestIndex + 1;
			iAP2_Sess_Ctrl_bCurMsgNest = bNestIndex + 1;
			
			bStatus = IAP2_SESS_CTRL_GROUP_COMPLETE;
		}
		else
		{
			bStatus = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
		}
	}
	else
	{
		/* Remaining data will be adjusted, but for the application, the parameter has been read successfully */
		bStatus = IAP2_SESS_CTRL_PARAM_COMPLETE;
	}
	
	return bStatus;
}

void iAP2_Sessions_vfnResetMessage(void)
{
	iAP2_Sessions_vfnFlushRequestedMem();
	
	iAP2_Sessions_vfnResetControls();
}

/*!
	@fn		void iAP2_Sessions_vfnCloseParamGroup()
	@brief	API to expose function of iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(..)
  			sending 'FALSE' as parameter to force a parameter close. Useful to mark the end in
  			group parameters where the same parameter ID is used and several groups of that
  			same ID are required in a session control packet.  
	@param	void
	@return void
 */
void iAP2_Sessions_vfnCloseParamGroup()
{
	iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(FALSE);
}

/*************************************************************************************************/
/*                               Static Functions Section                                        */
/*************************************************************************************************/

static void iAP2_Sessions_vfnResetControls()
{
	bpLinkBuffer = NULL;
	bpLinkBufferCtrlMsgStart = NULL;
	wLinkBufferSize = 0;
	wLinkBufferFree = 0;
	wFrameTotalBytesIdx = 0;
	wCurrentMemReqIdx = 0;
	wParamGroupTotalBytes = 0;
	wTotalSubparametersInGroup = 0;
	wCurrentParamId = 0;
	wPreviousParamId = 0;
	bSeqStats = CTRL_MSG_API_INIT_IDLE;

	bpaCurrentGrpParamSizeStorage[0] = NULL;
	bpaCurrentGrpParamSizeStorage[1] = NULL;
}

static uint8_t iAP2_Sessions_vfnSetCtrlMsgHeader(uint16_t wCtrlMsgID, uint16_t wSize)
{
	uint8_t baCtrlMsgHeaderData[CTRL_MSG_HEADER_SIZE];
	
	baCtrlMsgHeaderData[CTRL_MSG_SOM_MSB_OFFSET] = CTRL_MSG_SOM_MSB;
	baCtrlMsgHeaderData[CTRL_MSG_SOM_LSB_OFFSET] = CTRL_MSG_SOM_LSB;
	
	baCtrlMsgHeaderData[CTRL_MSG_LENGTHM_OFFSET] = (uint8_t)(wSize>>8);
	baCtrlMsgHeaderData[CTRL_MSG_LENGTHL_OFFSET] = (uint8_t)wSize;

	baCtrlMsgHeaderData[CTRL_MSG_ID_MSB_OFFSET] = (uint8_t)(wCtrlMsgID >> 8);
	baCtrlMsgHeaderData[CTRL_MSG_ID_LSB_OFFSET] = (uint8_t)wCtrlMsgID;
	
	return iAP2_Sessions_bfnSetInBuffer(&baCtrlMsgHeaderData[0], CTRL_MSG_HEADER_SIZE, NULL);
}

/*!
	@fn			static void iAP2_Sessions_vfnSetCtrlMsgHeaderSize(uint16_t wDataSize)
	@brief		Sets the total control message size into the message's header.
				Note:  Since the link layer provides as minimum memory size of 
				'CTRL_MSG_HEADER_SIZE', it is safe to assume the bytes to store
				the control message length (at positions 'CTRL_MSG_LENGTHM_OFFSET'
				and CTRL_MSG_LENGTHL_OFFSET from the start of the control message)
				are already allocated.
	@param		wDataSize:			Total control message size in bytes. 
	@return		void 
 */
static void iAP2_Sessions_vfnSetCtrlMsgHeaderSize(uint16_t wDataSize)
{
	if((NULL != bpLinkBufferCtrlMsgStart) && 
			(CTRL_MSG_UNKNOWN_SIZE == bpLinkBufferCtrlMsgStart[CTRL_MSG_LENGTHM_OFFSET]) &&
			(CTRL_MSG_UNKNOWN_SIZE == bpLinkBufferCtrlMsgStart[CTRL_MSG_LENGTHL_OFFSET]))
	{
		bpLinkBufferCtrlMsgStart[CTRL_MSG_LENGTHM_OFFSET] = (uint8_t)(wDataSize>>8);
		bpLinkBufferCtrlMsgStart[CTRL_MSG_LENGTHL_OFFSET] = (uint8_t)wDataSize;
		
		bpLinkBufferCtrlMsgStart = NULL;
	}
}

/*!
	@fn			static uint8_t iAP2_Sessions_bfnSetInBuffer(uint8_t baData[], uint16_t wDataSize, uint8_t* bpaDataStoredAt[])
	@brief		Function that copies the data into the Link Buffer
	@param		wDataSize:			Amount of bytes to be copied. 
	@param		baData:				Buffer where the data to be copied is located
	@param     	bpaDataStoredAt:	Buffer where the Link Buffer reference is going to be stored.  
	@return		SESSIONS_OK:		No error was found during execution
				SESSIONS_BUSY:			Unable to reserve required memory.			
				SESSIONS_MEM_FULL:		Fatal error the link layer is unable to reserve the
										required memory and will not be able to 

do it in the
										future.
 */
static uint8_t iAP2_Sessions_bfnSetInBuffer(uint8_t baData[], uint16_t wDataSize, uint8_t* bpaDataStoredAt[])
{
	uint8_t bFinalResult = SESSIONS_OK;
	uint16_t wCurrentPartialDataIdx = 0;
	
	while (wDataSize)
	{
		/* If there is no Link Buffer memory available, request memory */
		if (0 == wLinkBufferFree)
		{
			/* Request memory according to initially indicated max size, unless it has been exhausted */
			if(iAP2_Sess_Ctrl_wMsgMaxSize > wDataSize)
			{
				bFinalResult = iAP2_Sessions_bfnMemoryRequest(&bpLinkBuffer, iAP2_Sess_Ctrl_wMsgMaxSize);
				iAP2_Sess_Ctrl_wMsgMaxSize -= wLinkBufferFree;	/* Adjust with actually reserved size */
			}
			else
			{
				bFinalResult = iAP2_Sessions_bfnMemoryRequest(&bpLinkBuffer, wDataSize);
				iAP2_Sess_Ctrl_wMsgMaxSize = 0;
			}
		}
		
		/* If there is data to copy into link buffer and there has not been any error reported while
		   requesting memory, copy data to link buffer */
		if (SESSIONS_OK == bFinalResult)
		{	
			if (wDataSize <= wLinkBufferFree)
			{
				/* Data to copy fits into current link buffer */
				
				iAP2_Sessions_vfnWriteDataToLinkBuffer(wDataSize, &baData[0], bpaDataStoredAt, &wCurrentPartialDataIdx);
				wFrameTotalBytesIdx += wDataSize;
				wLinkBufferFree -= wDataSize;
				wDataSize = 0;
			}
			else
			{
				/* Data to copy is more than current link buffer, copy only amount of data that
				   fits into link buffer */
				
				iAP2_Sessions_vfnWriteDataToLinkBuffer(wLinkBufferFree, &baData[0], bpaDataStoredAt, 
																							&wCurrentPartialDataIdx);
				wFrameTotalBytesIdx += wLinkBufferFree;
				wDataSize -= wLinkBufferFree;
				wLinkBufferFree = 0;
			}
		}
		else
		{
			/* There was an error at memory request, set break condition for the loop */
			wDataSize = 0;
		}
	}

	return bFinalResult;	
}

/*!
	@fn			static uint8_t iAP2_Sessions_bfnMemoryRequest(uint8_t **bppIndex, uint16_t wDataSize)
	@brief		Requests memory space to the link layer and sets required module's control 
				variables.
	@param		bppIndex:			Link layer will store the pointer to the reserved memory. 
	@param		wDataSize:			Amount memory bytes to request the link layer. 

	@return		SESSIONS_OK:		Memory was reserved, the amount may be equal or less
									than requested.
				SESSIONS_BUSY:		Unable to reserve memory.			
				SESSIONS_MEM_FULL:	Fatal error the link layer is unable to reserve the
									requested memory and will not be able to do it in 

the
									future.
 */
static uint8_t iAP2_Sessions_bfnMemoryRequest(uint8_t **bppIndex, uint16_t wDataSize)
{
	uint8_t bFinalResult;
	wLinkBufferFree = wDataSize;//@todo JS:  could cause an error if memory request is !IAP2_LINK_STATUS_OK
	
	bFinalResult = iAP2_Link_bfnMemoryRequest(bppIndex, &wLinkBufferFree, &baMemRequestIds[bMemRequestIdsCount]);
	if(IAP2_LINK_STATUS_OK == bFinalResult)
	{
		wLinkBufferSize = wLinkBufferFree;
		wCurrentMemReqIdx = 0;
		bMemRequestIdsCount++; 
		bFinalResult = SESSIONS_OK;	
	}
	else if(IAP2_LINK_ERROR_MEM_BUSY == bFinalResult)
	{
		bFinalResult = SESSIONS_BUSY;	
	}
	else if(IAP2_LINK_ERROR_NO_MEM == bFinalResult)
	{
		iAP2_Sessions_vfnFlushRequestedMem();
		bFinalResult = SESSIONS_MEM_FULL;
	}
	else
	{
		/* Do nothing / Case not expected */
	}
	
	return bFinalResult;
}

/*!
	@fn			static void iAP2_Sessions_vfnFlushRequestedMem(void)
	@brief		Will signal the link layer to free all the previously reserved
				memory.
	@param		void
	@return		void
 */
static void iAP2_Sessions_vfnFlushRequestedMem(void)
{
	uint16_t wCounter;
	
	wCounter = bMemRequestIdsCount;
	//for(wCounter = (bMemRequestIdsCount - 1); wCounter < bMemRequestIdsCount; wCounter++)
	while(wCounter > 0)
	{
		wCounter--;
		iAP2_Link_vfnMemoryFlush(baMemRequestIds[wCounter]);
	}
	
	/* Reset index for memory request Ids array */
	bMemRequestIdsCount = 0;
}

/*!
	@fn			static uint8_t iAP2_Sessions_bfnSetCtrlMsgGrpParamHeader(uint16_t wParamID)
	@brief		Sets the header of a group type parameter and saves the location of
				where to store the MSB and LSB of the group type parameter's size to
				be able to use it in the future.
	@param		wParamID:	Group type parameter ID.
	@return		SESSIONS_OK:		No error was found during execution
				SESSIONS_BUSY:		Unable to reserve required memory.			
				SESSIONS_MEM_FULL:	Fatal error the link layer is unable to reserve the
									required memory and will not be able to do it in 

the
									future.
 */
static uint8_t iAP2_Sessions_bfnSetCtrlMsgGrpParamHeader(uint16_t wParamID)
{
	uint8_t baCtrlMsgHeaderData[CTRL_MSG_PARAMHEADER_SIZE];
	uint8_t bStatus;
	
	baCtrlMsgHeaderData[CTRL_MSG_PRMHDR_LENGTHM_OFFSET] = 0; /* Set to zero for now */
	baCtrlMsgHeaderData[CTRL_MSG_PRMHDR_LENGTHL_OFFSET] = 0; /* Set to zero for now */

	baCtrlMsgHeaderData[CTRL_MSG_PRMHDR_ID_MSB_OFFSET] = (uint8_t)(wParamID >> 8);
	baCtrlMsgHeaderData[CTRL_MSG_PRMHDR_ID_LSB_OFFSET] = (uint8_t)wParamID;
	
	/* Set position where the group parameter size will be located (MSB and LSM).
	   Initial value (data index) will be cast to pointer, after it is processed by 
	   the function the function will store a pointer to the related data */
	bpaCurrentGrpParamSizeStorage[0] = (uint8_t*)CTRL_MSG_PRMHDR_LENGTHM_OFFSET;
	bpaCurrentGrpParamSizeStorage[1] = (uint8_t*)CTRL_MSG_PRMHDR_LENGTHL_OFFSET;
	
	bStatus = iAP2_Sessions_bfnSetInBuffer(&baCtrlMsgHeaderData[0], CTRL_MSG_PARAMHEADER_SIZE, 
											&bpaCurrentGrpParamSizeStorage[0]); 
	
	return bStatus;

}

/*!
	@fn			static inline void iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(uint8_t bCheckPrevParamId)
	@brief		Check for previous stored group type parameters, if detected set corresponding length
	@param		bCheckPrevParamId:	indicates if setting the parameter size in the header dependes on a 
									parameter ID change from a previous set parameter ID
	@return		void
*/
#if defined __CC_ARM
static __inline void iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(uint8_t bCheckPrevParamId)
#else
static inline void iAP2_Sessions_vfnSetCtrlMsgPreviousGrpParamSize(uint8_t bCheckPrevParamId)
#endif
{
	if(_TRUE_ == bCheckPrevParamId)
	{
		bCheckPrevParamId = (wPreviousParamId != wCurrentParamId);
	}
	else
	{
		bCheckPrevParamId = _TRUE_;
	}
	
	/* Verify if set is required / when bCheckPrevParamId == TRUE, the if() condition will
	   evaluate the expression  equivalent to:
	   ((0 != wTotalSubparametersInGroup) && (wPreviousParamId != wCurrentParamId))
	*/
	if((0 != wTotalSubparametersInGroup) && bCheckPrevParamId)
	{
		if((NULL != bpaCurrentGrpParamSizeStorage[0]) && (NULL != bpaCurrentGrpParamSizeStorage[1]))
		{
			(*bpaCurrentGrpParamSizeStorage[0]) = (uint8_t)(wParamGroupTotalBytes >> 8);
			(*bpaCurrentGrpParamSizeStorage[1]) = (uint8_t)(wParamGroupTotalBytes);
		}
		
		bpaCurrentGrpParamSizeStorage[0] = NULL;
		bpaCurrentGrpParamSizeStorage[1] = NULL;
		
		wParamGroupTotalBytes = 0;
		wTotalSubparametersInGroup = 0;
	}
}


/*!
 	 @fn 		static void iAP2_Sessions_vfnWriteDataToLinkBuffer(uint16_t wDataSize, uint8_t baData[],
 	 	 	 	 	 	 	 	 	 	 	 uint8_t* bpaDataStoredAt[], uint16_t *wpCurrentPartialDataIdx)
 	 @brief		Copies a number of bytes into the link buffer
 	 @param		wDataSize: 					Amount of bytes to be copied. 
	 @param		baData: 					Buffer where the data to be copied is located.
	 @param     bpaDataStoredAt:			pointer where the link buffer reference is going to
											be stored.
	 @param		wpCurrentPartialDataIdx:	Pointer to current data array index to be copied to
	 	 	 	 	 	 	 	 	 	 	link buffer.
 	 @return	void
*/
static void iAP2_Sessions_vfnWriteDataToLinkBuffer(uint16_t wDataSize, uint8_t* baData, uint8_t* bpaDataStoredAt[], 
													uint16_t *wpCurrentPartialDataIdx)
{
	uint16_t wIdx;
	
	for(wIdx = 0; wIdx < wDataSize; wIdx++)
	{
		bpLinkBuffer[wCurrentMemReqIdx] = baData[*wpCurrentPartialDataIdx];
		/* If requested in params save memory address of matching index */
		if(NULL != bpaDataStoredAt)
		{
			if((*wpCurrentPartialDataIdx) == (uint32_t)bpaDataStoredAt[0])/* Warning expected */
			{
				bpaDataStoredAt[0] = &bpLinkBuffer[wCurrentMemReqIdx];
			}
			else if((*wpCurrentPartialDataIdx) == (uint32_t)bpaDataStoredAt[1])/* Warning expected */
			{
				bpaDataStoredAt[1] = &bpLinkBuffer[wCurrentMemReqIdx];				
			}
		}
		/* Advance index one per value set */
		wCurrentMemReqIdx++;
		(*wpCurrentPartialDataIdx)++;
	}
}
