/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_SESSIONS_EXTERNAL_ACC_H_
#define IAP2_SESSIONS_EXTERNAL_ACC_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP2_SESSIONS_EA_RECV_CALLBACK			iAP2_Feat_EA_vfnDataCallback

#define IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE                 (2)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef enum
{
	IAP2_SESSIONS_EA_OK = 0,
	IAP2_SESSIONS_EA_SEND_COMPLETE,
	IAP2_SESSIONS_EA_SEND_PENDING,
	IAP2_SESSIONS_EA_SEND_RETRY,
	IAP2_SESSIONS_EA_SEND_NO_MEM,
	IAP2_SESSIONS_EA_DATA_RECV
}IAP2_SESSIONS_EA_STATUS_t;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/

void iAP2_Sessions_bfnNewExt_AccSessionFrame(uint16_t wReadIndex ,uint16_t wSize);
uint8_t iAP2_Session_EA_SendData(uint8_t *pbDataBuffer, uint16_t wEATranID, uint16_t wDataSize);
uint8_t iAP2_Session_EA_ReadData(uint8_t *pbDataBuffer, uint16_t wDataSize, uint16_t wReadIndex);

/*************************************************************************************************/

#endif /* IAP2_SESSIONS_EXTERNAL_ACC_H_ */
