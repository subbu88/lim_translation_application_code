/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/

#include "ProjectTypes.h"
#include "iAP_Transport.h"
#include "iAP2_Nat_Ext_Acc_Feat.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
//#define MAX_NEA_PACKET_SIZE	2048 * 8//(8192)
#define MAX_NEA_PACKET_SIZE	TRANSDUCER_BUF_SIZE

//#define NEA_BUFFERS_SIZES	2048 * 8//(8192)
#define NEA_BUFFERS_SIZES	TRANSDUCER_BUF_SIZE
/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
void iAP2_Feat_NEA_vfnProcessReceivedData(uint8_t *pbData, uint16_t wDataSize);
void iAP2_Feat_NEA_vfnSendPendingData(void);
/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

static uint8_t  iAP2_NEA_bPendingDataFlag = _FALSE_;
static uint16_t iAP2_NEA_wPendingDataSize = 0;
static uint8_t *iAP2_NEA_pbDataBuffer;

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

void iAP2_Nat_Ex_Acc_Init(void)
{
	/* register callbacks */
	iAP_Transport_SetNEASentCallback(iAP2_Feat_NEA_vfnSendPendingData);
	iAP_Transport_SetNEARecvCallback(iAP2_Feat_NEA_vfnProcessReceivedData);
}

/** ***********************************************************************************************
*
* @brief    Function that prepares Native External Accessory Data to send
*
* @param   	*pData			        Pointer to the data to send.
* 			wDataSize               Size of the data to send
*
* @return  	IAP2_FEAT_NEA_ERROR_OK                    Data was copied to the TX circular buffer correctly.
*           IAP2_FEAT_NEA_ERROR_NO_BYTES_AVAILABLE    There is not enough space available in the TX buffer to copy the data.
*
**************************************************************************************************/
uint8_t iAP2_Feat_NEA_bfnSendData(uint8_t *pBuff, uint16_t wSize)
{
	uint8_t bStatus;

	/* if the data to send is larger than the max */
	/* keep track of the pointers and data size */
	if(MAX_NEA_PACKET_SIZE < wSize)
	{

		/* backup pointers and data size to keep sending */
		iAP2_NEA_bPendingDataFlag = _TRUE_;
		iAP2_NEA_wPendingDataSize = wSize - MAX_NEA_PACKET_SIZE;
		iAP2_NEA_pbDataBuffer = &pBuff[MAX_NEA_PACKET_SIZE];
		wSize = MAX_NEA_PACKET_SIZE;

	}
	else
	{
		iAP2_NEA_bPendingDataFlag = _FALSE_;
	}

	/* send thru transport layer */
	/* the return status depends on the transport */
	bStatus = iAP_Transport_NEASendData(pBuff,wSize);

	if(bStatus != IAP2_FEAT_NEA_ERROR_OK)
	{
		bStatus = IAP2_FEAT_NEA_ERROR_NO_BYTES_AVAILABLE;

	}

	return(bStatus);

}


void iAP2_Feat_NEA_vfnSendPendingData(void)
{
	if(_TRUE_ == iAP2_NEA_bPendingDataFlag)
	{
		iAP2_Feat_NEA_bfnSendData(iAP2_NEA_pbDataBuffer,iAP2_NEA_wPendingDataSize);
	}
	else
	{
		IAP2_FEAT_NEA_CALLBACK(IAP2_FEAT_NEA_PROT_SEND_COMPLETE,0);
	}
}

/** ***********************************************************************************************
*
* @brief    Function that prepares the Native External Accessory Read
*
* @param   	*pApplicationBuffer	Pointer to the buffer where the data will be read.
*
* @return   void
*
**************************************************************************************************/
void iAP2_Feat_NEA_bfnRead(uint8_t * pApplicationBuffer)
{
	//printf("\n iAP2_Feat_NEA_bfnRead %s\n", iAP_Transport_NEARcvData);	
	iAP_Transport_NEARcvData(pApplicationBuffer);
}

void iAP2_Feat_NEA_vfnProcessReceivedData(uint8_t *pbData, uint16_t wDataSize)
{
	IAP2_FEAT_NEA_CALLBACK(IAP2_FEAT_NEA_PROT_DATA_RECV,wDataSize);
}
