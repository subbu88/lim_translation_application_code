/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "MFi_Config.h"
#include "iAP2_Link.h"
#include "iAP2_Authentication_Feat.h"
#include "iAP2_Identification_Feat.h"
#include "iAP2_Host.h"
#if FSL_OS_SELECTED == FSL_OS_NONE
#include "iAP_Authentication.h"
#else
#include "iAP_Blocking_Authentication.h"
#endif
#include "iAP_Timer.h"
#include "iAP_Transport.h"
#if (IAP2_IDENT_PWR_SOURCE_TYPE_DATA != IAP2_PWR_TYPE_NONE)
#include "iAP2_Power_Feat.h"
#endif
#if IAP2_CTRL_MSG_APP_LAUNCH != 0
#include "iAP2_App_Launch_Feat.h"
#endif
#ifdef MFI_IAP1
#include "iAP_Host.h"
#endif
#include "assert.h"
///////////////////////////////////////////////////////////////////////////////////////////////////
//                                   Defines & Macros Section                   
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>

/* Inital link synchronization parameters. To be modified by PEx */
#define IAP2_HOST_MAX_NUM_PKTS          8       /** @todo iAP_Host - EM: This value could be referenced to the link macro so that PEx only modifies it once */
#define IAP2_HOST_MAX_PKT_SIZE          (8192)//512
#define IAP2_HOST_RET_TIMEOUT           1000
#define IAP2_HOST_CUM_ACK_TIMEOUT       500
#define IAP2_HOST_MAX_RET               30
#define IAP2_HOST_MAX_CUM_ACK           8
#define IAP2_HOST_LINK_VERSION          1


#if (IAP2_SESSIONS_FILE_TRANS_ENABLE == _TRUE_) && (IAP2_SESSIONS_EXT_ACC_ENABLE == _TRUE_)
#define IAP2_HOST_NUM_OF_SESSIONS       3

#define IAP2_HOST_SESS_1_ID             1       /** @todo iAP_Host - EM: This value could be referenced to the sessions macros so that PEx only modifies it once */
#define IAP2_HOST_SESS_1_TYPE           0
#define IAP2_HOST_SESS_1_VER            1

#if IAP2_HOST_NUM_OF_SESSIONS > 1
#define IAP2_HOST_SESS_2_ID             2       /** @todo iAP_Host - EM: This value could be referenced to the sessions macros so that PEx only modifies it once */
#define IAP2_HOST_SESS_2_TYPE           1
#define IAP2_HOST_SESS_2_VER            1
#endif

#if IAP2_HOST_NUM_OF_SESSIONS > 2
#define IAP2_HOST_SESS_3_ID             3       /** @todo iAP_Host - EM: This value could be referenced to the sessions macros so that PEx only modifies it once */
#define IAP2_HOST_SESS_3_TYPE           2
#define IAP2_HOST_SESS_3_VER            1
#endif

#endif

#if (IAP2_SESSIONS_FILE_TRANS_ENABLE == _TRUE_) && (IAP2_SESSIONS_EXT_ACC_ENABLE == _FALSE_)
#define IAP2_HOST_NUM_OF_SESSIONS       2

#define IAP2_HOST_SESS_1_ID             1       /** @todo iAP_Host - EM: This value could be referenced to the sessions macros so that PEx only modifies it once */
#define IAP2_HOST_SESS_1_TYPE           0
#define IAP2_HOST_SESS_1_VER            1

#if IAP2_HOST_NUM_OF_SESSIONS > 1
#define IAP2_HOST_SESS_2_ID             2       /** @todo iAP_Host - EM: This value could be referenced to the sessions macros so that PEx only modifies it once */
#define IAP2_HOST_SESS_2_TYPE           1
#define IAP2_HOST_SESS_2_VER            1
#endif

#endif

#if (IAP2_SESSIONS_FILE_TRANS_ENABLE == _FALSE_) && (IAP2_SESSIONS_EXT_ACC_ENABLE == _TRUE_)
#define IAP2_HOST_NUM_OF_SESSIONS       2

#define IAP2_HOST_SESS_1_ID             1       /** @todo iAP_Host - EM: This value could be referenced to the sessions macros so that PEx only modifies it once */
#define IAP2_HOST_SESS_1_TYPE           0
#define IAP2_HOST_SESS_1_VER            1

#if IAP2_HOST_NUM_OF_SESSIONS > 1
#define IAP2_HOST_SESS_2_ID             3       /** @todo iAP_Host - EM: This value could be referenced to the sessions macros so that PEx only modifies it once */
#define IAP2_HOST_SESS_2_TYPE           2
#define IAP2_HOST_SESS_2_VER            1
#endif

#endif

#define IAP2_HOST_AUTH_TIME_OUT    		(5000)   //Time out given in ms

#define IAP2_HOST_AUTH_WAIT_FOREVER 0
#define IAP2_HOST_TIME_OUT  		2000
#define IAP2H_NO_RETRIES			0

#define IAP2H_IAP1_INIT_SEQ_SIZE        5

#define IAP2_HOST_STATEMACHINE_TASK_PRIORITY        8

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                       Typedef Section                        
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>

//! <brief description>
//!< <brief description of each element in a enum/structure>


///////////////////////////////////////////////////////////////////////////////////////////////////
//                                  Function Prototypes Section                 
///////////////////////////////////////////////////////////////////////////////////////////////////
/*!
 * @brief brief function description
 *
 * @param param1 param1 brief description
 * @param param2 param2 brief description
 * @return brief description of return value
 * @retval retval1 retval1 brief description
 * 
 * 
 */
static void iAP2_Host_vfnReadACCCertificate_State(void);
static void iAP2_Host_vfnSendACCCertificate_State(void);
static void iAP2_Host_vfnCreateChallengeResponse_State(void);
static void iAP2_Host_vfnSendChallengeResponse_State(void);
static void iAP2_Host_vfnWaitCoprocessor_State(void);
static void iAP2_Host_vfnAccAuthenticationError_State(void);
static void iAP2_Host_vfnWaitAuthenticationStatus_State(void);
static void iAP2_Host_vfnDeviceAttachDetect_State(void);
static void iAP2_Host_vfnProtocolInit_State(void);
static void iAP2_Host_vfnLinkSync_State(void);
static void iAP2_Host_vfnReadChallengeData(void);
static void iAP2_Host_vfnIdentificationState(void);
static void iAP2_Host_vfnIdleState(void);
static void iAP2_Host_vfnUARTInitState(void);
static void iAP2_Host_vfnWaitLinkInitState(void);
static void iAP2_Host_vfnRoleSwitch(void);
static void iAP2_Host_vfnRoleSwitchWait(void);
#ifndef MFI_IAP1
static void iAP2_Host_vfniAP1State(void);
#endif
#if IAP2_IDENT_PWR_SOURCE_TYPE_DATA != 0
static void iAP2_Host_vfnPowerUpdateState(void);
#endif
#if IAP2_CTRL_MSG_APP_LAUNCH
static void iAP2_Host_vfnAppLaunchState(void);
#endif
static void iAP2_Host_TransportRxCbk(uint8_t *pbData, uint16_t wDataSize);

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/
typedef struct
{
	uint8_t PrevState;				/**< used to move the machine regardless the iOS communication */
	uint8_t ActualState;				/**< used to move the machine regardless the iOS communication */
	uint8_t NextState;				/**< used to move the machine regardless the iOS communication */
	uint8_t ResumeState;				/**< used to move the machine regardless the iOS communication */
	uint8_t TimeoutState;			/**< used to move the machine when no answer is received */
}sSM_Host2Type;

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
static void (*const iAP2_Host_vfnapHostDriver[])(void) = 
{
    iAP2_Host_vfnDeviceAttachDetect_State,
#ifdef IAPI_INTERFACE_UART
    iAP2_Host_vfnUARTInitState,
#endif
    iAP2_Host_vfnWaitLinkInitState,
#ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
    iAP2_Host_vfnRoleSwitch,
    iAP2_Host_vfnRoleSwitchWait,
#endif
    iAP2_Host_vfnProtocolInit_State,
    iAP2_Host_vfnLinkSync_State,
    iAP2_Host_vfnReadACCCertificate_State,
    iAP2_Host_vfnSendACCCertificate_State,
    iAP2_Host_vfnCreateChallengeResponse_State,
    iAP2_Host_vfnSendChallengeResponse_State,
    iAP2_Host_vfnAccAuthenticationError_State,
    iAP2_Host_vfnWaitAuthenticationStatus_State,
    iAP2_Host_vfnIdentificationState,
    iAP2_Host_vfnIdleState,
#ifdef MFI_IAP1
    iap_host_state_machine_driver        /* iAP1 State Machine */
#else
    iAP2_Host_vfniAP1State
#endif
#if IAP2_IDENT_PWR_SOURCE_TYPE_DATA != 0
    ,iAP2_Host_vfnPowerUpdateState
#endif
#if IAP2_CTRL_MSG_APP_LAUNCH
	,iAP2_Host_vfnAppLaunchState
#endif
};

static uint8_t iAP2_Host_cbaInitSeq[] = {0xFF,0x55,0x02,0x00,0xEE,0x10};

static const IAP2_LINK_SYNCHRONIZATION_PAYLOAD iAP2_Host_csInitLSP =
{
    NULL,
    IAP2_HOST_LINK_VERSION,
    IAP2_HOST_MAX_NUM_PKTS,
    IAP2_HOST_MAX_PKT_SIZE,
    IAP2_HOST_RET_TIMEOUT,
    IAP2_HOST_CUM_ACK_TIMEOUT,
    IAP2_HOST_MAX_RET,
    IAP2_HOST_MAX_CUM_ACK,
    IAP2_HOST_NUM_OF_SESSIONS
};

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                   Global Variables Section                   
///////////////////////////////////////////////////////////////////////////////////////////////////
//! <brief description>

uint16_t iAP2_Host_gwStatus;                 /**< Host Status variable */
uint16_t iAP2_Host_gwInternalErrorFlags;     /**< Host Internal Error Flags */
sSM iAP2_Host_SM;             /**< Structure to move the Host state machine */

static int g_authenticationEventsFlags;
static auth_config_parameters auth_config;

/* Global that indicates outside the stack that initialization has failed.
 * This can be polled by an application. */
bool iAP_init_failed;
int32_t g_app_launch = 0;
MFIHostConfig g_hostConfig;

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                   Static Variables Section                   
///////////////////////////////////////////////////////////////////////////////////////////////////
//! <brief description>

static uint16_t gs_iAP2HostCertificateLength;
static uint16_t gs_iAP2HostChallengeSize;
static uint16_t gs_iAP2HostChallengeParameterID;
static uint16_t gs_iAP2HostChallengeResponseSize;
static uint8_t gs_iAP2HostCoprocessorChallengeData[21];      /**< Buffer used to store the challenge data data from Apple Device */
static uint8_t iAP2_Host_gbAccAuthRetriesCounter;
static uint8_t gs_iAP2HostInitCount;
static uint8_t gs_iAP2HostReceiveCount;

static uint16_t gs_messagesByAccessory[30];
static uint16_t gs_messagesFromDevice[30];

static OsaMutex gs_iAP2HostMutex;
static OsaCondition gs_iAP2HostEvents;
static OsaMutex gs_iAP2CoprocessorMutex;
static OsaCondition gs_iAP2CoprocessorEvents;

static iAP2_Identification_RejectedInformation gs_rejectedInformation =
{
        0,  /**< used to identify rejected parameters */
        gs_messagesFromDevice,
        gs_messagesByAccessory,
        30,
        30
};

IAP2_LINK_CONFIG_SESSION iAP2_Host_sSessions[IAP2_HOST_NUM_OF_SESSIONS] =
{
    {
        IAP2_HOST_SESS_1_ID,
        IAP2_HOST_SESS_1_TYPE,
        IAP2_HOST_SESS_1_VER
    }
    
#if IAP2_HOST_NUM_OF_SESSIONS > 1
    ,{
        IAP2_HOST_SESS_2_ID,
        IAP2_HOST_SESS_2_TYPE,
        IAP2_HOST_SESS_2_VER
    }
#endif
    
#if IAP2_HOST_NUM_OF_SESSIONS > 2
    ,{
        IAP2_HOST_SESS_3_ID,
        IAP2_HOST_SESS_3_TYPE,
        IAP2_HOST_SESS_3_VER
    }
#endif
};

IAP2_LINK_SYNCHRONIZATION_PAYLOAD iAP2_Host_sLSP =
{
    iAP2_Host_sSessions,
    IAP2_HOST_LINK_VERSION,
    IAP2_HOST_MAX_NUM_PKTS,
    IAP2_HOST_MAX_PKT_SIZE,
    IAP2_HOST_RET_TIMEOUT,
    IAP2_HOST_CUM_ACK_TIMEOUT,
    IAP2_HOST_MAX_RET,
    IAP2_HOST_MAX_CUM_ACK,
    IAP2_HOST_NUM_OF_SESSIONS
};

static OsaThread iAP2_Host_stateMachine_TaskId; 
static bool iAP2_Host_stateMachine_Exit; 

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
static void iAP2_Host_vfnReadACCCertificate_State(void)
{ 
    uint8_t bUSBStatus;
    uint8_t eventStatus = ERRCODE_NO_ERROR;

    dprint("iAP2_Host_vfnReadACCCertificate_State start\n");
//printf("iAP2_Host_vfnReadACCCertificate_State start\n");

    if((iAP2_Host_gwStatus & (1 << IAP2H_ACC_CERTIFICATE_SENT)) == 0)
    {
        dprint("    !IAP2H_ACC_CERTIFICATE_SENT\n");
        /* Wait until auth request arrives */
        osa_mutex_lock(&gs_iAP2HostMutex);
        while ((g_authenticationEventsFlags &
                   (1 << IAP2H_ACC_AUTHENTICATION_REQUIRED)) == 0) {
            eventStatus = osa_cond_wait(&gs_iAP2HostEvents, &gs_iAP2HostMutex,
                                        IAP2_HOST_AUTH_TIME_OUT);
            if (eventStatus == ERRCODE_TIMED_OUT)
                break;
        }
        osa_mutex_unlock(&gs_iAP2HostMutex);
        //printf("\n eventStatus %d", eventStatus);
        if((ERRCODE_NO_ERROR == eventStatus) && 
           (g_authenticationEventsFlags & (1 << IAP2H_ACC_AUTHENTICATION_REQUIRED)))
        {
            g_authenticationEventsFlags &= ~(1 << IAP2H_ACC_AUTHENTICATION_REQUIRED);

            iAP2_Host_gwStatus |= (1 << IAP2H_ACC_CERTIFICATE_SENT);
            /* Ask a certificate to CP */
            IAP2_HOST_READ_ACCESSORY_CERTIFICATE(&iAP_Auth_CPCertificate[0], 
                    &gs_iAP2HostCertificateLength);
            /* Set variable to no retries in case the error state enters */
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;
        }    
        else if (ERRCODE_TIMED_OUT == eventStatus) 
        {
            g_authenticationEventsFlags = 0;
            dprint("iAP2_Host_vfnReadACCCertificate_State cert sent timeout\n");
                iAP_Auth_vfnCancel();
                iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
                iAP2_Host_gwStatus = 0;
        }
    }
    else
    {
        dprint("    IAP2H_ACC_CERTIFICATE_SENT\n");
        osa_mutex_lock(&gs_iAP2CoprocessorMutex);
        while (g_authenticationEventsFlags == 0) {
            eventStatus = osa_cond_wait(&gs_iAP2CoprocessorEvents,
                                          &gs_iAP2CoprocessorMutex,
                                          IAP2_HOST_AUTH_TIME_OUT);
            if (eventStatus == ERRCODE_TIMED_OUT)
                break;
        }
        osa_mutex_unlock(&gs_iAP2CoprocessorMutex);
        
        if (ERRCODE_NO_ERROR == eventStatus)
        {

            if(g_authenticationEventsFlags & (1 << KAuthenticationCommandComplete) )
            {

                iAP2_Host_SM.ActualState = IAP2_SEND_ACC_CERTIFICATE_STATE;

                /* Restart Accessory Authentication Retries Counter */
                iAP2_Host_gbAccAuthRetriesCounter = 0;

            }
            else if( (g_authenticationEventsFlags & (1<kAuthenticationInvalidRegisterForRead)) || 
                     (g_authenticationEventsFlags & (1<kAuthenticationInvalidRegisterForWrite))    || 
                     (g_authenticationEventsFlags & (1<kAuthenticationInvalidSignatureLength))    || 
                     (g_authenticationEventsFlags & (1<kAuthenticationInvalidChallengeLength))    || 
                     (g_authenticationEventsFlags & (1<kAuthenticationInvalidCertificateLength)) )
            {
                dprint("IAP2H_ACC_AUTHENTICATION_ERROR\n");

                /* Set error flags */
                iAP2_Host_gwInternalErrorFlags |= (1 << IAP2H_ACC_AUTHENTICATION_ERROR);
                iAP2_Host_gwStatus |= (1 << IAP2H_INTERNAL_ERROR);
                iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
            }
            else
            {
                iAP2_Host_gwStatus &= ~(1 << IAP2H_ACC_CERTIFICATE_SENT);
                iAP2_Host_SM.ActualState = IAP2_READ_ACC_CERTIFICATE_STATE;
            }

            g_authenticationEventsFlags = 0;
        }
        else if (ERRCODE_TIMED_OUT == eventStatus) 
        {
            g_authenticationEventsFlags = 0;

            dprint("iAP2_Host_vfnReadACCCertificate_State cert not sent timeout\n");
            if(IAP2H_MAX_ACC_AUTH_RETRIES > iAP2_Host_gbAccAuthRetriesCounter) 
            {
                    dprint("  retry\n");
                    iAP_Auth_vfnCancel();
                    /* Increment retries variable */
                    iAP2_Host_gbAccAuthRetriesCounter++;

                    iAP2_Host_SM.ActualState = IAP2_READ_ACC_CERTIFICATE_STATE;
            }
            else
            { 
                dprint("  retries expired\n");
                /* Restart Accessory Authentication Retries Counter */
                iAP2_Host_gbAccAuthRetriesCounter = 0;
                /* Set error flags */
                iAP2_Host_gwInternalErrorFlags |= (1 << IAP2H_ACC_AUTHENTICATION_ERROR);
                iAP2_Host_gwStatus |= (1 << IAP2H_INTERNAL_ERROR);
                iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
            }
        }
    }
}

/*************************************************************************************************
*
* @brief    State to send the information about Accessory version and certificate. 
* 
* @param       void
* 
* @return      void
*          
**************************************************************************************************/
static void iAP2_Host_vfnSendACCCertificate_State(void)
{ 
    uint8_t bStateStatus;
    uint8_t bUSBStatus;

    dprint("iAP2_Host_vfnSendACCCertificate_State start\n");
    //printf("iAP2_Host_vfnSendACCCertificate_State start\n");

    iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState; 

    /* Try to send the certificate to apple device */
    bStateStatus = IAP2_HOST_SEND_CERTIFICATE(&iAP_Auth_CPCertificate[0], 
            gs_iAP2HostCertificateLength);
    /* If data was correctly sent */
    if(SESSIONS_OK == bStateStatus)
    {
        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
        iAP2_Host_SM.ActualState = IAP2_CREATE_ACC_CHALLENGE_RESPONSE_STATE;
    }
    /* If lower session is busy or attempt to send data was incorrect */
    else if(SESSIONS_BUSY  == bStateStatus)
    {
        dprint("  iAP2_Host_vfnReadACCCertificate_State - busy\n");
        if(IAP2H_MAX_ACC_AUTH_RETRIES > iAP2_Host_gbAccAuthRetriesCounter) 
        {
            dprint("        retries expired\n");
            bUSBStatus = iAP_Transport_Status();
            if(bUSBStatus & USB_IOS_ATTACH)
            {
                /* Increment retries variable */
                iAP2_Host_gbAccAuthRetriesCounter++;
                iAP2_Host_SM.ActualState = IAP2_SEND_ACC_CERTIFICATE_STATE;
            }            
            else
            {
                iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
            }

        }
        else
        {
            dprint("        retry\n");
            /* Restart Accessory Authentication Retries Counter */
            iAP2_Host_gbAccAuthRetriesCounter = 0;
            /* Set error flags */
            iAP2_Host_gwInternalErrorFlags |= (1 << IAP2H_ACC_AUTHENTICATION_ERROR);
            iAP2_Host_gwStatus |= (1 << IAP2H_INTERNAL_ERROR);
            iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
        }
    }
    else
    {
        dprint("  iAP2_Host_vfnReadACCCertificate_State error\n");
        /* Restart Accessory Authentication Retries Counter */
        iAP2_Host_gbAccAuthRetriesCounter = 0;
        /* Set error flags */
        iAP2_Host_gwInternalErrorFlags |= (1 << IAP2H_ACC_AUTHENTICATION_ERROR);
        iAP2_Host_gwStatus |= (1 << IAP2H_INTERNAL_ERROR);
        iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
    }

}

/*************************************************************************************************
 *
 * @brief    State to create a challenge response
 * 
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnCreateChallengeResponse_State(void)
{
    uint8_t bUSBStatus;
    uint8_t eventStatus = ERRCODE_NO_ERROR;

    dprint("iAP2_Host_vfnCreateChallengeResponse_State start\n");
//printf("iAP2_Host_vfnCreateChallengeResponse_State start\n");

    if((iAP2_Host_gwStatus & (1 << IAP2_CREATE_CHALLENGE_START)) == 0)
    {
        dprint("    !IAP2_CREATE_CHALLENGE_START\n");
        //printf("    !IAP2_CREATE_CHALLENGE_START\n");
        /* Wait until challenge request arrives */
        osa_mutex_lock(&gs_iAP2HostMutex);
        while ((g_authenticationEventsFlags &
               (1 << IAP2H_ACC_AUTHENTICATION_CHALLENGE_REQUIRED)) == 0) {
            eventStatus = osa_cond_wait(&gs_iAP2HostEvents,
                                        &gs_iAP2HostMutex,
                                        IAP2_HOST_TIME_OUT);
            if (eventStatus == ERRCODE_TIMED_OUT)
                break;
        }
        osa_mutex_unlock(&gs_iAP2HostMutex);
        //printf("\n eventStatus %d", eventStatus);
        if ((ERRCODE_NO_ERROR == eventStatus) && 
            (g_authenticationEventsFlags &
             (1 << IAP2H_ACC_AUTHENTICATION_CHALLENGE_REQUIRED)))
        {   
            g_authenticationEventsFlags &=
                ~(1 << IAP2H_ACC_AUTHENTICATION_CHALLENGE_REQUIRED);

            iAP2_Host_gwStatus |= (1 << IAP2_CREATE_CHALLENGE_START);

            /* Ask a signature to CP */
            IAP2_HOST_CREATE_CHALLENGE_RESPONSE(&gs_iAP2HostCoprocessorChallengeData[1],
                    &iAP_Auth_gbaRxBufferForCP[0], \
                    sizeof(gs_iAP2HostCoprocessorChallengeData)-1, 
                    &gs_iAP2HostChallengeResponseSize);

        }
        else if (ERRCODE_TIMED_OUT == eventStatus)
        {
            g_authenticationEventsFlags = 0;
            dprint("iAP2_Host_vfnCreateChallengeResponse_State - timed out\n");
            
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
            iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
        }
    }
    else
    {
        dprint("    IAP2_CREATE_CHALLENGE_START\n");
        /* Set 2 second timeout */
        osa_mutex_lock(&gs_iAP2CoprocessorMutex);
        while (g_authenticationEventsFlags == 0) {
            eventStatus = osa_cond_wait(&gs_iAP2CoprocessorEvents,
                                          &gs_iAP2CoprocessorMutex,
                                          IAP2_HOST_AUTH_TIME_OUT);
            if (eventStatus == ERRCODE_TIMED_OUT)
                break;
        }
        osa_mutex_unlock(&gs_iAP2CoprocessorMutex);

        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;

        if (ERRCODE_NO_ERROR == eventStatus)
        {
            
            if(g_authenticationEventsFlags & (1<<KAuthenticationCommandComplete) )
            {
                iAP2_Host_gbAccAuthRetriesCounter = IAP2H_NO_RETRIES;
                iAP2_Host_SM.ActualState = IAP2_SEND_ACC_CHALLENGE_RESPONSE_STATE;

            }
            else if( (g_authenticationEventsFlags & (1<<kAuthenticationInvalidRegisterForRead)) || 
                     (g_authenticationEventsFlags & (1<<kAuthenticationInvalidRegisterForWrite))    || 
                     (g_authenticationEventsFlags & (1<<kAuthenticationInvalidSignatureLength))    || 
                     (g_authenticationEventsFlags & (1<<kAuthenticationInvalidChallengeLength))    || 
                     (g_authenticationEventsFlags & (1<<kAuthenticationInvalidCertificateLength)) ||
                     (g_authenticationEventsFlags & (1<<kAuthenticationInvalidProcessControl)))
            {
                dprint("iAP2_Host_vfnCreateChallengeResponse_State error\n");
                /* Set error flags */
                iAP2_Host_gwStatus |= (1 << IAP2H_INTERNAL_ERROR);

                iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
            }
            else
            {
                iAP2_Host_gwStatus &= ~(1 << IAP2H_ACC_AUTHENTICATION_CHALLENGE_REQUIRED);
                iAP2_Host_SM.ActualState = IAP2_CREATE_ACC_CHALLENGE_RESPONSE_STATE;
            }

            g_authenticationEventsFlags = 0;
        }
        else if (ERRCODE_TIMED_OUT == eventStatus) 
        {    
            g_authenticationEventsFlags = 0;

            dprint("iAP2_Host_vfnCreateChallengeResponse_State timed out2\n");
            iAP_Auth_vfnCancel();
            if(IAP2H_MAX_ACC_AUTH_RETRIES > iAP2_Host_gbAccAuthRetriesCounter) 
            {
                dprint("    retry\n");
                bUSBStatus = iAP_Transport_Status();
                if(bUSBStatus & USB_IOS_ATTACH)
                {
                    iAP2_Host_gwStatus &= ~(1 << IAP2H_ACC_AUTHENTICATION_CHALLENGE_REQUIRED);

                    /* Increment retries variable */
                    iAP2_Host_gbAccAuthRetriesCounter++;
                    iAP2_Host_SM.ActualState = IAP2_CREATE_ACC_CHALLENGE_RESPONSE_STATE;
                }
                else
                {
                    iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
                }

            }
            else
            {
                dprint("    retries expired\n");
                /* Restart Accessory Authentication Retries Counter */
                iAP2_Host_gbAccAuthRetriesCounter = 0;
                /* Set error flags */
                iAP2_Host_gwInternalErrorFlags |= (1 << IAP2H_ACC_AUTHENTICATION_ERROR);
                iAP2_Host_gwStatus |= (1 << IAP2H_INTERNAL_ERROR);
                iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
            }
        }
    }
}

/*************************************************************************************************
 *
 * @brief    State to send the challenge response
 * 
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnSendChallengeResponse_State(void)
{
    uint8_t bStateStatus;
    uint8_t bUSBStatus;

    dprint("iAP2_Host_vfnSendChallengeResponse_State start\n");
//printf("iAP2_Host_vfnSendChallengeResponse_State start\n");

    g_authenticationEventsFlags = 0;

    iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
    
    /* Try to send the challenge response to apple device */
    bStateStatus = IAP2_HOST_SEND_CHALLENGE_RESPONSE(&iAP_Auth_gbaRxBufferForCP[0], \
            gs_iAP2HostChallengeResponseSize);
    /* If data was correctly sent */    
    if(SESSIONS_OK == bStateStatus)
    {
        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
        iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_STATUS_STATE;
    }
    /* If lower session is busy or attempt to send data was incorrect */
    else if(SESSIONS_BUSY  == bStateStatus)
    {
        dprint("iAP2_Host_vfnSendChallengeResponse_State busy\n");
        if(IAP2H_MAX_ACC_AUTH_RETRIES > iAP2_Host_gbAccAuthRetriesCounter) 
        {
            dprint("    retry\n");
                /* Increment retries variable */
                iAP2_Host_gbAccAuthRetriesCounter++;
                iAP2_Host_SM.ActualState = IAP2_SEND_ACC_CHALLENGE_RESPONSE_STATE;        
        }
    }
    else
    {
        dprint("iAP2_Host_vfnSendChallengeResponse_State error\n");
        /* Restart Accessory Authentication Retries Counter */
        iAP2_Host_gbAccAuthRetriesCounter = 0;
        /* Set error flags */
        iAP2_Host_gwInternalErrorFlags |= (1 << IAP2H_ACC_AUTHENTICATION_ERROR);
        iAP2_Host_gwStatus |= (1 << IAP2H_INTERNAL_ERROR);
        iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
    }
}



/*************************************************************************************************
 *
 * @brief    State to handle authentication errors and retries.
 * 
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnAccAuthenticationError_State(void)
{       
    dprint("iAP2_Host_vfnAccAuthenticationError_State start\n");
//printf("iAP2_Host_vfnAccAuthenticationError_State start\n");

    /*Do not nothing*/
}


/*************************************************************************************************
*
* @brief    This state waits and sets the status of the whole authentication process.
* 
* @param       void
* 
* @return      void
*          
**************************************************************************************************/
static void iAP2_Host_vfnWaitAuthenticationStatus_State(void)
{
    iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;
    uint8_t eventStatus = ERRCODE_NO_ERROR;

    dprint("iAP2_Host_vfnWaitAuthenticationStatus_State start\n");
//printf("iAP2_Host_vfnWaitAuthenticationStatus_State start\n");

    /* Wait until auth request arrives */
    osa_mutex_lock(&gs_iAP2HostMutex);
    while ((g_authenticationEventsFlags &
           ((1 << IAP2H_ACC_AUTHENTICATION_FAILED) |
            (1 << IAP2H_ACC_AUTHENTICATION_SUCCEEDED))) == 0) {
        eventStatus = osa_cond_wait(&gs_iAP2HostEvents,
                                    &gs_iAP2HostMutex,
                                    IAP2_HOST_TIME_OUT);
        if (eventStatus == ERRCODE_TIMED_OUT)
            break;
    }
    osa_mutex_unlock(&gs_iAP2HostMutex);

    if(ERRCODE_NO_ERROR == eventStatus)
    {   
        if(g_authenticationEventsFlags & (1 << IAP2H_ACC_AUTHENTICATION_FAILED) )
        {
            g_authenticationEventsFlags &= ~(1 << IAP2H_ACC_AUTHENTICATION_FAILED);
            dprint("  IAP2H_ACC_AUTHENTICATION_FAILED\n");
//printf("  IAP2H_ACC_AUTHENTICATION_FAILED\n");
            /* Authentication process failed */
            iAP2_Host_gbAccAuthRetriesCounter = IAP2H_NO_RETRIES;
            iAP2_Host_gwStatus |= (1 << IAP2H_ACC_AUTHENTICATION_FAILED);
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;    
            iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
            iAP_Auth_vfnCancel();
        }
        else if (g_authenticationEventsFlags & (1 << IAP2H_ACC_AUTHENTICATION_SUCCEEDED) )
        {
            g_authenticationEventsFlags &= ~(IAP2H_ACC_AUTHENTICATION_SUCCEEDED);
            dprint("  IAP2H_ACC_AUTHENTICATION_SUCCEEDED\n");
//printf("  IAP2H_ACC_AUTHENTICATION_SUCCEEDED\n");
			/* Set aunthentication done flag */
			iAP2_Host_gwStatus |= (1 << IAP2H_ACC_AUTHENTICATION_DONE);
			/* Go to next state */
			iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;	
			iAP2_Host_SM.ActualState = IAP2_START_IDENTIFICATION_STATE;
		}
        iAP_Auth_bfnDeInit();
	}
	else if (ERRCODE_TIMED_OUT ==  eventStatus)
	{
        g_authenticationEventsFlags = 0;
        dprint("iAP2_Host_vfnWaitAuthenticationStatus_State timed out\n");
//printf("iAP2_Host_vfnWaitAuthenticationStatus_State timed out\n");
        iAP_Auth_vfnCancel();
        iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
    }
}

/*************************************************************************************************
 *
 * @brief    This state detects when a apple has been connected.
 * 
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnDeviceAttachDetect_State(void)
{
    uint8_t bUSBStatus = 0x00;
    uint8_t bUARTStatus = 0x00;

    dprint("iAP2_Host_vfnDeviceAttachDetect_State start\n");
//printf("iAP2_Host_vfnDeviceAttachDetect_State start\n");

    /* Check if a device has been connected */
    bUSBStatus = iAP_Transport_Status();
#if !defined(IAPI_INTERFACE_USB_DEV) && !defined(IAPI_INTERFACE_USB_HOST)
    bUARTStatus = IAP2_HOST_CHECK_IF_UART_MODE;     /* ACC_PWR is only needed for UART on Lightning Connector */
#endif
#if !defined(IAPI_INTERFACE_USB_HOST) && defined(MFI_IAP1)  /* IOS_DET is needed to set the resistor ID for iAP1 */ 
    if(IAP_HOST_IDEVICE_CONNECTED)
    {
        IAP_HOST_SET_ACC_DETECT_PIN;
    }
#endif
#if !defined(IAPI_INTERFACE_USB_DEV) && !defined(IAPI_INTERFACE_USB_HOST)   
    if((bUSBStatus & USB_IOS_ATTACH) || (bUARTStatus & UART_ATTACH))
#else
    if((bUSBStatus & USB_IOS_ATTACH))
#endif
    {
            
            /* Move state machine */
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
            iAP2_Host_SM.ActualState = IAP2_WAIT_LINK_INIT_STATE;

    }
    else
    {
        osa_time_delay(20);
    }
}

#ifdef IAPI_INTERFACE_UART
static void iAP2_Host_vfnUARTInitState(void)
{
    if(IAP_TIMER_CHECK_CHN_FLAG(IAP_WAIT_LINK_INIT_TIMER))
    {
        /* Send initial 0xFF */
        /** @todo Function Return */
        (void)iAP_Transport_SendData((uint8_t *)(&iAP2_Host_cbaInitSeq[0]), 1);
        /*Set timer to wait at least 80ms */
        IAP_TIMER_SET_PERIOD(IAP_WAIT_LINK_INIT_TIMER,IAP_TIMEOUT_40MS_PERIOD);
        IAP_TIMER_ENABLE_CHN(IAP_WAIT_LINK_INIT_TIMER);
        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;    
        iAP2_Host_SM.ActualState = IAP2_WAIT_LINK_INIT_STATE;

    }
}
#endif

/*************************************************************************************************
*
* @brief    This state waits for at least 80ms after an apple device has been connected.
* 
* @param        void
* 
* @return       void
*          
**************************************************************************************************/
static void iAP2_Host_vfnWaitLinkInitState(void)
{
    dprint("iAP2_Host_vfnWaitLinkInitState start\n");
//printf("iAP2_Host_vfnWaitLinkInitState start\n");

    osa_time_delay(80);
    /*Set event to wait for a time out at least 80ms (event must not happend) */
    //g_eventStatus =  event_wait(&gs_iAP2HostEvents, 1, &g_authenticationEventsFlags);

    /* Check if timer has expired */
    //if(kTimeout == g_eventStatus )
    {        
        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
        iAP2_Host_SM.ActualState = IAP2_PROTOCOL_INIT_STATE;
    }
}

/*************************************************************************************************
 *
 * @brief    This function initializes iAP2 or transitions to iAP2 depending on initialization response.
 * 
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnProtocolInit_State(void)
{
    uint16_t dataTransmitionStatus;
    uint8_t bStateStatus, authStatus, eventStatus = ERRCODE_NO_ERROR;

    dprint("iAP2_Host_vfnProtocolInit_State start\n");
 //printf("iAP2_Host_vfnProtocolInit_State start\n");

    if((iAP2_Host_gwStatus & (1 << IAP2H_INIT_SENT)) == 0)
    {
        dprint("  !IAP2H_INIT_SENT\n");
//printf("  !IAP2H_INIT_SENT\n");
        /* Device detected. Transmit init frame to detect protocol supported */
        /** @todo Function Return */
        dataTransmitionStatus = iAP_Transport_SendData((uint8_t *)(&iAP2_Host_cbaInitSeq[0]), sizeof(iAP2_Host_cbaInitSeq));
        if(IAP_TRANS_OK == dataTransmitionStatus)
        {
            gs_iAP2HostInitCount = 0;
            gs_iAP2HostReceiveCount = 0;
            iAP2_Host_gwStatus |= 1 << IAP2H_INIT_SENT;
        }
//        else
//        {
//            osa_time_delay(5);
//        }
    }
    else 
    {
        dprint("  IAP2H_INIT_SENT\n");
//printf("  IAP2H_INIT_SENT\n");
        /*Wait for init complete event or 1s timeout */
        osa_mutex_lock(&gs_iAP2HostMutex);
        eventStatus = ERRCODE_NO_ERROR;
        while ((g_authenticationEventsFlags &
               (1 << IAP2H_INIT_COMPLETE)) == 0) {
            eventStatus = osa_cond_wait(&gs_iAP2HostEvents,
                                       &gs_iAP2HostMutex,
                                        IAP2_HOST_TIME_OUT);
            if (eventStatus == ERRCODE_TIMED_OUT)
                break;
            //osa_time_delay(80);
	    
        }
        osa_mutex_unlock(&gs_iAP2HostMutex);
        //printf("g_authenticationEventsFlags %d eventStatus %d\n",g_authenticationEventsFlags, eventStatus);

        if(ERRCODE_NO_ERROR == eventStatus) 
        {
            authStatus = g_authenticationEventsFlags;
            g_authenticationEventsFlags = 0;

            /*Init Authentication state machine*/ 
            bStateStatus = iAP_Auth_bfnInit(&auth_config);
            if(IAP_AUTH_OK == bStateStatus) 
            {
                if(authStatus & (1 << IAP2H_IAP2_DETECTED))
                {
                    dprint("IAP2 detected\n");
//printf("IAP2 detected\n");
                    /* Init the iAP2 Link and send the initial LSP */
                    bStateStatus = iAP2_Link_bfnInit();
                    if(IAP2_LINK_STATUS_OK == bStateStatus) 
                    {
                        dprint("  iAP2_Sessions_bfnInit\n");
//printf("  iAP2_Sessions_bfnInit\n");
                        /*Init Sessions state machine*/
                        bStateStatus = iAP2_Sessions_bfnInit();
                        if(IAP_SESSIONS_INIT_OK == bStateStatus) 
                        {
                            dprint("  iAP2_Link_vfnSendSyncPayload\n");
                            printf("  iAP2_Link_vfnSendSyncPayload\n");
                            iAP2_Link_vfnSendSyncPayload(&iAP2_Host_sLSP);
                            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
                            iAP2_Host_SM.ActualState = IAP2_LINK_SYNC_PAYLOAD_STATE;
                        }
                        else
                        {
                            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
                            iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
                        }
                    }
                    else
                    {
                        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
                        iAP2_Host_SM.ActualState = IAP2_ACC_AUTHENTICATION_ERROR_STATE;
                    }
                }
                else
                {
                    dprint("IAP1 detected\n");
//printf("IAP1 detected\n");
                    /* Init the iAP2 Link and send the initial LSP */
#ifdef MFI_IAP1
                    /* Initialize iAP1 state machine */
                    iap_host_initialization();
#endif
                    /* Switch to iAP1 state */
                    iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
                    iAP2_Host_SM.ActualState = IAP1_STATE;
                    iAP2_Host_gwStatus |= (1 << IAP2H_IAP1_DETECTED);
                }
            }
        }
        else if(ERRCODE_TIMED_OUT == eventStatus)
        {
            g_authenticationEventsFlags = 0;
            dprint("iAP2_Host_vfnProtocolInit_State timed out\n");
//printf("iAP2_Host_vfnProtocolInit_State timed out\n");
            iAP2_Host_gwStatus = 0;
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
            iAP2_Host_SM.ActualState = IAP2_DEVICE_ATTACH_DET_STATE;
            
        }
    }
}

/*************************************************************************************************
 *
 * @brief    This function initializes iAP2 or transitions to iAP2 depending on initialization response.
 * 
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnLinkSync_State(void)
{
    uint8_t eventStatus = ERRCODE_NO_ERROR;

    dprint("iAP2_Host_vfnLinkSync_State start\n");
//printf("iAP2_Host_vfnLinkSync_State start\n");

    /*Wait for init complete event */
    osa_mutex_lock(&gs_iAP2HostMutex);
    while ((g_authenticationEventsFlags &
           (1 << IAP2H_LSP_RECEIVED)) == 0) {
        eventStatus = osa_cond_wait(&gs_iAP2HostEvents,
                                    &gs_iAP2HostMutex,
                                    IAP2_HOST_TIME_OUT);
    }
    osa_mutex_unlock(&gs_iAP2HostMutex);
    //printf("\n eventStatus %d", eventStatus);
    if ((ERRCODE_NO_ERROR == eventStatus) && 
       (g_authenticationEventsFlags & (1 << IAP2H_LSP_RECEIVED)))
    {
        g_authenticationEventsFlags &= ~(1 << IAP2H_LSP_RECEIVED);

        /* Compare the received data with the initially sent data to determine if values are valid */
        if((iAP2_Host_sLSP.wRetTimeOut <= iAP2_Host_csInitLSP.wRetTimeOut) &&
                (iAP2_Host_sLSP.wCumAckTimeOut <= iAP2_Host_csInitLSP.wCumAckTimeOut) &&
                (iAP2_Host_sLSP.bMaxNumRestransmission <= iAP2_Host_csInitLSP.bMaxNumRestransmission) &&
                (iAP2_Host_sLSP.bMaxCumAck <= iAP2_Host_csInitLSP.bMaxCumAck) &&
                (iAP2_Host_sLSP.bNumSessions <= iAP2_Host_csInitLSP.bNumSessions))
        {
            /** @todo iAP2_Host - EM: What to do if the number of sessions is less than initially sent?? */
            /* Configure the link with the latest negotiable parameters */
            iAP2_Host_sLSP.bMaxNumOutstandingPackets = iAP2_Host_csInitLSP.bMaxNumOutstandingPackets;
            iAP2_Host_sLSP.wMaxPackLen = iAP2_Host_csInitLSP.wMaxPackLen;
            iAP2_Link_vfnLinkConfig(&iAP2_Host_sLSP);

            iAP2_Link_vfnAcceptSyncPayload();
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
            iAP2_Host_SM.ActualState = IAP2_READ_ACC_CERTIFICATE_STATE;
        }
        else
        {
            dprint("iAP2_Host_vfnLinkSync_State ???\n");
            /** @todo iAP2_Host - EM: What to do if the device proposes higher values?? */
        }
    }
    else
    {
        g_authenticationEventsFlags = 0;
        dprint("iAP2_Host_vfnLinkSync_State not OK\n");
        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
        iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
        
    }
}

/** ***********************************************************************************************
 *
 * @brief    Function that reads the data to compute the challenge response
 * 
 * @param    void
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnReadChallengeData(void)
{
    uint8_t bStatus;

    dprint("iAP2_Host_vfnReadChallengeData start\n");

    /* Read data for challenge response */
    bStatus = IAP2_HOST_READ_CHALLENGE_DATA(&gs_iAP2HostChallengeParameterID, \
            &gs_iAP2HostCoprocessorChallengeData[1], &gs_iAP2HostChallengeSize);
    /* If data was correctly read and the parameter ID is right set Auth required flag */
    if((IAP2_SESS_CTRL_MESSAGE_COMPLETE == bStatus) && 
            (IAP2_AUTHEN_CHALLENGE_PARAMETER == gs_iAP2HostChallengeParameterID))
    {
        dprint("iAP2_Host_vfnReadChallengeData sess ctrl msg complete\n");
        osa_mutex_lock(&gs_iAP2HostMutex);
        g_authenticationEventsFlags |=
            (1 << IAP2H_ACC_AUTHENTICATION_CHALLENGE_REQUIRED);
        osa_mutex_unlock(&gs_iAP2HostMutex);
        osa_cond_signal(&gs_iAP2HostEvents);
    }

    dprint("iAP2_Host_vfnReadChallengeData end\n");
}

/*************************************************************************************************
 *
 * @brief    This function handles the Accessory Identification.
 * 
 * @param    wMessageID      Message ID.
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnIdentificationState(void)
{
    uint8_t eventStatus = ERRCODE_NO_ERROR;

    dprint("iAP2_Host_vfnIdentificationState start\n");
//printf("iAP2_Host_vfnIdentificationState start\n");

    /* g_eventStatusauth request arrives */
    osa_mutex_lock(&gs_iAP2HostMutex);
    while ((g_authenticationEventsFlags &
           ((1 << IAP2H_ID_START) |
            (1 << IAP2H_ID_ACCEPTED) |
            (1 << IAP2H_ID_REJECTED))) == 0) {
        eventStatus = osa_cond_wait(&gs_iAP2HostEvents,
                                    &gs_iAP2HostMutex,
                                    IAP2_HOST_TIME_OUT);
        if (eventStatus == ERRCODE_TIMED_OUT)
            break;
    }
    osa_mutex_unlock(&gs_iAP2HostMutex);
    //printf("eventStatus %d\n", eventStatus);

    if(ERRCODE_NO_ERROR == eventStatus)
    {
        if (g_authenticationEventsFlags & (1 << IAP2H_ID_START))
        {
            g_authenticationEventsFlags = 0;
            dprint("iAP2_Host_vfnIdentificationState ID START\n");
//printf("iAP2_Host_vfnIdentificationState ID START\n");
            iAP2_Host_gwStatus &= ~(1 << IAP2H_ID_START);
            /** @todo Function Return */
            (void)iAP2_Identification_bfnSendIdentificationInfo(&gs_rejectedInformation);
        }
        else if(g_authenticationEventsFlags & (1 << IAP2H_ID_ACCEPTED))
        {
            g_authenticationEventsFlags = 0;
            dprint("iAP2_Host_vfnIdentificationState ID ACCEPTED\n");
            //printf("iAP2_Host_vfnIdentificationState ID ACCEPTED\n");

            /** @todo iAP2_Host - EM: Indicate that features messages may be sent */
#if IAP2_IDENT_PWR_SOURCE_TYPE_DATA != 0
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
            iAP2_Host_SM.ActualState = IAP2_POWER_STATE;
#elif IAP2_CTRL_MSG_APP_LAUNCH
        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;
        iAP2_Host_SM.ActualState = IAP2_APP_LAUNCH_STATE;
#else
            iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
#endif
            
        }    
        else if(g_authenticationEventsFlags & (1 << IAP2H_ID_REJECTED))
        {
            g_authenticationEventsFlags = 0;
            dprint("iAP2_Host_vfnIdentificationState ID REJECTED\n");
//printf("iAP2_Host_vfnIdentificationState ID REJECTED\n");
            /** @todo iAP2_Host - EM: Manage rejection information */
            iAP2_Host_gwStatus &= ~(1 << IAP2H_ID_START);
            iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
            
        }
    }
    else if (ERRCODE_TIMED_OUT == eventStatus) 
    {
        g_authenticationEventsFlags = 0;
        dprint("iAP2_Host_vfnIdentificationState timed out\n");
//printf("iAP2_Host_vfnIdentificationState timed out\n");
        iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
        iAP2_Host_gwStatus = 0;
    }
}

#if IAP2_IDENT_PWR_SOURCE_TYPE_DATA != 0
static void iAP2_Host_vfnPowerUpdateState(void)
{
    uint8_t bStatus;

    dprint("iAP2_Host_vfnPowerUpdateState start\n");

    if(!(iAP2_Host_gwStatus & (1 << IAP2H_POWER_SENT)))
    {
        bStatus = iAP2_Power_Features_bfnPowerSourceUpdate(MFI_CHARGE_CURRENT_AVAILABLE, MFI_CHARGE_ENABLED);
        if(iAP2_POWER_FEATURE_ERROR_OK == bStatus)
        {
            iAP2_Host_gwStatus |= (1 << IAP2H_POWER_SENT);
            iAP2_Host_SM.ActualState = IAP2_IDLE_STATE;
        }
    }
    else
    {
#if IAP2_CTRL_MSG_APP_LAUNCH
    	iAP2_Host_gwStatus &= ~(1 << IAP2H_POWER_SENT);
        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;
        iAP2_Host_SM.ActualState = IAP2_APP_LAUNCH_STATE;
#endif
    }
}
#endif

static void iAP2_Host_vfnIdleState(void)
{
    /*Do not nothing*/
}
#if IAP2_CTRL_MSG_APP_LAUNCH
static void iAP2_Host_vfnAppLaunchState(void)
{
	uint8_t bStatus;

    if(!(iAP2_Host_gwStatus & (1 << IAP2H_POWER_SENT)))
    {
        bStatus = iAP2_Application_Launch_Feature_bfnAppLaunch();
        if(iAP2_APP_LAUNCH_ERROR_OK == bStatus)
        {
            iAP2_Host_gwStatus |= (1 << IAP2H_POWER_SENT);
        }
    }
    else
    {
    	/*Do nothing */
    }
    
     g_app_launch = 1;
}
/*************************************************************************************************
* @name     iAP2_Host_AppLaunchDone
*
* @brief    return the App launch status flag
* 
* @param   	None
* 
* @return  	void
**************************************************************************************************/
int iAP2_Host_AppLaunchDone()
{
    return g_app_launch; 
}

#endif
#ifndef MFI_IAP1
/*************************************************************************************************
*
* @brief    Function that calls iAP1 state machine.
* 
* @param       wMessageID        Message ID.
* 
* @return      void
*          
**************************************************************************************************/
static void iAP2_Host_vfniAP1State(void)
{
    dprint("iAP2_Host_vfniAP1State start\n");    
}
#endif

#ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
/*************************************************************************************************
 *
 * @brief    Function that calls iAP1 state machine.
 * 
 * @param    wMessageID      Message ID.
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnRoleSwitch(void)
{

    uint8_t bStatus;

dprint("iAP2_Host_vfnRoleSwitch \n");    
//printf("iAP2_Host_vfnRoleSwitch \n");    

    bStatus = iAP_Transport_Status();
    if(USB_ROLE_SWITCH_DONE_MASK & bStatus)
    {
        if(USB_ROLE_CHANGED_MASK & bStatus)
        {
            /** @todo Function Return */
            (void)iAP_Transport_SelectTranspot(IAP_TRANSPORT_USB_DEVICE);
            
            /* Set 1 second timeout */
            IAP_TIMER_SET_PERIOD(IAPH_COPROCESSOR_TIMEOUT_CHN,IAP_TIMEOUT_1SEC_PERIOD);
            IAP_TIMER_ENABLE_CHN(IAPH_COPROCESSOR_TIMEOUT_CHN);
        
            /* Set states of SM */
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;
            iAP2_Host_SM.ActualState = IAP2_ROLE_SWITCH_WAIT_STATE;
            iAP2_Host_gwStatus |= (1 << IAP2H_ROLE_CHANGED);
        }
        else
        {
            iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;  
            iAP2_Host_SM.ActualState = IAP2_PROTOCOL_INIT_STATE;

            /* Device detected. Transmit init frame to detect protocol supported */
            /** @todo Function Return */
            (void)iAP_Transport_SendData((uint8_t *)(&iAP2_Host_cbaInitSeq[0]), sizeof(iAP2_Host_cbaInitSeq));
            gs_iAP2HostInitCount = 0;
            gs_iAP2HostReceiveCount = 0;
        }
    }
}

/*************************************************************************************************
 *
 * @brief    Function that calls iAP1 state machine.
 * 
 * @param    wMessageID      Message ID.
 * 
 * @return   void
 *          
 **************************************************************************************************/
static void iAP2_Host_vfnRoleSwitchWait(void)
{
dprint("iAP2_Host_vfnRoleSwitchWait \n");    
//printf("iAP2_Host_vfnRoleSwitchWait \n");    
/*Wait for time out*/
    osa_time_delay(1000);
    //g_eventStatus =  event_wait(&gs_iAP2HostEvents, 1000, 
            //&g_authenticationEventsFlags);  
            
    /* Check if timer has expired */
    //if(kTimeout == g_eventStatus)
    {

        /* Move SM */
        iAP2_Host_SM.PrevState = iAP2_Host_SM.ActualState;
        iAP2_Host_SM.ActualState = IAP2_PROTOCOL_INIT_STATE;

        /* Device detected. Transmit init frame to detect protocol supported */
        /** @todo Function Return */
        (void)iAP_Transport_SendData((uint8_t *)(&iAP2_Host_cbaInitSeq[0]), sizeof(iAP2_Host_cbaInitSeq));
        gs_iAP2HostInitCount = 0;
        gs_iAP2HostReceiveCount = 0;

        iAP2_Host_gwStatus |= 1 << IAP2H_INIT_SENT;

    }
}
#endif

/*************************************************************************************************
*
* @brief    Function that sets flags depending on the message that arrived
* 
* @param       wMessageID        Message ID.
* 
* @return      void
*          
**************************************************************************************************/
void iAP2_Host_vfnNewAuthenticationMessage(uint16_t wMessageID)
{
    dprint("iAP2_Host_vfnNewAuthenticationMessage arrived\n");
//printf("iAP2_Host_vfnNewAuthenticationMessage arrived\n");

    /* Check what ID Message arrived */
    switch(wMessageID)
    {
    /* set a flag that correspond to the message that arrived */    
    /* Authentication requiered */
    case IAP2_AUTHEN_FEAT_REQ_AUTH_ID:
        dprint("    IAP2_AUTHEN_FEAT_REQ_AUTH_ID\n");
//printf("    IAP2_AUTHEN_FEAT_REQ_AUTH_ID\n");
        /* Set Authentication required Flag*/
        osa_mutex_lock(&gs_iAP2HostMutex);
        g_authenticationEventsFlags |=
            (1 << IAP2H_ACC_AUTHENTICATION_REQUIRED);
        osa_mutex_unlock(&gs_iAP2HostMutex);
        osa_cond_signal(&gs_iAP2HostEvents);
        break;
        /* A Challenge message arrived*/
    case IAP2_AUTHEN_FEAT_CHALLENGE_ID:
        dprint("    IAP2_AUTHEN_FEAT_CHALLENGE_ID\n");
//printf("    IAP2_AUTHEN_FEAT_CHALLENGE_ID\n");
        /* Call a function that read the data challenge */
        iAP2_Host_vfnReadChallengeData();
        break;
        /* Authentication failed message */
    case IAP2_AUTHEN_FEAT_AUTH_FAILED_ID:
        dprint("    IAP2_AUTHEN_FEAT_AUTH_FAILED_ID\n");
        /* Set Authentication Failed Flag*/
        osa_mutex_lock(&gs_iAP2HostMutex);
        g_authenticationEventsFlags |=
            (1 << IAP2H_ACC_AUTHENTICATION_FAILED);
        osa_mutex_unlock(&gs_iAP2HostMutex);
        osa_cond_signal(&gs_iAP2HostEvents);
        break;

        /* Authentication succeeded message */
    case IAP2_AUTHEN_FEAT_AUTH_SUCCEEDED_ID:
        dprint("    IAP2_AUTHEN_FEAT_AUTH_SUCCEEDED_ID\n");
//printf("    IAP2_AUTHEN_FEAT_AUTH_SUCCEEDED_ID\n");
        /* Set Authentication succeeded Flag*/
        osa_mutex_lock(&gs_iAP2HostMutex);
        g_authenticationEventsFlags |=
            (1 << IAP2H_ACC_AUTHENTICATION_SUCCEEDED);
        osa_mutex_unlock(&gs_iAP2HostMutex);
        osa_cond_signal(&gs_iAP2HostEvents);
        break;
    }
}

/*************************************************************************************************
 *
 * @brief    Function that sets flags depending on the message that arrived
 * 
 * @param    wMessageID      Message ID.
 * 
 * @return   void
 *          
 **************************************************************************************************/
void iAP2_Host_vfnNewIDMessage(uint16_t wMessageID)
{
    dprint("iAP2_Host_vfnNewIDMessage arrived\n");
//printf("iAP2_Host_vfnNewIDMessage arrived\n");

    /* Check what ID Message arrived */
    switch(wMessageID)
    {
    /* set a flag that correspond to the message that arrived */    
    /* Authentication requiered */
    case IAP2_IDENTIFICATION_START_IDENTIFICATION:
        dprint("    IAP2_IDENTIFICATION_START_IDENTIFICATION\n");
//printf("    IAP2_IDENTIFICATION_START_IDENTIFICATION\n");
        /* Set Authentication succeeded Flag*/
        /* Set Authentication required Flag*/
        osa_mutex_lock(&gs_iAP2HostMutex);
        g_authenticationEventsFlags |=
            (1 << IAP2H_ID_START);
        osa_mutex_unlock(&gs_iAP2HostMutex);
        osa_cond_signal(&gs_iAP2HostEvents);
        break;
        /* A Challenge message arrived*/
    case IAP2_IDENTIFICATION_ACCEPTED:
        dprint("    IAP2_IDENTIFICATION_ACCEPTED\n");
//printf("    IAP2_IDENTIFICATION_ACCEPTED\n");
        /* Set Authentication succeeded Flag*/
        /* Call a function that read the data challenge */
        iAP2_Host_gwStatus |= (1 << IAP2H_ID_ACCEPTED);
        osa_mutex_lock(&gs_iAP2HostMutex);
        g_authenticationEventsFlags |=
            (1 << IAP2H_ID_ACCEPTED);
        osa_mutex_unlock(&gs_iAP2HostMutex);
        osa_cond_signal(&gs_iAP2HostEvents);
        break;
        /* Authentication failed message */
    case IAP2_IDENTIFICATION_REJECTED:
        dprint("    IAP2_IDENTIFICATION_REJECTED\n");
//printf("    IAP2_IDENTIFICATION_REJECTED\n");
        /* Set Authentication succeeded Flag*/
        /* Set Authentication Failed Flag*/
        iAP2_Host_gwStatus |= (1 << IAP2H_ID_REJECTED);
        osa_mutex_lock(&gs_iAP2HostMutex);
        g_authenticationEventsFlags |=
            (1 << IAP2H_ID_REJECTED);
        osa_mutex_unlock(&gs_iAP2HostMutex);
        osa_cond_signal(&gs_iAP2HostEvents);
        break;
    }
}

/*************************************************************************************************
*
* @brief    Function to initialize the iAP2 Host module. There is no need to call other initialization routine.
* 
* @param       void
* 
* @return      void
*          
**************************************************************************************************/
uint8_t iAP2_Host_bfnHostInitialization(MFIHostConfig *config)
{
    OsaThreadAttr attr;
    uint8_t bHostInitStatus = IAP2_HOST_INIT_FAILURE;
    int status;

    //iAP_USB_Role_Switch_Init();

    dprint("iAP2_Host_bfnHostInitialization start\n");
printf("iAP2_Host_bfnHostInitialization start\n");

    strlcpy(g_hostConfig.hid_dev, config->hid_dev, 32);
    strlcpy(g_hostConfig.i2c_dev, config->i2c_dev, 32);
    g_hostConfig.i2c_addr = config->i2c_addr;

    dprint("iAP2_Host_SetConfig start\n");
    dprint("  using hiddev: %s\n", g_hostConfig.hid_dev);
    dprint("  using i2cdev: %s\n", g_hostConfig.i2c_dev);
    dprint("  using i2caddr: %d\n", g_hostConfig.i2c_addr);

    printf("iAP2_Host_SetConfig start\n");
    printf("  using hiddev: %s\n", g_hostConfig.hid_dev);
    printf("  using i2cdev: %s\n", g_hostConfig.i2c_dev);
    printf("  using i2caddr: %d\n", g_hostConfig.i2c_addr);

    strlcpy(auth_config.i2c_dev, config->i2c_dev, 32);
    auth_config.events_callback =iAP2_Host_bfnEventsCallback;
    auth_config.i2c_addr = g_hostConfig.i2c_addr;
    g_authenticationEventsFlags = 0;

    osa_mutex_create(&gs_iAP2HostMutex, false);
    osa_cond_create(&gs_iAP2HostEvents);
    osa_mutex_create(&gs_iAP2CoprocessorMutex, false);
    osa_cond_create(&gs_iAP2CoprocessorEvents);

    /* Init selected transport for communication */
	iAP_Transport_Init();
    
	/* Native endpoints initialization */
	iAP2_Nat_Ex_Acc_Init();    

    /* Configure transport callback to receive the protocol init response */
    iAP_Transport_SetRecvCallback(iAP2_Host_TransportRxCbk);
    iAP_Transport_SetSentCallback(NULL);

    iAP_init_failed = false;

    /* Clear Internal Flags */
    iAP2_Host_gwStatus = 0;
    iAP2_Host_gwInternalErrorFlags = 0;

#ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
    iAP2_Host_SM.ActualState = IAP2_ROLE_SWITCH_STATE;
	//printf("IAP2_ROLE_SWITCH_STATE\n");
#else
    /* Set main state machine to initial state */
    iAP2_Host_SM.ActualState = IAP2_DEVICE_ATTACH_DET_STATE;
#endif

    iAP2_Host_stateMachine_Exit = false;

    osa_thread_attr_init(&attr);

    osa_thread_attr_set_stack_size(&attr, 3000);
    status = osa_thread_create(&iAP2_Host_stateMachine_TaskId,
                               &attr,
                               iAP2_Host_vfStateMachineDriver,
                               NULL);
    osa_thread_set_name(iAP2_Host_stateMachine_TaskId, "iAP2 Main Task");

    osa_thread_attr_destroy(&attr);

    if (ERRCODE_NO_ERROR == status)
        bHostInitStatus = IAP2_HOST_INIT_OK;

    return bHostInitStatus;
}

/*************************************************************************************************
*
* @brief    Function to un-initialize the iAP2 Host module. There is no need to call other initialization routine.
* 
* @param       void
* 
* @return      void
*          
**************************************************************************************************/
uint8_t iAP2_Host_bfnHostUnInitialization(void)
{
    dprint("iAP2_Host_bfnHostUnInitialization start\n");

    if (iAP2_Host_stateMachine_Exit) {
        dprint("  uninit exit - already exiting\n");
        return 0;
    }
    
    iAP2_Host_stateMachine_Exit = true;
    iAP_init_failed = true;
    
    /* Only uninit Link and Sessions for iAP2 devices */
    if(!(iAP2_Host_gwStatus & (1 << IAP2H_IAP1_DETECTED)) && 
       iAP2_Host_SM.ActualState > IAP2_PROTOCOL_INIT_STATE)
    {
        iAP2_Link_bfnDeinit();
        iAP2_Sessions_bfnDeInit();
    }
#ifdef MFI_IAP1
    else if (iAP2_Host_gwStatus & (1 << IAP2H_IAP1_DETECTED)) {
        iap_host_uninitialization();
    }
#endif

    iAP_Auth_bfnDeInit();
    iAP_Transport_UnInit();

    osa_mutex_destroy(&gs_iAP2HostMutex);
    osa_cond_destroy(&gs_iAP2HostEvents);
    osa_mutex_destroy(&gs_iAP2CoprocessorMutex);
    osa_cond_destroy(&gs_iAP2CoprocessorEvents);

    return 0;
}

/*************************************************************************************************
*
*
* @brief    Main state machine used to dispatch the functions for the Host and the Authentication modules.
* 
* @param       void
* 
* @return      void
*          
**************************************************************************************************/
#if (FSL_OS_NONE == FSL_OS_SELECTED)
void iAP2_Host_vfStateMachineDriver(void)
#else
void iAP2_Host_vfStateMachineDriver(void * param)
#endif
{   
    uint8_t bUSBStatus = 0x00;
    uint8_t bUARTStatus = 0x00;
    uint8_t bEAStatus = 0x00; //@todo JS/DM: for External Accessory as temporal workaround

    dprint("iAP2_Host_vfStateMachineDriver start\n");
    printf("iAP2_Host_vfStateMachineDriver start\n");

#if (FSL_OS_NONE == FSL_OS_SELECTED)

#if defined(IAPI_INTERFACE_USB_HOST)
    /* Run USB Host task if it is enabled */    
    /** @todo Function Return */
    (void)iAP_USBHostTask();
#endif

    dprint("iAP_USBHostTask started\n");
    printf("iAP_USBHostTask started\n");

    /* Run Link Task */
    iAP2_Link_vfnTask();    /**@todo iAP2_Host - EM: This is called before the link is initialized. */
    /* Run Sessions State Machine */
    /** @todo Function Return */
    (void)iAP2_Sessions_vfnStateMachine();
    /* Run Auth State Machine */
    iAP_Auth_vfnStateMachine();

    dprint("Check point 1\n");
    //printf("check point 1\n");

#endif

#if (FSL_OS_NONE != FSL_OS_SELECTED)
    do{
#endif
   
        if (iAP2_Host_stateMachine_Exit)
            break;

        //printf("iAP2_Host_vfnapHostDriver\n");
/* Run State Machine */
        iAP2_Host_vfnapHostDriver[iAP2_Host_SM.ActualState]();

        /* Verify if the device was detached only if it is at or beyond the init state */
        if(iAP2_Host_SM.ActualState >= IAP2_WAIT_LINK_INIT_STATE)//IAP2_PROTOCOL_INIT_STATE)
        {
            /* Check if the device has been disconnected */
#if !defined(IAPI_INTERFACE_USB_DEV) && !defined(IAPI_INTERFACE_USB_HOST)
            bUARTStatus = IAP2_HOST_CHECK_IF_UART_MODE;
            if(!(bUARTStatus & UART_ATTACH))
#else                                                                                           
                bUSBStatus = iAP_Transport_Status();
            if(!(bUSBStatus & USB_IOS_ATTACH))
#endif
            {

                iAP2_Host_bfnHostUnInitialization();

                /* Set main state machine to initial state */
#ifdef IAPI_INTERFACE_USB_ROLE_SWITCH
                iAP2_Host_SM.ActualState = IAP2_ROLE_SWITCH_STATE;

		    dprint("Check point 2\n");
		    //printf("check point 2\n");

                /* Return transport to USB Host and deinit USB Device */
                if(iAP2_Host_gwStatus & (1 << IAP2H_ROLE_CHANGED))
                {
                    /** @todo Function Return */
                    (void)USB_Composite_DeInit(0);
                    /** @todo Function Return */
                    (void)iAP_Transport_SelectTranspot(IAP_TRANSPORT_USB_HOST);
                    iAP_Transport_Init();
		    //printf("check point 5\n");
                }
#else
                iAP2_Host_SM.ActualState = IAP2_DEVICE_ATTACH_DET_STATE;
#endif

		    dprint("Check point 3\n");
		    //printf("check point 3\n");

                /* Configure transport callback to receive the protocol init response */
                iAP_Transport_SetRecvCallback(iAP2_Host_TransportRxCbk);
                iAP_Transport_SetSentCallback(NULL);

                iAP2_Host_gwInternalErrorFlags = 0;
                iAP2_Host_sLSP = iAP2_Host_csInitLSP;
                iAP2_Host_sLSP.spSessionData = iAP2_Host_sSessions;
            }
            else
            {
		    dprint("Check point 4\n");
		    //printf("check point 4\n");

                osa_time_delay(5);
            }
        }
#if (FSL_OS_NONE != FSL_OS_SELECTED)
    }while(true);
#endif

    /* Clear Internal Flags */
    iAP2_Host_gwStatus = 0;

    dprint("iAP2_Host_vfStateMachineDriver exiting\n");
}

/*************************************************************************************************
*
* @brief    Callback for transport Rx data. Only during initialization this callback will be active.
* 
* @param       void
* 
* @return      void
*          
**************************************************************************************************/
static void iAP2_Host_TransportRxCbk(uint8_t *pbData, uint16_t wDataSize)
{
    uint8_t bCount;

    dprint("iAP2_Host_TransportRxCbk start - initialization\n");
    //printf("iAP2_Host_TransportRxCbk start - initialization wDataSize %d\n", wDataSize);

    /* Loop to compare init response with init sequence. */
    bCount = 0;
    gs_iAP2HostReceiveCount += wDataSize;

    if(gs_iAP2HostReceiveCount > IAP2H_IAP1_INIT_SEQ_SIZE)
    {
        gs_iAP2HostReceiveCount = IAP2H_IAP1_INIT_SEQ_SIZE;
    }

    while((gs_iAP2HostInitCount < IAP2H_IAP1_INIT_SEQ_SIZE) && 
            (pbData[bCount] == iAP2_Host_cbaInitSeq[gs_iAP2HostInitCount]) &&
            (bCount < wDataSize))
    {
        bCount++;
        gs_iAP2HostInitCount++;
    }

    //printf("\ngs_iAP2HostReceiveCount %d\n", gs_iAP2HostReceiveCount);

    if(gs_iAP2HostReceiveCount == IAP2H_IAP1_INIT_SEQ_SIZE)
    {
        dprint("  IAP2H_IAP1_INIT_SEQ_SIZE size received\n");
        //printf("\ngs_iAP2HostInitCount %d\n", gs_iAP2HostInitCount); 
        if(gs_iAP2HostReceiveCount == gs_iAP2HostInitCount)
        {
            osa_mutex_lock(&gs_iAP2HostMutex);
            g_authenticationEventsFlags |=
                 (1 << IAP2H_IAP2_DETECTED) | (1 << IAP2H_INIT_COMPLETE);
            osa_mutex_unlock(&gs_iAP2HostMutex);
            osa_cond_signal(&gs_iAP2HostEvents);
            //printf("\n(1 << IAP2H_IAP2_DETECTED) | (1 << IAP2H_INIT_COMPLETE)\n"); 
        }
        else
        {
            osa_mutex_lock(&gs_iAP2HostMutex);
            g_authenticationEventsFlags |=
                 (1 << IAP2H_INIT_COMPLETE);
            osa_mutex_unlock(&gs_iAP2HostMutex);
            osa_cond_signal(&gs_iAP2HostEvents);
	   // printf("\n(1 << IAP2H_INIT_COMPLETE)\n"); 
        } 
    }

    iAP_Transport_DataReceive();
}

void iAP2_Host_LinkCtrlCbk(uint8_t bEvent)
{
    if(bEvent == IAP2_LINK_CTRL_SYN)
    {
        osa_mutex_lock(&gs_iAP2HostMutex);
        g_authenticationEventsFlags |=
             (1 << IAP2H_LSP_RECEIVED);
        osa_mutex_unlock(&gs_iAP2HostMutex);
        osa_cond_signal(&gs_iAP2HostEvents);
    }
}

/* TODO: fix this to not call both iAPv1 and iAPv2 callbacks. */
void iAP2_Host_bfnEventsCallback(uint8_t eventFlag)
{
    dprint("iAP2_Host_bfnEventsCallback start: %d\n", eventFlag);

    iAP_Host_bfnEventsCallback(eventFlag);

    osa_mutex_lock(&gs_iAP2CoprocessorMutex);
    g_authenticationEventsFlags |=
         (1 << eventFlag);
    osa_mutex_unlock(&gs_iAP2CoprocessorMutex);
    osa_cond_signal(&gs_iAP2CoprocessorEvents);
}

