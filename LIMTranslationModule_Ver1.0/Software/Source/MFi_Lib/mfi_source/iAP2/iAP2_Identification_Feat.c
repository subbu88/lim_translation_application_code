/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP_Config.h"
#include "iAP2_Identification_Feat.h"
#include "iAP2_Config.h"
#include "iAP2_Features.h"
#include "iAP2_Host.h"
#include "iAP2_Sessions_Control.h"
#include "osa_common.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define DATA_SIZE_NULL 										(0x00)
#define PARAM_HEADER_SIZE									(0X04)
#define MESSAGE_HEADER_SIZE									(0x06)
#define PARAM_GROUP_HEADER_SIZE         					(0x07 * PARAM_HEADER_SIZE)
#define NULL_PARAM_SIZE										(0x00)
#define IAP2_IDENTIFICATION_MAXIMUM_PARAMETER_INFO_UPDATE 	( 7)
#define IAP2_IDENTIFICATION_REJECTED_TOTAL_PARAM			(20)
#define iAP2_IDENTIFICATION_INFO_UPDATES_TOTAL_PARAM		( 7)
#define IAP2_IDENTIFICATION_INFO_UPDATE_MSG_SIZE			(20)
#define IAP2_IDENTIFICATION_UTF8_MAX_SIZE					(64)
#define IAP2_IDENTIFICATION_REJECTED_MESSAGE_BY_ACCESORY	(6)
#define IAP2_IDENTIFICATION_MAX_RECEIVED_MESSAGE			(4)

#define GET_U16_MS_BYTE(value)								((uint8_t)((value & 0xFF00)>>8))	
#define GET_U16_LS_BYTE(value)								((uint8_t)(value & 0x00FF))

#define END_PARAM_GROUP_SIGNATURE_BYTE						(0xFF)

#define END_PARAM_GROUP_SIGNATURE_TWOBYTES					((END_PARAM_GROUP_SIGNATURE_BYTE << 8) \
															| (END_PARAM_GROUP_SIGNATURE_BYTE))

#define END_PARAM_GROUP_SIGNATURE_FOURBYTES					((END_PARAM_GROUP_SIGNATURE_TWOBYTES << 16) \
															| (END_PARAM_GROUP_SIGNATURE_TWOBYTES))

#define END_PARAM_GROUP_SIGNATURE							{(uint8_t*)END_PARAM_GROUP_SIGNATURE_FOURBYTES, \
															(uint16_t)END_PARAM_GROUP_SIGNATURE_TWOBYTES, \
															(uint16_t)END_PARAM_GROUP_SIGNATURE_TWOBYTES, \
															(uint16_t)END_PARAM_GROUP_SIGNATURE_TWOBYTES}

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/
typedef struct
{
	uint8_t 	*bpParameterAddress;
	uint16_t *wParameterSize;
	uint16_t wParameterId;
	uint16_t wSubParameterId;
}IAP2_IDENT_INFO;


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static iAP2_Identification_RejectedInformation  RejectedInfo;
static iAP2_Identification_RejectedInformation  *RejectedInfoAppPtr;
static iAP2_Identification_InformationUpdateData InformationUpdates;

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*-----------------------------------------------------------------------------------------------*/
/* Messages sent by accessory | Parameter ID 0x06 */
/** @todo iAP_ID_Feat - EM: A macro could be created that receives the msg id value and splits it. 
 * It would make this array more readable and easier to maintain.
 */
static const uint8_t iAP2_Identification_baMsgSentByAcc[] = 
{
#if (IAP2_CTRL_MSG_ACCESSORY_AUTHENTICATION == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_AUTH_CERTIFICATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_AUTH_CERTIFICATE),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_AUTH_RESPONSE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_AUTH_RESPONSE),
#endif
#if (IAP2_CTRL_MSG_ACCESSORY_IDENTIFICATION == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_INFO),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_CANCELL), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_CANCELL),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_INFO_UPDATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_INFO_UPDATE),
#endif
#if (IAP2_CTRL_MSG_APP_LAUNCH == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQUEST_APP_LAUNCH), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQUEST_APP_LAUNCH),
#endif
#if (IAP2_CTRL_MSG_ASSISTIVE_TOUCH == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_ASSISTIVE_TOUCH), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_ASSISTIVE_TOUCH),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_ASSISTIVE_TOUCH), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_ASSISTIVE_TOUCH),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_ASSISTIVE_TOUCH_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_ASSISTIVE_TOUCH_INFO),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_ASSISTIVE_TOUCH_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_ASSISTIVE_TOUCH_INFO),
#endif
#if (IAP2_CTRL_MSG_BLUETOOTH_STATUS == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_BLUETOOTH_COMPONENT_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_BLUETOOTH_COMPONENT_INFO),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_BLUETOOTH_CONN_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_BLUETOOTH_CONN_UPDATES),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_BLUETOOTH_CONN_UPDATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_BLUETOOTH_CONN_UPDATE),
#endif
#if (IAP2_CTRL_MSG_DEVICE_AUTHENTICATION == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_DEV_AUTH_CERTIFICATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_DEV_AUTH_CERTIFICATE),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_DEV_AUTH_CHALLENGE_RESPONSE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_DEV_AUTH_CHALLENGE_RESPONSE),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_DEV_AUTH_FAILED),
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_DEV_AUTH_FAILED),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_DEV_AUTH_SUCCEEDED), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_DEV_AUTH_SUCCEEDED),
#endif
#if (IAP2_CTRL_MSG_HUMAN_INTERFACE_DEVICE == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_HID), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_HID),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_ACC_HID_REPORT), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_ACC_HID_REPORT),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_HID), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_HID),
#endif
#if (IAP2_CTRL_MSG_LOCATION == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_LOCATION_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_LOCATION_INFO),
#endif
#if (IAP2_CTRL_MSG_MEDIA_LIBRARY_ACCESS == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_MEDIA_LIBRARY_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_MEDIA_LIBRARY_INFO),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_MEDIA_LIBRARY_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_MEDIA_LIBRARY_INFO),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_MEDIA_LIBRARY_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_MEDIA_LIBRARY_UPDATES),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_MEDIA_LIBRARY_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_MEDIA_LIBRARY_UPDATES),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_CURRENT_SEL), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_CURRENT_SEL),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_ITEMS), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_ITEMS),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_COLLECTION), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_PLAY_MEDIA_LIBRARY_COLLECTION),
#endif
#if (IAP2_CTRL_MSG_NOW_PLAYING == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_NOW_PLAYING_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_NOW_PLAYING_UPDATES),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_NOW_PLAYING_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_NOW_PLAYING_UPDATES),
#endif
#if (IAP2_CTRL_MSG_POWER == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_POWER_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_POWER_UPDATES),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_POWER_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_POWER_UPDATES),
#if IAP2_IDENT_PWR_SOURCE_TYPE_DATA == IAP2_PWR_TYPE_SELF
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_POWER_SOURCE_UPDATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_POWER_SOURCE_UPDATE),
#endif
#endif
#if (IAP2_CTRL_MSG_USB_DEVICE_MODE_AUDIO == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_USB_DEV_MODE_AUDIO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_USB_DEV_MODE_AUDIO),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_USB_DEV_MODE_AUDIO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_USB_DEV_MODE_AUDIO),
#endif
#if (IAP2_CTRL_MSG_VOICE_OVER == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_VOICE_OVER), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_VOICE_OVER),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_VOICE_OVER), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_VOICE_OVER),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_MOVE_CURSOR), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_MOVE_CURSOR),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_ACTIVATE_CURSOR), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_ACTIVATE_CURSOR),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_SCROLL_PAGE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_SCROLL_PAGE),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_SPEAK_TEXT), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_SPEAK_TEXT),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_PAUSE_TEXT), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_PAUSE_TEXT),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_RESUME_TEXT), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_RESUME_TEXT),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_VOICE_OVER_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_VOICE_OVER_UPDATES),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_VOICE_OVER_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_VOICE_OVER_UPDATES),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_CONFIG), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_VOICE_OVER_CONFIG),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_VOICE_OVER_CURSOR_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_VOICE_OVER_CURSOR_UPDATES),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_VOICE_OVER_CURSOR_UPDATES), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_VOICE_OVER_CURSOR_UPDATES),
#endif
#if (IAP2_CTRL_MSG_WIFI_INFO_SHARING == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_WIFI_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_WIFI_INFO),
#endif
#if (IAP2_CTRL_MSG_ACCESSORY_ANY_MESSAGE == _FALSE_)
                0x00,
#endif
};

static const uint16_t iAP2_Identification_wMsgSentByAccSize = sizeof(iAP2_Identification_baMsgSentByAcc);

/*-----------------------------------------------------------------------------------------------*/
/* Messages received from device | Parameter ID 0x07 */
static const uint8_t iAP2_Identification_baMsgReceivedFromDev[] = 
{
#if (IAP2_CTRL_MSG_ACCESSORY_AUTHENTICATION == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_AUTH_CERTIFICATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_AUTH_CERTIFICATE),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_REQ_AUTH_CHALLENGE_RESPONSE),
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_REQ_AUTH_CHALLENGE_RESPONSE),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_AUTH_FAILED), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_AUTH_FAILED),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_AUTH_SUCCEEDED), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_AUTH_SUCCEEDED),
#endif
#if (IAP2_CTRL_MSG_ACCESSORY_IDENTIFICATION == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_IDENTIFICATION), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_IDENTIFICATION),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_ACCEPTED), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_ACCEPTED),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_REJECTED), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_IDENTIFICATION_REJECTED),
#endif
#if (IAP2_CTRL_MSG_ASSISTIVE_TOUCH == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_ASSISTIVE_TOUCH_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_ASSISTIVE_TOUCH_INFO),
#endif
#if (IAP2_CTRL_MSG_BLUETOOTH_STATUS == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_BLUETOOTH_CONN_UPDATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_BLUETOOTH_CONN_UPDATE),
#endif
#if (IAP2_CTRL_MSG_DEVICE_AUTHENTICATION == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_DEV_AUTH_CERTIFICATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_DEV_AUTH_CERTIFICATE),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_DEV_AUTH_RESPONSE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_DEV_AUTH_RESPONSE),
#endif
#if (IAP2_CTRL_MSG_EXT_ACC_PROTOCOL == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_EXT_ACC_PROTOCOL_SESSION), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_EXT_ACC_PROTOCOL_SESSION),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_EXT_ACC_PROTOCOL_SESSION), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_EXT_ACC_PROTOCOL_SESSION),
#endif
#if (IAP2_CTRL_MSG_HUMAN_INTERFACE_DEVICE == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_DEVICE_HID_REPORT), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_DEVICE_HID_REPORT),
#endif
#if (IAP2_CTRL_MSG_LOCATION == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_START_LOCATION_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_START_LOCATION_INFO),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_STOP_LOCATION_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_STOP_LOCATION_INFO),
#endif
#if (IAP2_CTRL_MSG_MEDIA_LIBRARY_ACCESS == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_MEDIA_LIBRARY_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_MEDIA_LIBRARY_INFO),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_MEDIA_LIBRARY_UPDATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_MEDIA_LIBRARY_UPDATE),
#endif
#if (IAP2_CTRL_MSG_NOW_PLAYING == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_NOW_PLAYING_UPDATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_NOW_PLAYING_UPDATE),
#endif
#if (IAP2_CTRL_MSG_POWER == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_POWER_UPDATE),
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_POWER_UPDATE),
#endif
#if (IAP2_CTRL_MSG_USB_DEVICE_MODE_AUDIO == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_USB_DEV_MODE_AUDIO_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_USB_DEV_MODE_AUDIO_INFO),
#endif
#if (IAP2_CTRL_MSG_VOICE_OVER == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_VOICE_OVER_UPDATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_VOICE_OVER_UPDATE),
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_VOICE_OVER_CURSOR_UPDATE), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_VOICE_OVER_CURSOR_UPDATE),
#endif
#if (IAP2_CTRL_MSG_WIFI_INFO_SHARING == _TRUE_)
		GET_U16_MS_BYTE(IAP2_CTRL_MSG_WIFI_INFO), 
		GET_U16_LS_BYTE(IAP2_CTRL_MSG_WIFI_INFO),
#endif
#if (IAP2_CTRL_MSG_ACCESSORY_ANY_MESSAGE == _FALSE_)
                0x00,
#endif
};

static const uint16_t iAP2_Identification_wMsgReceivedFromDevSize = sizeof(iAP2_Identification_baMsgReceivedFromDev);

/*-----------------------------------------------------------------------------------------------*/
/* Power Source Type | Parameter ID 0x08 */
static const uint8_t iAP2_Identification_bPwrSrcType = (uint8_t)IAP2_IDENT_PWR_SOURCE_TYPE_DATA;
static const uint16_t iAP2_Identification_wPwrSrcTypeSize = sizeof(iAP2_Identification_bPwrSrcType);
/*-----------------------------------------------------------------------------------------------*/
/* Maximum Current Drawn From Device | Parameter ID 0x09 */
static const uint8_t iAP2_Identification_baMaxCurrentDrawn[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_MAX_CURRENT_DRAWN_DATA),
		GET_U16_LS_BYTE(IAP2_IDENT_MAX_CURRENT_DRAWN_DATA),
};

static const uint16_t iAP2_Identification_wMaxCurrentDrawnSize = sizeof(iAP2_Identification_baMaxCurrentDrawn);

/*-----------------------------------------------------------------------------------------------*/
/* Current Language | Parameter ID 0x0C */
static const uint8_t iAP2_Identification_baCurrentLanguage[] = IAP2_IDENT_CURRENT_LANGUAGE_DATA;
static const uint16_t iAP2_Identification_wCurrentLanguageSize = sizeof(iAP2_Identification_baCurrentLanguage);
/*-----------------------------------------------------------------------------------------------*/
/* Supported Language | Parameter ID 0x0D */
static const uint8_t iAP2_Identification_baSupportedLanguage0[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA0;
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 1)
static const uint8_t iAP2_Identification_baSupportedLanguage1[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA1;
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 2)
static const uint8_t iAP2_Identification_baSupportedLanguage2[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA2;
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 3)
static const uint8_t iAP2_Identification_baSupportedLanguage3[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA3;
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 4)
static const uint8_t iAP2_Identification_baSupportedLanguage4[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA4;
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 5)
static const uint8_t iAP2_Identification_baSupportedLanguage5[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA5;
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 6)
static const uint8_t iAP2_Identification_baSupportedLanguage6[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA6;
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 7)
static const uint8_t iAP2_Identification_baSupportedLanguage7[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA7;
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 8)
static const uint8_t iAP2_Identification_baSupportedLanguage8[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA8;
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 9)
static const uint8_t iAP2_Identification_baSupportedLanguage9[] = IAP2_IDENT_SUPPORTED_LANGUAGES_DATA9;
#endif
/*-----------------------------------------------------------------------------------------------*/
/* Serial Transport Component | Parameter ID 0x0E */
#if (IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT == _TRUE_)
static const uint8_t iAP2_Identification_baSerialTransCompId[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT_ID_DATA),
		GET_U16_LS_BYTE(IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT_ID_DATA),
};
static const uint16_t iAP2_Identification_wSerialTransCompIdSize = sizeof(iAP2_Identification_baSerialTransCompId);

static const uint8_t iAP2_Identification_baSerialTransCompName[] = IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT_NAME_DATA;
static const uint16_t iAP2_Identification_wSerialTransCompNameSize = sizeof(iAP2_Identification_baSerialTransCompName);
#endif 
/*-----------------------------------------------------------------------------------------------*/
/* USB Device Transport Component | Parameter ID 0x0F */
#if (IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT == _TRUE_)
static const uint8_t iAP2_Identification_baUsbDevTransCompId[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_ID_DATA),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_ID_DATA),
};
static const uint16_t iAP2_Identification_wUsbDevTransCompIdSize = sizeof(iAP2_Identification_baUsbDevTransCompId);

static const uint8_t iAP2_Identification_baUsbDevTransCompName[] = IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_NAME_DATA;
static const uint16_t iAP2_Identification_wUsbDevTransCompNameSize = sizeof(iAP2_Identification_baUsbDevTransCompName);
/*WARNING: no data for USB Dev transport support iAP2 need to add in sending structure */
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_8000HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp8000HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR0; 
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_11025HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp11025HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR1; 
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_12000HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp12000HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR2; 
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_16000HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp16000HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR3; 
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_22050HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp22050HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR4; 
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_24000HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp24000HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR5; 
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_32000HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp32000HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR6; 
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_44100HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp44100HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR7; 
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_48000HZ == _TRUE_)
static const uint8_t iAP2_Identification_bUsbDevTransComp48000HZ = IAP2_IDENT_USB_DEV_SUPPORTED_AUDIO_SR8; 
#endif

static const uint16_t iAP2_Identification_wUsbDevTransCompSize = sizeof(uint8_t);

#endif
/*-----------------------------------------------------------------------------------------------*/
/* USB Host Transport Component | Parameter ID 0x10 */
#if (IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT == _TRUE_)
static const uint8_t iAP2_Identification_baUsbHostTransCompId[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT_ID_DATA),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT_ID_DATA),
		//GET_U16_MS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA0),
		//GET_U16_LS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA0),
		
};

static const uint16_t iAP2_Identification_wUsbHostTransCompIdSize = sizeof(iAP2_Identification_baUsbHostTransCompId);

static const uint8_t iAP2_Identification_baUsbHostTransCompName[] = IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT_NAME_DATA;
static const uint16_t iAP2_Identification_wUsbHostTransCompNameSize = sizeof(iAP2_Identification_baUsbHostTransCompName);
/*WARNING: no data for USB Dev transport support iAP2 need to add in sending structure */
#endif
/*-----------------------------------------------------------------------------------------------*/
/* Bluetooth Transport Component | Parameter ID 0x11 */
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT == _TRUE_)
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 0)

static const uint8_t iAP2_Identification_baBluethoothTransCompId0[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA0),
		GET_U16_LS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA0),
};
static const uint16_t iAP2_Identification_wBluethoothTransCompIdSize = sizeof(iAP2_Identification_baBluethoothTransCompId0);

static const uint8_t iAP2_Identification_baBluethoothTransCompName0[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA0;
static const uint16_t iAP2_Identification_wBluethoothTransCompName0Size = sizeof(iAP2_Identification_baBluethoothTransCompName0);
/*WARNING: no data for USB Dev transport support iAP2 need to add in sending structure */
static const uint8_t iAP2_Identification_baBluethoothTransCompMAC0[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA0;
static const uint16_t iAP2_Identification_wBluethoothTransCompMACSize = sizeof(iAP2_Identification_baBluethoothTransCompMAC0);

#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 1)

static const uint8_t iAP2_Identification_baBluethoothTransCompId1[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA1),
		GET_U16_LS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA1),
};

static const uint8_t iAP2_Identification_baBluethoothTransCompName1[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA1;
static const uint16_t iAP2_Identification_wBluethoothTransCompName1Size = sizeof(iAP2_Identification_baBluethoothTransCompName1);

/*WARNING: no data for USB Dev transport support iAP2 need to add in sending structure */
static const uint8_t iAP2_Identification_baBluethoothTransCompMAC1[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA1;
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 2)

static const uint8_t iAP2_Identification_baBluethoothTransCompId2[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA2),
		GET_U16_LS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA2),
};

static const uint8_t iAP2_Identification_baBluethoothTransCompName2[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA2;
static const uint16_t iAP2_Identification_wBluethoothTransCompName2Size = sizeof(iAP2_Identification_baBluethoothTransCompName2);

/*WARNING: no data for USB Dev transport support iAP2 need to add in sending structure */
static const uint8_t iAP2_Identification_baBluethoothTransCompMAC2[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA2;
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 3)

static const uint8_t iAP2_Identification_baBluethoothTransCompId3[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA3),
		GET_U16_LS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA3),
};

static const uint8_t iAP2_Identification_baBluethoothTransCompName3[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA3;
static const uint16_t iAP2_Identification_wBluethoothTransCompName3Size = sizeof(iAP2_Identification_baBluethoothTransCompName3);

/*WARNING: no data for USB Dev transport support iAP2 need to add in sending structure */
static const uint8_t iAP2_Identification_baBluethoothTransCompMAC3[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA3;
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 4)

static const uint8_t iAP2_Identification_baBluethoothTransCompId4[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA4),
		GET_U16_LS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA4),
};

static const uint8_t iAP2_Identification_baBluethoothTransCompName4[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA4;
static const uint16_t iAP2_Identification_wBluethoothTransCompName4Size = sizeof(iAP2_Identification_baBluethoothTransCompName4);

/*WARNING: no data for USB Dev transport support iAP2 need to add in sending structure */
static const uint8_t iAP2_Identification_baBluethoothTransCompMAC4[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA4;
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 5)

static const uint8_t iAP2_Identification_baBluethoothTransCompId5[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA5),
		GET_U16_LS_BYTE(IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_ID_DATA5),
};

static const uint8_t iAP2_Identification_baBluethoothTransCompName5[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_NAME_DATA5;
static const uint16_t iAP2_Identification_wBluethoothTransCompName5Size = sizeof(iAP2_Identification_baBluethoothTransCompName5);

/*WARNING: no data for USB Dev transport support iAP2 need to add in sending structure */
static const uint8_t iAP2_Identification_baBluethoothTransCompMAC5[] = IAP2_IDENT_BLUETOOTH_TRANSPORT_MAC_ADDRESS_DATA5;
#endif
#endif
/*-----------------------------------------------------------------------------------------------*/
/* iAP HID Component | Parameter ID 0x12 */
#if (IAP2_IDENT_HID_COMPONENT == _TRUE_)
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 0)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHIDComponentId0[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA0),
		GET_U16_LS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA0),
};
static const uint16_t iAP2_Identification_wHIDComponentIdSize0 = sizeof(iAP2_Identification_baHIDComponentId0);

/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHIDComponentName0[] = IAP2_IDENT_HID_COMPONENT_NAME_DATA0;
static const uint16_t iAP2_Identification_wHIDComponentName0Size = sizeof(iAP2_Identification_baHIDComponentName0);
/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHIDComponentFunction0 = IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA0;
static const uint16_t iAP2_Identification_wHIDComponentFunctionSize0 = sizeof(iAP2_Identification_baHIDComponentFunction0);

#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 1)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHIDComponentId1[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA1),
		GET_U16_LS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA1),
};
static const uint16_t iAP2_Identification_wHIDComponentIdSize1 = sizeof(iAP2_Identification_baHIDComponentId1);
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHIDComponentName1[] = IAP2_IDENT_HID_COMPONENT_NAME_DATA1;
static const uint16_t iAP2_Identification_wHIDComponentName1Size = sizeof(iAP2_Identification_baHIDComponentName1);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHIDComponentFunction1 = IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA1;
static const uint16_t iAP2_Identification_wHIDComponentFunctionSize1 = sizeof(iAP2_Identification_baHIDComponentFunction1);
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 2)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHIDComponentId2[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA2),
		GET_U16_LS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA2),
};
static const uint16_t iAP2_Identification_wHIDComponentIdSize2 = sizeof(iAP2_Identification_baHIDComponentId2);
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHIDComponentName2[] = IAP2_IDENT_HID_COMPONENT_NAME_DATA2;
static const uint16_t iAP2_Identification_wHIDComponentName2Size = sizeof(iAP2_Identification_baHIDComponentName2);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHIDComponentFunction2 = IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA2;
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 3)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHIDComponentId3[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA3),
		GET_U16_LS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA3),
};
static const uint16_t iAP2_Identification_wHIDComponentIdSize3 = sizeof(iAP2_Identification_baHIDComponentId3);
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHIDComponentName3[] = IAP2_IDENT_HID_COMPONENT_NAME_DATA3;
static const uint16_t iAP2_Identification_wHIDComponentName3Size = sizeof(iAP2_Identification_baHIDComponentName3);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHIDComponentFunction3 = IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA3;
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 4)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHIDComponentId4[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA4),
		GET_U16_LS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA4),
};
static const uint16_t iAP2_Identification_wHIDComponentIdSize4 = sizeof(iAP2_Identification_baHIDComponentId4);
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHIDComponentName4[] = IAP2_IDENT_HID_COMPONENT_NAME_DATA4;
static const uint16_t iAP2_Identification_wHIDComponentName4Size = sizeof(iAP2_Identification_baHIDComponentName4);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHIDComponentFunction4 = IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA4;
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 5)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHIDComponentId5[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA5),
		GET_U16_LS_BYTE(IAP2_IDENT_HID_COMPONENT_ID_DATA5),
};
static const uint16_t iAP2_Identification_wHIDComponentIdSize5 = sizeof(iAP2_Identification_baHIDComponentId5);
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHIDComponentName5[] = IAP2_IDENT_HID_COMPONENT_NAME_DATA5;
static const uint16_t iAP2_Identification_wHIDComponentName5Size = sizeof(iAP2_Identification_baHIDComponentName5);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHIDComponentFunction5 = IAP2_IDENT_HID_COMPONENT_FUNCTION_DATA5;
#endif
#endif
/*-----------------------------------------------------------------------------------------------*/
/* USB Host HID Component */
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT == _TRUE_)
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 0)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDComponentId0[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA0),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA0),
};
static const uint16_t iAP2_Identification_wHostHIDComponentIdSize = sizeof(iAP2_Identification_baHostHIDComponentId0);
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHostHIDComponentName0[] = IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA0;
static const uint16_t iAP2_Identification_wHostHIDComponentName0Size = sizeof(iAP2_Identification_baHostHIDComponentName0);
/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHostHIDComponentFunction0 = IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA0;
static const uint16_t iAP2_Identification_wHostHIDComponentFunctionSize = sizeof(iAP2_Identification_baHostHIDComponentFunction0);
/*USBHostTransportComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDTransComponentId0[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA0),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA0),
};
static const uint16_t iAP2_Identification_wHostHIDTransComponentIdSize = sizeof(iAP2_Identification_baHostHIDTransComponentId0);

/*USBHostTransportInterfaceNumber*/
static const uint8_t iAP2_Identification_baHostHIDTransportInterface0 = IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA0;
static const uint16_t iAP2_Identification_wHostHIDTransportInterfaceSize = sizeof(iAP2_Identification_baHostHIDTransportInterface0);
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 1)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDComponentId1[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA1),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA1),
};
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHostHIDComponentName1[] = IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA1;
static const uint16_t iAP2_Identification_wHostHIDComponentName1Size = sizeof(iAP2_Identification_baHostHIDComponentName1);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHostHIDComponentFunction1 = IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA1;
/*USBHostTransportComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDTransComponentId1[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA1),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA1),
};
/*USBHostTransportInterfaceNumber*/
static const uint8_t iAP2_Identification_baHostHIDTransportInterface1 = IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA1;
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 2)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDComponentId2[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA2),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA2),
};
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHostHIDComponentName2[] = IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA2;
static const uint16_t iAP2_Identification_wHostHIDComponentName2Size = sizeof(iAP2_Identification_baHostHIDComponentName2);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHostHIDComponentFunction2 = IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA2;
/*USBHostTransportComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDTransComponentId2[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA2),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA2),
};
/*USBHostTransportInterfaceNumber*/
static const uint8_t iAP2_Identification_baHostHIDTransportInterface2 = IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA2;
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 3)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDComponentId3[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA3),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA3),
};
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHostHIDComponentName3[] = IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA3;
static const uint16_t iAP2_Identification_wHostHIDComponentName3Size = sizeof(iAP2_Identification_baHostHIDComponentName3);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHostHIDComponentFunction3 = IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA3;
/*USBHostTransportComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDTransComponentId3[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA3),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA3),
};
/*USBHostTransportInterfaceNumber*/
static const uint8_t iAP2_Identification_baHostHIDTransportInterface3 = IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA3;
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 4)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDComponentId4[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA4),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA4),
};
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHostHIDComponentName4[] = IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA4;
static const uint16_t iAP2_Identification_wHostHIDComponentName4Size = sizeof(iAP2_Identification_baHostHIDComponentName4);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHostHIDComponentFunction4 = IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA4;
/*USBHostTransportComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDTransComponentId4[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA4),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA4),
};
/*USBHostTransportInterfaceNumber*/
static const uint8_t iAP2_Identification_baHostHIDTransportInterface4 = IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA4;
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 5)
/*HIDComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDComponentId5[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA5),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_ID_DATA5),
};
/*HIDComponentName*/
static const uint8_t iAP2_Identification_baHostHIDComponentName5[] = IAP2_IDENT_USB_HOST_HID_COMPONENT_NAME_DATA5;
static const uint16_t iAP2_Identification_wHostHIDComponentName5Size = sizeof(iAP2_Identification_baHostHIDComponentName5);

/*HIDComponentFunction*/
static const uint8_t iAP2_Identification_baHostHIDComponentFunction5 = IAP2_IDENT_USB_HOST_HID_COMPONENT_FUNCTION_DATA5;
/*USBHostTransportComponentIdentifier*/
static const uint8_t iAP2_Identification_baHostHIDTransComponentId5[] = 
{
		GET_U16_MS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA5),
		GET_U16_LS_BYTE(IAP2_IDENT_USB_HOST_HID_COMPONENT_TRASPORT_ID_DATA5),
};
/*USBHostTransportInterfaceNumber*/
static const uint8_t iAP2_Identification_baHostHIDTransportInterface5 = IAP2_IDENT_USB_HOST_HID_TRANSPORT_INTERFACE_DATA5;
#endif
#endif

static const uint16_t iAP2_Identification_wNullSize = NULL_PARAM_SIZE;

static const IAP2_IDENT_INFO iAP2_Identification_sSendInfo[] = 
{
	{(uint8_t*)&iAP2_Identification_baName, 					/* Name */
	(uint16_t*)&iAP2_Identification_wNameSize,
	(uint16_t)NAME_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iAP2_Identification_baModel,					/* Model identifier */
	(uint16_t*)&iAP2_Identification_wModelSize,
	MODEL_IDENTIFIER_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iAP2_Identification_baManufacturer,			/* Manufacturer */
	(uint16_t*)&iAP2_Identification_wManufacturerSize,
	MANUFACTURER_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iA2_Identification_baSerialNum,				/* Serial Number */
	(uint16_t*)&iA2_Identification_wSerialNumSize,
	SERIAL_NUMBER_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iAP2_Identification_baFwVersion,				/* Firmware Version */
	(uint16_t*)&iAP2_Identification_wFwVersionSize,
	FIRMWARE_VERSION_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iAP2_Identification_baHwVersion,				/* Hardware Version */
	(uint16_t*)&iAP2_Identification_wHwVersionSize,
	HARDWARE_VERSION_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iAP2_Identification_baMsgSentByAcc,			/* Messages Sent By Accessory */
	(uint16_t*)&iAP2_Identification_wMsgSentByAccSize,
	MESSAGES_SENT_BY_ACC_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iAP2_Identification_baMsgReceivedFromDev,	/* Messages received by device */
	(uint16_t*)&iAP2_Identification_wMsgReceivedFromDevSize,
	MESSAGES_RECEIVED_FROM_DEV_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iAP2_Identification_bPwrSrcType,				/* Power Source Type */
	(uint16_t*)&iAP2_Identification_wPwrSrcTypeSize,
	POWER_SOURCE_TYPE_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
	{(uint8_t*)&iAP2_Identification_baMaxCurrentDrawn,		/* Maximum current drawn from device */
	(uint16_t*)&iAP2_Identification_wMaxCurrentDrawnSize,
	MAX_CURRENT_DRAWN_FROM_DEV_PARAM,
	IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL == _TRUE_)       /* Supported External Accessory Protocol */
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 0)   
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtID0,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtName0,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtName0Size,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_bSupportedExtAccProtMatch0,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtMatchSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_MATCH_ACTION_SUBPARAM},
//#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID0 == _TRUE_)
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtNativeTransID0,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtNativeTransIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_SUBPARAM},
//#endif
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 1)
    END_PARAM_GROUP_SIGNATURE,
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtID1,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtName1,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtName1Size,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_bSupportedExtAccProtMatch1,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtMatchSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_MATCH_ACTION_SUBPARAM},
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID1 == _TRUE_)
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtNativeTransID1,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtNativeTransIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_SUBPARAM},
#endif
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 2)
    END_PARAM_GROUP_SIGNATURE,
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtID2,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtName2,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtName2Size,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_bSupportedExtAccProtMatch2,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtMatchSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_MATCH_ACTION_SUBPARAM},
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID2 == _TRUE_)
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtNativeTransID2,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtNativeTransIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_SUBPARAM},
#endif
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 3)
    END_PARAM_GROUP_SIGNATURE,
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtID3,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtName3,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtName3Size,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_bSupportedExtAccProtMatch3,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtMatchSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_MATCH_ACTION_SUBPARAM},
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID3 == _TRUE_)
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtNativeTransID3,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtNativeTransIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_SUBPARAM},
#endif
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 4)
    END_PARAM_GROUP_SIGNATURE,
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtID4,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtName4,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtName4Size,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_bSupportedExtAccProtMatch4,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtMatchSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_MATCH_ACTION_SUBPARAM},
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID4 == _TRUE_)
    {(uint8_t*)&iAP2_Identification_baSupportedExtAccProtNativeTransID4,
    (uint16_t*)&iAP2_Identification_wSupportedExtAccProtNativeTransIDSize,
    SUPPORTED_EXTERNAL_ACC_PROTOCOL_PARAM,
    EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_SUBPARAM},
#endif
#endif
#endif
#if (IAP2_IDENT_PREF_APP_BUNDLE_SEED_ID == _TRUE_)      /* Preferred App Bundle Seed ID */
    {(uint8_t*)&iAP2_Identification_baPrefAppBundleSeedId,
    (uint16_t*)&iAP2_Identification_wPrefAppBundleSeedIdSize,
    PREFERRED_APP_BUNDLE_SEED_ID_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
    {(uint8_t*)&iAP2_Identification_baCurrentLanguage,       /* Current language */
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    CURRENT_LANGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage0,    /* Supported languages */
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 1)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage1,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 2)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage2,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 3)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage3,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 4)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage4,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 5)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage5,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 6)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage6,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 7)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage7,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 8)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage8,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if(IAP2_IDENT_NUMBER_OF_SUPPORTED_LAGUAGES > 9)
    {(uint8_t*)&iAP2_Identification_baSupportedLanguage9,
    (uint16_t*)&iAP2_Identification_wCurrentLanguageSize,
    SUPPORTED_LAGUAGE_PARAM,
    IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM},
#endif
#if (IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT == _TRUE_)       /* Serial transport component */
    {(uint8_t*)&iAP2_Identification_baSerialTransCompId,
    (uint16_t*)&iAP2_Identification_wSerialTransCompIdSize,
    SERIAL_TRANSPORT_COMPONENT_PARAM,
    TRANSPORT_SERIAL_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baSerialTransCompName,   
    (uint16_t*)&iAP2_Identification_wSerialTransCompNameSize,
    SERIAL_TRANSPORT_COMPONENT_PARAM,
    TRANSPORT_SERIAL_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_SERIAL_TRANSPORT_SUPPORTS_IAP2 == _TRUE_)
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    SERIAL_TRANSPORT_COMPONENT_PARAM,
    TRANSPORT_SERIAL_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif
#endif
#if (IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT == _TRUE_)  /* USB device transport component */
    {(uint8_t*)&iAP2_Identification_baUsbDevTransCompId,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompIdSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_TRANSPORT_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baUsbDevTransCompName,   
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompNameSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_TRANSPORT_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_USB_DEV_TRANSPORT_SUPPORTS_IAP2 == _TRUE_)
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_8000HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp8000HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_11025HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp11025HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif  
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_12000HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp12000HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_16000HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp16000HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_22050HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp22050HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_24000HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp24000HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_32000HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp32000HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_44100HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp44100HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif
#if(IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_48000HZ == _TRUE_)
    {(uint8_t*)&iAP2_Identification_bUsbDevTransComp48000HZ,
    (uint16_t*)&iAP2_Identification_wUsbDevTransCompSize,
    USB_DEV_TRANSPORT_COMPONENT_PARAM,
    USB_DEV_SUPPORTED_AUDIO_SAMPLE_RATE_SUBPARAM},
#endif
#endif
#if (IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT == _TRUE_) /* USB Host transport component */
    {(uint8_t*)&iAP2_Identification_baUsbHostTransCompId,
    (uint16_t*)&iAP2_Identification_wUsbHostTransCompIdSize,
    USB_HOST_TRANSPORT_COMPNENT_PARAM,
    USB_HOST_TRANSPORT_COMPONENT_ID_SUBPARAM},  
    {(uint8_t*)&iAP2_Identification_baUsbHostTransCompName,
    (uint16_t*)&iAP2_Identification_wUsbHostTransCompNameSize,
    USB_HOST_TRANSPORT_COMPNENT_PARAM,
    USB_HOST_TRANSPORT_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_USB_HOST_TRANSPORT_SUPPORTS_IAP2)
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    USB_HOST_TRANSPORT_COMPNENT_PARAM,
    USB_HOST_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT == _TRUE_)    /* Bluetooth transport component */
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 0)
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompId0,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompIdSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompName0,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompName0Size,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP20 == _TRUE_)   
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif  
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompMAC0,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompMACSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_MAC_ADDRESS_SUBPARAM},
#endif  
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 1)
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompId1,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompIdSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompName1,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompName1Size,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP21 == _TRUE_)   
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif  
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompMAC1,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompMACSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_MAC_ADDRESS_SUBPARAM},
#endif
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 2)
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompId2,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompIdSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompName2,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompName2Size,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP22 == _TRUE_)   
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    BLUETOOTH_TRANSPORT_COMPONENT,
    BLUETOOTH_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif  
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompMAC2,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompMACSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_MAC_ADDRESS_SUBPARAM},
#endif  
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 3)
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompId3,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompIdSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompName3,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompName3Size,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP23 == _TRUE_)   
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif  
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompMAC3,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompMACSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_MAC_ADDRESS_SUBPARAM},
#endif  
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 4)
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompId4,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompIdSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompName4,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompName4Size,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP24 == _TRUE_)   
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif  
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompMAC4,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompMACSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_MAC_ADDRESS_SUBPARAM},
#endif  
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT_INSTANCES > 5)
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompId5,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompIdSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompName5,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompName5Size,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_NAME_SUBPARAM},
#if (IAP2_IDENT_BLUETOOTH_TRANSPORT_SUPPORTS_IAP25 == _TRUE_)   
    {NULL,
    (uint16_t*)&iAP2_Identification_wNullSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_COMPONENT_SUPPORTS_IAP2_SUBPARAM},
#endif  
    {(uint8_t*)&iAP2_Identification_baBluethoothTransCompMAC5,
    (uint16_t*)&iAP2_Identification_wBluethoothTransCompMACSize,
    BLUETOOTH_TRANSPORT_COMPONENT_PARAM,
    BLUETOOTH_TRANSPORT_MAC_ADDRESS_SUBPARAM},
#endif  
#endif
#if (IAP2_IDENT_HID_COMPONENT == _TRUE_)                    /* iAP HID component */
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 0)    
    {(uint8_t*)&iAP2_Identification_baHIDComponentId0,
    (uint16_t*)&iAP2_Identification_wHIDComponentIdSize0,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentName0,
    (uint16_t*)&iAP2_Identification_wHIDComponentName0Size,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentFunction0,
    (uint16_t*)&iAP2_Identification_wHIDComponentFunctionSize0,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_FUNCTION_SUBPARAM},
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 1)    
    {(uint8_t*)&iAP2_Identification_baHIDComponentId1,
    (uint16_t*)&iAP2_Identification_wHIDComponentIdSize1,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentName1,
    (uint16_t*)&iAP2_Identification_wHIDComponentName1Size,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentFunction1,
    (uint16_t*)&iAP2_Identification_wHIDComponentFunctionSize1,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_FUNCTION_SUBPARAM},
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 2)    
    {(uint8_t*)&iAP2_Identification_baHIDComponentId2,
    (uint16_t*)&iAP2_Identification_wHIDComponentIdSize2,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentName2,
    (uint16_t*)&iAP2_Identification_wHIDComponentName2Size,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentFunction2,
    (uint16_t*)&iAP2_Identification_wHIDComponentFunctionSize,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_FUNCTION_SUBPARAM},
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 3)    
    {(uint8_t*)&iAP2_Identification_baHIDComponentId3,
    (uint16_t*)&iAP2_Identification_wHIDComponentIdSize3,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentName3,
    (uint16_t*)&iAP2_Identification_wHIDComponentName3Size,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentFunction3,
    (uint16_t*)&iAP2_Identification_wHIDComponentFunctionSize,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_FUNCTION_SUBPARAM},
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 4)    
    {(uint8_t*)&iAP2_Identification_baHIDComponentId4,
    (uint16_t*)&iAP2_Identification_wHIDComponentIdSize4,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentName4,
    (uint16_t*)&iAP2_Identification_wHIDComponentName4Size,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentFunction4,
    (uint16_t*)&iAP2_Identification_wHIDComponentFunctionSize,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_FUNCTION_SUBPARAM},
#endif
#if (IAP2_IDENT_HID_COMPONENT_INSTANCES > 5)    
    {(uint8_t*)&iAP2_Identification_baHIDComponentId5,
    (uint16_t*)&iAP2_Identification_wHIDComponentIdSize5,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentName5,
    (uint16_t*)&iAP2_Identification_wHIDComponentName5Size,
    IAP_HID_COMPONENT_PARAM,
    IAP_HID_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHIDComponentFunction5,
    (uint16_t*)&iAP2_Identification_wHIDComponentFunctionSize,
    IAP_HID_COMPONENT,
    IAP_HID_COMPONENT_FUNCTION_SUBPARAM},
#endif
#endif  
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT == _TRUE_)           /* USB Host HID component */
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 0)
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentId0,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentName0,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentName0Size,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentFunction0,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentFunctionSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_FUNCTION_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransComponentId0,
    (uint16_t*)&iAP2_Identification_wHostHIDTransComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransportInterface0,
    (uint16_t*)&iAP2_Identification_wHostHIDTransportInterfaceSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_INTERFACE_NUM_SUBPARAM},
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 1)
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentId1,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentName1,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentName1Size,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentFunction1,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentFunctionSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_FUNCTION_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransComponentId1,
    (uint16_t*)&iAP2_Identification_wHostHIDTransComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransportInterface1,
    (uint16_t*)&iAP2_Identification_wHostHIDTransportInterfaceSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_INTERFACE_NUM_SUBPARAM},
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 2)
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentId2,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentName2,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentName2Size,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentFunction2,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentFunctionSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_FUNCTION_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransComponentId2,
    (uint16_t*)&iAP2_Identification_wHostHIDTransComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransportInterface2,
    (uint16_t*)&iAP2_Identification_wHostHIDTransportInterfaceSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_INTERFACE_NUM_SUBPARAM},
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 3)
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentId3,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentName3,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentName3Size,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentFunction3,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentFunctionSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_FUNCTION_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransComponentId3,
    (uint16_t*)&iAP2_Identification_wHostHIDTransComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransportInterface3,
    (uint16_t*)&iAP2_Identification_wHostHIDTransportInterfaceSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_INTERFACE_NUM_SUBPARAM},
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 4)
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentId4,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentName4,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentName4Size,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentFunction4,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentFunctionSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_FUNCTION_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransComponentId4,
    (uint16_t*)&iAP2_Identification_wHostHIDTransComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransportInterface4,
    (uint16_t*)&iAP2_Identification_wHostHIDTransportInterfaceSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_INTERFACE_NUM_SUBPARAM},
#endif
#if (IAP2_IDENT_USB_HOST_HID_COMPONENT_INSTANCES > 5)
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentId5,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_HID_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentName5,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentName5Size,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_NAME_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDComponentFunction5,
    (uint16_t*)&iAP2_Identification_wHostHIDComponentFunctionSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_COMPONENT_FUNCTION_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransComponentId5,
    (uint16_t*)&iAP2_Identification_wHostHIDTransComponentIdSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_COMPONENT_ID_SUBPARAM},
    {(uint8_t*)&iAP2_Identification_baHostHIDTransportInterface5,
    (uint16_t*)&iAP2_Identification_wHostHIDTransportInterfaceSize,
    USB_HOST_HID_COMPONENT_PARAM,
    USB_HOST_TRANS_INTERFACE_NUM_SUBPARAM},
#endif
#endif
    
};

#define IDENT_INFO_NUMBER_OF_PARAMS 	ARRAY_N_ELEMENTS(iAP2_Identification_sSendInfo)
#define HEADER_SIZE_BYTES				((IDENT_INFO_NUMBER_OF_PARAMS * PARAM_HEADER_SIZE) + \
										PARAM_GROUP_HEADER_SIZE + MESSAGE_HEADER_SIZE)

/* Pointers to store data from Information Update*/
static void * const iAP2_Identification_Info_Update_ParamVal[]=
{
		&(InformationUpdates.pbName),
	    &(InformationUpdates.pbModeIdentifier),
		&(InformationUpdates.pbManufacturer),
		&(InformationUpdates.pbSerialNumber),
		&(InformationUpdates.pbFirmwareVersion),
		&(InformationUpdates.pbHardwareVersion), //5
		&(InformationUpdates.pbCurrentLanguage),
};

/* Pointers to get maximum data size from an Information Update*/
static uint8_t * const iAP2_Identification_Info_Update_Param_Size[] =
 {
		 &(InformationUpdates.bNameSize),
		 &(InformationUpdates.bModeIdentifierSize),
		 &(InformationUpdates.bManufacturerSize),
		 &(InformationUpdates.bSerialNumberSize),
		 &(InformationUpdates.bFirmwareVersionSize),
		 &(InformationUpdates.bHardwareVersionSize), //5
		 &(InformationUpdates.bCurrentLanguageSize),
 };

 /* Pointers to get maximum data size from a Rejected Message*/
static uint16_t * const iAP2_Identification_Rejected_Param_Max_Size[] =
{
		&(RejectedInfo.wMessageSentByAccesorySize),
		&(RejectedInfo.wMessageReceivedFromDeviceSize),
};

 /* Pointers where data will be stored from a Rejected Message parameters*/
static uint16_t ** const iAP2_Identification_Rejected_Param_Val[] =
{
		&(RejectedInfo.pwMessageSentByAccesory),
		&(RejectedInfo.pwMessageReceivedFromDevice),
};

 /*Arrays of flags for each rejected parameter*/
static const uint32_t iAP2_Identification_gabRejectedAttributes[IAP2_IDENTIFICATION_REJECTED_TOTAL_PARAM]=
{
		IAP2_IDENTIFICATION_NAME_REJECTED,
		IAP2_IDENTIFICATION_MODELL_REJECTED,
		IAP2_IDENTIFICATION_MANUFACTURER_REJECTED,
		IAP2_IDENTIFICATION_SERIAL_NUMBER_REJECTED,
		IAP2_IDENTIFICATION_FIRMWARE_VER_REJECTED,
		IAP2_IDENTIFICATION_HARDWARE_VER_REJECTED,
		IAP2_IDENTIFICATION_MESSAGE_SENT_REJECTED,
		IAP2_IDENTIFICATION_MESSAGE_RECEIVED_REJECTED,
		IAP2_IDENTIFICATION_POWER_SOURCE_REJECTED,
		IAP2_IDENTIFICATION_MAX_CURRENT_FROM_DEVICE_REJECTED,
		IAP2_IDENTIFICATION_EXTERNAL_ACCESORY_REJECTED,
		IAP2_IDENTIFICATION_APP_BUNDLE_SEED_REJECTED,
		IAP2_IDENTIFICATION_CURRENT_LANGUAGE_REJECTED,
		IAP2_IDENTIFICATION_SUPPORTED_LANGUAGE_REJECTED,
		IAP2_IDENTIFICATION_SERIAL_TRANSPORT_COMPONENT_REJECTED,
		IAP2_IDENTIFICATION_USB_DEVICE_TRANSPORT_REJECTED,
		IAP2_IDENTIFICATION_USB_HOST_TRANSPORT_REJECTED,
		IAP2_IDENTIFICATION_BLUETOOTH_TRANSPORT_REJECTED,
		IAP2_IDENTIFICATION_IAP2_HID_COMPONENT_REJECTED,
		IAP2_IDENTIFICATION_USB_HOST_HID_COMPONENT_REJECTED,
};

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

static uint16_t gwDataSize;					/**< Data Size for current parameter*/
static uint8_t gbCurrentFrame;				/**< Current frame status*/
static uint8_t gbMessageCurrentStatus;  		/**< Current message status*/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/

static void iAP2_Identification_vfnNewRequestCaller(void(*fnPtr)(uint16_t),uint16_t wMessageID);
static void iAP2_Identification_vfnStartOrAcceptedReceived(void(*fnPtr)(uint16_t),uint16_t wMessageID);
static void iAP2_Identification_vfnRejectedReceived(void(*fnPtr)(uint16_t),uint16_t wMessageID);
static void iAP2_Identification_vfnNotValidReceived(void(*fnPtr)(uint16_t),uint16_t wMessageID);
static void iAP2_Identification_Rejected_Update_blob_u16(uint16_t * pbParam_Value,uint16_t *wParam_Size);
static void iAP2_Identification_Rejected_vfnGetParameter(void);

/*************************************************************************************************/
/*                               Static Function Prototypes Section                              */
/*************************************************************************************************/

static uint8_t detectSignatureForEndParamGroup(const IAP2_IDENT_INFO * spContentToIdentifySignature);

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/

static void iAP2_Identification_vfnNotValidReceived(void(*fnPtr)(uint16_t),uint16_t wMessageID)
{
	(void)fnPtr;
	(void)wMessageID;
}

/*Pointers to function for a received MessageID */
void (* const IAP2_Identification_vfnapReceivedMessageID[]) (void(*fnPtr)(uint16_t),uint16_t wMessageID) =
{
		iAP2_Identification_vfnStartOrAcceptedReceived, //0
		iAP2_Identification_vfnNotValidReceived,
		iAP2_Identification_vfnStartOrAcceptedReceived,
		iAP2_Identification_vfnRejectedReceived, //3
};

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/


/** ***********************************************************************************************
*
* @brief    Function used to read blob (uint16_t) parameters from a message
*
* @param    pbParam_Value where will be stored the read blob data
* 			pwParam_Size Maximum possible value for the blob
* 			bpArraySize Indicates how many data array should be corrected
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_Identification_Rejected_Update_blob_u16(uint16_t * pbParam_Value,uint16_t *pwParam_Size)
{
	uint8_t bCommandIDIndex = 0;
	uint16_t wNumberElements = gwDataSize/2;
	if(wNumberElements > *pwParam_Size)
	{
		gbMessageCurrentStatus |=  iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED;
		gbCurrentFrame = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
	}
	else
	{
		gbCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(pbParam_Value,gwDataSize);
		*pwParam_Size = wNumberElements;
		(void)iAP2_Features_bfnSwapArray(pbParam_Value,wNumberElements,IAP2_FEATURES_UINT16_ARRAY);
	}
} 

/** ***********************************************************************************************
*
* @brief    Function called when a Message to Identification is received.
* 
* @param   	wMessageID Identifies messageID
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_Feat_ID_vfnCallBack(uint16_t wMessageID)
{
	iAP2_Identification_vfnNewRequestCaller(&iAP2_Host_vfnNewIDMessage,wMessageID); /*@TODO: Call correct function*/
}

/** ***********************************************************************************************
*
* @brief    Function used to when an InformationUpdate Message was received
*
* @param    fnPtr Pointer to the function to be called once received message was validated
*			wMessageID	MesssageID 
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_Identification_vfnStartOrAcceptedReceived(void(*fnPtr)(uint16_t),uint16_t wMessageID)
{
	fnPtr(wMessageID);
}

/*************************************************************************************************
*
* @brief    Function used to when an RejectedInformation Message was received
*
* @param    fnPtr Pointer to the function to be called once received message was validated
*			wMessageID	MesssageID 
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_Identification_vfnRejectedReceived(void(*fnPtr)(uint16_t),uint16_t wMessageID)
{
	if(iAP2_FEATURES_UPDATES_MESSAGE_NEW == gbMessageCurrentStatus)
	{
		RejectedInfo.dwRejectedIdentificationInfo = 0;
	}
	iAP2_Identification_Rejected_vfnGetParameter();
	if(gbMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED )
	{
		gbMessageCurrentStatus = iAP2_FEATURES_UPDATES_MESSAGE_NEW;
	}
	else
	{
		if(IAP2_SESS_CTRL_MESSAGE_COMPLETE == gbCurrentFrame)
		{	
			gbMessageCurrentStatus = iAP2_FEATURES_UPDATES_MESSAGE_NEW;
			/** @todo Function Return */
			(void)memcpy(RejectedInfoAppPtr,&RejectedInfo,sizeof(iAP2_Identification_RejectedInformation));
			fnPtr(wMessageID);
		}
		else
		{
			gbMessageCurrentStatus |= iAP2_FEATURES_UPDATES_MESSAGE_INCOMPLETE;
		}
	}
}

/*************************************************************************************************
*
* @brief    Function used to when a new Message was received from Sessions 
*
* @param    fnPtr Pointer to the function to be called once received message was validated
*			wMessageID	MesssageID 
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_Identification_vfnNewRequestCaller(void(*fnPtr)(uint16_t),uint16_t wMessageID)
{
	uint8_t bMessage = (uint8_t) wMessageID;
	if(bMessage < IAP2_IDENTIFICATION_MAX_RECEIVED_MESSAGE)
	{
		IAP2_Identification_vfnapReceivedMessageID[((bMessage))](fnPtr,wMessageID);
	}
}

/*************************************************************************************************
*
* @brief   Get all parameter information from a Rejected Message
*
* @param   void
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_Identification_Rejected_vfnGetParameter(void)
{
	uint8_t  bSessionStatus;
	uint16_t wParamID;
	uint32_t wTempFlag;
	uint8_t tmpData;
	gwDataSize=0;
	do
	{
		bSessionStatus= iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID,&gwDataSize);
		gbCurrentFrame  = IAP2_SESS_CTRL_PARAM_COMPLETE;
		if(IAP2_SESS_CTRL_OK ==  bSessionStatus)
		{	
			if(wParamID < IAP2_IDENTIFICATION_REJECTED_TOTAL_PARAM)
			{
				wTempFlag = iAP2_Identification_gabRejectedAttributes[wParamID];
				(RejectedInfo.dwRejectedIdentificationInfo) |= iAP2_Identification_gabRejectedAttributes[wParamID];
				wParamID -= IAP2_IDENTIFICATION_REJECTED_MESSAGE_BY_ACCESORY;
				if(2>wParamID)
 				{
					iAP2_Identification_Rejected_Update_blob_u16(*iAP2_Identification_Rejected_Param_Val[wParamID],(iAP2_Identification_Rejected_Param_Max_Size[wParamID]));
				}
				else
				{
					gbCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(&tmpData,0);
				}
			}				
		}
	}while((IAP2_SESS_CTRL_OK == bSessionStatus) && (IAP2_SESS_CTRL_MESSAGE_COMPLETE != gbCurrentFrame) && (IAP2_SESS_CTRL_DATA_PENDING != gbCurrentFrame));	
}

/**************************************************************************************************
*
* @brief    Send Cancel Identification Message.
* 
* @param   	void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_Identification_bfnSendCancell(void)
{
	uint8_t bSessionStatus;
	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{

		bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_IDENTIFICATION_CANCELL, NULL, 0, 0);

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}
	return bSessionStatus;
}


/**************************************************************************************************
*
* @brief    Send UpdateInformation Message.
* 
* @param   	void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_Identification_bfnSendUpdateInformation(iAP2_Identification_InformationUpdateData *InformationUpdatesApp)
{
	uint8_t bSessionStatus;
	uint8_t bCurrentParam;
	uint16_t wMessageParametersSize;

	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{

		wMessageParametersSize = 
					InformationUpdatesApp->bNameSize + 
					InformationUpdatesApp->bModeIdentifierSize +
					InformationUpdatesApp->bManufacturerSize +
					InformationUpdatesApp->bSerialNumberSize +
					InformationUpdatesApp->bFirmwareVersionSize +
					InformationUpdatesApp->bHardwareVersionSize +
					InformationUpdatesApp->bCurrentLanguageSize +
					IAP2_SESSIONS_CTRL_HEADER(7);
		
		bCurrentParam = 0;
		gbMessageCurrentStatus = 0;
		/** @todo Function Return */
		(void)memcpy(&InformationUpdates,InformationUpdatesApp,sizeof(iAP2_Identification_InformationUpdateData));
		bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_IDENTIFICATION_INFO_UPDATE, NULL, 0,wMessageParametersSize);
		while((IAP2_IDENTIFICATION_FEATURE_ERROR_OK == bSessionStatus) && (bCurrentParam<iAP2_IDENTIFICATION_INFO_UPDATES_TOTAL_PARAM))
		{			
			bSessionStatus= iAP2_Sessions_bfnAddCtrlMsgParam(bCurrentParam,IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, 
					((uint8_t *)(*(uint32_t *)iAP2_Identification_Info_Update_ParamVal[bCurrentParam])), *iAP2_Identification_Info_Update_Param_Size[bCurrentParam]);
			bCurrentParam++;
		}
		if(IAP2_IDENTIFICATION_FEATURE_ERROR_OK == bSessionStatus)
		{
			bSessionStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
		}
		else
		{
			iAP2_Sessions_vfnResetMessage();
		}

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}
	return bSessionStatus;
}

/** ***********************************************************************************************
*
* @brief    Function is called by the upper layer and sends all the information related to the 
* 			identification process. All the information is set using processor expert.
* 
* @param   	spRejectedInfoPtr: Pointer to structure where Rejected information in identification will
* 								be stored.
* 								The pointers pwMessageReceivedFromDevice and pwMessageSentByAccesory
* 								should be initialized with two arrays for the rejected ID elements.
* 								The wMessageReceivedFromDeviceSize and wMessageSentByAccesorySize will
* 								contain the number of MessageID rejected.
*								Element dwRejectedIdentificationInfo will indicated with flags the rejected
*								information.
*				
* 
* @return  	0x00 - No error occurred during function execution
* 			0x01 - An error occurred during function execution
*          
**************************************************************************************************/
uint8_t iAP2_Identification_bfnSendIdentificationInfo(iAP2_Identification_RejectedInformation * spRejectedInfoPtr)
{
	uint16_t wParamCount;
	uint16_t wMsgSize;
	uint8_t bSendIdentInfoReturnVal;
    
    
	int lockStatus;
	
	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
	
	if(ERRCODE_NO_ERROR == lockStatus)
	{
		wParamCount = 0;
		wMsgSize = 0;
		RejectedInfoAppPtr = spRejectedInfoPtr;

		/** @todo Function Return */
		(void)memcpy(&RejectedInfo,RejectedInfoAppPtr,sizeof(iAP2_Identification_RejectedInformation));

		/* Add all the data sizes from the send structure */
		while(wParamCount < IDENT_INFO_NUMBER_OF_PARAMS)
		{			
			wMsgSize = wMsgSize + *(iAP2_Identification_sSendInfo[wParamCount].wParameterSize);
			wParamCount++;
		}
		/* Add the approximate number of headers size */
		wMsgSize = wMsgSize + HEADER_SIZE_BYTES;
		/* Resets counter to zero */
		wParamCount = (uint16_t)0x00;
		/* Starts sending Identification Information */
		bSendIdentInfoReturnVal = iAP2_Sessions_bfnSendCtrlMessage(IAP2_CTRL_MSG_IDENTIFICATION_INFO, \
				NULL, (uint16_t)DATA_SIZE_NULL, wMsgSize);

		/* while parameters remaining in the send structure */
		while((IAP_SESSIONS_NEW_CTRL_SESSION_FRAME_OK == bSendIdentInfoReturnVal) && \
				(wParamCount < IDENT_INFO_NUMBER_OF_PARAMS))
		{			
			/* If multi-group parameter (with same Parameter ID) is detected by
			 * signature ('END_PARAM_GROUP_SIGNATURE') pattern send signal to
			 * sessions layer to close group. */
			if(TRUE == detectSignatureForEndParamGroup(&iAP2_Identification_sSendInfo[wParamCount]))
			{
				/* send the signal to the sessions control layer to close previous parameter group */
				iAP2_Sessions_vfnCloseParamGroup();

				/* jump signature entry */
				wParamCount++;
			}

			/* add one parameter at a time to the control message */
			bSendIdentInfoReturnVal = \
					iAP2_Sessions_bfnAddCtrlMsgParam(iAP2_Identification_sSendInfo[wParamCount].wParameterId, \
							iAP2_Identification_sSendInfo[wParamCount].wSubParameterId, \
							iAP2_Identification_sSendInfo[wParamCount].bpParameterAddress, \
							*(iAP2_Identification_sSendInfo[wParamCount].wParameterSize));
			/* Increase send parameter index */
			wParamCount++;
		}
		/* if there were no errors while adding parameters */
		if(IAP_SESSIONS_NEW_CTRL_SESSION_FRAME_OK == bSendIdentInfoReturnVal)
		{
			/* let the sessions layer know that message is complete */
			bSendIdentInfoReturnVal =  iAP2_Sessions_bfnEndSendCtrlMsg();
		}
		else
		{
			iAP2_Sessions_vfnResetMessage();
		}
		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSendIdentInfoReturnVal = SESSIONS_LOCK_FAIL;
		}

	}
    else
	{
		bSendIdentInfoReturnVal = SESSIONS_LOCK_FAIL;
	}
	return bSendIdentInfoReturnVal;
}

/*************************************************************************************************/
/*                               Static Functions Section                                        */
/*************************************************************************************************/

/**************************************************************************************************
*
* @brief    detects if signature is present in the given structure parameter array. The signature
* 			makes the structure equal to 'END_PARAM_GROUP_SIGNATURE' macro.
* 
* @param   	spContentToIdentifySignature	pointer to structure on where to look the signature in
* 
* @return  	TRUE:	the signature is present in the given pointed structure
* 			FALSE:	no signature found
*          
**************************************************************************************************/
static uint8_t detectSignatureForEndParamGroup(const IAP2_IDENT_INFO *spContentToIdentifySignature)
{
	uint8_t result = FALSE;
	
	if((spContentToIdentifySignature->bpParameterAddress == (uint8_t*)END_PARAM_GROUP_SIGNATURE_FOURBYTES) &&
			(spContentToIdentifySignature->wParameterId == (uint16_t)END_PARAM_GROUP_SIGNATURE_TWOBYTES) &&
			(*(spContentToIdentifySignature->wParameterSize) == (uint16_t)END_PARAM_GROUP_SIGNATURE_TWOBYTES) &&
			(spContentToIdentifySignature->wSubParameterId == (uint16_t)END_PARAM_GROUP_SIGNATURE_TWOBYTES))
	{
		result = TRUE;
	}
	
	return result;
}


