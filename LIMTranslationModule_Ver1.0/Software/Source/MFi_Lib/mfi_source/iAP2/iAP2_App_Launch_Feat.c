/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_App_Launch_Feat.h"
#include "iAP2_Sessions_Control.h"
#include "iAP2_Features.h"
#include "iAP2_Identification_Feat.h"
/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define iAP2_REQUEST_APP_LAUNCH_MAX_MESSAGE_SIZE		0x18

#define iAP2_REQUEST_APP_LAUNCH_APP_BUNDLE_ID_ID		    0
#define iAP2_REQUEST_APP_LAUNCH_LAUNCH_ALERT_ID				1

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
//static const uint8_t iAP2_AppLaunch_AppBundleId[] = "com.freescaleM.EADemo";//"com.freescaleM.EADemo";
static const uint8_t iAP2_AppLaunch_AppBundleId[] = "com.Philips.Lumify";//"com.PhilipsLumify";
#if iAP2_APP_LAUNCH_NO_ALERT
static uint8_t iAP2_AppLaunch_NoAlert = 1;
#endif
/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/**************************************************************************************************
*
* @brief    Function called to start AppLaunchRequest from accessory.
* 
* @param   	void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_Application_Launch_Feature_bfnAppLaunch(void)
{
	uint8_t bSessionStatus;
	bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(iAP2_APP_LAUNCH_REQUEST, NULL, 0,iAP2_REQUEST_APP_LAUNCH_MAX_MESSAGE_SIZE);
	if(iAP2_APP_LAUNCH_ERROR_OK == bSessionStatus)
	{
		bSessionStatus= iAP2_Sessions_bfnAddCtrlMsgParam(iAP2_REQUEST_APP_LAUNCH_APP_BUNDLE_ID_ID,IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, (uint8_t *)&iAP2_AppLaunch_AppBundleId, sizeof(iAP2_AppLaunch_AppBundleId));
#if iAP2_APP_LAUNCH_NO_ALERT
		if(iAP2_APP_LAUNCH_ERROR_OK == bSessionStatus)
		{
			bSessionStatus= iAP2_Sessions_bfnAddCtrlMsgParam(iAP2_REQUEST_APP_LAUNCH_LAUNCH_ALERT_ID,IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, (uint8_t *)&iAP2_AppLaunch_NoAlert, sizeof(iAP2_AppLaunch_NoAlert));
		}
		else
		{
			iAP2_Sessions_vfnResetMessage();
			return bSessionStatus;
		}
#endif
	}
	if(iAP2_APP_LAUNCH_ERROR_OK == bSessionStatus)
	{
		bSessionStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
	}
	else
	{
		iAP2_Sessions_vfnResetMessage();
	}
	return bSessionStatus;
}


