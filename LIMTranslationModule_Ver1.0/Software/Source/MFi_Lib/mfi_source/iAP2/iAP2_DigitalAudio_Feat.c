/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_DigitalAudio_Feat.h"
#include "iAP_USB_Audio_Interface.h"
#include "iAP2_Features.h"
#include "iAP2_Sessions_Control.h"


/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define iAP2_DIGITAL_AUDIO_FEAT_MAX_PARAM_ID	    (3)

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/

void (* const iAP2_AudioDigitalFeat_vfnapReceivedInfoParameters[])(void* subparamvalue,uint8_t maxValue, uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame) =
{
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32,
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32,
};

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
static uint16_t gwDataSize;					/**< Data Size for current parameter*/
static uint8_t gbCurrentFrame;				/**< Current frame status*/
static uint8_t gbMessageCurrentStatus;  		/**< Current message status*/
static iAP2_Digital_Audio_Feat_InfomationData * spDigitalAudioFeatInformationToUpdate;
static iAP2_Digital_Audio_Feat_InfomationData iAP2_Digital_Audio_Feat_InfomationDataTmp;


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
/*Pointers where AudioInformation data would be stored*/
static void * const iAP2_AudioDigitalFeat_paramVal[]=
{
		&(iAP2_Digital_Audio_Feat_InfomationDataTmp.bSampleRate),
		&(iAP2_Digital_Audio_Feat_InfomationDataTmp.lVolumeAdjustment),
		&(iAP2_Digital_Audio_Feat_InfomationDataTmp.lSoundCheckAdjustmet),
		
};

 /*Possible maximum values for received data*/
static uint8_t const iAP2_Digital_Audio_Feat_ParamMaxValue[]=
{
	8,
	0,
	0,
};

static uint16_t const iAP2_Digital_Audio_Feat_SampleRate[] = 
{
	0x320, //iAP2_DIGITAL_AUDIO_8000_HZ
	0x2B11, //iAP2_DIGITAL_AUDIO_11025_HZ,
	0x2EE0, //iAP2_DIGITAL_AUDIO_12000_HZ,
	0x3E80, //iAP2_DIGITAL_AUDIO_16000_HZ,
	0x5622, //iAP2_DIGITAL_AUDIO_22050_HZ,
	0x5DC0, //iAP2_DIGITAL_AUDIO_24000_HZ,
	0x7D00, //iAP2_DIGITAL_AUDIO_32000_HZ,
	0XAC44, //iAP2_DIGITAL_AUDIO_44100_HZ,
	0XBB80, //iAP2_DIGITAL_AUDIO_48000_HZ,
};

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void iAP2_DigitalAudio_Feat_vfnNewRequestCaller(void(*fnPtr)(uint16_t),uint16_t wMessageID);
static void iAP2_DigitalAudio_Feat_vfnGetParameter(void);

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/


/**************************************************************************************************
*
* @brief    Function called to start DigitalAudioFeat messages from the iPod, iPad or
* 			iPhone device.
* 
* @param   * iAP2_Digital_Audio_FeatInfomationData:	
* 									the pointer to the application structure where the updates will be 
* 									saved. The application should initialize the pointer.
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*			iAP2_DIGITAL_AUDIO_FEAT_INVALID_DATA_PARAMETER 0xFF
**************************************************************************************************/
uint8_t iAP2_DigitalAudio_Feat_bfnStartUSBDeviceModeAudio(iAP2_Digital_Audio_Feat_InfomationData * DigitalAudioFeatInformationDataPtr)
{
	uint8_t bSessionStatus;

	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{

		bSessionStatus= iAP2_DIGITAL_AUDIO_FEAT_INVALID_DATA_PARAMETER;
		if( NULL != DigitalAudioFeatInformationDataPtr)
		{
			spDigitalAudioFeatInformationToUpdate = DigitalAudioFeatInformationDataPtr;
			gbMessageCurrentStatus = 0;
			bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(iAP2_DIGITAL_AUDIO_FEAT_START, NULL, 0, 0);
		}

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}
	return bSessionStatus;
}



/** ***********************************************************************************************
*
* @brief    Function called to stop DigitalAudioFeat from iPod, iPad or
* 			iPhone device.
* 
* @param   void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_DigitalAudio_Feat_bfnStopUSBDeviceModeAudio(void)
{
	uint8_t bSessionStatus;
	
	int lockStatus;
	
	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
	
	if(ERRCODE_NO_ERROR == lockStatus)
	{

	
	gbMessageCurrentStatus = 0;
	bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(iAP2_DIGITAL_AUDIO_FEAT_STOP, NULL, 0, 0);
	

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}
	return bSessionStatus;
}

/** ***********************************************************************************************
*
* @brief    Function called when a Message for DigitalAudioFeat is received .
* 
* @param   	wMessageID Identifies messageID
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_Feat_DigitalAudio_vfnCallBack(uint16_t wMessageID)
{
	if(iAP2_DIGITAL_AUDIO_FEAT_INFO == wMessageID)
	{
		iAP2_DigitalAudio_Feat_vfnNewRequestCaller(&iAP2_CALLBACK_FROM_DIGITAL_AUDIO_FEAT,wMessageID); 
	}
}


static void iAP2_DigitalAudio_Feat_vfnNewRequestCaller(void(*fnPtr)(uint16_t),uint16_t wMessageID)
{
	(void)wMessageID;
	
	iAP2_DigitalAudio_Feat_vfnGetParameter();		
	if(gbMessageCurrentStatus &  iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED )
	{
		gbMessageCurrentStatus = iAP2_FEATURES_UPDATES_MESSAGE_NEW;
	}
	else
	{
		if(IAP2_SESS_CTRL_MESSAGE_COMPLETE == gbCurrentFrame)
		{
			/** @todo Function Return */
			(void)memcpy(spDigitalAudioFeatInformationToUpdate,&iAP2_Digital_Audio_Feat_InfomationDataTmp,sizeof(iAP2_Digital_Audio_Feat_InfomationData));
			gbMessageCurrentStatus = iAP2_FEATURES_UPDATES_MESSAGE_NEW;
			/*Call DigitalAudio with the current frequency*/
            if (fnPtr != NULL) 
			{
				fnPtr(iAP2_Digital_Audio_Feat_SampleRate[iAP2_Digital_Audio_Feat_InfomationDataTmp.bSampleRate]);	
            }
		}
		else
		{
			gbMessageCurrentStatus |= iAP2_FEATURES_UPDATES_MESSAGE_INCOMPLETE;
		}
	}
}

/** ***********************************************************************************************
*
* @brief    Function called to get Parameters from a DigitalAudio Feature Message.
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_DigitalAudio_Feat_vfnGetParameter(void)
{
	static uint8_t  bSessionStatus;
	static uint16_t wParamID;
	static uint8_t bDummyData;
	gwDataSize=0;
	do
	{
		bSessionStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID,&gwDataSize);
		gbCurrentFrame = IAP2_SESS_CTRL_PARAM_COMPLETE;
		if( IAP2_SESS_CTRL_OK == bSessionStatus)
		{	
			if(wParamID < iAP2_DIGITAL_AUDIO_FEAT_MAX_PARAM_ID)
			{
				iAP2_AudioDigitalFeat_vfnapReceivedInfoParameters[wParamID]( 
						iAP2_AudioDigitalFeat_paramVal[wParamID],
						iAP2_Digital_Audio_Feat_ParamMaxValue[wParamID],
							0,
							0,
							gwDataSize, &gbMessageCurrentStatus,&gbCurrentFrame);
			}
			else
			{
				gbCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(&bDummyData,0);	
			}
		}
	}while((IAP2_SESS_CTRL_OK == bSessionStatus) & (IAP2_SESS_CTRL_MESSAGE_COMPLETE != gbCurrentFrame ) & (IAP2_SESS_CTRL_DATA_PENDING != gbCurrentFrame ));	
}

void iAP2_DigitalAudio_Feat_vfnDummy(uint16_t wMessageID)
{
	/*Temporally call back for a new USBDeviceModeAudioInformation message*/ 
	(void)wMessageID;

}
