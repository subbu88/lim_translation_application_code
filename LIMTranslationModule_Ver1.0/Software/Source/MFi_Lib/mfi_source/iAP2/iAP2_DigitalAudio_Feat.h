/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_DIGITALAUDIO_FEAT_H_
#define IAP2_DIGITALAUDIO_FEAT_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
//#include "DigitalAudioDriver.h"
#include "string.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/*Callback from DigitalAudio Feat to DigitalAudio driver with the new Frequency*/
#define iAP2_CALLBACK_FROM_DIGITAL_AUDIO_FEAT			(iAP2_DA_Callback)

/*Identification MessageID */
#define iAP2_DIGITAL_AUDIO_FEAT_START				(0xDA00)
#define iAP2_DIGITAL_AUDIO_FEAT_STOP				(0xDA02)
#define iAP2_DIGITAL_AUDIO_FEAT_INFO			    (0xDA01)
  						
#define iAP2_DIGITAL_AUDIO_FEAT_INVALID_DATA_PARAMETER (0xFF)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/

typedef enum
{
	iAP2_DIGITAL_AUDIO_8000_HZ = 0,
	iAP2_DIGITAL_AUDIO_11025_HZ,
	iAP2_DIGITAL_AUDIO_12000_HZ,
	iAP2_DIGITAL_AUDIO_16000_HZ,
	iAP2_DIGITAL_AUDIO_22050_HZ,
	iAP2_DIGITAL_AUDIO_24000_HZ,
	iAP2_DIGITAL_AUDIO_32000_HZ,
	iAP2_DIGITAL_AUDIO_44100_HZ,
	iAP2_DIGITAL_AUDIO_48000_HZ
}iAP2_Digital_Audio_Valid_SampleRates;

typedef struct 
{
	int32_t lVolumeAdjustment;
	int32_t lSoundCheckAdjustmet;
	uint8_t 	bSampleRate; /*Posible values according to iAP2_Digital_Audio_Valid_SampleRates */
} iAP2_Digital_Audio_Feat_InfomationData;


/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void iAP2_Feat_DigitalAudio_vfnCallBack(uint16_t MessageID);
uint8_t iAP2_DigitalAudio_Feat_bfnStartUSBDeviceModeAudio(iAP2_Digital_Audio_Feat_InfomationData * DigitalAudioFeatInformationData);
uint8_t iAP2_DigitalAudio_Feat_bfnStopUSBDeviceModeAudio(void);
extern void iAP2_CALLBACK_FROM_DIGITAL_AUDIO_FEAT(uint16_t sample_rate);
/*************************************************************************************************/

#endif /* IAP2_DIGITALAUDIO_FEAT_H_ */
