/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_SESSIONS_FILE_TRANSFER_H_
#define IAP2_SESSIONS_FILE_TRANSFER_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/* Callback from file transfer session to application for PEx*/
#define iAP2_CALLBACK_FT_RX_EACH_PACKET				(vfnFileTransferData)
#define iAP2_CALLBACK_FT_NEW_TRANSFER				(vfnFileTransferSetUp)
#define iAP2_CALLBACK_FT_TRANSFER_END				(vfnFileTransferStatus)

#define IAP2_SESSIONS_FT_PARSER_OK    				(0x00)
#define IAP2_SESSIONS_FT_PARSER_ERR					(0x01)


#define IAP2_SESSIONS_FT_SETUP_DATAGRAM 			(0x04)
#define IAP2_SESSIONS_FT_START_DATAGRAM 			(0x01)
#define IAP2_SESSIONS_FT_FIRST_DATAGRAM 			(0x80)
#define IAP2_SESSIONS_FT_DATA_DATAGRAM 				(0x00)
#define IAP2_SESSIONS_FT_LAST_DATAGRAM 				(0x40)
#define IAP2_SESSIONS_FT_CANCEL_DATAGRAM 			(0x02)
#define IAP2_SESSIONS_FT_PAUSE_DATAGRAM 			(0x03)
#define IAP2_SESSIONS_FT_SUCCESS_DATAGRAM 			(0x05)
#define IAP2_SESSIONS_FT_FAILURE_DATAGRAM 			(0x06)
#define IAP2_SESSIONS_FT_FIRST_AND_ONLY_DATAGRAM 	(0xC0)

#define IAP2_SESSIONS_FT_RECEIVE_SUCCESS 			(_TRUE_)
#define IAP2_SESSIONS_FT_RECEIVE_FAILURE 			(_FALSE_)

#define IAP2_SESSIONS_FT_FREE_STATUS 				(0x00)
#define IAP2_SESSIONS_FT_WAIT_1ST_STATUS 			(0x01)
#define IAP2_SESSIONS_FT_RECEIVING_STATUS 			(0x02)
#define IAP2_SESSIONS_FT_PAUSE_STATUS	 			(0x80)
/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef struct{
	void (*vfnFTDataCallbackFn)(uint16_t wFileTransPacketSize, uint8_t wFileTransferID,
			void** bppStoreData, uint16_t *wpArrayMaxCapacity);
	void (*vfnStartFTCallbackFn)(uint32_t dwFileTransferSize, uint8_t bFileTransferId);
	void (*vfnEndFTCallbackFn)(uint8_t bFileTransferId, uint8_t bTransferStatus,
			uint8_t* bpUpperLayerStatus);
}iAP2_FTSessionType;

enum FTSessionsStatus
{
	FT_SESSIONS_OK = 0,
	FT_SESSIONS_BUSY,
	FT_SESSIONS_MEM_FULL,
	FT_SESSIONS_ERROR, /* causes: amount of reserved link memory is less than requested; */
	FT_SESSIONS_SEND_COMPLETE
};

typedef enum
{
	FT_DATAGRAM_SETUP,
	FT_DATAGRAM_START,
	FT_DATAGRAM_CANCEL,
	FT_DATAGRAM_PAUSE,
	FT_DATAGRAM_SUCCESS,
	FT_DATAGRAM_FAILURE
} FT_DATAGRAM_TYPE_t;

typedef struct
{
	uint8_t *bpFile;
	uint32_t dwFileSize;
	uint32_t dwSentBytes;
	uint8_t bFileTransferIdentifier;
}FT_FILE_SEND_t;
/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
uint8_t iAP2_Sessions_bfnFileTransferParser(uint16_t wReadIndex ,uint16_t wSize);
uint8_t iAP2_Sessions_bfnSendDatagram(FT_DATAGRAM_TYPE_t tDatagramType, FT_FILE_SEND_t *tpFileToSend);
void iAP2_Sessions_FT_vfnReset(void);
#ifdef IAP2_SESSIONS_FT_SEND_ENABLED
uint8_t iAP2_Sessions_bfnSendFile(FT_FILE_SEND_t *tpFileToSend);
#endif

extern void iAP2_CALLBACK_FT_RX_EACH_PACKET(uint16_t wFileTransPacketSize, uint8_t wFileTransferID,
		void** bppStoreData, uint16_t *wpArrayMaxCapacity);
extern void iAP2_CALLBACK_FT_NEW_TRANSFER(uint32_t dwFileTransferSize, uint8_t bFileTransferId);
extern void iAP2_CALLBACK_FT_TRANSFER_END(uint8_t bFileTransferId, uint8_t bTransferStatus,
		uint8_t* bpUpperLayerStatus);


/*************************************************************************************************/

#endif /* IAP2_SESSIONS_FILE_TRANSFER_H_ */
