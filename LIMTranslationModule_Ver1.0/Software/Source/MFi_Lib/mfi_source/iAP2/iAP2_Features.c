/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "iAP2_Features.h"

#if IAP2_FEAT_AUTH == _TRUE_
	#include "iAP2_Authentication_Feat.h" 
#endif
#if IAP2_FEAT_ID == _TRUE_
	#include "iAP2_Identification_Feat.h"
#endif
#if IAP2_FEAT_ASSIST_TOUCH == _TRUE_
	#include "iAP2_AssistiveTouch_Feat.h"
#endif
#if IAP2_FEAT_BT == _TRUE_
	#include "iAP2_Authentication_Feat"
#endif
#if IAP2_FEAT_EA == _TRUE_
	#include "iAP2_Ext_Acc_Feat.h"
#endif
#if IAP2_FEAT_HID == _TRUE_
	#include "iAP2_HID_Feat.h"
#endif
#if IAP2_FEAT_LOC == _TRUE_
	#include "iAP2_Authentication_Feat"
#endif
#if IAP2_FEAT_MEDIA_LIB == _TRUE_
	#include "iAP2_MediaLibraryAccess_Feat.h"
#endif
#if IAP2_FEAT_NOW_PLAY == _TRUE_
	#include "iAP2_NowPlayingUpdates_Feat.h"
#endif
#if IAP2_FEAT_POWER == _TRUE_
	#include "iAP2_Power_Feat.h"
#endif
#if IAP2_FEAT_USB_AUDIO == _TRUE_
	#include "iAP2_DigitalAudio_Feat.h"
#endif
#if IAP2_FEAT_VOICE_OVER == _TRUE_
	#include "iAP2_Authentication_Feat"
#endif
#if IAP2_FEAT_WIFI == _TRUE_
	#include "iAP2_Authentication_Feat"
#endif


/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

static uint8_t *gbpArray;

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
#if defined __CC_ARM
static __inline void iAP2_Feature_ToArrayU16(void);
static __inline void iAP2_Feature_ToArrayU32(void);
#else
 static inline void iAP2_Feature_ToArrayU16(void);
 static inline void iAP2_Feature_ToArrayU32(void);
#endif

static void iAP2_FEATURES_UPDATES_Data8(uint8_t * bpData,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
static void iAP2_FEATURES_UPDATES_Data8_Enum(uint8_t * bpData,uint8_t bmaxValue,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
static void iAP2_FEATURES_UPDATES_Data16(uint16_t * wpData,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
static void iAP2_FEATURES_UPDATES_Data32(uint32_t * dwpData,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
static void iAP2_FEATURES_UPDATES_DataUtf8(uint8_t * bpData, uint8_t bmaxValue, uint16_t wDataSize, uint8_t * bpCurrentFrame);

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
void iAP2_Features_Arbitrator(uint16_t wMsgId)
{
	uint8_t bFeatId;
	
	bFeatId = (uint8_t)(wMsgId >> 8);
	switch(bFeatId)
	{
		#if IAP2_FEAT_AUTH == _TRUE_
		case IAP2_FEAT_AUTH_ID:
			iAP2_Auth_Feat_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_ID == _TRUE_
		case IAP2_FEAT_ID_ID:
			iAP2_Feat_ID_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_ASSIST_TOUCH == _TRUE_
		case IAP2_FEAT_ASSIST_TOUCH_ID:
			iAP2_Feat_AssistTouch_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_BT == _TRUE_
		case IAP2_FEAT_BT_ID:
			iAP2_Feat_BT_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_EA == _TRUE_
		case IAP2_FEAT_EA_ID:
			iAP2_Feat_EA_vfnNewRequestCaller(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_HID == _TRUE_
		case IAP2_FEAT_HID_ID:
			iAP2_HID_Feat_vfnDeviceHIDReport(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_LOC == _TRUE_
		case IAP2_FEAT_LOC_ID:
			iAP2_Feat_Loc_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_MEDIA_LIB == _TRUE_
		case IAP2_FEAT_MEDIA_LIB_ID:
			iAP2_Feat_MediaLib_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_NOW_PLAY == _TRUE_
		case IAP2_FEAT_NOW_PLAY_ID:
			iAP2_Feat_NowPlay_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_POWER == _TRUE_
		case IAP2_FEAT_POWER_ID:
			iAP2_Feat_Power_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_USB_AUDIO == _TRUE_
		case IAP2_FEAT_USB_AUDIO_ID:
			iAP2_Feat_DigitalAudio_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_VOICE_OVER == _TRUE_
		case IAP2_FEAT_VOICE_OVER_ID:
			iAP2_Feat_VoiceOver_vfnCallBack(wMsgId);
			break;
		#endif
		#if IAP2_FEAT_WIFI == _TRUE_
		case IAP2_FEAT_WIFI_ID:
			iAP2_Feat_WiFi_vfnCallBack(wMsgId);
			break;
		#endif
		default:
			/* Invalid or not included feature */
			break;
	}
}



void (* const IAP2_Feature_vfnapConvertTo[]) (void) =
{
		iAP2_Feature_ToArrayU16,  /**< used to call ToArrayU16 function */
		iAP2_Feature_ToArrayU32,  /**< used to call ToArrayU32 function */
		
};
/** ***********************************************************************************************
*
* @brief    Function used to change an uint16_t data array to unsigned 8 data array.
*
* @param    void
* 
* @return  	void
*          
**************************************************************************************************/
#if defined __CC_ARM
static __inline void iAP2_Feature_ToArrayU16(void)
#else
static inline void iAP2_Feature_ToArrayU16(void)
#endif

{
	BYTESWAP16(*(uint16_t *)gbpArray,*(uint16_t *)gbpArray);
    gbpArray = (uint8_t*)(((uint16_t *)gbpArray) + 1); //@todo fix-kl DONE
}

/** ***********************************************************************************************
*
* @brief    Function used to change an uint32_t data array to unsigned 8 data array.
*
* @param    void
* 
* @return  	void
*          
**************************************************************************************************/
#if defined __CC_ARM
static __inline void iAP2_Feature_ToArrayU32(void)
#else
static inline void iAP2_Feature_ToArrayU32(void)
#endif
{
	BYTESWAP32(*(uint32_t *)gbpArray,*(uint32_t *)gbpArray);
        gbpArray = (uint8_t*)(((uint32_t *)gbpArray) + 1); //@todo fix-kl DONE
}

/** ***********************************************************************************************
*
* @brief    Function used to change any data array to unsigned 8 data array
*
* @param    bpArray Original data array
* 			bpArraySize Indicates how many data array should be corrected
* 			bTypeArray Indicates if bpArray is uint16_t or uint32_t data
* 				IAP2_FEATURES_UINT16_ARRAY
* 				IAP2_FEATURES_UINT32_ARRAY
* 
* @return  	If the conversion could be complete or not
*			IAP2_FEATURES_CONVERSION_DONE (0)
*			IAP2_FEATURES_CONVERSION_ERROR	(1)
*          
**************************************************************************************************/
uint8_t iAP2_Features_bfnSwapArray(void *bpArray, uint8_t bArraySize,uint8_t bTypeArray)
{
	uint8_t bStatusConversion = IAP2_FEATURES_CONVERSION_ERROR;
	gbpArray=bpArray;
	if((NULL != bpArray) &(bTypeArray<2))
	{
		while(bArraySize)
		{
			IAP2_Feature_vfnapConvertTo[bTypeArray]();
			bArraySize--;
		}
		bStatusConversion = IAP2_FEATURES_CONVERSION_DONE;
	}
	return bStatusConversion;
}

/** ***********************************************************************************************
*
* @brief    Function used to save uint8 data from a message, the validation of the correct data size is done too.
*
* @param    wpData pointer where uint8_t will be saved
* 			wDataSize how long is the message data
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_FEATURES_UPDATES_Data8(uint8_t * bpData,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	if(wDataSize != iAP2_FEATURES_UPDATES_BYTE)
	{
		*bpMessageCurrentStatus |=  iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED;
		*bpCurrentFrame = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
	}
	else
	{
		*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(bpData,iAP2_FEATURES_UPDATES_BYTE);
	}
}

/** ***********************************************************************************************
*
* @brief    Function used to save enum data from a message, the validation of the correct data size is done too.
* 
* @param    wpData pointer where enum will be saved
* 			wbMaxValue indicates bigger enum value
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_FEATURES_UPDATES_Data8_Enum(uint8_t * bpData,uint8_t bmaxValue,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	uint8_t tmpValueData;
	
	(void)bmaxValue;
	
	if((wDataSize != iAP2_FEATURES_UPDATES_BYTE))
	{
		*bpMessageCurrentStatus |= iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED;
		*bpCurrentFrame = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
	}
	else
	{
		*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(&tmpValueData,iAP2_FEATURES_UPDATES_BYTE);
		*bpData=tmpValueData;
	}
}

/** ***********************************************************************************************
*
* @brief    Function used to save uint16 data from a message, the validation of the correct data size is done too.
* 
* @param   	wpData pointer where uint16_t will be saved
* 			wDataSize how long is the message data
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_FEATURES_UPDATES_Data16(uint16_t * wpData,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{	
	if(wDataSize != iAP2_FEATURES_UPDATES_BYTES_PER_WORD)
	{
		*bpMessageCurrentStatus |= iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED;
		*bpCurrentFrame = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
	}
	else
	{
		*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData((uint8_t *)wpData,iAP2_FEATURES_UPDATES_BYTES_PER_WORD);
		if(IAP2_SESS_CTRL_DATA_PENDING != *bpCurrentFrame)
		{
		    BYTESWAP16(*wpData,*wpData);
		}
	}
}

/** ***********************************************************************************************
*
* @brief    Function used to save uint32 data from a message, the validation of the correct data size is done too.
* 
* @param   	wpData pointer where uint32_t will be saved
* 			wDataSize how long is the message data
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_FEATURES_UPDATES_Data32(uint32_t * dwpData,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	if(wDataSize != iAP2_FEATURES_UPDATES_BYTES_PER_DWORD)
	{
		*bpMessageCurrentStatus |= iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED;
		*bpCurrentFrame = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
	}
	else
	{
		*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData((uint8_t *)dwpData,iAP2_FEATURES_UPDATES_BYTES_PER_DWORD);
		if(IAP2_SESS_CTRL_DATA_PENDING != *bpCurrentFrame)
        {
		    BYTESWAP32(*dwpData,*dwpData);
        }
	}
}

/** ***********************************************************************************************
*
* @brief    Function used to save uint64 data from a message, the validation of the correct data size is done too.
* 
* @param   	qwpData pointer where uint64_t will be saved
* 			wDataSize how long is the message data
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_FEATURES_UPDATES_Data64(uint64_t * qwpData,uint16_t wDataSize,uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	if(wDataSize != iAP2_FEATURES_UPDATES_BYTES_PER_QWORD)
	{
		*bpMessageCurrentStatus |=  iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED;
		*bpCurrentFrame = IAP2_SESS_CTRL_MESSAGE_COMPLETE;
	}
	else
	{
		*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData((uint8_t *)qwpData,iAP2_FEATURES_UPDATES_BYTES_PER_QWORD);
	}
}

/** ***********************************************************************************************
*
* @brief    Function used to save utf8 data from a message, the validation of the correct data size according to
*  			information from original structure is done too.
* 
* @param   	wpData pointer where utf8 will be saved
* 			bmaxValue it is used to know how long the string could be
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_FEATURES_UPDATES_DataUtf8(uint8_t * bpData, uint8_t bmaxSize, uint16_t wDataSize, uint8_t * bpCurrentFrame)
{
		if(wDataSize > bmaxSize)
		{
			wDataSize = bmaxSize;
		}
		if( 0 != bmaxSize)
		{
			wDataSize--; /*Only read data size - 1 to add null character at the end of the utf8 */
			bpData[wDataSize]=0;
		}
		*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData(bpData,wDataSize);

}

/** ***********************************************************************************************
*
* @brief    Function used to save enum (uint8_t) data from a message.
* 
* @param   	*vpsubParameter pointer where enum will be saved
* 			bmaxValue the possible maximum value for this enum
* 			bsubParamFlag Indicates the flag to be set once the value was validated and saved.
* 			wpFlagToSet Flag to be set after copy data operation
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status

* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum(void* vpsubparamvalue,uint8_t bmaxValue ,uint32_t wsubParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
		                 uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	iAP2_FEATURES_UPDATES_Data8_Enum((uint8_t *)vpsubparamvalue,bmaxValue,wDataSize,bpMessageCurrentStatus,bpCurrentFrame);
	if(!(*bpMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED ))
	{
		if(NULL != wpFlagToSet)
		{
			(*wpFlagToSet) |= wsubParamFlag;
		}
	}
	/* Used to avoid an un-used parameter warning. 
	 * Parameters list is needed this way to use the function within a function pointer array*/
	(void)bmaxValue; 
}

/** ***********************************************************************************************
*
* @brief    Function used to save utf8 data from a message.
* 
* @param   	vpsubParameter pointer where utf8 will be saved
* 			bmaxValue it is used to know utf8 data length.
* 			bsubParamFlag Indicates the flag to be set once the value was validated and saved.
* 			wpFlagToSet Flag to be set after copy data operation
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8(void * vpsubparamVal,uint8_t bsubparamMaxSize ,uint32_t wsubParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	//iAP2_FEATURES_UPDATES_DataUtf8((uint8_t *)(*(uint32_t *)vpsubparamVal),bsubparamMaxSize,wDataSize,bpMessageCurrentStatus,bpCurrentFrame);
	iAP2_FEATURES_UPDATES_DataUtf8((uint8_t *)(*(uint32_t *)vpsubparamVal), bsubparamMaxSize, wDataSize, bpCurrentFrame);
	if(!(*bpMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED ))
	{
		if(NULL != wpFlagToSet)
		{
			(*wpFlagToSet) |= wsubParamFlag;
		}
	}
}

/** ***********************************************************************************************
*
* @brief    Function used to save uint8 data from a message.
* 
* @param    vpsubParameter pointer where uint8_t will be saved
* 			bmaxValue it is not used, it could be NULL
* 			bsubParamFlag Indicates the flag to be set once the value was validated and saved.
* 			wpFlagToSet Flag to be set after copy data operation
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8(void* subparamVal,uint8_t bmaxValue,uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	iAP2_FEATURES_UPDATES_Data8((uint8_t *)subparamVal,wDataSize,bpMessageCurrentStatus,bpCurrentFrame);
	if(!(*bpMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED ))
	{
		if(NULL != wpFlagToSet)
		{
			(*wpFlagToSet) |= subParamFlag;
		}
	}
	/* Used to avoid an un-used parameter warning. 
	 * Parameters list is needed this way to use the function within a function pointer array*/
	(void)bmaxValue;
}

/** ***********************************************************************************************
*
* @brief    Function used to save uint16 data from a message.
* 
* @param    vpsubParameter pointer where uint32_t will be saved
* 			bmaxValue it is not used, it could be NULL
* 			bsubParamFlag Indicates the flag to be set once the value was validated and saved.
* 			wpFlagToSet Flag to be set after copy data operation
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16(void * subparamvalue, uint8_t maxValue, uint32_t subParamFlag, uint32_t* wpFlagToSet,
		uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	iAP2_FEATURES_UPDATES_Data16((uint16_t *)subparamvalue,wDataSize,bpMessageCurrentStatus,bpCurrentFrame);
	if(!(*bpMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED ))
	{
		if(NULL != wpFlagToSet)
		{
			(*wpFlagToSet) |= subParamFlag;
		}
	}
	/* Used to avoid an un-used parameter warning. 
	 * Parameters list is needed this way to use the function within a function pointer array*/
	(void)maxValue;
}

/** ***********************************************************************************************
*
* @brief    Function used to save uint32 data from a message.
* 
* @param   *vpsubParameter pointer where uint32_t will be saved
* 			bmaxValue it is not used, it could be NULL
* 			bsubParamFlag Indicates the flag to be set once the value was validated and saved.
* 			wpFlagToSet Flag to be set after copy data operation
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32(void* subparamVal,uint8_t subparamSize ,uint32_t subParamFlag, uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	iAP2_FEATURES_UPDATES_Data32((uint32_t *)subparamVal,wDataSize,bpMessageCurrentStatus,bpCurrentFrame);
	if(!(*bpMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED ))
	{
		if(NULL != wpFlagToSet)
		{
			(*wpFlagToSet) |= subParamFlag;
		}
	}
	/* Used to avoid an un-used parameter warning. 
	 * Parameters list is needed this way to use the function within a function pointer array*/
	(void)subparamSize;
}

/** ***********************************************************************************************
*
* @brief    Function used to save uint64 data from a message. A swap is not apply to this data.
*  
* @param   *vpsubParameter pointer where uint64_t will be saved
* 			bmaxValue it is not used, it could be NULL
* 			bsubParamFlag Indicates the flag to be set once the value was validated and saved.
* 			wpFlagToSet Flag to be set after copy data operation
* 			bpMessageCurrentStatus pointer used to know the current message status
* 			bpCurrentFrame pointer used to know current frame status
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64(void* subparamVal,uint8_t subparamSize ,uint32_t subParamFlag,uint32_t *wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	iAP2_FEATURES_UPDATES_Data64((uint64_t *)subparamVal,wDataSize,bpMessageCurrentStatus,bpCurrentFrame);
	if(!(*bpMessageCurrentStatus & iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED ))
	{
		if(NULL != wpFlagToSet)
		{
			(*wpFlagToSet) |= subParamFlag;
		}
	}
	/* Used to avoid an un-used parameter warning. 
	 * Parameters list is needed this way to use the function within a function pointer array*/
	(void)subparamSize;
}

/** ***********************************************************************************************
*
* @brief    Function called when a unknown parameter ID is received
* 
* @param   	All parameters are not used, they could be NULL
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_FEATURES_UPDATES_InvalidParameter(void* subparamvalue,uint8_t maxValue, uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame)
{
	*bpCurrentFrame = iAP2_Sessions_bfnReadCtrlMsgParamData((uint8_t *)&subparamvalue,0);
	/* Used to avoid an un-used parameter warning. 
	 * Parameters list is needed this way to use the function within a function pointer array*/
	(void)subParamFlag; 
	(void)maxValue;
	(void)wpFlagToSet;
	(void)wDataSize;
	(void)bpMessageCurrentStatus;
}


