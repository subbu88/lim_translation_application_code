/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Authentication_Feat.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void iAP2_Auth_Feat_vfnNewRequestCaller(void(*fnPtr)(uint16_t), uint16_t MessageID);

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/** ***********************************************************************************************
*
* @brief    Function to be called by the arbitrator session layer when a new message that
* 			corresponds to authentication feature arrives.
* 
* @param   	MessageID		Message of authentication feature.
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_Auth_Feat_vfnCallBack(uint16_t MessageID)
{
	/* Call a function in the upper layer (MFI Interface) to pass it a new message */
	iAP2_Auth_Feat_vfnNewRequestCaller(&iAP2_Host_vfnNewAuthenticationMessage, MessageID);
}

/** ***********************************************************************************************
*
* @brief    Function that sends the authentication certificate data to the lower layer of session. 
* 
* @param   	*CertificateData		Pointer to the certificate data.
* 			wCertificateDataSize    Size of the certificate data.
* 
* @return   ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_Auth_Feat_bfnSendAuthenticationCertificate(uint8_t *CertificateData, uint16_t wCertificateDataSize)
{
	uint8_t bReturnValue;

	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{

		/* Call a function in the lower layer of session to send the certificate data */
		bReturnValue = iAP2_Sessions_bfnSendCtrlMessage(IAP2_AUTHEN_FEAT_SEND_CERT_ID, CertificateData, wCertificateDataSize, 0x0500);

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bReturnValue = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bReturnValue = SESSIONS_LOCK_FAIL;
	}

	/* Return the result of sending data to lower layers */
	return bReturnValue;
}

/** ***********************************************************************************************
*
* @brief    Function that sends the authentication challenge response data to the lower layer of session. 
* 
* @param   	*ResponseData			Pointer to the challenge response data.
* 			wResponseDataSize       Size of the challenge response data.
* 
* @return  	ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_Auth_Feat_bfnSendAuthenticationResponse(uint8_t *ResponseData, uint16_t wResponseDataSize)
{
	uint8_t bReturnValue;
	

	int lockStatus;
	
	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
	
	if(ERRCODE_NO_ERROR == lockStatus)
	{
	
	/* Call a function in the lower layer of session to send the challenge response data */
	bReturnValue = iAP2_Sessions_bfnSendCtrlMessage(IAP2_AUTHEN_FEAT_SEND_RESP_ID, ResponseData, wResponseDataSize, 0x0080 + IAP2_SESSIONS_CTRL_HEADER(1));
	

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bReturnValue = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bReturnValue = SESSIONS_LOCK_FAIL;
	}
	
	/* Return the result of sending data to lower layers */
	return bReturnValue;
}

/** ***********************************************************************************************
*
* @brief    Function that is called by an upper layer to get the parameters and data of new messages. 
* 
* @param   	*wParameterID			Pointer to parameter ID.
* 			*wParameterData			Pointer to parameter data.
* 			*wParamSize				Pointer to parameter size.
* 
* @return   IAP_SESSIONS_ASK_NEXT_PARAM - IAP_SESSIONS_LAST_PARAM_WAIT_NEXT_FRAME  Value that depends on whether the data was sent or not.
* 			IAP_SESSIONS_LAST_SUBPARAM - IAP_SESSIONS_LAST_MSG_PARAMETER     	
*          
**************************************************************************************************/
uint8_t iAP2_Auth_Feat_bfnGetChallengeData(uint16_t* wParameterID, void* wParameterData, uint16_t* wParamSize)
{
	uint8_t bReturnStatus; 
	/* Get the parameter ID and its size */
	bReturnStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(wParameterID, wParamSize);
	/* If ok read the parameter data */
	if(SESSIONS_OK == bReturnStatus)
	{
		bReturnStatus = iAP2_Sessions_bfnReadCtrlMsgParamData(wParameterData, (uint16_t)*wParamSize);
	}
	return bReturnStatus;
}

/** ***********************************************************************************************
*
* @brief    Function that call a a funtion . 
* 
* @param   	uint8_t(*fnPtr)(uint16_t)			Pointer to a function in a upper layer that is to be called.
* 			MessageID      			Message of authentication feature.
* 
* @return  	uint8_t
*          
**************************************************************************************************/
static void iAP2_Auth_Feat_vfnNewRequestCaller(void(*fnPtr)(uint16_t), uint16_t MessageID)
{
	fnPtr(MessageID);
}
