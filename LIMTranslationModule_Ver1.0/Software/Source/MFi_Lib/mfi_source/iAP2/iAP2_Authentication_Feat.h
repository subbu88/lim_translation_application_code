/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_AUTHENTICATION_FEAT_H_
#define IAP2_AUTHENTICATION_FEAT_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
/** @todo iAP_Auth_Feat - EM: The feature should not include the host for any reason */
#include "iAP2_Host.h"
/** @todo iAP_Auth_Feat - EM: This inclution is temporary just until it is defined what to do with the status codes */
#include "iAP2_Sessions_Control.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP2_AUTHEN_FEAT_REQ_AUTH_ID					(0xAA00)
#define IAP2_AUTHEN_FEAT_SEND_CERT_ID					(0xAA01)
#define IAP2_AUTHEN_FEAT_CHALLENGE_ID					(0xAA02)
#define IAP2_AUTHEN_FEAT_SEND_RESP_ID					(0xAA03)
#define IAP2_AUTHEN_FEAT_AUTH_FAILED_ID					(0xAA04)
#define IAP2_AUTHEN_FEAT_AUTH_SUCCEEDED_ID				(0xAA05)

#define IAP2_AUTHEN_CHALLENGE_PARAMETER					(0x0000)

#define MAX_DATA_SIZE		0x0000

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void iAP2_Auth_Feat_vfnCallBack(uint16_t MessageID);
uint8_t iAP2_Auth_Feat_bfnSendAuthenticationCertificate(uint8_t *CertificateData, uint16_t CertificateDataSize);
uint8_t iAP2_Auth_Feat_bfnSendAuthenticationResponse(uint8_t *ResponseData, uint16_t CResponseDataSize);
uint8_t iAP2_Auth_Feat_bfnGetChallengeData(uint16_t* wParameterID, void* wParameterData, uint16_t* wParamSize);


/*************************************************************************************************/

#endif /* IAP2_AUTHENTICATION_FEAT_H_ */
