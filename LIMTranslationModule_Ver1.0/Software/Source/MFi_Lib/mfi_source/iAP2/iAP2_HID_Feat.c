/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_HID_Feat.h"
#include "iAP2_Sessions_Control.h"
#include "osa_common.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define IAP2_HID_MP_COMP_ID		1

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/
static struct /**< used to store the data of param id 0x6800 */
{
	uint16_t wHIDComponentIdentifier; /**< used to store HIDComponentIdentifier data */
	uint16_t wSizeHIDReportDescriptor; /**< used to know the size of HIDReportDescriptor data */
	uint8_t LocalizedKeyBoardCountryCode; /**< used to to pint to LocalizedKeyBoardCountryCode data */
	uint8_t *HIDReportDescriptor; /**< used to point to HIDReportDescriptor data */
}sStartHID;

static struct /**< used to store the data of param id 0x6802 */
{
	uint16_t wHIDComponentIdentifier; /**< used to store HIDComponentIdentifier data */
	uint16_t wSizeHIDReport; /**< used to store the size of HIDReport data */
	uint8_t *HIDReport; /**< used to point to HIDReport data */
}sAccHID;

typedef enum
{
	HID_FEAT_PARAM_PENDING = 0,		/**< used to let the user know when new data is pending */
	HID_FEAT_FIRST_PARAM_REC,
	HID_FEAT_SECOND_PARAM_REC
}_iAP2_HID_Feat_Status;
/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
static const uint16_t aiAP2_HID_Feat_SizeParameters[] = {0x0002, 0x0002, 0x0002, KEY_BOARD_COUNTRY_CODE_SIZE};
static const uint16_t aiAP2_HID_Feat_ParameterID[] = {0x0000, 0x0001, 0x0002, 0x0003, 0x0004};
static const uint16_t bVendorIdentifier = 0x2C2B;
static const uint16_t bProductIdentifier = 0x0001;
static uint8_t *aiAP2_HID_Feat_StartParameterData[] = {(uint8_t*)&sStartHID.wHIDComponentIdentifier,
													(uint8_t*)&bVendorIdentifier,
													(uint8_t*)&bProductIdentifier,
													&sStartHID.LocalizedKeyBoardCountryCode};
static uint8_t *aiAP2_HID_Feat_AccParameterData[] = {(uint8_t*)&sAccHID.wHIDComponentIdentifier,
												(uint8_t*)&sAccHID.wSizeHIDReport};

#if IAP2_HID_MEDIA_PLAYBACK_EN == _TRUE_
static const uint8_t iAP2_HID_MediaPlaybackDesc[] = {
    0x05, 0x0c,                    // USAGE_PAGE (Consumer Devices)
    0x09, 0x01,                    // USAGE (Consumer Control)
    0xa1, 0x01,                    // COLLECTION (Application)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x25, 0x01,                    //   LOGICAL_MAXIMUM (1)
    0x75, 0x01,                    //   REPORT_SIZE (1)
    0x95, IAP2_HID_USAGE_CNT,      //   REPORT_COUNT (IAP2_HID_USAGE_CNT)
#if IAP2_HID_PLAY_EN
    0x09, 0xb0,                    //   USAGE (Play)
#endif
#if IAP2_HID_PAUSE_EN
    0x09, 0xb1,                    //   USAGE (Pause)
#endif
#if IAP2_HID_PLAY_PAUSE_EN
    0x09, 0xcd,                    //   USAGE (Play/Pause)
#endif
#if IAP2_HID_SCAN_NEXT_TRACK_EN
    0x09, 0xb5,                    //   USAGE (Scan Next Track)
#endif
#if IAP2_HID_SCAN_PREV_TRACK_EN
    0x09, 0xb6,                    //   USAGE (Scan Previous Track)
#endif
#if IAP2_HID_VOL_UP_EN
    0x09, 0xe9,                    //   USAGE (Volume Up)
#endif
#if IAP2_HID_VOL_DOWN_EN
    0x09, 0xea,                    //   USAGE (Volume Down)
#endif
#if IAP2_HID_RANDOM_EN
    0x09, 0xb9,                    //   USAGE (Random Play)
#endif
#if IAP2_HID_REPEAT_EN
    0x09, 0xbc,                    //   USAGE (Repeat)
#endif
#if IAP2_HID_TRACK_NORM_EN
    0x09, 0xbe,                    //   USAGE (Track Normal)
#endif
#if IAP2_HID_TRACK_INC_EN
    0x09, 0xca,                    //   USAGE (Tracking Increment)
#endif
#if IAP2_HID_TRACK_DEC_EN
    0x09, 0xcb,                    //   USAGE (Tracking Decrement)
#endif
#if IAP2_HID_MUTE_EN
    0x09, 0xe2,                    //   USAGE (Mute)
#endif
    0x81, 0x02,                    //   INPUT (Data,Var,Abs)
    0x75, IAP2_HID_PADDING_CNT,    //   REPORT_SIZE (IAP2_HID_PADDING_CNT)
    0x95, 0x01,                    //   REPORT_COUNT (1)
    0x81, 0x03,                    //   INPUT (Cnst,Var,Abs)
    0xc0                           // END_COLLECTION
};
#endif
/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
#if IAP2_HID_MEDIA_PLAYBACK_EN == _TRUE_
static uint16_t iAP2_HID_MPReport;
#endif

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
/** ***********************************************************************************************
*
* @brief    Function that sends the start HID message. 
* 
* @param   	wHIDComponentIdentifier 			ID of the component.
* 			wSizeHIDReportDescriptor 			Size of the HID Report descriptor
* 			LocalizedKeyBoardCountryCode 		Keyboard country code
* 			*HIDReportDescriptor				Pointer to HID Report Descriptor
* 
* @return   ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnStartHID(uint16_t wHIDComponentIdentifier, uint16_t wSizeHIDReportDescriptor, \
		uint8_t LocalizedKeyBoardCountryCode, uint8_t *HIDReportDescriptor)
{

	uint8_t bReturnStatus;
	uint8_t bControl;

	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{

		bReturnStatus = 0;
		bControl = 0;
		BYTESWAP16(wHIDComponentIdentifier,sStartHID.wHIDComponentIdentifier);
		BYTESWAP16(wSizeHIDReportDescriptor,sStartHID.wSizeHIDReportDescriptor);
		BYTESWAP16(wHIDComponentIdentifier,sStartHID.wHIDComponentIdentifier );
		BYTESWAP16(wSizeHIDReportDescriptor,sStartHID.wSizeHIDReportDescriptor);
		sStartHID.LocalizedKeyBoardCountryCode = LocalizedKeyBoardCountryCode;
		sStartHID.HIDReportDescriptor = HIDReportDescriptor;

		/* Ask for space to send data */
		bReturnStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_HID_FEAT_START, NULL, \
				IAP2_HID_FEAT_NO_DATA, 0x07 + wSizeHIDReportDescriptor + IAP2_SESSIONS_CTRL_HEADER(5));

		/* If there is space */
		if(SESSIONS_OK == bReturnStatus)
		{
			do
			{	
				/* Check if country code will be sent or not */
				if((!KEY_BOARD_COUNTRY_CODE_PRESENT) && KEY_BOARD_COUNTRY_PARAMETER_ID == bControl)
				{
					bControl++;
				}
				/* HID descriptor is blob so the size changes */
				else if(HID_REPORT_PARAMETER_ID == bControl)
				{
					bReturnStatus = iAP2_Sessions_bfnAddCtrlMsgParam(aiAP2_HID_Feat_ParameterID[bControl],  \
							IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, sStartHID.HIDReportDescriptor, 
							wSizeHIDReportDescriptor); 
					bControl++;	
				}
				/* Send the other four parameters */
				else
				{
					bReturnStatus = iAP2_Sessions_bfnAddCtrlMsgParam(aiAP2_HID_Feat_ParameterID[bControl], \
							IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, aiAP2_HID_Feat_StartParameterData[bControl], 
							aiAP2_HID_Feat_SizeParameters[bControl]);
					bControl++;
				}
			}while(SESSIONS_OK == bReturnStatus && NUMBER_PARAMETERS_START_HID >= bControl); /* If the there is no error and it is not the last parameter continue */

			/* If previous processes were correctly run, stop lower session setting */
			if(SESSIONS_OK == bReturnStatus)
			{
				bReturnStatus = iAP2_Sessions_bfnEndSendCtrlMsg();
			}
			else
			{
				iAP2_Sessions_vfnResetMessage();
			}
		}
		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bReturnStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bReturnStatus = SESSIONS_LOCK_FAIL;
	}
	/* Return the status of the operations */
	return bReturnStatus;
}

/** ***********************************************************************************************
*
* @brief    Function that sends the accessory HID report message. 
* 
* @param   	wHIDComponentIdentifier 			ID of the component.
* 			wSizeHIDReport			 			Size of the HID Report descriptor
* 			*HIDReport							Pointer to HID report
* 
* @return   ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnAccessoryHIDReport(uint16_t wHIDComponentIdentifier, uint16_t wSizeHIDReport, uint8_t *HIDReport)
{
	uint8_t bReturnStatus;
	uint8_t bControl;


	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{


		bReturnStatus = 0;
		bControl = 0;
		/* Store values in a local structure */
		BYTESWAP16(wHIDComponentIdentifier,sAccHID.wHIDComponentIdentifier);
		sAccHID.wSizeHIDReport = wSizeHIDReport;
		sAccHID.HIDReport = HIDReport;

		/* Ask for space to send data */
		bReturnStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_HID_FEAT_ACC_HID_REPORT, NULL, IAP2_HID_FEAT_NO_DATA, 2 + wSizeHIDReport + IAP2_SESSIONS_CTRL_HEADER(2));

		if(SESSIONS_OK == bReturnStatus)
		{
			do
			{
				/* If bControl is Acc_HID parameter */
				if(NUMBER_PARAMETERS_ACC_HID == bControl)
				{
					bReturnStatus = iAP2_Sessions_bfnAddCtrlMsgParam(aiAP2_HID_Feat_ParameterID[bControl], \
							IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM,sAccHID.HIDReport, sAccHID.wSizeHIDReport);
					bControl++;
				}
				/* If not send the other parameter  */
				else
				{
					bReturnStatus = iAP2_Sessions_bfnAddCtrlMsgParam(aiAP2_HID_Feat_ParameterID[bControl], \
							IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, aiAP2_HID_Feat_AccParameterData[bControl],
							aiAP2_HID_Feat_SizeParameters[bControl]);
					bControl++;
				}
			}while(SESSIONS_OK == bReturnStatus && NUMBER_PARAMETERS_ACC_HID >= bControl); /* No errors and all parameters sent  */
		}
		/* If previous processes were correctly run, stop lower session setting */
		if(SESSIONS_OK == bReturnStatus)
		{
			bReturnStatus = iAP2_Sessions_bfnEndSendCtrlMsg();
		}
		else
		{
			iAP2_Sessions_vfnResetMessage();
		}

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bReturnStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bReturnStatus = SESSIONS_LOCK_FAIL;
	}
	return bReturnStatus;
}

/** ***********************************************************************************************
*
* @brief    Function that sends the stop HID message. 
* 
* @param   	wHIDComponentIdentifier			ID of the component.
* 
* @return   ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnStopHID(uint16_t wHIDComponentIdentifier)
{
	uint8_t bReturnStatus;
	uint8_t bControl;

	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{	

		bReturnStatus = 0;
		bControl = 0;
		BYTESWAP16(wHIDComponentIdentifier,wHIDComponentIdentifier);
		/* Send stop HID message */
		bReturnStatus = iAP2_Sessions_bfnSendCtrlMessage(IAP2_HID_FEAT_STOP_HID, (uint8_t*)&wHIDComponentIdentifier, aiAP2_HID_Feat_SizeParameters[bControl], 2 + IAP2_SESSIONS_CTRL_HEADER(1));

		if(SESSIONS_OK != bReturnStatus)
		{

			iAP2_Sessions_vfnResetMessage();
		}

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bReturnStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bReturnStatus = SESSIONS_LOCK_FAIL;
	}
	return bReturnStatus;
}

/** ***********************************************************************************************
*
* @brief    Function that calls a function of an upper layer when a HID message arrives. 
* 
* @param   	wMessageID			New Message.
* 
* @return   void
*          
**************************************************************************************************/
void iAP2_HID_Feat_vfnDeviceHIDReport(uint16_t wMessageID)
{
	/* Call a function in the app layer  */
	(void)wMessageID;
}

/** ***********************************************************************************************
*
* @brief    Function that is called by an upper layer to get the parameters and data of new messages. 
* 
* @param   	*wParameterID			Pointer to parameter ID.
* 			*wParameterData			Pointer to parameter data.
* 			*wParamSize				Pointer to parameter size.
* 
* @return   ERROR - BUSY - _OK_     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnGetParamData(uint16_t* wParameterID, void* wParameterData, uint16_t* wpParamSize)
{
	uint8_t bReturnStatus; 
	/* Get the parameter ID and its size */
	bReturnStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(wParameterID, wpParamSize);
	/* If size of component ID is not correct return size error */
	if(*wParameterID == HID_COMPONENT_ID && *wpParamSize != HID_COMPONENT_ID_SIZE)
	{
		bReturnStatus = SIZE_ERROR;
	}
	/* If ok read the parameter data */
	if(_OK_ == bReturnStatus)
	{
		bReturnStatus = iAP2_Sessions_bfnReadCtrlMsgParamData(wParameterData, *wpParamSize);
	}
	return bReturnStatus;
}

#if IAP2_HID_MEDIA_PLAYBACK_EN == _TRUE_
/** ***********************************************************************************************
*
* @brief    Function that is called by the application to start using the media playback HID component. 
* 
* @param   	*wParameterID			Pointer to parameter ID.
* 			*wParameterData			Pointer to parameter data.
* 			*wParamSize				Pointer to parameter size.
* 
* @return   ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnMediaPBStart(void)
{
	uint8_t bStatus;
	bStatus = iAP2_HID_Feat_bfnStartHID(IAP2_HID_MP_COMP_ID, sizeof(iAP2_HID_MediaPlaybackDesc), 0, 
										(uint8_t *)iAP2_HID_MediaPlaybackDesc);
	
	iAP2_HID_MPReport = 0;
	
	return bStatus;
}

/** ***********************************************************************************************
*
* @brief    Function that is called by the application to start using the media playback HID component. 
* 
* @param   	*wParameterID			Pointer to parameter ID.
* 			*wParameterData			Pointer to parameter data.
* 			*wParamSize				Pointer to parameter size.
* 
* @return   ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnMediaPBPressButton(uint8_t bButton)
{
	uint8_t bStatus;
	
	iAP2_HID_MPReport |= (1 << bButton);
	bStatus = iAP2_HID_Feat_bfnAccessoryHIDReport(IAP2_HID_MP_COMP_ID, sizeof(iAP2_HID_MPReport),
												 (uint8_t*)&iAP2_HID_MPReport);
	
	return bStatus;
}

/** ***********************************************************************************************
*
* @brief    Function that is called by the application to start using the media playback HID component. 
* 
* @param   	*wParameterID			Pointer to parameter ID.
* 			*wParameterData			Pointer to parameter data.
* 			*wParamSize				Pointer to parameter size.
* 
* @return   ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnMediaPBReleaseButton(uint8_t bButton)
{
	uint8_t bStatus;
	
	iAP2_HID_MPReport &= ~(1 << bButton);
	bStatus = iAP2_HID_Feat_bfnAccessoryHIDReport(IAP2_HID_MP_COMP_ID, sizeof(iAP2_HID_MPReport),
																							(uint8_t*)&iAP2_HID_MPReport);
	
	return bStatus;
}

/** ***********************************************************************************************
*
* @brief    Function that is called by the application to start using the media playback HID component. 
* 
* @param   	*wParameterID			Pointer to parameter ID.
* 			*wParameterData			Pointer to parameter data.
* 			*wParamSize				Pointer to parameter size.
* 
* @return   ERROR - BUSY - OK     	Value that depends on whether the data was sent or not.
*          
**************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnMediaPBStop(void)
{
	uint8_t bStatus;
	
	bStatus = iAP2_HID_Feat_bfnStopHID(IAP2_HID_MP_COMP_ID);
	
	return bStatus;
}
#endif
