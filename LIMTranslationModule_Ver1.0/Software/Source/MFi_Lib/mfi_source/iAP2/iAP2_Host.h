/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_HOST_H_
#define IAP2_HOST_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Identification_Feat.h"
#include "osa_common.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP2H_MAX_ACC_AUTH_RETRIES			(5)
#define USB_IOS_ATTACH                      (1<<1)
#define USB_ANDROID_ATTACH					(1<<7)
#define UART_ATTACH							(0x01)
#define USB_ROLE_SWITCH_DONE_MASK			(1<<5)
#define USB_ROLE_CHANGED_MASK				(1<<6)


#define IAP2_HOST_INIT_OK						0
#define IAP2_HOST_INIT_FAILURE					0xFF
/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef enum
{
//    IAP2H_ACC_AUTHENTICATION_REQUIRED = 0,
//    IAP2H_ACC_AUTHENTICATION_CHALLENGE_REQUIRED,
    IAP2H_ACC_AUTHENTICATION_FAILED,
    IAP2H_ACC_AUTHENTICATION_SUCCEEDED,
    IAP2H_ACC_AUTHENTICATION_DONE,
//    IAP2H_ID_START,
    IAP2H_ID_ACCEPTED,
    IAP2H_ID_REJECTED,
    IAP2H_INTERNAL_ERROR,
    IAP2H_IAP1_DETECTED,
//    IAP2H_INIT_COMPLETE,
    IAP2H_INIT_SENT,
    IAP2H_ACC_CERTIFICATE_SENT,
    IAP2_CREATE_CHALLENGE_START,
//    IAP2H_LSP_RECEIVED,
    IAP2H_ROLE_CHANGED,
    IAP2H_POWER_SENT
}_eHost2Status;

typedef enum
{
    IAP2_DEVICE_ATTACH_DET_STATE = 0,               /* 0x00 */
    IAP2_WAIT_LINK_INIT_STATE,
#ifdef IAPI_INTERFACE_USB_ROLE_SWITCH           
    IAP2_ROLE_SWITCH_STATE,                         /* 0x01 */
    IAP2_ROLE_SWITCH_WAIT_STATE,                    /* 0x02 */
#endif
    IAP2_PROTOCOL_INIT_STATE,                       /* 0x03 */
    IAP2_LINK_SYNC_PAYLOAD_STATE,                   /* 0x04 */
    IAP2_READ_ACC_CERTIFICATE_STATE,                /* 0x05 */
    IAP2_SEND_ACC_CERTIFICATE_STATE,                /* 0x06 */
    IAP2_CREATE_ACC_CHALLENGE_RESPONSE_STATE,       /* 0x07 */
    IAP2_SEND_ACC_CHALLENGE_RESPONSE_STATE,         /* 0x08 */
    IAP2_ACC_AUTHENTICATION_ERROR_STATE,            /* 0x09 */
    IAP2_ACC_AUTHENTICATION_STATUS_STATE,           /* 0x0A */
    IAP2_START_IDENTIFICATION_STATE,                /* 0x0B*/
    IAP2_IDLE_STATE,
    IAP1_STATE                                      /* 0x0C */
#if (IAP2_IDENT_PWR_SOURCE_TYPE_DATA != IAP2_PWR_TYPE_NONE)
    ,IAP2_POWER_STATE                               /* 0x0E */
#endif
#if IAP2_CTRL_MSG_APP_LAUNCH != 0
	,IAP2_APP_LAUNCH_STATE                          /* 0x0E */
#endif
}_eHost2States;

typedef enum
{
    IAP2H_ACC_AUTHENTICATION_ERROR,
    IAP2H_ACC_CHALLENGE_DATA_NOT_READ_ERROR
}_eHost2InternalErrors;

typedef enum 
{
    IAP2H_ACC_AUTHENTICATION_REQUIRED = 0,
    IAP2H_ACC_AUTHENTICATION_CHALLENGE_REQUIRED,  
    IAP2H_LSP_RECEIVED,
    IAP2H_IAP2_DETECTED,
    IAP2H_INIT_COMPLETE,
    IAP2H_ID_START
}iAPHostEventsFlags_t;

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                Function-like Macros Section                                   
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>

#if (IAP2_IDENT_PWR_SOURCE_TYPE_DATA != IAP2_PWR_TYPE_NONE)
#define IAP2_HOST_MSG_ALLOWED                           (iAP2_Host_gwStatus & (1 << IAP2H_POWER_SENT))
#else
#define IAP2_HOST_MSG_ALLOWED                           (iAP2_Host_gwStatus & (1 << IAP2H_ID_ACCEPTED))
#endif

#define IAP_INIT_FAILED (iAP_init_failed)

#define IAP2_HOST_READ_ACCESSORY_CERTIFICATE            iAP_Auth_vfnReadAccessoryCertificate

#define IAP2_HOST_COPROCESSOR_BUSY                      IAP_AUTH_CP_BUSY

#define IAP2_HOST_SEND_CERTIFICATE                      iAP2_Auth_Feat_bfnSendAuthenticationCertificate
#define IAP2_HOST_SEND_CHALLENGE_RESPONSE               iAP2_Auth_Feat_bfnSendAuthenticationResponse
#define IAP2_HOST_READ_CHALLENGE_DATA                   iAP2_Auth_Feat_bfnGetChallengeData
#define IAP2_HOST_CREATE_CHALLENGE_RESPONSE             iAP_Auth_vfnAccessoryChallenge

#if !defined(IAPI_INTERFACE_USB_ROLE_SWITCH) && !defined(IAPI_INTERFACE_USB_HOST)
#define IAP2_HOST_CHECK_IF_UART_MODE                    (AccPwr1_GetFieldValue(AccPwr1_DeviceData,POWER))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                  Extern Constants Section                                     
///////////////////////////////////////////////////////////////////////////////////////////////////
//! <brief description>

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                  Extern Variables Section                                     
///////////////////////////////////////////////////////////////////////////////////////////////////

//! <brief description>
extern uint16_t iAP2_Host_gwStatus;
extern bool iAP_init_failed;

///////////////////////////////////////////////////////////////////////////////////////////////////
//                                Function Prototypes Section                                    
///////////////////////////////////////////////////////////////////////////////////////////////////

void iAP2_Host_vfnNewAuthenticationMessage(uint16_t wMessageID);
void iAP2_Host_vfnNewIDMessage(uint16_t wMessageID);


uint8_t iAP2_Host_bfnHostInitialization(MFIHostConfig *config);
uint8_t iAP2_Host_bfnHostUnInitialization(void);
#if (FSL_OS_NONE == FSL_OS_SELECTED)
void iAP2_Host_vfStateMachineDriver(void);
#else
void iAP2_Host_vfStateMachineDriver(void* param);
#endif

void iAP2_Host_bfnEventsCallback(uint8_t eventFlag);


#endif /* IAP2_HOST_H_ */
