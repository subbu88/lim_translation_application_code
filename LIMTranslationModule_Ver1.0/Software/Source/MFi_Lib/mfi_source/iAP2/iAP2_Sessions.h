/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_SESSIONS_H_
#define IAP2_SESSIONS_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "mfi_cfg.h"
#include "osa_common.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/* Macros to enable or disable each session. To be used by PEx. */
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_SESSIONS_FILE_TRANS_ENABLE			(_TRUE_)
#define IAP2_SESSIONS_EXT_ACC_ENABLE			(_TRUE_)

/* State machine enablers */
#define IAP2_SESSIONS_CTRL_SM_ENABLE			(_FALSE_)
#define IAP2_SESSIONS_FILE_TRANS_SM_ENABLE		(_FALSE_)
#define IAP2_SESSIONS_EXT_ACC_SM_ENABLE			(_TRUE_)

/* Reset function enablers */
#define IAP2_SESSIONS_CTRL_RESET_ENABLE			(_TRUE_)
#define IAP2_SESSIONS_FILE_TRANS_RESET_ENABLE	(_TRUE_)
#define IAP2_SESSIONS_EXT_ACC_RESET_ENABLE		(_TRUE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION

#define IAP_SESSIONS_NEW_FRAME_ERROR			(_TRUE_)
#define IAP_SESSIONS_NEW_FRAME_OK 				(_FALSE_)

#define IAP_SESSIONS_INIT_OK					(0)
#define IAP_SESSIONS_INIT_ERROR					(0xFF)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef enum
{
	IAP2_SESSIONS_RESERVED_SESSION_ID = 0,
	IAP2_SESSIONS_CTRL_MSG_SESSION_ID,
	IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID,
	IAP2_SESSIONS_EXTERNAL_ACC_SESSION_ID,
	IAP2_SESSIONS_SESSIONS_ID_BOUNDARY
}IAP2_SESSIONS_IDS;

typedef enum
{
	IAP2_SESSIONS_SM_STATUS_COMPLETE = 0,
	IAP2_SESSIONS_SM_STATUS_PENDING
}IAP2_SESSIONS_SM_STATUS;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

extern OsaMutex g_sessionLock[IAP2_SESSIONS_SESSIONS_ID_BOUNDARY - 1];

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
uint8_t iAP2_Sessions_bfnInit(void);
#if (FSL_OS_NONE == FSL_OS_SELECTED)
void iAP2_Sessions_vfnStateMachine(void);
#else
void iAP2_Sessions_vfnStateMachine(void* param);
#endif

void iAP2_Sessions_vfnReset(void);
uint8_t iAP2_Sessions_bfnDeInit(void);

/*************************************************************************************************/

#endif /* IAP2_SESSIONS_H_ */
