/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Sessions_File_Transfer.h"
#include "iAP2_Sessions_Control.h"
#include "iAP2_Sessions.h"
#include "iAP2_Link.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define IAP2_SESSIONS_FT_DEBUG						(_TRUE_)
#define IAP2_SESSIONS_FT_UNIT_TESTING				(_FALSE_)
#define IAP2_SESSIONS_FT_HEADER_BYTE_SIZE			(2)
#define IAP2_SESSIONS_FT_ID_INDEX					(0)
#define IAP2_SESSIONS_FT_DATAGRAM_TYPE_INDEX		(1)
#define IAP2_SESSIONS_FT_FILE_SIZE_BYTE_SIZE		(8)
/* @todo JS - IMPORTANT! */
/* @todo JS - IMPORTANT! */
/* @todo JS - IMPORTANT: Make the support of 255 FT IDs in a more memory efficient way! Consider ignoring the range 0 - 128 IDs */
#define IAP2_SESSIONS_FT_MAX_NUM_IDENTIFIERS		(255)
#define IAP2_SESSIONS_FT_SETUP_DTG_BYTE_SIZE		(10)
#define IAP2_SESSIONS_FT_COMMON_DTG_BYTE_SIZE		(2)
#define IAP2_SESSIONS_FT_MAX_PAYLOAD_BYTE_SIZE		(65523)
#define IAP2_SESSIONS_FT_SETUP_DTG_ID				(0x04)
#define IAP2_SESSIONS_FT_START_ID					(0x01)
#define IAP2_SESSIONS_FT_FIRST_DATA_ID				(0x80)
#define IAP2_SESSIONS_FT_FIRST_AO_DATA_ID			(0xC0)
#define IAP2_SESSIONS_FT_DATA_ID					(0x00)
#define IAP2_SESSIONS_FT_LAST_DATA_ID				(0x40)
#define IAP2_SESSIONS_FT_CANCEL_ID					(0x02)
#define IAP2_SESSIONS_FT_PAUSE_ID					(0x03)
#define IAP2_SESSIONS_FT_SUCCESS_ID					(0x05)
#define IAP2_SESSIONS_FT_FAILURES_ID				(0x06)
#define IAP2_SESSIONS_FT_STPDTG_FLSIZE_POS			(2)
/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
iAP2_FTSessionType iAP2_FTSessionFunctions = 
{
	&iAP2_CALLBACK_FT_RX_EACH_PACKET,
	&iAP2_CALLBACK_FT_NEW_TRANSFER,
	&iAP2_CALLBACK_FT_TRANSFER_END
};
/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

#if (IAP2_SESSIONS_FT_UNIT_TESTING == _TRUE_)
uint8_t* iAP2_FTSession_gbpaStoringAddress[IAP2_SESSIONS_FT_MAX_NUM_IDENTIFIERS];
uint8_t iAP2_FTSession_gbaIDs[IAP2_SESSIONS_FT_MAX_NUM_IDENTIFIERS];
uint8_t iAP2_FTSession_gbaStatus[IAP2_SESSIONS_FT_MAX_NUM_IDENTIFIERS];
#else
static uint8_t iAP2_FTSession_gbaStatus[IAP2_SESSIONS_FT_MAX_NUM_IDENTIFIERS];
#endif
static uint16_t	wLinkBufferSize = 0;
static uint16_t	wLinkBufferFree = 0;
static uint8_t	*bpLinkBuffer = NULL;
static uint16_t wLinkBufferIdx = 0;
/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static uint8_t bfnHandleWrongDatagram(uint8_t bFileTransferId);
static uint8_t bfnSendSetupDtg(FT_FILE_SEND_t *tpFileToSend);
static void vfnWriteDataToLinkBuffer(uint8_t* baData, uint16_t wDataSize, uint8_t bKeepLinkBufferIdx);
static uint8_t bfnMemoryRequest(uint16_t *wpDataSize, uint8_t bAtLeastDataSize, uint8_t *bpMemReqId);
static uint8_t bfnSendBuffer(uint8_t bMemRedIdToSend);
#if defined __CC_ARM
__inline static uint8_t bfnFileTransferSendMessage(FT_DATAGRAM_TYPE_t tDatagramType,
	FT_FILE_SEND_t *tpFileToSend);
__inline static void vfnResetStatusCtrls(void);
#else
inline static uint8_t bfnFileTransferSendMessage(FT_DATAGRAM_TYPE_t tDatagramType,
	FT_FILE_SEND_t *tpFileToSend);
inline static void vfnResetStatusCtrls(void);
#endif
static uint8_t bfnSendFileDataTo(void* vpReadPtr, uint16_t *wpReadIndex, uint16_t wSize, uint16_t wMaxSize,
	uint8_t bFileTransferId);

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/	
uint8_t iAP2_Sessions_bfnFileTransferParser(uint16_t wReadIndex ,uint16_t wSize)
{

	/* Local variable to store the size of the file */
	uint32_t dwFileTransferSize = (uint32_t)0x00;
	/* Local variable to store where the data will be saved */
	uint8_t* bpFileTransStoreData = NULL;
	/* Local variable to return status */
	uint8_t bFileTransferParserRetVal = IAP2_SESSIONS_FT_PARSER_OK;	
	/* Local buffer to store file transfer ID and datagram type */
	uint8_t bFTIDAndMessageBuffer[IAP2_SESSIONS_FT_HEADER_BYTE_SIZE] = {0x00, 0x00}; 
	/* Local buffer to store file size */
	uint8_t bFTFileSizeBuffer[IAP2_SESSIONS_FT_FILE_SIZE_BYTE_SIZE] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	/* Local variable to store the FT ID */
	uint8_t bFileTransferId = 0x00;
	/* Local variable to store the FT datagram type */
	uint8_t bFileTransferDatagram = 0x00;
	/* Local variable to store the message for complete file transfers */
	uint8_t bFileTransferEndMessage = 0;
	/* Local structure to store pass as parameter when sending start datagrams */
	FT_FILE_SEND_t tFileDesc;
	/* Local variable to store maximum capacity of target array to where received file data should be copied to */
	uint16_t wMaxSize = 0;
	/* Read file transfer header */
	bFileTransferParserRetVal = iAP2_Link_bfnReadData((uint8_t*)&bFTIDAndMessageBuffer[0],
			&wReadIndex, IAP2_SESSIONS_FT_HEADER_BYTE_SIZE);
	/* Subtract the two bytes read from the size variable */
	wSize = wSize - IAP2_SESSIONS_FT_HEADER_BYTE_SIZE;
	/* Save read values into local variables */
	bFileTransferId = bFTIDAndMessageBuffer[IAP2_SESSIONS_FT_ID_INDEX];
	bFileTransferDatagram = bFTIDAndMessageBuffer[IAP2_SESSIONS_FT_DATAGRAM_TYPE_INDEX];

    switch(bFileTransferDatagram)
	{
		/* If is a setup datagram */
		case IAP2_SESSIONS_FT_SETUP_DATAGRAM:	
			/* If file transfer ID status is free */
			if (IAP2_SESSIONS_FT_FREE_STATUS == iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				/* Read less significant long word for the 64-bit size variable */
				bFileTransferParserRetVal = iAP2_Link_bfnReadData((uint8_t*)&bFTFileSizeBuffer[0],
						&wReadIndex, IAP2_SESSIONS_FT_FILE_SIZE_BYTE_SIZE);
				/* Store file size */
				dwFileTransferSize = (uint32_t)(bFTFileSizeBuffer[7] & 0x000000FF);
				dwFileTransferSize |= (uint32_t)((bFTFileSizeBuffer[6] & 0x000000FF)<< 8);
				dwFileTransferSize |= (uint32_t)((bFTFileSizeBuffer[5] & 0x000000FF)<< 16);
				dwFileTransferSize |= (uint32_t)((bFTFileSizeBuffer[4] & 0x000000FF)<< 24);
				/* Execute upper layer callback function */
				iAP2_FTSessionFunctions.vfnStartFTCallbackFn(dwFileTransferSize, bFileTransferId);
				/* update file transfer state to start */
				iAP2_FTSession_gbaStatus[bFileTransferId] = IAP2_SESSIONS_FT_WAIT_1ST_STATUS;
				/* Send the start datagram */
				tFileDesc.bFileTransferIdentifier = bFileTransferId;
				bFileTransferParserRetVal = bfnFileTransferSendMessage(FT_DATAGRAM_START, &tFileDesc);
			}
			/* end case */
			break;
	
		/* If is a start datagram */
		case IAP2_SESSIONS_FT_START_DATAGRAM:	
	
			/* If file transfer was paused */
			if(IAP2_SESSIONS_FT_PAUSE_STATUS & iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				/* Remove Pause Mark */
				iAP2_FTSession_gbaStatus[bFileTransferId] &= (uint8_t)~IAP2_SESSIONS_FT_PAUSE_STATUS;
			}
			else
			{
				/* Handles wrong packet type */
				bFileTransferParserRetVal = bfnHandleWrongDatagram(bFileTransferId);
			}
			/* end case */
			break;
			
		/* If is a first datagram */
		case IAP2_SESSIONS_FT_FIRST_DATAGRAM:
			
			/* If file transfer status is waiting 1st datagram */
			if (IAP2_SESSIONS_FT_WAIT_1ST_STATUS == iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				/* set next status */
				iAP2_FTSession_gbaStatus[bFileTransferId] = IAP2_SESSIONS_FT_RECEIVING_STATUS;
				
				/* Call upper layer function every time a new packet arrives */
				iAP2_FTSessionFunctions.vfnFTDataCallbackFn(wSize, bFileTransferId, 
						(void*)&bpFileTransStoreData, &wMaxSize);
				/* Call link layer reading function */
				bFileTransferParserRetVal = bfnSendFileDataTo((void*)bpFileTransStoreData, 
							&wReadIndex, wSize, wMaxSize, bFileTransferId);
			}
			else
			{
				/* Handles wrong packet type */
				bFileTransferParserRetVal = bfnHandleWrongDatagram(bFileTransferId);
			}
			/* end case */
			break;
	
		/* If is a first and only datagram */
		case IAP2_SESSIONS_FT_FIRST_AND_ONLY_DATAGRAM:
				
			/* If file transfer status receiving datagrams */
			if (IAP2_SESSIONS_FT_WAIT_1ST_STATUS == iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				FT_DATAGRAM_TYPE_t tFileTransferEndMsgFinalID = FT_DATAGRAM_FAILURE;

				/* Call upper layer function every time a new packet arrives */
				iAP2_FTSessionFunctions.vfnFTDataCallbackFn(wSize, 
						bFileTransferId, (void*)&bpFileTransStoreData, &wMaxSize);
	
				/* Call link layer reading function */
				bFileTransferParserRetVal = bfnSendFileDataTo((void*)bpFileTransStoreData, 
							&wReadIndex, wSize, wMaxSize, bFileTransferId);
				/* Call upper layer callback function for the end of the file transfer */
				iAP2_FTSessionFunctions.vfnEndFTCallbackFn(bFileTransferId, IAP2_SESSIONS_FT_SUCCESS_DATAGRAM, 
						&bFileTransferEndMessage);
				/* Send the success or failure message */
				if(IAP2_SESSIONS_FT_RECEIVE_SUCCESS == bFileTransferEndMessage)
				{
					tFileTransferEndMsgFinalID = FT_DATAGRAM_SUCCESS;
				}
				tFileDesc.bFileTransferIdentifier = bFileTransferId;
				bFileTransferParserRetVal = bfnFileTransferSendMessage(tFileTransferEndMsgFinalID, &tFileDesc);
				/* set the free status */
				iAP2_FTSession_gbaStatus[bFileTransferId] = IAP2_SESSIONS_FT_FREE_STATUS;
			}
			else
			{
				/* Handles wrong packet type */
				bFileTransferParserRetVal = bfnHandleWrongDatagram(bFileTransferId);
			}
			/* end case */
			break;
	
		/* If is a data datagram */
		case IAP2_SESSIONS_FT_DATA_DATAGRAM:
			
			/* If file transfer was paused */
			if(IAP2_SESSIONS_FT_PAUSE_STATUS & iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				/* Remove Pause Mark */
				iAP2_FTSession_gbaStatus[bFileTransferId] &= (uint8_t)~IAP2_SESSIONS_FT_PAUSE_STATUS;
			}

			/* If file transfer status receiving datagrams */
			if (IAP2_SESSIONS_FT_RECEIVING_STATUS == iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				/* Call upper layer function every time a new packet arrives */
				iAP2_FTSessionFunctions.vfnFTDataCallbackFn(wSize,
						bFileTransferId, (void*)&bpFileTransStoreData, &wMaxSize);
		
				/* Call link layer reading function */
				bFileTransferParserRetVal = bfnSendFileDataTo((void*)bpFileTransStoreData, 
							&wReadIndex, wSize, wMaxSize, bFileTransferId);
			
			}
			else
			{
				/* Set error flag, sequence mismatch */
				bFileTransferParserRetVal = (uint8_t)IAP2_SESSIONS_FT_PARSER_ERR;
			}
			/* end case */
			break;
			
		/* If is a last datagram */
		case IAP2_SESSIONS_FT_LAST_DATAGRAM:
			
			/* If file transfer was paused */
			if(IAP2_SESSIONS_FT_PAUSE_STATUS & iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				/* Remove Pause Mark */
				iAP2_FTSession_gbaStatus[bFileTransferId] &= (uint8_t)~IAP2_SESSIONS_FT_PAUSE_STATUS;
			}

			/* If file transfer status receiving datagrams */
			if (IAP2_SESSIONS_FT_RECEIVING_STATUS == iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				FT_DATAGRAM_TYPE_t tFileTransferEndMsgFinalID = FT_DATAGRAM_FAILURE;
				
				/* Call upper layer function every time a new packet arrives */
				iAP2_FTSessionFunctions.vfnFTDataCallbackFn(wSize, 
						bFileTransferId, (void*)&bpFileTransStoreData, &wMaxSize);
			
				/* Call link layer reading function */
				bFileTransferParserRetVal = bfnSendFileDataTo((void*)bpFileTransStoreData, 
							&wReadIndex, wSize, wMaxSize, bFileTransferId);
				/* Call upper layer callback function for the end of the file transfer */
				iAP2_FTSessionFunctions.vfnEndFTCallbackFn(bFileTransferId, IAP2_SESSIONS_FT_SUCCESS_DATAGRAM, 
						&bFileTransferEndMessage);
				/* Send the success or failure message */
				if(IAP2_SESSIONS_FT_RECEIVE_SUCCESS == bFileTransferEndMessage)
				{
					tFileTransferEndMsgFinalID = FT_DATAGRAM_SUCCESS;
				}
				tFileDesc.bFileTransferIdentifier = bFileTransferId;
				bFileTransferParserRetVal = bfnFileTransferSendMessage(tFileTransferEndMsgFinalID, &tFileDesc);
			}
			else
			{
				/* Set error flag, sequence mismatch */
				bFileTransferParserRetVal = (uint8_t)IAP2_SESSIONS_FT_PARSER_ERR;
			}
		
			/* set the free status */
			iAP2_FTSession_gbaStatus[bFileTransferId] = IAP2_SESSIONS_FT_FREE_STATUS;
			/* end case */
			break;
			
		/* If is a cancel datagram */
		case IAP2_SESSIONS_FT_CANCEL_DATAGRAM:
	
			/* if the status for that file transfer is different than free */
			if (IAP2_SESSIONS_FT_FREE_STATUS != iAP2_FTSession_gbaStatus[bFileTransferId])
			{
				/* Update current transfer status from the status table to free */
				iAP2_FTSession_gbaStatus[bFileTransferId] = IAP2_SESSIONS_FT_FREE_STATUS;
				
				/* Executes upper layer end callback with the cancel status */
				iAP2_FTSessionFunctions.vfnEndFTCallbackFn(bFileTransferId, IAP2_SESSIONS_FT_CANCEL_DATAGRAM, 
						&bFileTransferEndMessage);
			}
			else
			{
				/* Do nothing */
			}
			/* end case */
			break;
			
		/* If is a pause datagram */
		case IAP2_SESSIONS_FT_PAUSE_DATAGRAM:

			/* Add pause mark to file transfer status */
			iAP2_FTSession_gbaStatus[bFileTransferId] |= (uint8_t)IAP2_SESSIONS_FT_PAUSE_STATUS;
			/* end case */
			break;
		
		/* If is a success datagram */
		case IAP2_SESSIONS_FT_SUCCESS_DATAGRAM:
	
			/* Set error flag this feature is not currently supported */
			bFileTransferParserRetVal = (uint8_t)IAP2_SESSIONS_FT_PARSER_ERR;
			/* end case */
			break;
		
		/* If is a failure datagram */
		case IAP2_SESSIONS_FT_FAILURE_DATAGRAM:
		
			/* Set error flag this feature is not currently supported */
			bFileTransferParserRetVal = (uint8_t)IAP2_SESSIONS_FT_PARSER_ERR;
			/* end case */
			break;
			
		default:
			/* Set error flag this feature is not currently supported */
			bFileTransferParserRetVal = (uint8_t)IAP2_SESSIONS_FT_PARSER_ERR;
			/* end case */
			break;
	}
	return bFileTransferParserRetVal;
}

/*!
 	 @fn	
 	 @brief	
 	 @param		FT_DATAGRAM_TYPE_t tDatagramType: FT_DATAGRAM_SETUP: file transfer identifier and file size
 	 	 	 	 	 	 	 	 	 	 	 	  must be set.
 	 	 	 	 	 	 	 	 	 	 	 	  FT_DATAGRAM_START, FT_DATAGRAM_CANCEL, FT_DATAGRAM_PAUSE, FT_DATAGRAM_SUCCESS,
 	 	 	 	 	 	 	 	 	 	 	 	  FT_DATAGRAM_FAILURE: file transfer identifier must be set.
 	 @return
*/
uint8_t iAP2_Sessions_bfnSendDatagram(FT_DATAGRAM_TYPE_t tDatagramType, FT_FILE_SEND_t *tpFileToSend)
{
	uint16_t wMemToReserve;
	uint8_t baDatagram[2];
	uint8_t bFinalResult; /*!< stores functions return value*/
	uint8_t bMemReqId; /*!< will store reserved memory ID assigned by the link layer */


	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{


		wMemToReserve = IAP2_SESSIONS_FT_COMMON_DTG_BYTE_SIZE;
		bFinalResult = FT_SESSIONS_OK;

		baDatagram[0] = tpFileToSend->bFileTransferIdentifier;

		switch(tDatagramType)
		{
		case FT_DATAGRAM_SETUP:
			baDatagram[1] = IAP2_SESSIONS_FT_SETUP_DTG_ID;
			break;
		case FT_DATAGRAM_START:
			baDatagram[1] = IAP2_SESSIONS_FT_START_ID;
			break;
		case FT_DATAGRAM_CANCEL:
			baDatagram[1] = IAP2_SESSIONS_FT_CANCEL_ID;
			break;
		case FT_DATAGRAM_PAUSE:
			baDatagram[1] = IAP2_SESSIONS_FT_PAUSE_ID;
			break;
		case FT_DATAGRAM_SUCCESS:
			baDatagram[1] = IAP2_SESSIONS_FT_SUCCESS_ID;
			break;
		case FT_DATAGRAM_FAILURE:
			baDatagram[1] = IAP2_SESSIONS_FT_FAILURES_ID;
			break;
		default:
			bFinalResult = FT_SESSIONS_ERROR;
			break;
		}

		if(FT_SESSIONS_OK == bFinalResult)
		{
			if(IAP2_SESSIONS_FT_SETUP_DTG_ID == baDatagram[1])
			{
				bFinalResult = bfnSendSetupDtg(tpFileToSend);
			}
			else
			{
				bFinalResult = bfnMemoryRequest(&wMemToReserve, TRUE, &bMemReqId);

				if(FT_SESSIONS_OK == bFinalResult)
				{
					vfnWriteDataToLinkBuffer(baDatagram, IAP2_SESSIONS_FT_COMMON_DTG_BYTE_SIZE, FALSE);

					bFinalResult = bfnSendBuffer(bMemReqId);
				}
			}
		}

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bFinalResult = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bFinalResult = SESSIONS_LOCK_FAIL;
	}
	return bFinalResult;
}

/* File send to device not fully supported by any feature by the specification yet */
#ifdef IAP2_SESSIONS_FT_SEND_ENABLED
/*!
 	 @fn	
 	 @brief	
 	 @param		FT_FILE_SEND_t *tpFileToSend: ->dwSentBytes: should be initialized to '0' when
 	 	 	 	 	 	 	 	 	 	 	 starting a file transfer.
 	 @return
*/
uint8_t iAP2_Sessions_bfnSendFile(FT_FILE_SEND_t *tpFileToSend)
{
	uint32_t dwPendingToSend;
	uint16_t wMemSizeReq;
	uint8_t bFinalResult;
	uint8_t bDatagramType;
	uint8_t bMemReqId;
	
	wMemSizeReq = IAP2_SESSIONS_FT_MAX_PAYLOAD_BYTE_SIZE;
	dwPendingToSend = tpFileToSend->dwFileSize - tpFileToSend->dwSentBytes;
	bFinalResult = FT_SESSIONS_ERROR;
	
	if(IAP2_SESSIONS_FT_MAX_PAYLOAD_BYTE_SIZE > (dwPendingToSend + IAP2_SESSIONS_FT_HEADER_BYTE_SIZE))
	{
		wMemSizeReq = (uint16_t)dwPendingToSend + IAP2_SESSIONS_FT_HEADER_BYTE_SIZE;
	}
	
	bFinalResult = bfnMemoryRequest(&wMemSizeReq, _FALSE_, &bMemReqId);
	
	if(FT_SESSIONS_OK == bFinalResult)
	{
		uint8_t baFlTrHeader[IAP2_SESSIONS_FT_HEADER_BYTE_SIZE];
		
		if((0 == tpFileToSend->dwSentBytes) && (wMemSizeReq >= tpFileToSend->dwFileSize))
		{
			bDatagramType = IAP2_SESSIONS_FT_FIRST_AO_DATA_ID;
		}
		else if(0 == tpFileToSend->dwSentBytes)
		{
			bDatagramType = IAP2_SESSIONS_FT_FIRST_DATA_ID;
		}
		else if(wMemSizeReq >= dwPendingToSend)
		{
			bDatagramType = IAP2_SESSIONS_FT_LAST_DATA_ID;
		}
		else
		{
			bDatagramType = IAP2_SESSIONS_FT_DATA_ID;
		}
		
		baFlTrHeader[0] = tpFileToSend->bFileTransferIdentifier;
		baFlTrHeader[1] = bDatagramType;
		
		/* reset link buffer array index to zero in case previous 
		   instances of this function were called */
		wLinkBufferIdx = 0;
		
		vfnWriteDataToLinkBuffer(baFlTrHeader, IAP2_SESSIONS_FT_HEADER_BYTE_SIZE, _TRUE_);
		
		vfnWriteDataToLinkBuffer(&tpFileToSend->bpFile[tpFileToSend->dwSentBytes], \
												wMemSizeReq - IAP2_SESSIONS_FT_HEADER_BYTE_SIZE, _TRUE_);
		
		bFinalResult = iAP2_Link_bfnSend(IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID, bMemReqId, 0);
		
		if(FT_SESSIONS_OK == bFinalResult)
		{		
			tpFileToSend->dwSentBytes += (wMemSizeReq - IAP2_SESSIONS_FT_HEADER_BYTE_SIZE);
			
			if((IAP2_SESSIONS_FT_FIRST_AO_DATA_ID == bDatagramType) || (IAP2_SESSIONS_FT_LAST_DATA_ID == bDatagramType))
			{
				bFinalResult = FT_SESSIONS_SEND_COMPLETE;
			}
		}
		else
		{
			iAP2_Link_vfnMemoryFlush(bMemReqId);
		}
	}
	
	return bFinalResult;
}
#endif

/************************** Private Functions ******************************************/


static uint8_t bfnHandleWrongDatagram(uint8_t bFileTransferId)
{
	/* If transfer ID was inactive (cancel)*/
	if (IAP2_SESSIONS_FT_FREE_STATUS == iAP2_FTSession_gbaStatus[bFileTransferId])
	{
		/* Local structure to store pass as parameter when sending datagrams */
		FT_FILE_SEND_t tFileDesc;
		tFileDesc.bFileTransferIdentifier = bFileTransferId;
		/* Send cancel file transfer message*/
		(void)bfnFileTransferSendMessage(FT_DATAGRAM_CANCEL, &tFileDesc);
	}
	/* Set error flag this feature is not currently supported */
	return (uint8_t)IAP2_SESSIONS_FT_PARSER_ERR;
}

/*!
 	 @fn	
 	 @brief	
 	 @param	
 	 @return
*/
static uint8_t bfnSendSetupDtg(FT_FILE_SEND_t *tpFileToSend)
{
	uint8_t bFinalResult = FT_SESSIONS_ERROR; /*!< stores functions return value*/
	uint16_t wMemToreserve = IAP2_SESSIONS_FT_SETUP_DTG_BYTE_SIZE;
	uint8_t baSetupDatagram[IAP2_SESSIONS_FT_SETUP_DTG_BYTE_SIZE];
	uint8_t bMemReqId; /*!< will store reserved memory ID assigned by the link layer */
	
	baSetupDatagram[0] = tpFileToSend->bFileTransferIdentifier;
	baSetupDatagram[1] = IAP2_SESSIONS_FT_SETUP_DTG_ID;
	
	baSetupDatagram[2] = 0;
	baSetupDatagram[3] = 0;
	baSetupDatagram[4] = 0;
	baSetupDatagram[5] = 0;
	
	baSetupDatagram[6] = (uint8_t)((tpFileToSend->dwFileSize & 0xFF000000) >> 24);
	baSetupDatagram[7] = (uint8_t)((tpFileToSend->dwFileSize & 0x00FF0000) >> 16);
	baSetupDatagram[8] = (uint8_t)((tpFileToSend->dwFileSize & 0x0000FF00) >> 8);
	baSetupDatagram[9] = (uint8_t)((tpFileToSend->dwFileSize & 0x000000FF));

	bFinalResult = bfnMemoryRequest(&wMemToreserve, _TRUE_, &bMemReqId);
	
	if(FT_SESSIONS_OK == bFinalResult)
	{
		vfnWriteDataToLinkBuffer(baSetupDatagram, IAP2_SESSIONS_FT_SETUP_DTG_BYTE_SIZE, _FALSE_);
		
		bFinalResult = bfnSendBuffer(bMemReqId);
	}
	
	return bFinalResult;
}

/*************************************************************************************************/
/*                               Static Functions Section                                        */
/*************************************************************************************************/

/*!
 	 @fn 		static void iAP2_Sessions_vfnWriteDataToLinkBuffer(uint8_t baData[]), uint16_t wDataSize,
 	 	 	 	 uint8_t bKeepLinkBufferIdx)
 	 @brief		Function that copies a number of bytes into the Link Buffer
	 @param		uint8_t* baData: 			buffer where the data to be copied is located.
 	 @param		uint16_t wDataSize: 			amount of bytes to be copied.
	 @param		uint8_t bKeepLinkBufferIdx:	_TRUE_: will not reset the link buffer index to zero, use this
	 	 	 	 	 	 	 	 	 	 	  for consecutive calls to this function.
	 	 	 	 	 	 	 	 	 	_FALSE_: will reset the link buffer index to zero.
 	 @return	void
*/
static void vfnWriteDataToLinkBuffer(uint8_t* baData, uint16_t wDataSize, uint8_t bKeepLinkBufferIdx)
{
	uint16_t wIdx;
	
	if(_TRUE_ == bKeepLinkBufferIdx)
	{

		for(wIdx = 0; wIdx < wDataSize; wIdx++)
		{
			bpLinkBuffer[wLinkBufferIdx++] = baData[wIdx];
		}
	}
	else
	{
		for(wIdx = 0; wIdx < wDataSize; wIdx++)
		{
			bpLinkBuffer[wIdx] = baData[wIdx];
		}
		wLinkBufferIdx = 0;
	}
}

/*! //@todo JS update the following function doc:
 	 @fn 		static uint8_t iAP2_Sessions_bfnMemoryRequest(uint8_t **bppIndex, uint16_t wDataSize, uint8_t bAtLeastDataSize)
 	 @brief	
 	 @param		uint16_t *wpDataSize:		into func.: 	pointer to amount of memory to request (unit: byte),
 	 	 	 	 	 	 	 	 	 	func. return:	pointer to amount of memory reserved.
 	 @param		uint8_t bAtLeastDataSize:	_TRUE_: reserved data size must be minimum returned by the 
 	 	 	 	 	 	 	 	 	 		  link layer.
 	 	 	 	 	 	 	 	 	 	_FALSE_: any reserved data size by link is valid. 
 	 @param		uint8_t *bpMemReqId:			pointer where the link layer will return an ID for the 
 	 	 	 	 	 	 	 	 	 	requested memory.
 	 @return	FT_SESSIONS_OK: 			requested memory successfully.
 	 	 	 	FT_SESSIONS_ERROR:			amount of reserved link memory is less than requested.
 	 	 	 	FT_SESSIONS_BUSY:
 	 	 	 	FT_SESSIONS_MEM_FULL:
*/
static uint8_t bfnMemoryRequest(uint16_t *wpDataSize, uint8_t bAtLeastDataSize, uint8_t *bpMemReqId)
{
	uint16_t wLinkBufferFreeReq; /*!< stores amount of memory requested and actually reserved by the link layer*/
	uint8_t bFinalResult; /*!< stores functions return value*/
	
	wLinkBufferFreeReq = (*wpDataSize);
	
	bFinalResult = iAP2_Link_bfnMemoryRequest(&bpLinkBuffer, &wLinkBufferFreeReq, bpMemReqId);
	
	if(IAP2_LINK_STATUS_OK == bFinalResult)
	{
		if(bAtLeastDataSize && ((*wpDataSize) > wLinkBufferFreeReq))
		{
			/* released reserved requested reserved memory since the amount is not the required one */
			iAP2_Link_vfnMemoryFlush(*bpMemReqId/*baMemRequestIds[bMemRequestIdsCount]*/);
			/* set return value */
			bFinalResult = FT_SESSIONS_ERROR;
		}
		else
		{
			(*wpDataSize) = wLinkBufferFreeReq;
			wLinkBufferFree = wLinkBufferFreeReq;
			wLinkBufferSize = wLinkBufferFreeReq;
			bFinalResult = FT_SESSIONS_OK;
		}
	}
	else if(IAP2_LINK_ERROR_MEM_BUSY == bFinalResult)
	{
		bFinalResult = FT_SESSIONS_BUSY;	
	}
	else if(IAP2_LINK_ERROR_NO_MEM == bFinalResult)
	{
		bFinalResult = FT_SESSIONS_MEM_FULL;
	}
	else
	{
		/* Do nothing; case not expected */
	}
	
	return bFinalResult;
}

/*! //@todo JS update the following function doc:
	@fn 		uint8_t iAP2_Sessions_bfnEndSendCtrlMsg(void)
 	@brief		Part of the control message send sequence, used signal the end of the send sequence
 	 	 	 	and fire the control message send from the link layer.
 	 	 	 	
 	 	 	 	This function is used along with these two other functions:
 	 	 	 		iAP2_Sessoions_bfnSendCtrlMessage(...)
 	 	 	 		iAP2_Sessions_bfnAddCtrlMsgParam(...)
 	 	 	 	
 	 	 	 	For more details see 'Mode B' in iAP2_Sessoions_bfnSendCtrlMessage(...) description. 
						
	@param		void				 
	@return		FT_SESSIONS_OK:			Data was successfully added to the control session 
 	 	 	 							message to be send.
				FT_SESSIONS_BUSY:			Link layer send problem, try again latter.
				SESSIONS_WRONG_INIT:	The function iAP2_Sessoions_bfnSendCtrlMessage(...)
										was not called before.				
*/
static uint8_t bfnSendBuffer(uint8_t bMemRedIdToSend)
{
	uint16_t wLinkBufferUsed;
	uint8_t bFinalResult;
	
	wLinkBufferUsed = 0;
	
	/* Check if the last requested buffer space is not full. If buffer full use wLinkBufferUsed = 0.
	   This assumes all previous requested buffers are full. */
	if(0 != wLinkBufferFree)
	{
		wLinkBufferUsed = wLinkBufferSize - wLinkBufferFree;
	}
	
	bFinalResult = iAP2_Link_bfnSend(IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID, bMemRedIdToSend,  wLinkBufferUsed);
	
	/* interpret link send function return value */
	if(IAP2_LINK_STATUS_OK == bFinalResult)
	{
		bFinalResult = FT_SESSIONS_OK;
	}
	else
	{
		bFinalResult = FT_SESSIONS_ERROR;
	}
	
	return bFinalResult;
}

#if defined __CC_ARM
__inline static uint8_t bfnFileTransferSendMessage(FT_DATAGRAM_TYPE_t tDatagramType, FT_FILE_SEND_t *tpFileToSend)
#else
inline static uint8_t bfnFileTransferSendMessage(FT_DATAGRAM_TYPE_t tDatagramType, FT_FILE_SEND_t *tpFileToSend)
#endif

{
	uint8_t bFileTransferParserRetVal = (uint8_t)IAP2_SESSIONS_FT_PARSER_ERR;
	
	if(FT_SESSIONS_OK == iAP2_Sessions_bfnSendDatagram(tDatagramType, tpFileToSend))
	{
		/* Return success */
		bFileTransferParserRetVal = (uint8_t)IAP2_SESSIONS_FT_PARSER_OK;
	}
	
	return bFileTransferParserRetVal;
}

/* set parameter uint16_t wMaxSize: to 0 to copy data without checking destination array size limit */
static uint8_t bfnSendFileDataTo(void* vpReadPtr, uint16_t *wpReadIndex, uint16_t wSize, uint16_t wMaxSize, uint8_t bFileTransferId)
{
	uint16_t wBytesCopied;
	uint16_t wBytes2Copy;
	uint8_t bFileTransferParserRetVal;
	
	wBytesCopied = 0;
	wBytes2Copy = wSize;
	
	do
	{
		if(wBytes2Copy > wMaxSize)
		{
			if(0 != wMaxSize)
			{
				wBytes2Copy = wMaxSize;				
			}
		}
		
		bFileTransferParserRetVal = iAP2_Link_bfnReadData(vpReadPtr, wpReadIndex, wBytes2Copy);
		wBytesCopied += wBytes2Copy;
		
		if((wBytesCopied < wSize) && (IAP2_LINK_CBUFFER_OK == bFileTransferParserRetVal))
		{
			wBytes2Copy = wSize - wBytesCopied;
			iAP2_FTSessionFunctions.vfnFTDataCallbackFn(wBytes2Copy, bFileTransferId, &vpReadPtr, &wMaxSize);
		}
	}
	while((wBytesCopied < wSize) && (IAP2_LINK_CBUFFER_OK == bFileTransferParserRetVal));
	
	return bFileTransferParserRetVal;
}

#if defined __CC_ARM
__inline static void vfnResetStatusCtrls()
#else
inline static void vfnResetStatusCtrls()
#endif
{
	uint16_t wCount;
	
	for(wCount = 0; wCount < IAP2_SESSIONS_FT_MAX_NUM_IDENTIFIERS; wCount++)
	{
		iAP2_FTSession_gbaStatus[wCount] = IAP2_SESSIONS_FT_FREE_STATUS;
	}
}

void iAP2_Sessions_FT_vfnReset()
{
	vfnResetStatusCtrls();
}

#if defined(__GNUC__)
__attribute__ ((weak))
#elif defined __IAR_SYSTEMS_ICC__
__weak
#endif
void iAP2_FileTrans_vfnDataReceived(uint16_t wFileTransPacketSize, uint8_t wFileTransferID, void** bppStoreData,
        uint16_t *wpArrayMaxCapacity)
{
    (void)wFileTransPacketSize;
    (void)wFileTransferID;
    (void)bppStoreData;
    (void)wpArrayMaxCapacity;
}

#if defined(__GNUC__)
__attribute__ ((weak))
#elif defined __IAR_SYSTEMS_ICC__
__weak
#endif
void iAP2_FileTrans_vfnNewTransfer(uint32_t dwFileTransferSize, uint8_t bFileTransferId)
{
    (void)dwFileTransferSize;
    (void)bFileTransferId;
}

#if defined(__GNUC__)
__attribute__ ((weak))
#elif defined __IAR_SYSTEMS_ICC__
__weak
#endif
void iAP2_FileTrans_vfnEndTransfer(uint8_t bFileTransferId, uint8_t bTransferStatus, uint8_t* bpUpperLayerStatus)
{
    (void)bFileTransferId;
    (void)bTransferStatus;
    (void)bpUpperLayerStatus;
}
