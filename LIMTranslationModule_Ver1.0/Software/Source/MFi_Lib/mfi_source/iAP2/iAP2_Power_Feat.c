/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Power_Feat.h"
#include "iAP2_Sessions_Control.h"
#include "iAP2_Features.h"
/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/


#define iAP2_POWER_UPDATES_TOTAL_PARAM				0x3
#define iAP2_POWER_SOURCE_UPDATES_TOTAL_PARAM		0x2

#define iAP2_POWER_UPDATES_BIGGER_PARAM_ID	    	2
#define iAP2_POWER_SOURCE_UPDATES_BIGGER_PARAM_ID 	1

#define iAP2_POWER_UPDATES_START_MAX_MESSAGE_SIZE		0x18	
#define iAP2_POWER_UPDATES_STOP_MAX_MESSAGE_SIZE	    0x06

#define iAP2_POWER_UPDATES_MAX_CURRENT_DRAWM_FROM_ACCESORY_ID					0
#define iAP2_POWER_UPDATES_DEVICE_BATT_WILL_CHARGE_IF_POWER_ID					1
#define iAP2_POWER_UPDATES_ACCESORY_POWER_MODE_ID								2

#define iAP2_POWER_SOURCE_UPDATES_AVAILABLE_CURRENT_FOR_DEVICE_ID				0
#define iAP2_POWER_SOURCE_UPDATES_DEVICE_BATT_SHOULD_CHARGE_IF_POWER_ID			1
			
/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void iAP2_PowerUpdates_vfnNewRequestCaller(void(*fnPtr)(uint16_t),uint16_t MessageID);
static void iAP2_PowerUpdates_vfnGetParameter(uint16_t MessageID);

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/

/*Pointers to function to be executed according to received parameter ID*/
void (* const iAP2_PowerUpdates_vfnapReceivedItemParameters[]) (void* vpsubparamvalue,uint8_t bmaxValue ,uint32_t wsubParamFlag,
		uint32_t* wpFlagToSet,uint16_t wDataSize, uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame) =
{
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16, //0
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum, // BOOL
		iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum,
};

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static iAP2_PowerUpdateFeature PowerUpdates; 				/**< Keep the last NowPlayingUpdates*/
static iAP2_PowerUpdateFeature *PowerUpdatesDataToUpdate;  /**< Keep pointer where NowPlayingUpdates will be updated*/

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/

/*Pointers where PowerUpdate data would be stored*/
static void * const iAP2_Power_Updates_Item_paramVal[]=
{
		&(PowerUpdates.wMaxCurrentDrawnFromAcc),
		&(PowerUpdates.bDeviceBattWillChargeIfPowerPresent),
		&(PowerUpdates.bAccessoryPowerMode),
		
};

/*Flag for received PowerUpdate data*/           
static uint16_t const iAP2_Power_Updates_Item_ParamFlag[] =
 {
		 iAP2_POWER_FEATURE_MAX_CURRENT_DRAWN_FROM_ACCESORY,	
		 iAP2_POWER_FEATURE_DEVICE_BATT_WILL_CHARGE_POWER_PRESENT,
		 iAP2_POWER_FEATURE_ACCESORY_POWER_MODE,
 };

 /*Possible maximum values for received data*/
static uint8_t const iAP2_Power_Updates_ParamMaxValue[]=
{
	0,
	_TRUE_,
	2,
};

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
static uint16_t gwDataSize;					/**< Data Size for current parameter*/
static uint8_t gbCurrentFrame;				/**< Current frame status*/
static uint8_t gbMessageCurrentStatus;  		/**< Current message status*/

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/*************************************************************************************************
*
* @brief    Function used to when a new Message was received from Sessions 
*
* @param    fnPtr Pointer to the function to be called once received message was validated
*			wMessageID	MesssageID 
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_PowerUpdates_vfnNewRequestCaller(void(*fnPtr)(uint16_t),uint16_t wMessageID)
{
	static iAP2_PowerUpdateFeature PowerUpdatesTmp;
	if(iAP2_FEATURES_UPDATES_MESSAGE_NEW == gbMessageCurrentStatus)
	{
		PowerUpdates.dwPowerUpdatesFlag = 0;
		/** @todo Function Return */
		(void)memcpy(&PowerUpdatesTmp,&PowerUpdates,sizeof(iAP2_PowerUpdateFeature));
	}
	iAP2_PowerUpdates_vfnGetParameter(wMessageID);
	if(gbMessageCurrentStatus &  iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED )
	{
			/** @todo Function Return */
			(void)memcpy(&PowerUpdates,&PowerUpdatesTmp,sizeof(iAP2_PowerUpdateFeature));
			gbMessageCurrentStatus = iAP2_FEATURES_UPDATES_MESSAGE_NEW;
	}
	else
	{
		if(IAP2_SESS_CTRL_MESSAGE_COMPLETE == gbCurrentFrame)
		{
				/** @todo Function Return */
				(void)memcpy(PowerUpdatesDataToUpdate,&PowerUpdates,sizeof(iAP2_PowerUpdateFeature));
				gbMessageCurrentStatus = iAP2_FEATURES_UPDATES_MESSAGE_NEW;
				fnPtr(wMessageID);	
		}
		else
		{
			gbMessageCurrentStatus |= iAP2_FEATURES_UPDATES_MESSAGE_INCOMPLETE;
		}
	}
}

/** ***********************************************************************************************
*
* @brief    Function called to get Parameters from a Power Feature Message.
* 
* @param   	void
* 
* @return  	void
*          
**************************************************************************************************/
static void iAP2_PowerUpdates_vfnGetParameter(uint16_t MessageID)
{
	static uint8_t  bSessionStatus;
	static uint16_t wParamID;
	
	(void)MessageID;
	
	gwDataSize=0;
	do
	{
		bSessionStatus = iAP2_Sessions_bfnGetCtrlMsgParamInfo(&wParamID,&gwDataSize);
		gbCurrentFrame = IAP2_SESS_CTRL_PARAM_COMPLETE;
		if(IAP2_SESS_CTRL_OK == bSessionStatus)
		{	
			if(wParamID < iAP2_POWER_UPDATES_TOTAL_PARAM)
			{
				iAP2_PowerUpdates_vfnapReceivedItemParameters[wParamID]( 
							iAP2_Power_Updates_Item_paramVal[wParamID],
							iAP2_Power_Updates_ParamMaxValue[wParamID],
							iAP2_Power_Updates_Item_ParamFlag[wParamID],
							&(PowerUpdates.dwPowerUpdatesFlag),
							gwDataSize, &gbMessageCurrentStatus,&gbCurrentFrame);
			}			
		}
	}while((IAP2_SESS_CTRL_OK == bSessionStatus) && (IAP2_SESS_CTRL_MESSAGE_COMPLETE != gbCurrentFrame ) && (IAP2_SESS_CTRL_DATA_PENDING != gbCurrentFrame ));	
}

/** ***********************************************************************************************
*
* @brief    Function called when a Message for PowerUpdate is received .
* 
* @param   	wMessageID Identifies messageID
* 
* @return  	void
*          
**************************************************************************************************/
void iAP2_Feat_Power_vfnCallBack(uint16_t wMessageID)
{
	if( iAP2_POWER_FEATURE_UPDATES == wMessageID )
	{
		iAP2_PowerUpdates_vfnNewRequestCaller(&iAP2_CALLBACK_FROM_POWER_UPDATE,wMessageID); /*@TODO: Call correct callback*/
	}
}

#if IAP2_IDENT_PWR_SOURCE_TYPE_DATA == IAP2_PWR_TYPE_SELF
/**************************************************************************************************
*
* @brief    Function called to start PowerSourceUpdate from accessory.
* 
* @param   	wAvailableCurrentForDevice				You could use defines for values 0,500,1000 or 2100 in mA. 
* 			bDeviceBattShouldChargeIfPowerPresen	1- True or 0-False
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          iAP2_POWER_FEATURE_INVALID_DATA_PARAMETER 0xFF
*          
**************************************************************************************************/
uint8_t iAP2_Power_Features_bfnPowerSourceUpdate(uint16_t wAvailableCurrentForDevice, uint8_t bDeviceBattShouldChargeIfPowerPresent)
{
	uint8_t bSessionStatus;	
	int lockStatus;

	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);

	if(ERRCODE_NO_ERROR == lockStatus)
	{
		gbMessageCurrentStatus = 0;
		bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(iAP2_POWER_FEATURE_SOURCE_UPDATE, NULL, 0,iAP2_POWER_UPDATES_START_MAX_MESSAGE_SIZE);
		if(iAP2_POWER_FEATURE_ERROR_OK == bSessionStatus)
		{
			BYTESWAP16(wAvailableCurrentForDevice,wAvailableCurrentForDevice);
			bSessionStatus= iAP2_Sessions_bfnAddCtrlMsgParam(iAP2_POWER_SOURCE_UPDATES_AVAILABLE_CURRENT_FOR_DEVICE_ID,IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, (uint8_t *)&wAvailableCurrentForDevice, 2);
			if(iAP2_POWER_FEATURE_ERROR_OK == bSessionStatus)
			{
				if (bDeviceBattShouldChargeIfPowerPresent < 2)
				{
					bSessionStatus= iAP2_Sessions_bfnAddCtrlMsgParam(iAP2_POWER_SOURCE_UPDATES_DEVICE_BATT_SHOULD_CHARGE_IF_POWER_ID,IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, &bDeviceBattShouldChargeIfPowerPresent, 1);
				}
				else
				{
					bSessionStatus = iAP2_POWER_FEATURE_INVALID_DATA_PARAMETER;
				}
			}
		}
		if(iAP2_POWER_FEATURE_ERROR_OK == bSessionStatus)
		{
			bSessionStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
		}
		else
		{
			iAP2_Sessions_vfnResetMessage();
		}
		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}
	return bSessionStatus;
}
#endif

/** ***********************************************************************************************
*
* @brief    Function called to start PowerUpdates from iPod, iPad or
* 			iPhone device.
* 
* @param   	bPowerUpdatesAttributes indicates which Power updates want to receive
* 			PowerUpdatesAppDataPtr The pointer to Application structure where the updates will be saved.
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_Power_Features_bfnStartPowerUpdates(uint8_t bPowerUpdatesAttributes,iAP2_PowerUpdateFeature * PowerUpdatesAppDataPtr)
{
	
	uint8_t bSessionStatus;
	uint8_t bCurrentParam;
	
	int lockStatus;
	
	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
	
	if(ERRCODE_NO_ERROR == lockStatus)
	{

		gbMessageCurrentStatus = 0;
		PowerUpdatesDataToUpdate=PowerUpdatesAppDataPtr;
		/** @todo Function Return */
		(void)memcpy(&PowerUpdates,PowerUpdatesAppDataPtr,sizeof(iAP2_PowerUpdateFeature));
		if(bPowerUpdatesAttributes<8)
		{
			bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(iAP2_POWER_FEATURE_UPDATES_START, NULL, 0,iAP2_POWER_UPDATES_START_MAX_MESSAGE_SIZE);
			/*Add Media Item Attributes Sub parameters*/
			bCurrentParam=0;
			while((iAP2_POWER_FEATURE_ERROR_OK == bSessionStatus) & (bCurrentParam<iAP2_POWER_UPDATES_TOTAL_PARAM))
			{
				if(bPowerUpdatesAttributes&0x01)
				{

					bSessionStatus= iAP2_Sessions_bfnAddCtrlMsgParam(bCurrentParam,IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM, NULL, 0);
				}
				bPowerUpdatesAttributes/=2;
				bCurrentParam++;
			}

			if(iAP2_POWER_FEATURE_ERROR_OK == bSessionStatus)
			{
				bSessionStatus =  iAP2_Sessions_bfnEndSendCtrlMsg();
			}
			else
			{
				iAP2_Sessions_vfnResetMessage();
			}
		}
		else
		{
			bSessionStatus=iAP2_POWER_FEATURE_INVALID_DATA_PARAMETER;
		}

		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}

	return bSessionStatus;
}


/** ***********************************************************************************************
*
* @brief    Function called to stop receiving PowerUpdates messages from iPod, iPad or
* 			iPhone device.
* 
* @param   void
* 
* @return  	If the message could be sent
*			SESSIONS_OK 		0
*			SESSIONS_BUSY 		1
*			SESSIONS_BUSY 		2
*			SESSIONS_MEM_FULL	3
*			SESSIONS_WRONG_INIT 4
*          
**************************************************************************************************/
uint8_t iAP2_Power_Features_bfnStopPowerUpdates(void)
{
	uint8_t bSessionStatus;

	int lockStatus;
	
	lockStatus = osa_mutex_lock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
	
	if(ERRCODE_NO_ERROR == lockStatus)
	{

		gbMessageCurrentStatus = 0;
		bSessionStatus = iAP2_Sessions_bfnSendCtrlMessage(iAP2_POWER_FEATURE_UPDATES_STOP, NULL, 0, 0);
		
		lockStatus = osa_mutex_unlock(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
		if(ERRCODE_NO_ERROR != lockStatus )
		{
			bSessionStatus = SESSIONS_LOCK_FAIL;
		}
	}
	else
	{
		bSessionStatus = SESSIONS_LOCK_FAIL;
	}

	return bSessionStatus;
}



void iAP2_Power_Features_bfnDummy(uint16_t wMessageID)
{
	(void)wMessageID;
}
