/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_FEATURE_ARBITRATOR_H_
#define IAP2_FEATURE_ARBITRATOR_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "mfi_cfg.h"
/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/* Macros to determine which features are included */
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_FEAT_AUTH				(_TRUE_)
#define IAP2_FEAT_ID				(_TRUE_)
#define IAP2_FEAT_APP_LAUNCH		(_FALSE_)
#define IAP2_FEAT_ASSIST_TOUCH		(_FALSE_)
#define IAP2_FEAT_BT				(_FALSE_)
#define IAP2_FEAT_EA				(_TRUE_)
#define IAP2_FEAT_HID				(_TRUE_)
#define IAP2_FEAT_LOC				(_FALSE_)
#if (MFICFG_ENABLE_METADATA)
#define IAP2_FEAT_MEDIA_LIB			(_TRUE_)
#else
#define IAP2_FEAT_MEDIA_LIB         (_FALSE_)
#endif
#define IAP2_FEAT_NOW_PLAY			(_TRUE_)
#define IAP2_FEAT_POWER				(_TRUE_)
#define IAP2_FEAT_USB_AUDIO			(_TRUE_)
#define IAP2_FEAT_VOICE_OVER		(_FALSE_)
#define IAP2_FEAT_WIFI				(_FALSE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION

/* MSB of Message ID per feature */
#define IAP2_FEAT_AUTH_ID			0xAA
#define IAP2_FEAT_ID_ID				0x1D
#define IAP2_FEAT_ASSIST_TOUCH_ID	0x54
#define IAP2_FEAT_BT_ID				0x4E
#define IAP2_FEAT_EA_ID				0xEA
#define IAP2_FEAT_HID_ID			0x68
#define IAP2_FEAT_LOC_ID			0xFF
#define IAP2_FEAT_MEDIA_LIB_ID		0x4C
#define IAP2_FEAT_NOW_PLAY_ID		0x50
#define IAP2_FEAT_POWER_ID			0xAE
#define IAP2_FEAT_USB_AUDIO_ID		0xDA
#define IAP2_FEAT_VOICE_OVER_ID		0x56
#define IAP2_FEAT_WIFI_ID			0x57

/*Used to conversion between an array to byte array*/
#define IAP2_FEATURES_UINT16_ARRAY		0
#define IAP2_FEATURES_UINT32_ARRAY		1
#define IAP2_FEATURES_CONVERSION_DONE	0
#define IAP2_FEATURES_CONVERSION_ERROR	1

/*Define bytes per each data*/
#define  iAP2_FEATURES_UPDATES_BYTES_PER_QWORD									8
#define  iAP2_FEATURES_UPDATES_BYTES_PER_DWORD									4
#define  iAP2_FEATURES_UPDATES_BYTES_PER_WORD									2
#define  iAP2_FEATURES_UPDATES_BYTE												1

/*Posible maximum size for utf8 parameter*/
#define  iAP2_FEATURES_UPDATES_UTF8_MAX_SIZE										64

/*Message status from sessions*/
#define iAP2_FEATURES_UPDATES_MESSAGE_NEW										0
#define	iAP2_FEATURES_UPDATES_MESSAGE_INCOMPLETE								1
#define	iAP2_FEATURES_UPDATES_MESSAGE_MUST_BE_IGNORED						 (1<<1)
#define	iAP2_FEATURES_UPDATES_MESSAGE_COMPLETE								 (1<<2)
#define	iAP2_FEATURES_UPDATES_MESSAGE_INVALID								 (1<<3)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
uint8_t iAP2_Features_bfnSwapArray(void *bpArray,uint8_t bArraySize,uint8_t bTypeArray);

void iAP2_FEATURES_UPDATES_InvalidParameter(void* subparamvalue,uint8_t maxValue, uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U64(void* subparamVal,uint8_t subparamSize ,uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U32(void* subparamVal,uint8_t subparamSize ,uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U16(void * subparamvalue,uint8_t maxValue,uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_U8(void* subparamVal,uint8_t bmaxValue,uint32_t subParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_UTF8(void * vpsubparamVal,uint8_t bsubparamMaxSize ,uint32_t wsubParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
        uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);
void iAP2_FEATURES_UPDATES_PARAMETER_UPDATE_u8_Enum(void* vpsubparamvalue,uint8_t bmaxValue ,uint32_t wsubParamFlag,uint32_t* wpFlagToSet,uint16_t wDataSize,
		uint8_t *bpMessageCurrentStatus, uint8_t * bpCurrentFrame);

/*************************************************************************************************/

#endif /* IAP2_FEATURE_ARBITRATOR_H_ */
