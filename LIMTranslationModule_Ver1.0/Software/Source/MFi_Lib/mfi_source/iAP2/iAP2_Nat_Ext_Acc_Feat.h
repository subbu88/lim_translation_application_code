/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_NAT_EXT_ACC_FEAT_H_
#define IAP2_NAT_EXT_ACC_FEAT_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP2_FEAT_NEA_CALLBACK				vfnMFi_EADemo_iAP2Callback

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/


//typedef void (* const IAP2_FEAT_EA_CBK)(uint8_t bEvent);

typedef enum
{
	IAP2_FEAT_NEA_PROT_ACTIVE = 0,
	IAP2_FEAT_NEA_PROT_INACTIVE,
	IAP2_FEAT_NEA_PROT_DATA_RECV,
	IAP2_FEAT_NEA_PROT_RECV_NO_MEM,
	IAP2_FEAT_NEA_PROT_SEND_COMPLETE,
} IAP2_FEAT_NEA_PROT_STATUS;

typedef enum
{
	IAP2_FEAT_NEA_ERROR_OK = 0,
	IAP2_FEAT_NEA_ERROR_NO_BYTES_AVAILABLE,

} IAP2_FEAT_NEA_ERRORS;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/

void IAP2_FEAT_NEA_CALLBACK(uint8_t bEvent, uint16_t wSize);
uint8_t iAP2_Feat_NEA_bfnSendData(uint8_t *pBuff, uint16_t wSize);;
void iAP2_Feat_NEA_bfnRead(uint8_t * pApplicationBuffer);

/*************************************************************************************************/

#endif /* IAP2_NAT_EXT_ACC_FEAT_H_ */
