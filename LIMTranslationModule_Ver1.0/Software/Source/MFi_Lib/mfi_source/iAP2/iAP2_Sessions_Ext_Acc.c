/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Sessions.h"
#include "iAP2_Sessions_Ext_Acc.h"
#include "iAP2_Link.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static uint8_t  iAP2_Sess_EA_bPendingDataFlag = _FALSE_;
static uint16_t iAP2_Sess_EA_wPendingDataSize = 0;
static uint16_t iAP2_Sess_EA_bTranID  = 0; 
static uint8_t *iAP2_Sess_EA_pbDataBuffer;

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
void IAP2_SESSIONS_EA_RECV_CALLBACK(uint16_t wEATranID, uint16_t wReadIndex, uint16_t wSize, uint8_t bStatus);

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/


/*************************************************************************************************
*
* @brief    This function is called every time an External Accessory frame arrives. The frame will
*           be analyzed and if does not have error the appropriate Protocol Callback must be execute. 
* 
* @param   	wReadIndex		Message of External Accessory feature.
*           wSize         Function pointer to Application callback 
*           
* @return  	IAP2_SESSIONS_NEW_EXT_ACC_SESSION_FRAME_OK			(0): Performed Correctly
 	 	 	IAP2_SESSIONS_NEW_EXT_ACC_SESSION_FRAME_ERROR		(1): An error occurred during frame reading
			IAP2_SESSIONS_NEW_EXT_ACC_WRONG_ID                  (2): Transfer ID was not found
*          
**************************************************************************************************/
void iAP2_Sessions_bfnNewExt_AccSessionFrame(uint16_t wReadIndex ,uint16_t wSize)
{
	uint16_t wTransferID = 0;
	
	/*Read Transfer ID from the link buffer*/
	/** @todo Function Return */
	(void)iAP2_Link_bfnReadData(&wTransferID, &wReadIndex, IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE);		
	
	/*Swap data in case of little endian*/
	#if(BIG_ENDIAN_CORE == _FALSE_)
		BYTESWAP16(wTransferID,wTransferID);
	#endif
	
	IAP2_SESSIONS_EA_RECV_CALLBACK(wTransferID, wReadIndex, wSize - IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE,
																							IAP2_SESSIONS_EA_DATA_RECV);
}


/*************************************************************************************************
*
* @brief    This function generate and send the appropriate frame to the iOS Device. This function 
*           has two options the first one is to send the complete package in the same transition.
*           If link has not enough memory the the function will send an error. The second option 
*           is to send all the information in different package with the available size of memory
*           in the moment until all data will be send. The application must support this option in 
*           case to be selected.        
* 
* @param   	wEAProtocolID		Protocol ID for the communication you want to write
*           pbDataBuffer        Pointer to begin of data buffer
*           wBuffDataSize       Size of the package to send
*           
* @return  	IAP2_SESSIONS_NEW_EXT_ACC_SESSION_FRAME_OK			(0)
			IAP2_SESSIONS_NEW_EXT_ACC_SESSION_FRAME_NO_MEM	    (1)
			IAP2_SESSIONS_NEW_EXT_ACC_SESSION_FRAME_MEM_BUSY	(2)
			IAP2_SESSIONS_NEW_EXT_ACC_WRONG_ID    				(3) 
*          
**************************************************************************************************/
//OsaTimeval g_timeval;
uint8_t iAP2_Session_EA_SendData(uint8_t *pbDataBuffer, uint16_t wEATranID, uint16_t wDataSize)
{
	uint8_t *pbWriteBuff;
	uint16_t wLinkDataSize;
	uint16_t wCount;
	uint8_t bpMemReqId;
	uint8_t bStatus;
	
	wLinkDataSize = wDataSize + IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE;
	
	//osa_time_get(&g_timeval);
        //printf("\n Start Time %d.%d\n",g_timeval.tv_sec, g_timeval.tv_usec);	
	/*Reserve memory on link buffer. wLinkDataSize will return the available memory*/
	bStatus = iAP2_Link_bfnMemoryRequest(&pbWriteBuff, &wLinkDataSize, &bpMemReqId);
        //printf("\n iAP2_Session_EA_SendData, %d %d\n",wDataSize, wLinkDataSize); 

	if(IAP2_LINK_STATUS_OK == bStatus) 
	{
		/* Write the Transfer ID to the start of the buffer */
		pbWriteBuff[0] = (wEATranID >> 8) & 0xFF00;
		pbWriteBuff[1] = wEATranID & 0x00FF; 
		wCount = IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE;

		/*write all data to the Link buffer*/
		do
		{
			pbWriteBuff[wCount] = pbDataBuffer[wCount - IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE];
			wCount++;
		}while(wCount < wLinkDataSize);
		
		/*Send data package*/
		bStatus = iAP2_Link_bfnSend(IAP2_SESSIONS_EXTERNAL_ACC_SESSION_ID, bpMemReqId, 0);
		
		if(IAP2_LINK_STATUS_OK == bStatus)
		{
			if(wLinkDataSize < (wDataSize + IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE))
			{
				/* Reserved space is not enough for all the data. Set flags and variables to continue
				 * with the state machine.
				 */
				iAP2_Sess_EA_bPendingDataFlag = _TRUE_;
				iAP2_Sess_EA_bTranID = wEATranID;
				iAP2_Sess_EA_wPendingDataSize = wDataSize - (wLinkDataSize - IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE);
				iAP2_Sess_EA_pbDataBuffer = &pbDataBuffer[wLinkDataSize - IAP2_EXT_ACC_SESSIONS_HEADER_ID_SIZE];
				//printf("\n IAP2_SESSIONS_EA_SEND_PENDING 0, %d\n",iAP2_Sess_EA_wPendingDataSize);
				bStatus = IAP2_SESSIONS_EA_SEND_PENDING;
			}
			else
			{
				/* All data was sent. Clear pending flag and return send complete status */
				iAP2_Sess_EA_bPendingDataFlag = _FALSE_;
                                bStatus = IAP2_SESSIONS_EA_SEND_COMPLETE;
				//osa_time_get(&g_timeval);
				//printf("\n End Time %d.%d\n",g_timeval.tv_sec, g_timeval.tv_usec);	

			}
		}
		else
		{
			iAP2_Link_vfnMemoryFlush(bpMemReqId);
                        printf("\n IAP2_SESSIONS_EA_SEND_RETRY 0\n");		
			bStatus = IAP2_SESSIONS_EA_SEND_RETRY;
		}
	}
	else if(bStatus == IAP2_LINK_ERROR_MEM_BUSY)
	{
		//printf("\n IAP2_LINK_ERROR_MEM_BUSY\n");		
		bStatus = IAP2_SESSIONS_EA_SEND_RETRY;
	}
	else if (bStatus == IAP2_LINK_ERROR_NO_MEM)
	{
		printf("\n IAP2_LINK_ERROR_NO_MEM\n");		
		bStatus = IAP2_SESSIONS_EA_SEND_NO_MEM;
	}
	
	if(IAP2_SESSIONS_EA_SEND_RETRY == bStatus)
	{
		/* Data could not be sent by the link. Retry through the state machine */
		iAP2_Sess_EA_bPendingDataFlag = _TRUE_;
		iAP2_Sess_EA_bTranID = wEATranID;
		iAP2_Sess_EA_wPendingDataSize = wDataSize;
		iAP2_Sess_EA_pbDataBuffer = pbDataBuffer;
		//printf("\n IAP2_SESSIONS_EA_SEND_PENDING 1, %d\n",iAP2_Sess_EA_wPendingDataSize);
	}
	
	return bStatus;
}


/*************************************************************************************************
*
* @brief    This function allows the user to read the data information on the External Accessory
*           Frame. This function must be called if is needed in the Protocol Callback.   
* 
* @param   	pbDataBuffer		Pointer where the data must be safe.
*           pbSourceDataBuffer      Pointer to link buffer where the data is read. 
*           wDataSize               Size of the data 
*           
* @return  	bRetVal                 check if the read was successful
*          
**************************************************************************************************/
uint8_t iAP2_Session_EA_ReadData(uint8_t *pbDataBuffer, uint16_t wDataSize, uint16_t wReadIndex)
{
	uint8_t bRetVal;

	bRetVal = iAP2_Link_bfnReadData(pbDataBuffer, &wReadIndex, wDataSize);	

	return bRetVal;
}

uint8_t iAP2_Sessions_bfnExtAccSM(void)
{
	uint8_t bStatus;
	
	if(_TRUE_ == iAP2_Sess_EA_bPendingDataFlag)
	{
		bStatus = iAP2_Session_EA_SendData(iAP2_Sess_EA_pbDataBuffer, iAP2_Sess_EA_bTranID, 
																					iAP2_Sess_EA_wPendingDataSize);
		
		if(IAP2_SESSIONS_EA_SEND_COMPLETE == bStatus)
		{
			/* Call callback for send complete */
			IAP2_SESSIONS_EA_RECV_CALLBACK(iAP2_Sess_EA_bTranID, 0, 0, IAP2_SESSIONS_EA_SEND_COMPLETE);
		}
		else if(IAP2_SESSIONS_EA_SEND_RETRY != bStatus)
		{
			bStatus = IAP2_SESSIONS_SM_STATUS_COMPLETE;
		}
		else
		{
			bStatus = IAP2_SESSIONS_SM_STATUS_PENDING;
		}
	}
	
	return bStatus;
}

void iAP2_Sessions_EA_vfnReset(void)
{
    iAP2_Sess_EA_bPendingDataFlag = FALSE;
    iAP2_Sess_EA_wPendingDataSize = 0;
}
