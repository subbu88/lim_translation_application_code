/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Sessions.h"
#include "MFi_Config.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define IAP2_SESSIONS_CTRL_SESSION_MAIN_PROCESS				iAP2_Sessions_Control_vfnRecvCallback

#if IAP2_SESSIONS_FILE_TRANS_ENABLE == _TRUE_
#define IAP2_SESSIONS_FILE_TRANS_SESSION_MAIN_PROCESS		iAP2_Sessions_bfnFileTransferParser
#else
#define IAP2_SESSIONS_FILE_TRANS_SESSION_MAIN_PROCESS		NULL
#endif

#if IAP2_SESSIONS_EXT_ACC_ENABLE == _TRUE_
#define IAP2_SESSIONS_EXT_ACC_SESSION_MAIN_PROCESS			iAP2_Sessions_bfnNewExt_AccSessionFrame
#else
#define IAP2_SESSIONS_EXT_ACC_SESSION_MAIN_PROCESS			NULL
#endif

/* Reset functions for each session */ 
#define IAP2_SESSIONS_CTRL_SESSION_RESET					iAP2_Sessions_Ctrl_vfnReset
#define IAP2_SESSIONS_FILE_TRANS_SESSION_RESET				iAP2_Sessions_FT_vfnReset
#define IAP2_SESSIONS_EXT_ACC_SESSION_RESET					iAP2_Sessions_EA_vfnReset

/* State machine functions for each session */ 
#define IAP2_SESSIONS_CTRL_SESSION_STATE_MACHINE			NULL
#define IAP2_SESSIONS_FILE_TRANS_SESSION_STATE_MACHINE		NULL
#define IAP2_SESSIONS_EXT_ACC_SESSION_STATE_MACHINE			iAP2_Sessions_bfnExtAccSM

#define IAP2_SESSIONS_STATEMACHINE_TASK_PRIORITY		10

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/
typedef uint8_t (* const IAP2_SESSIONS_STATE_MACHINES)(void);
typedef void (* const IAP2_SESSIONS_MAIN_PROCESS)(uint16_t wReadIndex, uint16_t wSize);

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
void IAP2_SESSIONS_CTRL_SESSION_MAIN_PROCESS(uint16_t wReadIndex, uint16_t wSize);

#if IAP2_SESSIONS_FILE_TRANS_ENABLE == _TRUE_
void IAP2_SESSIONS_FILE_TRANS_SESSION_MAIN_PROCESS(uint16_t wReadIndex, uint16_t wSize);
#endif

#if IAP2_SESSIONS_EXT_ACC_ENABLE == _TRUE_
void IAP2_SESSIONS_EXT_ACC_SESSION_MAIN_PROCESS(uint16_t wReadIndex, uint16_t wSize);
#endif

#if IAP2_SESSIONS_CTRL_SM_ENABLE == _TRUE_
uint8_t IAP2_SESSIONS_CTRL_SESSION_STATE_MACHINE(void);
#endif

#if IAP2_SESSIONS_FILE_TRANS_SM_ENABLE == _TRUE_
uint8_t IAP2_SESSIONS_FILE_TRANS_SESSION_STATE_MACHINE(void);
#endif

#if IAP2_SESSIONS_EXT_ACC_SM_ENABLE == _TRUE_
uint8_t IAP2_SESSIONS_EXT_ACC_SESSION_STATE_MACHINE(void);
#endif

void IAP2_SESSIONS_CTRL_SESSION_RESET(void);

void IAP2_SESSIONS_FILE_TRANS_SESSION_RESET(void);

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
static const IAP2_SESSIONS_STATE_MACHINES iAP2_Sessions_SMArray[IAP2_SESSIONS_SESSIONS_ID_BOUNDARY - 1] = 
{
	IAP2_SESSIONS_CTRL_SESSION_STATE_MACHINE,
	IAP2_SESSIONS_FILE_TRANS_SESSION_STATE_MACHINE,
	IAP2_SESSIONS_EXT_ACC_SESSION_STATE_MACHINE
};

static const IAP2_SESSIONS_MAIN_PROCESS iAP2_Sessions_MainProc[IAP2_SESSIONS_SESSIONS_ID_BOUNDARY - 1] =
{
	IAP2_SESSIONS_CTRL_SESSION_MAIN_PROCESS,
	IAP2_SESSIONS_FILE_TRANS_SESSION_MAIN_PROCESS,
	IAP2_SESSIONS_EXT_ACC_SESSION_MAIN_PROCESS
};


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
OsaMutex g_sessionLock[IAP2_SESSIONS_SESSIONS_ID_BOUNDARY - 1];

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
static OsaThread iAP2_Sessions_stateMachine_TaskId; 

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

uint8_t iAP2_Sessions_bfnInit(void)
{
    OsaThreadAttr attr;
	uint8_t bSessionsInitState = IAP_SESSIONS_INIT_ERROR;
	int status;

    osa_mutex_create(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1],
                     false);

#if IAP2_SESSIONS_FILE_TRANS_ENABLE == _TRUE_
    osa_mutex_create(&g_sessionLock[IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID-1],
                     false);
#endif

#if IAP2_SESSIONS_EXT_ACC_ENABLE == _TRUE_
    osa_mutex_create(&g_sessionLock[IAP2_SESSIONS_EXTERNAL_ACC_SESSION_ID-1],
                     false);
#endif
		
    osa_thread_attr_init(&attr);
    osa_thread_attr_set_stack_size(&attr, 2000);

    status = osa_thread_create(&iAP2_Sessions_stateMachine_TaskId,
                               &attr,
                               iAP2_Sessions_vfnStateMachine,
                               NULL);
    osa_thread_set_name(iAP2_Sessions_stateMachine_TaskId,
                        "iAP2_Sessions_stateMachine");

    osa_thread_attr_destroy(&attr);

    if (ERRCODE_NO_ERROR == status)
        bSessionsInitState = IAP_SESSIONS_INIT_OK;

    return bSessionsInitState;
}

uint8_t iAP2_Sessions_bfnDeInit(void)
{
	uint8_t bSessionDeinitStatus = IAP_SESSIONS_INIT_ERROR;

	int taskDestroyStatus;

	/*Destroy iAP2_Host stack*/
    if (iAP2_Sessions_stateMachine_TaskId)
    {
	taskDestroyStatus = osa_thread_destroy(iAP2_Sessions_stateMachine_TaskId);

	if(taskDestroyStatus == ERRCODE_NO_ERROR)
	{
		bSessionDeinitStatus = IAP_SESSIONS_INIT_OK;
#if IAP2_SESSIONS_CTRL_RESET_ENABLE == _TRUE_
		IAP2_SESSIONS_CTRL_SESSION_RESET();
		osa_mutex_destroy(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
#endif

#if IAP2_SESSIONS_FILE_TRANS_RESET_ENABLE == _TRUE_
		IAP2_SESSIONS_FILE_TRANS_SESSION_RESET();
		osa_mutex_destroy(&g_sessionLock[IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID-1]);
#endif

#if IAP2_SESSIONS_EXT_ACC_RESET_ENABLE == _TRUE_
		IAP2_SESSIONS_EXT_ACC_SESSION_RESET();
		osa_mutex_destroy(&g_sessionLock[IAP2_SESSIONS_EXTERNAL_ACC_SESSION_ID-1]);
#endif
    	}
	}
	return bSessionDeinitStatus;
}

void iAP2_Sessions_bfnNewFrameCallback(uint8_t bSessionId, uint16_t wReadIndex ,uint16_t wSize)
{
	/* If session id corresponds to a control message type */
	if(bSessionId < IAP2_SESSIONS_SESSIONS_ID_BOUNDARY)
	{
		/* Check that the session is enabled. Need to subtract 1 because the session ids start at 1, while the array
		 * starts at 0.
		 */
		if(iAP2_Sessions_MainProc[bSessionId - 1] != NULL)
		{
			iAP2_Sessions_MainProc[bSessionId - 1](wReadIndex, wSize);
		}
	}
}

#if (FSL_OS_NONE == FSL_OS_SELECTED)
void iAP2_Sessions_vfnStateMachine(void)
#else
void iAP2_Sessions_vfnStateMachine(void * param)
#endif
{
#if (IAP2_SESSIONS_CTRL_SM_ENABLE == _TRUE_) || (IAP2_SESSIONS_FILE_TRANS_SM_ENABLE == _TRUE_) || (IAP2_SESSIONS_EXT_ACC_SM_ENABLE == _TRUE_)
	static uint8_t bActiveSM = 0;
	uint8_t bStatus;
	uint8_t bRetry;

        dprint("iAP2_Sessions_vfnStateMachine started\n");
    printf("iAP2_Sessions_vfnStateMachine started\n");


#if (FSL_OS_NONE != FSL_OS_SELECTED)
	do
	{
#endif
	    
	bRetry = _FALSE_;
	do
	{
		if(iAP2_Sessions_SMArray[bActiveSM] != NULL)
		{
			bStatus = iAP2_Sessions_SMArray[bActiveSM]();
			
			if(bStatus == IAP2_SESSIONS_SM_STATUS_COMPLETE)
			{
				bActiveSM++;
			}
			
			bRetry = _FALSE_;
		}
		else
		{
			bActiveSM++;
			bRetry = _TRUE_;
		}
		
		if((IAP2_SESSIONS_SESSIONS_ID_BOUNDARY - 1) == bActiveSM)
		{
			bActiveSM = 0;
		}
		
		osa_time_delay(20);
		//usleep(1);
	}while(bRetry != _FALSE_);
#if (FSL_OS_NONE != FSL_OS_SELECTED)
	}while(true);
#endif
#endif
}

void iAP2_Sessions_vfnReset(void)
{
	
	#if IAP2_SESSIONS_CTRL_RESET_ENABLE == _TRUE_
	IAP2_SESSIONS_CTRL_SESSION_RESET();
	osa_mutex_destroy(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1]);
	osa_mutex_create(&g_sessionLock[IAP2_SESSIONS_CTRL_MSG_SESSION_ID-1],
                     false);
	#endif
	
	#if IAP2_SESSIONS_FILE_TRANS_RESET_ENABLE == _TRUE_
	IAP2_SESSIONS_FILE_TRANS_SESSION_RESET();
	osa_mutex_destroy(&g_sessionLock[IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID-1]);
	osa_mutex_create(&g_sessionLock[IAP2_SESSIONS_FILE_TRANSFER_SESSION_ID-1],
                     false);
	#endif
	
	#if IAP2_SESSIONS_EXT_ACC_RESET_ENABLE == _TRUE_
	IAP2_SESSIONS_EXT_ACC_SESSION_RESET();
	osa_mutex_destroy(&g_sessionLock[IAP2_SESSIONS_EXTERNAL_ACC_SESSION_ID-1]);
	osa_mutex_create(&g_sessionLock[IAP2_SESSIONS_EXTERNAL_ACC_SESSION_ID-1],
                     false);
	#endif
}
