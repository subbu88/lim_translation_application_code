/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_HID_FEAT_H_
#define IAP2_HID_FEAT_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "mfi_cfg.h"
/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/* Usage enable macros */
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define IAP2_HID_MEDIA_PLAYBACK_EN				(_TRUE_)
#define IAP2_HID_PLAY_EN						(_TRUE_)
#define IAP2_HID_PAUSE_EN						(_TRUE_)
#define IAP2_HID_PLAY_PAUSE_EN					(_TRUE_)
#define IAP2_HID_SCAN_NEXT_TRACK_EN				(_TRUE_)
#define IAP2_HID_SCAN_PREV_TRACK_EN				(_TRUE_)
#define IAP2_HID_VOL_UP_EN						(_TRUE_)
#define IAP2_HID_VOL_DOWN_EN                    (_TRUE_)
#define IAP2_HID_RANDOM_EN                      (_TRUE_)
#define IAP2_HID_REPEAT_EN                      (_TRUE_)
#define IAP2_HID_TRACK_NORM_EN                  (_TRUE_)
#define IAP2_HID_TRACK_INC_EN					(_TRUE_)
#define IAP2_HID_TRACK_DEC_EN                   (_TRUE_)
#define IAP2_HID_MUTE_EN                        (_TRUE_)
#define IAP2_HID_USAGE_CNT						(13)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION


#define IAP2_HID_PADDING_CNT					(16 - IAP2_HID_USAGE_CNT)

#define IAP2_HID_FEAT_START							(0x6800)
#define IAP2_HID_FEAT_ACC_HID_REPORT				(0x6802)
#define IAP2_HID_FEAT_STOP_HID						(0x6803)

#define IAP2_HID_FEAT_NO_DATA						(0x0000)

#define NUMBER_PARAMETERS_START_HID					(0x04)
#define NUMBER_PARAMETERS_ACC_HID					(0x01)
#define KEY_BOARD_COUNTRY_PARAMETER_ID				(0x0003)
#define HID_REPORT_PARAMETER_ID						(0x0004)
#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
#define KEY_BOARD_COUNTRY_CODE_PRESENT				(_FALSE_)  /* Must be set by the user */
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION

#define HID_COMPONENT_ID							(0x0000)
#define HID_COMPONENT_ID_SIZE						(0x0002)

#define SIZE_ERROR									(0x05)

#if (KEY_BOARD_COUNTRY_CODE_PRESENT == _TRUE_)
	#define KEY_BOARD_COUNTRY_CODE_SIZE					0x0001
#else
	#define KEY_BOARD_COUNTRY_CODE_SIZE					0x0000
#endif

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
#if IAP2_HID_MEDIA_PLAYBACK_EN == _TRUE_
typedef enum
{
#if IAP2_HID_PLAY_EN
	IAP2_HID_MP_PLAY = 0,
#endif
#if IAP2_HID_PAUSE_EN
	IAP2_HID_MP_PAUSE,
#endif
#if IAP2_HID_PLAY_PAUSE_EN
	IAP2_HID_MP_PLAY_PAUSE,
#endif
#if IAP2_HID_SCAN_NEXT_TRACK_EN
	IAP2_HID_MP_SCAN_NEXT_TRACK,
#endif
#if IAP2_HID_SCAN_PREV_TRACK_EN
	IAP2_HID_MP_SCAN_PREV_TRACK,
#endif
#if IAP2_HID_VOL_UP_EN
	IAP2_HID_MP_VOL_UP,
#endif
#if IAP2_HID_VOL_DOWN_EN
	IAP2_HID_MP_VOL_DOWN,
#endif
#if IAP2_HID_RANDOM_EN
	IAP2_HID_MP_RANDOM,
#endif
#if IAP2_HID_REPEAT_EN
	IAP2_HID_MP_REPEAT,
#endif
#if IAP2_HID_TRACK_NORM_EN
	IAP2_HID_MP_TRACK_NORM,
#endif
#if IAP2_HID_TRACK_INC_EN
	IAP2_HID_MP_TRACK_INC,
#endif
#if IAP2_HID_TRACK_DEC_EN
	IAP2_HID_MP_TRACK_DEC,
#endif
#if IAP2_HID_MUTE_EN
	IAP2_HID_MP_MUTE
#endif
}IAP2_HID_MED_PB_BUTTONS;
#endif
/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
uint8_t iAP2_HID_Feat_bfnStartHID(uint16_t wHIDComponentIdentifier, uint16_t wSizeHIDReportDescriptor, \
		uint8_t LocalizedKeyBoardCountryCode, uint8_t *HIDReportDescriptor);

uint8_t iAP2_HID_Feat_bfnAccessoryHIDReport(uint16_t wHIDComponentIdentifier, uint16_t wSizeHIDReport, uint8_t *HIDReport);

uint8_t iAP2_HID_Feat_bfnStopHID(uint16_t wHIDComponentIdentifier);

void iAP2_HID_Feat_vfnDeviceHIDReport(uint16_t wMessageID);

uint8_t iAP2_HID_Feat_bfnGetParamData(uint16_t* wParameterID, void* wParameterData, uint16_t* wParamSize);

#if IAP2_HID_MEDIA_PLAYBACK_EN == _TRUE_
uint8_t iAP2_HID_Feat_bfnMediaPBStart(void);

uint8_t iAP2_HID_Feat_bfnMediaPBPressButton(uint8_t bButton);

uint8_t iAP2_HID_Feat_bfnMediaPBReleaseButton(uint8_t bButton);

uint8_t iAP2_HID_Feat_bfnMediaPBStop(void);
#endif

/*************************************************************************************************/

#endif /* IAP2_HID_FEAT_H_ */
