/*HEADER******************************************************************************************
*
* Copyright 2016 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
* FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
* file except in compliance with the License.
*
*************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
*END*********************************************************************************************/

#ifndef IAP2_ASSISTIVETOUCH_FEAT_H_
#define IAP2_ASSISTIVETOUCH_FEAT_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/

#define IAP2_ASSISTIVE_TOUCH_FEAT_START				(0x5400)

#define IAP2_ASSISTIVE_TOUCH_FEAT_STOP				(0x5401)

#define IAP2_ASSISTIVE_TOUCH_FEAT_START_INFO		        (0x5402)

#define IAP2_ASSISTIVE_TOUCH_FEAT_INFORMATION		        (0x5403)

#define IAP2_ASSISTIVE_TOUCH_FEAT_STOP_INFO			(0x5404)

#define iAP2_ASSISTIVE_TOUCH_FEATURE_OK				(0U)

#define IAP2_ASSISTIVE_TOUCH_CALLBACK				(AssistiveTouch_UpdateCallback)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/

typedef enum
{
        iAP2_ASSISTIVE_TOUCH_FEATURE_DISABLED = 0,
        iAP2_ASSISTIVE_TOUCH_FEATURE_ENABLED 
} iAP2_AssistiveTouchUpdates_Information;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern void IAP2_ASSISTIVE_TOUCH_CALLBACK(uint8_t bAssistiveTouchStatus);
/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
#if defined(__cplusplus)
extern "C" {
#endif

void iAP2_Feat_AssistTouch_vfnCallBack(uint16_t wMessageID);
uint8_t iAP2_AssistiveTouch_Feat_bfnStartAssistiveTouch(void);
uint8_t iAP2_AssistiveTouch_Feat_bfnStartAssistiveTouchInformation(void);
uint8_t iAP2_AssistiveTouch_Feat_bfnStopAssistiveTouch(void);
uint8_t iAP2_AssistiveTouch_Feat_bfnStopAssistiveTouchInformation(void);

#if defined(__cplusplus)
}
#endif

#endif /* IAP2_ASSISTIVETOUCH_FEAT_H_ */
/*************************************************************************************************/
/*                                             EOF                                               */
/*************************************************************************************************/
