/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_SESSIONS_CONTROL_H_
#define IAP2_SESSIONS_CONTROL_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "iAP2_Sessions.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP_SESSIONS_NEW_CTRL_SESSION_FRAME_ERROR		(_TRUE_)
#define IAP_SESSIONS_NEW_CTRL_SESSION_FRAME_OK			(_FALSE_)
#define IAP_SESSIONS_READ_PARAM_INFO_ERROR				(_TRUE_)
#define IAP_SESSIONS_READ_PARAM_INFO_OK					(_FALSE_)
#define IAP_SESSIONS_READ_SUBPARAM_INFO_ERROR			(_TRUE_)
#define IAP_SESSIONS_READ_SUBPARAM_INFO_OK 				(_FALSE_)

#define CTRL_MSG_PARAMHEADER_SIZE		(4) //unit: Byte
#define CTRL_MSG_SUBPARAMHEADER_SIZE	(4) //unit: Byte

#define IAP2_SESSIONS_CTRL_HEADER(x)  	(CTRL_MSG_HEADER_SIZE + (x * CTRL_MSG_PARAMHEADER_SIZE))

#define IAP2_SESSIONS_CTRL_MSG_NO_SUBPARAM				(0xFFFF)
/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
enum SessionsStatus
{
	SESSIONS_OK = 0, /* SESSIONS_OK must be =0 */
	SESSIONS_BUSY,
	SESSIONS_MEM_FULL,
	SESSIONS_WRONG_INIT,
	SESSIONS_LOCK_FAIL
};

typedef enum
{
	CTRL_MSG_HEADER_SOM_MSB = 0,
	CTRL_MSG_HEADER_SOM_LSB,
	CTRL_MSG_HEADER_SIZE_MSB,
	CTRL_MSG_HEADER_SIZE_LSB,
	CTRL_MSG_HEADER_ID_MSB,
	CTRL_MSG_HEADER_ID_LSB,
	CTRL_MSG_HEADER_SIZE
}CTRL_MSG_HEADER_IDX_t;

typedef enum
{
	IAP2_SESS_CTRL_NEST_MSG = 0,
	IAP2_SESS_CTRL_NEST_PARAM,
	IAP2_SESS_CTRL_NEST_SUBPARAM,
	IAP2_SESS_CTRL_NEST_MAX
}IAP2_SESS_CTRL_NEST_LEVELS;

typedef enum
{
	IAP2_SESS_CTRL_PARAM_COMPLETE = 0,
	IAP2_SESS_CTRL_GROUP_COMPLETE,
	IAP2_SESS_CTRL_MESSAGE_COMPLETE,
	IAP2_SESS_CTRL_DATA_PENDING
}IAP2_SESS_CTRL_DATA_STATUS;

typedef enum
{
	IAP2_SESS_CTRL_OK = 0,
	IAP2_SESS_CTRL_TOO_MANY_GROUPS,
	IAP2_SESS_CTRL_WAIT_NEXT_PACKET
}IAP2_SESS_CTRL_STATUS_t;

typedef enum
{
	IAP2_SESSIONS_CTRL_PEND_DATA_ADJ = 0,
	IAP2_SESSIONS_CTRL_READ_IN_PROG
}IAP2_SESSIONS_CTRL_FLAG_t;
/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

extern uint8_t iAP2_Sessions_gbCtrlMsgStatusFlags;
/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
uint8_t iAP2_Sessions_bfnSendCtrlMessage(uint16_t wCtrlMsgID, uint8_t baData[], uint16_t wDataSize, uint16_t wMaxDataSize);
uint8_t iAP2_Sessions_bfnAddCtrlMsgParam(uint16_t wParamID, uint16_t wSubParamId, uint8_t baData[], uint16_t wDataSize);
uint8_t iAP2_Sessions_bfnEndSendCtrlMsg(void);
uint8_t iAP2_Sessions_bfnGetCtrlMsgParamInfo(uint16_t* wpParamID, uint16_t* wpParamSize);
uint8_t iAP2_Sessions_bfnReadCtrlMsgParamData(void* vpParamData, uint16_t wDataSize);
uint8_t iAP2_Sessions_bfnCtrlMsgGroupStart(void);
void iAP2_Sessions_vfnResetMessage(void);
void iAP2_Sessions_vfnCloseGroup(void);

/*************************************************************************************************/

#endif /* IAP2_SESSIONS_CONTROL_H_ */
