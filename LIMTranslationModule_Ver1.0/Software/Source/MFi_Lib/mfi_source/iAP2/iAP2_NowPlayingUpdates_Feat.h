/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_NOWPLAYINGUPDATES_H_
#define IAP2_NOWPLAYINGUPDATES_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include "string.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/*Callback from NowPlayingUpdate Feat to Application*/
#define iAP2_CALLBACK_FROM_NOW_PLAYING_UPDATE				(User_NowPlayingUpdate_Callback)	

/*Identification MessageID */
#define iAP2_NOW_PLAYING_UPDATES_START				(0x5000)
#define iAP2_NOW_PLAYING_UPDATES_STOP				(0x5002)
#define iAP2_NOW_PLAYING_UPDATES				    (0x5001)

/*Flags for Received Media Item parameters */
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_TITLE							(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_TITLE_BIT)
#define	iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PLAYBACK_DURATION				(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_PLAYBACK_DURATION_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_TRACK_NUMBER				(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_TRACK_NUMBER_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_TITLE						(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_TITLE_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_TRACK_COUNT				(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_TRACK_COUNT_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_DISC_NUMBER				(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_DISC_NUMBER_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ALBUM_DISC_COUNT				(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_DISC_COUNT_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ARTIST							(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_ARTIST_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_GENRE							(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_GENRE_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_COMPOSER						(1<<iAP2_NOW_PLAYING_MEDIA_ITEM_COMPOSER_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PROPERTY_IS_LIKE_SUPPORTED      (1<<iAP2_NOW_PLAYING_MEDIA_ITEM_PROPERTY_IS_LIKE_SUPPORTED_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PROPERTY_IS_BAN_SUPPORTED       (1<<iAP2_NOW_PLAYING_MEDIA_ITEM_PROPERTY_IS_BAN_SUPPORTE_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PROPERTY_IS_LIKED               (1<<iAP2_NOW_PLAYING_MEDIA_ITEM_PROPERTY_IS_LIKED_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_PROPERTY_IS_BANNED              (1<<iAP2_NOW_PLAYING_MEDIA_ITEM_PROPERTY_IS_BANNED_BIT)
#define iAP2_NOW_PLAYING_UPDATES_MEDIA_ITEM_ARTWORK_FILE_TRANSFER_IDENTIFIER (1<<iAP2_NOW_PLAYING_MEDIA_ITEM_ARTWORK_FILE_TRANSFER_IDENTIFIER_BIT)

/*Flags for Received playback status parameters */
#define	iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_STATUS						    (1<<iAP2_NOW_PLAYING_PLAYBACK_STATUS_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_ELAPSED_TIME				    (1<<iAP2_NOW_PLAYING_PLAYBACK_STATUS_ELAPSED_TIME_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_QUEUE_INDEX				    (1<<iAP2_NOW_PLAYING_PLAYBACK_STATUS_QUEUE_INDEX_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_QUEUE_COUNT				    (1<<iAP2_NOW_PLAYING_PLAYBACK_STATUS_QUEUE_COUNT_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_QUEUE_CHAPTER_INDEX 		    (1<<iAP2_NOW_PLAYING_PLAYBACK_STATUS_QUEUE_CHAPTER_INDEX_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_SHUFFLE_MODE				    (1<<iAP2_NOW_PLAYING_PLAYBACK_STATUS_SHUFFLE_MODE_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_REPEAT_MODE				    (1<<iAP2_NOW_PLAYING_PLAYBACK_STATUS_REPEAT_MODE_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STATUS_APP_NAME					    (1<<iAP2_NOW_PLAYING_PLAYBACK_STATUS_APP_NAME_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_MEDIA_LIBRARY_UNIQUE_ID               (1<<iAP2_NOW_PLAYING_PLAYBACK_MEDIA_LIBRARY_UNIQUE_ID_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_PB_ITUNES_RADIO_AD                    (1<<iAP2_NOW_PLAYING_PLAYBACK_PB_ITUNES_RADIO_AD_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_PB_ITUNES_RADIO_STATION_NAME          (1<<iAP2_NOW_PLAYING_PLAYBACK_PB_ITUNES_RADIO_STATION_NAME_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_PB_ITUNES_RADIO_STATION_PERSISTENT_ID (1<<iAP2_NOW_PLAYING_PLAYBACK_PB_ITUNES_RADIO_STATION_PERSISTENT_ID_BIT)

#define iAP2_NOW_PLAYING_UPDATES_ERROR_OK										 (0)
#define iAP2_NOW_PLAYING_UPDATES_INVALID_DATA_PARAMETER							(0xFF)

// NMFI-26
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_SPEED									(1<<iAP2_NOW_PLAYING_PLAYBACK_SPEED_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_SET_ELAPSED_TIME						(1<<iAP2_NOW_PLAYING_PLAYBACK_SET_ELAPSED_TIME_AVAILABLE_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_QUEUE_LIST_AVAILABLE					(1<<iAP2_NOW_PLAYING_PLAYBACK_QUEUE_LIST_AVAILABLE_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_QUEUE_LIST_TRANFER_ID					(1<<iAP2_NOW_PLAYING_PLAYBACK_QUEUE_LIST_TRANSFER_ID_BIT)
#define iAP2_NOW_PLAYING_UPDATES_PLAYBACK_APP_BUNDLE_ID							(1<<iAP2_NOW_PLAYING_PLAYBACK_APP_BUNDLE_ID_BIT)



/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
/*Possible values for PlaybackStatus attribute*/
typedef enum
{
	iAP2_NOW_PLAYING_UPDATES_PLAYBACK_STOPPED = 0,
	iAP2_NOW_PLAYING_UPDATES_PLAYBACK_PLAYING,
	iAP2_NOW_PLAYING_UPDATES_PLAYBACK_PAUSED,
	iAP2_NOW_PLAYING_UPDATES_PLAYBACK_SEEKFORWARD,
	iAP2_NOW_PLAYING_UPDATES_PLAYBACK_SEEKBACKWARD
} iAP2_NowPlayingUpdates_ePlaybackStatus;


/*Possible values for PlaybackSuffle attribute*/
typedef enum
{
	iAP2_NOW_PLAYING_UPDATES_SUFFLE_OFF = 0,
	iAP2_NOW_PLAYING_UPDATES_SUFFLE_SONGS,
	iAP2_NOW_PLAYING_UPDATES_SUFFLE_ALBUMS
} iAP2_NowPlayingUpdates_ePlaybackSuffle;

/*Possible values for PlaybackRepeat attribute*/
typedef enum
{
	iAP2_NOW_PLAYING_UPDATES_REPEAT_OFF = 0,
	iAP2_NOW_PLAYING_UPDATES_REPEAT_ONE,
	iAP2_NOW_PLAYING_UPDATES_REPEAT_ALL
} iAP2_NowPlayingUpdates_ePlaybackRepeat;

/*NowPlayingUpdates media item flags*/
enum MediaItemBitFlags
{
    iAP2_NOW_PLAYING_MEDIA_ITEM_TITLE_BIT =0,
    iAP2_NOW_PLAYING_MEDIA_ITEM_PLAYBACK_DURATION_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_TRACK_NUMBER_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_TITLE_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_TRACK_COUNT_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_DISC_NUMBER_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_ALBUM_DISC_COUNT_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_ARTIST_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_GENRE_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_COMPOSER_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_PROPERTY_IS_LIKE_SUPPORTED_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_PROPERTY_IS_BAN_SUPPORTE_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_PROPERTY_IS_LIKED_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_PROPERTY_IS_BANNED_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_ARTWORK_FILE_TRANSFER_IDENTIFIER_BIT,
    iAP2_NOW_PLAYING_MEDIA_ITEM_MAX_VALID_PARAMETER
};

/*NowPlayingUpdates playback status item flags*/
enum PlaybackStatusBitFlags
{
    iAP2_NOW_PLAYING_PLAYBACK_STATUS_BIT=0,
    iAP2_NOW_PLAYING_PLAYBACK_STATUS_ELAPSED_TIME_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_STATUS_QUEUE_INDEX_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_STATUS_QUEUE_COUNT_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_STATUS_QUEUE_CHAPTER_INDEX_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_STATUS_SHUFFLE_MODE_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_STATUS_REPEAT_MODE_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_STATUS_APP_NAME_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_MEDIA_LIBRARY_UNIQUE_ID_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_PB_ITUNES_RADIO_AD_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_PB_ITUNES_RADIO_STATION_NAME_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_PB_ITUNES_RADIO_STATION_PERSISTENT_ID_BIT,
    // NMFI-26
    iAP2_NOW_PLAYING_PLAYBACK_SPEED_BIT,							//12
    iAP2_NOW_PLAYING_PLAYBACK_SET_ELAPSED_TIME_AVAILABLE_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_QUEUE_LIST_AVAILABLE_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_QUEUE_LIST_TRANSFER_ID_BIT,
    iAP2_NOW_PLAYING_PLAYBACK_APP_BUNDLE_ID_BIT,					//16
    iAP2_NOW_PLAYING_PLAYBACK_MAX_VALID_PARAMETER
};

/*Structure where the received NowPlayingUpdates information would be stored*/
typedef struct 
{
    uint64_t qwPBiTunesRadioStationMediaPlaylistPersistentID;
	uint32_t dwMediaItemPlaybackDuration;
	uint32_t dwPlaybackElapsedTime;
	uint32_t dwPlaybackQueueIndex;
	uint32_t dwPlaybackQueueCount;
	uint32_t dwPlaybackQueueChapterIndex;
	uint32_t dwPlaybackAttributesUpdated; /*Flags with the PlaybackAttributes parameters received in NowPlayingUpdate message*/
	uint32_t dwMediaItemAttributesUpdated; 	/*Flags with the MediaItemAttributes parameters received in NowPlayingUpdate message*/
	uint8_t *pbMediaItemMediaTitle; /*Need to be initialized by application */
	uint8_t *pbMediaItemAlbumTitle; /*Need to be initialized by application */
	uint8_t *pbMediaItemArtist; /*Need to be initialized by application */
	uint8_t *pbMediaItemGenre; /*Need to be initialized by application */
	uint8_t *pbMediaItemComposer; /*Need to be initialized by application */
	uint8_t *pbPlaybackAppName; /*Need to be initialized by application */
	uint8_t *pbPBMediaLibraryUniqueIdentifier; /*Need to be initialized by application */
	uint8_t *pbPBiTunesRadioStationName; /*Need to be initialized by application */
	uint16_t wMediaItemAlbumTrackNumber;
	uint16_t wMediaItemAlbumTrackCount;
	uint16_t wMediaItemAlbumDiscNumber;
	uint16_t wMediaItemAlbumDiscCount;
	uint8_t bMediaItemMediaTitleSize;	/*Size reserved for pbMediaItemMediaTitle parameter*/
	uint8_t bMediaItemAlbumTitleSize; 	/*Size reserved for pbMediaItemAlbumTitle parameter*/
	uint8_t bMediaItemArtistSize; 		/*Size reserved for pbMediaItemArtist parameter*/
	uint8_t bMediaItemGenreSize;			/*Size reserved for pbMediaItemGenre parameter*/
	uint8_t bMediaItemComposerSize;		/*Size reserved for pbMediaItemComposer parameter*/
	uint8_t bPlaybackAppNameSize;		/*Size reserved for pbPlaybackAppName parameter*/
	uint8_t bPBMediaLibraryUniqueIdentifierSize; /*Size reserved for pbPBMediaLibraryUniqueIdentifier parameter*/
	uint8_t bPBiTunesRadioStationNameSize; /*Size reserved for pbPBiTunesRadioStationName parameter*/
	uint8_t bMediaItemArtworkFileTransferIdentifier;
	uint8_t bMediaItemPropertyIsLikeSupported;
    uint8_t bMediaItemPropertyIsBanSupported;
    uint8_t bMediaItemPropertyIsLiked;
    uint8_t bMediaItemPropertyIsBanned;
	uint8_t bPlaybackStatus; 
	uint8_t bPlaybackSuffleMode;
	uint8_t bPlaybackStatusRepeatMode;	
	uint8_t bPBiTunesRadioAd;
	
} iAP2_NowPlayingUpdatesData;                /* Device data structure type */

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
void iAP2_Feat_NowPlay_vfnCallBack(uint16_t MessageID);
uint8_t iAP2_NowPlayingUpdates_bfnStartPlayingUpdates(uint16_t wMediaItemAttributes, uint16_t wPlaybackAttributes,iAP2_NowPlayingUpdatesData * NowPlayingAppDataPtr);
uint8_t iAP2_NowPlayingUpdates_bfnStopPlayingUpdates(void);

extern void iAP2_CALLBACK_FROM_NOW_PLAYING_UPDATE(uint16_t MessageID); 


/*************************************************************************************************/

#endif /* IAP2_NOWPLAYINGUPDATES_H_ */
