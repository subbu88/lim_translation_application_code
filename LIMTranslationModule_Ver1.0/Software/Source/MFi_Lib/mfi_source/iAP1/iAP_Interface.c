/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP_Interface.h"
#include <stdbool.h>
#include "fsl_rtos_abstraction_bm.h"


/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
/*! Minimum packet size with transaction ID */
#define IAPI_MIN_PKT_SIZE_WITH_TRANSID              (4)

/*! Beginning of the reception buffer */
#define IAPI_RECEPTION_BUFFER                       g_IapiBuffers.buffer0

/*! Extended lingo value */
#define IAPI_EXTENDED_LINGO_ID                      (4)

/*! Minimun length value for commands with only 1-byte length field*/
#define MIN_LENGTH_FOR_ONE_BYTE_LENGTH_FIELD        (0x02)

/*! Maximun length value for commands with only 1-byte length field*/
#define MAX_LENGTH_FOR_ONE_BYTE_LENGTH_FIELD        (0xFD)

/* Set the initial state for State machine */
#if(_TRUE_ == IAPI_RX_SYNC_BYTE_ENABLE)
#define kIapiRxCallbackInitialState                 kIapiSof1State
#else
#define kIapiRxCallbackInitialState                 kIapiSof2State
#endif

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/
/*! Interface Rx callback state machine states */
typedef enum
{
    /*! Rx callback SM waiting for SOF1 state */
    kIapiSof1State = 0,     /* 0x00 */
    /*! Rx callback SM waiting for SOF2 state */
    kIapiSof2State,
    /*! Rx callback SM state to save the length of the receiving packet */
    kIapiLengthState,
    /*! Rx callback SM state to save the lingo of the receiving packet */
    kIapiLingoIdState,
    /*! Rx callback SM state to save the command of the receiving packet */
    kIapiCommandIdState,
    /*! Rx callback SM state to save the TID of the receiving packet */
    kIapiTransactionIdState,
    /*! Rx callback SM state to save the payload of the receiving packet */
    kIapiPayloadState,
    /*! Rx callback SM state to check if the calculate checksum  matches with the received checksum */
    kIapiChecksumState,
    /*! Rx callback SM  idle state */
    kIapiIdleState
}iapiReceptionCallbackStates_t;

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/
static void s_transport_transmission_callback   (void);
static void s_transport_reception_callback      (uint8_t* dataPointer, uint16_t dataSize);

/*! Rx States*/

/*! Validate the SOF1 and SOF2 of the receiving packet  */
static void s_iap_interface_sof1_rx_state       (uint8_t rxByte);
static void s_iap_interface_sof2_rx_state       (uint8_t rxByte);

/*! Stores the length of the receiving packet */
static void s_iap_interface_length_rx_state     (uint8_t rxByte);

/*! Stores the lingo of the receiving packet */
static void s_iap_interface_lingo_id_rx_state   (uint8_t rxByte);

/*! Stores the command of the receiving packet */
static void s_iap_interface_command_id_rx_state (uint8_t rxByte);

/*! Stores the TID of the receiving packet */
static void s_iap_interface_tid_rx_state        (uint8_t rxByte);

/*! Stores the payload of the receiving packet */
static void s_iap_interface_payload_rx_state    (uint8_t rxByte);

/*! Checks if the calculate checksum  matches with the received checksum */
static void s_iap_interface_checksum_rx_state   (uint8_t rxByte);


static inline uint32_t s_iap_interface_get_time_stamp(void);

static inline iap_interface_command_timeout_t s_iap_interface_has_retry_time_expired
                                                (uint32_t commandTimeStamp,
                                                 uint32_t commandTimeout);
static void s_iap_interface_check_retry_commands_array(void);

static void s_iap_interface_check_transmission_complete(void);

static uint8_t s_iap_interface_calculate_checksum(uint8_t* dataPacket, uint16_t packetSize);

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/

/*! Variable that stores the value of the sync byte, a variable must be used in order to be 
 * able to use the macro IAP_INTERFACE_SEND_SYNC_BYTE */
const uint8_t g_iAPInterfaceSyncByte = IAP_INTERFACE_SYNC_BYTE;

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
/*! States for Reception callback state machine */
static void (*const s_kReceptionCallbackStates[])(uint8_t data) =
{
        s_iap_interface_sof1_rx_state,
        s_iap_interface_sof2_rx_state,
        s_iap_interface_length_rx_state,
        s_iap_interface_lingo_id_rx_state,
        s_iap_interface_command_id_rx_state,
        s_iap_interface_tid_rx_state,
        s_iap_interface_payload_rx_state,
        s_iap_interface_checksum_rx_state
};
/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
/*! Current receiving frame */
volatile iap_interface_buffer_rx_frame_t*    g_iapiCurrentReceivedFrame;

/*! Variable to enable or disable TID */
volatile bool g_iapInterfaceEnableTransactionId;

/*! Variable to move the Reception callback Interface state machine */
uint8_t g_iapInterfaceReceptionCallbackActualState;     

/*! This variable is the Tx transaction ID, it is used to store the current TID for the Tx packets */
uint16_t g_iapInterfaceTransactionId = 0;

/*! Handler of the transmission queue */
msg_queue_handler_t g_iapInterfaceTxQueueHandler;

/*! Handler of the reception queue */
msg_queue_handler_t g_iapInterfaceRxQueueHandler;

/*! Global declaration of the Tx queue */
// TODO Change macros (NUM_OF_TX_PACKETS and NUM_OF_RX_PACKETS)
MSG_QUEUE_DECLARE(g_iapInterfaceTxQueue, NUM_OF_TX_PACKETS,
        sizeof(iap_interface_data_transmit_packet_t)/sizeof(uint32_t));

/*! Global declaration of the Rx queue */
MSG_QUEUE_DECLARE(g_iapInterfaceRxQueue, NUM_OF_RX_PACKETS,
        sizeof(iap_interface_buffer_rx_frame_t*)/sizeof(uint32_t));

/*! Global declaration of Interface Retry Commands Array*/
random_access_array_information_t g_iapInterfaceRetryCommandsArrayInfo = {0, 0, 0, NUM_OF_TX_PACKETS};
iap_interface_data_retry_transmit_packet_t g_iapInterfaceRetryCommandsArray[NUM_OF_TX_PACKETS];

/*! Current Status of the receiving packet*/
volatile iap_interface_rx_packet_status_t g_iapInterfaceRxStatus;

/*! Current Status of the transmission packet */
volatile iap_interface_tx_packet_status_t g_iapInterfaceTxStatus;

/*! Flag used by the stack is in MFI_TRANSPORT_USB_HOST_MODE  */
volatile bool iAPI_RxCallbackFlag;

/*! Variable that saves the maximum Tx payload buffer size */
uint16_t g_txPayloadBufferSize = IAPI_MAX_TX_PAYLOAD_SIZE;

iap_interface_data_transmit_packet_t g_iapInterfaceCurrentTxPacket;

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

/*! Variable that saves the maximum Rx payload buffer size */
const static uint16_t s_kRxpayloadBufferSize = IAPI_MAX_RX_PAYLOAD_SIZE;

/*! Variable to keep track of the number of received bytes */
volatile static uint16_t s_receivedBytesCount;  

/*! Variable to calculate the checksum */
volatile static uint8_t    s_calculateChecksum;    

/*! Interface pointer to function that stores the address of the iAP_Commands function 
 * that will be called if a command has been retried 'IAPI_MAX_RETRY_INTENTS' times and
 * it hasn't received response */
static void (*s_iap_interface_no_response_error_callback)(uint8_t retryCommandsArrayIndex);

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
/* See iAP_Interface.h for documentation of this function. */
void iap_interface_initialization(void)
{
    RandomAccessArray_flush_array(&g_iapInterfaceRetryCommandsArrayInfo);

    g_iapInterfaceRxStatus = 0x0000;

    IAPI_ENABLE_TRANSACTION_ID;
    g_iapInterfaceTransactionId = 0x0000;

    g_iapInterfaceTxQueueHandler = msg_queue_create(g_iapInterfaceTxQueue, NUM_OF_TX_PACKETS,
            sizeof(iap_interface_data_transmit_packet_t)/sizeof(uint32_t));
    
    g_iapInterfaceRxQueueHandler = msg_queue_create(g_iapInterfaceRxQueue, NUM_OF_RX_PACKETS,
            sizeof(iap_interface_buffer_rx_frame_t*)/sizeof(uint32_t));

    buffersAlloc_resetAllBuffersStatus();

    /* Reserve space for the first packet */
    g_iapiCurrentReceivedFrame = (iap_interface_buffer_rx_frame_t*) buffersAlloc_reserveSpace
            (kIapiReceptionBufferIndex, IAPI_RX_PACKET_SIZE);
    
    iAP_Transport_SetRecvCallback(s_transport_reception_callback);
    iAP_Transport_SetSentCallback(s_transport_transmission_callback);

    /* Set the first state to execute for SM driver */
    g_iapInterfaceReceptionCallbackActualState = kIapiRxCallbackInitialState;
}

/* See iAP_Interface.h for documentation of this function. */
iap_interface_send_frame_t iap_interface_send_frame(iap_interface_tx_frame_data_t* txFrameData, 
                                                    uint8_t* txIndex)
{
    iap_interface_data_transmit_packet_t txItem;
    uint8_t* outDataBuffer;
    uint8_t* startOfFrameToTx;
    uint32_t frameLength;
    uint16_t fieldLength;
    uint8_t  insertIndexRetryArray;
    uint8_t  calculateChecksum = 0;
    uint16_t transportStatus = 0;
    iap_interface_send_frame_t  sendFrameTxStatus = kMemoryNotAvailable;

    if (g_txPayloadBufferSize >= txFrameData->payloadSize)
    {
        /* Reserve Space to save outgoing message */
        outDataBuffer = buffersAlloc_reserveSpace(kIapiTransmissionBufferIndex,
                IAPI_TX_PACKET_SIZE);

        if(outDataBuffer != NULL)
        {
            /* Assign address of the start of the frame to be transmitted, 
             * this will be used to calculate the size of the whole packet */
            frameLength = (uint32_t)(outDataBuffer);

            /* Backup pointer to the start of the frame to be transmitted */
            startOfFrameToTx = outDataBuffer;

#if(IAPI_TX_SYNC_BYTE_ENABLE)
            *outDataBuffer++ = IAP_INTERFACE_SOF1_BYTE;
#endif
            *outDataBuffer++ = IAP_INTERFACE_SOF2_BYTE;

            /* Payload size (N-bytes) + lingo (1-byte) */
            fieldLength = txFrameData->payloadSize + kOneByteOffset;
            
            if(IAPI_EXTENDED_LINGO_ID != txFrameData->lingoId)
            {
                /* Command field size (1-byte in this case) */
                fieldLength += kOneByteOffset;
            }else
            {
                /* Command field size (2-bytes in this case) */
                fieldLength += kTwoBytesOffset;
            }
                        
            if(IAPI_CHECK_TRANSACTION_ID_ENABLED)
            {
                /* If TID is enabled add the size of the TID (2-bytes) */
                fieldLength += kTwoBytesOffset;
            }

            /* Write 1-byte or 2-bytes length field depending on the value of fieldLength */
            if(fieldLength < MAX_LENGTH_FOR_ONE_BYTE_LENGTH_FIELD)
            {
                /* Write the value of the length field (1-byte) */
                *outDataBuffer++ = fieldLength;
                calculateChecksum += (uint8_t)(fieldLength);
            }
            else
            {
                /* In the 2-bytes length field case, the first byte must be 0x00*/
                *outDataBuffer++ = 0x00;
                
                /* Write the value of the length field (2-bytes) */
                *outDataBuffer++ = (uint8_t)(fieldLength >> 8);
                calculateChecksum += (uint8_t)(fieldLength >> 8);
                *outDataBuffer++ = (uint8_t)(fieldLength & 0x00FF);
                calculateChecksum += (uint8_t)(fieldLength & 0x00FF);
            }

            /* Write the value of the lingo field (1-byte) */
            *outDataBuffer++ = txFrameData->lingoId;
            calculateChecksum += txFrameData->lingoId;

            /* Use 16 bit command ID if lingoID = extended interface */
            if(IAPI_EXTENDED_LINGO_ID == txFrameData->lingoId)
            {
                *outDataBuffer++ = 0x00;
            }

            /* Write the value of the command field */
            *outDataBuffer++ = txFrameData->commandId;
            calculateChecksum += txFrameData->commandId;

            if(IAPI_CHECK_TRANSACTION_ID_ENABLED)
            {
                /* Check for type of packet to increase TID in case of QUESTION 
                 * or paste TID from Device response in case of ANSWER*/ 
                if(kQuestionPacket & txFrameData->questionOrAnswerPacket)
                {
                    if(!(kDoNotIncrementTID & txFrameData->questionOrAnswerPacket))
                    {
                        g_iapInterfaceTransactionId++;
                    }
                    
                    *outDataBuffer++ = (uint8_t)(g_iapInterfaceTransactionId >> 8);
                    calculateChecksum += (uint8_t)(g_iapInterfaceTransactionId >> 8);
                    *outDataBuffer++ = (uint8_t)(g_iapInterfaceTransactionId & 0x00FF);
                    calculateChecksum += (uint8_t)(g_iapInterfaceTransactionId & 0x00FF);
                }
                else if(kAnswerPacket & txFrameData->questionOrAnswerPacket)
                {
                    *outDataBuffer++ = (uint8_t)(txFrameData->transactionIDCopied >> 8);
                    calculateChecksum += (uint8_t)(txFrameData->transactionIDCopied >> 8);
                    *outDataBuffer++ = (uint8_t)(txFrameData->transactionIDCopied & 0x00FF);
                    calculateChecksum += (uint8_t)(txFrameData->transactionIDCopied & 0x00FF);
                }
            }

            /* Copy the payload to be sent */
            while((txFrameData->payloadSize)--)
            {
                *outDataBuffer++ = *(txFrameData->payload);
                calculateChecksum += (uint8_t) *(txFrameData->payload);
                (txFrameData->payload)++;
            }

            /* Write the value of the calculated checksum */
            *outDataBuffer++ = (uint8_t)(-calculateChecksum); 

            /* Calculate the whole packet size, this value will be used to indicate to the 
             * transport layer how many bytes it must send */
            frameLength = (uint32_t)(outDataBuffer) - frameLength;

            /* Put packet into the Tx pointers queue */
            txItem.packetToTransmit = startOfFrameToTx;
            txItem.packetSize = frameLength;
            txItem.commandCurrentTxState = txFrameData->commandCurrentTxState;
            txItem.retryArrayIndexId = kNotAvailableIndex;  
            (*txIndex) = kNotAvailableIndex;

            /* If the transmitted command must wait for response, insert it into the retry
             * commands structures array used to check its timestamp and retry if necessary*/
            if((kCommandIsWaitingForResponse & txFrameData->commandCurrentTxState) 
                    || (kCommandRepeatsPeriodically & txFrameData->commandCurrentTxState))
            {
                insertIndexRetryArray = 
                        RandomAccessArray_get_next_avaliable_position(&g_iapInterfaceRetryCommandsArrayInfo);
                g_iapInterfaceRetryCommandsArray[insertIndexRetryArray].packetSize = frameLength;
                g_iapInterfaceRetryCommandsArray[insertIndexRetryArray].timeStamp  = s_iap_interface_get_time_stamp();          //TODO check timestamp init
                g_iapInterfaceRetryCommandsArray[insertIndexRetryArray].timeout     = txFrameData->commandTimeout;
                g_iapInterfaceRetryCommandsArray[insertIndexRetryArray].packetToTransmit = startOfFrameToTx;
                g_iapInterfaceRetryCommandsArray[insertIndexRetryArray].retryCounter = IAPI_MAX_RETRY_INTENTS;

                if(kCommandRepeatsPeriodically & txFrameData->commandCurrentTxState)
                {
                    g_iapInterfaceRetryCommandsArray[insertIndexRetryArray].retryCounter = IAPI_INDEFINITE_RETRY_INTENTS;
                }

                txItem.retryArrayIndexId = insertIndexRetryArray;
                (*txIndex) = insertIndexRetryArray;
            }
            
            if(!(IAP_INTERFACE_TX_IN_PROGRESS_FLAG))
            {
                transportStatus = IAP_INTERFACE_SEND_MESSAGE(txItem.packetToTransmit,
                                                             txItem.packetSize);
                if(transportStatus == 0x00)
                {
                    g_iapInterfaceCurrentTxPacket = txItem;
                }else
                {
                    /* The transport function used to send a message returned a status different
                     * from zero which means it cannot send the message at the moment, so we 
                     * will queue the packet and the s_iap_interface_check_transmission_complete 
                     * function will pop and transmit this element once the transport has 
                     * finished sending the latest packet */
                    msg_queue_put(g_iapInterfaceTxQueueHandler,(msg_queue_item_t) &txItem);
                }
                
            }else
            {
                /* If the Interface IAP_INTERFACE_TX_IN_PROGRESS_FLAG  is set, then it means
                 * the transport layer is busy, so we will queue the packet and the
                 * s_iap_interface_check_transmission_complete function will pop and transmit
                 * this element once the transport has finished sending the latest packet */
                msg_queue_put(g_iapInterfaceTxQueueHandler,(msg_queue_item_t) &txItem);
            }

            sendFrameTxStatus = kInterfaceTxSuccess;
        }
    }else
    {
        sendFrameTxStatus = kPayloadOverflowError;
    }

    return (sendFrameTxStatus);
}

/* See iAP_Interface.h for documentation of this function. */
iap_interface_buffer_rx_frame_t* iap_interface_get_rx_command(void)
{
    iap_interface_buffer_rx_frame_t* headReceivedPacket = NULL;
    
    if(!(g_iapInterfaceRxQueueHandler->isEmpty))
    {
        /* Get the head of the reception queue*/
        msg_queue_get(g_iapInterfaceRxQueueHandler, (msg_queue_item_t) &headReceivedPacket,
                      MSG_QUEUE_TIMEOUT);
    }
    
    return headReceivedPacket;
}

/* See iAP_Interface.h for documentation of this function. */
void iap_interface_periodical_task(void)
{
    s_iap_interface_check_transmission_complete();
    iap_interface_callback; 
    s_iap_interface_check_retry_commands_array();
}

/* See iAP_Interface.h for documentation of this function. */
uint8_t iap_interface_get_tid_position(uint8_t* dataPacket)
{
    uint8_t auxPacketIndex = 0;

    /* Skip one or two positions depending of the Sync byte _TRUE_d to point to Length field */
#if(_TRUE_ == IAPI_TX_SYNC_BYTE_ENABLE)
    auxPacketIndex += kTwoBytesOffset;
#else
    auxPacketIndex += kOneByteOffset;
#endif

    /* Skip the corresponding bytes of the length field */
    if(dataPacket[auxPacketIndex] == 0x00)
    {
        /* Three positions must be skipped in case a 16-bit length */
        auxPacketIndex += kThreeBytesOffset;
    }
    else
    {
        /* One position must be skipped in case a 8-bit length */
        auxPacketIndex += kOneByteOffset;
    }

    /* Skip the corresponding bytes of the Lingo and Command fields */
    if(dataPacket[auxPacketIndex] == IAPI_EXTENDED_LINGO_ID)
    {
        /* Three positions must be skipped in case of a two-bytes command */
        auxPacketIndex += kThreeBytesOffset;
    }
    else
    {
        /* Two positions must be skipped in case of a one-byte command  */
        auxPacketIndex += kTwoBytesOffset;
    }

    return auxPacketIndex;
}


/* See iAP_Interface.h for documentation of this function. */
uint8_t* iap_interface_get_tx_command_from_index(uint8_t index)
{
    uint8_t* returnTxCommandPointer;

    returnTxCommandPointer = NULL;
    if( index < (sizeof(g_iapInterfaceRetryCommandsArray)/sizeof(g_iapInterfaceRetryCommandsArray[0])) )
    {
        returnTxCommandPointer = g_iapInterfaceRetryCommandsArray[index].packetToTransmit;
    }

    return returnTxCommandPointer;
}

/* See iAP_Interface.h for documentation of this function. */
iap_interface_release_status_t iap_interface_release_tx_command(uint8_t interfaceRetryIndex)
{
    iap_interface_release_status_t releaseStatus;
    uint8_t* txPacketToFree;

    releaseStatus = kReleaseSuccess;

    /* Check if the given index is a valid index of the retry commands array*/
    if(interfaceRetryIndex < ARRAY_N_ELEMENTS(g_iapInterfaceRetryCommandsArray))
    {
        /* Get the pointer to the beginning of the transmitted packet in order to release it*/
        txPacketToFree = g_iapInterfaceRetryCommandsArray[interfaceRetryIndex].packetToTransmit;

        /* Free the position of the retry commands array
         * and signal an error in case the operation is not successful */
        if(RandomAccessArray_free_position(&g_iapInterfaceRetryCommandsArrayInfo, interfaceRetryIndex)
                == kFreeIndexOutOfRangeError)
        {
            releaseStatus |= (kReleaseRetryCommandsArrayError);
        }

        /* Try to release the transmitted packet from the transmission buffer 
         * and signal an error in case the operation is not successful  */
        if(buffersAlloc_releaseSpace(kIapiTransmissionBufferIndex, txPacketToFree) == kBufferEmpty)
        {
            releaseStatus |= (kReleaseTxError);
        }

    }else
    {
        releaseStatus = kReleaseIndexOutOfRange;
    }

    return releaseStatus;
}

/* Read header file for this function documentation  */
void iap_interface_request_max_payload_change (uint16_t requestedPayloadSize)
{
    if(IAPI_MAX_TX_PAYLOAD_SIZE > requestedPayloadSize)
    {
        g_txPayloadBufferSize = requestedPayloadSize; 
    }
    else
    {
        g_txPayloadBufferSize = IAPI_MAX_TX_PAYLOAD_SIZE; 
    }
}

/* See iAP_Interface.h for documentation of this function. */
void iap_interface_reset(void)
{
    msg_queue_flush((msg_queue_handler_t) &g_iapInterfaceTxQueue);
    msg_queue_flush((msg_queue_handler_t) &g_iapInterfaceRxQueue);
    g_txPayloadBufferSize = IAPI_MAX_TX_PAYLOAD_SIZE;
    
    RandomAccessArray_flush_array(&g_iapInterfaceRetryCommandsArrayInfo);
    
    /* Clear all occupied messages flags */
    buffersAlloc_resetAllBuffersStatus(); 
    
    /* Reserve space for the first packet */
    g_iapiCurrentReceivedFrame = (iap_interface_buffer_rx_frame_t*) buffersAlloc_reserveSpace
                                            (kIapiReceptionBufferIndex, IAPI_RX_PACKET_SIZE);
}
    
/* See iAP_Interface.h for documentation of this function. */
void iap_interface_reset_transaction_id(void)
{
    g_iapInterfaceTransactionId = 0;
}

/* See iAP_Interface.h for documentation of this function. */
void iap_interface_payload_update(uint8_t* newPayload, uint8_t newPayloadSize, uint8_t retryCommandArrayIndex)
{
    uint8_t payloadIndex;
    uint8_t newCheckSum; 
    uint8_t tidOffset = 0;
    
    if(IAPI_CHECK_TRANSACTION_ID_ENABLED)
    {
        tidOffset = kTwoBytesOffset;
    }
    
    payloadIndex = iap_interface_get_tid_position(g_iapInterfaceRetryCommandsArray[retryCommandArrayIndex].packetToTransmit) 
                    + tidOffset;
    
    while(newPayloadSize--)
    {
        g_iapInterfaceRetryCommandsArray[retryCommandArrayIndex].packetToTransmit[payloadIndex + newPayloadSize]
                                                                      = newPayload[newPayloadSize];
    }
    
    newCheckSum = s_iap_interface_calculate_checksum(g_iapInterfaceRetryCommandsArray[retryCommandArrayIndex].packetToTransmit,
                                                     g_iapInterfaceRetryCommandsArray[retryCommandArrayIndex].packetSize);
    
    g_iapInterfaceRetryCommandsArray[retryCommandArrayIndex].packetToTransmit[g_iapInterfaceRetryCommandsArray[retryCommandArrayIndex].packetSize - 1] = newCheckSum;
    
}

/* See iAP_Interface.h file for function documentation */
void iap_interface_update_timeout(uint8_t indexOfRetryArray, uint32_t newTimeout)
{
    if(newTimeout != 0x00)
    {
        g_iapInterfaceRetryCommandsArray[indexOfRetryArray].timeout = newTimeout;
    }
    g_iapInterfaceRetryCommandsArray[indexOfRetryArray].timeStamp = s_iap_interface_get_time_stamp();
}

/* See iAP_Interface.h file for function documentation */
uint8_t iap_interface_register_no_response_error_callback(void(*callback)(uint8_t))
{
    uint8_t returnValue = _ERROR_; 
    if (NULL != callback)
    {
        s_iap_interface_no_response_error_callback = callback;
        returnValue = _OK_;
    }
    return (returnValue); 
}

/*************************************************************************************************
 *
 * @brief   Callback for transport Rx data. Only during initialization this callback will be active.
 * @param   void
 * @return  void
 *
 **************************************************************************************************/
static void s_transport_reception_callback(uint8_t *dataPointer, uint16_t dataSize)
{
    uint16_t count;
    uint8_t rxByte;
    count = 0;    
    
    iAPI_RxCallbackFlag = true;

    do
    {
        rxByte = dataPointer[count];
        count++;
        /* Validates the Next state to execute and jumps to the corresponding 
         * state with the received byte from the callback */
        if (g_iapInterfaceReceptionCallbackActualState < kIapiIdleState)
        {
            s_kReceptionCallbackStates[g_iapInterfaceReceptionCallbackActualState](rxByte);
        }
        else
        {
            /* Go to initial state */    
            g_iapInterfaceReceptionCallbackActualState = kIapiRxCallbackInitialState;
        }
    }while(count < dataSize);
}

/*************************************************************************************************
 *
 * @brief   SOF1 State, This state handles the reception of SOF1 and validates if it is 0xFF
 * @param   rxByte This variables contains the value of the received byte
 * @return  void
 *
 **************************************************************************************************/
static void s_iap_interface_sof1_rx_state(uint8_t rxByte)
{
    /* Compares the received byte with the SOF1 */
    if (IAP_INTERFACE_SOF1_BYTE == rxByte)
    {    
        /* Set next state */
        g_iapInterfaceReceptionCallbackActualState = kIapiSof2State;
    }
}

/*************************************************************************************************
 *
 * @brief   SOF2 State  This state handles the reception of SOF2 and validates if it is 0x55
 * @param   rxByte      This variables contains the value of the received byte
 * @return  void
 *
 **************************************************************************************************/
static void s_iap_interface_sof2_rx_state(uint8_t rxByte)
{
    if (IAP_INTERFACE_SOF2_BYTE == rxByte)
    {      
        if(g_iapiCurrentReceivedFrame != NULL)
        {
            g_iapiCurrentReceivedFrame->payloadSize = 0;
            g_iapiCurrentReceivedFrame->transactionId = 0;
            
            /* Set next state */
            g_iapInterfaceReceptionCallbackActualState = kIapiLengthState;
            
            s_receivedBytesCount = 0;
            s_calculateChecksum = 0;
        }else
        {   /* If g_iapiCurrentReceivedFrame == NULL, this means that after the last checksum state
             * the stack was not able to reserve space for the next packet, so it must
             * try again here to see if there is available space */
            g_iapiCurrentReceivedFrame = (iap_interface_buffer_rx_frame_t*) buffersAlloc_reserveSpace
                    (kIapiReceptionBufferIndex, IAPI_RX_PACKET_SIZE);
            
            /* If now there is available space then continue with normal operation */
            if(g_iapiCurrentReceivedFrame != NULL)
            {
                g_iapiCurrentReceivedFrame->payloadSize = 0;
                g_iapiCurrentReceivedFrame->transactionId = 0;
                
                /* Set next state */
                g_iapInterfaceReceptionCallbackActualState = kIapiLengthState;
                
                s_receivedBytesCount = 0;
                s_calculateChecksum = 0;
            /* If there is still not available space then go back to the initial state */
            }else
            {
                g_iapInterfaceReceptionCallbackActualState = kIapiRxCallbackInitialState;
            }
        }
    }
    else
    {
        /* The SOF2 was no received so go back to the initial state */
        g_iapInterfaceReceptionCallbackActualState = kIapiRxCallbackInitialState;
    }
}

/*************************************************************************************************
 *
 * @brief   Length State   This state handles the reception of the Length and stores it in the
 *                         corresponding buffer
 * @param   rxByte         This variables contains the value of the received byte
 * @return  void
 *
 **************************************************************************************************/
static void s_iap_interface_length_rx_state(uint8_t rxByte)
{
    static uint8_t s_lengthCount = 0;
    /*
     * At this point the SOF and Length have been received. Data in the 
     * buffer should be stored in this format:
     * [0:1] = Length
     * [2:3] = LingoID
     * [4:5] = Command ID for extended interface mode, [1] = Command ID for other lingoes
     * [6:7] = Transaction ID for extended interface mode, [2:3] = Transaction ID for other lingoes
     * [8:9] = PayLoadSize
     * [5:Length-1] = DataPayload for extended interface mode,
     * [4:Length-1] = DataPayload for other lingoes
     * 
     */    

    /* Add the received byte to keep track of the calculated checksum */
    s_calculateChecksum += rxByte;
    /* Validate the bLength Auxiliar variable to find out the length frame */
    if(0x00 == s_lengthCount)
    {
        /* If the received byte is different of 0, it means the frame is 8-bit size */
        if (rxByte != 0x00)
        {
            g_iapiCurrentReceivedFrame->frameLength = rxByte;
            
            /* Valid range for 8 bit length must be 2 to 252 */
            if((g_iapiCurrentReceivedFrame->frameLength >= MIN_LENGTH_FOR_ONE_BYTE_LENGTH_FIELD) 
                    && (g_iapiCurrentReceivedFrame->frameLength < MAX_LENGTH_FOR_ONE_BYTE_LENGTH_FIELD))
            {
                g_iapInterfaceReceptionCallbackActualState = kIapiLingoIdState; 
            }
            else
            {
                /* Signal length error */
                g_iapInterfaceRxStatus = (1 << kIapiLengthError);

                /* Return to init of SM */
                g_iapInterfaceReceptionCallbackActualState = kIapiRxCallbackInitialState;                                
            }
        }
        /* Otherwise the frame must be 16-bit size */
        else
        {
            s_lengthCount++;
        }
    }
    else
    {
        /* Stores the size of the 16-bit frame length */
        g_iapiCurrentReceivedFrame->frameLength <<= 8; 
        g_iapiCurrentReceivedFrame->frameLength |= rxByte;
        s_lengthCount++;

        /* Three bytes are received when frame is a 16-bit frame */
        if(3 == s_lengthCount)
        {
            s_lengthCount = 0;        

            /* Set next state to execute and the corresponding flag for the 16-bit flag */
            if((g_iapiCurrentReceivedFrame->frameLength >= MAX_LENGTH_FOR_ONE_BYTE_LENGTH_FIELD)
                    && (g_iapiCurrentReceivedFrame->frameLength < IAPI_RX_PACKET_SIZE))
            {
                g_iapInterfaceReceptionCallbackActualState = kIapiLingoIdState;
            }
            else
            {
                /* Signal length error */
                g_iapInterfaceRxStatus = (1 << kIapiLengthError);

                /* Return to init of SM */
                g_iapInterfaceReceptionCallbackActualState = kIapiRxCallbackInitialState;
            }
        }
    }
}

/*************************************************************************************************
 *
 * @brief   Lingo ID state   This state is in charge of the receiving and storing the Lingo ID
 * @param   rxByte           This variables contains the value of the received byte
 * @return  void
 *
 **************************************************************************************************/
static void s_iap_interface_lingo_id_rx_state(uint8_t rxByte)
{
    /* Stores the Lingo ID */
    g_iapiCurrentReceivedFrame->lingoId = rxByte;    
    /* Keeps calculating the Checksum */
    s_calculateChecksum += rxByte;
    /* Count and increment the number of bytes received */
    s_receivedBytesCount++;    
    /* Set next state to execute */
    g_iapInterfaceReceptionCallbackActualState = kIapiCommandIdState;
}

/*************************************************************************************************
 *
 * @brief   Command ID state is in charge of storing the Command ID and decides whether to jump
 *          to the Checksum, payload or Transaction ID state.
 * @param   rxByte This variables contains the value of the received byte
 * @return  void
 *
 **************************************************************************************************/
static void s_iap_interface_command_id_rx_state(uint8_t rxByte)
{
    static uint8_t s_lengthCount = 0;
    /* variable to know if the command id has been received */ 
    bool commandIdReceived = false; 

    /* Validate whether the received command is from extended lingo */
    if(IAPI_EXTENDED_LINGO_ID != g_iapiCurrentReceivedFrame->lingoId)
    {
        g_iapiCurrentReceivedFrame->commandId = rxByte;
        commandIdReceived = true;
    }
    /* Extended Interface Lingo, so the command is a two-byte commandId */
    else
    {
        g_iapiCurrentReceivedFrame->commandId <<= 8; 
        g_iapiCurrentReceivedFrame->commandId |= rxByte;
        s_lengthCount++;
        /* Set the Two command bytes flag */
        if(2 == s_lengthCount)
        {
            s_lengthCount = 0;
            commandIdReceived = true;
        }
    }
    /* Keeps calculating the Checksum */
    s_calculateChecksum += rxByte;
    /* Increment the number of bytes received */
    s_receivedBytesCount++;

    /* If the command id has been received, then set the next corresponding state */
    if(commandIdReceived)
    {
        if( (!IAPI_CHECK_TRANSACTION_ID_ENABLED) ||
                (g_iapiCurrentReceivedFrame->frameLength < IAPI_MIN_PKT_SIZE_WITH_TRANSID) )
        {
            if(s_receivedBytesCount == g_iapiCurrentReceivedFrame->frameLength)
            {
                g_iapInterfaceReceptionCallbackActualState = kIapiChecksumState;    
            }
            else
            {
                g_iapInterfaceReceptionCallbackActualState = kIapiPayloadState;
            }
        }
        else
        {
            g_iapInterfaceReceptionCallbackActualState = kIapiTransactionIdState;
        }
    }
}

/*************************************************************************************************
 *
 * @brief   TID state stores the TID received and selects the next state according the frame
 *          length.
 * @param   rxByte This variables contains the value of the received byte
 * @return  void
 *
 **************************************************************************************************/
static void s_iap_interface_tid_rx_state(uint8_t rxByte)
{
    static uint8_t s_lengthCount = 0;

    g_iapiCurrentReceivedFrame->transactionId <<= 8; 
    g_iapiCurrentReceivedFrame->transactionId |= rxByte;
    s_calculateChecksum += rxByte;
    s_receivedBytesCount++;    

    s_lengthCount++;
    if(2 == s_lengthCount)
    {
        s_lengthCount = 0;

        if(s_receivedBytesCount == g_iapiCurrentReceivedFrame->frameLength)
        {
            g_iapInterfaceReceptionCallbackActualState = kIapiChecksumState;
        }
        else
        {
            g_iapInterfaceReceptionCallbackActualState = kIapiPayloadState;
        }
    }
}

/*************************************************************************************************
 *
 * @brief   Payload state is in charge of storing the entire payload in the corresponding buffer
 * @param   rxByte This variables contains the value of the received byte
 * @return  void
 *
 **************************************************************************************************/
static void s_iap_interface_payload_rx_state(uint8_t rxByte)
{
    
    if(g_iapiCurrentReceivedFrame->payloadSize < s_kRxpayloadBufferSize)
    {
        /* Store the byte received to where the payload pointer is pointing */
        g_iapiCurrentReceivedFrame->payload[g_iapiCurrentReceivedFrame->payloadSize] = rxByte;
        g_iapiCurrentReceivedFrame->payloadSize++;
        
        /* Keeps calculating the Checksum */
        s_calculateChecksum += rxByte;
        /* Increment the number of bytes received */
        s_receivedBytesCount++;
        /* Jumps to the Checksum state */
        if (s_receivedBytesCount == g_iapiCurrentReceivedFrame->frameLength)
        {
            g_iapInterfaceReceptionCallbackActualState = kIapiChecksumState;
        }
        
    }else
    {    
        /* Set the Overflow flag in case the payload size exceeds the max size of the buffer */
        g_iapInterfaceRxStatus = (1 << kIapiPayloadOverflowError);

        /* Return to init of SM */
        g_iapInterfaceReceptionCallbackActualState = kIapiRxCallbackInitialState;
    }
}

/**************************************************************************************************
 *
 * @brief   Checksum state is in charge of validating the received checksum and comparing it 
 *          with the calculated one
 * @param   rxByte This variables contains the value of the received byte
 * @return  void
 *
 *************************************************************************************************/
static void s_iap_interface_checksum_rx_state(uint8_t rxByte)
{
    uint8_t currentChecksum;
    currentChecksum = rxByte;
    s_calculateChecksum = -s_calculateChecksum;

    g_iapInterfaceReceptionCallbackActualState = kIapiRxCallbackInitialState;
    
    if(currentChecksum == s_calculateChecksum)
    {
        /* Queue received Command in the Rx Pointers Queue */
        msg_queue_put(g_iapInterfaceRxQueueHandler, (msg_queue_item_t) &g_iapiCurrentReceivedFrame);
        
        g_iapInterfaceRxStatus = (1 << kIapiFullFrame);
        
        /* Reserve space for the next packet */
        g_iapiCurrentReceivedFrame = (iap_interface_buffer_rx_frame_t*) buffersAlloc_reserveSpace
                (kIapiReceptionBufferIndex, IAPI_RX_PACKET_SIZE);
        
    }else
    {
        /* Signal Checksum error*/
        g_iapInterfaceRxStatus = (1 << kIapiChecksumError);
    }


}

/**************************************************************************************************
 *
 * @brief   Transmission callback, checks if there is any transmission completed an sets flags 
 *          that will be checked by a periodical task
 * @return  void
 *
 *************************************************************************************************/
static void s_transport_transmission_callback(void)
{
    if(!IAPI_CHECK_TX_SYNC_BYTE_FLAG)
    {
        IAPI_SET_TX_COMPLETE_FLAG;
    }    
}

/**************************************************************************************************
 *
 * @brief   Function used to get a timestamp. Gets the current value of the free running timer 
 * @return  The current value of the free counter from the sw_timer module
 *
 *************************************************************************************************/
static inline uint32_t s_iap_interface_get_time_stamp(void)
{
    /* Return the global counter timer of sw_timer service layer */
    return sw_timer_get_free_counter();
}

/**************************************************************************************************
 *
 * @brief   Function used to check if the retry time of a command has expired 
 * @return  Whether the time has expired
 * @retval  kCommandNoTimeout The time has not expired
 * @retval  kCommandTimeout The time has expired
 *
 *************************************************************************************************/
static inline iap_interface_command_timeout_t s_iap_interface_has_retry_time_expired(uint32_t commandTimeStamp,
                                                                                     uint32_t commandTimeout)
{
    iap_interface_command_timeout_t status = kCommandNoTimeout;
    /* Check if there has been a timeout */
    if((uint32_t)(sw_timer_get_free_counter() - commandTimeStamp) > commandTimeout)
    {
        /* Retry command */
        status = kCommandTimeout;
    }
    return status;
}

/**************************************************************************************************
 *
 * @brief   Function that is being called constantly to check if a transmission event has occurred
 *          in order to sent the next element in the transmission queue if it is necessary.
 *          In other words verifies if there are further commands to send after finishing the last 
 *          and, if so, sends the next in the TX queue 
 * @return  void
 *
 *************************************************************************************************/
static void s_iap_interface_check_transmission_complete(void)
{
    if(IAPI_CHECK_TX_COMPLETE_FLAG)
    {
        /* If the transmitted command is waiting for response write its timestamp 
         * into the retry commands structures array  */
        if ((kCommandIsWaitingForResponse & g_iapInterfaceCurrentTxPacket.commandCurrentTxState) 
                || (kCommandRepeatsPeriodically & g_iapInterfaceCurrentTxPacket.commandCurrentTxState))
        {

            g_iapInterfaceRetryCommandsArray[g_iapInterfaceCurrentTxPacket.retryArrayIndexId].timeStamp = 
                    s_iap_interface_get_time_stamp();
            /* If the transmitted command is a retried command decrement its retry counter  */
            if(kCommandRetry & g_iapInterfaceCurrentTxPacket.commandCurrentTxState) 
            {
                g_iapInterfaceRetryCommandsArray[g_iapInterfaceCurrentTxPacket.retryArrayIndexId].retryCounter--;
            }
        }
        /* If the transmitted command is not waiting for response release 
         * its entire packet from Tx Buffer */
        else if(kCommandIsNotWaitingForResponse & g_iapInterfaceCurrentTxPacket.commandCurrentTxState)
        {
            if(!(kCommandRepeatsPeriodically & g_iapInterfaceCurrentTxPacket.commandCurrentTxState))
            {
                buffersAlloc_releaseSpace(kIapiTransmissionBufferIndex,
                                          g_iapInterfaceCurrentTxPacket.packetToTransmit);  
            }
        }
        
        IAPI_CLEAR_TX_COMPLETE_FLAG;
        
        /* If there are more queued commands in the Tx pointers queue 
         * then send the next one in the queue */
        if (!g_iapInterfaceTxQueueHandler->isEmpty)
        {
            iap_interface_data_transmit_packet_t nextPacketToSend;  
            
            /* Pop/get last transmitted command from Tx Pointers Queue*/
            msg_queue_get(g_iapInterfaceTxQueueHandler,
                    (msg_queue_item_t) &nextPacketToSend, MSG_QUEUE_TIMEOUT); 

            IAP_INTERFACE_SEND_MESSAGE(nextPacketToSend.packetToTransmit,
                                       nextPacketToSend.packetSize);
            
            g_iapInterfaceCurrentTxPacket = nextPacketToSend;  
        }

    }
}

/**************************************************************************************************
 *
 * @brief   Function that is being called constantly to check the retry commands array in order to 
 *          resend a command if it is necessary.
 *          
 * @return  void
 *
 *************************************************************************************************/
static void s_iap_interface_check_retry_commands_array(void)
{
    uint8_t searchForRetryIndex;
    uint8_t newChecksum;
    uint8_t* oldChecksum;
    uint8_t tidPosition;
    iap_interface_command_timeout_t timeoutStatus;
    
    for(searchForRetryIndex = 0; searchForRetryIndex < NUM_OF_TX_PACKETS; searchForRetryIndex++)
    {
        /* Check if the position in the array has a valid element */
        if (IS_POSITION_RESERVED(g_iapInterfaceRetryCommandsArrayInfo, searchForRetryIndex) &&
            g_iapInterfaceRetryCommandsArray[searchForRetryIndex].timeout != kNoTimeout)
        {
            /* Check if the command timeout has expired */
            timeoutStatus = s_iap_interface_has_retry_time_expired(
                                    g_iapInterfaceRetryCommandsArray[searchForRetryIndex].timeStamp,
                                    g_iapInterfaceRetryCommandsArray[searchForRetryIndex].timeout);
            if(timeoutStatus == kCommandTimeout) 
            {
                /* If the command has not been retried 'IAPI_MAX_RETRY_INTENTS' 
                 * times then SEND it again */
                if( g_iapInterfaceRetryCommandsArray[searchForRetryIndex].retryCounter != 0 )
                {
                    iap_interface_data_transmit_packet_t retryTxItem;
                    uint16_t transportStatus = 0;
                    
                    /* Write the element to be pushed into the Tx pointers queue */
                    retryTxItem.packetToTransmit = 
                                    g_iapInterfaceRetryCommandsArray[searchForRetryIndex].packetToTransmit;
                    retryTxItem.packetSize =
                                    g_iapInterfaceRetryCommandsArray[searchForRetryIndex].packetSize;
                    retryTxItem.retryArrayIndexId = searchForRetryIndex;
                    
                    if((g_iapInterfaceRetryCommandsArray[searchForRetryIndex].retryCounter) != 
                            IAPI_INDEFINITE_RETRY_INTENTS)
                    {
                        retryTxItem.commandCurrentTxState = (kCommandIsWaitingForResponse | kCommandRetry);
                    }else
                    {
                        retryTxItem.commandCurrentTxState = 
                                        (kCommandIsWaitingForResponse | kCommandRepeatsPeriodically);
                    }
                    

                    if(IAPI_CHECK_TRANSACTION_ID_ENABLED)
                    {
                        /* Get TID position in order to rewrite its value in the Tx packet*/
                        tidPosition = iap_interface_get_tid_position(retryTxItem.
                                                                     packetToTransmit);

                        /* Increment TID because of retried command */
                        g_iapInterfaceTransactionId++;

                        /* Rewrite the new TID in the packet*/
                        *(retryTxItem.packetToTransmit + tidPosition) = 
                                            (uint8_t) ((g_iapInterfaceTransactionId & 0xFF00) >> 8);
                        *(retryTxItem.packetToTransmit + tidPosition + 1) =
                                            (uint8_t) ((g_iapInterfaceTransactionId & 0x00FF));

                        /* Recalculate checksum and rewrite it in the packet */
                        oldChecksum = (retryTxItem.packetToTransmit + (retryTxItem.packetSize-1));
                        
                        
                        
                        newChecksum = s_iap_interface_calculate_checksum(retryTxItem.packetToTransmit,
                                                                         retryTxItem.packetSize);
                        
                        *oldChecksum = newChecksum;
                    }
         
                    if(!(IAP_INTERFACE_TX_IN_PROGRESS_FLAG))
                    {
                        transportStatus = IAP_INTERFACE_SEND_MESSAGE(retryTxItem.packetToTransmit,
                                                                     retryTxItem.packetSize);
                        if(transportStatus == 0x00)
                        {
                            g_iapInterfaceCurrentTxPacket = retryTxItem;
                        }else
                        {
                            /* The transport function used to send a message returned a status different
                             * from zero which means it cannot send the message at the moment, so we 
                             * will queue the packet and the s_iap_interface_check_transmission_complete 
                             * function will pop and transmit this element once the transport has 
                             * finished sending the latest packet */
                            msg_queue_put(g_iapInterfaceTxQueueHandler,
                                          (msg_queue_item_t) &retryTxItem);
                        }
                        
                    }else
                    {
                        /* If the Interface IAP_INTERFACE_TX_IN_PROGRESS_FLAG  is set, then it means
                         * the transport layer is busy, so we will queue the packet and the
                         * s_iap_interface_check_transmission_complete function will pop and transmit
                         * this element once the transport has finished sending the latest packet */
                        msg_queue_put(g_iapInterfaceTxQueueHandler,
                                (msg_queue_item_t) &retryTxItem);
                    }
                    
                    /*TODO Check reinit of timestamp value here needed in case the 
                     * function is called before the command is sent */
                     g_iapInterfaceRetryCommandsArray[searchForRetryIndex].timeStamp = 
                                                                     s_iap_interface_get_time_stamp();

                }else 
                {
                    /* Free Structures array position */
                    RandomAccessArray_free_position(&g_iapInterfaceRetryCommandsArrayInfo,
                                                    searchForRetryIndex);

                    /* Release the data of the packet stored in the Tx Buffer */
                    buffersAlloc_releaseSpace(kIapiTransmissionBufferIndex,
                                              g_iapInterfaceRetryCommandsArray[searchForRetryIndex].packetToTransmit); 

                    /* Let commands layer know one of the commands waiting for response has been
                     * retried 'IAPI_MAX_RETRY_INTENTS' times */ 
                    s_iap_interface_no_response_error_callback(searchForRetryIndex);
                }
            }
        }
    }
}

/**************************************************************************************************
 *
 * @brief   Function that can be used to calculate the checksum of a packet
 * @param   dataPacket  Pointer to the start of the packet 
 * @param   packetSize  The size of the packet
 * @return  The calculate checksum
 *
 *************************************************************************************************/
static uint8_t s_iap_interface_calculate_checksum(uint8_t* dataPacket, uint16_t packetSize)
{
    uint8_t calculatedChecksum = 0;
    uint16_t packetIndex;
    
#if(IAPI_TX_SYNC_BYTE_ENABLE)
    packetSize -= kThreeBytesOffset; /* Sync byte, SOF byte, CheckSum byte */
    packetIndex = kTwoBytesOffset;
#else
    packetSize -= kTwoBytesOffset; /* SOF byte, CheckSum byte */
    packetIndex = kOneByteOffset;
#endif 
    
    while(packetSize--)
    {
        calculatedChecksum += dataPacket[packetIndex];
        packetIndex++;
    }
    
    return  ((uint8_t)(-calculatedChecksum));
}
