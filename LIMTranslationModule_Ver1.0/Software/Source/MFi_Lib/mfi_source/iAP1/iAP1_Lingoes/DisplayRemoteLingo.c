/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

/*************************************************************************************************
*                                      Includes Section                        
*************************************************************************************************/
#include "DisplayRemoteLingo.h"

/*************************************************************************************************
*                                   Defines & Macros Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                       Typedef Section                        
*************************************************************************************************/
#pragma pack (push,1)

#pragma pack (pop)

/*************************************************************************************************
*                                  Function Prototypes Section                 
*************************************************************************************************/

/* Display Remote Lingo asynchronous commands ****************************************************/

/*! Display Remote functions prototypes*/
static void s_drl_async_event_notification(iap_interface_buffer_rx_frame_t* rxFrame);

/* End of Display Remote Lingo asynchronous commands *********************************************/   

/*************************************************************************************************
*                                   Global Constants Section                   
*************************************************************************************************/

/*! Display Remote Asynchronous Commands*/
const uint8_t kDisplayRemoteAsynchronousCommands
        [NUMBER_OF_DISPLAYREMOTE_ASYNCHRONOUS_COMMANDS] =
{
        kDisplayRemoteRemoteEventNotification               //0x09
};

/*************************************************************************************************
*                                   Static Constants Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                   Global Variables Section                   
*************************************************************************************************/

/*! Array of Display Remote Lingo functions */
 void (* kDisplayRemoteAsynchronousFunctions
        [NUMBER_OF_DISPLAYREMOTE_ASYNCHRONOUS_COMMANDS])(iap_interface_buffer_rx_frame_t* rxFrame) =
{
        s_drl_async_event_notification
};
 
/*************************************************************************************************
*                                   Static Variables Section                   
*************************************************************************************************/
static iap_display_remote_lingo_event_notification_callback_t s_iap_display_remote_event_notification;

/*************************************************************************************************
*                                      Functions Section                       
*************************************************************************************************/
uint8_t set_drl_remote_event_notification_callback(iap_display_remote_lingo_event_notification_callback_t callback)
{
     uint8_t returnValue = _ERROR_; 
     
     if (NULL != callback)
     {
         s_iap_display_remote_event_notification = callback;
     }
     
     return returnValue;
 } 
 
static void s_drl_async_event_notification(iap_interface_buffer_rx_frame_t* rxFrame)
{
    if (NULL != s_iap_display_remote_event_notification)
    {
        s_iap_display_remote_event_notification((iap_remote_event_notification_number_t) rxFrame->payload[0], 
                                                &rxFrame->payload[0],
                                                rxFrame->payloadSize);
    }
    
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*) rxFrame);
}
 
/*************************************************************************************************
* EOF
*************************************************************************************************/
