/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

#ifndef DISPLAYREMOTELINGO_H_
#define DISPLAYREMOTELINGO_H_


/*************************************************************************************************
*                                        Includes Section                              
*************************************************************************************************/
#include "iAP_Commands.h"

/*! @addtogroup iAP1_Lingoes iAP1 Lingoes
*   @{ 
*   
*  @addtogroup DisplayRemoteLingo Display Remote Lingo
*  @brief  This file contains the specific types (enums and structures) and macros, the asynchronous 
*          commands functions, the inline functions used to transmit commands from the Display 
*          Remote lingo and the inline functions used to access to the received data from specific
*          commands.
*          
*  @details The specific Display Remote lingo asynchronous commands functions have a one to one 
*           relation with the asynchronous commands from this lingo so these functions are called 
*           once their corresponding asynchronous command is received. The inline functions can be 
*           used as a mean to easily transmit an specific command from this lingo.
*  @{  
*/

/*************************************************************************************************
*                                        Defines & Macros Section                             
*************************************************************************************************/
/*!
*  @addtogroup DisplayRemoteLingoDefs Display Remote Lingo Macros and Defines
*  @{  
*/

/*! Amount of Asynchronous Commands from Display Remote Lingo*/
#define NUMBER_OF_DISPLAYREMOTE_ASYNCHRONOUS_COMMANDS       0x01

#define IAP_DISPLAYREMOTELINGO_COMMANDS_COUNT               0x23

/*! @} 
* Group DisplayRemoteLingoDefs
*/

/*************************************************************************************************
*                                       Typedef Section                             
*************************************************************************************************/
#pragma pack (push,1)

/** Display Remote Lingo Enumeration */
typedef enum
{
    kDisplayRemoteIpodAck,                              /*0x00*/
    kDisplayRemoteGetCurrentEQProfileIndex,             /*0x01*/
    kDisplayRemoteRetCurrentEQProfileIndex,             /*0x02*/
    kDisplayRemoteSetCurrentEQProfileIndex,             /*0x03*/
    kDisplayRemoteGetNumEQProfiles,                     /*0x04*/
    kDisplayRemoteRetNumEQProfiles,                     /*0x05*/
    kDisplayRemoteGetIndexedEQProfileName,              /*0x06*/
    kDisplayRemoteRetIndexedEQProfileName,              /*0x07*/
    kDisplayRemoteSetRemoteEventNotification,           /*0x08*/
    kDisplayRemoteRemoteEventNotification,              /*0x09*/
    kDisplayRemoteGetRemoteEventStatus,                 /*0x0A*/
    kDisplayRemoteRetRemoteEventStatus,                 /*0x0B*/
    kDisplayRemoteGetIpodStateInfo,                     /*0x0C*/
    kDisplayRemoteRetIpodStateInfo,                     /*0x0D*/
    kDisplayRemoteSetIpodStateInfo,                     /*0x0E*/
    kDisplayRemoteGetPlayStatus,                        /*0x0F*/
    kDisplayRemoteRetPlayStatus,                        /*0x10*/
    kDisplayRemoteSetCurrentPlayingTrack,               /*0x11*/
    kDisplayRemoteGetIndexedPlayingTrackInfo,           /*0x12*/
    kDisplayRemoteRetIndexedPlayingTrackInfo,           /*0x13*/
    kDisplayRemoteGetNumPlayingTracks,                  /*0x14*/
    kDisplayRemoteRetNumPlayingTracks,                  /*0x15*/
    kDisplayRemoteGetArtworkFormats,                    /*0x16*/
    kDisplayRemoteRetArtworkFormats,                    /*0x17*/
    kDisplayRemoteGetTrackArtworkData,                  /*0x18*/
    kDisplayRemoteRetTrackArtworkData,                  /*0x19*/
    kDisplayRemoteGetPowerBatteryState,                 /*0x1A*/
    kDisplayRemoteRetPowerBatteryState,                 /*0x1B*/
    kDisplayRemoteGetSoundCheckState,                   /*0x1C*/
    kDisplayRemoteRetSoundCheckState,                   /*0x1D*/
    kDisplayRemoteSetSoundCheckState,                   /*0x1E*/
    kDisplayRemoteGetTrackArtworkTimes,                 /*0x1F*/
    kDisplayRemoteRetTrackArtworkTimes,                 /*0x20*/
    kDisplayRemoteCreateGeniusPlaylist,                 /*0x21*/
    kDisplayRemoteIsGeniusAvailableForTrack             /*0x22*/      
}iap_display_remote_lingo_commands_t;

/** Ret Current EQ Profile Index                                                            0x02*/
typedef uint32_t iap_drl_current_equalizer_index_t;

/** Restore On Exit Values (Lingo = 0x03 for commands = 0x03, ) Enumeration. */
typedef enum
{
    kDisplayRemoteRestoreOnExitOff,
    kDisplayRemoteRestoreOnExitOn
}iap_display_remote_restore_on_exit_t;

/** Set Current Equalizer Profile Index (Lingo = 0x03, Cmd = 0x03) Structure */
typedef struct
{
    uint32_t currenEQIndex;
    uint8_t restoreOnExit;
}iap_set_current_eq_profile_index_t;

/** Ret Number of EQ Profiles                                                               0x05*/
typedef uint32_t iap_drl_number_of_eq_profiles_t;

/** iPod State Information - Event Number (Lingo = 0x03, Cmd = 0x0C ) Enumeration */
typedef enum
{
    kIpodStateTrackTimePositionMsec,
    kIpodStateTrackPlaybackIndex,
    kIpodStateChapterIndex,
    kIpodStatePlayStatus,
    kIpodStateMuteUIVolume,
    kIpodStatePowerBattery,
    kIpodStateEqualizerSettings,
    kIpodStateShuffleSettings,
    kIpodStateRepeatSettings,
    kIpodStateDateTimeSetting,
    kIpodStateAlarm,
    kIpodStateBackLightLevel,
    kIpodStateHoldSwitchState,
    kIpodStateSoundCheckState,
    kIpodStateAudioBookSpeed,
    kIpodStateTrackTimePositionSec,
    kIpodStateMuteUIAbsoluteVolume
}iap_ipod_state_info_t;

/** Remote Event Notification - Event Number (Lingo = 0x03, Cmd = 0x08, Cmd = 0x09, Cmd = 0x0B, Cmd = 0x0C and Cmd = 0x0E) Enumeration */
typedef enum
{
    kRemoteEventTrackTimePositionMsec,
    kRemoteEventTrackPlaybackIndex,
    kRemoteEventChapterIndex,
    kRemoteEventPlayStatus,
    kRemoteEventMuteUIVolume,
    kRemoteEventPowerBattery,
    kRemoteEventEqualizerSettings,
    kRemoteEventShuffleSettings,
    kRemoteEventRepeatSettings,
    kRemoteEventDateTimeSetting,
    kRemoteEventBackLightLevel = 11,
    kRemoteEventHoldSwitchState,
    kRemoteEventSoundCheckState,
    kRemoteEventAudioBookSpeed,
    kRemoteEventTrackTimePositionSec,
    kRemoteEventMuteUIAbsoluteVolume,
    kRemoteEventTrackCapabilities,
    kRemoteEventPlaybackEngineContents
}iap_remote_event_notification_number_t;

/** Remote Event Notification - Track Position in Milliseconds (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x00 */
    uint32_t trackPositionMs;
}iap_remote_event_notification_track_position_ms_t;

/** Remote Event Notification - Track Index (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x01 */
    uint32_t trackIndex;
}iap_remote_event_notification_track_index_t;


/** Remote Event Notification - Chapter Information (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x02 */
    uint32_t trackIndex;
    uint16_t chapterCount;
    uint16_t chapterIndex;
}iap_remote_event_notification_chapter_info_t;

/** Remote Event Notification - Play Status Values (Event Number = 0x03) Enumeration. Note: works also for Cmd = 0x10 */
typedef enum
{
    kPlayStatusPlaybackStopped,
    kPlayStatusPlaying,
    kPlayStatusPlaybackPaused,
    kPlayStatusFastForward,
    kPlayStatusFastRewind,
    kPlayStatusEndFastForwardRewindMode
}iap_remote_event_play_status_values_t;

/** Remote Event Notification - Play Status (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t eventNumber;                        /**< Event number = 0x03 */
    uint8_t playStatus;
}iap_remote_event_play_status_t;

/** Remote Event Notification - Mute/UI Volume (Lingo = 0x03, Cmd = 0x09, and Cmd = 0x0D) Structure */
typedef struct
{
    uint8_t eventNumber;                        /**< Event number = 0x04 */
    uint8_t muteState;
    uint8_t UIVolumeLevel;
}iap_remote_event_mute_ui_volume_t;

/** Remote Event Notification - Power and Battery States (Event Number = 0x05) Enumeration. Note: works also for Cmd = 0x1B */
typedef enum
{
    kInternalBatteryLowPower,
    kBatteryLowPower,
    kExternalBatteryLowPower,
    kExternalPowerBatteryPackNoCharging,
    kExternalPowerNoCharging,
    kExternalPowerBatteryCharging,
    kExternalPowerBatteryCharged
}iap_remote_event_power_and_battery_states_t;

/** Remote Event Notification - Power/Battery (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t eventNumber;                        /**< Event number = 0x05 */
    uint8_t powerState;
    uint8_t batteryLevel;
}iap_remote_event_power_and_battery_t;

/** Remote Event Notification - Equalizer State (Lingo = 0x03, Cmd = 0x09, and Cmd = 0x0D) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x06 */
    uint32_t equalizerIndex;
}iap_remote_event_equalizer_t;

/** Remote Event Notification - Shuffle States (Event Number = 0x07) Enumeration */
typedef enum
{
    kRemoteEventShuffleOff,
    kRemoteEventShuffleTrackAndSongs,
    kRemoteEventShuffleAlbums
}iap_remote_event_shuffle_states_t;

/** Remote Event Notification - Shuffle (Lingo = 0x03, Cmd = 0x09, and Cmd = 0x0D) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x07 */
    uint8_t  shuffleState;
}iap_remote_event_shuffle_t;

/** Remote Event Notification - Repeat States (Event Number = 0x08) Enumeration */
typedef enum
{
    kRemoteEventRepeatOff,
    kRemoteEventRepeatOneTrackOrSong,
    kRemoteEventRepeatAllTracks
}iap_remote_event_repeat_states_t;

/** Remote Event Notification - Repeat (Lingo = 0x03, Cmd = 0x09, and Cmd = 0x0D) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x08 */
    uint8_t  repeatState;
}iap_remote_event_repeat_t;

/** Remote Event Notification - Date/Time (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x09 */
    uint16_t year;
    uint8_t  month;
    uint8_t  day;
    uint8_t  hour;
    uint8_t  minute;
}iap_remote_event_date_time_t;

/** Remote Event Notification - Backlight (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x0B */
    uint8_t  backlightLevel;
}iap_remote_event_backlight_t;

/** Remote Event Notification - Hold Switch (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x0C */
    uint8_t  holdSwitchState;
}iap_remote_event_hold_switch_t;

/** Remote Event Notification - Sound Check (Lingo = 0x03, Cmd = 0x09, and Cmd = 0x0D) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x0D */
    uint8_t  soundCheckState;
}iap_remote_event_sound_check_t;

/** Remote Event Notification - Audiobook Speed (Event Number = 0x0E) Enumeration */
typedef enum
{
    kAudioBookSpeedNormal,
    kAudioBookSpeedFaster,
    kAudioBookSpeedSlower= 0xFF
}iap_remote_event_audio_book_speeds_t;

/** Remote Event Notification - Audiobook Speed (Lingo = 0x03, Cmd = 0x09, and Cmd = 0x0D) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x0E */
    uint8_t  speedSetting;
}iap_remote_event_audio_book_t;

/** Remote Event Notification - Track Position in Seconds (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x0F */
    uint16_t trackPositionSec;
}iap_remote_event_track_position_sec_t;

/** Remote Event Notification - Mute/UI/Absolute Volume (Lingo = 0x03, Cmd = 0x09, and Cmd = 0x0D) Structure */
typedef struct
{
    uint8_t eventNumber;                        /**< Event number = 0x10 */
    uint8_t muteState;
    uint8_t UIVolumeLevel;
    uint8_t absoluteVolumeLevel;
}iap_remote_event_mute_ui_absolute_volume_t;

/** Remote Event Notification - Track Capabilities Bits (Event Number = 0x11) Enumeration. Note: works also for Cmd = 0x12*/
typedef enum
{
    kTrackCapAudioBook,
    kTrackHasChapter,
    kTrackCapAlbumArtworkAvailable,
    kTrackCapHasSongLyrics,
    kTrackCapPodcastEpisode,
    kTrackCapHasReleaseDate,
    kTrackCapHasDescription,
    kTrackCapContainsVideo,
    kTrackCapCurrentlyQueuedAsVideo,
    kTrackCapGeniusPlaylistCapability = 13,
    kTrackCapITunesUEpisode
}iap_remote_event_track_capabilities_bits_t;

/** Remote Event Notification - Track Capabilities (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x11 */
    uint32_t trackCapabilities;
}iap_remote_event_track_capabilities_t;

/** Remote Event Notification - Playback Engine contents Changed (Lingo = 0x03, Cmd = 0x09, Cmd = 0x0D and Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  eventNumber;                        /**< Event number = 0x12 */
    uint32_t trackNumberInPlaylist;
}iap_remote_event_playback_engine_t;

/** Ret Remote Event Status                                                                 0x0B*/
typedef uint32_t iap_drl_remote_event_status_t;
/** Set iPod State Info Mute/UI Volume (Lingo = 0x03, Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t infoType;                        /**< Event number = 0x04 */
    uint8_t muteState;
    uint8_t UIVolumeLevel;
    uint8_t restoreOnExit;
}iap_set_ipod_state_mute_ui_volume_t;

/** Set iPod State Info - Equalizer State (Lingo = 0x03, Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  infoType;                        /**< Event number = 0x06 */
    uint32_t equalizerIndex;
    uint8_t  restoreOnExit;
}iap_set_ipod_state_equalizer_t;

/** Set iPod State Info - Shuffle (Lingo = 0x03, Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  infoType;                        /**< Event number = 0x07 */
    uint8_t  shuffleState;
    uint8_t  restoreOnExit;
}iap_set_ipod_state_shuffle_t;

/** Set iPod State Info - Repeat (Lingo = 0x03, Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  infoType;                        /**< Event number = 0x08 */
    uint8_t  repeatState;
    uint8_t  restoreOnExit;
}iap_set_ipod_state_repeat_t;

/** Set iPod State Info - Sound Check (Lingo = 0x03, Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  infoType;                        /**< Event number = 0x0D */
    uint8_t  soundCheckState;
    uint8_t  restoreOnExit;
}iap_set_ipod_state_check_t;

/** Set iPod State Info - Audiobook Speed (Lingo = 0x03, Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t  infoType;                        /**< Event number = 0x0E */
    uint8_t  speedSetting;
    uint8_t  restoreOnExit;
}iap_set_ipod_state_audio_book_t;

/** Set iPod State Info - Mute/UI/Absolute Volume (Lingo = 0x03, Cmd = 0x0E) Structure */
typedef struct
{
    uint8_t infoType;                        /**< Event number = 0x10 */
    uint8_t muteState;
    uint8_t UIVolumeLevel;
    uint8_t absoluteVolumeLevel;
    uint8_t restoreOnExit;
}iap_set_ipod_state_mute_ui_absolute_volume_t;

/** Return Play Status (Lingo = 0x03, Cmd = 0x10) Structure */
typedef struct
{
    iap_remote_event_play_status_values_t  playState;
    uint32_t trackIndex;
    uint32_t trackTotalLength;
    uint32_t trackCurrentPosition;
}iap_ret_play_status_t;

/** Get Indexed Playing Track Info (Lingo = 0x03, Cmd = 0x12) Structure */
typedef struct
{
    uint8_t  infoType;
    uint32_t trackIndex;
    uint16_t chapterIndex;
}iap_get_indexed_playing_track_info_t;

/** Return Indexed Playing Track Info Types (Lingo = 0x03, Cmd = 0x13) Enumeration. */
typedef enum
{
    kTrackCapabilitiesInfoType,
    kChapterTimeNameType,
    kArtistNameType,
    kAlbumNameType,
    kGenreNameType,
    kTrackTitleType,
    kComposerNameType,
    kLyricsType,
    kArtworkCountType
}iap_ret_playing_track_info_values_t;

/** Return Indexed Playing Track Info - Track Capabilities (Lingo = 0x03, Cmd = 0x13) Structure */
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x00 */
    uint32_t trackCapabilities;
    uint32_t trackTotalLength;
    uint16_t chapterCount;
}iap_ret_track_info_track_capabilities_t;

/** Return Indexed Playing Track Info - Chapter Time/Name (Lingo = 0x03, Cmd = 0x13) Structure */ 
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x01 */
    uint32_t chapterTime;
    uint8_t  chapterName[128];                    /**< Variable length */
}iap_ret_track_info_chapter_time_name_t;

/** Return Indexed Playing Track Info - Artist Name (Lingo = 0x03, Cmd = 0x13) Structure */ 
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x02 */
    uint8_t  artistName[128];                    /**< Variable length */
}iap_ret_track_info_artist_name_t;

/** Return Indexed Playing Track Info - Album Name (Lingo = 0x03, Cmd = 0x13) Structure */ 
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x03 */
    uint8_t  albumName[128];                    /**< Variable length */
}iap_ret_track_info_album_name_t;

/** Return Indexed Playing Track Info - Genre Name (Lingo = 0x03, Cmd = 0x13) Structure */ 
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x04 */
    uint8_t  genreName[128];                    /**< Variable length */
}iap_ret_track_info_genre_name_t;

/** Return Indexed Playing Track Info - Track Title (Lingo = 0x03, Cmd = 0x13) Structure */ 
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x05 */
    uint8_t  trackTitle[128];                    /**< Variable length */
}iap_ret_track_info_track_title_t;

/** Return Indexed Playing Track Info - Composer Name (Lingo = 0x03, Cmd = 0x13) Structure */ 
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x06 */
    uint8_t  composerName[128];                /**< Variable length */
}iap_ret_track_info_composer_name_t;

/** Return Indexed Playing Track Info - Lyrics Values (Info Type = 0x07) Enumeration */
typedef enum
{
    klyricsValuesOneOfMultiplePackets,
    klyricsValueslastPackets
}iap_ret_track_info_lyrics_values_t;

/** Return Indexed Playing Track Info - Lyrics (Lingo = 0x03, Cmd = 0x13) Structure */ 
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x07 */
    uint8_t  packetInfo;
    uint16_t packetIndex;
    uint8_t  trackLyric[128];                    /**< Variable length */
}iap_ret_track_info_lyrics_t;

/** Return Indexed Playing Track Info - Artwork Count (Lingo = 0x03, Cmd = 0x13) Structure */ 
typedef struct
{
    uint8_t  infoType;                            /**< Info Type = 0x08 */
    uint8_t  artworkCount[68];                    /**< Variable length */
}iap_ret_track_info_artwork_count_t;

/** Return Artwork Formats - Pixel Format Codes Enumeration */
typedef enum
{
    kPixelFormatMonochrome = 1,
    kPixelFormatMonochromeRGB565LittleEndian,
    kPixelFormatMonochromeRGB565BigEndian
}iap_ret_artwork_formats_pixel_format_codes_t;

/** Return Artwork Formats (Lingo = 0x03, Cmd = 0x17) Structure */ 
typedef struct
{
    uint16_t formatID;
    uint8_t  pixelFormat;
    uint16_t imageWidth;
    uint16_t imageHeight;
    uint8_t  previousParameters[128];            /**< Variable length */
}iap_ret_artwork_formats_t;

/** Get Track Artwork Data (Lingo = 0x03, Cmd = 0x18) Structure */ 
typedef struct
{
    uint32_t trackIndex;
    uint16_t formatID;
    uint32_t timeOffsetMs;
}iap_get_track_artwork_data_t;

/** Return Track Artwork Data - Packet Index 0 (Lingo = 0x03, Cmd = 0x19) Structure */ 
typedef struct
{
    uint16_t packetIndex;
    uint8_t  pixelFormat;
    uint16_t imageWidth;
    uint16_t imageHeight;
    uint16_t topLeftXValue;
    uint16_t topLeftYValue;
    uint16_t bottomRightXValue;
    uint16_t bottomRightYValue;
    uint32_t rowSize;                            /**< Row size in bytes */
    uint8_t  imagePixelData[IAPI_MAX_RX_PAYLOAD_SIZE];    /**< Variable length */
}iap_ret_track_artwork_data0_t;

/** Return Track Artwork Data - Packet Index 1 to N (Lingo = 0x03, Cmd = 0x19) Structure */ 
typedef struct
{
    uint16_t packetIndex;
    uint8_t  imagePixelData[IAPI_MAX_RX_PAYLOAD_SIZE];    /**< Variable length */
}iap_ret_track_artwork_data1N_t;

/** Return Power Battery State (Lingo = 0x03, Cmd = 0x1B) Structure */ 
typedef struct
{
    uint8_t powerStatus;
    uint8_t batteryLevel;
}iap_ret_power_battery_state_t;

/** Set Sound Check State (Lingo = 0x03, Cmd = 0x1E) Structure */ 
typedef struct
{
    uint8_t soundCheckState;
    uint8_t restoreOnExit;
}iap_set_sound_check_state_t;

/** Set Sound Check State (Lingo = 0x03, Cmd = 0x1E) Enum */ 
typedef enum
{
    kSoundCheckStateOff,
    kSoundCheckStateOn
}iap_sound_check_t;

/** Get Track Artwork Times (Lingo = 0x03, Cmd = 0x1F) Structure */ 
typedef struct
{
    uint32_t trackIndex;
    uint16_t formatID;
    uint16_t artworkIndex;
    uint16_t artworkCount;
}iap_get_track_artwork_times_t;

/** Return Track Artwork Times (Lingo = 0x03, Cmd = 0x20) Structure */ 
typedef struct
{
    uint32_t timeOffsetMs;
    uint8_t  previousParameters[128];            /**< Variable length */
}iap_ret_track_artwork_times_t;

typedef void (* iap_display_remote_lingo_event_notification_callback_t)(
                                            iap_remote_event_notification_number_t eventNumber,
                                            uint8_t* notificationPayload,
                                            uint16_t payloadSize);

#pragma pack (pop)

/*************************************************************************************************
*                                       Function-like Macros Section                           
*************************************************************************************************/


/*************************************************************************************************
*                                       Extern Constants Section                          
*************************************************************************************************/
extern const uint8_t kDisplayRemoteAsynchronousCommands
        [NUMBER_OF_DISPLAYREMOTE_ASYNCHRONOUS_COMMANDS];

/*************************************************************************************************
*                                       Extern Variables Section                          
*************************************************************************************************/
extern void (* kDisplayRemoteAsynchronousFunctions
        [NUMBER_OF_DISPLAYREMOTE_ASYNCHRONOUS_COMMANDS])(iap_interface_buffer_rx_frame_t* rxFrame);

/*************************************************************************************************
*                                      Function Prototypes Section                        
*************************************************************************************************/
uint8_t set_drl_remote_event_notification_callback(iap_display_remote_lingo_event_notification_callback_t callback);

/* Display Remote Lingo Commands ***********************************************************************/

/*!
 * @brief                       Display Remote Lingo (0x03) GetCurrentEQProfileIndex command (0x01)                     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_current_eq_profile_index(uint32_t* currentEQIndex,
                                                                      void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetCurrentEQProfileIndex,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) currentEQIndex,
                                                sizeof(uint32_t),
                                                kDisplayRemoteRetCurrentEQProfileIndex};

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) SetCurrentEQProfileIndex command (0x03)                     
 * @param equalizerProfile      The equalizer profile to be set
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_set_current_eq_profile_index
                                            (iap_set_current_eq_profile_index_t equalizerProfile,
                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &equalizerProfile,
                                                sizeof(iap_set_current_eq_profile_index_t),
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteSetCurrentEQProfileIndex,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kDisplayRemoteIpodAck};

#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(equalizerProfile.currenEQIndex, equalizerProfile.currenEQIndex);
#endif 
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) GetNumEQProfiles command (0x04)                     
 * @param profileCount          Pointer to where the received data will be saved 
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_num_eq_profiles(uint32_t* profileCount,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetNumEQProfiles,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) profileCount,
                                                sizeof(uint32_t),
                                                kDisplayRemoteRetNumEQProfiles};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) GetindexedEQProfileName command (0x06)                     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_indexed_eq_profile_name(uint32_t equalizerProfileIndex,
                                                                     uint8_t* rxPayLoad,
                                                                     uint16_t rxPayLoadSize,
                                                                     void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &equalizerProfileIndex,
                                                sizeof(equalizerProfileIndex),
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetIndexedEQProfileName,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kDisplayRemoteRetIndexedEQProfileName};
    

    
    return iap_commands_transmit_command(&txCommandData);
}


/*!
 * @brief                       Display Remote Lingo (0x03) SetRemoteEventMask command (0x08)                     
 * @param remoteEventBitMask    Bit mask that contains the events for which you want to enable
 *                              notifications 
 * @sa                          iap_remote_event_notification_number_t
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_set_remote_event_mask(uint32_t remoteEventBitMask,
                                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &remoteEventBitMask,
                                                sizeof(remoteEventBitMask),
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteSetRemoteEventNotification,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kDisplayRemoteIpodAck};
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(remoteEventBitMask, remoteEventBitMask);
#endif 
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) GetRemoteEventStatus command (0x0A)                     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_remote_event_status(uint32_t* remEventStatus,
                                                                 void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetRemoteEventStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) remEventStatus,
                                                sizeof(uint32_t),
                                                kDisplayRemoteRetRemoteEventStatus};
 
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) GetiPodStateInfo command (0x0C)                     
 * @param infoType              The type of state information for which to query the Apple device
 *                              notifications 
 * @sa                          iap_ipod_state_info_t
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_ipod_state_info(iap_ipod_state_info_t infoType,
                                                             uint8_t* rxPayLoad,
                                                             uint16_t rxPayLoadSize,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &infoType,
                                                sizeof(infoType),
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetIpodStateInfo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kDisplayRemoteRetIpodStateInfo};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) SetiPodStateInfo command (0x0E)                     
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @sa                          iap_remote_event_notification_track_position_ms_t
 *                              iap_remote_event_notification_track_index_t
 *                              iap_remote_event_notification_chapter_info_t
 *                              iap_remote_event_play_status_t
 *                              iap_remote_event_power_and_battery_t
 *                              iap_remote_event_date_time_t
 *                              iap_remote_event_backlight_t
 *                              iap_remote_event_hold_switch_t
 *                              iap_remote_event_audio_book_speeds_t
 *                              iap_remote_event_track_position_sec_t
 *                              iap_set_ipod_state_mute_ui_volume_t,
 *                              iap_set_ipod_state_equalizer_t,
 *                              iap_set_ipod_state_shuffle_t,
 *                              iap_set_ipod_state_repeat_t,
 *                              iap_set_ipod_state_check_t
 *                              iap_set_ipod_state_audio_book_t,
 *                              iap_set_ipod_state_mute_ui_absolute_volume_t,
 *                              iap_remote_event_track_capabilities_t,
 *                              iap_remote_event_playback_engine_t
 * @param txPayLoadSize         Size of the buffer that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_set_ipod_state_info(uint8_t* txPayLoad,
                                                             uint16_t txPayLoadSize,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteSetIpodStateInfo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kDisplayRemoteIpodAck};
    
    return iap_commands_transmit_command(&txCommandData);    

}

/*!
 * @brief                       Display Remote Lingo (0x03) GetPlayStatus command (0x0F)                     
 * @param playStatus             Pointer to the structure that will store the received payload 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_play_status(iap_ret_play_status_t* playStatus,
                                                         void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetPlayStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) playStatus,
                                                sizeof(iap_ret_play_status_t),
                                                kDisplayRemoteRetPlayStatus};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) SetCurrentPlayingTrack command (0x11)                     
 * @param trackIndex            The track index to begin playing
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_set_current_playing_track(uint32_t trackIndex,
                                                                   void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                (uint8_t*) &trackIndex,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteSetCurrentPlayingTrack,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kDisplayRemoteIpodAck};
    
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(trackIndex, trackIndex);
#endif 
    
  return iap_commands_transmit_command(&txCommandData);

}
/* This command is not supported the GetIndexedPlayingTrackInfo (0x000C) command from the Extended
 * Interface lingo (0x04) can be used instead of this one */
/*!
 * @brief                       Display Remote Lingo (0x03) GetIndexedPlayingTrackInfo command (0x12)                     
 * @param trackIndex            The track index to begin playing
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
/*
static inline  command_transmission_status_t drl_get_indexed_playing_track_info
        (iap_get_indexed_playing_track_info_t* trackToRequestInfo,
         uint8_t* rxPayLoad,
         uint16_t rxPayLoadSize,
         void (*rxCallback)(uint8_t))
{
    (void) trackToRequestInfo;
    (void) rxPayLoad;
    (void) rxPayLoadSize;
    (void) rxCallback;
}
*/

/*!
 * @brief                       Display Remote Lingo (0x03) GetNumPlayingTracks command (0x14)                     
 * @param numPlayingTracks      Pointer to variable where the payload of the answer will be saved 
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_num_playing_tracks(uint32_t* numPlayingTracks,
                                                                void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetNumPlayingTracks,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) numPlayingTracks,
                                                sizeof(uint32_t),
                                                kDisplayRemoteRetNumPlayingTracks};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) GetArtworkFormats command (0x16)                     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_artwork_formats(uint8_t* rxPayLoad,
                                                             uint16_t rxPayLoadSize,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetArtworkFormats,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kDisplayRemoteRetArtworkFormats};
                                                
    return iap_commands_transmit_command(&txCommandData);
                                                
}

/*! This command is not supported by the stack
 *  Per section 4.3.27 This command has been superseded by the Extended Interface lingo (0x04)  
 *  command GetArtworkData (0x004E) which provides a better functionality  
 * @brief                       Display Remote Lingo (0x03) GetTrackArtworkData command (0x18)                     
 * @param trackData             Pointer to structure that contains the information of the track to
 *                              get the artwork data
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
*/
/*
static inline  command_transmission_status_t drl_get_track_artwork_data(iap_get_track_artwork_data_t* trackData,
                                                                uint8_t* rxPayLoad,
                                                                uint16_t rxPayLoadSize,
                                                                void (*rxCallback)(uint8_t))
{
    (void) trackData;
    (void) rxPayLoad;
    (void) rxPayLoadSize;
    (void) rxCallback;
    
    return (kTransmissionInvalidCommand); 
}
*/

/*!
 * @brief                       Display Remote Lingo (0x03) GetPowerBatteryState command (0x1A)                     
 * @param batteryState          Pointer to the structure where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_power_battery_state(iap_ret_power_battery_state_t* 
                                                                 batteryState,
                                                                 void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetPowerBatteryState,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) batteryState,
                                                sizeof(iap_ret_power_battery_state_t),
                                                kDisplayRemoteRetPowerBatteryState};

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Display Remote Lingo (0x03) GetSoundCheckState command (0x1C)                     
 * @param soundCheckState       Pointer to the varibles where the payload of the answer will be saved 
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_get_sound_check_state(iap_sound_check_t* soundCheckState,
                                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteGetSoundCheckState,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) soundCheckState,
                                                sizeof(uint8_t),
                                                kDisplayRemoteRetSoundCheckState};

    return iap_commands_transmit_command(&txCommandData);
    
}

/*!
 * @brief                       Display Remote Lingo (0x03) SetSoundCheckState command (0x1E)                     
 * @param soundCheckState       Pointer to the structure that stores the sound check state to be set
 * @sa                          iap_set_sound_check_state_t
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_set_sound_check_state(iap_set_sound_check_state_t* soundCheckState,
                                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) soundCheckState,
                                                sizeof(iap_set_sound_check_state_t),
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteSetSoundCheckState,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kDisplayRemoteIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*! Per section 4.3.34 This command has been superseded by the extended interface lingo command 
 *  0x004C GetArtworkTimes which provides a better functionality  
 * @brief                       Display Remote Lingo (0x03) GetTrackArtworkTimes command (0x1F)                     
 * @param trackData             Pointer to structure that contains the information of the track to
 *                              get the artwork data times
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
/*
static inline  command_transmission_status_t drl_get_track_artwork_times(iap_get_track_artwork_times_t* trackData,
                                                                uint8_t* rxPayLoad,
                                                                uint16_t rxPayLoadSize,
                                                                void (*rxCallback)(uint8_t))
{
    (void) trackData;
    (void) rxPayLoad;
    (void) rxPayLoadSize;
    (void) rxCallback;
    
    return (kTransmissionInvalidCommand); 
}
*/

/*!
 * @brief                       Display Remote Lingo (0x03) CreateGeniusPlaylist command (0x21)                     
 * @param trackIndex            Playback index
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_create_genius_playlist(uint32_t trackIndex,
                                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k30sTimeout,                
                                                (uint8_t*) &trackIndex,
                                                sizeof(trackIndex),
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteCreateGeniusPlaylist,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kDisplayRemoteIpodAck};
                                                
 
#if(BIG_ENDIAN_CORE == _FALSE_)
  BYTESWAP32(trackIndex, trackIndex);
#endif 
  
    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief                       Display Remote Lingo (0x03) IsGeniusAvailableForTrack command (0x21)                     
 * @param trackIndex            Playback index
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t drl_is_genius_available_for_track(uint32_t trackIndex,
                                                                       void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                (uint8_t*) &trackIndex,
                                                sizeof(trackIndex),
                                                0,
                                                kQuestionPacket,
                                                kDisplayRemoteLingo,
                                                kDisplayRemoteIsGeniusAvailableForTrack,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kDisplayRemoteIpodAck};

#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(trackIndex, trackIndex);
#endif 
    
    return iap_commands_transmit_command(&txCommandData);
}

/* End of Display Remote Lingo Commands ***********************************************************/

#if(BIG_ENDIAN_CORE == _FALSE_)
/*!
*  @addtogroup  DisplayRemoteLingoDataAccessors Display Remote Lingo Data Accessors
*  @brief       These are the macros to obtain the 16 & 32 bits fields from the structures  
*               used, in certain commands from the Display Remote lingo, to store the received data  
*  @{  
*/
/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x02*/
static inline  uint32_t get_current_equalizer_index(uint32_t* currentEqIndex)
{
    return BYTESWAP32(*currentEqIndex, *currentEqIndex);
}

/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x05*/
static inline  uint32_t get_number_of_eq_profiles(uint32_t* profileCount)
{
    return BYTESWAP32(*profileCount, *profileCount);
}

/*! The argument for these macros is a pointer to
 * iap_remote_event_notification_track_position_ms_t type                                   0x09
 *                                                               Note: works also for Cmd = 0x0D*/
static inline  uint32_t get_event_notification_track_position
                (iap_remote_event_notification_track_position_ms_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackPositionMs, dataPacket->trackPositionMs);
}

/*! The argument for these macros is a pointer to
 * iap_remote_event_notification_track_index_t type */
static inline  uint32_t get_event_notification_track_index
                (iap_remote_event_notification_track_index_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackIndex, dataPacket->trackIndex);
}

/*! The argument for these macros is a pointer to
 * iap_remote_event_notification_chapter_info_t type */
static inline  uint32_t get_event_notification_chapter_info_track_index
                (iap_remote_event_notification_chapter_info_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackIndex, dataPacket->trackIndex);
}

static inline  uint16_t get_event_notification_chapter_info_chapter_count
                (iap_remote_event_notification_chapter_info_t* dataPacket)
{
    return BYTESWAP16(dataPacket->chapterCount, dataPacket->chapterCount);
}

static inline  uint16_t get_event_notification_chapter_info_chapter_index
                (iap_remote_event_notification_chapter_info_t* dataPacket)
{
    return BYTESWAP16(dataPacket->chapterIndex, dataPacket->chapterIndex);
}

/*! The argument for these macros is a pointer to
 * iap_remote_event_equalizer_t type */
static inline  uint32_t get_event_notification_equalizer_state
                (iap_remote_event_equalizer_t* dataPacket)
{
    return BYTESWAP32(dataPacket->equalizerIndex, dataPacket->equalizerIndex);
}

/*! The argument for these macros is a pointer to
 * iap_remote_event_date_time_t type */
static inline  uint32_t get_event_notification_current_year
                (iap_remote_event_date_time_t* dataPacket)
{
    return BYTESWAP16(dataPacket->year, dataPacket->year);
}

/*! The argument for these macros is a pointer to
 * iap_remote_event_track_position_sec_t type */
static inline  uint16_t get_event_notification_track_position_in_seconds
                (iap_remote_event_track_position_sec_t* dataPacket)
{
    return BYTESWAP16(dataPacket->trackPositionSec, dataPacket->trackPositionSec);
}

/*! The argument for these macros is a pointer to
 * iap_remote_event_track_capabilities_t type */
static inline  uint32_t get_event_notification_track_capabilities
                (iap_remote_event_track_capabilities_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackCapabilities, dataPacket->trackCapabilities);
}

/*! The argument for these macros is a pointer to
 * iap_remote_event_playback_engine_t type */
static inline  uint32_t get_event_notification_playback_engine
                (iap_remote_event_playback_engine_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackNumberInPlaylist, dataPacket->trackNumberInPlaylist);
}

/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x0B*/
static inline  uint32_t get_remote_event_status (uint32_t* dataPacket)
{
    return BYTESWAP32(*dataPacket, *dataPacket);
}

/*! The argument for these macros is a pointer to
 * iap_ret_play_status_t type                                                               0x10*/
static inline  uint32_t get_play_status_track_index(iap_ret_play_status_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackIndex, dataPacket->trackIndex);
}

static inline  uint32_t get_play_status_track_total_length(iap_ret_play_status_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackTotalLength, dataPacket->trackTotalLength);
}

static inline  uint32_t get_play_status_track_current_position(iap_ret_play_status_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackCurrentPosition, dataPacket->trackCurrentPosition);
}

/*! The argument for these macros is a pointer to
 * iap_ret_track_info_track_capabilities_t type                                             0x13*/
static inline  uint32_t get_indexed_playing_track_info_capabilities
                (iap_ret_track_info_track_capabilities_t* dataPacket)
{
    return BYTESWAP32(dataPacket->trackCapabilities, dataPacket->trackCapabilities);
}

static inline  uint16_t get_indexed_playing_track_info_track_total_length
                (iap_ret_track_info_track_capabilities_t* dataPacket)
{
    return BYTESWAP16(dataPacket->trackTotalLength, dataPacket->trackTotalLength);
}

static inline  uint16_t get_indexed_playing_track_info_chapter_count
                (iap_ret_track_info_track_capabilities_t* dataPacket)
{
    return BYTESWAP16(dataPacket->chapterCount, dataPacket->chapterCount);
}

/*! The argument for these macros is a pointer to
 * iap_ret_track_info_chapter_time_name_t type */
static inline  uint32_t get_idexed_playing_track_info_chapter_time
                (iap_ret_track_info_chapter_time_name_t* dataPacket)
{
    return BYTESWAP32(dataPacket->chapterTime, dataPacket->chapterTime);
}
/*! The argument for these macros is a pointer to
 * iap_ret_track_info_lyrics_t type */
static inline  uint16_t get_indexed_playing_track_info_lyrics_packet_index
                (iap_ret_track_info_lyrics_t* dataPacket)
{
    return BYTESWAP16(dataPacket->packetIndex, dataPacket->packetIndex);
}

/*! The argument for these macros is a pointer to
 * iap_ret_track_info_artwork_count_t type */
static inline  uint16_t get_indexed_playing_track_info_artwork_format_id
                (iap_ret_track_info_artwork_count_t* dataPacket, uint8_t artworkCountIndex)
{
    uint16_t* formatId;
    formatId = (uint16_t*)&dataPacket->artworkCount[artworkCountIndex * 4];
    return BYTESWAP16(*formatId, *formatId);
}

static inline  uint16_t get_indexed_playing_track_info_artwork_count
                (iap_ret_track_info_artwork_count_t* dataPacket, uint8_t artworkCountIndex)
{
    uint16_t* artworkCount;
    artworkCount = (uint16_t*)&dataPacket->artworkCount[(artworkCountIndex * 4) + 2];
    return BYTESWAP16(*artworkCount, *artworkCount);
}
#endif

/*! The argument for these macros is a pointer to
 * uint32_t type                                                                            0x15*/
static inline  uint32_t get_number_of_playing_tracks (uint32_t* numplayingTracks)
{
    return BYTESWAP32(*numplayingTracks, *numplayingTracks);
}
/*!     @} 
* Group DisplayRemoteLingoDataAccessors
*/

/*! @} 
* Group DisplayRemoteLingo
* @}  
*
* Group iAP1_Lingoes
*/
#endif /* DISPLAYREMOTELINGO_H_ */
/*************************************************************************************************
* EOF                     
*************************************************************************************************/
