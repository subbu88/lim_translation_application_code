/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

/*************************************************************************************************
*                                      Includes Section                        
*************************************************************************************************/
#include "ExtendedInterfaceLingo.h"

/*************************************************************************************************
*                                   Defines & Macros Section                   
*************************************************************************************************/
/*! Maximum allowed size for the track info buffer*/
#define MAX_TRACK_INFO_BUFFER_SIZE                          (128)

#if(TRACK_INFO_DEFAULT_BUFFER_SIZE > MAX_TRACK_INFO_BUFFER_SIZE)
    #error "The size of the track info buffer exceeds the maximum permitted size"
#endif

/*! Maximum allowed size for the track lyrics buffer*/
#define MAX_TRACK_INFO_LYRICS_BUFFER_SIZE                   (2048)

#if(TRACK_INFO_LYRICS_BUFFER_SIZE > MAX_TRACK_INFO_LYRICS_BUFFER_SIZE)
    #error "The size of the lyrics buffer exceeds the maximum permitted size"
#endif

/*! Number of bytes that must be skipped for the RetDBTrackInfo and RetPBTrackInfo commands 
 *  in order to access to the track information type (bit position number) */
#define TRACK_INFO_INDEX_OFFSET                             (4)

/*! Number of bytes that must be skipped for the RetUIDTrackInfo in order to access to the
 * track information type (bit position number) */
#define UID_TRACK_INFO_INDEX_OFFSET                         (8)

/*! 
 * track information type (bit position number) for the lyrics of the
 * RetDBTrackInfo, RetPBTrackInfo, and RetUIDTrackInfo commands */
#define TRACK_INFO_LYRICS_INDEX                             (0x0b)

/*! Number of bytes that must be skipped for the ReturnCategorizedDatabaseRecords command (0x001B)
 *  in order to access to the actual database record character array */
#define DATABASE_INDEX_OFFSET                               (4)

/** Macro that contains track information the enabled bits  */
#define TRACK_INFO_TYPE_BITS_ENABLED            ((CAPABILITIES_MASK << kCapabilities) |\
                                                 (TRACK_NAME_MASK << kTrackName) |\
                                                 (ARTIRST_NAME_MASK << kArtistName) |\
                                                 (ALBUM_NAME_MASK << kAlbumName) |\
                                                 (GENRE_NAME_MASK << kGenreName) |\
                                                 (COMPOSER_NAME_MASK << kComposerName) |\
                                                 (TOTAL_TRACK_TIME_DURATION_MASK << kTotalTrackTimeDuration) |\
                                                 (UNIQUE_TRACK_ID_MASK << kUniqueTrackID) |\
                                                 (CHAPTER_COUNT_MASK << kChapterCount) |\
                                                 (CHAPTER_TIMES_MASK << kChapterTimes) |\
                                                 (CHAPTER_NAMES_MASK << kChapterNames) |\
                                                 (LYRIC_OF_SONG_CURRENTLY_PLAYING_MASK << kLyricOfSongCurrentlyPlaying) |\
                                                 (DESCRIPTION_MASK << kDescription) |\
                                                 (ALBUM_TRACK_INDEX_MASK << kAlbumTrackIndex) |\
                                                 (DISC_SET_ALBUM_INDEX_MASK << kDiscSetAlbumIndex) |\
                                                 (PLAY_COUNT_MASK << kPlayCount) |\
                                                 (SKIP_COUNT_MASK << kSkipCount) |\
                                                 (PODCAST_RELEASE_DATE_MASK << kPodcastReleaseDate) |\
                                                 (LAST_PLAYED_DATE_TIME_MASK << kLastPlayedDateTime) |\
                                                 (YEAR_RELEASE_DATE_MASK << kYearReleaseDate) |\
                                                 (STAR_RATING_MASK << kStarRating) |\
                                                 (SERIES_NAME_MASK << kSeriesName) |\
                                                 (SEASON_NUMBER_MASK << kSeasonNumber) |\
                                                 (TRACK_VOLUME_ADJUST_MASK << kTrackVolumeAdjust) |\
                                                 (TRACK_EQ_PRESET_MASK << kTrackEQPreset) |\
                                                 (TRACK_DATA_RATE_MASK << kTrackDataRate) |\
                                                 (BOOKMARK_OFFSET_MASK << kBookmartOffset) |\
                                                 (START_STOP_TIME_OFFSET_MASK << kStartStopTimeOffset))

/** Position of the notification type in the 
 * Play Status Change Notification packet */
#define NOTIFICATION_TYPE_POSITION                          (0)

/** Position of the notification payload in the 
 * Play Status Change Notification packet */
#define NOTIFICATION_PAYLOAD_POSITION                       (1)

/*************************************************************************************************
*                                       Typedef Section                        
*************************************************************************************************/

typedef enum
{
    kNoIdentifier,
    kUniqueIdentifier,
    kDatabaseIndex
}track_identifier_t;

typedef enum
{
    kMultiplePacketsEnabled = 0x00,
    kLastMultiplePacket
}packet_information_bits_t;
/*************************************************************************************************
*                                  Function Prototypes Section                 
*************************************************************************************************/
static void s_iap_extended_interface_play_status_change_notification
                                    (iap_interface_buffer_rx_frame_t* rxFrame);

static uint8_t iap_extended_interface_get_track_information_callback(uint8_t* receivedPayload,
                                                                     uint16_t receivedPayloadSize);

static uint8_t iap_extended_interface_retrieve_categorized_database_records_callback(uint8_t* receivedPayload,
                                                                                     uint16_t receivedPayloadSize);

static uint8_t iap_extended_interface_get_indexed_playing_track_info
                                    (uint8_t* receivedPayload, uint16_t receivedPayloadSize);
/*************************************************************************************************
*                                   Global Constants Section                   
*************************************************************************************************/

/*! Extended Interface Lingo Asynchronous Commands*/
const uint8_t kExtendedInterfaceAsynchronousCommands
        [NUMBER_OF_EXTENDEDINTERFACE_ASYNCHRONOUS_COMMANDS] =
{
        kExtendedInterfacePlayStatusChangeNotification          //0x27
};

/*************************************************************************************************
*                                   Static Constants Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                   Global Variables Section                   
*************************************************************************************************/

/*! Array of Extended Interface Lingo functions */
void (* kExtendedInterfaceAsynchronousFunctions[NUMBER_OF_EXTENDEDINTERFACE_ASYNCHRONOUS_COMMANDS])
        (iap_interface_buffer_rx_frame_t* rxFrame) = 
        {
                s_iap_extended_interface_play_status_change_notification
        };


/*! Global variables for the RetDBTrackInfo, RetPBTrackInfo, and RetUIDTrackInfo commands */

/*! Stores the type of identifier for the requested track that will be returned, could 
 * be a database index or an unique ID*/
track_identifier_t g_identifierType = kNoIdentifier;

/*! Variable that stores the number of tracks to request information from */
uint8_t g_trackCount;

/*! Variable that stores the number of bytes that must be skipped of the data payload in order to 
 * access to the actual data of the RetDBTrackInfo, RetPBTrackInfo, and RetUIDTrackInfo commands */
uint8_t g_dataOffset;

/*! Variable to store the track information bit mask that contains the requested information 
 * for a track */
uint32_t g_trackInformationBitmask;

/*! Variable to store the track information bit mask as a backup used internally by the module for 
 * the track information commands to keep restoring the information bit mask in the case of 
 * requesting information from multiple tracks */
uint32_t g_trackInformationBitmaskBackUp;

/*! Variable to save the callback that will be called when the total information of each track
 * is completely stored */
void (*g_trackInfoAppCallback)(iap_eil_track_information_t*, uint8_t);

/*! Variables and buffers that will store the information for a track */
#if(CAPABILITIES_MASK == _TRUE_)
    /*! Capabilities */
    uint32_t capabilities;
#endif
#if(TRACK_NAME_MASK == _TRUE_)
    /*! Track Name */
    uint8_t trackNameBuffer[TRACK_INFO_DEFAULT_BUFFER_SIZE];
#endif
#if(ARTIRST_NAME_MASK == _TRUE_)
    /*! Artist Name */
    uint8_t artistNameBuffer[TRACK_INFO_DEFAULT_BUFFER_SIZE];
#endif
#if(ALBUM_NAME_MASK == _TRUE_)
    /*! Album Name */
    uint8_t albumNameBuffer[TRACK_INFO_DEFAULT_BUFFER_SIZE];
#endif
#if(GENRE_NAME_MASK == _TRUE_)
    /*! Genre Name */
    uint8_t genreNameBuffer[TRACK_INFO_DEFAULT_BUFFER_SIZE];
#endif
#if(COMPOSER_NAME_MASK == _TRUE_)
    /*! Composer Name */
    uint8_t composerNameBuffer[TRACK_INFO_DEFAULT_BUFFER_SIZE];
#endif
#if(TOTAL_TRACK_TIME_DURATION_MASK == _TRUE_)
    /*! Total Track Duration in milliseconds */
    uint32_t totalTrackDurationMs;
#endif
#if(UNIQUE_TRACK_ID_MASK == _TRUE_)
    /*! Unique Track ID */
    uint8_t UniqueTrackIDBuffer[8];
#endif
#if(CHAPTER_COUNT_MASK == _TRUE_)
    /*! Chapter Count */
    uint16_t chapterCount;
#endif
#if(CHAPTER_TIMES_MASK == _TRUE_)
    /*! Chapter Times */
    uint8_t chapterTimesBuffer[6];
#endif
#if(CHAPTER_NAMES_MASK == _TRUE_)
    /*! Chapter Names*/
    uint8_t chapterNamesBuffer[TRACK_INFO_DEFAULT_BUFFER_SIZE];
#endif
#if(LYRIC_OF_SONG_CURRENTLY_PLAYING_MASK == _TRUE_)
    /*! Lyric of currently playing song */
    uint8_t lyricOfSongCurrentlyPlayingBuffer[TRACK_INFO_LYRICS_BUFFER_SIZE];
#endif
#if(DESCRIPTION_MASK == _TRUE_)
    /*! Description */
    uint8_t descriptionBuffer[TRACK_INFO_DEFAULT_BUFFER_SIZE];
#endif
#if(ALBUM_TRACK_INDEX_MASK == _TRUE_)
    /*! Album Track Index */
    uint16_t albumTrackIndex;
#endif
#if(DISC_SET_ALBUM_INDEX_MASK == _TRUE_)
    /*! Disc Set Album Index */
    uint16_t discSetAlbumIndex;
#endif
#if(PLAY_COUNT_MASK == _TRUE_)
    /*! Play Count */
    uint32_t playCount;
#endif
#if(SKIP_COUNT_MASK == _TRUE_)
    /*! Skip Count */
    uint32_t skipCount;
#endif
#if(PODCAST_RELEASE_DATE_MASK == _TRUE_)
    /*! Podcast Release Date */
    uint8_t podcastReleaseDateBuffer[7];
#endif
#if(LAST_PLAYED_DATE_TIME_MASK == _TRUE_)
    /*! Last Played Date/Time*/
    uint8_t lastPlayedDateTimeBuffer[7];
#endif
#if(YEAR_RELEASE_DATE_MASK == _TRUE_)
    /*! Year (release date) */
    uint16_t yearReleaseDate;
#endif
#if(STAR_RATING_MASK == _TRUE_)
    /*! Star Rating */
    uint8_t starRating;
#endif
#if(SERIES_NAME_MASK == _TRUE_)
    /*! Series Name */
    uint8_t seriesNameBuffer[TRACK_INFO_DEFAULT_BUFFER_SIZE];
#endif
#if(SEASON_NUMBER_MASK == _TRUE_)
    /*! Season Number */
    uint16_t seasonNumber;
#endif
#if(TRACK_VOLUME_ADJUST_MASK == _TRUE_)
    /*! Track volume Adjust */
    uint8_t trackVolumeAdjust;
#endif
#if(TRACK_EQ_PRESET_MASK == _TRUE_)
    /*! Track EQ Preset */
    uint16_t trackEQPreset;
#endif
#if(TRACK_DATA_RATE_MASK == _TRUE_)
    /*! Track Data Rate */
    uint32_t trackDataRate;
#endif
#if(BOOKMARK_OFFSET_MASK == _TRUE_)
    /*! Bookmark Offset */
    uint32_t bookmarkOffset;
#endif
#if(START_STOP_TIME_OFFSET_MASK == _TRUE_)
    /*! Start/Stop Time Offset */
    uint8_t starStopTimeOffsetBuffer[8];
#endif

/*! Global structure that stores pointers to the variables and buffers where the track information
 *  will be saved. The application can access to the track information received through a pointer 
 *  to this structure */
iap_eil_track_information_t g_trackInformation = {
            {0, 0, 0, 0, 0, 0, 0, 0},
#if(LYRIC_OF_SONG_CURRENTLY_PLAYING_MASK == _TRUE_)
            0x0000,
#endif
    {   {
#if(CAPABILITIES_MASK == _TRUE_)
            &capabilities,
#else
    NULL,
#endif
#if(TRACK_NAME_MASK == _TRUE_)
            &trackNameBuffer[0],
#else
            NULL,
#endif
#if(ARTIRST_NAME_MASK == _TRUE_)
            &artistNameBuffer[0],
#else
            NULL,
#endif
#if(ALBUM_NAME_MASK == _TRUE_)
            &albumNameBuffer[0],
#else
            NULL,
#endif
#if(GENRE_NAME_MASK == _TRUE_)
            &genreNameBuffer[0],
#else
            NULL,
#endif
#if(COMPOSER_NAME_MASK == _TRUE_)
            &composerNameBuffer[0],
#else
            NULL,
#endif
#if(TOTAL_TRACK_TIME_DURATION_MASK == _TRUE_)
            &totalTrackDurationMs,
#else
            NULL,
#endif
#if(UNIQUE_TRACK_ID_MASK == _TRUE_)
            &UniqueTrackIDBuffer[0],
#else
            NULL,
#endif
#if(CHAPTER_COUNT_MASK == _TRUE_)
            &chapterCount,
#else
            NULL,
#endif
#if(CHAPTER_TIMES_MASK == _TRUE_)
            &chapterTimesBuffer[0],
#else
            NULL,
#endif
#if(CHAPTER_NAMES_MASK == _TRUE_)
            &chapterNamesBuffer[0],
#else
            NULL,
#endif
#if(LYRIC_OF_SONG_CURRENTLY_PLAYING_MASK == _TRUE_)
            &lyricOfSongCurrentlyPlayingBuffer[0],
#else
            NULL,
#endif
#if(DESCRIPTION_MASK == _TRUE_)
            &descriptionBuffer[0],
#else
            NULL,
#endif
#if(ALBUM_TRACK_INDEX_MASK == _TRUE_)
            &albumTrackIndex,
#else
            NULL,
#endif
#if(DISC_SET_ALBUM_INDEX_MASK == _TRUE_)
            &discSetAlbumIndex,
#else
            NULL,
#endif
#if(PLAY_COUNT_MASK == _TRUE_)
            &playCount,
#else
            NULL,
#endif
#if(SKIP_COUNT_MASK == _TRUE_)
            &skipCount,
#else
            NULL,
#endif
#if(PODCAST_RELEASE_DATE_MASK == _TRUE_)
            &podcastReleaseDateBuffer[0],
#else
            NULL,
#endif
#if(LAST_PLAYED_DATE_TIME_MASK == _TRUE_)
            &lastPlayedDateTimeBuffer[0],
#else
            NULL,
#endif
#if(YEAR_RELEASE_DATE_MASK == _TRUE_)
            &yearReleaseDate,
#else
            NULL,
#endif
#if(STAR_RATING_MASK == _TRUE_)
            &starRating,
#else
            NULL,
#endif
#if(SERIES_NAME_MASK == _TRUE_)
            &seriesNameBuffer[0],
#else
            NULL,
#endif
#if(SEASON_NUMBER_MASK == _TRUE_)
            &seasonNumber,
#else
            NULL,
#endif
#if(TRACK_VOLUME_ADJUST_MASK == _TRUE_)
            &trackVolumeAdjust,
#else
            NULL,
#endif
#if(TRACK_EQ_PRESET_MASK == _TRUE_)
            &trackEQPreset,
#else
            NULL,
#endif
#if(TRACK_DATA_RATE_MASK == _TRUE_)
            &trackDataRate,
#else
            NULL,
#endif
#if(BOOKMARK_OFFSET_MASK == _TRUE_)
            &bookmarkOffset,
#else
            NULL,
#endif
#if(START_STOP_TIME_OFFSET_MASK == _TRUE_)
            &starStopTimeOffsetBuffer[0]
#else
            NULL
#endif
    }   }
};

/*! Global structure for the ReturnCategorizedDatabaseRecords command (0x001B) that stores the 
 *  number of requested records and the pointers to where the record's names will be saved*/
iap_eil_return_db_records_t g_categorizedRecords;

/*! Variable for the ReturnCategorizedDatabaseRecords command (0x001B) that stores the application 
 *  callback that is called when all the requested records from this command has been received */
void (*g_retrieveDBRecordAppCallback)(uint8_t);

/*! Global structure for ReturnIndexedPlayingTrackInfo command (0x000D) that store the track info
 * type and track information requested */
iap_eil_ret_indexed_playing_track_info_t g_indexedPlayingTrackInfo;

/*! Variable for the ReturnIndexedPlayingTrackInfo command (0x000D) that store the application
 * callback that is called when the response has been received and codified */
void (*g_indexedPlayingTrackInfoAppCallback)(iap_eil_ret_indexed_playing_track_info_t*);
/*************************************************************************************************
*                                   Static Variables Section                   
*************************************************************************************************/

static iap_commands_play_status_change_notification_callback_t s_iap_commands_play_status_change_notification_callback;

/*************************************************************************************************
*                                      Functions Section                       
*************************************************************************************************/
uint8_t set_eil_play_status_change_notification_callback
                                 (iap_commands_play_status_change_notification_callback_t callback)
{
    uint8_t returnValue = _ERROR_; 
    
    if (NULL != callback)
    {
        s_iap_commands_play_status_change_notification_callback = callback;
    }
    
    return returnValue;
}

static void s_iap_extended_interface_play_status_change_notification
                                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    if(NULL != s_iap_commands_play_status_change_notification_callback)
    {
        s_iap_commands_play_status_change_notification_callback
        ((iap_eil_status_change_notification_t)rxFrame->payload[NOTIFICATION_TYPE_POSITION],
         (iap_eil_status_change_parameters_t*)&rxFrame->payload[NOTIFICATION_PAYLOAD_POSITION]);
    }
    
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/* See ExtendedInterfaceLingo.h for documentation of this function */
command_transmission_status_t iap_extended_interface_get_track_information(iap_eil_get_track_information_t
                                                                           trackInfoPacketPayload,
                                                                           uint8_t indexType,
                                                                           void (*rxCallback)(iap_eil_track_information_t*, uint8_t))
{
    command_transmission_status_t txStatus;

    if(rxCallback != NULL && 
            ((TRACK_INFO_TYPE_BITS_ENABLED & trackInfoPacketPayload.trackInformationBitmask)
                    == trackInfoPacketPayload.trackInformationBitmask))
    {           
        iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                    (uint8_t*) &trackInfoPacketPayload, 
                                                    sizeof(iap_eil_get_track_information_t),
                                                    0,
                                                    kQuestionPacket,
                                                    kExtendedInterfaceLingo,
                                                    0,
                                                    (kCommandIsWaitingForResponse | kCommandWithMultipleResponses)},
                                                    (void (*)(uint8_t))iap_extended_interface_get_track_information_callback,
                                                    NULL,
                                                    NULL,
                                                    0,
                                                    0};

        g_trackCount = trackInfoPacketPayload.trackCount;
        g_dataOffset = TRACK_INFO_INDEX_OFFSET;
        g_identifierType = kDatabaseIndex;
        g_trackInformationBitmask = trackInfoPacketPayload.trackInformationBitmask;
        g_trackInformationBitmaskBackUp = trackInfoPacketPayload.trackInformationBitmask;
        g_trackInfoAppCallback = rxCallback;
        
#if(BIG_ENDIAN_CORE == _FALSE_)
        BYTESWAP32(trackInfoPacketPayload.trackDBStartIndex,trackInfoPacketPayload.trackDBStartIndex);
        BYTESWAP32(trackInfoPacketPayload.trackCount,trackInfoPacketPayload.trackCount);
#endif
        
        /* Check if requested track info is from database or playback */
        if(kDatabaseEngine == indexType)
        {
            txCommandData.txFrameData.commandId = kExtendedInterfaceGetDbTrackInfo;
            txCommandData.expectedAnswer = kExtendedInterfaceRetDbTrackInfo;
        }
        else
        {
            txCommandData.txFrameData.commandId  = kExtendedInterfaceGetPbTrackInfo;
            txCommandData.expectedAnswer = kExtendedInterfaceRetPbTrackInfo;
        }
        
        txStatus = iap_commands_transmit_command(&txCommandData);
       
    }else
    {
        txStatus = kInvalidParameter;
    }

    return txStatus;

}

/* See ExtendedInterfaceLingo.h for documentation of this function */
command_transmission_status_t iap_extended_interface_get_uid_track_info(iap_eil_get_uid_track_information_t
                                                                        getUIDTrackInfoPacketPayload,
                                                                        void (*rxCallback)(iap_eil_track_information_t*, uint8_t))
{
    command_transmission_status_t txStatus;

    /* Check the given parameters */
    if(rxCallback != NULL &&
       ((TRACK_INFO_TYPE_BITS_ENABLED & getUIDTrackInfoPacketPayload.trackInformationBitmask) == 
               getUIDTrackInfoPacketPayload.trackInformationBitmask))
    {
        iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                    (uint8_t*) &getUIDTrackInfoPacketPayload,
                                                    sizeof(iap_eil_get_uid_track_information_t),
                                                    0,
                                                    kQuestionPacket,
                                                    kExtendedInterfaceLingo,
                                                    kExtendedInterfaceGetUidTrackInfo,
                                                    (kCommandIsWaitingForResponse | kCommandWithMultipleResponses)},
                                                    (void (*)(uint8_t)) iap_extended_interface_get_track_information_callback,
                                                    NULL,
                                                    NULL,
                                                    0,
                                                    kExtendedInterfaceRetUidTrackInfo};
          
        g_trackCount = 1;
        g_dataOffset = UID_TRACK_INFO_INDEX_OFFSET;
        g_identifierType = kUniqueIdentifier;
        g_trackInformationBitmask = getUIDTrackInfoPacketPayload.trackInformationBitmask;
        g_trackInformationBitmaskBackUp = getUIDTrackInfoPacketPayload.trackInformationBitmask;
        g_trackInfoAppCallback = rxCallback;

        txStatus = iap_commands_transmit_command(&txCommandData);

    }else
    {
        txStatus = kInvalidParameter;
    }

    return txStatus;
}

/*!
 * @brief Internal Extended Interface callback that is called by commands layer each time a packet
 *        of RetDBTrackInfo or RetUIDTrackInfo has been received, the function handles the logic 
 *        to store the packet's payload into the corresponding buffer and after the information of 
 *        a whole song has arrived an application callback is called so the application layer can 
 *        access to the data 
 * @param receivedPayload       Pointer to the received payload
 * @param receivedPayloadSize   Size of the received payload
 * @return A boolean value that signals to commands layer if the Tx command can be released
 */
static uint8_t iap_extended_interface_get_track_information_callback(uint8_t* receivedPayload,
                                                                     uint16_t receivedPayloadSize)
{
    bool readyToRelease = false;
    
    if((receivedPayload != NULL) && (receivedPayloadSize != 0x00))
    {
        uint8_t* destinationPointer;
        uint16_t destinationSize;
        uint8_t bitPositionNumber = 0;
        bool payloadCopyDone = false;
        
        /* For the first packet saves the track identifier or database index */
        if(g_trackInformationBitmask == g_trackInformationBitmaskBackUp)
        {
            g_trackInformation.identifierBuffer[0] = receivedPayload[0];
            g_trackInformation.identifierBuffer[1] = receivedPayload[1]; 
            g_trackInformation.identifierBuffer[2] = receivedPayload[2];
            g_trackInformation.identifierBuffer[3] = receivedPayload[3];    
            
            if(g_identifierType == kUniqueIdentifier)
            {
                g_trackInformation.identifierBuffer[4] = receivedPayload[4];
                g_trackInformation.identifierBuffer[5] = receivedPayload[5]; 
                g_trackInformation.identifierBuffer[6] = receivedPayload[6];
                g_trackInformation.identifierBuffer[7] = receivedPayload[7];    
            }
        }
    
        /* Extract the track information type from the received payload */
        bitPositionNumber = receivedPayload[g_dataOffset];
        
        /* Assign where to save the received payload, using the track information type as 
         * an index to an array of pointers */
        destinationPointer = g_trackInformation.trackInformationPointers[bitPositionNumber];
        
        /* Clear the corresponding bit of the bit mask to signal that the information has already been saved */
        g_trackInformationBitmask &= ~(1 << bitPositionNumber);
        
        /* If the receiving packet is the lyrics packet, use the multisection logic, otherwise just    
         * copy the received payload into the corresponding buffer (album name, artist name,
         * composer name, etc.) */
        if(bitPositionNumber != TRACK_INFO_LYRICS_INDEX)
        {    
            destinationSize = TRACK_INFO_DEFAULT_BUFFER_SIZE;
            
            /* Adjust the received paylod in order to ignore the first bytes of the
             *  payload that contains the track index */
            receivedPayload += (g_dataOffset + 1);
            receivedPayloadSize -= (g_dataOffset + 1);
            
            iap_commands_copy_payload(receivedPayload, receivedPayloadSize,
                                        destinationPointer, destinationSize);
            payloadCopyDone = true;
        }else
        {
            uint16_t totalReceivedPayloadSize;
            uint16_t sectionMax; 
            uint16_t sectionCurrent;
            copy_multisection_status_t multisectionStatus;
            
            /* Get the sectCur and sectMax fields from the received payload */
            sectionCurrent = (((uint16_t) receivedPayload[g_dataOffset + 1]) << 8) | 
                              ((uint16_t) receivedPayload[g_dataOffset + 2]);
            
            sectionMax = (((uint16_t) receivedPayload[g_dataOffset + 3]) << 8) |
                          ((uint16_t) receivedPayload[g_dataOffset + 4]);
            
            /* Copy the received payload into the lyrics buffer, using the multisection logic */
            destinationSize = TRACK_INFO_LYRICS_BUFFER_SIZE;
            multisectionStatus = iap_commands_copy_multisection_payload(sectionCurrent, sectionMax, 
                                                                        g_dataOffset + 5, 
                                                                        receivedPayload, receivedPayloadSize,
                                                                        destinationPointer,
                                                                        destinationSize,
                                                                        &totalReceivedPayloadSize);
            if(multisectionStatus != kMultisectionIncomplete)
            {
    #if(LYRIC_OF_SONG_CURRENTLY_PLAYING_MASK == ENABLE)
                g_trackInformation.lyricsReceivedSize = totalReceivedPayloadSize;
    #endif
                payloadCopyDone = true;
            }
        }
        
        if(payloadCopyDone)
        {
            /* Check if all the information of the track has been received */
            if(g_trackInformationBitmask == 0)
            {
                /* Restore the bit mask in order to use it again in case  the 
                 * command requested information from more than one track*/
                g_trackInformationBitmask = g_trackInformationBitmaskBackUp;
                
                /* Check if the information of all the tracks has been received*/
                if((--g_trackCount) == 0)
                {
                    /* Signal to commands layer that all the requested information
                     * has been received and can now proceed to release the Tx command*/
                    readyToRelease = true;
                }
                
                /* Jump to the application callback */
                g_trackInfoAppCallback(&g_trackInformation, kIAPAckSuccess);
            }
        }
    }else
    {
        readyToRelease = true;
        
        /* Jump to the application callback */
        g_trackInfoAppCallback(&g_trackInformation, kIAPAckBadParameter);
    }
    
    return (readyToRelease);
}

/* See ExtendedInterfaceLingo.h for documentation of this function */
command_transmission_status_t eil_retrieve_categorized_database_records(iap_eil_retrieve_db_records_t
                                                                        retDBRecordsPacket,
                                                                        uint8_t* rxPayLoad,
                                                                        uint16_t rxPayLoadSize,
                                                                        uint16_t sizeForEachRecord,
                                                                        void (*rxCallback)(uint8_t))
{
    command_transmission_status_t txStatus;
    
    /* Validate the parameters */
    if(((sizeForEachRecord*retDBRecordsPacket.databaseRecordReadCount) <= rxPayLoadSize) &&
       (retDBRecordsPacket.databaseRecordReadCount < MAX_CATEGORIZED_DB_RECORDS_TO_REQUEST) &&
       (rxCallback != NULL))
    {
        uint8_t databaseRecordReadCount = retDBRecordsPacket.databaseRecordReadCount;
        iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                    (uint8_t*) &retDBRecordsPacket,
                                                    sizeof(iap_eil_retrieve_db_records_t),
                                                    0,
                                                    kQuestionPacket,
                                                    kExtendedInterfaceLingo,
                                                    kExtendedInterfaceRetrieveCategorizedDatabaseRecords,
                                                    (kCommandIsWaitingForResponse | kCommandWithMultipleResponses)},
                                                    (void (*)(uint8_t)) iap_extended_interface_retrieve_categorized_database_records_callback,
                                                    NULL,
                                                    NULL,
                                                    0,
                                                    kExtendedInterfaceReturnCategorizedDatabaseRecord};
        
        /* Backup user callback, that will be called once all the records names have been received */
        g_retrieveDBRecordAppCallback = rxCallback;
        g_categorizedRecords.sizeForEachRecord = sizeForEachRecord;
        g_categorizedRecords.numberOfRequestedRecords = databaseRecordReadCount;
        
        /* Distribute the application buffer to save the requested records names*/
        while((databaseRecordReadCount--))
        {
            g_categorizedRecords.recordNames[databaseRecordReadCount] =  rxPayLoad + 
                                                          sizeForEachRecord*databaseRecordReadCount;
        }

#if(BIG_ENDIAN_CORE == _FALSE_)
        BYTESWAP32(retDBRecordsPacket.databaseRecordReadCount,
                   retDBRecordsPacket.databaseRecordReadCount);
        
        BYTESWAP32(retDBRecordsPacket.databaseRecordStartIndex,
                   retDBRecordsPacket.databaseRecordStartIndex);
#endif

        txStatus = iap_commands_transmit_command(&txCommandData);
    }else
    {
        txStatus = kInvalidParameter;
    }

    return txStatus;

}

/*!
 * @brief Internal Extended Interface callback that is called by commands layer each time a packet 
 *          of ReturnCategorizedDatabaseRecord
 *        
 * @param receivedPayload       Pointer to the received payload
 * @param receivedPayloadSize   Size of the received payload
 * @return A boolean value that signals to commands layer if the Tx command can be released
 */
static uint8_t iap_extended_interface_retrieve_categorized_database_records_callback(uint8_t* receivedPayload,
                                                                                     uint16_t receivedPayloadSize)
{
   bool readyToRelease = false;
   if((receivedPayload != NULL) && (receivedPayloadSize != 0x00))
   {
      static uint8_t s_recordCounter = 0; 
      uint8_t* destinationPayload;

      
      /* Assign the pointer where the current record name will be saved */
      destinationPayload = g_categorizedRecords.recordNames[s_recordCounter];
      
      /* Skip the database index (First 4 bytes of the received payload)*/
      receivedPayload += DATABASE_INDEX_OFFSET;
      receivedPayloadSize -= DATABASE_INDEX_OFFSET;
      
      /* Save the record name */
      iap_commands_copy_payload(receivedPayload, receivedPayloadSize,
                                  destinationPayload, g_categorizedRecords.sizeForEachRecord);
      
      /* Check if the last packet has been received */
      if((++s_recordCounter) == g_categorizedRecords.numberOfRequestedRecords)
      {
          s_recordCounter = 0;
          
          /* Signal to commands layer that all the requested information
             has been received and can now proceed to release the Tx command*/
          readyToRelease = true;
          
          /* If all the records have been received then jump to the application callback */
          g_retrieveDBRecordAppCallback(kIAPAckSuccess);
      }
   }else
   {
       readyToRelease = true;
       /* If the received response is not the expected information */
       g_retrieveDBRecordAppCallback(kIAPAckBadParameter);
   }
 
   return (readyToRelease);
}

/* See ExtendedInterfaceLingo.h for documentation of this function */
command_transmission_status_t eil_get_indexed_playing_track_info(eil_get_indexed_playing_track_info_t
                                                                 getIndexedTrackPacket,
                                                                 void (*rxCallback)(iap_eil_ret_indexed_playing_track_info_t*))
{
    command_transmission_status_t txStatus;
    
    if(rxCallback != NULL)
    {
        iap_commands_tx_command_t txCommandData = {{k30sTimeout,                
                                                    (uint8_t*) &getIndexedTrackPacket, 
                                                    sizeof(eil_get_indexed_playing_track_info_t),
                                                    0,
                                                    kQuestionPacket,
                                                    kExtendedInterfaceLingo,
                                                    kExtendedInterfaceGetIndexedPlayingTrackInfo,
                                                    (kCommandIsWaitingForResponse | kCommandWithMultipleResponses)},
                                                    (void (*)(uint8_t))iap_extended_interface_get_indexed_playing_track_info,
                                                    NULL,
                                                    NULL,
                                                    0,
                                                    kExtendedInterfaceReturnIndexPlayingTrackinfo};
                                                    
        g_indexedPlayingTrackInfoAppCallback = rxCallback;

#if(BIG_ENDIAN_CORE == _FALSE_)
        BYTESWAP16(getIndexedTrackPacket.chapterIndex, getIndexedTrackPacket.chapterIndex);
        BYTESWAP32(getIndexedTrackPacket.trackIndex, getIndexedTrackPacket.trackIndex);
#endif

        txStatus = iap_commands_transmit_command(&txCommandData);
    }
    else
    {
        txStatus = kInvalidParameter;
    }
    
    return(txStatus);
}

static uint8_t iap_extended_interface_get_indexed_playing_track_info(uint8_t* receivedPayload,
                                                                     uint16_t receivedPayloadSize)
{
    uint8_t* destinationPointer;
    uint16_t destinationSize;
    uint16_t totalReceivedPayloadSize;

    uint8_t trackInfoType;
    uint8_t packetInfoBits;

    copy_multisection_status_t multisectionStatus;
    bool readyToRelease = false;

    static uint8_t sectionMaxIndicator = 2;
    static uint8_t sectionCurrentIndicator;
    static uint16_t packetIndex;

    destinationPointer = &g_indexedPlayingTrackInfo.trackInformation[0];
    destinationSize = INDEXED_PLAYING_TRACK_INFO_BUFFER_SIZE;
    trackInfoType = receivedPayload[0];
    g_dataOffset = TRACK_INFO_INDEX_OFFSET;

    if(!((kTrackDescription == trackInfoType) || (kTrackSongLyric == trackInfoType)))
    {
        iap_commands_copy_payload((receivedPayload + 1), (receivedPayloadSize - 1),
                (uint8_t*)&g_indexedPlayingTrackInfo.trackInformation,
                (uint16_t)sizeof(g_indexedPlayingTrackInfo.trackInformation));
        readyToRelease = true;
    }
    else
    {
        /* for lyric or description data */
        packetInfoBits = receivedPayload[1];
        if(packetInfoBits & (1 << kMultiplePacketsEnabled))
        {
            packetIndex = ((uint16_t)(receivedPayload[3] << 8) | (uint16_t)(receivedPayload[2]));
            if(!packetIndex)
            {
                /* for the first packet */
                sectionCurrentIndicator = 0;
            }
            else
            {
                if(packetInfoBits & (1 << kLastMultiplePacket))
                {
                    /* for the last packet, set section current to 2 to indicate in multisection
                     * copy function that is the last packet to copy 
                     * (sectionCurrentIndicator == sectionMaxIndicator) */
                    sectionCurrentIndicator = 2;
                }
                else
                {
                    /* for all packets that are neither the first nor the last,
                     * set the section current to 1 while the received packet isn't the last*/
                    sectionCurrentIndicator = 1;
                }
            }
            multisectionStatus = iap_commands_copy_multisection_payload(sectionCurrentIndicator, 
                                                                        sectionMaxIndicator,
                                                                        g_dataOffset, 
                                                                        receivedPayload, 
                                                                        receivedPayloadSize,
                                                                        destinationPointer,
                                                                        destinationSize,
                                                                        &totalReceivedPayloadSize);
            
            /* check if multisection packet has been stored completely */
            if(multisectionStatus != kMultisectionIncomplete)
            {
                readyToRelease = true;
            }
        }
        else
        {
            /* save only the track information */
            iap_commands_copy_payload((receivedPayload + 4), (receivedPayloadSize - 4),
                                      destinationPointer, destinationSize);
            /* this command is complete and ready to release */
            readyToRelease = true;
        }
    }

    if(readyToRelease)
    {
        /* save the track info type into the structure returns to the app */
        g_indexedPlayingTrackInfo.trackInfoType = trackInfoType;
        /* jump to applicaiton callback */
        g_indexedPlayingTrackInfoAppCallback(&g_indexedPlayingTrackInfo);
    }

    return(readyToRelease);
}

/* See ExtendedInterfaceLingo.h for documentation of this function */
command_transmission_status_t eil_get_artwork_times(iap_eil_track_identifier_t trackIdentifierType,
                                                    uint8_t* trackIdentifier,
                                                    uint16_t formatID,
                                                    uint16_t artworkIndex,
                                                    uint16_t artworkCount,
                                                    uint8_t* rxPayLoad,
                                                    uint16_t rxPayLoadSize,
                                                    void (*rxCallback)(uint8_t))
{
    uint8_t localTxBuffer[15];
    uint8_t localTxBufferSize = sizeof(localTxBuffer);
    uint8_t positionCounter = 0;
    
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                localTxBuffer,
                                                localTxBufferSize,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetArtworkTimes,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceRetArtworkTimes};

#if(BIG_ENDIAN_CORE == _FALSE_)
    localTxBuffer[positionCounter] = trackIdentifierType;
    
    /* for 8 bytes: save the UID */
    if(kUID == trackIdentifierType)
    {
        for(positionCounter = kOneByte;
                positionCounter <= UNIQUE_TRACK_IDENTIFIER_SIZE;
                positionCounter++)
        {
            localTxBuffer[positionCounter] =
                    trackIdentifier[positionCounter-1];
        }
    }
    /* for 4 bytes: save index of the playback engine queue or index of the database selection */
    else
    {
        for(positionCounter = kOneByte;
                positionCounter <= kFourBytes;
                positionCounter++)
        {
            localTxBuffer[positionCounter] =
                    trackIdentifier[kFourBytes - positionCounter];
        }
        localTxBufferSize -= kFourBytes;
    }
    
    localTxBuffer[positionCounter]      = (uint8_t)(formatID >> 8);
    localTxBuffer[positionCounter + 1]  = (uint8_t)(formatID);
    localTxBuffer[positionCounter + 2]  = (uint8_t)(artworkIndex >> 8);
    localTxBuffer[positionCounter + 3]  = (uint8_t)(artworkIndex);
    localTxBuffer[positionCounter + 4]  = (uint8_t)(artworkCount >> 8);
    localTxBuffer[positionCounter + 5]  = (uint8_t)(artworkCount);
#endif
    
    txCommandData.txFrameData.payloadSize = localTxBufferSize;
    
    return iap_commands_transmit_command(&txCommandData);

}

/* See ExtendedInterfaceLingo.h for documentation of this function */
command_transmission_status_t eil_get_artwork_data(iap_eil_track_identifier_t trackIdentifierType,
                                                   uint8_t* trackIdentifier,
                                                   uint16_t formatID,
                                                   uint32_t timeOffset,
                                                   uint8_t* rxPayLoad,
                                                   uint16_t rxPayLoadSize,
                                                   uint16_t* ptrToSaveTotalPayloadSize,
                                                   void (*rxCallback)(uint8_t))
{
    uint8_t localTxBuffer[15];
    uint8_t localTxBufferSize = sizeof(localTxBuffer);
    uint8_t positionCounter = 0;
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                localTxBuffer,
                                                localTxBufferSize,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetArtworkData,
                                                (kCommandIsWaitingForResponse | kCommandWithMultisectionResponse)},
                                                rxCallback,
                                                ptrToSaveTotalPayloadSize,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceRetArtworkData};

    localTxBuffer[positionCounter] = trackIdentifierType;

    /* For 8 bytes: save the UID */
    if(kUID == trackIdentifierType)
    {
        for(positionCounter = kOneByte;
            positionCounter <= UNIQUE_TRACK_IDENTIFIER_SIZE;
            positionCounter++)
        {
            localTxBuffer[positionCounter] =
                    trackIdentifier[positionCounter - 1];
        }
    }
    /* For 4 bytes: save index of the playback engine queue or index of the database selection */
    else
    {
        for(positionCounter = kOneByte;
            positionCounter <= kFourBytes;
            positionCounter++)
        {
            localTxBuffer[positionCounter] =
                    trackIdentifier[kFourBytes - positionCounter];
        }
        localTxBufferSize -= kFourBytes;
    }

#if(BIG_ENDIAN_CORE == _FALSE_)
    localTxBuffer[positionCounter]      = (uint8_t)(formatID >> 8);
    localTxBuffer[positionCounter + 1]  = (uint8_t)(formatID);
    localTxBuffer[positionCounter + 2]  = (uint8_t)(timeOffset >> 24);
    localTxBuffer[positionCounter + 3]  = (uint8_t)(timeOffset >> 16);
    localTxBuffer[positionCounter + 4]  = (uint8_t)(timeOffset >> 8);
    localTxBuffer[positionCounter + 5]  = (uint8_t)(timeOffset);
#else
    
#endif
    txCommandData.txFrameData.payloadSize = localTxBufferSize;
    
    return iap_commands_transmit_command(&txCommandData);
}


uint32_t get_artwork_times_offset_of_record(uint8_t* artworkTimesPacket, 
                                            uint8_t trackIdentifierType,
                                            uint8_t artworkIndex)
{
    uint32_t* timeOffset;
    uint8_t dataOffset;
    
    if(kUID == trackIdentifierType)
    {
        dataOffset = kEightBytes;
    }
    else
    {
        dataOffset = kFourBytes;
    }
    
    /* Set the offset from where will be changed the time offset */
    timeOffset = (uint32_t*)&artworkTimesPacket
            [(dataOffset + (SIZE_OF_TIMEOFFSET_RECORDS_IN_BYTES * artworkIndex))];
    
    return BYTESWAP32(*timeOffset, *timeOffset);
}

uint16_t get_arwork_data_image_width(uint8_t* artworkData, uint8_t trackIdentifierType)
{
    uint16_t* imageWidth;
    
    if(kUID == trackIdentifierType)
    {
        imageWidth = (uint16_t*)&artworkData[10];
    }
    else
    {
        imageWidth = (uint16_t*)&artworkData[6];
    }
    return BYTESWAP16(*imageWidth, *imageWidth);
}

uint16_t get_artwork_data_inset_rectangle_top_left_point_x_value(uint8_t* artworkData,
                                                                 uint8_t trackIdentifierType)
{
    uint16_t* topLeftXValue;
    
    if(kUID == trackIdentifierType)
    {
        topLeftXValue = (uint16_t*)&artworkData[12];
    }
    else
    {
        topLeftXValue = (uint16_t*)&artworkData[8];
    }
    return BYTESWAP16(*topLeftXValue, *topLeftXValue);
}

uint16_t get_artwork_data_inset_rectangle_top_left_point_y_value(uint8_t* artworkData, 
                                                                 uint8_t trackIdentifierType)
{
    uint16_t* topLeftYValue;
    
    if(kUID == trackIdentifierType)
    {
        topLeftYValue = (uint16_t*)&artworkData[14];
    }
    else
    {
        topLeftYValue = (uint16_t*)&artworkData[10];
    }
    return BYTESWAP16(*topLeftYValue, *topLeftYValue);
}

uint16_t get_artwork_data_inset_rectangle_bottom_right_point_x_value(uint8_t* artworkData, 
                                                                     uint8_t trackIdentifierType)
{
    uint16_t* bottomRigthXValue;
    
    if(kUID == trackIdentifierType)
    {
        bottomRigthXValue = (uint16_t*)&artworkData[16];
    }
    else
    {
        bottomRigthXValue = (uint16_t*)&artworkData[12];
    }
    return BYTESWAP16(*bottomRigthXValue, *bottomRigthXValue);
}

uint16_t get_artwork_data_inset_rectangle_bottom_right_point_y_value(uint8_t* artworkData,
                                                                     uint8_t trackIdentifierType)
{
    uint16_t* bottomRigthYValue;
    
    if(kUID == trackIdentifierType)
    {
        bottomRigthYValue = (uint16_t*)&artworkData[18];
    }
    else
    {
        bottomRigthYValue = (uint16_t*)&artworkData[14];
    }
    return BYTESWAP16(*bottomRigthYValue, *bottomRigthYValue);
}

uint32_t get_artwork_data_row_size(uint8_t* artworkData, uint8_t trackIdentifierType)
{
    uint32_t* rowSizeInBytes;
    
    if(kUID == trackIdentifierType)
    {
        rowSizeInBytes = (uint32_t*)&artworkData[20];
    }
    else
    {
        rowSizeInBytes = (uint32_t*)&artworkData[16];
    }
    return BYTESWAP32(*rowSizeInBytes, *rowSizeInBytes);
}


/*************************************************************************************************
* EOF
*************************************************************************************************/
