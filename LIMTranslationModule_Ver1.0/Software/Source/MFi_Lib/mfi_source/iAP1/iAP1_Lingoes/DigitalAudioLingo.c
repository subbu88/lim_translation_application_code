/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

/*************************************************************************************************
*                                      Includes Section                        
*************************************************************************************************/
#include "DigitalAudioLingo.h"
#include "GeneralLingo.h"

/*************************************************************************************************
*                                   Defines & Macros Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                       Typedef Section                        
*************************************************************************************************/
#pragma pack (push,1)

/** Track New Audio Attributes (Lingo = 0x0A, Cmd = 0x04) Structure */ 
typedef struct
{
    uint32_t newSampleRate;
    uint32_t newSoundCheckValue;
    uint32_t newTrackVolume;
}iap_track_new_audio_attributes_t;

/** Set FID Token Values - Accessory Digital Audio Sample Rates Token 
 * (Lingo = 0x00, Cmd = 0x39) Enumeration */
typedef enum
{
    kDigitalSampleRateBytesOffset    =   3
}iap_gl_accessory_audio_sample_rates_offsets_t;

#pragma pack (pop)

/*************************************************************************************************
*                                  Function Prototypes Section                 
*************************************************************************************************/

/* Digital Audio Lingo asynchronous commands **********************************************************/

/*! Digital Audio lingo function to get supported sample rates from the accessory */
static void  s_dal_async_audio_get_sample_rate_caps(iap_interface_buffer_rx_frame_t* rxFrame);

/*! Digital Audio lingo function to send the new sampling frequency and sound check values for a track*/
static void  s_dal_async_track_new_audio_attributes(iap_interface_buffer_rx_frame_t* rxFrame);

/* End of Digital Audio Lingo asynchronous commands ***************************************************/   

/*************************************************************************************************
*                                   Global Constants Section                   
*************************************************************************************************/

/*! Digital Audio Lingo Asynchronous Commands*/
const uint8_t kDigitalAudioLingoAsynchronousCommands
        [NUMBER_OF_DIGITALAUDIO_ASYNCHRONOUS_COMMANDS] =
{
        kDigitalAudioGetAccessorySampleRateCaps,                    /*0x02*/
        kDigitalAudioTrackNewAudioAttributes                        /*0x04*/
};

/*! Accessory Digital Audio Sample Rate token array */
extern const uint8_t s_kFidAccDigitalAudioSampleRateTokenValues[ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_SIZE + 1];

/*************************************************************************************************
*                                   Static Constants Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                   Global Variables Section                   
*************************************************************************************************/

/*! Array of Digital Audio Lingo functions */
 void (* kDigitalAudioLingoAsynchronousFunctions
        [NUMBER_OF_DIGITALAUDIO_ASYNCHRONOUS_COMMANDS])(iap_interface_buffer_rx_frame_t* rxFrame) =
{
        s_dal_async_audio_get_sample_rate_caps,
        s_dal_async_track_new_audio_attributes
};

/*************************************************************************************************
*                                   Static Variables Section                   
*************************************************************************************************/

/* Asynchronous functions Digital Audio Lingo */
static iap_commands_asynchronous_callback_t s_iap_cmd_dal_acc_sample_rate_caps = NULL;

static iap_commands_asynchronous_callback_t s_iap_cmd_dal_track_new_attributes = NULL;
/*************************************************************************************************
*                                      Functions Section                       
*************************************************************************************************/

static void  s_dal_async_audio_get_sample_rate_caps(iap_interface_buffer_rx_frame_t* rxFrame)
{
    g_iapCommandsTransactionID = rxFrame->transactionId;
    
    dal_ret_accessory_sample_rate_caps((uint8_t*)(&s_kFidAccDigitalAudioSampleRateTokenValues[kDigitalSampleRateBytesOffset]),  /* Tx Payload */
                                   (uint16_t)(ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_SIZE - kDigitalSampleRateBytesOffset),       /* Tx Payload size */
                                   NULL,                                                           /* Rx Payload ptr*/
                                   0,                                                              /* Rx Payload size */
                                   g_iapCommandsTransactionID,                  
                                   NULL);                                       /* Callback routine */
    
    if (NULL != s_iap_cmd_dal_acc_sample_rate_caps)
    {
        s_iap_cmd_dal_acc_sample_rate_caps(rxFrame);
    }
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

static void  s_dal_async_track_new_audio_attributes(iap_interface_buffer_rx_frame_t* rxFrame)
{
    iap_track_new_audio_attributes_t* audioAttributes = (iap_track_new_audio_attributes_t*)(rxFrame->payload);
    uint8_t AckResponse[2]; 
    g_iapCommandsTransactionID = rxFrame->transactionId;
    
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(audioAttributes->newSampleRate, audioAttributes->newSampleRate);  
    BYTESWAP32(audioAttributes->newSoundCheckValue, audioAttributes->newSoundCheckValue);
    BYTESWAP32(audioAttributes->newTrackVolume, audioAttributes->newTrackVolume);
#endif
    
    AckResponse[0] = kIAPAckSuccess;
    AckResponse[1] = kDigitalAudioTrackNewAudioAttributes;
    
    dal_accessory_ack(AckResponse, sizeof(AckResponse), g_iapCommandsTransactionID);
    if (NULL != s_iap_cmd_dal_track_new_attributes)
    {
        s_iap_cmd_dal_track_new_attributes(rxFrame); 
    }
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}
 

/* See DigitalAudioLingo.h for documentation of this function. */
uint8_t set_dal_asynchronous_command_callback(uint8_t callbackId, iap_commands_asynchronous_callback_t hostFunctionAddress)
{
    uint8_t returnValue = _ERROR_; 
    if (NULL != hostFunctionAddress)
    {
       returnValue = _OK_;
       switch(callbackId)
       {
           case (kDigitalAudioGetAccessorySampleRateCaps):
                s_iap_cmd_dal_acc_sample_rate_caps = hostFunctionAddress;
               break;
           
           case (kDigitalAudioTrackNewAudioAttributes):
                s_iap_cmd_dal_track_new_attributes = hostFunctionAddress;
               break;
               
           default: 
               returnValue = _ERROR_;
               break;
       }
    }
    
    return (returnValue); 
}
 
/*************************************************************************************************
* EOF
*************************************************************************************************/
