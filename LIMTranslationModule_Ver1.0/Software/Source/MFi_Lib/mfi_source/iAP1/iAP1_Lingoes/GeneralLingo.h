/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

#ifndef GENERALLINGO_H_
#define GENERALLINGO_H_


/*************************************************************************************************
*                                        Includes Section                              
*************************************************************************************************/
#include "iAP_Commands.h"

/*! @addtogroup iAP1_Lingoes iAP1 Lingoes
*   @{ 

*  @addtogroup GeneralLingo General Lingo
*  @brief  This file contains the specific types (enums and structures) and macros, the asynchronous 
*          commands functions, the inline functions used to transmit commands from the General 
*          lingo and the inline functions used to access to the received data from specific
*          commands.
*          
*  @details The specific General Lingo asynchronous commands functions have a one to one 
*           relation with the asynchronous commands from this lingo so these functions are called 
*           once their corresponding asynchronous command is received. The inline functions can be 
*           used as a mean to easily transmit an specific command from this lingo.
*  @{  
*/

/*************************************************************************************************
*                                        Defines & Macros Section                             
*************************************************************************************************/
/*!
*  @addtogroup GeneralLingoDefs General Lingo Macros and Defines
*  @{  
*/

/*! Amount of Asynchronous Commands from General Lingo */
#define NUMBER_OF_GENERALLINGO_ASYNCHRONOUS_COMMANDS    0x0B

/*! Amount of Commands from General Lingo */
#define IAP_GENERALLINGO_COMMANDS_COUNT                 0x6C

/*! Accessory RF Certification Token Configuration. See (Lingo = 0x00, Cmd = 0x39) }
 * FID Tokens, RF Certification Declarations */
#define RF_CLASS1_MASK                                  (_TRUE_)
#define RF_CLASS2_MASK                                  (_FALSE_)
#define RF_CLASS4_MASK                                  (_TRUE_)
#define RF_CLASS5_MASK                                  (_FALSE_)

/*! Ipod Preferences Token Configuration. See (Lingo = 0x00, Cmd = 0x2A) */
#define DEFAULT_VIDEO_OUT                               (kVideoOutOff)
#define DEFAULT_SCREEN_CONFIG                           (kScreenConfigFillEntireScreen)
#define DEFAULT_VIDEO_SIGNAL_FORMAT                     (VIDEO_SIGNAL_FORMAT_NTSC)
#define DEFAULT_LINE_OUT_USAGE                          (kLineOutUsageNotUsed)
#define DEFAULT_VIDEO_OUT_CONNECTION                    (kVideoOutConnectionComposite)
#define DEFAULT_CLOSED_CAPTIONING                       (kClosedCaptioningOn)
#define DEFAULT_VIDEO_ASPECT_RATIO                      (kVideoAspectRatioFullscreen)
#define DEFAULT_SUBTITLES                               (kSubtitlesOn)
#define DEFAULT_VIDEO_ALTERNATE_AUDIO                   (kVideoAlternateAudioOn)
#define DEFAULT_PAUSE_ON_POWER_REMOVAL                  (kPauseOnPowerRemovalOn)
#define DEFAULT_VOICE_OVER_PREF                         (kVoiceOverPrefOff)
#define DEFAULT_ASSISTIVE_TOUCH                         (kAssistiveTouchOn)

/*! Digital Audio Sample Rate Token Configuration. See (Lingo = 0x00, Cmd = 0x39) */
#define DIGITAL_SAMPLE_RATE_32KHZ_MASK                  (_TRUE_)
#define DIGITAL_SAMPLE_RATE_44_1KHZ_MASK                (_TRUE_)
#define DIGITAL_SAMPLE_RATE_48KHZ_MASK                  (_TRUE_)
#define DIGITAL_SAMPLE_RATE_96KHZ_MASK                  (_FALSE_)
#define DIGITAL_SAMPLE_RATE_192KHZ_MASK                 (_FALSE_)

/*! Digital Audio Video Delay Token Configuration. See (Lingo = 0x00, Cmd = 0x39) */
#define DEFAULT_AUDIO_VIDEO_DELAY                       (50)

#define EA_PROTOCOL_TOKEN_OFFSET                        23
#define BUNDLE_SEED_ID_PREF_TOKEN_OFFSET                24
#define EA_PROTOCOL_METADATA_TOKEN_OFFSET               25

#define IDENTIFY_TOKEN_BIT_MASK                         (1<<0)
#define ACC_CAPABILITY_TOKEN_BIT_MASK                   (1<<1)
#define ACC_INFO_NAME_TOKEN_BIT_MASK                    (1<<2)
#define ACC_INFO_FIRMWARE_VERSION_TOKEN_BIT_MASK        (1<<3)
#define ACC_INFO_HARDWARE_VERSION_TOKEN_BIT_MASK        (1<<4)
#define ACC_INFO_MANUFACTURER_TOKEN_BIT_MASK            (1<<5)
#define ACC_INFO_MODEL_NUMBER_TOKEN_BIT_MASK            (1<<6)
#define ACC_INFO_SERIAL_NUMBER_TOKEN_BIT_MASK           (1<<7)
#define ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_BIT_MASK        (1<<8)
#define ACC_INFO_STATUS_TOKEN_BIT_MASK                  (1<<9)
#define ACC_INFO_RF_CERTIFICATION_TOKEN_BIT_MASK        (1<<10)
#define IPOD_PREF_VIDEO_OUT_SETTINGS_BIT_MASK           (1<<11)
#define IPOD_PREF_SCREEN_CONFIGURATION_BIT_MASK         (1<<12)
#define IPOD_PREF_VIDEO_SIGNAL_FORMAT_BIT_MASK          (1<<13)
#define IPOD_PREF_LINE_OUT_USAGE_BIT_MASK               (1<<14)
#define IPOD_PREF_VIDEO_OUT_CONNECTION_BIT_MASK         (1<<15)
#define IPOD_PREF_CLOSED_CAPTIONING_BIT_MASK            (1<<16)
#define IPOD_PREF_VIDEO_ASPECT_RATIO_BIT_MASK           (1<<17)
#define IPOD_PREF_SUBTITLES_BIT_MASK                    (1<<18)
#define IPOD_PREF_VIDEO_ALTERNATE_AUDIO_BIT_MASK        (1<<19)
#define IPOD_PREF_PAUSE_ON_POWER_REMOVAL_BIT_MASK       (1<<20)
#define IPOD_PREF_VOICE_OVER_BIT_MASK                   (1<<21)
#define IPOD_PREF_ASSISTIVE_TOUCH_BIT_MASK              (1<<22)
#define EA_PROTOCOL_TOKEN_BIT_MASK                      (1<<23)
#define BUNDLE_SEED_ID_PREF_TOKEN_BIT_MASK              (1<<24)
#define EA_PROTOCOL_METADATA_TOKEN_BIT_MASK             (1<<25)
#define ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_BIT_MASK    (1<<26)
#define ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_BIT_MASK    (1<<27)

/** @warning Some apple devices indicate they support Audio Sample Token 
 *  but they don't send the ACKFID for this command, leading to errors and warnings on the ATS 
 *  @todo Need to submit bug report to Apple */
#define IAP_CMD_ENABLED_FID_TOKENS_MASK     ((IDENTIFY_TOKEN_BIT_MASK * IDENTIFY_TOKEN_MASK) \
        | (ACC_CAPABILITY_TOKEN_BIT_MASK * ACC_CAPABILITY_TOKEN_MASK) \
        | (ACC_INFO_NAME_TOKEN_BIT_MASK * ACC_INFO_NAME_TOKEN_MASK) \
        | (ACC_INFO_FIRMWARE_VERSION_TOKEN_BIT_MASK * ACC_INFO_FIRMWARE_VERSION_TOKEN_MASK) \
        | (ACC_INFO_HARDWARE_VERSION_TOKEN_BIT_MASK * ACC_INFO_HARDWARE_VERSION_TOKEN_MASK) \
        | (ACC_INFO_MANUFACTURER_TOKEN_BIT_MASK * ACC_INFO_MANUFACTURER_TOKEN_MASK) \
        | (ACC_INFO_MODEL_NUMBER_TOKEN_BIT_MASK * ACC_INFO_MODEL_NUMBER_TOKEN_MASK) \
        | (ACC_INFO_SERIAL_NUMBER_TOKEN_BIT_MASK * ACC_INFO_SERIAL_NUMBER_TOKEN_MASK) \
        | (ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_BIT_MASK * ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_MASK) \
        | (ACC_INFO_STATUS_TOKEN_BIT_MASK * ACC_INFO_STATUS_TOKEN_MASK) \
        | (ACC_INFO_RF_CERTIFICATION_TOKEN_BIT_MASK * ACC_INFO_RF_CERTIFICATION_TOKEN_MASK) \
        | (IPOD_PREF_VIDEO_OUT_SETTINGS_BIT_MASK * IPOD_PREF_VIDEO_OUT_SETTINGS_MASK) \
        | (IPOD_PREF_SCREEN_CONFIGURATION_BIT_MASK * IPOD_PREF_SCREEN_CONFIGURATION_MASK) \
        | (IPOD_PREF_VIDEO_SIGNAL_FORMAT_BIT_MASK * IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK) \
        | (IPOD_PREF_LINE_OUT_USAGE_BIT_MASK * IPOD_PREF_LINE_OUT_USAGE_MASK) \
        | (IPOD_PREF_VIDEO_OUT_CONNECTION_BIT_MASK * IPOD_PREF_VIDEO_OUT_CONNECTION_MASK) \
        | (IPOD_PREF_CLOSED_CAPTIONING_BIT_MASK * IPOD_PREF_CLOSED_CAPTIONING_MASK) \
        | (IPOD_PREF_VIDEO_ASPECT_RATIO_BIT_MASK * IPOD_PREF_VIDEO_ASPECT_RATIO_MASK) \
        | (IPOD_PREF_SUBTITLES_BIT_MASK * IPOD_PREF_SUBTITLES_MASK) \
        | (IPOD_PREF_VIDEO_ALTERNATE_AUDIO_BIT_MASK * IPOD_PREF_VIDEO_ALTERNATE_AUDIO_MASK) \
        | (IPOD_PREF_PAUSE_ON_POWER_REMOVAL_BIT_MASK * IPOD_PREF_PAUSE_ON_POWER_REMOVAL_MASK) \
        | (IPOD_PREF_VOICE_OVER_BIT_MASK * IPOD_PREF_VOICE_OVER_MASK) \
        | (IPOD_PREF_ASSISTIVE_TOUCH_BIT_MASK * IPOD_PREF_ASSISTIVE_TOUCH_MASK) \
        | (EA_PROTOCOL_TOKEN_BIT_MASK * EA_PROTOCOL_TOKEN_MASK) \
        | (BUNDLE_SEED_ID_PREF_TOKEN_BIT_MASK * BUNDLE_SEED_ID_PREF_TOKEN_MASK) \
        | (EA_PROTOCOL_METADATA_TOKEN_BIT_MASK * EA_PROTOCOL_METADATA_TOKEN_MASK)) /* \
        | (ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_BIT_MASK * ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_MASK) \
        | (ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_BIT_MASK * ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK)) */


#define ANALOG_LINE_OUT_MASK                            (IPOD_PREF_LINE_OUT_USAGE_MASK)
#define ANALOG_VIDEO_OUT_MASK                           (IPOD_PREF_VIDEO_OUT_SETTINGS_MASK)
#define USE_VOICE_OVER_MASK                             (IPOD_PREF_VOICE_OVER_MASK)
#define EA_PROTOCOL_TOKEN_MASK                          (COMM_WITH_IOS_APPLICATIONS_MASK)
#define BUNDLE_SEED_ID_PREF_TOKEN_MASK                  (COMM_WITH_IOS_APPLICATIONS_MASK)
#define EA_PROTOCOL_METADATA_TOKEN_MASK                 (COMM_WITH_IOS_APPLICATIONS_MASK)
#define HANDLE_ASSISTIVETOUCH_MASK                      (IPOD_PREF_ASSISTIVE_TOUCH_MASK)
#define AUDIO_VIDEO_SYNC_MASK                           (ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK)

#define IAP_CMD_LINGOES_SUPPORTED_MASK  ((IAP_GENERALLINGO_MASK << kGeneralLingo) | \
                                         (IAP_SIMPLEREMOTELINGO_MASK << kSimpleRemoteLingo) | \
                                         (IAP_DISPLAYREMOTELINGO_MASK << kDisplayRemoteLingo) | \
                                         (IAP_EXTENDEDINTERFACELINGO_MASK << kExtendedInterfaceLingo) | \
                                         (IAP_USBHOSTMODELINGO_MASK << kUSBHostModeLingo) | \
                                         (IAP_RFTUNERLINGO_MASK << kRFTunerLingo) | \
                                         (IAP_SPORTSLINGO_MASK << kSportsLingo) | \
                                         (IAP_DIGITALAUDIOLINGO_MASK << kDigitalAudioLingo) |\
                                         (IAP_STORAGELINGO_MASK << kStorageLingo) | \
                                         (IAP_IPODOUTLINGO_MASK << kIpodOutLingo) | \
                                         (IAP_LOCATIONLINGO_MASK << kLocationLingo))

#define IAP_CMD_NUM_LINGOES             (IAP_GENERALLINGO_MASK + IAP_SIMPLEREMOTELINGO_MASK \
                                        + IAP_DISPLAYREMOTELINGO_MASK + IAP_EXTENDEDINTERFACELINGO_MASK \
                                        + IAP_USBHOSTMODELINGO_MASK + IAP_RFTUNERLINGO_MASK \
                                        + IAP_SPORTSLINGO_MASK + IAP_DIGITALAUDIOLINGO_MASK \
                                        + IAP_STORAGELINGO_MASK + IAP_IPODOUTLINGO_MASK \
                                        + IAP_LOCATIONLINGO_MASK)

#define IAP_NUMBER_OF_SAMPLE_RATES      ((DIGITAL_SAMPLE_RATE_32KHZ_MASK) + \
                                         (DIGITAL_SAMPLE_RATE_44_1KHZ_MASK) + \
                                         (DIGITAL_SAMPLE_RATE_48KHZ_MASK) + \
                                         (DIGITAL_SAMPLE_RATE_96KHZ_MASK) + \
                                         (DIGITAL_SAMPLE_RATE_192KHZ_MASK))

#define ACC_INFO_NAME_STRING_DATA       'F', 'r', 'e', 'e', 's', 'c', 'a', 'l', 'e', ' ', \
                                        't', 'e', 's', 't', ' ', \
                                        'a', 'c', 'c', 'e', 's', 's', 'o', 'r', 'y', '\0'
#define ACC_INFO_NAME_STRING_SIZE                   0x19
#define ACC_INFO_FIRMWARE_VERSION_STRING_DATA       0x00, 0x00, 0x01
#define ACC_INFO_FIRMWARE_VERSION_STRING_SIZE       0x03
#define ACC_INFO_HARDWARE_VERSION_STRING_DATA       0x00, 0x00, 0x01
#define ACC_INFO_HARDWARE_VERSION_STRING_SIZE       0x03
#define ACC_INFO_MANUFACTURER_STRING_DATA           'F', 'r', 'e', 'e', 's', 'c', 'a', 'l', 'e','\0'
#define ACC_INFO_MANUFACTURER_STRING_SIZE           0x0A
#define ACC_INFO_MODEL_NUMBER_STRING_DATA           'T', 'e', 's', 't','\0'
#define ACC_INFO_MODEL_NUMBER_STRING_SIZE           0x05
#define ACC_INFO_SERIAL_NUMBER_STRING_DATA          '0', '0', '0', '0', '0', '1','\0'
#define ACC_INFO_SERIAL_NUMBER_STRING_SIZE          0x07

#define IDENTIFY_TOKEN_SIZE                         (IDENTIFY_TOKEN_MASK * (0x0C + IAP_CMD_NUM_LINGOES))
#define ACC_CAPABILITY_TOKEN_SIZE                   (ACC_CAPABILITY_TOKEN_MASK * 0x0B)
#define ACC_INFO_NAME_TOKEN_SIZE                    (ACC_INFO_NAME_TOKEN_MASK * (ACC_INFO_NAME_STRING_SIZE + 4))
#define ACC_INFO_FIRMWARE_VERSION_TOKEN_SIZE        (ACC_INFO_FIRMWARE_VERSION_TOKEN_MASK * (ACC_INFO_FIRMWARE_VERSION_STRING_SIZE + 4))
#define ACC_INFO_HARDWARE_VERSION_TOKEN_SIZE        (ACC_INFO_HARDWARE_VERSION_TOKEN_MASK * (ACC_INFO_HARDWARE_VERSION_STRING_SIZE + 4))
#define ACC_INFO_MANUFACTURER_TOKEN_SIZE            (ACC_INFO_MANUFACTURER_TOKEN_MASK * (ACC_INFO_MANUFACTURER_STRING_SIZE + 4))
#define ACC_INFO_MODEL_NUMBER_TOKEN_SIZE            (ACC_INFO_MODEL_NUMBER_TOKEN_MASK * (ACC_INFO_MODEL_NUMBER_STRING_SIZE + 4))
#define ACC_INFO_SERIAL_NUMBER_TOKEN_SIZE           (ACC_INFO_SERIAL_NUMBER_TOKEN_MASK * (ACC_INFO_SERIAL_NUMBER_STRING_SIZE + 4))
#define ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_SIZE        (ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_MASK * 0x06)
#define ACC_INFO_STATUS_TOKEN_SIZE                  (ACC_INFO_STATUS_TOKEN_MASK * 0x08)
#define ACC_INFO_RF_CERTIFICATION_TOKEN_SIZE        (0x08)
#define IPOD_PREF_VIDEO_OUT_SETTINGS_SIZE           (IPOD_PREF_VIDEO_OUT_SETTINGS_MASK * 0x06)
#define IPOD_PREF_SCREEN_CONFIGURATION_SIZE         (IPOD_PREF_SCREEN_CONFIGURATION_MASK * 0x06)
#define IPOD_PREF_VIDEO_SIGNAL_FORMAT_SIZE          (IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK * 0x06)
#define IPOD_PREF_LINE_OUT_USAGE_SIZE               (IPOD_PREF_LINE_OUT_USAGE_MASK * 0x06)
#define IPOD_PREF_VIDEO_OUT_CONNECTION_SIZE         (IPOD_PREF_VIDEO_OUT_CONNECTION_MASK * 0x06)
#define IPOD_PREF_CLOSED_CAPTIONING_SIZE            (IPOD_PREF_CLOSED_CAPTIONING_MASK * 0x06)
#define IPOD_PREF_VIDEO_ASPECT_RATIO_SIZE           (IPOD_PREF_VIDEO_ASPECT_RATIO_MASK * 0x06)
#define IPOD_PREF_SUBTITLES_SIZE                    (IPOD_PREF_SUBTITLES_MASK * 0x06)
#define IPOD_PREF_VIDEO_ALTERNATE_AUDIO_SIZE        (IPOD_PREF_VIDEO_ALTERNATE_AUDIO_MASK * 0x06)
#define IPOD_PREF_PAUSE_ON_POWER_REMOVAL_SIZE       (IPOD_PREF_PAUSE_ON_POWER_REMOVAL_MASK * 0x06)
#define IPOD_PREF_VOICE_OVER_SIZE                   (IPOD_PREF_VOICE_OVER_MASK * 0x06)
#define IPOD_PREF_ASSISTIVE_TOUCH_SIZE              (IPOD_PREF_ASSISTIVE_TOUCH_MASK * 0x06)
#define ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_SIZE    (ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_MASK * (0x03 + IAP_NUMBER_OF_SAMPLE_RATES * 4))
#define ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_SIZE    (ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK * 0x07)

#define IAP_CMD_TOTAL_NO_TOKENS         (IDENTIFY_TOKEN_MASK + ACC_CAPABILITY_TOKEN_MASK + ACC_INFO_NAME_TOKEN_MASK \
        + ACC_INFO_FIRMWARE_VERSION_TOKEN_MASK + ACC_INFO_HARDWARE_VERSION_TOKEN_MASK \
        + ACC_INFO_MANUFACTURER_TOKEN_MASK + ACC_INFO_MODEL_NUMBER_TOKEN_MASK \
        + ACC_INFO_SERIAL_NUMBER_TOKEN_MASK + ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_MASK \
        + ACC_INFO_STATUS_TOKEN_MASK + ACC_INFO_RF_CERTIFICATION_TOKEN_MASK \
        + IPOD_PREF_VIDEO_OUT_SETTINGS_MASK + IPOD_PREF_SCREEN_CONFIGURATION_MASK \
        + IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK + IPOD_PREF_LINE_OUT_USAGE_MASK \
        + IPOD_PREF_VIDEO_OUT_CONNECTION_MASK +    IPOD_PREF_CLOSED_CAPTIONING_MASK \
        + IPOD_PREF_VIDEO_ASPECT_RATIO_MASK + IPOD_PREF_SUBTITLES_MASK \
        + IPOD_PREF_VIDEO_ALTERNATE_AUDIO_MASK + IPOD_PREF_PAUSE_ON_POWER_REMOVAL_MASK \
        + IPOD_PREF_VOICE_OVER_MASK + IPOD_PREF_ASSISTIVE_TOUCH_MASK \
        + EA_PROTOCOL_TOKEN_MASK + BUNDLE_SEED_ID_PREF_TOKEN_MASK \
        + EA_PROTOCOL_METADATA_TOKEN_MASK + ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_MASK \
        + ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK)

#define IAP_TOTAL_NUMBER_TOKENS_FOR_NOT_IDPS_SUPPORT      ACC_INFO_SERIAL_NUMBER_TOKEN_MASK +\
                                                          ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_MASK +\
                                                          ACC_CAPABILITY_TOKEN_MASK +\
                                                          ACC_INFO_NAME_TOKEN_MASK +\
                                                          ACC_INFO_FIRMWARE_VERSION_TOKEN_MASK +\
                                                          ACC_INFO_HARDWARE_VERSION_TOKEN_MASK +\
                                                          ACC_INFO_MANUFACTURER_TOKEN_MASK +\
                                                          ACC_INFO_MODEL_NUMBER_TOKEN_MASK\
//                                                          + ACC_INFO_STATUS_TOKEN_MASK

/*********************************************************************************/
/****************     Accessory Capability definitions Begin     *****************/
/*********************************************************************************/
#define IAP_CMD_ACC_CAP_B0              ((ANALOG_LINE_OUT_MASK << 0)|(ANALOG_LINE_IN_MASK << 1) \
        |(ANALOG_VIDEO_OUT_MASK << 2)|(0 << 3) \
        |(USB_AUDIO_OUT_MASK << 4)|(0 << 5) \
        |(0 << 6)|(0 << 7))
#define IAP_CMD_ACC_CAP_B1              ((0 << 0)|(COMM_WITH_IOS_APPLICATIONS_MASK << 1) \
        |(0 << 2)|(CHECK_IOS_VOLUME_MASK << 3) \
        |(0 << 4)|(0 << 5) \
        |(0 << 6)|(0 << 7))
#define IAP_CMD_ACC_CAP_B2              ((0 << 0)|(USE_VOICE_OVER_MASK << 1) \
        |(HANDLE_ASYNC_PLAYBACK_MASK << 2)|(HANDLE_MULTI_PACKET_MASK << 3) \
        |(0 << 4)|(AUDIO_VIDEO_SYNC_MASK << 5) \
        |(0 << 6)|(HANDLE_ASSISTIVETOUCH_MASK << 7))
#define IAP_CMD_ACC_CAP_B3              ((0 << 0)|(0 << 1) \
        |(0 << 2)|(0 << 3) \
        |(0 << 4)|(0 << 5) \
        |(0 << 6)|(0 << 7))
#define IAP_CMD_ACC_CAP_B4              ((0 << 0)|(0 << 1) \
        |(0 << 2)|(0 << 3) \
        |(0 << 4)|(0 << 5) \
        |(0 << 6)|(0 << 7))
#define IAP_CMD_ACC_CAP_B5              ((0 << 0)|(0 << 1) \
        |(0 << 2)|(0 << 3) \
        |(0 << 4)|(0 << 5) \
        |(0 << 6)|(0 << 7))
#define IAP_CMD_ACC_CAP_B6              ((0 << 0)|(0 << 1) \
        |(0 << 2)|(0 << 3) \
        |(0 << 4)|(0 << 5) \
        |(0 << 6)|(0 << 7))
#define IAP_CMD_ACC_CAP_B7              ((0 << 0)|(0 << 1) \
        |(0 << 2)|(0 << 3) \
        |(0 << 4)|(0 << 5) \
        |(0 << 6)|(0 << 7))
#define IAP_CMD_ACC_CAP_BITMASK          IAP_CMD_ACC_CAP_B7,IAP_CMD_ACC_CAP_B6,IAP_CMD_ACC_CAP_B5, \
        IAP_CMD_ACC_CAP_B4,IAP_CMD_ACC_CAP_B3,IAP_CMD_ACC_CAP_B2, \
        IAP_CMD_ACC_CAP_B1,IAP_CMD_ACC_CAP_B0
/*********************************************************************************/
/****************      Accessory Capability definitions End      *****************/
/*********************************************************************************/

/*********************************************************************************/
/******************     Accessory Status definitions Begin     *******************/
/*********************************************************************************/
#if(BIG_ENDIAN_CORE)
#define IAP_CMD_ACC_STATUS_BITMASK              0x00,0x00,0x00, \
        ((BLUETOOTH_DEVICE_STATUS << kBluetoothDevice) \
                | (FAULT_CONDITION_STATUS << kFaultCondition))
#else
#define IAP_CMD_ACC_STATUS_BITMASK              ((BLUETOOTH_DEVICE_STATUS << kBluetoothDevice) \
        | (FAULT_CONDITION_STATUS << kFaultCondition)), \
        0x00,0x00,0x00
#endif
/*********************************************************************************/
/******************      Accessory Status definitions End      *******************/
/*********************************************************************************/

/*! @} 
* Group GeneralLingoDefs
*/

/*! Size (in bytes) of structures that uses some 'enum type' */
/* iap_gl_sessions_for_protocol_rx_t */
#define SIZE_OF_SESION_FOR_PROTOCOL_RX_TYPE                 (10)

/* */

/*************************************************************************************************
*                                       Typedef Section                             
*************************************************************************************************/
#pragma pack (push,1)
/*! General Lingo Enumeration */
typedef enum
{
    kGeneralLingoRequestIdentify,                       /*0x00*/
    kGeneralLingoUnusedOrDeprecated,                    /*0x01*/
    kGeneralLingoIpodAck,                               /*0x02*/
    kGeneralLingoRequestExtendedInterfaceMode,          /*0x03*/
    kGeneralLingoReturnExtendedInterfaceMode,           /*0x04*/
    kGeneralLingoEnterExtendedInterfaceMode,            /*0x05*/
    kGeneralLingoExitExtendedInterfaceMode = 0x06,      /*0x06*/
    kGeneralLingoRequestIpodName,                       /*0x07*/
    kGeneralLingoReturnIpodName,                        /*0x08*/
    kGeneralLingoRequestIpodSWVersion,                  /*0x09*/
    kGeneralLingoReturnIpodSWVersion,                   /*0x0A*/
    kGeneralLingoRequestIpodSerialNumber,               /*0x0B*/
    kGeneralLingoReturnIpodSerialNumber,                /*0x0C*/
  /*kGeneralLingoUnusedOrDeprecated,                    0x0D*/
  /*kGeneralLingoUnusedOrDeprecated,                    0x0E*/
    kGeneralLingoRequestLingoProtocolVersion = 0x0F,    /*0x0F*/
    kGeneralLingoReturnLingoProtocolVersion,            /*0x10*/
    kGeneralLingoRequestMaxPayloadSize,                 /*0x11*/
    kGeneralLingoReturnMaxPayloadSize,                  /*0x12*/
    kGeneralLingoIdentifyDeviceLingoes,                 /*0x13*/
    kGeneralLingoGetAccessoryAuthenticationInfo,        /*0x14*/
    kGeneralLingoRetAccessoryAuthenticationInfo,        /*0x15*/
    kGeneralLingoAckAccessoryAuthenticationInfo,        /*0x16*/
    kGeneralLingoGetAccessoryAuthenticationSignature,   /*0x17*/
    kGeneralLingoRetAccessoryAuthenticationSignature,   /*0x18*/
    kGeneralLingoAckAccessoryAuthenticationStatus,      /*0x19*/
    kGeneralLingoGetIpodAuthenticationInfo,             /*0x1A*/
    kGeneralLingoRetIpodAuthenticationInfo,             /*0x1B*/
    kGeneralLingoAckIpodAuthenticationInfo,             /*0x1C*/
    kGeneralLingoGetIpodAuthenticationSignature,        /*0x1D*/
    kGeneralLingoRetIpodAuthenticationSignature,        /*0x1E*/
    kGeneralLingoAckIpodAuthenticationStatus,           /*0x1F*/
  /*kGeneralLingoUnusedOrDeprecated,                      0x20
    kGeneralLingoUnusedOrDeprecated,                      0x21
    kGeneralLingoUnusedOrDeprecated,                      0x22*/
    kGeneralLingoNotifyIpodStateChange = 0x23,          /*0x23*/
    kGeneralLingoGetIpodOptions,                        /*0x24*/
    kGeneralLingoRetIpodOptions,                        /*0x25*/
  /*kGeneralLingoUnusedOrDeprecated,                      0x26*/
    kGeneralLingoGetAccessoryInfo = 0x27,               /*0x27*/
    kGeneralLingoRetAccessoryInfo,                      /*0x28*/
    kGeneralLingoGetIpodPreferences,                    /*0x29*/
    kGeneralLingoRetIpodPreferences,                    /*0x2A*/
    kGeneralLingoSetIpodPreferences,                    /*0x2B*/
  /*kGeneralLingoUnusedOrDeprecated,                      0x2C
    kGeneralLingoUnusedOrDeprecated,                      0x2D
    kGeneralLingoUnusedOrDeprecated,                      0x2E
    kGeneralLingoUnusedOrDeprecated,                      0x2F
    kGeneralLingoUnusedOrDeprecated,                      0x30
    kGeneralLingoUnusedOrDeprecated,                      0x31
    kGeneralLingoUnusedOrDeprecated,                      0x32
    kGeneralLingoUnusedOrDeprecated,                      0x33
    kGeneralLingoUnusedOrDeprecated,                      0x34*/
    kGeneralLingoGetUIMode = 0x35,                      /*0x35*/
    kGeneralLingoRetUIMode,                             /*0x36*/
    kGeneralLingoSetUIMode,                             /*0x37*/
    kGeneralLingoStartIDPS,                             /*0x38*/
    kGeneralLingoSetFIDTokenValues,                     /*0x39*/
    kGeneralLingoAckFIDTokenValues,                     /*0x3A*/
    kGeneralLingoEndIDPS,                               /*0x3B*/
    kGeneralLingoIDPSStatus,                            /*0x3C*/
  /*kGeneralLingoUnusedOrDeprecated,                      0x3D
    kGeneralLingoUnusedOrDeprecated,                      0x3E*/
    kGeneralLingoOpenDataSessionForProtocol = 0x3F,     /*0x3F*/
    kGeneralLingoCloseDataSession,                      /*0x40*/
    kGeneralLingoAccessoryAck,                          /*0x41*/
    kGeneralLingoAccessoryDataTransfer,                 /*0x42*/
    kGeneralLingoIpodDataTransfer,                      /*0x43*/
  /*kGeneralLingoUnusedOrDeprecated,                      0x44
    kGeneralLingoUnusedOrDeprecated,                      0x45*/
    kGeneralLingoSetAccessoryStatusNotification = 0x46, /*0x46*/
    kGeneralLingoRetAccessoryStatusNotification,        /*0x47*/
    kGeneralLingoAccessoryStatusNotification,           /*0x48*/
    kGeneralLingoSetEventNotification,                  /*0x49*/
    kGeneralLingoIpodNotification,                      /*0x4A*/
    kGeneralLingoGetIpodOptionsForLingo,                /*0x4B*/
    kGeneralLingoRetIpodOptionsForLingo,                /*0x4C*/
    kGeneralLingoGetEventNotification,                  /*0x4D*/
    kGeneralLingoRetEventNotification,                  /*0x4E*/
    kGeneralLingoGetSupportedEventNotification,         /*0x4F*/
    kGeneralLingoCancelCommand,                         /*0x50*/
    kGeneralLingoRetSupportedEventNotification,         /*0x51*/
  /*kGeneralLingoUnusedOrDeprecated,                      0x52
    kGeneralLingoUnusedOrDeprecated,                      0x53*/
    kGeneralLingoSetAvailableCurrent = 0x54,            /*0x54*/
  /*kGeneralLingoUnusedOrDeprecated,                      0x55*/
    kGeneralLingoSetInternalBatteryChargingState = 0x56,/*0x56*/
  /*kGeneralLingoUnusedOrDeprecated,                      0x57
    kGeneralLingoUnusedOrDeprecated,                      0x58
    kGeneralLingoUnusedOrDeprecated,                      0x59
    kGeneralLingoUnusedOrDeprecated,                      0x5A
    kGeneralLingoUnusedOrDeprecated,                      0x5B
    kGeneralLingoUnusedOrDeprecated,                      0x5C
    kGeneralLingoUnusedOrDeprecated,                      0x5D
    kGeneralLingoUnusedOrDeprecated,                      0x5E
    kGeneralLingoUnusedOrDeprecated,                      0x5F
    kGeneralLingoUnusedOrDeprecated,                      0x60
    kGeneralLingoUnusedOrDeprecated,                      0X61
    kGeneralLingoUnusedOrDeprecated,                      0X62
    kGeneralLingoUnusedOrDeprecated,                      0x63*/
    kGeneralLingoRequestApplicationLaunch = 0x64,       /*0x64*/
    kGeneralLingoGetNowPlayingApplicationBundleName,    /*0x65*/
    kGeneralLingoRetNowPlayingApplicationBundleName,    /*0x66*/
    kGeneralLingoGetLocalizationInfo,                   /*0x67*/
    kGeneralLingoRetLocalizationInfo,                   /*0x68*/
    kGeneralLingoRequestWiFiConnectionInfo,             /*0x69*/
    kGeneralLingoWiFiConnectionInfo                     /*0x6A*/
    /*kGeneralLingoUnusedOrDeprecated,                    0x6B To 0xFF*/
}iap_general_lingo_commands_t;

/*! Application callbacks IDs */
typedef enum
{
    kIapAppCallbackID1 = 0,
    kIapAppCallbackID2,
    kIapAppCallbackID3,
    kIapAppCallbackID4,
    kIapAppCallbackID5,
    kIapAppCallbackID6
}iap1_ios_applications_callbacks_Ids_t;

/** Return iPod Software Version (Lingo = 0x00, Cmd = 0x0A) Structure */ 
typedef struct
{
    uint8_t appleMajorVerNum;
    uint8_t appleMinorVerNum;
    uint8_t appleRevisionVerNum;
}iap_gl_ret_ipod_software_version_t;

/** Return Lingo Protocol Version (Lingo = 0x00, Cmd = 0x10) Structure */ 
typedef struct
{
    uint8_t lingoRequested;
    uint8_t lingoMajorProtocolVer;
    uint8_t lingoMinorProtocolVer;
}iap_gl_ret_lingo_protocol_version_t;

/** Return Transport Max Payload Size (Lingo = 0x00, Cmd = 0x12) type */
typedef uint32_t iap_gl_return_transport_max_payload_size_t;

/** Identify Device Lingoes (Lingo = 0x00, Cmd = 0x13) Structure */ 
typedef struct
{
    uint32_t deviceLingoesSpoken;
    uint32_t options;
    uint32_t deviceID;
}iap_gl_identify_device_lingoes_t;

/** Identify Device Lingoes - Options (Lingo = 0x00, Cmd = 0x13) Enumeration */ 
typedef enum
{
    kNoAuthenticationSupported = 0,
    kDeferAuthentication,
    kAuthAfterIdentification
}iap_gl_identify_device_lingoes_options_t;

/** Ack Accessory Authentication Info (Lingo = 0x00, Cmd = 0x16) Enumeration */
typedef enum
{
    kAuthInfoSupported = 0x00,
    kCertTooLongSectionsOutOrder = 0x04,
    kAuthInfoNotSupported = 0x08,
    kCertificateInvalid = 0x0A,
    kCertPermissionsInvalid = 0x0B
}iap_gl_ack_accessory_authentication_info_values_t;

/** Ack iPod Authentication info (Lingo = 0x00, Cmd = 0x1C and Cmd = 0x1F) Enumeration */
typedef enum
{
    kAuthInfoValid = 0,
    kInvalidRegRead,
    kInvalidRegWrite,
    kInvalidSignLength,
    kInvalidChallengeLength,
    kInvalidCertLength,
    kErrorOnSignatureGen,
    kErrorOnChallengeGen,
    kErrorOnSignatureVal,
    kErrorOnCertVal,
    kInvalidProcessCtrl,
    kProcessCtrlOutSequence
}iap_gl_ack_ipod_authentication_info_values_t;

/** Get iPod Authentication Signature (Lingo = 0x00, Cmd = 0x1D) Structure */ 
typedef struct
{
    uint8_t challenge[20];
    uint8_t authRetryCounter;
}iap_gl_get_ipod_authentication_signature_t;

/** Notify iPod State Change Values (Lingo = 0x00, Cmd = 0x23) Enumeration */
typedef enum
{
    kIpodGoingToHibernateNotPreservMenu = 1,
    kIpodGoingToHibernatePreservMenu,
    kIpodGoingToSleep,
    kIpodGoingToPowerOn
}iap_gl_notify_ipod_state_change_values_t;

/** Return iPod Options (Lingo = 0x00, Cmd = 0x25) Enumeration */
typedef enum
{
    kAppleSupportsVideoOutput,
    kAppleSupportsLineoutUsage
}iap_gl_ret_ipod_option_values_t;

/** Get Accessory Info (Lingo = 0x00, Cmd = 0x27, Cmd = 0x28) Enumeration */
typedef enum
{
    kAccessoryCapabilities = 0,
    kAccessoryName,
    kAccessoryMinIpodFwSupported,
    kAccessoryMinLingoVerSupported,
    kAccessoryFirmwareVer,
    kAccessoryHardwareVer,
    kAccessoryManufacturer,
    kAccessoryModelNum,
    kAccessorySerialNum,
    kAccessoryIncomingMaxPayload,
    kAccessoryStatusTypeSupported = 0x0B
}iap_gl_accessory_info_type_values_t;

/** Get Accessory Info - Accessory Minimum iPod Firmware Version Type 
 * (Lingo = 0x00, Cmd = 0x27) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint32_t appleDeviceModelID;
}iap_gl_get_accessory_info_minimum_ipod_firmware_version_t;

/** Get Accessory Info - Accessory Minimum Lingo Version Type 
 * (Lingo = 0x00, Cmd = 0x27) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint8_t lingoId;
}iap_gl_get_accessory_info_minimum_lingo_version_t;

/** Return Accessory Info - Accessory info capabilities (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint32_t accCapabilities;
}iap_gl_ret_accessory_info_capabilities_t;

/** Return Accessory Info - Accessory Name (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint8_t charecterArray[64];                /**< Variable length */
}iap_gl_ret_accessory_info_name_t;

/** Return Accessory Info - Accessory Minimum iPod Firmware Supported 
 * (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint32_t appleDeviceModelID;
    uint8_t minAppleFWMajorVer;
    uint8_t minAppleFWMinorVer;
    uint8_t minAppleFWRevVer;
}iap_gl_ret_accessory_info_minimum_ipod_firmware_version_supported_t;

/** Return Accessory Info - Accessory Minimum Lingo Version Supported 
 * (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint8_t lingoId;
    uint8_t lingoIDMajorVer;
    uint8_t lingoIDMinorVer;
}iap_gl_ret_accessory_info_minimum_lingo_version_supported_t;

/** Return Accessory Info - Accessory Firmware Version (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint8_t accessoryMajorVer;
    uint8_t accessoryMinorVer;
    uint8_t accessoryRevVer;
}iap_gl_ret_accessory_info_firmware_version_t;

/** Return Accessory Info - Accessory Hardware Version (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint8_t accessoryMajorVer;
    uint8_t accessoryMinorVer;
    uint8_t accessoryRevVer;
}iap_gl_ret_accessory_info_hardware_version_t;

/** Return Accessory Info - Accessory Manufacturer (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint8_t manufacturerName[64];                    /**< Variable length */
}iap_gl_ret_accessory_info_manufacturer_t;

/** Return Accessory Info - Accessory Model Number (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint8_t modelNumber[64];                    /**< Variable length */
}iap_gl_ret_accessory_model_number_t;

/** Return Accessory Info - Accessory Serial Number (Lingo = 0x00, Cmd = 0x28) Structure */ 
typedef struct
{
    uint8_t accessoryInfoType;
    uint8_t serialNumber[64];                    /**< Variable length */
}iap_gl_ret_accessory_info_serial_number_t;

/** Return Accessory Info - Accessory Incoming Max Payload (Lingo = 0x00, Cmd = 0x28) Structure */
typedef struct
{
    uint8_t accessoryInfoType;
    uint16_t accInMaxPayload;
}iap_gl_ret_accessory_info_incoming_max_payload_t;

/** Return Accessory Info - Accessory Status Types 
 * (Lingo = 0x00, Cmd = 0x28, Cmd = 0x39, Cmd = 0x46, Cmd = 0x47 and Cmd = 0x48) Enumeration */
typedef enum
{
    kBluetoothDevice = 1,
    kFaultCondition
}iap_gl_accessory_status_notification_t;

/** Return Accessory Info - Accessory Fault Type 
 * (Lingo = 0x00, Cmd = 0x28, Cmd = 0x48) Enumeration */
typedef enum
{
    kAccessoryStatusNoFault,
    kAccessoryStatusVoltageFault,
    kAccessoryStatusCurrentFault
}iap_gl_accessory_fault_t;

/** Return Accessory Info - Accessory Fault Condition 
 * (Lingo = 0x00, Cmd = 0x28, Cmd = 0x48) Enumeration */
typedef enum
{
    kFaultConditionNoFault,
    kFaultConditionRecoverable,
    kFaultConditionNotRecoverable
}iap_gl_accessory_fault_condition_value_t;

/** Return Accessory Info - Accessory Status Type Supported 
 * (Lingo = 0x00, Cmd = 0x28) Structure */
typedef struct
{
    uint8_t accessoryInfoType;
    uint32_t accStatusTypesSupported;
}iap_gl_ret_accessory_info_status_supported_t;

/** Get/Return iPod Preferences - Preference Class ID 
 * (Lingo = 0x00, Cmd = 0x29, Cmd = 0x2A) Enumeration */
typedef enum
{
    kClassVideoOut = 0,
    kClassScreenConfig,
    kClassVideoSignalFormat,
    kClassLineOutUsage,         /**< This is the only class valid if ACC is not authenticated */
    kClassVideoOutConnection = 0x08,
    kClassClosedCaptioning,
    kClassVideoAspectRatio,
    kClassSubtitles = 0x0C,
    kClassVideoAlternateAudio,
    kClassPauseOnPowerRemoval = 0x0F,
    kClassVoiceOverPreference = 0x14,
    kClassAssistiveTouch = 0x16
}iap_gl_ipod_preference_class_id_t;

/** Return iPod Preferences - Video out Setting (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kVideoOutOff = 0,
    kVideoOutOn
}iap_gl_ipod_preference_video_out_setting_values_t;

/** Return iPod Preferences - Screen Configurations (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kScreenConfigFillEntireScreen = 0,
    kScreenConfigFitToScreenEdge
}iap_gl_ipod_preference_screen_configuration_values_t;

/** Return iPod Preferences - Video Signal Format (Lingo = 0x00, Cmd = 0x2A) Enumeration */
/*typedef enum
{
    kVideoSignalFormatNtsc = 0,
    kVideoSignalFormatPal
}_IPodPrefs_VideoSignalFormatValues_;*/

/**@warning Need to fix. Used macros because CW was not compiling code OK */
#define VIDEO_SIGNAL_FORMAT_NTSC           0
#define VIDEO_SIGNAL_FORMAT_PAL            1


/** Return iPod Preferences - Line-out Usage (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kLineOutUsageNotUsed = 0,
    kLineOutUsageUsed
}iap_gl_ipod_preference_line_out_usage_values_t;

/** Return iPod Preferences - Video-out Connection (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kVideoOutConnectionNone = 0,
    kVideoOutConnectionComposite,
    kVideoOutConnectionSVideo,
    kVideoOutConnectionComponent
}iap_gl_ipod_preference_video_out_connection_values_t;

/** Return Extended Interface Mode (Lingo = 0x00, Cmd = 0x04) Enumeration */
typedef enum
{
    kAppleDevIsNotInExtendedInterfaceMode = 0,
    kAppleDevIsInExtendedInterfaceMode
}iap_gl_return_extended_interface_mode_t;

/**@warning Need to fix. Used macros because CW was not compiling code OK */
/*
#define VIDEO_OUT_CONNECTION_NONE            0
#define VIDEO_OUT_CONNECTION_COMPOSITE       1
#define VIDEO_OUT_CONNECTION_S_VIDEO         2
#define VIDEO_OUT_CONNECTION_COMPONENT       3
*/

/** Return iPod Preferences - Closed Captioning (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kClosedCaptioningOff = 0,
    kClosedCaptioningOn
}iap_gl_ipod_preference_closed_captioning_values_t;

/** Return iPod Preferences - Aspect Ratio (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kVideoAspectRatioFullscreen = 0,
    kVideoAspectRatioWidescreen
}iap_gl_ipod_preference_aspect_ratio_values_t;

/**@warning Need to fix. Used macros because CW was not compiling code OK */
/*
#define VIDEO_ASPECT_RATIO_FULLSCREEN        0
#define VIDEO_ASPECT_RATIO_WIDESCREEN        1
*/
/** Return iPod Preferences - Subtitles (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kSubtitlesOff = 0,
    kSubtitlesOn
}iap_gl_ipod_preference_subtitles_values_t;

/** Return iPod Preferences - Video Alternate Audio Channel 
 * (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kVideoAlternateAudioOff = 0,
    kVideoAlternateAudioOn
}iap_gl_ipod_preference_video_alternate_audio_values_t;

/** Return iPod Preferences - Pause on Power Removal (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kPauseOnPowerRemovalOff = 0,
    kPauseOnPowerRemovalOn
}iap_gl_ipod_preference_pause_on_power_removal_values_t;

/** Return iPod Preferences - Voice Over Preference (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kVoiceOverPrefOff = 0,
    kVoiceOverPrefOn
}iap_gl_ipod_preference_voice_over_preference_values_t;

/** Return iPod Preferences - Assistive Touch (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kAssistiveTouchOff = 0,
    kAssistiveTouchOn
}iap_gl_ipod_preference_assistive_touch_values_t;

/** Return iPod Preferences - Restore on Exit (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    kGeneralLingoRestoreOnExitOff = 0,
    kGeneralLingoRestoreOnExitOn
}iap_gl_ipod_preference_restore_on_exit_values_t;


/** Return iPod Preferences (Lingo = 0x00, Cmd = 0x2A) Structure */ 
typedef struct
{
    uint8_t preferenceClassID;
    uint8_t preferenceSettingID;
}iap_gl_ret_ipod_preference_t;

/** Set iPod Preferences (Lingo = 0x00, Cmd = 0x2B) Structure */ 
typedef struct
{
    uint8_t preferenceClassID;      /**< Use Ret iPod Preference Apple device class ID Enumeration values */
    uint8_t preferenceSettingID;    /**< Use Ret iPod Preference Apple device Settings ID Enumeration values */
    uint8_t restoreOnExit;
}iap_gl_set_ipod_preference_t;

/** Return UI Mode (Lingo = 0x00, Cmd = 0x36) Enumeration */
typedef enum{
    kStandardUIMode = 0x00,
    kExtendedUIMode,
    kIpodOutFullScreenUIMode,
    kIpodOutActionSafeUIMode
}iap_gl_operating_user_interface_mode_t;

/** Set FID Token Values - Identify Token (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                        /**< FIDType = 0x00 */
    uint8_t fidSubtype;                    /**< FIDSubType = 0x00 */
    uint8_t numLingoes;
    uint8_t accessoryLingoes[3];            /**< Variable length */
    uint8_t deviceOptions[4];
    uint8_t deviceID[4];
}iap_gl_identify_token_t;

/** Set FID Token Values - Accessory Capabilities Token (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x01 */
    uint8_t accCapsBitmask[8];
}iap_gl_accessory_caps_token_t;

/** Set FID Token Values - Accessory Capabilities Bit Mask 
 * (Lingo = 0x00, Cmd = 0x39) Enumeration */
typedef enum
{
    kAnalogLineOutByte0 = 0,
    kAnalogLineInByte0,
    kAnalogVideoOutByte0,
    kUsbAudioOutByte0 = 4,
    kCommunicationWithIosAppByte1 = 9,
    kCheckIosVolumeByte1 = 11,
    kUseVoiceOverByte2 = 17,
    kHandleAsynchronousPlaybackByte2,
    kHandleMultipacketByte2,
    kAudioVideoSynchronizationByte2 = 21,
    HANDLE_ASSISTIVE_TOUCH_CURSOR_BYTE2 = 23
}iap_gl_accessory_capabilities_bitmask_t;

/** Set FID Token Values - Accessory Info Token - Accessory Name 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x01 */
    uint8_t accessoryName[64];               /**< Variable length */
}iap_gl_accessory_name_token_t;

/** Set FID Token Values - Accessory Info Token - Accessory Firmware Version 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x04 */
    uint8_t firmwareVersion[3];
}iap_gl_accessory_firmware_version_token_t;

/** Set FID Token Values - Accessory Info Token - Accessory Hardware Version 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x05 */
    uint8_t hardwareVersion[3];
}iap_gl_accessory_hardware_version_token_t;

/** Set FID Token Values - Accessory Info Token - Accessory Manufacturer 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x06 */
    uint8_t accManufacturer[64];             /**< Variable length */
}iap_gl_accessory_manufacturer_token_t;

/** Set FID Token Values - Accessory Info Token - Accessory Model Number 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x07 */
    uint8_t accModelNumber[64];              /**< Variable length */
}iap_gl_accessory_model_number_token_t;

/** Set FID Token Values - Accessory Info Token - Accessory Serial Number 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x08 */
    uint8_t accSerialNumber[64];             /**< Variable length */
}iap_gl_accessory_serial_number_token_t;

/** Set FID Token Values - Accessory Info Token - Accessory Incoming Max Payload 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x09 */
    uint16_t inMaxPayload;
}iap_gl_accessory_in_max_payload_token_t;

/** Set FID Token Values - Accessory Info Token - Accessory Status 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x0B */
    uint32_t accStatus;
}iap_gl_accessory_status_token_t;

/** Set FID Token Values - Accessory Info Token - Accessory RF Certification 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x02 */
    uint8_t accInfoType;                      /**< AccInfoType = 0x0C */
    uint32_t rfCertification;
}iap_gl_accessory_rf_certification_token_t;

/** Set FID Token Values - Accessory Info Token - RF Certification 
 * (Lingo = 0x00, Cmd = 0x39) Enumeration */
typedef enum
{
    kRfClass1,
    kRfClass2,
    kRfClass4 = 3,
    kRfClass5
}iap_gl_accessory_rf_certification_classes_t;

/** Set FID Token Values - Ipod Preference Token (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;                       /**< Length = 0x05 */
    uint8_t fidType;                      /**< FIDType = 0x00 */
    uint8_t fidSubtype;                   /**< FIDSubType = 0x03 */
    uint8_t ipodPrefClass;
    uint8_t prefClassSetting;
    uint8_t restoreOnExit;
}iap_gl_ipod_preference_token_t;

/** Set FID Token Values - EA Protocol Token (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                      /**< FIDType = 0x00 */
    uint8_t fidSubtype;                   /**< FIDSubType = 0x04 */
    uint8_t protocolIndex;
    uint8_t protocolString[64];          /**< Variable length */
}iap_gl_ea_protocoltoken_t;

/** Set FID Token Values - Bundle Seed ID Preference Token (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;                        /**< Length = 0x0D */
    uint8_t fidType;                       /**< FIDType = 0x00 */
    uint8_t fidSubtype;                    /**< FIDSubType = 0x05 */
    uint8_t bundleSeedIDString[11];
}iap_gl_bundle_seed_id_token_t;

/** Set FID Token Values - EA Protocol Metadata Token (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;                        /**< Length = 0x04 */
    uint8_t fidType;                       /**< FIDType = 0x00 */
    uint8_t fidSubtype;                    /**< FIDSubType = 0x08 */
    uint8_t protocolIndex;
    uint8_t metadataType;
}iap_gl_ea_protocol_metadata_token_t;

/** Set FID Token Values - EA Protocol Metadata Token (Lingo = 0x00, Cmd = 0x39) Enumeration */
typedef enum
{
    kEaProtHideInformation = 0,
    kEaProtAppStoreDefault,
    kEaProtAppStoreNeverMatch = 3
}iap_gl_ea_protocol_metadata_value_t;

/** Set FID Token Values - Accessory Digital Audio Sample Rates Token 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;
    uint8_t fidType;                          /**< FIDType = 0x00 */
    uint8_t fidSubtype;                       /**< FIDSubType = 0x0E */
    uint32_t sampleRates[IAP_NUMBER_OF_SAMPLE_RATES];
}iap_gl_audio_sample_rates_token_t;

/** Set FID Token Values - Accessory Digital Audio Sample Rates Token 
 * (Lingo = 0x00, Cmd = 0x39) Enumeration */
typedef enum
{
    kDigitalSampleRate32khz     =   32000,
    kDigitalSampleRate441khz    =   44100,
    kDigitalSampleRate48khz     =   48000,
    kDigitalSampleRate96khz     =   96000,
    kDigitalSampleRate192khz    =   192000
}iap_gl_accessory_audio_sample_rates_value_t;

/** Set FID Token Values - Accessory Digital Audio Video Delay Token 
 * (Lingo = 0x00, Cmd = 0x39) Structure */
typedef struct
{
    uint8_t tokenNumber;
    uint8_t length;                        /**< Length = 0x06 */
    uint8_t fidType;                       /**< FIDType = 0x00 */
    uint8_t fidSubtype;                    /**< FIDSubType = 0x0F */
    uint32_t videoDelayMs;
}iap_gl_audio_video_delay_token_t;

/** Set FID Token SubTypes Values (Lingo = 0x00, Cmd = 0x39) Enumeration */
typedef enum
{
    kIdentifyTokenFIDSubtype = 0x00,                        /*0x00*/
    kAccessoryCapsTokenFIDSubtype,                          /*0x01*/
    kAccessoryInfoTokenFIDSubtype,                          /*0x02*/
    kIpodPreferenceTokenFIDSubtype,                         /*0x03*/
    kEAProtocolTokenFIDSubtype,                             /*0x04*/
    kBundleSeedIDPrefTokenFIDSubtype,                       /*0x05*/
  /*kNotUsedFIDSubtype,                                       0x06
    kNotUsedFIDSubtype,                                       0x07*/
    kEAProtocolMetadataTokenFIDSubtype = 0x08,              /*0x08*/
  /*kFIDSubtypeNotUsed,                                       0x09
    kFIDSubtypeNotUsed,                                       0x0A
    kFIDSubtypeNotUsed,                                       0x0B
    kFIDSubtypeNotUsed,                                       0x0C
    kFIDSubtypeNotUsed,                                       0x0D*/   
    kAccessoryDigitalAudioSampleRatesTokenFIDSubtype = 0x0E,/*0x0E*/  
    kAccessoryDigitalAudioVideoDelayTokenFIDSubtype,        /*0x0F*/
}iap_fid_subtype_t;

/** Set FID Token - Accessory Info Types Values (Lingo = 0x00, Cmd = 0x39) Enumeration */
typedef enum
{
    kAccessoryNameType = 0x01,
    kAccessoryFirmwareVersionType = 0x04,
    kAccessoryHardwareVersionType,
    kAccessoryManufacturerType,
    kAccessoryModelNumberType,
    kAccessorySerialNumberType,
    kAccessoryInMaxPayloadType,
    kAccessoryStatusType = 0x0B,
    kAccessoryRfCertificationType
}iap_gl_accessory_info_types_value_t;

/** Acknowledge FID Token Values (Lingo = 0x00, Cmd = 0x3A) Enumeration */
typedef enum
{
    kTokenAccepted,
    kRequiredTokenFailed,
    kOptionalTokenFailed,
    kTokenNotSupported,
    kLingoesBusy,
    kMaximumConnectionsReached
}iap_gl_acknowledge_status_codes_t;

/** AckFID Token Values - Acknowledgment for Identify Token (Lingo = 0x00, Cmd = 0x3A) Structure*/
typedef struct
{
    uint8_t numFidTokenValueAcks;
    uint8_t length;
    uint8_t fidType;
    uint8_t fidSubtype;
    uint8_t ackStatus;
    uint8_t lingoIDs[16];
}iap_gl_ack_fid_identify_token_t;


/** AckFID Token Values - Acknowledgment for Accessory Caps Token 
 * (Lingo = 0x00, Cmd = 0x3A) Structure*/
typedef struct
{
    uint8_t numFidTokenValueAcks;
    uint8_t length;
    uint8_t fidType;
    uint8_t fidSubtype;
    uint8_t ackStatus;
}iap_gl_ack_fid_accessory_caps_t;

/** AckFID Token Values - Acknowledgment for Accessory Info Token 
 * (Lingo = 0x00, Cmd = 0x3A) Structure*/
typedef struct
{
    uint8_t numFidTokenValueAcks;
    uint8_t length;
    uint8_t fidType;
    uint8_t fidSubtype;
    uint8_t ackStatus;
    uint8_t accInfoType;
}iap_gl_ack_fid_accessory_info_t;

/** AckFID Token Values - Acknowledgment for ipod Preference Token 
 * (Lingo = 0x00, Cmd = 0x3A) Structure*/
typedef struct
{
    uint8_t numFidTokenValueAcks;
    uint8_t length;
    uint8_t fidType;
    uint8_t fidSubtype;
    uint8_t ackStatus;
    uint8_t ipodPreferenceClass;
}iap_gl_ack_fid_ipod_preference_t;

/** AckFID Token Values - Acknowledgment for EA Protocol Token 
 * (Lingo = 0x00, Cmd = 0x3A) Structure*/
typedef struct
{
    uint8_t numFidTokenValueAcks;
    uint8_t length;
    uint8_t fidType;
    uint8_t fidSubtype;
    uint8_t ackStatus;
    uint8_t protocolIndex;
}iap_gl_ack_fid_ea_protocol_t;

/** AckFID Token Values - Acknowledgment for BundleSeedIDPrefToken 
 * (Lingo = 0x00, Cmd = 0x3A) Structure*/
typedef struct
{
    uint8_t numFidTokenValueAcks;
    uint8_t length;
    uint8_t fidType;
    uint8_t fidSubtype;
    uint8_t ackStatus;
}iap_gl_ack_fid_bundle_seed_id_t;

/** AckFID Token Values - Acknowledgment for Screen Info Token 
 * (Lingo = 0x00, Cmd = 0x3A) Structure*/
typedef struct
{
    uint8_t numFidTokenValueAcks;
    uint8_t length;
    uint8_t fidType;
    uint8_t fidSubtype;
    uint8_t ackStatus;
}iap_gl_ack_fid_screen_info_t;

/** AckFID Token Values - Acknowledgment for EA Protocol Metadata 
 * (Lingo = 0x00, Cmd = 0x3A) Structure*/
typedef struct
{
    uint8_t numFidTokenValueAcks;
    uint8_t length;
    uint8_t fidType;
    uint8_t fidSubtype;
    uint8_t ackStatus;
    uint8_t dummyByte;  /**< Test show that this command is returning an extra byte not documented */
}iap_gl_ack_fid_ea_protocol_metadata_t;


/** Types for Accessory Digital Audio Sample Rates Token and Digital Audio Video Delay Token 
 * are not implemented because they are not supported in this version */

/** Accessory End IDPS (Lingo = 0x00, Cmd = 0x3B) Enumeration */
typedef enum
{
    kAccessoryEndsIdpsAndStartAuth,
    kAccessoryAskToResetIdpsInfo,
    kAccessoryAbandonedIdps,
    kAccessoryRestartIdpsOnNewTransport
}iap_gl_accessory_end_idps_t;

/** IDPS Status (Lingo = 0x00, Cmd = 0x3C) Enumeration */
typedef enum
{
    kAllRequiredTokensReceived,
    kRequiredTokenRejected,
    kRequiredTokenMissing,
    kRequiredTokensRejectedAndMissing,
    kIdpsTimeLimitNotExceeded,
    kIdpsTimeLimitWasReached,
    kIosWontAcceptAnyToken,
    kPreferenceTokenMismatchAccessoryCapabilities
}iap_gl_idps_status_t;

/** Accessory ack (Lingo = 0x00, Cmd = 0x41) Structure */
typedef struct
{
    uint8_t ackStatus;
    uint8_t commandID;
}iap_gl_accessory_ack_t;

/** Accessory Data transfer (Lingo = 0x00, Cmd = 0x42) Structure */
typedef struct {
    uint16_t appDataSize;
    uint16_t sessionID;
    uint8_t applicationData[IAPI_MAX_RX_PAYLOAD_SIZE];
}iap_gl_app_data_transfer_t;

/** Session for protocol status (Lingo = 0x00, Cmd = 0x3C, Cmd = 0x40 and Cmd = 0x43) Enumeration */
typedef enum
{
    kReceivedData,
    kOpenSession,
    kCloseSession
}iap_gl_sessions_for_protocol_rx_status_t;

/** Open Data Session for Protocol  (Lingo = 0x00, Cmd = 0x3F) Structure 
 *  Close Data Session for Protocol (Lingo = 0x00, Cmd = 0x40) Structure
 *  iPod Data Transfer              (Lingo = 0x00, Cmd = 0x43) Structure */
typedef struct
{
    uint8_t* protocolSessionData;
    uint16_t sessionId;
    uint16_t applicationPayloadSize; 
    iap_gl_sessions_for_protocol_rx_status_t communicationStatus;
    iap1_ios_applications_callbacks_Ids_t appCallbackId;
}iap_gl_sessions_for_protocol_rx_t;

/** Accessory Data Transfer (Lingo = 0x00, Cmd = 0x42) Structure */
typedef struct
{
    uint16_t sessionId;
    uint8_t sessionData[IAPI_MAX_RX_PAYLOAD_SIZE];
}iap_gl_ipod_data_transfer_packet_t;

/** Accessory Status Notification - Bluetooth Device Type 
 * (Lingo = 0x00, Cmd = 0x48) Enumeration */
typedef enum
{
    kBluetoothClassic
}iap_gl_accessory_bluetooth_device_t;

/** Accessory Status Notification - Bluetooth Device State 
 * (Lingo = 0x00, Cmd = 0x48) Enumeration */
typedef enum
{
    kBluetoothClassicOff,
    kBluetoothClassicOn
}iap_gl_accessory_bluetooth_device_state_t;

/** Accessory Status Notification - Bluetooth Device Class 
 * (Lingo = 0x00, Cmd = 0x48) Enumeration */
typedef enum
{
    ///@todo JCB - For Bluetooth Device Class read Bluetooth Core Specification Version 4.0, Volume 3, Section 3.2.4
    ABDC_
}iap_gl_accessory_bluetooth_device_class_t;

/** Accessory Status Notification - Bluetooth Pairing Type 
 * (Lingo = 0x00, Cmd = 0x48) Enumeration */
typedef enum
{
    kBluetoothPairingNone,
    kBluetoothPairingPinCode,
    kBluetoothPairingSsp                        /**< Secure Simple Pairing (SSP)*/
}iap_gl_accessory_bluetooth_pairing_t;

/** Accessory Status Notification - Bluetooth (Lingo = 0x00, Cmd = 0x48) Structure */
typedef struct
{
    uint8_t statusType;                        /**< Status Type = 0x01 */
    uint32_t deviceType;
    uint32_t deviceState;
    uint32_t deviceClass;
    uint8_t pairingType;
    uint8_t reserved;                            /**< Set to 0x00 */
    uint8_t pinCode[16];
    uint8_t macAddress[6];
}iap_gl_accessory_status_bluetooth_t;

/** Accessory Status Notification - Fault Condition (Lingo = 0x00, Cmd = 0x48) Structure */
typedef struct
{
    uint8_t statusType;                        /**< Status Type = 0x02 */
    uint8_t faultType;
    uint8_t faultCondition;    
}iap_gl_accessory_status_fault_t;

/** Set Event Notification - Notification bits 
 * (Lingo = 0x00, Cmd = 0x49, Cmd = 0x4A, Cmd = 0x4E and Cmd = 0x51) Enumeration */
typedef enum
{
    kNotifyFlowControl = 2,
    kNotifyRadioTaggingStatus,
    kNotifyCameraStatus,
    kNotifyChargingInfo,
    kNotifyDatabaseChanged = 9,
    kNotifyNowPlayingAppBundleNameStatus,
    kNotifySessionsSpaceAvailableStatus,
    kNotifyCommandCompleted = 13,
    kNotifyStatusIpodOutMode = 15,
    kNotifyBluetoothConnectionStatus = 17,
    kNotifyNowPlayingAppDisplayNameStatus = 19,
    kNotifyAssistiveTouchStatus
}iap_gl_event_notification_t;

/** @todo rr: notifications structs iPod Notification - Flow Control 
 * (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint32_t waitTime;
    uint16_t overFlowTransactionId;
}iap_gl_ipod_notification_flow_control_t;

/** iPod Notification - Radio Tagging (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t tagStatus;
}iap_gl_ipod_notification_radio_tagging_t;

/** iPod Notification - Radio Tagging status (Lingo = 0x00, Cmd = 0x4A) Enumeration */
typedef enum
{
    kTaggingOperationSuccessful = 0,
    kTaggingOperationFailed,
    kInformationAvailableForTagging,
    kInformationNotAvailableForTagging
}iap_gl_ipod_notification_radio_tagging_value_t;

/** iPod Notification - Camera Notification (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t cameraStatus;
}iap_gl_ipod_notification_camera_t;

/** iPod Notification - Camera Notification Values (Lingo = 0x00, Cmd = 0x4A) Enumeration */
typedef enum
{
    kCameraAppOff = 0,
    kCameraPreview = 3,
    kCameraRecording
}iap_gl_ipod_notification_camera_value_t;

/** iPod Notification - Charging Notification (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t chargingInfoType;
    uint16_t chargingInfoValue;
}iap_gl_ipod_notification_charging_info_t;

/** iPod Notification - Charging Notification Values (Lingo = 0x00, Cmd = 0x4A) Enumeration */
typedef enum
{
    kNotifyAvailableCurrent = 0
}iap_gl_ipod_notification_charging_value_t;

/** iPod Notification - Now Playing Application Bundle Name 
 * (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t applicationID[128];                /**< Variable length */
}iap_gl_ipod_notification_now_playing_application_bundle_t;

/** iPod Notification - Session Space Available (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint16_t sessionId;
}iap_gl_ipod_notification_session_space_t;

/** iPod Notification - Command Complete (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t lingoId;
    uint16_t commandId;
    uint8_t commandCompletionStatus;
}iap_gl_ipod_notification_command_complete_t;

/** iPod Notification - Command Complete Status (Lingo = 0x00, Cmd = 0x4A) Enumeration */
typedef enum
{
    kNotifyCommandSuccessful = 0,
    kNotifyCommandFailed,
    kNotifyCommandCancelled
}iap_gl_ipod_notification_command_complete_status_t;

/** iPod Notification - iPod Out Mode (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t status;
}iap_gl_ipod_notification_out_mode_t;

/** iPod Notification - Bluetooth Connection (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t macAddress[6];
    uint8_t profileID[8];
}iap_gl_ipod_notification_bluetooth_connection_t;

/** iPod Notification - Bluetooth Profile ID (Lingo = 0x00, Cmd = 0x4A) Enumeration */
typedef enum
{
    kBluetoothHandsFree = 0,
    kBluetoothPhoneBookAccess,
    kBluetoothAudioVideoRemoteControl = 3,
    kBluetoothAdvancedAudioDistribution,
    kBluetoothHumanInterfaceDevice,
    kBluetoothAppleIap = 7,
    kBluetoothNetworkSharing,
    kBluetoothNetworkConsumer = 12
}iap_gl_ipod_notification_bluetooth_profile_id_t;

/** iPod Notification - Now Playing Application Display Name 
 * (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t applicationName[128];                /**< Variable length */
}iap_gl_ipod_notification_now_playing_application_display_t;

/** iPod Notification - Assistive Touch (Lingo = 0x00, Cmd = 0x4A) Structure */ 
typedef struct
{
    uint8_t assistiveTouchStatus;
}iap_gl_ipod_notification_assistive_touch_t;

/** Return iPod Options for Lingo (Lingo = 0x00, Cmd = 0x4C) Structure */ 
typedef struct
{
    uint8_t lingoId;
    uint8_t optionBits[8];
}iap_gl_ret_ipod_options_for_lingo_t;

/** Return iPod Options for Lingo - General Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kGeneralLineOutUsage = 0,
    kGeneralVideoOutput,
    kGeneralNtscVideoSignalFormat,
    kGeneralPalVideoSignalFormat,
    kGeneralCompositeVideoOutConnection,
    kGeneralSVideoOutConnection,
    kGeneralComponentVideoOutConenction,
    kGeneralClosedCaptioning,
    kGeneralVideoAspectRatioFullscreen,
    kGeneralVideoAspectRatioWidescreen,
    kGeneralSubtitles,
    kGeneralVideoAlternateAudio,
    kGeneralCommunicationWithIosApp = 13,
    kGeneralAppleDeviceNotifications,
    kGeneralPauseOnPowerRemoval = 19,
    kGeneralRestrictedIdpsTokens = 23,
    kGeneralRequestApplicationLaunch,
    kGeneralAnalogAndUsbAudioSwitching = 26,
    kGeneralUsbHostModeInvokedByHardware,
    kGeneralUsbHostModeAudioOutputStreaming,
    kGeneralUsbHostModeAudioInputStreaming,
    kGeneralUsbHostModeMaxCurrentInLowpower
}iap_gl_general_lingo_option_bits_t;

/** Return iPod Options for Lingo -  Simple Remote Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kSimpleRemoteContextSpecificControls = 0,
    kSimpleRemoteAudioMediaControls,
    kSimpleRemoteVideoMediaControls,
    kSimpleRemoteImageMediaControls,
    kSimpleRemoteSportsMediaControls,
    kSimpleRemoteCameraMediaControls = 8,
    kSimpleRemoteUsbHidCommands,
    kSimpleRemoteVoiceOverControls,
    kSimpleRemoteVoiceOverPreferences,
    kSimpleRemoteAssistiveTouchCursor
}iap_gl_simple_remote_option_bits_t;

/** Return iPod Options for Lingo -  Display Remote Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kDisplayRemoteUiVolumeControl,
    kDisplayRemoteAbsoluteVolumeControl,
    kDisplayRemoteSupportsGeniusPlaylist
}iap_gl_display_remote_option_bits_t;

/** Return iPod Options for Lingo -  Extended Interface Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kExtendedInterfaceVideoBrowsing = 0,
    kExtendedInterfaceEnableCmds,
    kExtendedInterfaceNestedPlaylists,
    kExtendedInterfaceGeniusPlaylistCreationRefresh,
    kExtendedInterfaceImagesSentExtendedCmds,
    kExtendedInterfaceCategoriesAccess,
    kExtendedInterfacePlayPauseSupported,
    kExtendedInterfaceUidCmdsSupport
}iap_gl_extended_interface_option_bits_t;

/** Return iPod Options for Lingo -  USB Host Mode Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kUsbHostDeprecated = 0,
    kUsbHostModeInvoked
}iap_gl_usb_host_option_bits_t;

/** Return iPod Options for Lingo -  RF Tuner Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kRfTunerRawModeSupport = 0,
    kRfTunerHdRadioTuningSupport,
    kRfTunerAmRadioTuningSupport
}iap_gl_rf_tuner_option_bits_t;

/** Return iPod Options for Lingo -  Sports Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kSportNikeIpodCardio = 1
}iap_gl_sports_option_bits_t;

/** Return iPod Options for Lingo -  Digital Audio Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kDigitalAudioAVSynchronization = 0,
    kDigitalAudioAccessorySamplerateReporting = 2,
    kDigitalAudioAccessoryVideoSyncReporting
}iap_gl_digital_audio_option_bits_t;

/** Return iPod Options for Lingo -  Storage Lingo Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kStorageItunesTagging = 0,
    kStorageNikeIpodCardio
}iap_gl_storage_option_bits_t;

/** Return iPod Options for Lingo -  iPod Out Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kIpodOutUiModeAvailable = 0,
    kIpodOutPalVideoEnabled = 2
}iap_gl_ipod_out_option_bits_t;

/** Return iPod Options for Lingo -  Location Option Bits 
 * (Lingo = 0x00, Cmd = 0x4C) Enumeration */
typedef enum
{
    kIosAcceptsNmeaGps = 0,
    kIosSendLocationData
}iap_gl_location_option_bits_t;

/** Cancel Command (Lingo = 0x00, Cmd = 0x50) Structure */ 
typedef struct
{
    uint8_t lingoId;
    uint16_t commandId;
    uint16_t transactionID;
}iap_gl_cancel_command_t;

/** Set Internal Battery Charging State (Lingo = 0x00, Cmd = 0x56) Enumeration */
typedef enum
{
    kIosShouldNotChargeItsBattery = 0,
    kIosMayChargeItsBattery
}iap_gl_set_internal_battery_charge_states_t;

/** Request Application Launch (Lingo = 0x00, Cmd = 0x64) Structure */ 
typedef struct
{
    uint8_t reserved0;
    uint8_t retries;                 /**< Must be set to 0x02 */
    uint8_t reserved2;
    uint8_t appIDString[128];         /**< Variable length */
}iap_gl_request_application_launch_t;

/** Get Localization Info Types (Lingo = 0x00, Cmd = 0x67) Enumeration */ 
typedef enum
{
    kLocalizationLanguage = 0,
    kLocalizationRegion
}iap_gl_get_localization_info_t;

/** Return Localization Info (Lingo = 0x00, Cmd = 0x68) Structure */
/** Language Tags have the following format:
 * - Primary Language Subtag - Two letters -> 2 + hyphen
 * - Three optional extended language subtags, three letters each - Nine letters -> 9 + hyphen  
 * - Script subtag - Four letters -> 4 + hyphen
 * - Region subtag - Two to three letters -> 3 + hyphen 
 * - Variant Subtags - N subtags with Five to Eight letters -> Consider 8 + hyphen
 * - Extension subtags - single caracter with N fields separated by hyphens -> 1 + hyphen
 * - private use subtag - letter x and hyphen followed by 1 to n -> 1 + hyphen
 * Consider 3 + 10 + 5 + 4 + 9 + 2 + 2 = 35 
 * For safety an array of 48 will be considered */
typedef struct
{
    uint8_t infoType;
    uint8_t localizationInfo[48];             /**< Variable length */
}iap_gl_ret_localization_info_t;

/** WiFi Connection Info - Status (Lingo = 0x00, Cmd = 0x6A) Enumeration */
typedef enum
{
    kWifiConnectionSuccess = 0,
    kWifiConnectionInfoUnavailable,
    kWifiConnectionUserDeclined,
    kWifiConnectionCommandFailed
}iap_gl_wifi_connection_info_status_t;

/** WiFi Connection Info - Security (Lingo = 0x00, Cmd = 0x6A) Enumeration */
typedef enum
{
    kWifiConnectionUnsecured = 0,
    kWifiConnectionWep,
    kWifiConnectionWpa,
    kWifiConnectionWpa2,
    kWifiConnectionWpaAndWpa2
}iap_gl_wifi_connection_info_security_t;

/** WiFi Connection Info (Lingo = 0x00, Cmd = 0x6A) Structure */
typedef struct
{
    /*! @sa iap_gl_wifi_connection_info_status_t */
    uint8_t status;
    /*! @sa iap_gl_wifi_connection_info_security_t */
    uint8_t securityType;
    uint8_t ssid[32];
    uint8_t passPhrase[128];                /**< Variable length */
}iap_gl_wifi_connection_info_t;

/*! Type for the OpenDataSessionsForProtocol                                                0x3F*/
typedef struct
{
    /*! Session ID for the subsequent data transfer commands */
    uint16_t sessionID;
    /*! Index of the protocol for which session is being opened */
    uint8_t protocolIndex;
}iap_gl_data_session_for_protocol_t;

/*! Type for the Session id for closeDataSessionForProtocol and IpodDataTransfer      0x40, 0x43*/
typedef uint16_t iap_gl_session_id_t;

/*! Type for the SetAccessoryStatusNotification                                             0x46*/
typedef uint32_t iap_gl_set_accessory_status_notification;


typedef void (* iap_commands_data_transfer_callback_t)(iap_gl_sessions_for_protocol_rx_t* applicationData);

typedef void (* iap_general_lingo_ipod_notification_callback_t)(iap_gl_event_notification_t notificationType,
                                                                uint8_t* notificationPayload,
                                                                uint16_t payloadSize);

typedef void (* iap_general_lingo_wifi_connection_callback_t)(iap_gl_wifi_connection_info_t* wifiConnectionInfo);

#pragma pack (pop)

/*************************************************************************************************
*                                       Function-like Macros Section                           
*************************************************************************************************/

/*************************************************************************************************
*                                       Extern Constants Section                          
*************************************************************************************************/
/*! Array of pointers used to access the arrays that stores the FID token values during the IDPS
 * process */
extern const uint8_t* kIapCommandsFidTokenFields[];

/*! Array that contains the sizes of each FID token */
extern uint8_t kIapCommandsFidTokenSize[];

/*! Array that will be used by iAP_Commands.c, contains the values of the general lingo 
 * asynchronous commands */
extern const uint8_t kGeneralLingoAsynchronousCommands[NUMBER_OF_GENERALLINGO_ASYNCHRONOUS_COMMANDS];

/*! Array that will be used by iAP_Commands.c, contains the pointers to the general lingo
 * asynchronous commands functions */
extern  void (* kGeneralLingoAsyncrhonousFunctions[NUMBER_OF_GENERALLINGO_ASYNCHRONOUS_COMMANDS]) (iap_interface_buffer_rx_frame_t* rxFrame);

/*************************************************************************************************
*                                       Extern Variables Section                          
*************************************************************************************************/

#if(ACC_INFO_STATUS_TOKEN_MASK)
extern  uint32_t g_iapCommandsAccStatusNotificationMask;
extern uint32_t g_iapCommandsAccStatusNotification;

#if(BLUETOOTH_DEVICE_STATUS)
extern iap_gl_accessory_status_bluetooth_t g_iapCommandsAccStatusBluetooth;
#endif

#if(FAULT_CONDITION_STATUS)
extern iap_gl_accessory_status_fault_t g_iapCommandsAccStatusFault;
#endif
#endif

extern uint8_t g_iapChallengeData[21];

/*************************************************************************************************
*                                      Function Prototypes Section                        
*************************************************************************************************/

/*************************************************************************************************
*
* @brief    Set of datatransfer callbacks 
* @return   void
* @param    callbackId - This variables works as identifier of the application callback
* @sa       iap1_ios_applications_callbacks_Ids_t
* @param    appCallback - This is the address of the function
*           that will be used when an iPodDataTransfer is received
* @details  This function is used to set the application callbacks to decide which function to jump
*           when an asyncrhonous iPodDataTransfer is received
*          
**************************************************************************************************/
uint8_t iap_set_callback_app_data_transfer(iap1_ios_applications_callbacks_Ids_t callbackId, 
                                           iap_commands_data_transfer_callback_t appCallback);

/*************************************************************************************************
*
* @brief   Register a callback routine for the asynchronous commands of General lingo
* @param   uint8_t callbackId This variables works as identifier of the application callback
* @param   hostFunctionAddress Address of the function
* @return  Whether the callback was registered successfully      
* @retval  _OK_ The callback was registered successfully     
* @retval  _ERROR_ there was an error while registering the callback     
**************************************************************************************************/
uint8_t set_gl_asynchronous_command_callback
                    (uint8_t callbackId, iap_commands_asynchronous_callback_t hostFunctionAddress);

/*************************************************************************************************
*
* @brief   Register a callback routine for the asynchronous iPodNotification
* @param   callback the application function that will be called each time an iPodNotification
*          command is received
* @return  Whether the callback was registered successfully      
* @retval  _OK_ The callback was registered successfully     
* @retval  _ERROR_ there was an error while registering the callback     
**************************************************************************************************/
uint8_t set_gl_ipod_notification_callback(iap_general_lingo_ipod_notification_callback_t callback);

/*************************************************************************************************
*
* @brief   Register a callback routine for the asynchronous WifiConnectionInfo
* @param   callback the application function that will be called each time a WifiConnectionInfo
*          command is received
* @return  Whether the callback was registered successfully      
* @retval  _OK_ The callback was registered successfully     
* @retval  _ERROR_ there was an error while registering the callback     
**************************************************************************************************/
uint8_t set_gl_wifi_connection_info_callback(iap_general_lingo_wifi_connection_callback_t callback);

/*************************************************************************************************
*
* @brief   Restart some variables related with the communication with the ios application.
* @param   
* @return       
* @retval    
**************************************************************************************************/
void iap_general_lingo_reset(void);

/* General Lingo Commands ************************************************************************/
/*!
 * @brief                       General Lingo (0x00) RequestExtendedInterfaceMode command (0x03)     
 * @details                     Command to request enter into Extended Interface Mode
 * @param appDevMode            Pointer to where the received data will be stored
 * @sa                          iap_gl_return_extended_interface_mode_t
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_request_extended_interface_mode(iap_gl_return_extended_interface_mode_t* appDevMode,
                                                                        void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRequestExtendedInterfaceMode,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) &appDevMode,
                                                sizeof(uint8_t),
                                                kGeneralLingoReturnExtendedInterfaceMode};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) EnterExtendedInterfaceMode command (0x05),
 *                              Command to enter into Extended Interface Mode
 * @details                     Per note in section 3.3.39 this command is deprecated, but still works 
 *                              on Apple devices that support iPod Out
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_enter_extended_interface_mode(void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoEnterExtendedInterfaceMode,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) ExitExtendedInterfaceMode command (0x06)     
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_exit_extended_interface_mode(void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoExitExtendedInterfaceMode,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RequestIpodName command (0x07)     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_request_ipod_name(uint8_t* rxPayLoad,
                                                          uint16_t rxPayLoadSize,
                                                          void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRequestIpodName,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoReturnIpodName};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RequestIpodSoftwareVersion command (0x09)     
 * @param iPodSWVersion         Pointer to the structure that will store the received data
 * @sa                          iap_gl_ret_ipod_software_version_t
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_request_ipod_software_version
                                                (iap_gl_ret_ipod_software_version_t* iPodSWVersion,
                                                 void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRequestIpodSWVersion,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) iPodSWVersion,
                                                sizeof(iap_gl_ret_ipod_software_version_t),
                                                kGeneralLingoReturnIpodSWVersion};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RequestIpodSerialNum command (0x0B)     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_request_ipod_serial_num
        (uint8_t* rxPayLoad,
         uint16_t rxPayLoadSize,
         void (*rxCallback)(uint8_t))
{
    
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRequestIpodSerialNumber,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoReturnIpodSerialNumber};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RequestLingoProtocolVersion command (0x0F)     
 * @param lingoRequestInfo      The lingo for which to request version information
 * @param lingoProtocolVersion  Pointer to structure where the payload of the answer will be saved 
 * @sa                          iap_gl_ret_lingo_protocol_version_t
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_request_lingo_protocol_version
        (uint8_t lingoRequestInfo,
         iap_gl_ret_lingo_protocol_version_t* lingoProtocolVersion,
         void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                &lingoRequestInfo,
                                                sizeof(lingoRequestInfo),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRequestLingoProtocolVersion,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) lingoProtocolVersion,
                                                sizeof(iap_gl_ret_lingo_protocol_version_t),
                                                kGeneralLingoReturnLingoProtocolVersion};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RequestTransportMaxPayload command (0x11)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_request_max_payload_size(uint8_t* rxPayLoad,
                                                                 uint16_t rxPayLoadSize,
                                                                 void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRequestMaxPayloadSize,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoReturnMaxPayloadSize};

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) IdentifyDeviceLingoes command (0x13)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @sa                          iap_gl_identify_device_lingoes_t and iap_gl_identify_device_lingoes_options_t 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_identify_device_lingoes(uint8_t* txPayLoad,
                                                                uint16_t txPayLoadSize,
                                                                void (*rxCallback)(uint8_t))
{
	IAPI_DISABLE_TRANSACTION_ID;
	iap_commands_tx_command_t txCommandData = {{k1000msTimeout,
                                                txPayLoad,
                                                txPayLoadSize,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoIdentifyDeviceLingoes,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RetAccessoryAuthenticationInfo command (0x15)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_ret_accessory_authentication_info(uint8_t* txPayLoad,
                                                                          uint16_t txPayLoadSize,
                                                                          uint8_t* rxPayLoad,
                                                                          uint16_t rxPayLoadSize,
                                                                          void (*rxCallback)(uint8_t),
                                                                          uint8_t expectedCommand)
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                g_iapCommandsTransactionID,
                                                kAnswerPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRetAccessoryAuthenticationInfo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                expectedCommand};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RetAccessoryAuthenticationSignature command (0x18)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_ret_accessory_authentication_signature(uint8_t* txPayLoad,
                                                                               uint16_t txPayLoadSize,
                                                                               uint8_t* rxPayLoad,
                                                                               uint16_t rxPayLoadSize,
                                                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                g_iapCommandsTransactionID,
                                                kAnswerPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRetAccessoryAuthenticationSignature,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoAckAccessoryAuthenticationStatus};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetIpodAuthenticationInfo command (0x1A)     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_ipod_authentication_info(uint8_t* rxPayLoad,
                                                                     uint16_t rxPayLoadSize,
                                                                     uint16_t* ptrToSaveTotalPayloadSize,
                                                                     void (*rxCallback)(uint8_t))
{   
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoGetIpodAuthenticationInfo,
                                                (kCommandIsWaitingForResponse | kCommandWithMultisectionResponse)},
                                                rxCallback,
                                                ptrToSaveTotalPayloadSize,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoRetIpodAuthenticationInfo};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) AckiPodAuthenticationInfo command (0x1B)     
 * @param authenticationStatus  Status of the authentication information 
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_ack_ipod_authentication_info(uint8_t authenticationStatus,
                                                                     uint8_t* rxPayLoad,
                                                                     uint16_t rxPayLoadSize,
                                                                     void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                &authenticationStatus,
                                                sizeof(authenticationStatus),
                                                0,
                                                (kQuestionPacket | kDoNotIncrementTID),
                                                kGeneralLingo,
                                                kGeneralLingoAckIpodAuthenticationInfo,
                                                kCommandIsNotWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                0};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetiPodAuthenticationSignature command (0x1D)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @sa                          iap_gl_get_ipod_authentication_signature_t
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_ipod_authentication_signature(uint8_t* txPayLoad,
                                                                          uint16_t txPayLoadSize,
                                                                          uint8_t* rxPayLoad,
                                                                          uint16_t rxPayLoadSize,
                                                                          uint16_t* ptrToSaveTotalPayloadSize,
                                                                          void (*rxCallback)(uint8_t))
{
        iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                    txPayLoad,
                                                    txPayLoadSize,
                                                    0,
                                                    kQuestionPacket,
                                                    kGeneralLingo,
                                                    kGeneralLingoGetIpodAuthenticationSignature,
                                                    kCommandIsWaitingForResponse},
                                                    rxCallback,
                                                    ptrToSaveTotalPayloadSize,
                                                    rxPayLoad,
                                                    rxPayLoadSize,
                                                    kGeneralLingoRetIpodAuthenticationSignature};
                                                    
        return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) AckiPodAuthenticationStatus command (0x1F)     
 * @param authenticationStatus  Status of the authentication information 
 * @sa                          iap_gl_ack_ipod_authentication_info_values_t
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_ack_ipod_authentication_status(uint8_t authenticationStatus,
                                                                       uint8_t* rxPayLoad,
                                                                       uint16_t rxPayLoadSize,
                                                                       void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                &authenticationStatus,
                                                sizeof(authenticationStatus),
                                                0,
                                                (kQuestionPacket | kDoNotIncrementTID),
                                                kGeneralLingo,
                                                kGeneralLingoAckIpodAuthenticationStatus,
                                                kCommandIsNotWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                0};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetiPodOptions command (0x24)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @sa                          iap_gl_ret_ipod_option_values_t
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_ipod_options(uint8_t* txPayLoad,
                                                         uint16_t txPayLoadSize,
                                                         uint8_t* rxPayLoad,
                                                         uint16_t rxPayLoadSize,
                                                         void (*rxCallback)(uint8_t))
{
        iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                    txPayLoad,
                                                    txPayLoadSize,
                                                    0,
                                                    kQuestionPacket,
                                                    kGeneralLingo,
                                                    kGeneralLingoGetIpodOptions,
                                                    kCommandIsWaitingForResponse},
                                                    rxCallback,
                                                    NULL,
                                                    rxPayLoad,
                                                    rxPayLoadSize,
                                                    kGeneralLingoRetIpodOptions};
                                                    
        return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RetAccessoryInfo command (0x28)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @sa                          iap_gl_accessory_info_type_values_t,
 *                              iap_gl_ret_accessory_info_capabilities_t,
 *                              iap_gl_ret_accessory_info_name_t,
 *                              iap_gl_ret_accessory_info_minimum_ipod_firmware_version_supported_t,
 *                              iap_gl_ret_accessory_info_minimum_lingo_version_supported_t,
 *                              iap_gl_ret_accessory_info_firmware_version_t,
 *                              iap_gl_ret_accessory_info_hardware_version_t,
 *                              iap_gl_ret_accessory_info_manufacturer_t,
 *                              iap_gl_ret_accessory_model_number_t,
 *                              iap_gl_ret_accessory_info_serial_number_t,
 *                              iap_gl_ret_accessory_info_incoming_max_payload_t,
 *                              iap_gl_accessory_status_notification_t,
 *                              iap_gl_accessory_fault_condition_value_t,
 *                              iap_gl_ret_accessory_info_status_supported_t
 *                              
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @sa                          iap_gl_accessory_info_type_values_t,
 *                              iap_gl_get_accessory_info_minimum_lingo_version_t,
 *                              iap_gl_get_accessory_info_minimum_ipod_firmware_version_t
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_ret_accessory_info(uint8_t* txPayLoad,
                                                           uint16_t txPayLoadSize,
                                                           uint8_t* rxPayLoad,
                                                           uint16_t rxPayLoadSize,
                                                           void (*rxCallback)(uint8_t))
{
    /* The TID value will be depreciated because the command is only used with Apple devices
     *  that does not support the IDPS process*/
    iap_commands_tx_command_t txCommandData = {{kNoTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                g_iapCommandsTransactionID, 
                                                kAnswerPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRetAccessoryInfo,
                                                kCommandIsNotWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                0};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetiPodPreferences command (0x29)     
 * @param classId               Id of the class for which to get the current preferences
 * @sa                          iap_gl_ipod_preference_class_id_t     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @sa                          iap_gl_ipod_preference_video_out_setting_values_t,
 *                              iap_gl_ipod_preference_screen_configuration_values_t,
 *                              iap_gl_ipod_preference_line_out_usage_values_t,
 *                              iap_gl_ipod_preference_video_out_connection_values_t,
 *                              iap_gl_ipod_preference_closed_captioning_values_t,
 *                              iap_gl_ipod_preference_aspect_ratio_values_t,
 *                              iap_gl_ipod_preference_subtitles_values_t,
 *                              iap_gl_ipod_preference_video_alternate_audio_values_t,
 *                              iap_gl_ipod_preference_pause_on_power_removal_values_t,
 *                              iap_gl_ipod_preference_voice_over_preference_values_t,
 *                              iap_gl_ipod_preference_assistive_touch_values_t,
 *                              iap_gl_ret_ipod_preference_t
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_ipod_preferences(iap_gl_ipod_preference_class_id_t classId,
                                                             uint8_t* rxPayLoad,
                                                             uint16_t rxPayLoadSize,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                (uint8_t*) &classId,
                                                sizeof(classId),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoGetIpodPreferences,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoRetIpodPreferences};
                                                
    return iap_commands_transmit_command(&txCommandData);                                             
}

/*!
 * @brief                       General Lingo (0x00) SetiPodPreferences command (0x2B)     
 * @param ipodPreference        pointer to structure that contains the information to set an 
 *                              iPod preference
 * @sa                          iap_gl_set_ipod_preference_t and iap_gl_ipod_preference_class_id_t 
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_set_ipod_preferences(iap_gl_set_ipod_preference_t* iPodPreference,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) iPodPreference,
                                                sizeof(iap_gl_set_ipod_preference_t),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoSetIpodPreferences,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetUIMode command (0x35)         
 * @param deviceCurrentMode     Pointer to where the received data will be stored
 * @sa                          iap_gl_operating_user_interface_mode_t
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_ui_mode(iap_gl_operating_user_interface_mode_t* deviceCurrentMode,
                                                    void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoGetUIMode,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) &deviceCurrentMode,
                                                sizeof(uint8_t),
                                                kGeneralLingoRetUIMode};
    
       return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) SetUIMode command (0x37)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_set_ui_mode(iap_gl_operating_user_interface_mode_t modeToSet,
                                                    void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                (uint8_t*) &modeToSet,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoSetUIMode,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) StartIDPS command (0x38)         
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_start_idps(void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoStartIDPS,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) SetFIDTokenValues command (0x39)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_set_fid_token_values(uint8_t* txPayLoad,
                                                             uint16_t txPayLoadSize,
                                                             uint8_t* rxPayLoad,
                                                             uint16_t rxPayLoadSize,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoSetFIDTokenValues,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoAckFIDTokenValues};

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) EndIDPS command (0x3B)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @sa                          iap_gl_accessory_end_idps_t
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @sa                          iap_gl_idps_status_value_t
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_end_idps(iap_gl_accessory_end_idps_t accEndIDPSStatus,
                                                 iap_gl_idps_status_t* endIDPSStatus,
                                                 void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                (uint8_t*) &accEndIDPSStatus,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoEndIDPS,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) endIDPSStatus,
                                                sizeof(uint8_t),
                                                kGeneralLingoIDPSStatus};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) AccessoryAck command (0x41)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_accessory_ack(iap_gl_accessory_ack_t* ackPayload)
{
    iap_commands_tx_command_t txCommandData = {{kNoTimeout,                
                                                (uint8_t*) ackPayload,
                                                sizeof(iap_gl_accessory_ack_t),
                                                g_iapCommandsTransactionID,
                                                kAnswerPacket,
                                                kGeneralLingo,
                                                kGeneralLingoAccessoryAck,
                                                kCommandIsNotWaitingForResponse},
                                                NULL,
                                                NULL,
                                                NULL,
                                                0,
                                                0};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) AccessoryDataTransfer command (0x42)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @sa                          iap_gl_ipod_data_transfer_packet_t
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t gl_accessory_data_transfer(iap1_ios_applications_callbacks_Ids_t appID,
                                                         iap_gl_app_data_transfer_t* txAppData,
                                                         uint8_t* rxPayLoad,
                                                         uint16_t rxPayLoadSize,
                                                         void (*rxCallback)(uint8_t));

/*!
 * @brief                       General Lingo (0x00) RetAccessoryStatusNotification command (0x47)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @sa                          iap_gl_accessory_bluetooth_device_t,
 *                              iap_gl_accessory_bluetooth_device_state_t,
 *                              iap_gl_accessory_bluetooth_device_class_t,
 *                              iap_gl_accessory_bluetooth_pairing_t,
 *                              iap_gl_accessory_status_bluetooth_t,
 *                              
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_ret_accessory_status_notification(uint32_t notificationMask)
{
    iap_commands_tx_command_t txCommandData = {{kNoTimeout,                
                                                (uint8_t*) &notificationMask,
                                                sizeof(notificationMask),
                                                g_iapCommandsTransactionID,
                                                kAnswerPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRetAccessoryStatusNotification,
                                                kCommandIsNotWaitingForResponse},
                                                NULL,
                                                NULL,
                                                NULL,
                                                0,
                                                0};
    
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(notificationMask, notificationMask);
#endif
    
   return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) AccessoryStatusNotification command (0x48)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_accessory_status_notification(uint8_t* txPayLoad,
                                                                      uint16_t txPayLoadSize)
{
    iap_commands_tx_command_t txCommandData = {{kNoTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoAccessoryStatusNotification,
                                                kCommandIsNotWaitingForResponse},
                                                NULL,
                                                NULL,
                                                NULL,
                                                0,
                                                0};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) SetEventNotification command (0x49)         
 * @param notificationBitMask   Contains a bit mask with the events notifications to be set
 * @sa                          iap_gl_set_event_notification_bitmask_value_t
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t gl_set_event_notification(uint32_t notificationBitMask,
                                                        void (*rxCallback)(uint8_t));


/*!
 * @brief                       General Lingo (0x00) GetiPodOptionsForLingo command (0x4B)         
 * @param txPayLoad             Pointer to the buffer that stores the payload to be transmitted
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @sa                          iap_gl_ret_ipod_options_for_lingo_t,
 *                              iap_gl_general_lingo_option_bits_t,
 *                              iap_gl_simple_remote_option_bits_t,
 *                              iap_gl_display_remote_option_bits_t,
 *                              iap_gl_extended_interface_option_bits_t,
 *                              iap_gl_usb_host_option_bits_t,
 *                              iap_gl_rf_tuner_option_bits_t,
 *                              iap_gl_sports_option_bits_t,
 *                              iap_gl_digital_audio_option_bits_t,
 *                              iap_gl_storage_option_bits_t,
 *                              iap_gl_ipod_out_option_bits_t,
 *                              iap_gl_location_option_bits_t
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_ipod_options_for_lingo(uint8_t* txPayLoad,
                                                                   uint16_t txPayLoadSize,
                                                                   uint8_t* rxPayLoad,
                                                                   uint16_t rxPayLoadSize,
                                                                   void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoGetIpodOptionsForLingo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoRetIpodOptionsForLingo};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetEventNotification command (0x4D)     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_event_notification(uint8_t eventNotificationMask[8],
                                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoGetEventNotification,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                eventNotificationMask,
                                                8,
                                                kGeneralLingoRetEventNotification};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetSupportedEventNotification command (0x4F)     
 * @param eventNotificationMask Buffer where the received event notification mask will be saved 
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_supported_event_notification(uint8_t eventNotificationMask[8],
                                                                         void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoGetSupportedEventNotification,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                eventNotificationMask,
                                                8,
                                                kGeneralLingoRetSupportedEventNotification};
    
    return iap_commands_transmit_command(&txCommandData);
}

/* TODO command not supported yet, consider this command when implementing multipacket 
 * and multisection */
/*!
 * @brief                       General Lingo (0x00) CancelCommand command (0x50)     
 * @param lingoId               Lingo of the commands being cancelled
 * @param commandId             Command being cancelled
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_cancel_command(uint8_t lingoId,
                                                       uint16_t commandId,
                                                       uint8_t* rxPayLoad,
                                                       uint16_t rxPayLoadSize,
                                                       void (*rxCallback)(uint8_t))
{    
    (void) lingoId;
    (void) commandId;
    (void) rxPayLoad;
    (void) rxPayLoadSize;
    (void) rxCallback;
    
    return (kTransmissionInvalidCommand);
}

/*!
 * @brief                       General Lingo (0x00) SetAvailableCurrent command (0x54)     
 * @param currentLimit          Maximum current in mA that the Apple device may draw
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_set_available_current(uint16_t currentLimit,
                                                              void (*rxCallback)(uint8_t))
{  
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &currentLimit, 
                                                sizeof(currentLimit),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoSetAvailableCurrent,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP16(currentLimit, currentLimit);
#endif 
    
    return iap_commands_transmit_command(&txCommandData);
    
}

/*!
 * @brief                       General Lingo (0x00) SetInternalBatteryChargingState command (0x56)     
 * @param chargingState         Charging state requested
 * @sa                          iap_gl_set_internal_battery_charge_states_t
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_set_internal_battery_charging_state
                                         (iap_gl_set_internal_battery_charge_states_t chargingState,
                                          void (*rxCallback)(uint8_t))
{

    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &chargingState,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoSetInternalBatteryChargingState,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) RequestApplicationLaunch command (0x64)       
 * @details                       
 * @param appToLaunchInfo       Pointer to the structure that stores the payload to be transmitted
 * @sa                          iap_gl_request_application_lauch_t
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_request_application_launch
                                              (iap_gl_request_application_launch_t* appToLaunchInfo,
                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                (uint8_t*) appToLaunchInfo,
                                                sizeof(iap_gl_request_application_launch_t),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRequestApplicationLaunch,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
    
    appToLaunchInfo->retries = 0x02;
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetNowPlayingApplicationBundleName command (0x65)     
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_now_playing_application_bundle_name(uint8_t* rxPayLoad,
                                                                                uint16_t rxPayLoadSize,
                                                                                void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoGetNowPlayingApplicationBundleName,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoRetNowPlayingApplicationBundleName};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       General Lingo (0x00) GetLocalizationInfo command (0x67)     
 * @param localizationType      Localization type
 * @sa                          iap_gl_get_localization_info_t
 * @param retLocalizationInfo   Pointer to the structure where the payload of the answer will be saved 
 * @sa                          iap_gl_ret_localization_info_t
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_get_localization_info
                                               (iap_gl_get_localization_info_t localizationType,
                                                iap_gl_ret_localization_info_t* retLocalizationInfo,
                                                void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                (uint8_t*) &localizationType,
                                                sizeof(localizationType), 
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoGetLocalizationInfo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) retLocalizationInfo,
                                                sizeof(iap_gl_ret_localization_info_t),
                                                kGeneralLingoRetLocalizationInfo};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*
 * TODO after sending this command the apple device sends an iPodAck to notify that it has received
 * the command and only after the user allows to share the wifi information in the apple device, it 
 * sends the WiFiConnectionInfo command
 */
/*!
 * @brief                       General Lingo (0x00) RequestWifiConnectionInfo command (0x69)     
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t gl_request_wifi_connection_info(void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0, 
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoRequestWiFiConnectionInfo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoWiFiConnectionInfo};
    
    return iap_commands_transmit_command(&txCommandData);
}

/* End of General Lingo Commands ******************************************************************/

#if(BIG_ENDIAN_CORE == _FALSE_)

/*!
*  @addtogroup  GeneralLingoDataAccessors General Lingo Data Accessors
*  @brief       These are the macros to obtain the 16 & 32 bits fields from the structures  
*               used, in certain commands from the General lingo, to store the received data  
*  @{  
*/
/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x03*/
static inline  uint16_t get_transport_max_payload_size (uint32_t* maxPayloadSize)
{
    return BYTESWAP16(*maxPayloadSize, *maxPayloadSize);
}

/*! The argument for these macros is a pointer to
 * iap_gl_get_accessory_info_minimum_ipod_firmware_version_t structure                      0x27*/
static inline  uint32_t get_accessory_info_apple_device_model_id
                (iap_gl_get_accessory_info_minimum_ipod_firmware_version_t* accessoryInfo)
{
    return BYTESWAP32(accessoryInfo->appleDeviceModelID, accessoryInfo->appleDeviceModelID);
}

/*! The argument for these macros is a pointer to
 * iap_gl_data_session_for_protocol_t structure                                             0x3F*/
static inline  uint16_t get_open_data_session_id(iap_gl_data_session_for_protocol_t* dataSession)
{
    return BYTESWAP16(dataSession->sessionID, dataSession->sessionID);
}

/*! The argument for these macros is a 
 * pointer to uint32_t type                                                    0x3F, 0x40 & 0x43*/
static inline  uint16_t get_session_id (uint16_t* dataSession)
{
    return BYTESWAP16(*dataSession, *dataSession);
}

/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x46*/
static inline  uint32_t get_accessory_status_notification (uint32_t* statusNotification)
{
    return BYTESWAP32(*statusNotification, *statusNotification);
}

/*! The argument for these macros is a pointer to
 * iap_gl_ipod_notification_flow_control_t structure                                        0x4A*/
static inline  uint32_t get_ipod_notification_for_flow_control_wait_time
                (iap_gl_ipod_notification_flow_control_t* ipodNotification)
{
    return BYTESWAP32(ipodNotification->waitTime, ipodNotification->waitTime);
}

static inline  uint16_t get_ipod_notification_for_flow_control_overflow_tid
                (iap_gl_ipod_notification_flow_control_t* ipodNotification)
{
    return BYTESWAP16(ipodNotification->overFlowTransactionId,
            ipodNotification->overFlowTransactionId);
}
/*! The argument for these macros is a pointer to
 * iap_gl_ipod_notification_charging_info_t structure */
static inline  uint16_t get_ipod_notification_charging_info_value
                (iap_gl_ipod_notification_charging_info_t* ipodNotification)
{
    return BYTESWAP16(ipodNotification->chargingInfoValue, ipodNotification->chargingInfoValue);
}

/*! The argument for these macros is a pointer to
 * iap_gl_ipod_notification_session_space_t structure */
static inline  uint16_t get_ipod_notification_for_session_space_available_id
                (iap_gl_ipod_notification_session_space_t* ipodNotification)
{
    return BYTESWAP16(ipodNotification->sessionId, ipodNotification->sessionId);
}

/*! The argument for these macros is a pointer to
 * iap_gl_ipod_notification_command_complete_t structure */
static inline  uint16_t get_ipod_notification_for_command_complete_cmd_id
                (iap_gl_ipod_notification_command_complete_t* ipodNotification)
{
    return BYTESWAP16(ipodNotification->commandId, ipodNotification->commandId);
}

#endif
/*!     @} 
* Group GeneralLingoDataAccessors
*/

/*! @} 
* Group GeneralLingo
* @}  
*
* Group iAP1_Lingoes
*/

#endif /* GENERALLINGO_H_ */
/*************************************************************************************************
* EOF                     
*************************************************************************************************/
