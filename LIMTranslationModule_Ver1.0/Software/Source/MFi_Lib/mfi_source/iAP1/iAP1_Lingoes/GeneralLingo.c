/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

/*************************************************************************************************
*                                      Includes Section                        
*************************************************************************************************/
#include "GeneralLingo.h"
#include "iAP1_Config.h"

/*************************************************************************************************
*                                   Defines & Macros Section                   
*************************************************************************************************/

/*********************************************************************************/
/************     Accessory RF Certifications definitions Begin     **************/
/*********************************************************************************/
#if(BIG_ENDIAN_CORE)
#define IAP_CMD_ACC_RF_BITMASK                0x00,0x00,0x00, \
        ((RF_CLASS1_MASK << kRfClass1) \
                | (RF_CLASS2_MASK << kRfClass2) \
                | (RF_CLASS4_MASK << kRfClass4) \
                | (RF_CLASS5_MASK << kRfClass5))
#else
#define IAP_CMD_ACC_RF_BITMASK                ((RF_CLASS1_MASK << kRfClass1) \
        | (RF_CLASS2_MASK << kRfClass2) \
        | (RF_CLASS4_MASK << kRfClass4) \
        | (RF_CLASS5_MASK << kRfClass5)), \
        0x00,0x00,0x00
#endif
/*********************************************************************************/
/************      Accessory RF Certifications definitions End      **************/
/*********************************************************************************/

/*! Size of payload for bluetooth ipod notification */
#define IPOD_NOTIFICATION_BLUETOOTH_PAYLOAD_SIZE     (15)
#define IPOD_NOTIFICATION_BLUETOOTH_MAC_ADDR_SIZE    (6)
#define IPOD_NOTIFICATION_BLUETOOTH_STATUS_SIZE      (8) 

/*! Number of bytes that must be skipped in the OpenDataSessionForProtocol,
 *  CloseDataSessionForProtocol and iPodDataTransfer in order to access to the session ID*/
#define SESSION_ID_PAYLOAD_OFFSET           0

/*! Number of bytes that must be skipped in the OpenDataSessionForProtocol in order to access 
 *  to the protocol index */
#define PROTOCOL_INDEX_PAYLOAD_OFFSET       2

/*************************************************************************************************
*                                       Typedef Section                        
*************************************************************************************************/



/*************************************************************************************************
*                                  Function Prototypes Section                 
*************************************************************************************************/

/* General Lingo asynchronous commands **********************************************************/

static void s_iap_general_lingo_request_identify
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_get_accessory_authentication_info       
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_get_accessory_authentication_signature  
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_notify_ipod_state_change                
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_get_accessory_info                      
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_open_data_session_for_protocol          
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_close_data_session                      
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_ipod_data_transfer                      
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_set_accessory_status_notification       
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_ipod_notification                       
        (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_general_lingo_wifi_connection_info 
        (iap_interface_buffer_rx_frame_t* rxFrame);

/* General Lingo end of asynchronous commands ***************************************************/   

/*************************************************************************************************
*                                   Static Constants Section                   
*************************************************************************************************/
#if (IAP_GENERALLINGO_MASK)
static const uint8_t s_kIapCommandsGeneralLingoMinimumVersion[] =
{
        0x03,kGeneralLingo,0x01,0x00
};
#endif
#if (IAP_SIMPLEREMOTELINGO_MASK)
static const uint8_t s_kIapCommandsSimpleRemoteLingoMinimumVersion[] =
{
        0x03,kSimpleRemoteLingo,0x01,0x00
};
#endif
#if (IAP_DISPLAYREMOTELINGO_MASK)
static const uint8_t s_kIapCommandsDisplayRemoteLingoMinimumVersion[] =
{
        0x03,kDisplayRemoteLingo,0x01,0x00
};
#endif
#if (IAP_EXTENDEDINTERFACELINGO_MASK)
static const uint8_t s_kIapCommandsExtendedInterfaceLingoMinimumVersion[] =
{
        0x03,kExtendedInterfaceLingo,0x01,0x00
};
#endif
#if (IAP_USBHOSTMODELINGO_MASK)
static const uint8_t s_kIapCommandsUsbHostModeLingoMinimumVersion[] =
{
        0x03,kUSBHostModeLingo,0x01,0x00
};
#endif
#if (IAP_RFTUNERLINGO_MASK)
static const uint8_t s_kIapCommandsRfTunerLingoMinimumVersion[] =
{
        0x03,kRFTunerLingo,0x01,0x00
};
#endif
#if (IAP_SPORTSLINGO_MASK)
static const uint8_t s_kIapCommandsSportsLingoMinimumVersion[] =
{
        0x03,kSportsLingo,0x01,0x00
};
#endif
#if (IAP_DIGITALAUDIOLINGO_MASK)
static const uint8_t s_kIapCommandsDigitalAudioLingoMinimumVersion[] =
{
        0x03,kDigitalAudioLingo,0x01,0x00
};
#endif
#if (IAP_STORAGELINGO_MASK)
static const uint8_t s_kIapCommandsStorageLingoMinimumVersion[] =
{
        0x03,kStorageLingo,0x01,0x00
};
#endif
#if (IAP_IPODOUTLINGO_MASK)
static const uint8_t s_kIapCommandsIpodOutLingoMinimumVersion[] =
{
        0x03,kIpodOutLingo,0x01,0x00
};
#endif
#if (IAP_LOCATIONLINGO_MASK)
static const uint8_t s_kIapCommandsLocationLingoMinimumVersion[] =
{
        0x03,kLocationLingo,0x01,0x00
};
#endif


static const uint8_t *s_kIapCommandsLingoesMinimumVersion[] =
{
#if (IAP_GENERALLINGO_MASK)
        (uint8_t*)(&s_kIapCommandsGeneralLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
        (uint8_t*)(NULL),                        /* Lingo does not exist */
#if (IAP_SIMPLEREMOTELINGO_MASK)
        (uint8_t*)(&s_kIapCommandsSimpleRemoteLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_DISPLAYREMOTELINGO_MASK)
        (uint8_t*)(&s_kIapCommandsDisplayRemoteLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_EXTENDEDINTERFACELINGO_MASK)
        (uint8_t*)(&s_kIapCommandsExtendedInterfaceLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
        (uint8_t*)(NULL),                        /* Lingo does not exist */
#if (IAP_USBHOSTMODELINGO_MASK)
        (uint8_t*)(&s_kIapCommandsUsbHostModeLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_RFTUNERLINGO_MASK)
        (uint8_t*)(&s_kIapCommandsRfTunerLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
        (uint8_t*)(NULL),                        /* Lingo does not exist */
#if (IAP_SPORTSLINGO_MASK)
        (uint8_t*)(&s_kIapCommandsSportsLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_DIGITALAUDIOLINGO_MASK)
        (uint8_t*)(&s_kIapCommandsDigitalAudioLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
        (uint8_t*)(NULL),                        /* Lingo does not exist */
#if (IAP_STORAGELINGO_MASK)
        (uint8_t*)(&s_kIapCommandsStorageLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_IPODOUTLINGO_MASK)
        (uint8_t*)(&s_kIapCommandsIpodOutLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_LOCATIONLINGO_MASK)
        (uint8_t*)(&s_kIapCommandsLocationLingoMinimumVersion[0]),
#else
        (uint8_t*)(NULL),
#endif
};

/* Accessory name, Manufacture, model and serial number string size must be between 1 and 64 */
/*! Identify token array */
static const uint8_t s_kFidIdentifyTokenValues[IDENTIFY_TOKEN_SIZE] =
{
#if (IDENTIFY_TOKEN_MASK)
        (0x0B + IAP_CMD_NUM_LINGOES),           /* length of this token (1byte) */
        0x00,                                   /* FIDType (1 byte)= 0x00 */ 
        kIdentifyTokenFIDSubtype,               /* FIDSubtype (1 byte) = 0x00 */ 
        IAP_CMD_NUM_LINGOES,                    /* numLingoes (1 byte) */
#if (IAP_GENERALLINGO_MASK)
        kGeneralLingo,
#endif
#if (IAP_SIMPLEREMOTELINGO_MASK)
        kSimpleRemoteLingo,
#endif
#if (IAP_DISPLAYREMOTELINGO_MASK)
        kDisplayRemoteLingo,
#endif
#if (IAP_EXTENDEDINTERFACELINGO_MASK)
        kExtendedInterfaceLingo,
#endif
#if (IAP_USBHOSTMODELINGO_MASK)
        kUSBHostModeLingo,
#endif
#if (IAP_RFTUNERLINGO_MASK)
        kRFTunerLingo,
#endif
#if (IAP_SPORTSLINGO_MASK)
        kSportsLingo,
#endif
#if (IAP_DIGITALAUDIOLINGO_MASK)
        kDigitalAudioLingo,
#endif
#if (IAP_STORAGELINGO_MASK)
        kStorageLingo,
#endif
#if (IAP_IPODOUTLINGO_MASK)
        kIpodOutLingo,
#endif
#if (IAP_LOCATIONLINGO_MASK)
        kLocationLingo,
#endif
        0x00, 0x00, 0x00,0x02,              /* DeviceOptions (4 bytes) = 0x00000002 (Authentication 2.0) */ 
        0x00, 0x00, 0x02, 0x00              /* DeviceID (4 bytes), shall come from the CP */
#endif
};

/*! Accessory Capability token array */
static const uint8_t s_kFidAccCapabilityTokenValues[ACC_CAPABILITY_TOKEN_SIZE] =
{
    #if (ACC_CAPABILITY_TOKEN_MASK)
        0x0A,                               /* Length (1 byte) = 0x0A */
        0x00,                               /* FIDType = 0x00 */
        kAccessoryCapsTokenFIDSubtype,      /* FIDSubtype = 0x01 */  
        IAP_CMD_ACC_CAP_BITMASK             /* accCapsBitmask (8 bytes) */
    #endif
};

#if (ACC_INFO_TOKEN_MASK)

/*! Accessory Info Name token array */
static const uint8_t s_kFidAccInfoNameTokenValues[ACC_INFO_NAME_TOKEN_SIZE] =
{
    #if (ACC_INFO_NAME_TOKEN_MASK)
        (ACC_INFO_NAME_TOKEN_SIZE - 1),     /* length (1 byte) = Accessory Name + 3 = 0x1C */
        0x00,                               /* FIDType (1 byte) = 0x00 */  
        kAccessoryInfoTokenFIDSubtype,      /* FIDSubtype (1 byte) = 0x02 */  
        0x01,                               /* accInfoType (1 byte) = 0x01 */
        ACC_INFO_NAME_STRING_DATA           /* accInfo = "Freescale Test Accessory" */
    #endif
};

/*! Accessory info Firmware version token array */
static const uint8_t s_kFidAccInfoFirmwareVersionTokenValues[ACC_INFO_FIRMWARE_VERSION_TOKEN_SIZE] =
{
    #if (ACC_INFO_FIRMWARE_VERSION_TOKEN_MASK)
        (ACC_INFO_FIRMWARE_VERSION_TOKEN_SIZE - 1), /* length (1 byte) = 0x06 */
        0x00,                                       /* FIDType (1 byte) = 0x00 */ 
        kAccessoryInfoTokenFIDSubtype,              /* FIDSubtype (1 byte) = 0x02 */ 
        0x04,                                       /* accInfoType (1 byte) = 0x04 */ 
        ACC_INFO_FIRMWARE_VERSION_STRING_DATA       /* accInfo (3 bytes) */ 
    #endif
};

/*! Accessory info hardware version token array */
static const uint8_t s_kFidAccInfoHardwareVersionTokenValues[ACC_INFO_HARDWARE_VERSION_TOKEN_SIZE] =
{
    #if (ACC_INFO_HARDWARE_VERSION_TOKEN_MASK)
        (ACC_INFO_HARDWARE_VERSION_TOKEN_SIZE - 1), /* length (1 byte) = 0x06 */
        0x00,                                       /* FIDType (1 byte) = 0x00 */ 
        kAccessoryInfoTokenFIDSubtype,              /* FIDSubtype (1 byte) = 0x02 */ 
        0x05,                                       /* accInfoType (1 byte) = 0x05 */
        ACC_INFO_HARDWARE_VERSION_STRING_DATA       /* accInfo (3 bytes) */
    #endif
};

/*! Accessory info manufacturer token array */
static const uint8_t s_kFidAccInfoManufacturerTokenValues[ACC_INFO_MANUFACTURER_TOKEN_SIZE] =
{
    #if (ACC_INFO_MANUFACTURER_TOKEN_MASK)
        (ACC_INFO_MANUFACTURER_TOKEN_SIZE - 1),    /* length (1 byte) = Accessory Manufacturer + 3 = 0x0D */
        0x00,                                   /* FIDType (1 byte) = 0x00 */
        kAccessoryInfoTokenFIDSubtype,          /* FIDSubtype (1 byte) = 0x02 */ 
        0x06,                                   /* accInfoType (1 byte) = 0x06 */
        ACC_INFO_MANUFACTURER_STRING_DATA       /* accInfo = "Freescale" */
    #endif
};

/*! Accessory info model number token array */
static const uint8_t s_kFidAccInfoModelNumberTokenValues[ACC_INFO_MODEL_NUMBER_TOKEN_SIZE] =
{
    #if (ACC_INFO_MODEL_NUMBER_TOKEN_MASK)
        (ACC_INFO_MODEL_NUMBER_TOKEN_SIZE - 1), /* length (1 byte) = Accessory Model + 3 = 0x08 */
        0x00,                                   /* FIDType (1 byte) = 0x00 */
        kAccessoryInfoTokenFIDSubtype,          /* FIDSubtype (1 byte) = 0x02 */ 
        0x07,                                   /* accInfoType (1 byte) = 0x07 */
        ACC_INFO_MODEL_NUMBER_STRING_DATA       /* accInfo = "Test" */
    #endif
};

/*! Accessory info serial number token array */
static const uint8_t s_kFidAccInfoSerialNumberTokenValues[ACC_INFO_SERIAL_NUMBER_TOKEN_SIZE + 1] =
{
    #if (ACC_INFO_SERIAL_NUMBER_TOKEN_MASK)
        (ACC_INFO_SERIAL_NUMBER_TOKEN_SIZE - 1), /* length (1 byte) = Accessory Serial + 3 = 0x0A */
        0x00,                                   /* FIDType (1 byte) = 0x00 */
        kAccessoryInfoTokenFIDSubtype,          /* FIDSubtype (1 byte) = 0x02 */ 
        0x08,                                   /* accInfoType (1 byte) = 0x08 */
        ACC_INFO_SERIAL_NUMBER_STRING_DATA      /* accInfo = "000001" */
    #endif
};

/*! Accessory info Max Payload size token array */
static const uint8_t s_kFidAccInfoMaxPayloadSizeTokenValues[ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_SIZE + 1] =
{
    #if (ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kAccessoryInfoTokenFIDSubtype,      /* FIDSubtype (1 byte) = 0x02 */
        0x09,                               /* accInfoType (1 byte) = 0x09 */
        #if(BIG_ENDIAN_CORE)
            (uint8_t)(IAP_CMD_ACC_MAX_PAYLOAD >> 8),
            (uint8_t)(IAP_CMD_ACC_MAX_PAYLOAD & 0x00FF),    /* accInfo (2 bytes), min=128, max=65529 */
        #else
            (uint8_t)(IAP_CMD_ACC_MAX_PAYLOAD & 0x00FF),
            (uint8_t)(IAP_CMD_ACC_MAX_PAYLOAD >> 8),
        #endif
    #endif
};

/*! Accessory info status token array */
static const uint8_t s_kFidAccInfoStatusTokenValues[ACC_INFO_STATUS_TOKEN_SIZE + 1] =
{
    #if (ACC_INFO_STATUS_TOKEN_MASK)
        0x07,                               /* length (1 byte) = 0x07 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kAccessoryInfoTokenFIDSubtype,      /* FIDSubtype (1 byte) = 0x02 */
        0x0B,                               /* accInfoType (1 byte) = 0x0B */
        IAP_CMD_ACC_STATUS_BITMASK          /* accInfo (4 bytes) */
    #else
        0
    #endif
};

/*! Accessory info Rf Certification token array */
static const uint8_t s_kFidAccInfoRfCertificationTokenValues[ACC_INFO_RF_CERTIFICATION_TOKEN_SIZE] =
{
    #if (ACC_INFO_RF_CERTIFICATION_TOKEN_MASK)
        0x07,                               /* length (1 byte) = 0x07 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kAccessoryInfoTokenFIDSubtype,      /* FIDSubtype (1 byte) = 0x02 */
        0x0C,                               /* accInfoType (1 byte) = 0x0C */
        IAP_CMD_ACC_RF_BITMASK             /* accInfo (4 bytes) */
    #else
        0
    #endif
};
        
#endif
        
#if (IPOD_PREFERENCE_TOKEN_MASK)
        
/*! iPod Preference Video out token array */
static const uint8_t s_kFidIpodPrefVideoOutTokenValues[IPOD_PREF_VIDEO_OUT_SETTINGS_SIZE + 1] =
{
    #if (IPOD_PREF_VIDEO_OUT_SETTINGS_MASK)
        0x05,                      /* length (1 byte) = 0x05 */
        0x00,                      /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,  /* FIDSubtype (1 byte) = 0x03 */
        kClassVideoOut,           /* iPodPrefClass (1 byte) = 0x00 (Video Out Settings) */
        DEFAULT_VIDEO_OUT,         /* prefClassSetting (1 byte) */
        0x01                       /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};   

/*! iPod Preference Screen Configuration token array */
static const uint8_t s_kFidIpodPrefScreenConfigTokenValues[IPOD_PREF_SCREEN_CONFIGURATION_SIZE + 1] =
{
    #if (IPOD_PREF_SCREEN_CONFIGURATION_MASK)
         0x05,                               /* length (1 byte) = 0x05 */
         0x00,                               /* FIDType (1 byte) = 0x00 */
         kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
         kClassScreenConfig,                 /* iPodPrefClass (1 byte) = 0x01 (Screen Configuration) */
         DEFAULT_SCREEN_CONFIG,              /* prefClassSetting (1 byte) */
         0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};
        
/*! iPod Preference Video Signal format token array */
static const uint8_t s_kFidIpodPrefVideoSignalTokenValues[IPOD_PREF_VIDEO_SIGNAL_FORMAT_SIZE + 1] =
{
    #if (IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassVideoSignalFormat,            /* iPodPrefClass (1 byte) = 0x02 (Video Signal Format) */
        DEFAULT_VIDEO_SIGNAL_FORMAT,        /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};

/*! iPod Preference Line out usage token array */

static const uint8_t s_kFidIpodPrefLineoutUsageTokenValues[IPOD_PREF_LINE_OUT_USAGE_SIZE + 1] =
{
    #if (IPOD_PREF_LINE_OUT_USAGE_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassLineOutUsage,                 /* iPodPrefClass (1 byte) = 0x03 (Line Out Usage) */
        DEFAULT_LINE_OUT_USAGE,             /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};
    

/*! iPod Preference video out connection token array */
static const uint8_t s_kFidIpodPrefVideoOutConnectionTokenValues[IPOD_PREF_VIDEO_OUT_CONNECTION_SIZE + 1] =
{
    #if (IPOD_PREF_VIDEO_OUT_CONNECTION_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassVideoOutConnection,         /* iPodPrefClass (1 byte) = 0x08 (Video Out Connection) */
        DEFAULT_VIDEO_OUT_CONNECTION,       /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};

/*! iPod Preference closed captioning token array */

static const uint8_t s_kFidIpodPrefClosedCaptionTokenValues[IPOD_PREF_CLOSED_CAPTIONING_SIZE + 1] =
{
    #if (IPOD_PREF_CLOSED_CAPTIONING_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassClosedCaptioning,            /* iPodPrefClass (1 byte) = 0x09 (Closed Captioning) */
        DEFAULT_CLOSED_CAPTIONING,          /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
}; 
        
/*! iPod Preference video aspect ratio token array */
static const uint8_t s_kFidIpodPrefVideoAspectRatioTokenValues[IPOD_PREF_VIDEO_ASPECT_RATIO_SIZE + 1] =
{
    #if (IPOD_PREF_VIDEO_ASPECT_RATIO_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassVideoAspectRatio,             /* iPodPrefClass (1 byte) = 0x0A (Video Aspect Ratio) */
        DEFAULT_VIDEO_ASPECT_RATIO,         /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};
        
/*! iPod Preference subtitles token array */
static const uint8_t s_kFidIpodPrefSubtitlesTokenValues[IPOD_PREF_SUBTITLES_SIZE + 1] =
{
    #if (IPOD_PREF_SUBTITLES_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassSubtitles,                    /* iPodPrefClass (1 byte) = 0x0C (Subtitles) */
        DEFAULT_SUBTITLES,                  /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};
        
/*! iPod Preference video alternate audio token array */
static const uint8_t s_kFidIpodPrefVideoAltAudioTokenValues[IPOD_PREF_VIDEO_ALTERNATE_AUDIO_SIZE + 1] =
{
    #if (IPOD_PREF_VIDEO_ALTERNATE_AUDIO_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassVideoAlternateAudio,        /* iPodPrefClass (1 byte) = 0x0D (Video Alternate Audio) */
        DEFAULT_VIDEO_ALTERNATE_AUDIO,      /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};

/*! iPod Preference pause on power token array */
static const uint8_t s_kFidIpodPrefPauseOnPowerTokenValues[IPOD_PREF_PAUSE_ON_POWER_REMOVAL_SIZE + 1] =
{
    #if (IPOD_PREF_PAUSE_ON_POWER_REMOVAL_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassPauseOnPowerRemoval,          /* iPodPrefClass (1 byte) = 0x0F (Pause On Power Removal) */
        DEFAULT_PAUSE_ON_POWER_REMOVAL,     /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};

        
/*! iPod Preference voice over token array */
static const uint8_t s_kFidIpodPrefVoiceOverTokenValues[IPOD_PREF_VOICE_OVER_SIZE + 1] =
{
    #if (IPOD_PREF_VOICE_OVER_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassVoiceOverPreference,          /* iPodPrefClass (1 byte) = 0x14 (Voice Over Preference) */
        DEFAULT_VOICE_OVER_PREF,            /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};
    
        
/*! iPod Preference assistive touch token array */
 static const uint8_t s_kFidIpodPrefAssitiveTouchTokenValues[IPOD_PREF_ASSISTIVE_TOUCH_SIZE + 1] =
{
    #if (IPOD_PREF_ASSISTIVE_TOUCH_MASK)
        0x05,                               /* length (1 byte) = 0x05 */
        0x00,                               /* FIDType (1 byte) = 0x00 */
        kIpodPreferenceTokenFIDSubtype,     /* FIDSubtype (1 byte) = 0x03 */
        kClassAssistiveTouch,               /* iPodPrefClass (1 byte) = 0x16 (Assistive Touch) */
        DEFAULT_ASSISTIVE_TOUCH,            /* prefClassSetting (1 byte) */
        0x01                                /* restoreOnExit   (1 byte) = 0x01 */
    #else
        0
    #endif
};
        
#endif

/*! Accessory Digital Audio Sample Rate token array */
const uint8_t s_kFidAccDigitalAudioSampleRateTokenValues[ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_SIZE + 1] =
{
    #if (ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_MASK)
        IAP_NUMBER_OF_SAMPLE_RATES * 4 + 2, /* length (1 byte) = Supported Sample Rates + 2 = 0x0A */
        0x00,                       /* FIDType = 0x00 */ 
        kAccessoryDigitalAudioSampleRatesTokenFIDSubtype,        /* FIDSubtype = 0x0E */
        /* Supported Sample Rates */
        #if(DIGITAL_SAMPLE_RATE_32KHZ_MASK)
            uint32_t_TO_uint8_t_SPLIT((uint32_t)(kDigitalSampleRate32khz)),
        #endif
        #if(DIGITAL_SAMPLE_RATE_44_1KHZ_MASK)
            uint32_t_TO_uint8_t_SPLIT((uint32_t)(kDigitalSampleRate441khz)),
        #endif
        #if(DIGITAL_SAMPLE_RATE_48KHZ_MASK)
            uint32_t_TO_uint8_t_SPLIT((uint32_t)(kDigitalSampleRate48khz)),
        #endif
        #if(DIGITAL_SAMPLE_RATE_96KHZ_MASK)
            uint32_t_TO_uint8_t_SPLIT((uint32_t)(kDigitalSampleRate96khz)),
        #endif
        #if(DIGITAL_SAMPLE_RATE_192KHZ_MASK)
            uint32_t_TO_uint8_t_SPLIT((uint32_t)(kDigitalSampleRate192khz))
        #endif
    #endif
};

/*! Accessory Digital Audio Video Delay token array */
static const uint8_t s_kFidAccDigitalAudioVideoDelayTokenValues[ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_SIZE + 1] =
{
    #if (ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK)
        0x06,                                               /* length (1 byte) = 0x06 */
        0x00,                                               /* FIDType = 0x00 */
        kAccessoryDigitalAudioVideoDelayTokenFIDSubtype,    /* FIDSubtype = 0x0F */
        uint32_t_TO_uint8_t_SPLIT((uint32_t)(DEFAULT_AUDIO_VIDEO_DELAY))    /* Video Delay in milliseconds */
    #else
        0
    #endif
};

static const uint8_t s_kIapCommandsAccCapability[5] =
{
        0x00,                                               /* Accessory Info Capabilities Type */
        0x00,                                               /* Reserved */
        (_FALSE_ << 0)                                      /* Reserved */
        |(_FALSE_ << 1)                                     /* Reserved */
        /** @todo JCB - Asynchronous playback is needed for bluetooth communication. See table 3-48 */
        |(_FALSE_ << 2),                                    /* Asynchronous Playback State Changes */
        (ACC_INFO_SERIAL_NUMBER_TOKEN_MASK << 0)            /* Accessory serial number */
        |(ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_MASK << 1)        /* Accessory maximum payload size */
        |(_FALSE_ << 2)                                     /* Reserved */
        |(ACC_INFO_STATUS_TOKEN_MASK << 3),                 /* Accessory status types */
        (ACC_CAPABILITY_TOKEN_MASK << 0)                    /* Accessory Capabilities */
        |(ACC_INFO_NAME_TOKEN_MASK << 1)                    /* Accessory Name */
        |(_FALSE_ << 2)                                     /* Deprecated */
        |(_FALSE_ << 3)                                     /* Accessory minimum supported lingo version */
        |(ACC_INFO_FIRMWARE_VERSION_TOKEN_MASK << 4)        /* Accessory firmware version */
        |(ACC_INFO_HARDWARE_VERSION_TOKEN_MASK << 5)        /* Accessory hardware version */
        |(ACC_INFO_MANUFACTURER_TOKEN_MASK << 6)            /* Accessory manufacturer */
        |(ACC_INFO_MODEL_NUMBER_TOKEN_MASK << 7)            /* Accessory model number */
};

static const uint8_t* s_kIapCommandsClassicAccTokenFields[] =
{
    /* Accessory Capability Field */
    (uint8_t*)(&s_kIapCommandsAccCapability[0]),
    /* Accessory Name Field */
    (uint8_t*)(&s_kFidAccInfoNameTokenValues[3]),
    /* Deprecated */
    (uint8_t*)(NULL),
    /* Minimum supported lingo (Optional) */
    (uint8_t*)(NULL),
    /* Accessory Firmware version Field */
    (uint8_t*)(&s_kFidAccInfoFirmwareVersionTokenValues[3]),
    /* Accessory Hardware version Field */
    (uint8_t*)(&s_kFidAccInfoHardwareVersionTokenValues[3]),
    /* Accessory Manufacturer Field */
    (uint8_t*)(&s_kFidAccInfoManufacturerTokenValues[3]),
    /* Accessory Model number Field */
    (uint8_t*)(&s_kFidAccInfoModelNumberTokenValues[3]),
    /* Accessory Serial number Field */
    (uint8_t*)(&s_kFidAccInfoSerialNumberTokenValues[3]),
    /* Accessory Maximum Payload Size Field */
    (uint8_t*)(&s_kFidAccInfoMaxPayloadSizeTokenValues[3]),
    /* Reserved */
    (uint8_t*)(NULL),
    /* Accessory Status types supported Field */
    (uint8_t*)(&s_kFidAccInfoStatusTokenValues[3]),
};

static const uint8_t s_kIapCommandsClassicAccTokenSize[] =
{
        /* Accessory Capability Size */
        sizeof(s_kIapCommandsAccCapability),
        /* Accessory Name Size */
        (uint8_t)(ACC_INFO_NAME_TOKEN_SIZE - 3),
        /* Deprecated */
        (uint8_t)(0),
        /* Minimum supported lingo (Optional) */
        (uint8_t)(0),
        /* Accessory Firmware version Size */
        (uint8_t)(ACC_INFO_FIRMWARE_VERSION_TOKEN_SIZE - 3),
        /* Accessory Hardware version Size */
        (uint8_t)(ACC_INFO_HARDWARE_VERSION_TOKEN_SIZE - 3),
        /* Accessory Manufacturer Size */
        (uint8_t)(ACC_INFO_MANUFACTURER_TOKEN_SIZE - 3),
        /* Accessory Model number Size */
        (uint8_t)(ACC_INFO_MODEL_NUMBER_TOKEN_SIZE - 3),
        /* Accessory Serial number Size */
        (uint8_t)(ACC_INFO_SERIAL_NUMBER_TOKEN_SIZE - 3),
        /* Accessory Maximum Payload Size */
        (uint8_t)(ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_SIZE - 3),
        /* Reserved */
        (uint8_t)(0),
        /* Accessory Status types supported Size */
        (uint8_t)(ACC_INFO_STATUS_TOKEN_SIZE - 3)
};

/*************************************************************************************************
*                                   Global Constants Section                   
*************************************************************************************************/

/*! General Lingo Asynchronous Commands values*/
const uint8_t kGeneralLingoAsynchronousCommands[NUMBER_OF_GENERALLINGO_ASYNCHRONOUS_COMMANDS] =
{
        kGeneralLingoRequestIdentify,                       /*0x00*/
        kGeneralLingoGetAccessoryAuthenticationInfo,        /*0x14*/
        kGeneralLingoGetAccessoryAuthenticationSignature,   /*0x17*/
        kGeneralLingoNotifyIpodStateChange,                 /*0x23*/
        kGeneralLingoGetAccessoryInfo,                      /*0x27*/
        kGeneralLingoOpenDataSessionForProtocol,            /*0x3F*/
        kGeneralLingoCloseDataSession,                      /*0x40*/
        kGeneralLingoIpodDataTransfer,                      /*0x43*/
        kGeneralLingoSetAccessoryStatusNotification,        /*0x46*/
        kGeneralLingoIpodNotification,                      /*0x4A*/
        kGeneralLingoWiFiConnectionInfo                     /*0xAA*/
};

/*! Array of pointers that stores the addresses of the arrays that contains the FID token 
 * values used during the IDPS process when sending the SetFIDTokenValues command */
const uint8_t* kIapCommandsFidTokenFields[] =
{
    /* Identify Token Field                                     0x00*/
    (uint8_t*)(&s_kFidIdentifyTokenValues[0]),
    /* Accessory Capabilities Token Field                       0x01*/
    (uint8_t*)(&s_kFidAccCapabilityTokenValues[0]),
    /* Accessory Name Token Field                               0x02*/
    (uint8_t*)(&s_kFidAccInfoNameTokenValues[0]),
    /* Accessory Firmware version Token Field                   0x03*/
    (uint8_t*)(&s_kFidAccInfoFirmwareVersionTokenValues[0]),
    /* Accessory Hardware version Token Field                   0x04*/
    (uint8_t*)(&s_kFidAccInfoHardwareVersionTokenValues[0]),
    /* Accessory Manufacturer Token Field                       0x05*/
    (uint8_t*)(&s_kFidAccInfoManufacturerTokenValues[0]),
    /* Accessory Model number Token Field                       0x06*/
    (uint8_t*)(&s_kFidAccInfoModelNumberTokenValues[0]),
    /* Accessory Serial number Token Field                      0x07*/
    (uint8_t*)(&s_kFidAccInfoSerialNumberTokenValues[0]),
    /* Accessory Maximum Payload Size Token Field               0x08*/
    (uint8_t*)(&s_kFidAccInfoMaxPayloadSizeTokenValues[0]),
    /* Accessory Status types supported Token Field             0x09*/
    (uint8_t*)(&s_kFidAccInfoStatusTokenValues[0]),
    /* Accessory RF Certification Token Field                   0x0A*/
    (uint8_t*)(&s_kFidAccInfoRfCertificationTokenValues[0]),
    /* Ipod Preferences Video Out Settings Token Field          0x0B*/
    (uint8_t*)(&s_kFidIpodPrefVideoOutTokenValues[0]),
    /* Ipod Preferences Screen Configuration Token Field        0x0C*/
    (uint8_t*)(&s_kFidIpodPrefVideoOutTokenValues[0]),
    /* Ipod Preferences Video Signal Format Token Field         0x0D*/
    (uint8_t*)(&s_kFidIpodPrefVideoSignalTokenValues[0]),
    /* Ipod Preferences Line Out Usage Token Field              0x0E*/
    (uint8_t*)(&s_kFidIpodPrefLineoutUsageTokenValues[0]),
    /* Ipod Preferences Video Out Connection Token Field        0x0F*/
    (uint8_t*)(&s_kFidIpodPrefVideoOutConnectionTokenValues[0]),
    /* Ipod Preferences Closed Captioning Token Field           0x10*/
    (uint8_t*)(&s_kFidIpodPrefClosedCaptionTokenValues[0]),
    /* Ipod Preferences Video Aspect Ratio Token Field          0x11*/
    (uint8_t*)(&s_kFidIpodPrefVideoAspectRatioTokenValues[0]),
    /* Ipod Preferences Subtitles Token Field                   0x12*/
    (uint8_t*)(&s_kFidIpodPrefSubtitlesTokenValues[0]),
    /* Ipod Preferences Video Alternate Audio Token Field       0x13*/
    (uint8_t*)(&s_kFidIpodPrefVideoAltAudioTokenValues[0]),
    /* Ipod Preferences Pause On Power Removal Token Field      0x14*/
    (uint8_t*)(&s_kFidIpodPrefPauseOnPowerTokenValues[0]),
    /* Ipod Preferences Voice Over Token Field                  0x15*/
    (uint8_t*)(&s_kFidIpodPrefVoiceOverTokenValues[0]),
    /* Ipod Preferences Assistive Touch Token Field             0x16*/
    (uint8_t*)(&s_kFidIpodPrefAssitiveTouchTokenValues[0]),
    /* EA Protocol Token Field                                  0x17*/
    (uint8_t*)(&s_kFidEaProtocolTokenValues[0]),
    /* Bundle Seed ID Preference Token Field                    0x18*/
    (uint8_t*)(&s_kFidBundleSeedIdTokenValues[0]),
    /* EA Protocol Metadata Token Field                         0x19*/
    (uint8_t*)(&s_kFidEaProtocolMetadataTokenValues[0]),
    /* Accessory Digital Audio Sample Rate Token Field          0x1A*/
    (uint8_t*)(&s_kFidAccDigitalAudioSampleRateTokenValues[0]),
    /* Accessory Digital Audio Video Delay Token Field          0x1B*/
    (uint8_t*)(&s_kFidAccDigitalAudioSampleRateTokenValues[0])
};

/*! Array that contains the sizes of each array that stores the FID token values */
uint8_t kIapCommandsFidTokenSize[] =
{
        /* Identify Token Size */
        (uint8_t)(IDENTIFY_TOKEN_SIZE),
        /* Accessory Capabilities Size */
        (uint8_t)(ACC_CAPABILITY_TOKEN_SIZE),
        /* Accessory Name Size */
        (uint8_t)(ACC_INFO_NAME_TOKEN_SIZE),
        /* Accessory Firmware version Size */
        (uint8_t)(ACC_INFO_FIRMWARE_VERSION_TOKEN_SIZE),
        /* Accessory Hardware version Size */
        (uint8_t)(ACC_INFO_HARDWARE_VERSION_TOKEN_SIZE),
        /* Accessory Manufacturer Size */
        (uint8_t)(ACC_INFO_MANUFACTURER_TOKEN_SIZE),
        /* Accessory Model number Size */
        (uint8_t)(ACC_INFO_MODEL_NUMBER_TOKEN_SIZE),
        /* Accessory Serial number Size */
        (uint8_t)(ACC_INFO_SERIAL_NUMBER_TOKEN_SIZE),
        /* Accessory Maximum Payload Size */
        (uint8_t)(ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_SIZE),
        /* Accessory Status types supported Size */
        (uint8_t)(ACC_INFO_STATUS_TOKEN_SIZE),
        /* Accessory RF Certification Size */
        (uint8_t)(ACC_INFO_RF_CERTIFICATION_TOKEN_SIZE),
        /* Ipod Preferences Video Out Settings Size */
        (uint8_t)(IPOD_PREF_VIDEO_OUT_SETTINGS_SIZE),
        /* Ipod Preferences Screen Configuration Size */
        (uint8_t)(IPOD_PREF_SCREEN_CONFIGURATION_SIZE),
        /* Ipod Preferences Video Signal Format Size */
        (uint8_t)(IPOD_PREF_VIDEO_SIGNAL_FORMAT_SIZE),
        /* Ipod Preferences Line Out Usage Size */
        (uint8_t)(IPOD_PREF_LINE_OUT_USAGE_SIZE),
        /* Ipod Preferences Video Out Connection Size */
        (uint8_t)(IPOD_PREF_VIDEO_OUT_CONNECTION_SIZE),
        /* Ipod Preferences Closed Captioning Size */
        (uint8_t)(IPOD_PREF_CLOSED_CAPTIONING_SIZE),
        /* Ipod Preferences Video Aspect Ratio Size */
        (uint8_t)(IPOD_PREF_VIDEO_ASPECT_RATIO_SIZE),
        /* od Preferences Subtitles Size */
        (uint8_t)(IPOD_PREF_SUBTITLES_SIZE),
        /* Ipod Preferences Video Alternate Audio Size */
        (uint8_t)(IPOD_PREF_VIDEO_ALTERNATE_AUDIO_SIZE),
        /* Ipod Preferences Pause On Power Removal Size */
        (uint8_t)(IPOD_PREF_PAUSE_ON_POWER_REMOVAL_SIZE),
        /* Ipod Preferences Voice Over Size */
        (uint8_t)(IPOD_PREF_VOICE_OVER_SIZE),
        /* Ipod Preferences Assistive Touch Size */
        (uint8_t)(IPOD_PREF_ASSISTIVE_TOUCH_SIZE),
        /* EA Protocol Size */
        (uint8_t)(0x00),
        /* Bundle Seed ID Preference Size */
        (uint8_t)(0x00),
        /* EA Protocol Metadata Size */
        (uint8_t)(0x00),
        /* Accessory Digital Audio Sample Rate Size */
        (uint8_t)(ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_SIZE),
        /* Accessory Digital Audio Video Delay Size */
        (uint8_t)(ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_SIZE)
};



/*************************************************************************************************
*                                   Global Variables Section                   
*************************************************************************************************/
uint8_t g_iapChallengeData[21];

#if (COMM_WITH_IOS_APPLICATIONS_MASK == _TRUE_)

/*! Structure that stores the latest received information from an application */
iap_gl_sessions_for_protocol_rx_t g_receivedSession;

/*! Array that stores the session IDs and the protocol index of all the iOS applications that
 * have established a successful connection with the accessory */
iap_gl_data_session_for_protocol_t g_iapGeneralLingoDataSessions[MFI_MAX_APPS_NUMBER];

/*! Struct info for command sessions IDS buffer */
random_access_array_information_t g_iapGeneralLingoDataSessionsInfo = {0, /* Last Available Index */
                                                               0, /* Last insert index */
                                                               0, /* Available positions */
                                                               MFI_MAX_APPS_NUMBER /* # elements */
                                                               };
#endif

uint8_t* g_iapHostAccessoryInfo;
uint8_t g_iapHostAccessoryInfoSize;

/*! Array of General Lingo asynchronous functions */
void (* kGeneralLingoAsyncrhonousFunctions[NUMBER_OF_GENERALLINGO_ASYNCHRONOUS_COMMANDS]) (iap_interface_buffer_rx_frame_t* rxFrame) =
{
        s_iap_general_lingo_request_identify,
        s_iap_general_lingo_get_accessory_authentication_info,
        s_iap_general_lingo_get_accessory_authentication_signature,
        s_iap_general_lingo_notify_ipod_state_change,
        s_iap_general_lingo_get_accessory_info,
        s_iap_general_lingo_open_data_session_for_protocol,
        s_iap_general_lingo_close_data_session,
        s_iap_general_lingo_ipod_data_transfer,
        s_iap_general_lingo_set_accessory_status_notification,
        s_iap_general_lingo_ipod_notification,
        s_iap_general_lingo_wifi_connection_info
};
 
#if(ACC_INFO_STATUS_TOKEN_MASK)
uint32_t g_iapCommandsAccStatusNotificationMask = 0;
uint32_t g_iapCommandsAccStatusNotification;

#if(BLUETOOTH_DEVICE_STATUS)
iap_gl_accessory_status_bluetooth_t g_iapCommandsAccStatusBluetooth = {kBluetoothDevice,
                                                                       0x00, 0x00, 0x00, 0x00, 0x00,
                                                                       {0x00}, {0x00}};
#endif

#if(FAULT_CONDITION_STATUS)
iap_gl_accessory_status_fault_t g_iapCommandsAccStatusFault = {kFaultCondition, 0x00, 0x00};
#endif
#endif

/*************************************************************************************************
*                                   Static Variables Section                   
*************************************************************************************************/
/*! Pointers to Host functions that will be called during/after the execution of 
 * an asynchronous command */

/*! General Lingo Host callbacks */
static iap_commands_asynchronous_callback_t s_iap_commands_request_identify;
static iap_commands_asynchronous_callback_t s_iap_commands_get_accessory_auth_info;
static iap_commands_asynchronous_callback_t s_iap_commands_get_accessory_auth_signature;
static iap_commands_asynchronous_callback_t s_iap_commands_notify_ipod_change;
static iap_commands_asynchronous_callback_t s_iap_commands_get_accesory_info;
static iap_commands_asynchronous_callback_t s_iap_commands_accessory_status_notification;

/*! General Lingo App callbacks */
static iap_general_lingo_ipod_notification_callback_t s_iap_commands_ipod_notification;
static iap_general_lingo_wifi_connection_callback_t s_iap_commands_wifi_connection;

/*! Variable to count the number of applications opened */
static uint8_t g_appsOpenCounter = 0;

/*! Pointer to the ApplicationDataTransfer callbacks*/
iap_commands_data_transfer_callback_t applicationDataTransferFunctions[MFI_MAX_APPS_NUMBER] = {NULL};

/*************************************************************************************************
*                                      Functions Section                       
*************************************************************************************************/
/* See GeneralLingo.h for documentation of this function. */
uint8_t iap_set_callback_app_data_transfer(iap1_ios_applications_callbacks_Ids_t callbackId, 
                                           iap_commands_data_transfer_callback_t appCallback)
{
    uint8_t returnValue = _ERROR_; 
    if ((NULL != appCallback) && (callbackId < MFI_MAX_APPS_NUMBER))
    {
        returnValue = _OK_;
        applicationDataTransferFunctions[callbackId] = appCallback;
    }
    return (returnValue); 
}

/* See GeneralLingo.h for documentation of this function. */
uint8_t set_gl_asynchronous_command_callback(uint8_t callbackId, iap_commands_asynchronous_callback_t hostFunctionAddress)
{
    uint8_t returnValue = _ERROR_; 
    if (NULL != hostFunctionAddress)
    {
       returnValue = _OK_;
       switch(callbackId)
       {
           case (kGeneralLingoRequestIdentify):
                s_iap_commands_request_identify = hostFunctionAddress;
               break;
           
           case (kGeneralLingoGetAccessoryAuthenticationInfo):
                s_iap_commands_get_accessory_auth_info = hostFunctionAddress;
               break;
           
           case (kGeneralLingoGetAccessoryAuthenticationSignature):
                s_iap_commands_get_accessory_auth_signature = hostFunctionAddress;
               break;
           
           case (kGeneralLingoNotifyIpodStateChange):
                s_iap_commands_notify_ipod_change = hostFunctionAddress;
               break;
           
           case (kGeneralLingoGetAccessoryInfo):
                s_iap_commands_get_accesory_info = hostFunctionAddress;
               break;
           
           case (kGeneralLingoAccessoryStatusNotification):
                s_iap_commands_accessory_status_notification = hostFunctionAddress;
               break;
           
           default: 
               returnValue = _ERROR_;
               break;
       }
       
    }
    return (returnValue); 
}

/* See GeneralLingo.h for documentation of this function. */
uint8_t set_gl_ipod_notification_callback(iap_general_lingo_ipod_notification_callback_t callback)
{
    uint8_t returnValue = _ERROR_; 
    
    if (NULL != callback)
    {
        returnValue = _OK_;
        s_iap_commands_ipod_notification = callback;
    }
    
    return returnValue;
}

/* See GeneralLingo.h for documentation of this function. */
uint8_t set_gl_wifi_connection_info_callback(iap_general_lingo_wifi_connection_callback_t callback)
{
    uint8_t returnValue = _ERROR_; 
    
    if (NULL != callback)
    {
        returnValue = _OK_;
        s_iap_commands_wifi_connection = callback;
    }
    
    return returnValue;
}

/* See GeneralLingo.h for documentation of this function. */
command_transmission_status_t gl_accessory_data_transfer(iap1_ios_applications_callbacks_Ids_t appID,
                                                         iap_gl_app_data_transfer_t* txAppData,
                                                         uint8_t* rxPayLoad,
                                                         uint16_t rxPayLoadSize,
                                                         void (*rxCallback)(uint8_t))
{
    command_transmission_status_t txStatus; 
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoAccessoryDataTransfer,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kGeneralLingoIpodAck};


    txAppData->sessionID = g_iapGeneralLingoDataSessions[appID].sessionID;
    BYTESWAP16(txAppData->sessionID, txAppData->sessionID);
    
    txCommandData.txFrameData.payload = (((uint8_t*) (txAppData)) + 2);
    
    // TODO add validation to check the payloadSize that will be transmitted 
    txCommandData.txFrameData.payloadSize = (txAppData->appDataSize + 2);
   
    txStatus = iap_commands_transmit_command(&txCommandData); 

    BYTESWAP16(txAppData->sessionID, txAppData->sessionID);
    
    return txStatus;
}


/* See GeneralLingo.h for documentation of this function. */
command_transmission_status_t gl_set_event_notification(uint32_t notificationBitMask,
                                                               void (*rxCallback)(uint8_t))
{
    uint8_t tempNotificationBuffer[8];
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                tempNotificationBuffer,
                                                sizeof(tempNotificationBuffer),
                                                0,
                                                kQuestionPacket,
                                                kGeneralLingo,
                                                kGeneralLingoSetEventNotification,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kGeneralLingoIpodAck};
    
    tempNotificationBuffer[7] = (uint8_t) (notificationBitMask);
    tempNotificationBuffer[6] = (uint8_t) (notificationBitMask >> 8);
    tempNotificationBuffer[5] = (uint8_t) (notificationBitMask >> 16);
    tempNotificationBuffer[4] = (uint8_t) (notificationBitMask >> 24);
    tempNotificationBuffer[3] = 0x00;
    tempNotificationBuffer[2] = 0x00;
    tempNotificationBuffer[1] = 0x00;
    tempNotificationBuffer[0] = 0x00;
    
    return iap_commands_transmit_command(&txCommandData);
}

/*************************************************************************************************
*
* @brief    Asynchronous RequestIdentify function
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  Once the iPod has sent this command, stack will jump to this function and release the 
*           message from queue
*          
**************************************************************************************************/
static void s_iap_general_lingo_request_identify
                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    /* Jump to the registered callback */
    s_iap_commands_request_identify(rxFrame);
    /* Release space from the queue */
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous GetAccessoryAuthenticationInfo function
* @return   void
* @details  Once the iPod has sent this command, stack will jump to this function and release the 
*           message from queue
*          
**************************************************************************************************/
static void s_iap_general_lingo_get_accessory_authentication_info
                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    /* Save TID to use it in answer*/
    g_iapCommandsTransactionID = rxFrame->transactionId;
    /* Jump to the registered callback */
    s_iap_commands_get_accessory_auth_info(rxFrame);
    /* Release space from the queue */
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous GetAccessoryAuthenticationSignature function
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  Once the iPod has sent this command, stack will jump to this function and release the 
*           message from queue
*          
**************************************************************************************************/
static void s_iap_general_lingo_get_accessory_authentication_signature
                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    /* Save TID to use it in answer */
    g_iapCommandsTransactionID = rxFrame->transactionId;
    if(sizeof(g_iapChallengeData) < (rxFrame->payloadSize))
    {
        rxFrame->payloadSize = sizeof(g_iapChallengeData);
    }
    /* Skip Authentication Retry Counter */
    rxFrame->payloadSize--;
    /* Copy Challenge Data */
    while(rxFrame->payloadSize--)
    {
        g_iapChallengeData[rxFrame->payloadSize + 1] = \
                rxFrame->payload[rxFrame->payloadSize];
    }
    s_iap_commands_get_accessory_auth_signature(rxFrame);
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous NotifyiPodStateChange function
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  This asynchronous command gives information about iPod status changes e.g. going to hi-
*           bernation, going to sleep or power on. This function makes a jump to a callback host
*           to handle if necessary state machine changes.
*          
**************************************************************************************************/
static void s_iap_general_lingo_notify_ipod_state_change
                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    /* Jump to the host callback command */
    s_iap_commands_notify_ipod_change(rxFrame);
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous GetAccessoryInfo
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  This asynchronous command ask for specfic information about the accessory and it is 
*           only sent when connecting an iPod classic or a non IDPS-device replacing SetFIDTokens.
*           The function responds with RetAccessoryInfo carrying specific payload depending of the
*           requested information from iPod.
*          
**************************************************************************************************/
static void s_iap_general_lingo_get_accessory_info
(iap_interface_buffer_rx_frame_t* rxFrame)
{
    static bool s_firstInfoCapabilitiesDone = false;
    command_transmission_status_t currentCommandTxStatus;

    
    if(s_firstInfoCapabilitiesDone)
    {
        static uint8_t s_capabilitieCounter = IAP_TOTAL_NUMBER_TOKENS_FOR_NOT_IDPS_SUPPORT;
        uint8_t iapCommandsGetAccessoryInfo[2];
        
#if(_FALSE_ == BIG_ENDIAN_CORE)
        iap_gl_ret_accessory_info_incoming_max_payload_t* swapParameters;
#endif
        /* No need to save TID, this command is only sent with non-idps devices i.e. classic */

        if(sizeof(iapCommandsGetAccessoryInfo) < (rxFrame->payloadSize))
        {
            rxFrame->payloadSize = sizeof(iapCommandsGetAccessoryInfo);
            //iAP_Cmd_gdwStatus |= (1 << IAP_CMD_INVALID_PAYLOAD_SIZE);   
        }
        while(rxFrame->payloadSize--)
        {
            iapCommandsGetAccessoryInfo[rxFrame->payloadSize] = \
                    rxFrame->payload[rxFrame->payloadSize];
        }

        if(0x03 == iapCommandsGetAccessoryInfo[0])
        {
            g_iapHostAccessoryInfo =
                    (uint8_t*)(s_kIapCommandsLingoesMinimumVersion[iapCommandsGetAccessoryInfo[1]]);
            g_iapHostAccessoryInfoSize =
                    (uint16_t)(sizeof(s_kIapCommandsGeneralLingoMinimumVersion));
        }
        else
        {
            g_iapHostAccessoryInfo =
                    (uint8_t*)(s_kIapCommandsClassicAccTokenFields[iapCommandsGetAccessoryInfo[0]]);
            g_iapHostAccessoryInfoSize =
                    (uint16_t)(s_kIapCommandsClassicAccTokenSize[iapCommandsGetAccessoryInfo[0]]);
        }

#if(_FALSE_ == BIG_ENDIAN_CORE)
        /* Create a local buffer for the incoming max payload case because the tokens array is constant */
        volatile uint8_t  maxPayloadBuff[3];

        swapParameters =
                (iap_gl_ret_accessory_info_incoming_max_payload_t *)(g_iapHostAccessoryInfo);
        if(kAccessoryIncomingMaxPayload == swapParameters->accessoryInfoType)
        {
            maxPayloadBuff[0] = swapParameters->accessoryInfoType;
            maxPayloadBuff[1] = (uint8_t)((swapParameters->accInMaxPayload) >>8);
            maxPayloadBuff[2] = (uint8_t)((swapParameters->accInMaxPayload)&0xFF);
            g_iapHostAccessoryInfo = (uint8_t*) &maxPayloadBuff[0];
        }
        else if(kAccessoryStatusTypeSupported == swapParameters->accessoryInfoType)
        {
            iap_gl_ret_accessory_info_status_supported_t *spSwapTemp = \
                    (iap_gl_ret_accessory_info_status_supported_t *)(swapParameters);
            BYTESWAP32(spSwapTemp->accStatusTypesSupported,spSwapTemp->accStatusTypesSupported);
        }

#endif
        /* Answer with RetAccessoryInfo */
        currentCommandTxStatus = gl_ret_accessory_info(
                g_iapHostAccessoryInfo, /* Transmission Payload */
                g_iapHostAccessoryInfoSize, /* Transmission Payload Size */
                NULL, /* Buffer to receive payload */
                0,    /* Buffer payload size received */
                NULL);  /* Callback when reception of answer */

        if(kCommandTransmissionOk != currentCommandTxStatus)
        {
            s_capabilitieCounter = IAP_TOTAL_NUMBER_TOKENS_FOR_NOT_IDPS_SUPPORT;
            //TODO handle error 
        }else
        {
            if((--s_capabilitieCounter) == 0x00)
            {
                s_capabilitieCounter = IAP_TOTAL_NUMBER_TOKENS_FOR_NOT_IDPS_SUPPORT;
                s_firstInfoCapabilitiesDone = false;
                s_iap_commands_get_accesory_info(rxFrame);
            }
        }
        
    }else
    {
        s_iap_commands_get_accesory_info(rxFrame);
        s_firstInfoCapabilitiesDone = true;
    }

    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous OpenDataSessionForProtocol function
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  This function will validate if the token is enabled, in case it's enabled then the fun-
*           ction will validate if the stack support a new application communication and it will sa-
*           ve the SessionID received into a specified buffer. Finally an accessory ack will be 
*           prepared with the status of the processed command
*          
**************************************************************************************************/
static void s_iap_general_lingo_open_data_session_for_protocol
                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    /* Asynchronous Command */
    command_transmission_status_t currentCommandTxStatus;
    uint16_t  receivedSessionID; 
    uint8_t insertIndex;
    iap_gl_accessory_ack_t accessoryAck;
    
    /* The accessory ack[1] brings inside the question it is answering */
    accessoryAck.commandID = kGeneralLingoOpenDataSessionForProtocol;
    /* Save Transaction ID to use it in answer */
    g_iapCommandsTransactionID = rxFrame->transactionId;
    
#if (COMM_WITH_IOS_APPLICATIONS_MASK == _TRUE_) 
    /* Increment the actual number of open applications */
    g_appsOpenCounter++;
    
    if(MFI_MAX_APPS_NUMBER > g_appsOpenCounter)
    {
        /* Prepare an accessory ack success in case the sessionID is minor or 
         * equal than the maximum supported by the accessory*/
        accessoryAck.ackStatus = kIAPAckSuccess; 
        /* Get the next available buffer position for the sessionIds buffer */
        insertIndex = RandomAccessArray_get_next_avaliable_position
                (&g_iapGeneralLingoDataSessionsInfo);

        receivedSessionID = (((uint16_t) rxFrame->payload[SESSION_ID_PAYLOAD_OFFSET]) << 8) |
                             ((uint16_t) rxFrame->payload[SESSION_ID_PAYLOAD_OFFSET + 1]);

        /* Save SessionID in the specific buffer of actual sessions IDs */
        /* Store the SessionID in the available position of SessionIds buffer */
        g_iapGeneralLingoDataSessions[insertIndex].sessionID = receivedSessionID;
        g_iapGeneralLingoDataSessions[insertIndex].protocolIndex = 
                rxFrame->payload[PROTOCOL_INDEX_PAYLOAD_OFFSET];
    }
    else
    {
        /* No more comm. application is allowed */
        accessoryAck.ackStatus  = kIAPAckBadParameter; 
        g_appsOpenCounter--;
    }
    
#else
    accessoryAck.ackStatus  = kIAPAckBadParameter;
#endif
    
    currentCommandTxStatus = gl_accessory_ack(&accessoryAck); 
    
    /* TODO handle transmission error, validate if the command could be sent or queued and act 
     * accordingly otherwise */
    
    if(accessoryAck.ackStatus == kIAPAckSuccess)
    {
        /* Signal to the application that an OpenDataSessionForProtocol command has been received */
        g_receivedSession.applicationPayloadSize = 0;
        g_receivedSession.protocolSessionData = NULL;
        g_receivedSession.communicationStatus = kOpenSession;
        g_receivedSession.sessionId = receivedSessionID;
        g_receivedSession.appCallbackId = (iap1_ios_applications_callbacks_Ids_t) insertIndex;

        if(applicationDataTransferFunctions[insertIndex] != NULL)
        {
            applicationDataTransferFunctions[insertIndex](&g_receivedSession);
        }
    }
    
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous CloseDataSession function
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  This function handles the closure of an opened session, validating the session and
*           erasing it from the specific buffer g_iapCmdSessionsIds, It also decrement the number
*           of open applications and return an accessoryAck with the command process status
*          
**************************************************************************************************/
static void s_iap_general_lingo_close_data_session
                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    command_transmission_status_t currentCommandTxStatus;
    uint16_t receivedSessionID; 
    iap_gl_accessory_ack_t accessoryAck;
    uint8_t sessionSearchIndex = 0;
    uint8_t sessionFoundIndex = kNotAvailableIndex;
       
    /* Prepare accessory ack */
    accessoryAck.commandID  = kGeneralLingoCloseDataSession;
    accessoryAck.ackStatus = kIAPAckBadParameter;
    /* Save Transaction ID to use it in answer */
    g_iapCommandsTransactionID = rxFrame->transactionId;
    
#if (COMM_WITH_IOS_APPLICATIONS_MASK == _TRUE_)
    
    receivedSessionID = (((uint16_t) rxFrame->payload[SESSION_ID_PAYLOAD_OFFSET]) << 8) |
                        ((uint16_t) rxFrame->payload[SESSION_ID_PAYLOAD_OFFSET + 1]);
    
    /* Look for the sessionID received in sessionsIds Buffer to get the position */
    do
    {
        if(IS_POSITION_RESERVED(g_iapGeneralLingoDataSessionsInfo, sessionSearchIndex))
        {
            /* If the received session match with one previously saved prepare an Accessory
             * Ack Success and get the position of the array */
            if(g_iapGeneralLingoDataSessions[sessionSearchIndex].sessionID == receivedSessionID)
            {
                accessoryAck.ackStatus = kIAPAckSuccess; 
                sessionFoundIndex = sessionSearchIndex;
            }
        }
        sessionSearchIndex++;
    }while((sessionSearchIndex < MFI_MAX_APPS_NUMBER) && (kNotAvailableIndex == sessionFoundIndex));

    currentCommandTxStatus = gl_accessory_ack(&accessoryAck); 
    
    if(sessionFoundIndex != kNotAvailableIndex)
    {
        /* Signal to the application that a CloseDataSessionForProtocol command has been received */
        g_receivedSession.applicationPayloadSize = 0;
        g_receivedSession.protocolSessionData = NULL;
        g_receivedSession.communicationStatus = kCloseSession;
        g_receivedSession.sessionId = receivedSessionID;
        g_receivedSession.appCallbackId = (iap1_ios_applications_callbacks_Ids_t) sessionFoundIndex;
        
        if(applicationDataTransferFunctions[sessionFoundIndex] != NULL)
        {
            applicationDataTransferFunctions[sessionFoundIndex](&g_receivedSession);
        }
        
        /* Free the position of sessionIdsBufferInfo */
        RandomAccessArray_free_position(&g_iapGeneralLingoDataSessionsInfo, sessionFoundIndex);
        
        /* Decrement the actual number of open applications */
        g_appsOpenCounter--;
    }
    
#else
    accessoryAck.ackStatus = kIAPAckBadParameter;
#endif

    
    
    /* TODO handle transmission error, validate if the command could be sent or queued and act 
     * accordingly otherwise */
    
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous iPodDataTransfer function
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  This function is in charge of jumping to the specific user callback according the
*           sessionID received, it also sends the accessoryAck with the command process status
*          
**************************************************************************************************/
static void s_iap_general_lingo_ipod_data_transfer
                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    command_transmission_status_t currentCommandTxStatus;
    uint16_t receivedSessionID;
    iap_gl_accessory_ack_t accessoryAck;
    uint8_t sessionSearchIndex = 0;
    uint8_t sessionFoundIndex = kNotAvailableIndex;

    accessoryAck.ackStatus = kIAPAckBadParameter;
    accessoryAck.commandID = kGeneralLingoIpodDataTransfer;

    /* Save Transaction ID to use it in answer */
    g_iapCommandsTransactionID = rxFrame->transactionId;
    
#if (COMM_WITH_IOS_APPLICATIONS_MASK == _TRUE_)
    receivedSessionID = (((uint16_t) rxFrame->payload[SESSION_ID_PAYLOAD_OFFSET]) << 8) |
                         ((uint16_t) rxFrame->payload[SESSION_ID_PAYLOAD_OFFSET + 1]);
    
    /* Look for the sessionID received in sessionsIds Buffer to get the position */
    do
    {
        if(IS_POSITION_RESERVED(g_iapGeneralLingoDataSessionsInfo, sessionSearchIndex))
        {
            /* If the received session match with one previously saved prepare an Accessory
             * Ack Success and get the position of the array */
            if(g_iapGeneralLingoDataSessions[sessionSearchIndex].sessionID == receivedSessionID)
            {
                accessoryAck.ackStatus = kIAPAckSuccess; 
                sessionFoundIndex = sessionSearchIndex;
            }
        }
        sessionSearchIndex++;
    }while((sessionSearchIndex < MFI_MAX_APPS_NUMBER) && (kNotAvailableIndex == sessionFoundIndex));

    /* Bytes 0 and 1 are the Session ID, from Byte 2 there is the applications sent Data */
    /* Jump to the user's callback in case there was a match in session ID received */
    if(sessionFoundIndex != kNotAvailableIndex)
    {  
        /* Signal to the application that an iPodDataTransfer command has been received */
        g_receivedSession.applicationPayloadSize = (rxFrame->payloadSize - 2);
        g_receivedSession.protocolSessionData = (rxFrame->payload + 2);
        g_receivedSession.communicationStatus = kReceivedData;
        g_receivedSession.sessionId = receivedSessionID;
        g_receivedSession.appCallbackId = (iap1_ios_applications_callbacks_Ids_t) sessionFoundIndex;
        
        if(applicationDataTransferFunctions[sessionFoundIndex] != NULL)
        {
            applicationDataTransferFunctions[sessionFoundIndex](&g_receivedSession);
        }
    }
#endif

    currentCommandTxStatus = gl_accessory_ack(&accessoryAck); 
    
    /* TODO handle transmission error, validate if the command could be sent or queued and act 
     * accordingly otherwise */

    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous SetAccessoryStatusNotification function
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  When receiving the async command SetAccessoryStatusNot this function will be executed
*           to save the notification mask and then it will send the RetAccessoryStatusNot and
*           finally will send an initial accessoryStatusNotification
*          
**************************************************************************************************/
static void s_iap_general_lingo_set_accessory_status_notification
                (iap_interface_buffer_rx_frame_t* rxFrame)
{
    /* Asynchronous Command */
#if(ACC_INFO_STATUS_TOKEN_MASK)
    command_transmission_status_t currentCommandTxStatus;
    uint8_t accStatusFlag;

    /* Save Received Transaction ID to be used on answer */
    g_iapCommandsTransactionID = rxFrame->transactionId;

    g_iapCommandsAccStatusNotificationMask = rxFrame->payload[0];
    g_iapCommandsAccStatusNotificationMask <<= 8;
    g_iapCommandsAccStatusNotificationMask |= rxFrame->payload[1];
    g_iapCommandsAccStatusNotificationMask <<= 8;
    g_iapCommandsAccStatusNotificationMask |= rxFrame->payload[2];
    g_iapCommandsAccStatusNotificationMask <<= 8;
    g_iapCommandsAccStatusNotificationMask |= rxFrame->payload[3];

    accStatusFlag = (uint8_t) g_iapCommandsAccStatusNotificationMask;
    
    /* Send answer RetAccessoryStatusNotification */
    currentCommandTxStatus = gl_ret_accessory_status_notification(g_iapCommandsAccStatusNotificationMask);
           
    /* TODO handle transmission error, validate if the command could be sent or queued and act 
     * accordingly otherwise */
    
    /* @todo RR: treated in cmds Jump to the host callback to return an 
     * initial AccessoryStatusNotification */
#if(BLUETOOTH_DEVICE_STATUS || FAULT_CONDITION_STATUS)
    #if(BLUETOOTH_DEVICE_STATUS)
        if(accStatusFlag & (1 << kBluetoothDevice))
        {
            accStatusFlag &= ~(1 << kBluetoothDevice);
            
            /* Send an initial AccessorryStatusNotification */
            currentCommandTxStatus = gl_accessory_status_notification((uint8_t*)(&g_iapCommandsAccStatusBluetooth), 
                                                                      sizeof(g_iapCommandsAccStatusBluetooth)); 
        }
    #endif
    
    #if(FAULT_CONDITION_STATUS)
        if(accStatusFlag & (1 << kFaultCondition))
        {
            accStatusFlag &= ~(1 << kFaultCondition);

            /* Send an initial AccessorryStatusNotification */
            currentCommandTxStatus = gl_accessory_status_notification((uint8_t*)(&g_iapCommandsAccStatusFault), 
                                                                sizeof(g_iapCommandsAccStatusFault)); 
        }
    #endif
   
        /* TODO handle transmission error, validate if the command could be sent or queued and act 
         * accordingly otherwise */
    
#endif
#else

#endif
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous ipodNotification
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
* @details  When receiving the async command stack must check the endianess to decide whether jump
*           to the callback with the swap bytes in the payload according the notification received
**************************************************************************************************/
static void s_iap_general_lingo_ipod_notification(iap_interface_buffer_rx_frame_t* rxFrame)
{
    /* Jump to the callback specified by the user */
    if(NULL != s_iap_commands_ipod_notification)
    {
        s_iap_commands_ipod_notification((iap_gl_event_notification_t) rxFrame->payload[0], 
                                         (&rxFrame->payload[0] + 1),
                                         (rxFrame->payloadSize - 1));
    }
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*************************************************************************************************
*
* @brief    Asynchronous WifiConnectionInfo
* @param    iapi_buffer_rx_frame_t* rxFrame - This pointer to the structure is used to access the
*           received bytes in a regular form
* @return   void
**************************************************************************************************/
static void s_iap_general_lingo_wifi_connection_info (iap_interface_buffer_rx_frame_t* rxFrame)
{
    if(NULL != s_iap_commands_wifi_connection)
    {
        s_iap_commands_wifi_connection((iap_gl_wifi_connection_info_t*) rxFrame->payload);
    }
    
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*) rxFrame);
}

/* See GeneralLingo.h for documentation of this function. */
void iap_general_lingo_reset(void)
{
    /* Reinitialize the random access array that handles the iAP1 
     * communication with iOS apps*/
    RandomAccessArray_flush_array(&g_iapGeneralLingoDataSessionsInfo);

    g_appsOpenCounter = 0;
}
/*************************************************************************************************
* EOF
*************************************************************************************************/
