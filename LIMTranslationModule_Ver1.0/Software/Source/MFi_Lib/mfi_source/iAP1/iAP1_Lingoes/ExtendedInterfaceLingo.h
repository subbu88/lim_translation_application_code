/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

#ifndef EXTENDEDINTERFACELINGO_H_
#define EXTENDEDINTERFACELINGO_H_


/*************************************************************************************************
*                                        Includes Section                              
*************************************************************************************************/
#include "iAP_Commands.h"

/*! @addtogroup iAP1_Lingoes iAP1 Lingoes
*   @{ 

*  @addtogroup ExtendedInterfaceLingo Extended Interface Lingo
*  @brief  This file contains the specific types (enums and structures) and macros, the asynchronous 
*          commands functions, the inline functions used to transmit commands from the Extended 
*          Interface lingo and the inline functions used to access to the received data from specific
*          commands.
*          
*  @details The specific Extended Interface Lingo asynchronous commands functions have a one to one 
*           relation with the asynchronous commands from this lingo so these functions are called 
*           once their corresponding asynchronous command is received. The inline functions can be 
*           used as a mean to easily transmit an specific command from this lingo.
*  @{  
*/

/*************************************************************************************************
*                                        Defines & Macros Section                             
*************************************************************************************************/
/*!
*  @addtogroup ExtendedInterfaceLingoDefs Extended Interface Lingo Macros and Defines
*  @{  
*/
#define IAP_EXTENDEDINTERFACELINGO_COMMANDS_COUNT   0x50

/*! Amount of Asynchronous Commands from Extended Interface Lingo */
#define NUMBER_OF_EXTENDEDINTERFACE_ASYNCHRONOUS_COMMANDS   0x01

/*! Limit for the command RetrieveCategorizedDatabaseRecords (0x001A), 
 * determines the max number of records that can be requested */
#define MAX_CATEGORIZED_DB_RECORDS_TO_REQUEST               (8)

/** Unique Track Identifier Size */
#define UNIQUE_TRACK_IDENTIFIER_SIZE                        (8)

/** Size in byte of all track index */
#define TRACK_INDEX_SIZE                                    (4)

/*! Buffer size for fields such as Artist Name, Track Name, Album Name, etc */
#define TRACK_INFO_DEFAULT_BUFFER_SIZE                      (32)

/*! Buffer size for the lyrics */
#define TRACK_INFO_LYRICS_BUFFER_SIZE                       (1024)

/*! Generic Buffer size to use in every track info type */
#define INDEXED_PLAYING_TRACK_INFO_BUFFER_SIZE              (1024)

/*! Max amount of tracks for playback list */
#define MAX_AMOUNT_OF_TRACKS_FOR_PLAYBACK_LIST              (IAPI_MAX_TX_PAYLOAD_SIZE / \
                                                             UNIQUE_TRACK_IDENTIFIER_SIZE)

/*! number of bytes that include the information packet such as current packet transmitted 
 * and maximum amount of packet for transmit from PrepareUIDList command (0x004A) */
#define PREPARE_UID_LIST_OFFSET                             (4)

/*! Max size of buffer for sequence of 4bytes time offset records of retArtworkTimes command */
#define MAX_SIZE_OF_SEQUENCE_OF_TIMEOFFSET_RECORDS_BUFFER   (1000)

/*! Size of time offset record in bytes */
#define SIZE_OF_TIMEOFFSET_RECORDS_IN_BYTES                 (4)

/*! Size of artwork data buffer, size of the buffer where the received data from the command
 * RetTrackArtworkData will be saved */
#define SIZE_ARTWORK_DATA_BUFFER                            (6100)

/*! @addtogroup TrackInformationEnablers
 *  @brief Macro Enablers for the commands GetUIDTrackInfo (0x3E), GetDBTrackInfo (0x40) and 
 *  GetDBTrackInfo (0x42), must me set to _TRUE_ or _FALSE_ accordingly to the data fields that will
 *  be requested in these commands.
 * @{ */ 
#define CAPABILITIES_MASK                       (_TRUE_)
#define TRACK_NAME_MASK                         (_TRUE_)
#define ARTIRST_NAME_MASK                       (_TRUE_)
#define ALBUM_NAME_MASK                         (_TRUE_)
#define GENRE_NAME_MASK                         (_TRUE_)
#define COMPOSER_NAME_MASK                      (_TRUE_)
#define TOTAL_TRACK_TIME_DURATION_MASK          (_TRUE_)
#define UNIQUE_TRACK_ID_MASK                    (_TRUE_)
#define CHAPTER_COUNT_MASK                      (_FALSE_)
#define CHAPTER_TIMES_MASK                      (_FALSE_)
#define CHAPTER_NAMES_MASK                      (_FALSE_)
#define LYRIC_OF_SONG_CURRENTLY_PLAYING_MASK    (_TRUE_)
#define DESCRIPTION_MASK                        (_FALSE_)
#define ALBUM_TRACK_INDEX_MASK                  (_TRUE_)
#define DISC_SET_ALBUM_INDEX_MASK               (_FALSE_)
#define PLAY_COUNT_MASK                         (_FALSE_)
#define SKIP_COUNT_MASK                         (_FALSE_)
#define PODCAST_RELEASE_DATE_MASK               (_FALSE_)
#define LAST_PLAYED_DATE_TIME_MASK              (_FALSE_)
#define YEAR_RELEASE_DATE_MASK                  (_FALSE_)
#define STAR_RATING_MASK                        (_TRUE_)
#define SERIES_NAME_MASK                        (_FALSE_)
#define SEASON_NUMBER_MASK                      (_FALSE_)
#define TRACK_VOLUME_ADJUST_MASK                (_FALSE_)
#define TRACK_EQ_PRESET_MASK                    (_FALSE_)
#define TRACK_DATA_RATE_MASK                    (_FALSE_)
#define BOOKMARK_OFFSET_MASK                    (_FALSE_)
#define START_STOP_TIME_OFFSET_MASK             (_FALSE_)
/*! @}
 * Group TrackInformationEnablers */ 

/*! Macro with the max number of fields for the track information command (Do NOT Modify this value) */
#define TRACK_INFO_NUMBER_OF_FIELDS                 (28)


/*! @} 
* Group ExtendedInterfaceLingoDefs
*/

/*************************************************************************************************
*                                       Typedef Section                             
*************************************************************************************************/
/*!
 * A brief text description (one line).
 * A detailed text description of the code object being described, can span more
 * lines and contain formatting tags (both Doxygen and HTML). Optional tag. */

#pragma pack (push,1)

/** Extended Interface Lingo Enumeration */
typedef enum
{
    /*kExtendedInterfaceReserved,                                  0x00*/
    kExtendedInterfaceIpodAck = 0x01,                            /*0x01*/
    kExtendedInterfaceGetCurrentPlayingTrackChapterInfo,         /*0x02*/
    kExtendedInterfaceReturnCurrentPlayingTrackChapterInfo,      /*0x03*/
    kExtendedInterfaceSetCurrentPlayingTrackChapter,             /*0x04*/
    kExtendedInterfaceGetCurrentPlayingTrackChapterPlayStatus,   /*0x05*/
    kExtendedInterfaceReturnCurrentPlayinGtrackChapterPlayStatus,/*0x06*/
    kExtendedInterfaceGetCurrentPlayingTrackChapterName,         /*0x07*/
    kExtendedInterfaceReturnCurrentPlayingTrackChapterName,      /*0x08*/
    kExtendedInterfaceGetAudiobookSpeed,                         /*0x09*/
    kExtendedInterfaceReturnAudiobookSpeed,                      /*0x0A*/
    kExtendedInterfaceSetAudiobookSpeed,                         /*0x0B*/
    kExtendedInterfaceGetIndexedPlayingTrackInfo,                /*0x0C*/
    kExtendedInterfaceReturnIndexPlayingTrackinfo,               /*0x0D*/
    kExtendedInterfaceGetArtworkFormats,                         /*0x0E*/
    kExtendedInterfaceReturnArtworkFormats,                      /*0x0F*/
    kExtendedInterfaceGetTrackArtworkData,                       /*0x10*/
    kExtendedInterfaceRetTrackArtworkData,                       /*0x11*/
  /*kExtendedInterfaceUnusedOrDeprecated,                          0x12
    kExtendedInterfaceUnusedOrDeprecated,                          0x13
    kExtendedInterfaceUnusedOrDeprecated,                          0x14
    kExtendedInterfaceUnusedOrDeprecated,                          0x15*/
    kExtendedInterfaceResetDbSelection = 0x16,                   /*0x16*/
    kExtendedInterfaceSelectDbRecord,                            /*0x17*/
    kExtendedInterfaceGetNumberCategorizedDbRecords,             /*0x18*/
    kExtendedInterfaceReturnNumberCategorizedDbRecords,          /*0x19*/
    kExtendedInterfaceRetrieveCategorizedDatabaseRecords,        /*0x1A*/
    kExtendedInterfaceReturnCategorizedDatabaseRecord,           /*0x1B*/
    kExtendedInterfaceGatPlayStatus,                             /*0x1C*/
    kExtendedInterfaceReturnPlayStatus,                          /*0x1D*/
    kExtendedInterfaceGetCurrentPlayingTrackIndex,               /*0x1E*/
    kExtendedInterfaceReturnCurrentPlayingTrackIndex,            /*0x1F*/
    kExtendedInterfaceGetIndexedPlayingTrackTitle,               /*0x20*/
    kExtendedInterfaceReturnIndexedPlayingTrackTitle,            /*0x21*/
    kExtendedInterfaceGetIndexedPlayingTrackArtistName,          /*0x22*/
    kExtendedInterfaceReturnIndexedPlayingTrackArtistName,       /*0x23*/
    kExtendedInterfaceGetIndexedPlayingTrackAlbumName,           /*0x24*/
    kExtendedInterfaceReturnIndexedPlayingTrackAlbumName,        /*0x25*/
    kExtendedInterfaceSetPlayStatusChangeNotification,           /*0x26*/
    kExtendedInterfacePlayStatusChangeNotification,              /*0x27*/
  /*kExtendedInterfaceUnusedOrDeprecated,                        0x28*/
    kExtendedInterfacePlayControl = 0x29,                        /*0x29*/
    kExtendedInterfaceGetTrackArtworkTimes,                      /*0x2A*/
    kExtendedInterfaceRetTrackArtworkTimes,                      /*0x2B*/
    kExtendedInterfaceGetShuffle,                                /*0x2C*/
    kExtendedInterfaceReturnShuffle,                             /*0x2D*/
    kExtendedInterfaceSetShuffle,                                /*0x2E*/
    kExtendedInterfaceGetRepeat,                                 /*0x2F*/
    kExtendedInterfaceReturnRepeat,                              /*0x30*/
    kExtendedInterfaceSetRepeat,                                 /*0x31*/
  /*kExtendedInterfaceSetDisplayImage,                             0x32
    kExtendedInterfaceGetMonoDisplayImageList,                     0x33
    kExtendedInterfaceReturnMonoDisplayImageList,                  0x34*/
    kExtendedInterfaceGetNumPlayingTrack = 0x35,                 /*0x35*/
    kExtendedInterfaceReturnNumPlayingTrack,                     /*0x36*/
    kExtendedInterfaceSetCurrentPlayingTrack,                    /*0x37*/
  /*kExtendedInterfaceUnusedOrDeprecated,                          0x38
    kExtendedInterfaceGetColorDisplayImageLimits,                  0x39
    kExtendedInterfaceReturnColorDisplayImageLimits,               0x3A*/
    kExtendedInterfaceResetDbSelectionHierarchy = 0x3B,          /*0x3B*/
    kExtendedInterfaceGetDbItunesInfo,                           /*0x3C*/
    kExtendedInterfaceRetDbItunesInfo,                           /*0x3D*/
    kExtendedInterfaceGetUidTrackInfo,                           /*0x3E*/
    kExtendedInterfaceRetUidTrackInfo,                           /*0x3F*/
    kExtendedInterfaceGetDbTrackInfo,                            /*0x40*/
    kExtendedInterfaceRetDbTrackInfo,                            /*0x41*/
    kExtendedInterfaceGetPbTrackInfo,                            /*0x42*/
    kExtendedInterfaceRetPbTrackInfo,                            /*0x43*/
    kExtendedInterfaceCreateGeniusPlayList,                      /*0x44*/
    kExtendedInterfaceRefreshGeniusPlayList,                     /*0x45*/
  /*kExtendedInterfaceReserved,                                  0x46*/
    kExtendedInterfaceIsGeniusAvailableForTrack = 0x0047,        /*0x47*/
    kExtendedInterfaceGetPlayListInfo,                           /*0x48*/
    kExtendedInterfaceRetPlayListInfo,                           /*0x49*/
    kExtendedInterfacePrepareUidList,                            /*0x4A*/
    kExtendedInterfacePlayPreparedUidList,                       /*0x4B*/
    kExtendedInterfaceGetArtworkTimes,                           /*0x4C*/
    kExtendedInterfaceRetArtworkTimes,                           /*0x4D*/
    kExtendedInterfaceGetArtworkData,                            /*0x4E*/
    kExtendedInterfaceRetArtworkData                             /*0x4F*/
  /*kExtendedInterfaceReserved                                     0x50 to 0xFF */
}iap_eil_commands_index_t;

typedef enum
{
    kOneByte = 0x01,
    kTwoBytes = 0x02,
    kFourBytes = 0x04,
    kEightBytes = 0x08
}iap_eil_bytes_t;

/** Return Current chapter info Structure                                                   0x03*/
typedef struct
{
    uint32_t currentChapterIndex;
    uint32_t chapterCount;
}iap_eil_ret_current_playing_track_chapter_info_t;

/** Return Current Playing Track Chapter Play Status Structure                              0x06*/
typedef struct
{
    uint32_t chapterLengthMilliseconds;
    uint32_t elapsedTimeChapterMilliseconds;
}iap_eil_ret_current_playing_track_chapter_play_status_t;

/** Audio book speed states                                                                 0x0A*/
typedef enum
{
    kNormal = 0x00,
    kFaster = 0x01,
    kSlower = 0xFF
}iap_eil_audio_book_speed_states_t;

/** Optional Restore on Exit                                                                0x0B*/
typedef enum
{
    kNewSpeedForAllAudiobooksSettingPersistsAfterAccDetach,
    kOriginalSpeedForAllAudiobooksSettingIsRestoredOnDetach,
    kSetSpeedForCurrentAudiobook
}iap_eil_audiobook_speed_optional_restore_on_exit_t;

/** Track Information type                                                                  0x0C*/
typedef enum
{
    kFormatId320px,
    kFormatId240px,
    kFormatId128px,
    kFormatId80px,
    kFormatId55px
}iap_eil_format_id_t;

/** Track Information type                                                                  0x0C*/
typedef enum
{
    kTrackCapabilitiesAndInformation,
    kPodcastName,
    kTrackReleaseDate,
    kTrackDescription,
    kTrackSongLyric,
    kTrackGenre,
    kTrackComposer,
    kTrackArtworkCount
}iap_eil_track_information_types_t;

typedef struct
{
    uint8_t trackInfoType;
    uint32_t trackIndex;
    uint16_t chapterIndex;
}eil_get_indexed_playing_track_info_t;

/** Return the requested track information type and data                                    0x0D*/
typedef struct
{
    uint8_t trackInfoType;
    uint8_t trackInformation[INDEXED_PLAYING_TRACK_INFO_BUFFER_SIZE];  /**< Variable length */
}iap_eil_ret_indexed_playing_track_info_t;

/** List of supported artwork formats                                                       0x0F*/
typedef struct
{
    uint16_t formatID;
    uint8_t pixelFormat;
    uint16_t imageWidth;
    uint16_t imageHeight;
    uint8_t* previous7bytes;
}iap_eil_ret_artwork_formats_packet_t;

typedef struct
{
    uint32_t trackIndex;
    uint16_t formatID;
    uint32_t timeOffsetFromTrackStartMS;
}eil_get_track_artwork_data_t;

/** Requested artwork packet                                                                0x11*/
typedef struct
{
    uint8_t displayPixelFormatCode;
    uint16_t imageWidthInPixels;
    uint16_t imageHeightInPixels;
    uint16_t insetRectangleTopLeftPointXValue;
    uint16_t insetRectangleTopLeftPointYValue;
    uint16_t insetRectangleBottomRightPointXValue;
    uint16_t insetRectangleBottomRightPointYValue;
    uint32_t rowSizeInBytes;
    uint8_t imagePixelData[SIZE_ARTWORK_DATA_BUFFER];
}iap_eil_ret_track_artwork_data_t;

typedef struct {
    uint8_t databaseCategoryType;
    uint32_t databaseRecordIndex;
}eil_select_db_record_t;

/** Database category type DB Records                                                       0x17*/
typedef enum
{
    kTopLevel,
    kPlayList,
    kArtist,
    kAlbum,
    kGenre,
    kTrack,
    kComposer,
    kAudiobook,
    kPodcast,
    kNestedPlaylist,
    kGeniusMixes,
    kItunesU
}iap_eil_database_category_t;

/** Return Number Categorized DB Records                                                    0x19*/
typedef uint32_t iap_eil_return_number_categorized_db_records_t;

/** Retrieve Categorized DB Records                                                         0x1A*/
typedef struct{
    uint8_t databaseCategoryType;
    uint32_t databaseRecordStartIndex;
    uint32_t databaseRecordReadCount;
}iap_eil_retrieve_db_records_t;

/** Return Categorized Database Record                                                      0x1B*/
typedef struct{
    uint16_t sizeForEachRecord;
    uint8_t numberOfRequestedRecords;
    uint8_t* recordNames[MAX_CATEGORIZED_DB_RECORDS_TO_REQUEST];
}iap_eil_return_db_records_t;

/** Play control commands (Code)                                                            0x1D*/
typedef struct
{
    uint32_t trackLengthMilliseconds;
    uint32_t trackPositionMilliseconds;
    uint8_t playerState;
}iap_eil_ret_play_status_packet_t;

typedef enum
{
    kStopped,
    kPlaying,
    kPaused
}iap_eil_player_states_t;

/** Enable notifications types                                                              0x26*/
typedef enum
{
    kGenericEnabler,
    kSpecificEnabler
}iap_eil_type_of_notification_packet_t;


/** Enable notifications types                                                              0x26*/
typedef struct
{
	uint8_t enableNotifications;
    uint8_t reserved;
    uint16_t statusChangeEnableEventMask;
}eil_set_play_status_change_notification_t;

/** Mask bits for the SetPlayStatusChangeNotification command                               0x26*/
typedef enum
{
    kBasicPlayStateChanges,
    kExtendedPlayStateChanges,
    kTrackIndex,
    kTrackTimeOffsetmsec,
    ktrackTimeOffsetsec,
    kChapterIndex,
    kChapterTimeOffsetmsec,
    kChapterTimeOffsetsec,
    kTrackUniqueIdentifier,
    kTrackMediaType,
    kTrackLyricReady,
    kTrackCapabilitiesChanged,
    kPlaybackEngineContentsChanged
}iap_eil_status_change_event_maskbits_t;

/** Play Status change notifications status changed type                                    0x27*/
typedef enum
{
    kIapEiPlaybackStopped,
    kIapEiPlaybackEngineTrackIndex,
    kIapEiPlaybackFFWSeekStop,
    kIapEiPlaybackEWSeekStop,
    kIapEiTrackTimeOffsetMsec,
    kIapEiChapterIndexChanged,
    kIapEiPlayStatusExtended,
    kIapEiTrackTimeOffsetSec,
    kIapEiChapterTimeOffsetSec,
    kIapEiChapterTimeOffsetMsec,
    kIapEiTrackUniqueID,
    kIapEiTrackPlaybackMode,
    kIapEiTrackLyricsReady,
    kIapEiTrackCapabilities,
    kIapEiPlaybackEngineContentChanged
}iap_eil_status_change_notification_t;

/** Play Status change notifications status parameters                                      0x27*/
typedef union
{
    uint8_t trackUID[8];
    uint32_t trackIndex;
    uint32_t trackOffsetMs;
    uint32_t chapterIndex;
    uint32_t trackOffsetSec;
    uint32_t chapterOffsetMs;
    uint32_t chapterOffsetSec;
    uint32_t trackCapabilities;
    uint32_t numberTracks;
    uint8_t playState;
    uint8_t playMode;
}iap_eil_status_change_parameters_t;

typedef uint32_t iap_eil_status_change_notification_track_index_t;
typedef uint32_t iap_eil_status_change_notification_time_offset_t;
typedef uint32_t iap_eil_status_change_notification_chapter_index_t;
typedef uint32_t iap_eil_status_change_notification_track_capabilities_t;
typedef uint32_t iap_eil_status_change_notification_number_of_tracks_t;
typedef uint8_t iap_eil_status_change_notification_play_state_t;
typedef uint8_t iap_eil_status_change_notification_play_mode_t;

/** Type for the Unique Track Identifier (UID)                                                  */
typedef struct
{
    uint8_t trakUniqueIdentifier[UNIQUE_TRACK_IDENTIFIER_SIZE];
}iap_eil_status_change_notification_trackUID_t;

/** Play control commands (Code)                                                            0x29*/
typedef enum
{
    kIapEiTogglePlayPause = 0x01,
    kIapEiStop,
    kIapEiNextTrack,
    kIapEiPreviousTrack,
    kIapEiStartFF = 0x05,
    kIapEiStartRew,
    kIapEiEndFFRew,
    kIapEiNext,
    kIapEiPrevious,
    kIapEiPlay,
    kIapEiPause,
    kIapEiNextChapter,
    kIapEiPreviousChapter,
    kIapEiResumeIPod
}iap_eil_play_control_command_code_t;

/** Return the list of artworks times for a given track                                     0x2B*/        
typedef struct
{
    /*! format ID indicates which type of artwork is desired. 
     *  See "Transferring Album Art" pp. 64 from Firmware specification */
    uint32_t trackIndex;
    /*! Track index specifies which track from playback engine is to be selected */
    uint16_t formatID;
    /*! Artwork index specifies at which index to begin searching for artwork */
    uint16_t artworkIndex;
    /*! Artwork count specifies the max number of times (artwork location) to be returned */
    uint16_t artworkCount;
}iap_eil_get_track_artwork_times_packet_t;

/** Return the list of artworks times for a given track                                     0x2B*/
typedef struct
{
    uint32_t timeOffestFromTrackStartMilliseconds;
    uint8_t* numberOfPacketRepeatedTimes;
}iap_eil_ret_track_artwork_times_packet_t;

/** Shuffle modes                                                                           0x2D*/
typedef enum
{
    kIapEiShuffleOff,
    kIapEiShuffleTracks,
    kIapEiShuffleAlbums
}iap_eil_shuffle_modes_t;

/** Optional Restore on Exit for set shuffle                                                0x2E*/
typedef enum
{
    kNewShuffleSettingPersistsAfterAccDetach,
    kOriginalShuffleSettingIsRestoredOnDetach
}iap_eil_shuffle_optional_eestore_on_exit_t;

/** Repeat state values                                                                     0x30*/
typedef enum
{
    kIapEiRepeatOff,
    kIapEiRepeatOnTrack,
    kIapEiRepeatAllTracks
}iap_eil_repeat_state_t;

/** Optional Restore on Exit for set repeat                                                 0x31*/
typedef enum
{
    kNewRepeatSettingPersistsAfterAccDetach,
    kOriginalRepeatSettingIsRestoredOnDetach
}iap_eil_repeat_restore_on_exit_t;

/** Return Number Playing Tracks                                                            0x31*/
typedef uint32_t iap_eil_return_num_playing_tracks_t;

/** Hierarchy Selection                                                                     0x3B*/
typedef enum
{
    kAudioHierarchySelection = 0x01,
    kVideoHierarchySelection
}iap_eil_hierarchy_selection_values_t;

/** iTunes Database Metadata Types                                                          0x3C*/
typedef enum
{
    kDatabaseUIDAssignedByItunes,
    kLastSyncWithItunes,
    kTotalAudioTrackCount,
    kTotalVideaTrackCount,
    kTotalAudiobookCount,
    kTotalPhotoCount
}iap_eil_itunes_database_metadata_t;

/** Return database itunes information structure                                            0x3D*/
typedef struct
{
    uint8_t itunesDBMetadataType;
    uint8_t itunesDBMetadataInformation[8];
}iap_eil_ret_dab_itunes_info_packet_t;

/** Return database itunes information structure                                            0x3D*/
typedef struct
{
    uint16_t formatID;
    uint8_t pixelFormat;
    uint16_t imageWidth;
    uint16_t imageHeight;
}iap_eil_ret_artwork_data_t;

/** Itunes database metadata information: Date/Time format                                  0x3D*/
typedef enum
{
    kSeconds,
    kMinute,
    kHour,
    kDay,
    kMonth,
    kYear
}iap_eil_itunes_DB_metadata_information_date_time_format_t;

/** Structure used in the commands GetDBTrackInfo (0x40) and GetPBTrackInfo (0x42) 
 * to request track Information      */
typedef struct
{
    /*! Starting track index based on the current DB track section(s) */ 
    uint32_t trackDBStartIndex;
    /*! Amount of tracks on which information is requested */
    uint32_t trackCount;
    /*! Track Information bitmask */
    uint32_t trackInformationBitmask;
}iap_eil_get_track_information_t;

/** Track Information type bits                      (0x3E & 0x3F), (0x40 & 0x41), (0x42 & 0x43)*/
typedef enum
{
    kCapabilities = 0x00,
    kTrackName,
    kArtistName,
    kAlbumName,
    kGenreName,
    kComposerName,
    kTotalTrackTimeDuration,
    kUniqueTrackID,
    kChapterCount,
    kChapterTimes,
    kChapterNames,
    kLyricOfSongCurrentlyPlaying,
    kDescription,
    kAlbumTrackIndex,
    kDiscSetAlbumIndex,
    kPlayCount,
    kSkipCount,
    kPodcastReleaseDate,
    kLastPlayedDateTime,
    kYearReleaseDate,
    kStarRating,
    kSeriesName,
    kSeasonNumber,
    kTrackVolumeAdjust,
    kTrackEQPreset,
    kTrackDataRate,
    kBookmartOffset,
    kStartStopTimeOffset
}iap_eil_track_information_type_bits_t;

/** UID Track Info Packet                                                                   0x3F*/
/** DB Track Info Packet                                                                    0x41*/
/** PB Track Info Packet                                                                    0x43*/
/*! Type to access to each field of the track information either by using an specific pointer
 *  to the field or an array of pointers */
typedef struct
{
    uint8_t identifierBuffer[UNIQUE_TRACK_IDENTIFIER_SIZE];
#if(LYRIC_OF_SONG_CURRENTLY_PLAYING_MASK == _TRUE_)
    uint16_t lyricsReceivedSize;
#endif
    union
    {
        struct
        {
            /*! Capabilities */
            uint32_t* capabilities;
            /*! Track Name */
            uint8_t* trackName;
            /*! Artist Name */
            uint8_t* artistName;
            /*! Album Name */
            uint8_t* albumName;
            /*! Genre Name */
            uint8_t* genreName;
            /*! Composer Name */
            uint8_t* composerName;
            /*! Total Track Duration in milliseconds */
            uint32_t* totalTrackDurationMs;
            /*! Unique Track ID */
            uint8_t* UniqueTrackID;
            /*! Chapter Count */
            uint16_t* chapterCount;
            /*! Chapter Times */
            uint8_t* chapterTimes;
            /*! Chapter Names*/
            uint8_t* chapterNames;
            /*! Lyric of currently playing song */
            uint8_t* lyricOfSongCurrentlyPlaying;
            /*! Description */
            uint8_t* description;
            /*! Album Track Index */
            uint16_t* albumTrackIndex;
            /*! Disc Set Album Index */
            uint16_t* discSetAlbumIndex;
            /*! Play Count */
            uint32_t* playCount;
            /*! Skip Count */
            uint32_t* skipCount;
            /*! Podcast Release Date */
            uint8_t* podcastReleaseDate;
            /*! Last Played Date/Time*/
            uint8_t* lastPlayedDateTime;
            /*! Year (release date) */
            uint16_t* yearReleaseDate;
            /*! Star Rating */
            uint8_t* starRating;
            /*! Series Name */
            uint8_t* seriesName;
            /*! Season Number */
            uint16_t* seasonNumber;
            /*! Track volume Adjust */
            uint8_t* trackVolumeAdjust;
            /*! Track EQ Preset */
            uint16_t* trackEQPreset;
            /*! Track Data Rate */
            uint32_t* trackDataRate;
            /*! Bookmark Offset */
            uint32_t* bookmarkOffset;
            /*! Start/Stop Time Offset */
            uint8_t* startStopTimeOffset;
        };
        /*! Pointers array that can be used to access any of the fields */
        uint8_t* trackInformationPointers[TRACK_INFO_NUMBER_OF_FIELDS];
    };
}iap_eil_track_information_t;

/** Type used in the command GetUIDTrackInfo to request track Information                    0x3E*/
typedef struct
{
    uint8_t trackUniqueIdentifier[UNIQUE_TRACK_IDENTIFIER_SIZE];
    uint32_t trackInformationBitmask;
}iap_eil_get_uid_track_information_t;

/** Type used to create a genius playlist along with the CreateGeniusPlaylist command       0x44*/
typedef struct
{
    uint8_t indexType;
    uint32_t trackIndex;
}iap_eil_genius_playlist_t;

/**  Index Type                                                                       0x44, 0x47*/
typedef enum
{
    kDatabaseEngine,
    kPlaybackEngine
}iap_eil_indexType_t;

/**  iPodAck Response to Create Genius Playlist                                             0x44*/
typedef enum
{
    kGeniusPlaylistIsBeingCreated = 0x00,
    kGeniusPlaylistCouldNotBeCreated = 0x02,
    kGeniusCreatedTrackIndexNotValid = 0x04,
    kGeniusInfoNotAvailableForTrack = 0x12
}iap_eil_ipodAck_response_to_create_genius_playlist_t;

/**  iPodAck Response to Refresh Genius Playlist                                            0x45*/
typedef enum
{
    kGeniusPlaylistIsBeingRefreshed = 0x00,
    kGeniusPlaylistCouldNotBeRefreshed = 0x02,
    kPlaylistIsNotAGeniusPlaylist = 0x12
}iap_eil_ipodAck_response_to_refresh_genius_playlist_t;

/**  iPodAck Response to Is Genius Available for Track                                      0x47*/
typedef enum
{
    kGeniusInformationIsAvailable = 0x00,
    kTrackIndexNotValid = 0x04,
    kGeniusInfoIsNotAvailableForTrack = 0x12
}iap_eil_ipodAck_response_to_is_genius_available_for_track_t;

/**  Get Playlist Information Type                                                          0x48*/
typedef enum
{
    kPlaylistCapabilities = 0x00
}iap_eil_playlist_info_t;

/**  Get Playlist Information Type                                                          0x48*/
typedef struct
{
    uint8_t infoType;
    uint32_t databasePlaylistIndex;
}iap_eil_get_playlist_info_packet_t;

/**  Ret Playlist Information Type                                                          0x49*/
typedef struct
{
    uint8_t infoType;
    uint8_t infoData;
}iap_eil_ret_playlist_info_packet_t;

/** Prepare UID List Type                                                                   0x4A*/
typedef struct
{
    /* not use this field */
    uint32_t reserved;
    /* matrix to save the UID of tracks with which to want create the playback list */
    uint8_t UIDList[MAX_AMOUNT_OF_TRACKS_FOR_PLAYBACK_LIST][UNIQUE_TRACK_IDENTIFIER_SIZE];
}iap_eil_prepare_uid_list_t;

/** Play Prepared UID List Type                                                             0x4B*/
typedef struct
{
    /* not use this field */
    uint8_t reserved;
    /* array to save the UID of track from which to start playback */
    uint8_t trackUID[UNIQUE_TRACK_IDENTIFIER_SIZE];
}iap_eil_play_prepared_uid_list_t;

/**  Track Identifier Type                                               0x4C, 0x4D, 0x4E & 0x4F*/
typedef enum
{
    kUID,
    kPlaybackListIndex,
    kDatabaseListIndex
}iap_eil_track_identifier_t;

/** Ret Artwork Times                                                                       0x4D*/
typedef struct
{
    uint8_t trackIdentifierType;
    uint32_t trackIndex;
    uint8_t SequenceOfTimeOffsetRecords[MAX_SIZE_OF_SEQUENCE_OF_TIMEOFFSET_RECORDS_BUFFER];
}iap_eil_ret_artwork_times_with_track_index_t;

typedef struct
{
    uint8_t trackIdentifierType;
    uint8_t trackID[UNIQUE_TRACK_IDENTIFIER_SIZE];
    uint8_t SequenceOfTimeOffsetRecords[MAX_SIZE_OF_SEQUENCE_OF_TIMEOFFSET_RECORDS_BUFFER];
}iap_eil_ret_artwork_times_with_track_id_t;

typedef void (* iap_commands_play_status_change_notification_callback_t) 
                                        (iap_eil_status_change_notification_t notificationType,
                                         iap_eil_status_change_parameters_t* notificationPayload);

#pragma pack (pop)


/*************************************************************************************************
*                                       Function-like Macros Section                           
*************************************************************************************************/

/*************************************************************************************************
*                                       Extern Constants Section                          
*************************************************************************************************/
extern const uint8_t kExtendedInterfaceAsynchronousCommands
                                         [NUMBER_OF_EXTENDEDINTERFACE_ASYNCHRONOUS_COMMANDS];
/*************************************************************************************************
*                                       Extern Variables Section                          
*************************************************************************************************/
extern void (* kExtendedInterfaceAsynchronousFunctions
    [NUMBER_OF_EXTENDEDINTERFACE_ASYNCHRONOUS_COMMANDS])(iap_interface_buffer_rx_frame_t* rxFrame);

/*************************************************************************************************
*                                      Function Prototypes Section                        
*************************************************************************************************/
uint8_t set_eil_play_status_change_notification_callback
                                 (iap_commands_play_status_change_notification_callback_t callback);

/* Extended Interface Commands ******************************************************************/
/*!
 * @brief Requests the chapter information of the current playing track                       0x02
 * @param currentPlayingTrackInfo     Pointer to the structure where the answer will be saved 
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_current_playing_track_chapter_info
                         (iap_eil_ret_current_playing_track_chapter_info_t* currentPlayingTrackInfo,
                          void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetCurrentPlayingTrackChapterInfo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) currentPlayingTrackInfo,
                                                sizeof(iap_eil_ret_current_playing_track_chapter_info_t),
                                                kExtendedInterfaceReturnCurrentPlayingTrackChapterInfo};
    
    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief Sets the currently playing chapter                                                  0x04
 * @param chapterIndex  Chapter Index
 * @param rxCallback    function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_set_current_playing_track_chapter(uint32_t chapterIndex,
                                                                           void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(chapterIndex, chapterIndex);
#endif
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &chapterIndex,
                                                sizeof(chapterIndex),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceSetCurrentPlayingTrackChapter,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Requests the chapter playtime of the currently playing track                        0x05
 * @param currentlyPlayingChapterIndex  Currently Playing Chapter Index
 * @param currentPlayingTrackStatus     Pointer to the structure where the answer will be saved 
 * @param rxCallback                    function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_current_playing_track_chapter_play_status
    (uint32_t currentlyPlayingChapterIndex,
     iap_eil_ret_current_playing_track_chapter_play_status_t* currentPlayingTrackStatus,
     void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(currentlyPlayingChapterIndex, currentlyPlayingChapterIndex);
#endif
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &currentlyPlayingChapterIndex,
                                                sizeof(currentlyPlayingChapterIndex),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetCurrentPlayingTrackChapterPlayStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) currentPlayingTrackStatus,
                                                sizeof(iap_eil_ret_current_playing_track_chapter_play_status_t),
                                                kExtendedInterfaceReturnCurrentPlayinGtrackChapterPlayStatus};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Requests a chapter name in the currently playing track                              0x07
 * @param chapterIndex  Chapter Index
 * @param rxPayLoad     Pointer to the buffer where the answer will be saved 
 * @param rxPayLoadSize response size
 * @param rxCallback    function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_current_playing_track_chapter_name(uint32_t chapterIndex,
                                                                                uint8_t* rxPayLoad,
                                                                                uint16_t rxPayLoadSize,
                                                                                void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(chapterIndex, chapterIndex);
#endif
    
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &chapterIndex,
                                                sizeof(chapterIndex),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetCurrentPlayingTrackChapterName,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceReturnCurrentPlayingTrackChapterName};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Requests the current Apple device audiobook speed state                             0x09
 * @param audioBookSpeed    Pointer to the variable where the answer will be saved 
 * @param rxCallback        Function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_audiobook_speed(iap_eil_audio_book_speed_states_t* audioBookSpeed,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetAudiobookSpeed,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) audioBookSpeed,
                                                sizeof(uint8_t),
                                                kExtendedInterfaceReturnAudiobookSpeed};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Sets the speed of audiobook playback                                                0x0B
 * @param newAudioBookSpeedCode Audio book speed state
 * @param optionalRestoreOnExit Set speed of the currently playing or for all audiobook
 * @param rxCallback            function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_set_audiobook_speed(iap_eil_audio_book_speed_states_t newAudioBookSpeedCode,
                                                             uint8_t optionalRestoreOnExit,
                                                             void (*rxCallback)(uint8_t))
{
    uint8_t localTxBuffer[2] = {(uint8_t) newAudioBookSpeedCode, optionalRestoreOnExit};
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                localTxBuffer,
                                                sizeof(localTxBuffer),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceSetAudiobookSpeed,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Gets track information for the track at the specified index                         0x0C
 * @param trackInfoType Types of information you can obtain for a track
 * @param trackIndex    track index
 * @param chapterIndex  chapter index
 * @param rxPayLoad     pointer from where the answer will be saved 
 * @param rxPayLoadSize response size
 * @param rxCallback    function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t eil_get_indexed_playing_track_info(eil_get_indexed_playing_track_info_t
                                                                 getIndexedTrackPacket,
                                                                 void (*rxCallback)(iap_eil_ret_indexed_playing_track_info_t*));

/*!
 * @brief Requests the list of supported artwork formats on the Apple device                  0x0E
 * @param rxPayLoad     pointer from where the answer will be saved 
 * @param rxPayLoadSize response size
 * @param rxCallback    function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_artwork_formats(uint8_t* rxPayLoad,
                                                             uint16_t rxPayLoadSize,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetArtworkFormats,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceReturnArtworkFormats};
                                                
    return iap_commands_transmit_command(&txCommandData);
    
}

/*!
 * @brief Requests data for a given trackIndex, formatID and artworkIndex                     0x10
 *          See "Transferring Album Art" pp. 64 from Firmware specificaiton.
 * @param trackIndex                    Track Index
 * @param formatID                      Format ID
 * @param timeOffsetFromTrackStartMS    Offset obtained from "GetTrackArtworkTimes" command
 * @param rxPayLoad                     Pointer from where the answer will be saved 
 * @param rxPayLoadSize                 Response size
 * @param rxCallback                    Function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_track_artwork_data(eil_get_track_artwork_data_t
                                                                trackArtworkDataToRequest,
                                                                iap_eil_ret_track_artwork_data_t* 
                                                                rxRetArtworkData,
                                                                uint16_t* ptrToSaveTotalPayloadSize,
                                                                void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(trackArtworkDataToRequest.timeOffsetFromTrackStartMS, 
               trackArtworkDataToRequest.timeOffsetFromTrackStartMS);
    BYTESWAP32(trackArtworkDataToRequest.trackIndex, trackArtworkDataToRequest.trackIndex);
    BYTESWAP16(trackArtworkDataToRequest.formatID, trackArtworkDataToRequest.formatID);
#endif
    
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                (uint8_t*) &trackArtworkDataToRequest,
                                                sizeof(eil_get_track_artwork_data_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetTrackArtworkData,
                                                (kCommandIsWaitingForResponse | kCommandWithMultisectionResponse)},
                                                rxCallback,
                                                ptrToSaveTotalPayloadSize,
                                                (uint8_t*) rxRetArtworkData,
                                                sizeof(iap_eil_ret_track_artwork_data_t),
                                                kExtendedInterfaceRetTrackArtworkData};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Resets the current database selection to an empty state,
 *        invalidates the category entre count for all categories except the playlist category,
 *        and sets the database hierarchy to the audio hierarchy                            0x16
 * @param rxCallback    function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_reset_db_selection(void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceResetDbSelection,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Selects one or more records in the database engine                                  0x17
 * @param databaseCategoryType  select any type of database category. 
 *                                  See table 5-27 pp. 416 from Firmware Specification
 * @param databaseRecordIndex   record index
 * @param rxCallback            function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_select_db_record(eil_select_db_record_t databaseRecord,
                                                          void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(databaseRecord.databaseRecordIndex, databaseRecord.databaseRecordIndex);  
#endif
    
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                (uint8_t*) &databaseRecord,
                                                sizeof(eil_select_db_record_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceSelectDbRecord,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
        
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Retrieves the number of records in a particular database category                   0x18
 * @param databaseCategoryType  select any type of database category. 
 *                                  See table 5-27 pp. 416 from Firmware Specification
 * @param databaseRecordCount   Pointer to the variable where the answer will be saved 
 * @param rxCallback            function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_number_categorized_db_records(iap_eil_database_category_t databaseCategoryType,
                                                                                   uint32_t* databaseRecordCount,
                                                                                   void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                (uint8_t*) &databaseCategoryType,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetNumberCategorizedDbRecords,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) databaseRecordCount,
                                                sizeof(uint32_t),
                                                kExtendedInterfaceReturnNumberCategorizedDbRecords};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Retrieves one or more database records from the apple device
 *          (typically based on the results from the command 0x18)                            0x1A
 * @param databaseCategoryType      select any type of database category. 
 *                                      See table 5-27 pp. 416 from Firmware Specification
 * @param databaseRecordStartIndex  Starting record index
 * @param databaseRecordReadCount   number of records to retrieve
 * @param rxPayLoad                 pointer from where the answer will be saved 
 * @param rxPayLoadSize             response size
 * @param rxCallback                function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t eil_retrieve_categorized_database_records(iap_eil_retrieve_db_records_t
                                                                        retDBRecordsPacket,
                                                                        uint8_t* rxPayLoad,
                                                                        uint16_t rxPayLoadSize,
                                                                        uint16_t sizeForEachRecord,
                                                                        void (*rxCallback)(uint8_t));

/*!
 * @brief Request the current apple device playback status 
 *        allowing the accessory to display feedback to the user                            0x1C
 * @param playStatus     Pointer to the structure where the answer will be saved  
 * @param rxCallback    function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_play_status(iap_eil_ret_play_status_packet_t* playStatus,
                                                                 void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGatPlayStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) playStatus,
                                                sizeof(iap_eil_ret_play_status_packet_t),
                                                kExtendedInterfaceReturnPlayStatus};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Request the playback engine index of the currently playing track                    0x1E
 * @param playbacktrackIndex    Pointer to the variable where the answer will be saved  
 * @param rxCallback            function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_current_playing_track_index(uint32_t* playbacktrackIndex,
                                                                                 void (*rxCallback)(uint8_t))
{

    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetCurrentPlayingTrackIndex,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) playbacktrackIndex,
                                                sizeof(uint32_t),
                                                kExtendedInterfaceReturnCurrentPlayingTrackIndex};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Request the title name of the indexed playing track from the apple device           0x20
 * @param playbackTrackIndex    track index
 * @param rxPayLoad             Pointer to the buffer where the payload of the answer will be saved  
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_indexed_playing_track_title(uint32_t playbackTrackIndex,
                                                                                 uint8_t* rxPayLoad,
                                                                                 uint16_t rxPayLoadSize,
                                                                                 void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(playbackTrackIndex, playbackTrackIndex);
#endif
    
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &playbackTrackIndex,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetIndexedPlayingTrackTitle,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceReturnIndexedPlayingTrackTitle};

    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Request the name of the artist of the indexed playing track                         0x22
 * @param playbackTrackIndex    track index
 * @param rxPayLoad             Pointer to the buffer where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_indexed_playing_track_artist_name(uint32_t playbackTrackIndex,
                                                                                       uint8_t* rxPayLoad,
                                                                                       uint16_t rxPayLoadSize,
                                                                                       void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &playbackTrackIndex,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetIndexedPlayingTrackArtistName,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceReturnIndexedPlayingTrackArtistName};

#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(playbackTrackIndex, playbackTrackIndex);
#else

#endif
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Request the album name of the indexed playing track                                 0x24
 * @param playbackTrackIndex    track index
 * @param rxPayLoad             Pointer to the buffer where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_indexed_playing_track_album_name(uint32_t playbackTrackIndex,
                                                                              uint8_t* rxPayLoad,
                                                                              uint16_t rxPayLoadSize,
                                                                              void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(playbackTrackIndex, playbackTrackIndex);
#endif
    
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &playbackTrackIndex,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetIndexedPlayingTrackAlbumName,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceReturnIndexedPlayingTrackAlbumName};
    
    return iap_commands_transmit_command(&txCommandData);
   
}

/*!
 * @brief Controls the status change event types sent by the apple device                     0x26
 * @param typeOfPacket                  Type of packet: either one_byte (kGenericEnable)
 *                                      or four_byte (kSpecificEnabler)
 *                                      (pp. 425 from Firmware Specification)
 * @param enableNotifications           TRUE or FALSE to enable or disable play status
 *                                      change notification, only if "one-byte" form
 *                                      is selected.
 *                                      Otherwise set this field to 0 (false).
 * @param statusChangeEnableEventMask   Status change notifications bitmask
 * @param rxPayLoad                     Pointer to the buffer where the payload of the answer
 *                                      will be saved 
 * @param rxPayLoadSize                 Size of the buffer that will store the response payload
 * @param rxCallback                    Callback function that will be called when the response 
 *                                      to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_set_play_status_change_notification(uint8_t typeOfPacket,
                                                                             eil_set_play_status_change_notification_t
                                                                             changeNotificationsPacket,
                                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &changeNotificationsPacket,
                                                sizeof(eil_set_play_status_change_notification_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceSetPlayStatusChangeNotification,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
    
    changeNotificationsPacket.reserved = 0x00;
    
    if(kGenericEnabler == typeOfPacket)
    {
        txCommandData.txFrameData.payloadSize = sizeof(uint8_t);
    }
    else
    {
#if(BIG_ENDIAN_CORE == _FALSE_)
        BYTESWAP16(changeNotificationsPacket.statusChangeEnableEventMask,
                   changeNotificationsPacket.statusChangeEnableEventMask);
#endif
    }
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Controls the media playback state of the apple device                               0x29
 * @param playControlCommandCode    Play control command code
 * @param rxCallback                Callback function that will be called when the response 
 *                                  to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_play_control(iap_eil_play_control_command_code_t playControlCommandCode,
                                                      void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &playControlCommandCode,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfacePlayControl,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Request the list of artwork time locations for a track                              0x2A
 * @param trackArtworkTimes     Structure that stores the payload to be transmitted
 * @param rxPayLoad             Pointer to the buffer where the payload of the answer
 *                              will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_track_artwork_times(iap_eil_get_track_artwork_times_packet_t
                                                                 trackArtworkTimes,
                                                                 uint8_t* rxPayLoad,
                                                                 uint16_t rxPayLoadSize,
                                                                 void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(trackArtworkTimes.trackIndex, trackArtworkTimes.trackIndex);
    BYTESWAP16(trackArtworkTimes.trackIndex, trackArtworkTimes.formatID);
    BYTESWAP16(trackArtworkTimes.trackIndex, trackArtworkTimes.artworkIndex);
    BYTESWAP16(trackArtworkTimes.trackIndex, trackArtworkTimes.artworkCount);
#endif
    
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &trackArtworkTimes,
                                                sizeof(iap_eil_get_track_artwork_times_packet_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetTrackArtworkTimes,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceRetTrackArtworkTimes};
         
        return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Request the current state of the apple device shuffle setting                       0x2C
 * @param shuffleMode   Pointer to the variable where the received data will be stored 
 * @param rxCallback    Callback function that will be called when the response 
 *                      to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_shuffle(iap_eil_shuffle_modes_t* shuffleMode,
                                                     void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetShuffle,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) shuffleMode,
                                                sizeof(uint8_t),
                                                kExtendedInterfaceReturnShuffle};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Sets the apple device shuffle mode                                                  0x2E
 * @param newShuffleMode    Shuffle mode
 * @param restorOnExit      restore any settings modified or not on detach
 * @param rxCallback        Callback function that will be called when the response 
 *                          to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_set_shuffle(iap_eil_shuffle_modes_t newShuffleMode,
                                                     uint8_t restoreOnExit,
                                                     void (*rxCallback)(uint8_t))
{
    uint8_t localTxBuffer[2] = {(uint8_t) newShuffleMode, restoreOnExit};

    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                localTxBuffer,
                                                sizeof(localTxBuffer),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceSetShuffle,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Requests the track repeat state of the apple device                                 0x2F
 * @param repeatCurrentState    Pointer to the variable where the received data will be saved
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_repeat(iap_eil_repeat_state_t* repeatCurrentState,
                                                    void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetRepeat,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) repeatCurrentState,
                                                sizeof(uint8_t),
                                                kExtendedInterfaceReturnRepeat};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Sets the repeat state of apple device                                               0x31
 * @param newRepeatMode     repeat mode
 * @param restorOnExit      restore any settings modified or not on detach
 * @param rxCallback        Callback function that will be called when the response 
 *                          to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_set_repeat(iap_eil_repeat_state_t newRepeatMode,
                                                    uint8_t restoreOnExit,
                                                    void (*rxCallback)(uint8_t))
{
    uint8_t localTxBuffer[2] = {(uint8_t) newRepeatMode, restoreOnExit};
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                localTxBuffer,
                                                sizeof(localTxBuffer),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceSetRepeat,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Requests the number of tracks in the list of tracks queued to play on apple device  0x35
 * @param numberOfTracksPlaying     Pointer to the variable that will store the received data 
 * @param rxCallback                Callback function that will be called when the response 
 *                                  to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_num_playing_tracks(uint32_t* numberOfTracksPlaying,
                                                                void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                NULL,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetNumPlayingTrack,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) numberOfTracksPlaying,
                                                sizeof(uint32_t),
                                                kExtendedInterfaceReturnNumPlayingTrack};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Sets the index of the track to play in the now playing list on the apple device     0x37
 * @param newCurrentPlayingTrackIndex   Index of the track to play in the now playing playlist
 * @param rxCallback                    Callback function that will be called when the response 
 *                                      to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_set_current_playing_track(uint32_t newCurrentPlayingTrackIndex,
                                                                   void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(newCurrentPlayingTrackIndex, newCurrentPlayingTrackIndex);
#endif
    
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &newCurrentPlayingTrackIndex,
                                                sizeof(newCurrentPlayingTrackIndex),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceSetCurrentPlayingTrack,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
                                               
    return iap_commands_transmit_command(&txCommandData);  
}

/*!
 * @brief Database Engine:                                                                    0x3B
 *          A hierarchy selection value of 0x01 -> the acc. wants to navigate the audio hierarchy
 *          A hierarchy selection value of 0x02 -> the acc. wants to navigate the video hierarchy
 * @param hierarchySelection    This value means that acc. wants to navigate 
 *                              either video or audio hierarchy
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_reset_db_selection_hierarchy(iap_eil_hierarchy_selection_values_t
                                                                      hierarchySelection, 
                                                                      void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &hierarchySelection,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceResetDbSelectionHierarchy,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Gets the specified apple device itunes database metadata information                0x3C
 * @param itunesDBMetadataType  itunes database metadata type
 * @param rxPayLoad             pointer from where the answer will be saved 
 * @param rxPayLoadSize response size
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_db_itunes_info(iap_eil_itunes_database_metadata_t
                                                            itunesDBMetadataType,
                                                            uint8_t* rxPayLoad,
                                                            uint16_t rxPayLoadSize,
                                                            void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k1000msTimeout,                
                                                (uint8_t*) &itunesDBMetadataType,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetDbItunesInfo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kExtendedInterfaceRetDbItunesInfo};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Gets the specified apple device playing track 
 *      information types for the specified track index range
 * @param trackInfoPacketPayload    Structure that contains the payload to be transmitted
 * @param indexType                 Select track information from database or playback
 *                                  check for iap_eil_indexType_t type
 * @param rxCallback                Callback function that will be called when the response 
 *                                  to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t iap_extended_interface_get_track_information(iap_eil_get_track_information_t
                                                                           trackInfoPacketPayload,
                                                                           uint8_t indexType,
                                                                           void (*rxCallback)(iap_eil_track_information_t*, uint8_t));

/*!
 * @brief Gets types of track information using the track's apple device-unique identifier
 * @param getUIDTrackInfoPacketPayload     Structure that contains the payload to be transmitted
 * @param rxCallback                       Callback function that will be called when the response 
 *                                         to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t iap_extended_interface_get_uid_track_info(iap_eil_get_uid_track_information_t
                                                                        getUIDTrackInfoPacketPayload,
                                                                        void (*rxCallback)(iap_eil_track_information_t*, uint8_t));



/*!
 * @brief Sends this command to an device to ask it to create a genius playlist               0x44
 * @param geniusPlaylistInfo     Structure that contains the information to be transmitted
 * @param rxCallback             Callback function that will be called when the response 
 *                               to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_create_genius_playlist(iap_eil_genius_playlist_t
                                                                geniusPlaylistInfo,
                                                                void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(geniusPlaylistInfo.trackIndex, geniusPlaylistInfo.trackIndex);
#endif
    iap_commands_tx_command_t txCommandData = {{k30sTimeout,                
                                                (uint8_t*) &geniusPlaylistInfo,
                                                sizeof(iap_eil_genius_playlist_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceCreateGeniusPlayList,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Sends this command to an device to ask it to refresh a genius playlist 
 *          previously created by "CreateGeniusPlayList"                                      0x45
 * @param playlistIndex Playlist Index
 * @param rxCallback    Callback function that will be called when the response 
 *                      to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_refresh_genius_playlist (uint32_t playlistIndex,
                                                                  void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(playlistIndex, playlistIndex);
#endif
    
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &playlistIndex,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceRefreshGeniusPlayList,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};

    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief Determines if genius information is available for a given track                     0x47
 * @param geniusPlaylistInfo     Structure that contains the information to be transmitted
 * @param rxCallback             Callback function that will be called when the response 
 *                               to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_is_genius_available_for_track(iap_eil_genius_playlist_t
                                                                       geniusTrackInfo,
                                                                       void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(geniusTrackInfo.trackIndex, geniusTrackInfo.trackIndex);
#endif
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &geniusTrackInfo,
                                                sizeof(iap_eil_genius_playlist_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceIsGeniusAvailableForTrack,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Gets information about a given playlist                                             0x48
 * @param infoType          Info Type (See iap_eil_playlistInfoTypes_t in typedef section)
 * @param DBPlaylistIndex   Track Index
 * @param rxPayLoad         pointer from where the answer will be saved 
 * @param rxPayLoadSize     response size
 * @param rxCallback        function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_get_playlist_info(iap_eil_get_playlist_info_packet_t getPlaylistInfo,
                                                           iap_eil_ret_playlist_info_packet_t* retPlaylistInfo,
                                                           void (*rxCallback)(uint8_t))
{
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(getPlaylistInfo.databasePlaylistIndex, getPlaylistInfo.databasePlaylistIndex);
#endif
    
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &getPlaylistInfo,
                                                sizeof(iap_eil_get_playlist_info_packet_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfaceGetPlayListInfo,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) retPlaylistInfo,
                                                sizeof(iap_eil_ret_playlist_info_packet_t),
                                                kExtendedInterfaceRetPlayListInfo};

    return iap_commands_transmit_command(&txCommandData);
}

/* Multisection command functionality not supported yet */
/*!
 * @brief Prepares a list of tracks for playblack                                             0x4A
 * @param uidList               structure with the track UIDs to prepare a list for playback 
 * @param amountOfTracksInList  amount of track UIDs
 * @param rxPayLoad             pointer from where the answer will be saved 
 * @param rxPayLoadSize         response size
 * @param rxCallback            function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_prepare_uid_list(iap_eil_prepare_uid_list_t* uidList,
                                                          uint8_t amountOfTracksInList,
                                                          void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k5000msTimeout,                
                                                (uint8_t*) uidList,
                                                0,
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfacePrepareUidList,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
    
    /* Put the section current and section max of the packet as zero 
     * to indicate that is the only one message to transmit */
    uidList->reserved = 0x00;
    
    /* get the total payload size to transmit */
    if(amountOfTracksInList > MAX_AMOUNT_OF_TRACKS_FOR_PLAYBACK_LIST)
    {
        txCommandData.txFrameData.payloadSize = sizeof(uidList);
    }
    else
    {
        txCommandData.txFrameData.payloadSize  = 
                ((amountOfTracksInList * UNIQUE_TRACK_IDENTIFIER_SIZE) + PREPARE_UID_LIST_OFFSET);
    }
       
    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief Starts playback of the list of tracks prepared by a previous "PrepareUIDList" cmd   0x4B
 * @param uidTrackToStartPlayback   structure with the UID of track from which to start playback
 * @param rxPayLoad                 pointer from where the answer will be saved 
 * @param rxPayLoadSize             response size
 * @param rxCallback                function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t eil_play_prepared_uid_list(iap_eil_play_prepared_uid_list_t*
                                                                uidTrackToStartPlayback,
                                                                void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k30sTimeout,                
                                                (uint8_t*) uidTrackToStartPlayback,
                                                sizeof(iap_eil_play_prepared_uid_list_t),
                                                0,
                                                kQuestionPacket,
                                                kExtendedInterfaceLingo,
                                                kExtendedInterfacePlayPreparedUidList,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kExtendedInterfaceIpodAck};
    
    uidTrackToStartPlayback->reserved = 0x00;
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief Requests the list of artwork time location for a track                              0x4C
 * @param trackIdentifierType   Track Identifier Type (See iap_eil_trackIdentifier_t type)
 * @param trackIdentifier       Track Identifier: UID or List Index
 * @param formatID              Indicates which type of artwork is desire (See Transferring Album Art)
 * @param artworkIndex          specifies at which index to begin searching for artwork
 * @param artworkCount          specifies the maximum number of times to be returned
 * @param rxPayLoad             pointer from where the answer will be saved 
 * @param rxPayLoadSize         response size
 * @param rxCallback            function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t eil_get_artwork_times(iap_eil_track_identifier_t trackIdentifierType,
                                                    uint8_t* trackIdentifier,
                                                    uint16_t formatID,
                                                    uint16_t artworkIndex,
                                                    uint16_t artworkCount,
                                                    uint8_t* rxPayLoad,
                                                    uint16_t rxPayLoadSize,
                                                    void (*rxCallback)(uint8_t));

/*!
 * @brief Requests data for a given track, formatID and artworkIndex (Transferring Album Art) 0x4E
   @param trackIdentifierType   Track Identifier Type (See iap_eil_trackIdentifier_t type)
   @param trackIdentifier       Track Identifier: UID or List Index
   @param formatID              Indicates which type of artwork is desire (See Transferring Album Art)
   @param timeOffset            time offset from track start (in ms)
 * @param rxPayLoad     pointer from where the answer will be saved 
 * @param rxPayLoadSize response size
 * @param rxCallback    function to which jump when a response is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t eil_get_artwork_data(iap_eil_track_identifier_t trackIdentifierType,
                                                   uint8_t* trackIdentifier,
                                                   uint16_t formatID,
                                                   uint32_t timeOffset,
                                                   uint8_t* rxPayLoad,
                                                   uint16_t rxPayLoadSize,
                                                   uint16_t* ptrToSaveTotalPayloadSize,
                                                   void (*rxCallback)(uint8_t));

/* End of Extended Interface Lingo Commands *******************************************************/

#if(BIG_ENDIAN_CORE == _FALSE_)
/*!
*  @addtogroup  ExtendedInterfaceLingoDataAccessors Extended Interface Lingo Data Accessors
*  @brief       These are the macros to obtain the 16 & 32 bits fields from the structures  
*               used, in certain commands from the Extended Interface lingo, to store the received data  
*  @{  
*/
/*! The argument for these macros is a pointer to 
 * iap_eil_ret_current_playing_track_chapter_info_t structure                               0x03*/
static inline uint32_t get_current_chapter_index
                (iap_eil_ret_current_playing_track_chapter_info_t* currentPlayingTrackChapterInfo)
{
    return  BYTESWAP32(currentPlayingTrackChapterInfo->currentChapterIndex,
            currentPlayingTrackChapterInfo->currentChapterIndex);
}

static inline uint32_t get_chapter_count
                (iap_eil_ret_current_playing_track_chapter_info_t* currentPlayingTrackChapterInfo)
{
    return BYTESWAP32(currentPlayingTrackChapterInfo->chapterCount,
            currentPlayingTrackChapterInfo->chapterCount);
}

/*! The argument for these macros is a pointer to 
 * iap_eil_ret_current_playing_track_chapter_play_status_t structure                        0x06*/
static inline uint32_t get_chapter_length
                (iap_eil_ret_current_playing_track_chapter_play_status_t* chapterPlayStatus)
{
    return BYTESWAP32(chapterPlayStatus->chapterLengthMilliseconds,
            chapterPlayStatus->elapsedTimeChapterMilliseconds);
}

static inline uint32_t get_elapsed_time_in_chapter
                (iap_eil_ret_current_playing_track_chapter_play_status_t* chapterPlayStatus)
{
    return BYTESWAP32(chapterPlayStatus->elapsedTimeChapterMilliseconds,
            chapterPlayStatus->elapsedTimeChapterMilliseconds);
}

/*! The argument for these macros is a pointer to 
 * iap_eil_ret_artwork_formats_packet_t structure                                           0x0F*/
static inline uint16_t get_format_id(iap_eil_ret_artwork_formats_packet_t* artworkFormats)
{
    return BYTESWAP16(artworkFormats->formatID, artworkFormats->formatID);
}

static inline uint16_t get_image_width(iap_eil_ret_artwork_formats_packet_t* artworkFormats)
{
    return BYTESWAP16(artworkFormats->imageWidth, artworkFormats->imageWidth);
}

static inline uint16_t get_image_height(iap_eil_ret_artwork_formats_packet_t* artworkFormats)
{
    return BYTESWAP16(artworkFormats->imageHeight, artworkFormats->imageHeight);
}

/*! The argument for these macros is a pointer to 
 * iap_eil_ret_track_artwork_data_t structure                                               0x11*/

static inline uint16_t get_image_width_in_pixels
                (iap_eil_ret_track_artwork_data_t* trackArtworkData)
{
    return BYTESWAP16(trackArtworkData->imageWidthInPixels,
            trackArtworkData->imageWidthInPixels);
}

static inline uint16_t get_image_height_in_pixels
                (iap_eil_ret_track_artwork_data_t* trackArtworkData)
{
    return BYTESWAP16(trackArtworkData->imageHeightInPixels,
            trackArtworkData->imageHeightInPixels);
}

static inline uint16_t get_top_left_point_x_value
                (iap_eil_ret_track_artwork_data_t* trackArtworkData)
{
    return BYTESWAP16(trackArtworkData->insetRectangleTopLeftPointXValue,
            trackArtworkData->insetRectangleTopLeftPointXValue);
}

static inline uint16_t get_top_left_point_y_value
                (iap_eil_ret_track_artwork_data_t* trackArtworkData)
{
    return BYTESWAP16(trackArtworkData->insetRectangleTopLeftPointYValue,
            trackArtworkData->insetRectangleTopLeftPointYValue);
}

static inline uint16_t get_bottom_right_point_x_value
                (iap_eil_ret_track_artwork_data_t* trackArtworkData)
{
    return BYTESWAP16(trackArtworkData->insetRectangleBottomRightPointXValue,
            trackArtworkData->insetRectangleBottomRightPointXValue);
}

static inline uint16_t get_bottom_right_point_y_value
                (iap_eil_ret_track_artwork_data_t* trackArtworkData)
{
    return BYTESWAP16(trackArtworkData->insetRectangleBottomRightPointYValue,
            trackArtworkData->insetRectangleBottomRightPointYValue);
}

static inline uint32_t get_row_size_in_bytes
                (iap_eil_ret_track_artwork_data_t* trackArtworkData)
{
    return BYTESWAP32(trackArtworkData->rowSizeInBytes,
            trackArtworkData->rowSizeInBytes);
}

/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x19*/
static inline uint32_t get_number_categorized_db_records(uint32_t* categorizedDbRecords)
{
    return BYTESWAP32(*categorizedDbRecords, *categorizedDbRecords);
}

/*! The argument for these macros is a pointer to 
 * iap_eil_ret_play_status_packet_t structure                                               0x1D*/
static inline uint32_t get_track_length(iap_eil_ret_play_status_packet_t* playStatus)
{
    return BYTESWAP32(playStatus->trackLengthMilliseconds,
            playStatus->trackLengthMilliseconds);
}

static inline uint32_t get_track_position(iap_eil_ret_play_status_packet_t* playStatus)
{
    return BYTESWAP32(playStatus->trackPositionMilliseconds,
            playStatus->trackPositionMilliseconds);
}

/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x1F*/
static inline uint32_t get_current_playing_track_index (uint32_t* playingTrackIndex)
{
    return BYTESWAP32(*playingTrackIndex, *playingTrackIndex);
}

/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x27*/
static inline uint32_t get_status_change_notification_track_index (uint32_t* trackIndex)
{
    return BYTESWAP32(*trackIndex, *trackIndex);
}
/*! The argument for these macros is a 
 * pointer to uint32_t type */
static inline uint32_t get_status_change_notification_time_offset (uint32_t* timeOffset)
{
    return BYTESWAP32(*timeOffset, *timeOffset);
}
/*! The argument for these macros is a 
 * pointer to uint32_t type */
static inline uint32_t get_status_change_notification_chapter_index (uint32_t* chapterIndex)
{
    return BYTESWAP32(*chapterIndex, *chapterIndex);
}
/*! The argument for these macros is a 
 * pointer to uint32_t type */
static inline uint32_t get_status_change_notification_track_capabilities (uint32_t* trackCapabilities)
{
    return BYTESWAP32(*trackCapabilities, *trackCapabilities);
}
/*! The argument for these macros is a 
 * pointer to uint32_t type */
static inline uint32_t get_status_change_notification_number_of_tracks (uint32_t* numberOfTracks)
{
    return BYTESWAP32(*numberOfTracks, *numberOfTracks);
}

/*! The argument for these macros is a pointer to 
 * iap_eil_ret_track_artwork_times_packet_t structure                                       0x2B*/
static inline uint32_t get_time_offset_from_track_start_in_ms
                (iap_eil_ret_track_artwork_times_packet_t* trackArtworkTimes)
{
    return BYTESWAP32(trackArtworkTimes->timeOffestFromTrackStartMilliseconds,
            trackArtworkTimes->timeOffestFromTrackStartMilliseconds);
}

/*! The argument for these macros is a 
 * pointer to uint32_t type                                                                 0x36*/
static inline uint32_t get_number_playing_tracks(uint32_t* numberPlayingTracks)
{
    return BYTESWAP32(*numberPlayingTracks, *numberPlayingTracks);
}

/*! The argument for these macros is a pointer to 
 * iap_eil_ret_dab_itunes_info_packet_t structure                                           0x3D*/
static inline uint16_t get_last_sync_year(iap_eil_ret_dab_itunes_info_packet_t* itunesInfo)
{
    uint16_t* lastSyncYear;
    lastSyncYear = (uint16_t*)&itunesInfo->itunesDBMetadataInformation[5];
    return BYTESWAP16(*lastSyncYear, *lastSyncYear);
}

static inline uint32_t get_total_audio_track_count(iap_eil_ret_dab_itunes_info_packet_t* itunesInfo)
{
    uint32_t* totalAudioTrackCount;
    totalAudioTrackCount = (uint32_t*)&itunesInfo->itunesDBMetadataInformation;
    return BYTESWAP32(*totalAudioTrackCount, *totalAudioTrackCount);
}

static inline uint32_t get_total_video_track_count(iap_eil_ret_dab_itunes_info_packet_t* itunesInfo)
{
    uint32_t* totalVideoTrackCount;
    totalVideoTrackCount = (uint32_t*)&itunesInfo->itunesDBMetadataInformation;
    return BYTESWAP32(*totalVideoTrackCount, *totalVideoTrackCount);
}

static inline uint32_t get_total_audiobook_count(iap_eil_ret_dab_itunes_info_packet_t* itunesInfo)
{
    uint32_t* totalAudiobookCount;
    totalAudiobookCount = (uint32_t*)&itunesInfo->itunesDBMetadataInformation;
    return BYTESWAP32(*totalAudiobookCount, *totalAudiobookCount);
}

static inline uint32_t get_total_photo_count(iap_eil_ret_dab_itunes_info_packet_t* itunesInfo)
{
    uint32_t* totalPhotoCount;
    totalPhotoCount = (uint32_t*)&itunesInfo->itunesDBMetadataInformation;
    return BYTESWAP32(*totalPhotoCount, *totalPhotoCount);
}

/*! The argument for these macros is a pointer to 
 * iap_eil_track_information_t structure                                        0x3F, 0x41, 0x43*/
static inline uint32_t get_track_info_capabilities(iap_eil_track_information_t* trackInfoData)
{
    return BYTESWAP32(*trackInfoData->capabilities, *trackInfoData->capabilities);
}

static inline uint32_t get_track_info_total_track_duration(iap_eil_track_information_t* trackInfoData)
{
    return BYTESWAP32(*trackInfoData->totalTrackDurationMs, *trackInfoData->totalTrackDurationMs);
}

static inline uint16_t get_track_info_chapter_count(iap_eil_track_information_t* trackInfoData)
{
    return BYTESWAP16(*trackInfoData->chapterCount, *trackInfoData->chapterCount);
}

static inline uint16_t get_track_info_chapter_times_index(iap_eil_track_information_t* trackInfoData)
{
    return BYTESWAP16(*trackInfoData->chapterTimes, *trackInfoData->chapterTimes);
}

static inline uint32_t get_track_info_chapter_times_offset(iap_eil_track_information_t* trackInfoData)
{
    uint32_t* chapterOffset;
    chapterOffset = (uint32_t*)((trackInfoData->chapterTimes) + 2);
    return BYTESWAP32(*chapterOffset, *chapterOffset);
}

static inline uint16_t get_track_info_chapter_names(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP16(*trackInfoData->composerName, *trackInfoData->chapterNames);
}

static inline uint16_t get_track_info_album_track_index(iap_eil_track_information_t* trackInfoData)
{
    return BYTESWAP16(*trackInfoData->albumTrackIndex, *trackInfoData->albumTrackIndex);
}

static inline uint16_t get_track_info_disc_set_album_index(iap_eil_track_information_t* trackInfoData)
{
    return BYTESWAP16(*trackInfoData->discSetAlbumIndex, *trackInfoData->discSetAlbumIndex);
}

static inline uint32_t get_track_info_play_count(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP32(*trackInfoData->playCount, *trackInfoData->playCount);
}

static inline  uint32_t get_track_info_skip_count(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP32(*trackInfoData->skipCount, *trackInfoData->skipCount);
}

static inline  uint16_t get_track_info_podcast_release_year(iap_eil_track_information_t* trackInfoData)
{
    uint16_t* podcastReleaseYear;
    podcastReleaseYear = (uint16_t*)((trackInfoData->podcastReleaseDate) + 5);
    return  BYTESWAP16(*podcastReleaseYear, *podcastReleaseYear);
}

static inline  uint16_t get_track_info_last_played_year(iap_eil_track_information_t* trackInfoData)
{
    uint16_t* lastPlayedYear;
    lastPlayedYear = (uint16_t*)((trackInfoData->lastPlayedDateTime) + 5);
    return  BYTESWAP16(*lastPlayedYear, *lastPlayedYear);
}

static inline  uint16_t get_track_info_release_year(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP16(*trackInfoData->yearReleaseDate, *trackInfoData->yearReleaseDate);
}

static inline  uint16_t get_track_info_season_number(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP16(*trackInfoData->seasonNumber, *trackInfoData->seasonNumber);
}

static inline  uint16_t get_track_info_eq_preset(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP16(*trackInfoData->trackEQPreset, *trackInfoData->trackEQPreset);
}

static inline  uint32_t get_track_info_data_rate(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP32(*trackInfoData->trackDataRate, *trackInfoData->trackDataRate);
}

static inline  uint32_t get_track_info_bookmark_offset(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP32(*trackInfoData->bookmarkOffset, *trackInfoData->bookmarkOffset);
}

static inline  uint32_t get_track_info_start_time(iap_eil_track_information_t* trackInfoData)
{
    return  BYTESWAP32(*trackInfoData->startStopTimeOffset, *trackInfoData->startStopTimeOffset);
}

static inline  uint32_t get_track_info_stop_time(iap_eil_track_information_t* trackInfoData)
{
    uint32_t* stopTime;
    stopTime = (uint32_t*)((trackInfoData->startStopTimeOffset) + 4);
    return BYTESWAP32(*stopTime, *stopTime);
}

/*! The argument for these macros is a pointer to 
 * uint8_tstructure                                                                         0x4D*/
static inline  uint32_t get_artwork_times_track_index (uint8_t* artworkTimesPacket)
{
    uint32_t* trackIndex;
    trackIndex = (uint32_t*)&artworkTimesPacket[1];
    return BYTESWAP32(*trackIndex, *trackIndex);
}

uint32_t get_artwork_times_offset_of_record(uint8_t* artworkTimesPacket, 
                                            uint8_t trackIdentifierType,
                                            uint8_t artworkIndex);

/*! The argument for these macros is a pointer to 
 * uint8_t structure                                                                         0x4F*/
static inline  uint32_t get_artwork_data_track_index(uint8_t* artworkData)
{
    uint32_t* trackIndex;
    trackIndex = (uint32_t*)&artworkData[1];
    return BYTESWAP32(*trackIndex, *trackIndex);
}

uint16_t get_arwork_data_image_width(uint8_t* artworkData, uint8_t trackIdentifierType);

uint16_t get_artwork_data_inset_rectangle_top_left_point_x_value(uint8_t* artworkData, 
                                                                 uint8_t trackIdentifierType);

uint16_t get_artwork_data_inset_rectangle_top_left_point_y_value(uint8_t* artworkData, 
                                                                 uint8_t trackIdentifierType);

uint16_t get_artwork_data_inset_rectangle_bottom_right_point_x_value(uint8_t* artworkData, 
                                                                     uint8_t trackIdentifierType);

uint16_t get_artwork_data_inset_rectangle_bottom_right_point_y_value(uint8_t* artworkData,
                                                                     uint8_t trackIdentifierType);

uint32_t get_artwork_data_row_size(uint8_t* artworkData, uint8_t trackIdentifierType);

#endif
/*!     @} 
* Group ExtendedInterfaceLingoDataAccessors
*/

/*! @} 
* Group ExtendedInterfaceLingo
* @}  
*
* Group iAP1_Lingoes
*/

#endif /* EXTENDEDINTERFACELINGO_H_ */
/*************************************************************************************************
* EOF                     
*************************************************************************************************/
