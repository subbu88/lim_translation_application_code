/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

/*************************************************************************************************
*                                      Includes Section                        
*************************************************************************************************/
#include "SimpleRemoteLingo.h"

/*************************************************************************************************
*                                   Defines & Macros Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                       Typedef Section                        
*************************************************************************************************/

/*************************************************************************************************
*                                  Function Prototypes Section                 
*************************************************************************************************/

/* Simple Remote asynchronous commands **********************************************************/

static void s_srl_async_accessory_hid_report        (iap_interface_buffer_rx_frame_t* rxFrame);

static void s_srl_async_voiceover_parameter_changed (iap_interface_buffer_rx_frame_t* rxFrame);

/* Simple Remote end of asynchronous commands ***************************************************/   

/*************************************************************************************************
*                                   Global Constants Section                   
*************************************************************************************************/

/*! Simple Remote Asynchronous Commands*/
const uint8_t kSimpleRemoteAsynchronousCommands
        [NUMBER_OF_SIMPLEREMOTE_ASYNCHRONOUS_COMMANDS] =
{
        kSimpleRemoteAccessoryHIDReport,                    // 0x11
        kSimpleRemoteVoiceOverParameterChanged,             // 0x1A
};

/*************************************************************************************************
*                                   Static Constants Section                   
*************************************************************************************************/

/*************************************************************************************************
*                                   Global Variables Section                   
*************************************************************************************************/

/*! Array of Simple Remote Lingo functions */
void (* kSimpleRemoteAsynchronousFunctions
        [NUMBER_OF_SIMPLEREMOTE_ASYNCHRONOUS_COMMANDS])(iap_interface_buffer_rx_frame_t* rxFrame) =
{
        s_srl_async_accessory_hid_report,
        s_srl_async_voiceover_parameter_changed
};

/*************************************************************************************************
*                                   Static Variables Section                   
*************************************************************************************************/
static iap_simple_remote_voice_over_parameter_changed_callback_t s_iap_simple_remote_voice_over_parameter_changed;

/*************************************************************************************************
*                                      Functions Section                       
*************************************************************************************************/
/* See SimpleRemoteLingo.h for documentation of this function. */
command_transmission_status_t srl_get_current_voice_over_item_property(voice_over_item_properties_t
                                                                       propertyType,
                                                                       uint8_t* rxPayLoad,
                                                                       uint16_t rxPayLoadSize,
                                                                       void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &propertyType,
                                                sizeof(propertyType),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteGetCurrentVoiceOverItemProperty,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                kSimpleRemoteRetCurrentVoiceOverItemProperty};
    if((kVoiveOverItemLabel == propertyType) || 
       (kVoiveOverItemValue == propertyType) || 
       (kVoiveOverItemHint == propertyType))
    {
        txCommandData.txFrameData.commandCurrentTxState |= kCommandWithMultisectionResponse;
    }
    
    return iap_commands_transmit_command(&txCommandData);
}

/* See SimpleRemoteLingo.h for documentation of this function. */
uint8_t set_srl_voice_over_parameter_changed_callback(iap_simple_remote_voice_over_parameter_changed_callback_t
                                                      callback)
{
     uint8_t returnValue = _ERROR_; 
     
     if (NULL != callback)
     {
         returnValue = _OK_;
         s_iap_simple_remote_voice_over_parameter_changed = callback;
     }
     
     return returnValue;
 } 

/*!
 * @brief Simple Remote Lingo (0x02) AccessoryHIDReport asynchronous command (0x11)  
 *
 * @param rxFrame Pointer to the received packet
 * @return void
 */
static void s_srl_async_accessory_hid_report(iap_interface_buffer_rx_frame_t* rxFrame)
{
    /* The current version of the stack does not support this functionality */
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}

/*!
 * @brief Simple Remote Lingo (0x02) VoiceOverParameterChanged asynchronous command (0x1A)  
 *
 * @param rxFrame Pointer to the received packet
 * @return void
 */
static void s_srl_async_voiceover_parameter_changed(iap_interface_buffer_rx_frame_t* rxFrame)
{
    if (NULL != s_iap_simple_remote_voice_over_parameter_changed)
    {
        s_iap_simple_remote_voice_over_parameter_changed((voice_over_parameter_changed_t*) &rxFrame->payload[0]);
    }
    
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*)rxFrame);
}


/*************************************************************************************************
* EOF
*************************************************************************************************/
