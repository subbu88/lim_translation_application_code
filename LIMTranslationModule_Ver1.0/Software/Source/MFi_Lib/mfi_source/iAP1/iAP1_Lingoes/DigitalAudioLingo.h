/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

#ifndef DIGITALAUDIOLINGO_H_
#define DIGITALAUDIOLINGO_H_


/*************************************************************************************************
*                                        Includes Section                              
*************************************************************************************************/
#include "iAP_Commands.h"

/*! @addtogroup iAP1_Lingoes iAP1 Lingoes
*   @{ 

*  @addtogroup DigitalAudioLingo Digital Audio Lingo
*  @brief  This file contains the specific types (enums and structures) and macros, the asynchronous 
*          commands functions and the inline functions used to transmit commands from the Digital 
*          Audio lingo.
*          
*  @details The specific Digital Audio lingo asynchronous commands functions have a one to one 
*           relation with the asynchronous commands from this lingo so these functions are called 
*           once their corresponding asynchronous command is received. The inline functions can be 
*           used as a mean to easily transmit an specific command from this lingo.
*  @{  
*/

/*************************************************************************************************
*                                        Defines & Macros Section                             
*************************************************************************************************/

/*!
*  @addtogroup DigitalAudioLingoDefs Digital Audio Lingo Macros and Defines
*  @{  
*/

/*! Amount of Asynchronous Commands from Digital Audio Lingo*/
#define NUMBER_OF_DIGITALAUDIO_ASYNCHRONOUS_COMMANDS        0x02

/*! Amount of Commands from Digital Audio Lingo */
#define IAP_DIGITALAUDIOLINGO_COMMANDS_COUNT                0x06

/*! @} 
* Group DigitalAudioLingoDefs
*/

/*************************************************************************************************
*                                       Typedef Section                             
*************************************************************************************************/

/*! Digital Audio Lingo Enumeration */
typedef enum
{
    kDigitalAudioAccessoryAck = 0x00u,          /* 0x00 */
    kDigitalAudioIpodAck,                       /* 0x01 */
    kDigitalAudioGetAccessorySampleRateCaps,    /* 0x02 */
    kDigitalAudioRetAccessorySampleRateCaps,    /* 0x03 */
    kDigitalAudioTrackNewAudioAttributes,       /* 0x04 */
    kDigitalAudioSetVideoDelay                  /* 0x05 */
}iap_digital_audio_lingo_commands_t;

/*************************************************************************************************
*                                       Function-like Macros Section                           
*************************************************************************************************/


/*************************************************************************************************
*                                       Extern Constants Section                          
*************************************************************************************************/
/*! Array that will be used by iAP_Commands.c, contains the values of the digital audio lingo 
 * asynchronous commands */
extern const uint8_t kDigitalAudioLingoAsynchronousCommands
        [NUMBER_OF_DIGITALAUDIO_ASYNCHRONOUS_COMMANDS];

/*************************************************************************************************
*                                       Extern Variables Section                          
*************************************************************************************************/
/*! Array that will be used by iAP_Commands.c, contains the pointers to the digital audio 
 * asynchronous commands functions  */
extern void (* kDigitalAudioLingoAsynchronousFunctions
        [NUMBER_OF_DIGITALAUDIO_ASYNCHRONOUS_COMMANDS])(iap_interface_buffer_rx_frame_t* rxFrame);

/*************************************************************************************************
*                                      Function Prototypes Section                        
*************************************************************************************************/

/*************************************************************************************************
*
* @brief   Register a callback routine for the asynchronous commands of Digital Audio lingo
* @param   uint8_t callbackId This variables works as identifier of the application callback
* @param   hostFunctionAddress Address of the function
* @return  Whether the callback was registered successfully      
* @retval  _OK_ The callback was registered successfully     
* @retval  _ERROR_ there was an error while registering the callback     
**************************************************************************************************/
uint8_t set_dal_asynchronous_command_callback(uint8_t callbackId, 
                                              iap_commands_asynchronous_callback_t hostFunctionAddress);



/* Digital Audio Lingo Commands ***********************************************************************/

/*!
 * @brief                       Digital Audio Lingo (0x0A) RetAccessorySampleRateCaps command (0x03)         
 * @param txPayLoad             Pointer to the buffer  that stores the payload 
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param rxPayLoad             Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize         Size of the buffer that will store the response payload
 * @param transactionID         The TID that will be used for this command, this parameter is 
 *                              necessary because this command is sent as a response to the
 *                              GetAccessorySampleRateCaps command (0x02) 
 * @param rxCallback            callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t dal_ret_accessory_sample_rate_caps(uint8_t* txPayLoad, 
                                                                       uint16_t txPayLoadSize,
                                                                       uint8_t* rxPayLoad,
                                                                       uint16_t rxPayLoadSize,
                                                                       uint16_t transactionID,
                                                                       void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{kNoTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                transactionID,
                                                kAnswerPacket,
                                                kDigitalAudioLingo,
                                                kDigitalAudioRetAccessorySampleRateCaps,
                                                kCommandIsNotWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                rxPayLoad,
                                                rxPayLoadSize,
                                                0};
                                                
    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief                       Digital Audio Lingo (0x0A) AccessoryAck command (0x00)         
 * @param txPayLoad             Pointer to the buffer  that stores the payload 
 * @param txPayLoadSize         Size of the buffer  that stores the data to be transmitted
 * @param transactionID         The TID that will be used for this command, this parameter is 
 *                              necessary because this command is sent as a response to the
 *                              other commands from this Lingo
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline  command_transmission_status_t dal_accessory_ack(uint8_t* txPayLoad, uint16_t txPayLoadSize,
                                                       uint16_t transactionID)
{
    iap_commands_tx_command_t txCommandData = {{kNoTimeout,                
                                                txPayLoad,
                                                txPayLoadSize,
                                                transactionID,
                                                kAnswerPacket,
                                                kDigitalAudioLingo,
                                                kDigitalAudioAccessoryAck,
                                                kCommandIsNotWaitingForResponse},
                                                NULL,
                                                NULL,
                                                NULL,
                                                0,
                                                0};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/* End of Digital Audio Lingo Commands ************************************************************/

/*! @} 
* Group DigitalAudioLingo
* @}  
*
* Group iAP1_Lingoes
*/

#endif /* DIGITALAUDIOLINGO_H_ */
/*************************************************************************************************
* EOF                     
*************************************************************************************************/
