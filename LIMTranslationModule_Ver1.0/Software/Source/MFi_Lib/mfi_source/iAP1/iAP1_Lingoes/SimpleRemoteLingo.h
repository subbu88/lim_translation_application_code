/*HEADER******************************************************************************************
*
* Copyright 2013 Freescale Semiconductor, Inc.
*
* Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See  
* the FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use  
* this file except in compliance with the License.
*
**************************************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************************************
*
* Notes: 
*    This software/document contains information restricted to MFi licensees and subject to the 
*    MFi license terms and conditions.
*
************************************************************************************************* 
*
* Comments:
*
*
**END********************************************************************************************/

#ifndef SIMPLEREMOTELINGO_H_
#define SIMPLEREMOTELINGO_H_


/*************************************************************************************************
*                                        Includes Section                              
*************************************************************************************************/
#include "iAP_Commands.h"

/*! @addtogroup iAP1_Lingoes iAP1 Lingoes
*   @{ 

*  @addtogroup SimpleRemoteLingo Simple Remote Lingo
*  @brief  This file contains the specific types (enums and structures) and macros, the asynchronous 
*          commands functions, the inline functions used to transmit commands from the Simple 
*          Remote lingo and the inline functions used to access to the received data from specific
*          commands.
*          
*  @details The specific Simple Remote Lingo asynchronous commands functions have a one to one 
*           relation with the asynchronous commands from this lingo so these functions are called 
*           once their corresponding asynchronous command is received. The inline functions can be 
*           used as a mean to easily transmit an specific command from this lingo.
*  @{  
*/

/*************************************************************************************************
*                                        Defines & Macros Section                             
*************************************************************************************************/
/*!
*  @addtogroup SimpleRemoteLingoDefs Simple Remote Lingo Macros and Defines
*  @{  
*/

/*! Amount of Asynchronous Commands from Simple Remote Lingo*/
#define NUMBER_OF_SIMPLEREMOTE_ASYNCHRONOUS_COMMANDS        0x02

/*! Amount of Commands from Simple Remote Lingo*/
#define IAP_SIMPLEREMOTE_COMMANDS_COUNT                     0x1B

/*! @} 
* Group SimpleRemoteLingoDefs
*/


/*************************************************************************************************
*                                       Typedef Section                             
*************************************************************************************************/

/*! Simple Remote Lingo Enumeration */
typedef enum
{  
    kSimpleRemoteContextStatus,                         /*0x00*/
    kSimpleRemoteIpodAck,                               /*0x01*/
    kSimpleRemoteVideoButtonStatus = 0x03,              /*0x03*/
    kSimpleRemoteAudioButtonStatus,                     /*0x04*/
    kSimpleRemoteIpodOutButtonStatus = 0x0b,            /*0x0b*/
    kSimpleRemoteRotationInputStatus,                   /*0x0c*/
    kSimpleRemoteRadioButtonStatus,                     /*0x0d*/
    kSimpleRemoteCameraButtonStatus,                    /*0x0e*/
    kSimpleRemoteRegisterDescriptor,                    /*0x0f*/
    kSimpleRemoteIpodHIDReport,                         /*0x10*/
    kSimpleRemoteAccessoryHIDReport,                    /*0x11*/
    kSimpleRemoteUnregisterDescriptor,                  /*0x12*/
    kSimpleRemoteVoiceOverEvent,                        /*0x13*/
    kSimpleRemoteGetVoiceOverParameter,                 /*0x14*/
    kSimpleRemoteRetVoiceOverParameter,                 /*0x15*/
    kSimpleRemoteSetVoiceOverParameter,                 /*0x16*/
    kSimpleRemoteGetCurrentVoiceOverItemProperty,       /*0x17*/
    kSimpleRemoteRetCurrentVoiceOverItemProperty,       /*0x18*/
    kSimpleRemoteSetVoiceOverContext,                   /*0x19*/
    kSimpleRemoteVoiceOverParameterChanged,             /*0x1A*/
    kSimpleRemoteAccessoryAck = 0x81                    /*0x81*/
}iap_simple_remote_lingo_commands_t;

/*! Context Button Status Bits (Lingo = 0x02, Cmd = 0x00) Enumeration */
typedef enum
{
    kContextButtonPlayPause,
    kContextButtonVolumeUp,
    kContextButtonVolumeDown,
    kContextButtonNextTrack,
    kContextButtonPreviousTrack,
    kContextButtonNextAlbum,
    kContextButtonPreviousAlbum,
    kContextButtonStop,
    kContextButtonPlayResume,
    kContextButtonPause,
    kContextButtonMuteToggle,
    kContextButtonNextChapter,
    kContextButtonPreviousChapter,
    kContextButtonNextPlaylist,
    kContextButtonPreviousPlaylist,
    kContextButtonShuffleSettingAdvance,
    kContextButtonRepeatSettingAdvance,
    kContextButtonPowerOn,
    kContextButtonPowerOff,
    kContextButtonBacklightFor30Sec,
    kContextButtonFastForward,
    kContextButtonBeginFastForward,
    kContextButtonBeginRewind,
    kContextButtonMenu,
    kContextButtonSelect,
    kContextButtonUpArrow,
    kContextButtonDownArrow,
    kContextButtonBacklightOff
}context_button_status_bits_t;

/*! Video Button Status Bits (Lingo = 0x02, Cmd = 0x03) Enumeration */
typedef enum
{
    kVideoButtonPlayPause,
    kVideoButtonNextVideo,
    kVideoButtonPreviouVideo,
    kVideoButtonStop,
    kVideoButtonPlayResume,
    kVideoButtonPause,
    kVideoButtonBeginFastForward,
    kVideoButtonBeginRewind,
    kVideoButtonNextChapter,
    kVideoButtonPreviousChapter,
    kVideoButtonNextFrame,
    kVideoButtonPreviousFrame
}video_button_status_bits_t;

/*! Audio Button Status Bits (Lingo = 0x02, Cmd = 0x04) Enumeration */
typedef enum
{
    kAudioButtonPlayPause,
    kAudioButtonVolumeUp,
    kAudioButtonVolumeDown,
    kAudioButtonNextTrack,
    kAudioButtonPreviousTrack,
    kAudioButtonNextAlbum,
    kAudioButtonPreviousAlbum,
    kAudioButtonStop,
    kAudioButtonPlayResume,
    kAudioButtonPause,
    kAudioButtonMuteToggle,
    kAudioButtonNextChapter,
    kAudioButtonPreviousChapter,
    kAudioButtonNextPlaylist,
    kAudioButtonPreviousPlaylist,
    kAudioButtonShuffleSettingAdvance,
    kAudioButtonRepeatSettingAdvance,
    kAudioButtonBeginFastForward,
    kAudioButtonBeginRewind,
    kAudioButtonRecord
}audio_button_status_bits_t;

/*! iPod Out Button Status Bits (Lingo = 0x02, Cmd = 0x0B) Enumeration */
/*! iPod Out Button status - Button Source */
typedef enum
{
    kiPodOutButtonSourceCarCenterConsole,
    kiPodOutButtonSourceSteeringWheel,
    kiPodOutButtonSourceCarDashboard
}ipod_out_button_source_t;

/*! iPod Out Button status - Button Mask */
typedef enum
{
    kiPodOutButtonMaskSelectEvent,
    kiPodOutButtonMaskLeftEvent,
    kiPodOutButtonMaskRightEvent,
    kiPodOutButtonMaskUpEvent,
    kiPodOutButtonMaskDownEvent,
    kiPodOutButtoMasknMenuEvent
}ipod_out_button_mask_bits_t;

/*! iPod Out Button status - Structure used to transmit the iPodOutButtonStatus command (0x0B)*/
typedef struct
{
    uint8_t buttonSource;
    uint32_t bitMask;
}srl_ipod_out_button_status_t;

/*! Rotation Input Status (Lingo = 0x02, Cmd = 0x0C) Enumeration */
/*! Rotation Input Status - Wheel Location Enumeration */
typedef enum
{
    kRotationInputWheelLocationCarCenterConsole,
    kRotationInputWheelLocationSteeringWheel,
    kRotationInputWheelLocationCarDashBoard
}rotation_input_status_wheel_location_t;

/*! Rotation Input Status - Wheel Type Enumeration*/
typedef enum
{
    kRotationInputWheelTypeFreeWheel,
    kRotationInputWheelTypeJogWheel
}rotation_input_status_wheel_type_t;

/*! Rotation Input Status - Rotation Direction Enumeration */
typedef enum
{
    kRotationInputDirectionCounterClockWise,
    kRotationInputDirectionClockWise
}rotation_input_status_rotation_direction_t;

/*! Rotation Input Status - Rotation Action Enumeration */
typedef enum
{
    /*! User has released control */
    kRotationInputActionCompleted,  
    /*! Wheel turning */
    kRotationInputActionInProgress,   
    /*! User has not advanced the wheel */
    kRotationInputActionRepeat                           
}rotation_input_status_rotation_action_t;

/*! Rotation Input Status - Rotation Type Enumeration */
typedef enum
{
    kRotationInputWheelHasDetents,
    kRotationInputReportsAngularDegrees
}rotation_input_status_rotation_types_t;

/*! Rotation Input Status struct */
typedef struct
{
    /*! Contains the time in Ms since the start of the action in the rotation input */
    uint32_t userActionDurationMs;
    /*! Number of detents of degrees the user has moved the rotation input */
    uint16_t detentOrDegreesMoved;
    /* Constant number of detents or degrees in a full turn */
    uint16_t detentOrDegreesTotal;
    /*! Field that stores the wheel location */
    uint8_t rotationSource;
    /*! Field that stores the type of wheel */
    uint8_t controllerType;
    /*! Field that stores the rotation direction */
    uint8_t rotationDirection;
    /*! Field that stores the rotation type of action  */
    uint8_t rotationAction;
    /*! Field that stores the rotation type */
    uint8_t rotationType;
}rotation_input_status_t;

/*! Radio Button Status Values (Lingo = 0x02, Cmd = 0x0D)  Enumeration */
typedef enum
{
    kRadioButtonReleased,
    kRadioButtonPushed = 2
}radio_button_status_values_t;

/*! Camera Button Status Values (Lingo = 0x02, Cmd = 0x0E) Enumeration */
typedef enum
{
    kCameraButtonUp,
    kCameraButtonDown
}camera_button_status_values_t;

/*! Structure used to send the RegisterDescriptor command (0x0F) */
typedef struct
{
    uint8_t usbDescriptorIndex;
    uint16_t vendorId;
    uint16_t productId;
    uint8_t countryCode; 
    uint8_t usbHidDescriptor;
}srl_register_descriptor_t;

/*! Structure used to send the iPodHIDReport command (0x10) */
typedef struct
{
    uint8_t usbDescriptorIndex;
    uint8_t reportType;
    uint8_t usbHidReport;
}srl_ipod_hid_report_t;
                                                         
/*! Voice Over Event Types (Lingo = 0x02, Cmd = 0x13) Enumeration */
typedef enum
{
    kVoiceOverEventMoveToAPointOnTheScreen,
    kVoiceOverEventMoveToFirst,
    kVoiceOverEventMoveToLast,
    kVoiceOverEventMoveToNext,
    kVoiceOverEventMoveToPrevious,
    kVoiceOverEventScrollLeftPage,
    kVoiceOverEventScrollRightPage,
    kVoiceOverEventScrollUpPage,
    kVoiceOverEventScrollDownPage,
    kVoiceOverEventScrollToAPointOnTheScreen,
    kVoiceOverEventSendTextToInputField,
    kVoiceOverEventCut,
    kVoiceOverEventCopy,
    kVoiceOverEventPaste,
    kVoiceOverEventHome,
    kVoiceOverEventSendTouchEventToCurrentItem,
    kVoiceOverEventSetScaleDisplayFactor,
    kVoiceOverEventCenterDisplayAroundAPoint,
    kVoiceOverEventPauseSpeaking,
    kVoiceOverEventResumeSpeaking,
    kVoiceOverEventTextFromCurrentPoint,
    kVoiceOverEventTextFromTop,
    kVoiceOverEventTextToBeSpokenByTheIOS,
    kVoiceOverEventEscape
}voice_over_event_values_t;

/*! Voice Over Event - Move to a point on the screen Structure */
/*! Voice Over Event - Scroll to a point on the screen  Structure */
/*! Voice Over Event - Center display around a point on the screen Structure */
typedef struct
{
    uint8_t  eventType;                        /*! Event Type = 0x00,0x09,0x11 */
    uint16_t xCoordinate;
    uint16_t yCoordinate;
}voice_over_event_xy_coordinate_t;

/*! Voice Over Event - Send text from the accessory to the input field Structure */
/*! Voice Over Event - Send text to be spoken by the iOS Structure */
typedef struct
{
    uint8_t  eventType;                        /*! Event Type = 0x0A,0x16 */
    uint16_t currentSectionIndex;
    uint16_t lastSectionIndex;
    uint8_t  text[128];
}voice_over_event_text_sections_t;

/*! Voice Over Event - Touch Event Types Enumeration */
typedef enum
{
    kVoiceOverTouchEventBegan = 0,
    kVoiceOverTouchEventMoved,
    kVoiceOverTouchEventStationary,
    kVoiceOverTouchEventEnded,
    kVoiceOverTouchEventCanceled
}voice_over_touch_event_values_t;

/*! Voice Over Event - Send touch event to the current VoiceOver item Structure */
typedef struct
{
    uint8_t  eventType;                        /*! Event Type = 0x0F */
    uint32_t reserved;                         /*! Set to zero */
    uint8_t  touchEventType;
}voice_over_touch_event_t;

/*! Voice Over Event - Set scale factor Structure */
typedef struct
{
    uint8_t  eventType;                        /*! Event Type = 0x10 */
    uint16_t scaleFactor;
}voice_over_scale_factor_t;

/*! Voice Over Event - Center display around a point Structure 
 *  Event type 0x11, Bytes 1 and 2, X Cordinate, Bytes 3 and 4, Y Coordinate*/
typedef struct
{
    uint8_t  eventType;
    uint16_t xCoordinate;
    uint16_t yCoordinate;
}voice_over_center_display_t;

/*! Voice Over Event - Send text from the accessory to an iPhone Structure 
 *  Event type 0x16, Bytes 1 and 2, current section index, bytes 3 and 4 last section index, 5 to NN, String*/
typedef struct
{
    uint8_t  eventType;
    uint16_t currentSectionIndex;
    uint16_t lastSectionIndex;
    uint8_t  dataBuffer[64];                /*! Can be adjusted based on the application needs */
}voice_over_send_text_t;

/*! Get, Ret and Set Voice event Parameters (Lingo = 0x02, Cmd = 0x14, Cmd = 0x15, Cmd = 0x16) Enumeration */
typedef enum
{
    kGetVoiceOverVolume = 0ul,
    kGetVoiceOverSpeakingRate,
    kGetVoiceOverEnabled
}voice_over_parameters_t;

/*! Return Voice Over Parameter (Lingo = 0x02, Cmd = 0x15) Structure */
typedef struct
{
    uint8_t paramType;
    uint8_t paramValue;
}ret_voice_over_parameter_t;

/*! Set Voice Over Parameter (Lingo = 0x02, Cmd = 0x16) Structure */
typedef struct
{
    uint8_t paramType;
    uint8_t paramValue;
}srl_set_voice_over_parameter_t;

/*! Return and Get Current Voice Over Item Property  (Lingo = 0x02, Cmd = 0x17 and Cmd = 0x18) Enumeration */
typedef enum
{
    kVoiveOverItemLabel = 0,
    kVoiveOverItemValue,
    kVoiveOverItemHint,
    kVoiveOverItemFrame,
    kVoiveOverItemTraits
}voice_over_item_properties_t;

/*! Return Current Voice Over Item Property - Label (Lingo = 0x02, Cmd = 0x18) Estructure */
typedef struct
{
    uint8_t  property;                        /*! Property = 0x00 */
    uint16_t currentSection;
    uint16_t lastSection;
    uint8_t  valueString[64];
}ret_voice_over_item_label_t;

/*! Return Current Voice Over Item Property - Value (Lingo = 0x02, Cmd = 0x18) Estructure */
typedef struct
{
    uint8_t  property;                        /*! Property = 0x01 */
    uint16_t currentSection;
    uint16_t lastSection;
    uint8_t  valueString[64];
}ret_voice_over_item_value_t;

/*! Return Current Voice Over Item Property - Hint (Lingo = 0x02, Cmd = 0x18) Estructure */
typedef struct
{
    uint8_t  property;                        /*!< Property = 0x02 */
    uint16_t currentSection;
    uint16_t lastSection;
    uint8_t  valueString[64];
}ret_voice_over_item_hint_t;

/*! Return Current Voice Over Item Property - Frame (Lingo = 0x02, Cmd = 0x18) Estructure */
typedef struct
{
    uint8_t  property;                        /*! Property = 0x03 */
    uint16_t topLeftXCoord;
    uint16_t topLeftYCoord;
    uint16_t bottomRightXCoord;
    uint16_t bottomRightYCoord;
}ret_voice_over_item_frame_t;

/*! Return Current Voice Over Item Property - Traits (Lingo = 0x02, Cmd = 0x18) Estructure */
typedef struct
{
    uint8_t  property;                        /**< Property = 0x04 */
    uint16_t traitValues[64];
}ret_voice_over_item_traits_t;

/*! Set Voice Over Context and Voice Over parameter changed enumeration. (Lingo = 0x02, Cmd = 0x19
 *  and Cmd = 0x1A) */
typedef enum
{
    kVoiceOverContextNone = 0ul,
    kVoiceOverContextHeader,
    kVoiceOverContextLink,
    kVoiceOverContextForm,
    kVoiceOverContextCursor,
    kVoiceOverContextVerticalNavigation,
    kVoiceOverContextValueAdjustment,
    kVoiceOverContextZoomAdjustment
}voice_over_context_t;

/*! VoiceOverParameterChanged - Lingo = 0x02, Cmd = 0x1A */
typedef struct
{
    uint8_t parameterType;
    uint8_t parameterValue;
}voice_over_parameter_changed_t;

typedef struct
{
    uint8_t ackStatus;
    uint8_t commandID;
}iap_srl_accessory_ack_t;

typedef void (* iap_simple_remote_voice_over_parameter_changed_callback_t)(voice_over_parameter_changed_t* voiceOverParameter);

/*************************************************************************************************
*                                       Function-like Macros Section                           
*************************************************************************************************/

/*************************************************************************************************
*                                       Extern Constants Section                          
*************************************************************************************************/
/*! Array that will be used by iAP_Commands.c, contains the values of the simple remote 
 * asynchronous commands */
extern const uint8_t kSimpleRemoteAsynchronousCommands[NUMBER_OF_SIMPLEREMOTE_ASYNCHRONOUS_COMMANDS];


/*************************************************************************************************
*                                       Extern Variables Section                          
*************************************************************************************************/
/*! Array that will be used by iAP_Commands.c, contains the pointers to the simple remote 
 * asynchronous commands functions  */
extern void (* kSimpleRemoteAsynchronousFunctions[NUMBER_OF_SIMPLEREMOTE_ASYNCHRONOUS_COMMANDS])
                                                                (iap_interface_buffer_rx_frame_t* rxFrame);
/*************************************************************************************************
*                                      Function Prototypes Section                        
*************************************************************************************************/
/*!
* @brief   Register a callback routine for the asynchronous command VoiceOverParameterChanged
* @param   callback Address of the function
* @return  Whether the callback was registered successfully      
* @retval  _OK_ The callback was registered successfully     
* @retval  _ERROR_ there was an error while registering the callback    
 */
uint8_t set_srl_voice_over_parameter_changed_callback(iap_simple_remote_voice_over_parameter_changed_callback_t
                                                      callback);

/* Simple Remote Commands ***********************************************************************/

/*!
 * @brief                       Simple Remote Lingo (0x02) ContextButtonStatus command (0x00)         
 * @param contextButtonStatus   Contains the context buttons current status 
 *                                 (check "context_button_status_bits_t" type in SimpleRemoteLingo.h)
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_context_button_status(uint32_t contextButtonStatus)
{
    iap_commands_tx_command_t txCommandData = {{k50msTimeout,                
                                                (uint8_t*) &contextButtonStatus, 
                                                sizeof(contextButtonStatus),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteContextStatus,
                                                (kCommandIsNotWaitingForResponse | kCommandRepeatsPeriodically)},
                                                NULL,
                                                NULL,
                                                NULL,
                                                0,
                                                0};
                                                
    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief               Function used to signal a release of the context buttons 
 *                         (Not a command from the spec.)          
 * @return void
 */
static inline command_transmission_status_t srl_context_button_status_release(void)
{
    uint32_t releaseValue = 0;
    iap_commands_tx_command_t txCommandData = {{kNoTimeout,                
                                                (uint8_t*) &releaseValue,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteContextStatus,
                                                kCommandIsNotWaitingForResponse},
                                                NULL,
                                                NULL,
                                                NULL,
                                                0,
                                                0};
    
    return iap_commands_release_periodical_command(&txCommandData);
}

/*!
 * @brief                       Simple Remote Lingo (0x02) VideoButtonStatus command (0x03)         
 * @param videoButtonStatus     Contains the video buttons current status 
 *                              (check "video_button_status_bits_t" type in SimpleRemoteLingo.h)
 * @param rxCallback            callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_video_button_status(uint32_t videoButtonStatus, 
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k50msTimeout,                
                                                (uint8_t*) &videoButtonStatus,
                                                sizeof(videoButtonStatus),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteVideoButtonStatus,
                                                (kCommandIsWaitingForResponse | kCommandRepeatsPeriodically)},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Function used to signal a release of the video buttons 
 *                              (Not a command from the spec.)     
 * @param rxCallback            callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_video_button_status_release(void (*rxCallback)(uint8_t))
{
    uint32_t releaseValue = 0;
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &releaseValue,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteVideoButtonStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
    
    return iap_commands_release_periodical_command(&txCommandData);

}

/*!
 * @brief                       Simple Remote Lingo (0x02) AudioButtonStatus command (0x04)         
 * @param audioButtonStatus     Contains the audio buttons current status 
 *                              (check "video_button_status_bits_t" type in SimpleRemoteLingo.h)
 * @param rxCallback            callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @retval check "command_transmission_status_t" type iAP_Commands.h
 */
static inline command_transmission_status_t srl_audio_button_status(uint32_t audioButtonStatus, 
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k50msTimeout,                
                                                (uint8_t*) &audioButtonStatus,
                                                sizeof(audioButtonStatus),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteAudioButtonStatus,
                                                (kCommandIsWaitingForResponse | kCommandRepeatsPeriodically)},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief               Function used to signal a release of the audio buttons 
 *                      (Not a command from the spec.)          
 * @param rxCallback    callback function that will be called when the response 
 *                      to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_audio_button_status_release(void (*rxCallback)(uint8_t))
{
    uint32_t releaseValue = 0;
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &releaseValue,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteAudioButtonStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
    
    return iap_commands_release_periodical_command(&txCommandData);
    
}

/*!
 * @brief                       Simple Remote Lingo (0x02) iPodOutButtonStatus command (0x0B)         
 * @param iPodButtonStatus      Contains all the informations needed to send this command 
 *                              (check "srl_ipod_out_button_status_t" type in SimpleRemoteLingo.h)
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_ipod_out_button_status(srl_ipod_out_button_status_t 
                                                                iPodButtonStatus,
                                                                void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k50msTimeout,                
                                                (uint8_t*) &iPodButtonStatus,
                                                sizeof(srl_ipod_out_button_status_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteIpodOutButtonStatus,
                                                (kCommandIsWaitingForResponse | kCommandRepeatsPeriodically)},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
                                                
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(iPodButtonStatus.bitMask, iPodButtonStatus.bitMask);
#endif
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief               Function used to signal a release of the ipod out buttons 
 *                      (Not a command from the spec.)          
 * @param rxCallback    Callback function that will be called when the response 
 *                      to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_ipod_out_button_status_release(void (*rxCallback)(uint8_t))
{
    uint32_t releaseValue = 0;
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &releaseValue,
                                                sizeof(uint32_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteIpodOutButtonStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
    
    return iap_commands_release_periodical_command(&txCommandData);
   
}


/*!
 * @brief                       Simple Remote Lingo (0x02) RotationInputStatus command (0x0C)     
 * @param rotationInputData     Pointer to a structure that contains all the data of the 
 *                              rotation input 
 *                              (check "rotation_input_status_rotation_direction_t" type 
 *                              in SimpleRemoteLingo.h)
 * @param rxCallback            callback function that will be called when the response 
 *                                to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_rotation_input_status(rotation_input_status_t rotationInputData,
                                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k50msTimeout,                
                                                (uint8_t*) &rotationInputData,
                                                sizeof(rotation_input_status_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteRotationInputStatus,
                                                (kCommandIsWaitingForResponse | kCommandRepeatsPeriodically)},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
    
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(rotationInputData.userActionDurationMs, rotationInputData.userActionDurationMs);
    BYTESWAP16(rotationInputData.detentOrDegreesMoved, rotationInputData.detentOrDegreesMoved);
    BYTESWAP16(rotationInputData.detentOrDegreesTotal, rotationInputData.detentOrDegreesTotal);
#endif

    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Function used to signal a release of the rotation input 
 *                              (Not a command from the spec.)          
 * @param rotationInputData     Pointer to a structure that contains all the data of the 
 *                              rotation input (check "rotation_input_status_rotation_direction_t" 
 *                              type in SimpleRemoteLingo.h)
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_rotation_input_status_release(rotation_input_status_t rotationInputData,
                                                                       void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &rotationInputData,
                                                sizeof(rotation_input_status_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteRotationInputStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
#if(BIG_ENDIAN_CORE == _FALSE_)
    rotationInputData.rotationAction = (rotation_input_status_rotation_action_t) 0x00;
    BYTESWAP32(rotationInputData.userActionDurationMs, rotationInputData.userActionDurationMs);
    BYTESWAP16(rotationInputData.detentOrDegreesMoved, rotationInputData.detentOrDegreesMoved);
    BYTESWAP16(rotationInputData.detentOrDegreesTotal, rotationInputData.detentOrDegreesTotal);
#endif

    return iap_commands_release_periodical_command(&txCommandData);
    
}


/*!
 * @brief                       Simple Remote Lingo (0x02) RadioButtonStatus command (0x0D)         
 * @param radioButtonStatus     Contains the radio button current status 
 *                              (check "radio_button_status_values_t" type in SimpleRemoteLingo.h)
 * @param rxCallback            callback function that will be called when the response 
 *                                to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_radio_button_status(radio_button_status_values_t radioButtonStatus, 
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k50msTimeout,                
                                                (uint8_t*) &radioButtonStatus,
                                                sizeof(radioButtonStatus),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteRadioButtonStatus,
                                                (kCommandIsWaitingForResponse | kCommandRepeatsPeriodically)},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
    
    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief               Function used to signal a release of the radio button 
 *                      (Not a command from the spec.)          
 * @param rxCallback    Callback function that will be called when the response 
 *                      to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_radio_button_status_release(void (*rxCallback)(uint8_t))
{
    uint8_t releaseValue = 0;
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                &releaseValue,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteRadioButtonStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};

    return iap_commands_release_periodical_command(&txCommandData);
}

/*!
 * @brief                       Simple Remote Lingo (0x02) CameraButtonStatus command (0x0E)         
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_camera_button_status(void (*rxCallback)(uint8_t))
{
    command_transmission_status_t txStatus;
    uint8_t cameraButton;
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                &cameraButton,
                                                sizeof(cameraButton),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteCameraButtonStatus,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
    
    /* Command is waiting for response */
    cameraButton = kCameraButtonDown;
    txStatus = iap_commands_transmit_command(&txCommandData);
    
    cameraButton = kCameraButtonUp;
    txStatus |= iap_commands_transmit_command(&txCommandData);
    
    return txStatus;
}

/*!
 * @brief                   Simple Remote Lingo (0x02) RegisterDescriptor command (0x0F)         
 * @param descriptorInfo    Structure that contains all the information needed to send the command
 * @param rxCallback        Callback function that will be called when the response 
 *                          to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_register_descriptor(srl_register_descriptor_t descriptorInfo,
                                                             void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &descriptorInfo,
                                                sizeof(srl_register_descriptor_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteRegisterDescriptor,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
    
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP16(descriptorInfo.productId, descriptorInfo.productId);
    BYTESWAP16(descriptorInfo.vendorId, descriptorInfo.vendorId);
#endif
    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                Simple Remote Lingo (0x02) iPodHIDReport command (0x10)         
 * @param hidReportInfo  Structure that contains all the packet payload to be transmitted
 * @param rxCallback     callback function that will be called when the response 
 *                       to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_ipod_hid_report(srl_ipod_hid_report_t hidReportInfo,
                                                         void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &hidReportInfo,
                                                sizeof(srl_ipod_hid_report_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteIpodHIDReport,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
        
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                       Simple Remote Lingo (0x02) UnregisterDescriptor command (0x12)     
 * @param usbDescriptorIndex    USB descriptor index that will be unregistered
 * @param rxCallback            callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_unregister_descriptor(uint8_t usbDescriptorIndex,
                                                               void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                &usbDescriptorIndex,
                                                sizeof(uint8_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteUnregisterDescriptor,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};

    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief                Simple Remote Lingo (0x02) VoiceOverEvent command (0x13)         
 * @param txPayLoad      Pointer to the buffer or structure that stores the payload 
 *                       to be transmitted (check "voice_over_send_text_t"
                         "voice_over_center_display_t", "voice_over_scale_factor_t"
                         "voice_over_touch_event_t", "voice_over_event_text_sections_t" and
                         "voice_over_event_xy_coordinate_t" types in SimpleRemoteLingo.h)
 * @param txPayLoadSize  Size of the buffer or structure that stores the data to be transmitted
 * @param rxPayLoad      Pointer to where the payload of the answer will be saved 
 * @param rxCallback     callback function that will be called when the response 
 *                       to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_voice_over_event(uint8_t* txPayload,
                                                          uint16_t txPayLoadSize,
                                                          void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                txPayload,
                                                txPayLoadSize,
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteVoiceOverEvent,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
    
    return iap_commands_transmit_command(&txCommandData);

}

/*!
 * @brief                       Simple Remote Lingo (0x02) GetVoiceOverParameter command (0x14)   
 * @param paramType             The type of parameter to be returned by the Apple device 
 *                              (check "voice_over_parameters_values_t" type in SimpleRemoteLingo.h)      
 * @param voiceOverParameter    Pointer to the structure where the received payload will be saved 
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_get_voice_over_parameter(voice_over_parameters_t paramType,
                                                                  ret_voice_over_parameter_t* voiceOverParameter,
                                                                  void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &paramType,
                                                sizeof(paramType),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteGetVoiceOverParameter,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                (uint8_t*) &voiceOverParameter,
                                                sizeof(ret_voice_over_parameter_t),
                                                kSimpleRemoteRetVoiceOverParameter};
                                                
    return iap_commands_transmit_command(&txCommandData);
    
}

/*!
 * @brief                       Simple Remote Lingo (0x02) SetVoiceOverParameter command (0x16)   
 * @param voiceOverParameter    Structure that contains the paramType and the paramValue to be set
 *                              for an specific voice over parameter
 * @param rxCallback            Callback function that will be called when the response 
 *                              to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_set_voice_over_parameter(srl_set_voice_over_parameter_t 
                                                                  voiceOverParameter,
                                                                  void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &voiceOverParameter,
                                                sizeof(srl_set_voice_over_parameter_t),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteSetVoiceOverParameter,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
                                                    
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                Simple Remote Lingo (0x02) GetCurrentVoiceOveritemProperty command (0x17)   
 * @param propertyType   The type of parameter to be returned by the Apple device 
 *                       (check "voice_over_item_properties_t" type in SimpleRemoteLingo.h)      
 * @param rxPayLoad      Pointer to where the payload of the answer will be saved 
 * @param rxPayLoadSize  Size of the buffer that will store the response payload
 * @param rxCallback     callback function that will be called when the response 
 *                       to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
command_transmission_status_t srl_get_current_voice_over_item_property(voice_over_item_properties_t
                                                                       propertyType,
                                                                       uint8_t* rxPayLoad,
                                                                       uint16_t rxPayLoadSize,
                                                                       void (*rxCallback)(uint8_t));

/*!
 * @brief                Simple Remote Lingo (0x02) SetVoiceOverContext command (0x19)   
 * @param paramType      The user interface voice over context to be set 
 *                       (check "voice_over_context_t" type in SimpleRemoteLingo.h)      
 * @param rxCallback     callback function that will be called when the response 
 *                       to the command is received
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_set_voice_over_context(voice_over_context_t paramType,
                                                                void (*rxCallback)(uint8_t))
{
    iap_commands_tx_command_t txCommandData = {{k500msTimeout,                
                                                (uint8_t*) &paramType,
                                                sizeof(paramType),
                                                0,
                                                kQuestionPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteSetVoiceOverContext,
                                                kCommandIsWaitingForResponse},
                                                rxCallback,
                                                NULL,
                                                NULL,
                                                0,
                                                kSimpleRemoteIpodAck};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/*!
 * @brief                Simple Remote Lingo (0x02) AccessoryAck command (0x81)   
 * @param accAck         Structure that contains the payload to be transmitted
 * @param transactionID  The TID that will be used for this command, this parameter is 
 *                       necessary because this command is sent as a response to the
 *                       other commands from this Lingo
 * @return Transmitted command status
 * @sa command_transmission_status_t
 */
static inline command_transmission_status_t srl_accessory_ack(iap_srl_accessory_ack_t accAck,
                                                       uint16_t transactionID)
{
    iap_commands_tx_command_t txCommandData = {{kNoTimeout,                
                                                (uint8_t*) &accAck,
                                                sizeof(iap_srl_accessory_ack_t),
                                                transactionID,
                                                kAnswerPacket,
                                                kSimpleRemoteLingo,
                                                kSimpleRemoteAccessoryAck,
                                                kCommandIsWaitingForResponse},
                                                NULL,
                                                NULL,
                                                NULL,
                                                0,
                                                0};
                                                
    return iap_commands_transmit_command(&txCommandData);
}

/* End of Simple Remote Commands ***********************************************************/
/*! @} 
* Group SimpleRemoteLingo
* @}  
*
* Group iAP1_Lingoes
*/

#endif /* SIMPLEREMOTELINGO_H_ */
/*************************************************************************************************
* EOF                     
*************************************************************************************************/
