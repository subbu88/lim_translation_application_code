/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_INTERFACE_H_
#define IAP_INTERFACE_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "ProjectTypes.h"
#include <stdbool.h>

#include "MFi_Config.h"

#include "iAP_AccessoryIO.h"
#include "BuffersAlloc.h"
#include "iAP_Transport.h"
#include "RandomAccessArray.h"
#if (MFI_TRANSPORT_USB_DEVICE_MODE == _TRUE_)
    #include  "iAP_USBHost.h"
#endif
#if (MFI_TRANSPORT_USB_HOST_MODE == _TRUE_)
    #include  "iAP_USBDevice.h"
#endif

/*! @addtogroup iAP1 iAP1 Software Stack
* @{
* 
* @addtogroup iAP_Interface iAP1 Interface Software Layer
* @brief    This layer handles the queueing of packets (Rx and Tx), the interaction with the 
*           transport layer and the commands that need to be retried.
*           
* @details  The layers builds and sends the packets to be transmitted (or queues them in the Tx 
*           queue if the transport layer is busy). It also puts the received bytes in an specific 
*           format and once a complete packet has been received, the iAP_Interface queues it 
*           (in the Rx queue) so the upper layers can easily have access to the received packet.
*           To handle the commands that need to be retried this layer is constantly checking for 
*           a command timeout field that signals whether a command should be retried.
* @{
 */

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#if defined(IAPI_INTERFACE_UART)
#define IAPI_TX_SYNC_BYTE_ENABLE                (_TRUE_)
#define IAPI_RX_SYNC_BYTE_ENABLE                (_FALSE_)
#else
#define IAPI_TX_SYNC_BYTE_ENABLE                (_FALSE_)
#define IAPI_RX_SYNC_BYTE_ENABLE                (_FALSE_)
#endif

//TODO rename this
#define USB_ATTACH                              (1<<1)

/*! TODO document this Defines IOS Detect */ 
#if defined(IAPI_INTERFACE_UART)
    /*! When using UART or USB Host, Apple device detect pin must be used for the iOS devices detection */
    #define IAPI_IOSDETECT_LOW                      iap_accessory_io_ios_detect_low()
    #define IAPI_IOSDETECT_HIGH                     iap_accessory_io_ios_detect_high()
    
    /*! When using UART or USB Host, Accessory Power pin will output a 3.3v voltage upon connection */ 
    #define IAPI_ACCPWR_HIGH                        iap_accessory_io_acc_power_high()
    #define IAPI_ACCPWR_LOW                         iap_accessory_io_acc_power_low()
    
    /*! Used to signal when there is a transmission in progress through the transport used */ 
    #define IAP_INTERFACE_TX_IN_PROGRESS_FLAG       (iAP_Transport_Status() & (1 << UART_TX_IN_PROGRESS))

#elif defined(IAPI_INTERFACE_USB_HOST)
    /*! When using USB device mode, the iOS Detection is done through a USB Attach event */
    #define IAPI_IOSDETECT_LOW                      (iAP_Transport_Status() & USB_ATTACH)
    #define IAPI_IOSDETECT_HIGH                     (!(iAP_Transport_Status() & USB_ATTACH))
    /*! When using USB device mode Accessory Power is not needed for device detection */
    #define IAPI_ACCPWR_HIGH                        _TRUE_
    #define IAPI_ACCPWR_LOW                         _FALSE_
    /*! Used to signal when there is a transmission in progress through the transport used */
    #define IAP_INTERFACE_TX_IN_PROGRESS_FLAG       (iAP_Transport_Status() & (1 << USB_HOST_TX_IN_PROGRESS))

#else
    /*! When using UART or USB Host, Apple device detect pin must be used for the iOS devices detection */        
    #define IAPI_IOSDETECT_LOW                      iap_accessory_io_ios_detect_low()
    #define IAPI_IOSDETECT_HIGH                     iap_accessory_io_ios_detect_high()
    /*! When using UART or USB Host, Accessory Power pin will output a 3.3v voltage upon connection */
    #define IAPI_ACCPWR_HIGH                        iap_accessory_io_acc_power_high()
    #define IAPI_ACCPWR_LOW                         iap_accessory_io_acc_power_low()
    /*! Used to signal when there is a transmission in progress through the transport used */
    #define IAP_INTERFACE_TX_IN_PROGRESS_FLAG       (iAP_Transport_Status() & (1 << USB_DEV_TX_IN_PROGRESS))

#endif

/*! Value of the Sync byte */
#define IAP_INTERFACE_SYNC_BYTE                     (0xFF)

/*! Value of the SOF1 byte */
#define IAP_INTERFACE_SOF1_BYTE                     (0xFF)

/*! Value of the SOF2 byte */
#define IAP_INTERFACE_SOF2_BYTE                     (0x55)

/*! Number of times every command will be retried if it is waiting for response */
#define IAPI_MAX_RETRY_INTENTS                      (3)

/*! If the commands has this value in its retry counter it means it is a periodical command so its
 *  retry counter is not decremented */
#define IAPI_INDEFINITE_RETRY_INTENTS               (0xFF)


/*!
 * @brief Definition used to determine the amount of bytes received in an iAP1 frame 
 * @details 10 bytes for Rx frame structure: length(2bytes), lingo(2bytes), command(2bytes), TID(2bytes), 
 *          PayloadSize(2 bytes)
 */
#define IAPI_FRAME_RECEIVED_BYTES                   (10)
/*!
 * @brief Definition used to determine the amount of bytes that will be part of an an iAP1 standard Tx frame
 * @details Frame contains SOF1(1 byte), SOF2(1 byte), length(up to 3 bytes), lingo(1 byte), CMD(up to 2 bytes), 
 *          TID(2 bytes) and checksum (1byte) 
 */ 
#define IAPI_FRAME_TRANSMIT_BYTES                   (11)

/*! Size of Tx Packet */
#define IAPI_TX_PACKET_SIZE                     BUFFER_1_MSG_SIZE

/*! Size of Rx Packet */
#define IAPI_RX_PACKET_SIZE                     BUFFER_0_MSG_SIZE

/*! Maximum Tx Payload size */
#define IAPI_MAX_TX_PAYLOAD_SIZE                (IAPI_TX_PACKET_SIZE - IAPI_FRAME_TRANSMIT_BYTES)

/*! Maximum Rx Payload size */
#define IAPI_MAX_RX_PAYLOAD_SIZE                (IAPI_RX_PACKET_SIZE - IAPI_FRAME_RECEIVED_BYTES)

/*! Number of packets that can be stored in the Rx and Tx queues*/
#define NUM_OF_RX_PACKETS                       BUFFER_0_SUBBUFFERS_AMOUNT
#define NUM_OF_TX_PACKETS                       BUFFER_1_SUBBUFFERS_AMOUNT

/*! Default value for the max payload accepted by the Apple device */
#define kiAPIDefaultMaxPayloadSizeSpec          (506)

/*! Macro to _TRUE_ the TID */
#define IAPI_ENABLE_TRANSACTION_ID              g_iapInterfaceEnableTransactionId = _TRUE_

/*! Macro to disbale the TID */
#define IAPI_DISABLE_TRANSACTION_ID             g_iapInterfaceEnableTransactionId = _FALSE_

/*! Macro to check if the TID is _TRUE_d */
#define IAPI_CHECK_TRANSACTION_ID_ENABLED       _TRUE_ == g_iapInterfaceEnableTransactionId

/*! Macro for the msg_queue_get function timeout */
#define MSG_QUEUE_TIMEOUT                       (0x0000)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
/*! Tx and Rx buffers indexes */
typedef enum {
    /*! Rx buffer will be buffer 0 */
    kIapiReceptionBufferIndex = 0,
    /*! Tx buffer will be buffer 1 */
    kIapiTransmissionBufferIndex = 1
}iap_interface_buffers_index_t;

/*! Constants to signal the state of a current command */
typedef enum
{
    /*! The command is waiting for response so it will NOT be inserted in the
     *  retry commands array */
    kCommandIsNotWaitingForResponse = 1,
    /*! The command is not waiting for response so it will be inserted in the
     *  retry commands array because */
    kCommandIsWaitingForResponse = 2,
    /*! The command needs to be repeated periodically */
    kCommandRepeatsPeriodically = 4,
    /*! The command is waiting for any known amount of packets to receive*/
    kCommandWithMultipleResponses = 8,
    /*! The command is waiting for an amount of packets previously unknown, the amount of packets
     *  will be calculated or extracted depending of each specific command */
    kCommandWithMultisectionResponse = 16,
    /*! The command is a retry command so it has already been inserted in the
     *  retry commands array because it is a retry command */
    kCommandRetry = 64
}command_tx_state_t;

/*! Offset bytes */
typedef enum
{
    kOneByteOffset = 1,
    kTwoBytesOffset = 2,
    kThreeBytesOffset = 3,
    kFourBytesOffset = 4,
    kFiveBytesOffset = 5
}packet_offset_t;

/*! Command timeout status */
typedef enum {
    /*! The command has not passed yet */
    kCommandNoTimeout = 0,
    /*! The command needs to be retried because its timeout has passed */
    kCommandTimeout
}iap_interface_command_timeout_t;

/*! Interface current receiving packet status type */
typedef uint8_t iap_interface_rx_packet_status_t;

/*! Interface receiving packet status bits */
typedef enum
{    
    kIapiReceivedByte = 0,
    kIapiLengthError,
    kIapiChecksumError,
    kIapiPayloadOverflowError,
    kIapiFullFrame
}iap_interface_rx_packet_status_bits_t;

/*! Interface current transmission packet status type */
typedef uint8_t iap_interface_tx_packet_status_t;

/*! Interface transmission packet status bits */
typedef enum
{   
    kIapiNoPacketTransmission = 0,
    kIapiPacketTransmitted = 1,
    kIapiSyncByteTranssmission = 2,
    kIapiTransmissionInProgress = 4
}iap_interface_tx_packet_status_bits_t;

/*! Number of bytes of the received command */
typedef enum
{    
    /*! Invalid number of bytes received */
    kUnknowByteCommand,
    /*! The received command is a one byte command */
    kOneByteCommand,
    /*! The received command is a two bytes command */
    kTwoByteCommand
}iap_interface_bytes_of_command_t;

/*! Send frame function error types */
typedef enum{
    /*! The command was queued successfully and will be sent */
    kInterfaceTxSuccess,
    /*! The payload of the command that wants to be transmitted 
     * exceeds the maximun permitted payload size */
    kPayloadOverflowError,
    /*! The Tx buffer is full */
    kMemoryNotAvailable,
    /*! The Interface is busy */
    kInterfaceBusy
}iap_interface_send_frame_t;

/*! Constant for the commands timeouts */
typedef enum
{
    /*! No timeout value */
    kNoTimeout = 0,
    /*! 50ms timeout value */
    k50msTimeout  = 50,
    /*! 75ms timeout value */
    k75msTimeout  = 75,
    /*! 100ms timeout value */
    k100msTimeout  = 100,
    /*! 200ms timeout value */
    k200msTimeout  = 200,
    /*! 300ms timeout value */
    k300msTimeout  = 300,
    /*! 400ms timeout value */
    k400msTimeout  = 400,
    /*! 500ms timeout value */
    k500msTimeout  = 500,
    /*! 1000ms timeout value */
    k1000msTimeout = 1000,
    /*! 5000ms timeout value */
    k5000msTimeout = 5000,
    /*! 30s timeout value */
    k30sTimeout = 30000
}command_typical_timeouts_t;

/*! Type of packet to be send either a question or an answer from the accessory 
 * to the Apple device */
typedef enum
{
    /*! The packet to be sent is a question */
    kQuestionPacket = 1,
    /*! The packet to be sent is an answer */
    kAnswerPacket = 2,
    /*! The packet to be sent must keep using the last TID used*/
    kDoNotIncrementTID = 4
}iap_interface_type_of_packet_t;

/*! Status to inform to commands layer if the command was successfully release */
typedef enum
{
    /*! The command was successfully release */
    kReleaseSuccess = 0,
    /*! The retry commands array index that wants to be released is out of range */
    kReleaseIndexOutOfRange = 1,
    /*! There was a error releasing memory from the Tx Buffer */
    kReleaseTxError = 2,
    /*! There was a error releasing an element from the retry commands array */
    kReleaseRetryCommandsArrayError = 4
}iap_interface_release_status_t;

/*! Type that contains the fields that will be saved from an iAP1 received frame */
typedef struct
{
    /*! The packet length */
    uint16_t frameLength;
    /*! The Lingo of the command */
    uint16_t lingoId;
    /*! The number of the command */
    uint16_t commandId;
    /*! The Transaction ID of the command */
    uint16_t transactionId;
    /*! The length of the payload */
    uint16_t payloadSize;
    /*! An array that contains the payload of the packet*/
    uint8_t  payload[IAPI_MAX_RX_PAYLOAD_SIZE];
}iap_interface_buffer_rx_frame_t;

/*! Type that contains the fields that will be used to create the iAP1 Tx frame */
typedef struct
{
    /*! Timeout of the command that will be sent */
    uint32_t commandTimeout;
    /*! Pointer to the paylod that will be sent */
    uint8_t* payload;
    /*! Size of payload that will be sent */
    uint16_t payloadSize;
    /*! Value of the TID in case of the transmitted commands
     *  is an answer to a previous question sent by the Apple device */
    uint16_t transactionIDCopied;
    /*! Indicates if the command to be sent is a question or answer */
    uint8_t  questionOrAnswerPacket;
    /*! Lingo of the command to be transmitted */
    uint8_t lingoId;
    /*! Command value of the command to be transmitted */
    uint8_t  commandId;
    /*!  Parameter to indicate whether the command waits for response */
    uint8_t  commandCurrentTxState;
}iap_interface_tx_frame_data_t;

typedef struct
{
    uint16_t packetStatus;
    uint16_t protocolStatus;
}iap_interface_status_t;

/* commandCurrentTxState and retryArrayIndexId declared as uint16_t cause msg_queue API requires a size in double word */
/*! Structure used to transmit a frame using iAP1 interface */
typedef struct
{
    /*! Pointer to the start of the packet */
    uint8_t* packetToTransmit;
    /*! Size of the whole packet to be transmitted */
    uint32_t packetSize;
    /*! Field used to know if the command either waits for response or not, if it needs to be 
     * repeated periodically or is being retried (See command_tx_state_t in iAP_Interface.h) */
    uint16_t commandCurrentTxState;     
    /*! Index of the retry commands array used each time a packet is transmitted to write to the
     *  timeStamp and decrement the retryCounter of one of the elements of the retry commands array */
    uint16_t retryArrayIndexId;
}iap_interface_data_transmit_packet_t;

/*! Structure used to store the information of a packet that has to be to be resent
 *  after a timeout expires */
typedef struct
{
    /*! Pointer to the start of the packet */
    uint8_t* packetToTransmit;
    /*! Size of the whole packet to be transmitted */
    uint32_t packetSize;
    /*! Timestamp of the command */
    uint32_t timeStamp;
    /*! Timeout of the command in ms */
    uint32_t timeout;
    /*! Number of times a command will be retried */
    uint8_t  retryCounter;    
}iap_interface_data_retry_transmit_packet_t;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/
#define IAP_INTERFACE_SEND_MESSAGE(pointer,size)    iAP_Transport_SendData(pointer,size)

#define IAP_INTERFACE_SEND_SYNC_BYTE                IAPI_SET_TX_SYNC_BYTE_FLAG;\
                                                    IAP_INTERFACE_SEND_MESSAGE((uint8_t*)(&g_iAPInterfaceSyncByte),\
                                                                               (uint16_t)0x01)

#if defined(IAPI_INTERFACE_USB_HOST)
    #define IAP_INTERFACE_CALLBACK                  iAP_USBHostTask()
    #define iap_interface_callback                  iAP_USBHostTask()
#else
    #define IAP_INTERFACE_CALLBACK
    #define iap_interface_callback
#endif
/*! Used to verify if the flag to signal when the interface finished sending data is set, 
 * independently of the transport used */
#define IAPI_CHECK_TX_COMPLETE_FLAG             (g_iapInterfaceTxStatus & (1 << kIapiPacketTransmitted))
/*! Set the flag to signal that the last transmission has finished */
#define IAPI_SET_TX_COMPLETE_FLAG               (g_iapInterfaceTxStatus |= (1 << kIapiPacketTransmitted))
/*! Clear the flag to signal that the last transmission has finished */
#define IAPI_CLEAR_TX_COMPLETE_FLAG             (g_iapInterfaceTxStatus &= ~(1 << kIapiPacketTransmitted))
/*! Used to verify if the flag to signal when the interface finished sending its sync byte data is set, 
 * independently of the transport used */
#define IAPI_CHECK_TX_SYNC_BYTE_FLAG            (g_iapInterfaceTxStatus & (1 << kIapiSyncByteTranssmission))
/*! Set the flag to signal that the last sync byte transmission has finished */
#define IAPI_SET_TX_SYNC_BYTE_FLAG              (g_iapInterfaceTxStatus |= (1 << kIapiSyncByteTranssmission))
/*! Clear the flag to signal that the last sync byte transmission has finished */
#define IAPI_CLEAR_TX_SYNC_BYTE_FLAG            (g_iapInterfaceTxStatus &= ~(1 << kIapiSyncByteTranssmission))

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
/*! Variable used in the interface to enable/disable the usage of transaction IDs. It's external to other
 * modules (i.e. commands) so they can determine if the usage of transaction ID is needed*/
extern volatile bool g_iapInterfaceEnableTransactionId;

extern const uint8_t g_iAPInterfaceSyncByte;

extern volatile iap_interface_tx_packet_status_t g_iapInterfaceTxStatus;

extern uint16_t g_txPayloadBufferSize;

    extern volatile bool iAPI_RxCallbackFlag;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
/*************************************************************************************************
 *
 * @brief    Function to initialize the interface, set the message queue handlers to their initial
 *              state and configure variables for start communicating through iAP1. 
 * @return   void 
 * 
**************************************************************************************************/
void iap_interface_initialization(void);


/*************************************************************************************************
 *
 * @brief    Function that builds, queues and sends a packet .
 * 
 * @brief  Function that builds, queues and sends a packet .
 * @param  txFrameData Pointer to structure that contains all the needed data to create a packet to 
 * @sa     iap_interface_tx_frame_data_t
 * @param  txIndex Pointer to the variable that will store the index to identify the Tx command
 * @return sendFrameTxStatus Whether the iAP1_interface Layer was able to send the command
 * @retval kInterfaceTxSuccess The command was queued successfully and will be sent 
 * @retval kPayloadOverflowError The given payload exceeds the maximum permitted value
 * @retval kMemoryNotAvailable The Tx buffer is full
 * 
 **************************************************************************************************/
iap_interface_send_frame_t iap_interface_send_frame(iap_interface_tx_frame_data_t* txFrameData, uint8_t* txIndex);


/**************************************************************************************************
 *
 * @brief   Public function used by other layers to get the the last Rx command.
 * @return   Pointer to the last queued packet in the Rx queue
 *                                              
 * @retval     iap_interface_buffer_rx_frame_t* A valid pointerto the last received command
 * @retval  NULL An invalid pointer meaning there a aren't any received commands to process
 * 
 **************************************************************************************************/
iap_interface_buffer_rx_frame_t* iap_interface_get_rx_command(void);

/**************************************************************************************************
 *
 * @brief   Function to get the next command in the queue to be transmitted
 * @param   uint8_t index location in the array where the command is located
 * @return  uint8_t* address where the next command to be sent is stored
 *
 **************************************************************************************************/
uint8_t* iap_interface_get_tx_command_from_index(uint8_t index);

/**************************************************************************************************
 *
 * @brief   Function that it is called to find the position of the TID  in a packet in the Tx data 
 *          buffer
 * @param   outDataBuffer - Pointer to the data packet 
 * @return  TID position in the Tx data packet
 *
 **************************************************************************************************/
uint8_t iap_interface_get_tid_position(uint8_t* outDataBuffer);

/**************************************************************************************************
 *
 * @brief   Function that it is called each time an expected answer is received, it releases the
 *          Tx packet from the Tx buffer and the element from the retry commands array
 * @param   interfaceRetryIndex The index of the retry commands array, using this index is possible
 *                             to release the transmitted packet 
 * @return  Whether the packet were released successfully 
 * @retval  Check "iap_interface_release_status_t" type in iAP_Interface.h
 *
 **************************************************************************************************/
iap_interface_release_status_t iap_interface_release_tx_command(uint8_t interfaceRetryIndex);

/**************************************************************************************************
 *
 * @brief   Function that shall be called periodically to ensure that the packets and retries are 
 *             being properly managed by the interface
 * @return  void
 *
 **************************************************************************************************/
void iap_interface_periodical_task(void);

/**************************************************************************************************
 *
 * @brief   Function that request a maximum Tx payload size change.
 * @param   requestedPayloadSize Requested value for the payload size
 * @return  void
 * @details If the payload requested is smaller than the IAPI_MAX_TX_PAYLOAD_SIZE, requestedPayloadSize 
 *          will be set as the new maximum payload size. Otherwise IAPI_MAX_TX_PAYLOAD_SIZE will be
 *          kept as the maximum Tx payload size (this is defined by the buffers size)
 *
 **************************************************************************************************/
void iap_interface_request_max_payload_change (uint16_t requestedPayloadSize);

/**************************************************************************************************
 *
 * @brief   Function that flushes the queues to return the interface
 *          to its initial state
 * @return  void
 *
 **************************************************************************************************/
void iap_interface_reset(void);

/**************************************************************************************************
 *
 * @brief   Function to reset the transaction ID 
 * @return  void
 *
 **************************************************************************************************/
void iap_interface_reset_transaction_id(void);

/**************************************************************************************************
 *
 * @brief   Function to update the payload of a command that is currently being trasmitted
 * @param   newPayload Pointer to the buffer or variable that stores the new payload
 * @param   newPayloadSize Size in bytes of the buffer or variable that stores the new payload
 * @param   retryCommandArrayIndex Index of the retry commands array 
 * @return  void
 *
 **************************************************************************************************/
void iap_interface_payload_update(uint8_t* newPayload, uint8_t newPayloadSize, uint8_t retryCommandArrayIndex);

/**************************************************************************************************
 *
 * @brief   Function to update the timeout of a command
 * @param   indexOfRetryArray  The index of the retry commands array, using this index is possible
 *                             to update the timeout for the desired command
 * @param   newTimeout         The new timeout value, if 0 the timeout is not updated
 * @param   newTimeout The new timeout value to set 
 * @return  void
 *
 **************************************************************************************************/
void iap_interface_update_timeout(uint8_t indexOfRetryArray, uint32_t newTimeout);

/*************************************************************************************************
*
* @brief   Register a callback routine which is called if a command has been retried
*          'IAPI_MAX_RETRY_INTENTS' times and it hasn't received response
* @return  Whether the callback was registered successfully      
* @retval  OK The callback was registered successfully     
* @retval  ERROR there was an error while registering the callback     
**************************************************************************************************/
uint8_t iap_interface_register_no_response_error_callback(void(*callback)(uint8_t));

/*! @} 
* Group iAP_Interface
* @}  
*
* Group iAP1
*/
#endif /* IAP_INTERFACE_H_ */
