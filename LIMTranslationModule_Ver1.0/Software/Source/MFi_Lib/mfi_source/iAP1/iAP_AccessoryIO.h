/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_ACCESSORYIO_H_
#define IAP_ACCESSORYIO_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include <stdint.h>

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/*
#define IAP_ACCSSRYIO_IOSDETECT_LOW         (!iOSDetect1_GetFieldValue(iOSDetect1_DeviceData,DETECT))
#define IAP_ACCSSRYIO_IOSDETECT_HIGH        (iOSDetect1_GetFieldValue(iOSDetect1_DeviceData,DETECT))

#define IAP_ACCSSRYIO_ACCPWR_LOW        (!AccPwr1_GetFieldValue(AccPwr1_DeviceData,POWER))
#define IAP_ACCSSRYIO_ACCPWR_HIGH       (AccPwr1_GetFieldValue(AccPwr1_DeviceData,POWER))

#ifdef IAPI_INTERFACE_USB_HOST
#define ACCSSRYIO_SET_ACCDETECT
#define ACCSSRYIO_CLEAR_ACCDETECT
#else
#define ACCSSRYIO_SET_ACCDETECT      AccDetect1_SetPortBits(AccDetect1_DeviceData,AccDetect1_ACCDTCT_MASK)
#define ACCSSRYIO_CLEAR_ACCDETECT    AccDetect1_ClearPortBits(AccDetect1_DeviceData,AccDetect1_ACCDTCT_MASK)
#endif

#define ACCSSRYIO_SET_CPRTS         CPRts1_SetPortBits(CPRts1_DeviceData,CPRts1_ALLOCATED_PINS_MASK)
#define ACCSSRYIO_CLEAR_CPRTS       CPRts1_ClearPortBits(CPRts1_DeviceData,CPRts1_ALLOCATED_PINS_MASK)
*/

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern uint8_t gdwAccssryIoEvntFlgs; 

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/

uint32_t iap_accessory_io_ios_detect_low    (void);
uint32_t iap_accessory_io_ios_detect_high   (void);

uint32_t iap_accessory_io_acc_power_low       (void);
uint32_t iap_accessory_io_acc_power_high      (void);

void iap_accessory_io_set_acc_detect        (void);
void iap_accessory_io_clear_acc_detect      (void);

void iap_accessory_io_set_cprts             (void);
void iap_accessory_io_clar_cprts            (void);

/*************************************************************************************************/

#endif /* IOCTL_H_ */
