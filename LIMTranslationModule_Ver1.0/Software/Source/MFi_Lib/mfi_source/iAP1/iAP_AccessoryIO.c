/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/

#include "iAP_AccessoryIO.h"

#include "AccDetect1.h"
#include "iOSDetect1.h"
#include "AccPwr1.h"
#include "CPRts1.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

uint8_t gdwAccssryIoEvntFlgs; 

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
uint32_t iap_accessory_io_ios_detect_low    (void)
{
    uint32_t iosDetectLow;
    iosDetectLow = (uint32_t)(!iOSDetect1_GetFieldValue(iOSDetect1_DeviceData,DETECT));
    return (iosDetectLow);
}

uint32_t iap_accessory_io_ios_detect_high   (void)
{
    uint32_t iosDetectHigh;
    iosDetectHigh = (uint32_t)(iOSDetect1_GetFieldValue(iOSDetect1_DeviceData,DETECT));
    return (iosDetectHigh);
}

uint32_t iap_accessory_io_acc_power_low       (void)
{
    uint32_t accPowerLow;
    accPowerLow = (uint32_t)(!AccPwr1_GetFieldValue(AccPwr1_DeviceData,POWER));
    return (accPowerLow);
}

uint32_t iap_accessory_io_acc_power_high      (void)
{
    uint32_t accPowerHigh;
    accPowerHigh = (uint32_t)(AccPwr1_GetFieldValue(AccPwr1_DeviceData,POWER));
    return (accPowerHigh);
}

void iap_accessory_io_set_acc_detect        (void)
{
    AccDetect1_SetPortBits(AccDetect1_DeviceData,AccDetect1_ACCDTCT_MASK);
}

void iap_accessory_io_clear_acc_detect      (void)
{
    AccDetect1_ClearPortBits(AccDetect1_DeviceData,AccDetect1_ACCDTCT_MASK);
}

void iap_accessory_io_set_cprts             (void)
{
    CPRts1_SetPortBits(CPRts1_DeviceData,CPRts1_ALLOCATED_PINS_MASK);
}
void iap_accessory_io_clar_cprts            (void)
{
    CPRts1_ClearPortBits(CPRts1_DeviceData,CPRts1_ALLOCATED_PINS_MASK);
}
