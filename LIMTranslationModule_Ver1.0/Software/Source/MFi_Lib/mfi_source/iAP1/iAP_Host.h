/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_HOST_H_
#define IAP_HOST_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include <stdint.h>
#include "iAP_Commands.h"
#include "iAP_Blocking_Authentication.h"
#include "iAP_AccessoryIO.h"

/*! @addtogroup iAP1 iAP1 Software Stack
* @{
* 
* @addtogroup iAP_Host iAP1 Host Software Layer
* @brief    This layer handles the Apple device connection,the iOS authentication (if enabled) 
*           the accessory identification and authentication. 
*           
* @details  The core of the layer is an state machine that communicates with the coprocessor,  
*           trough the authentication layer, and sends the appropriate commands to the Apple decide 
*           in order to perform the identification and authentication of the accessory. 
* @{
 */

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
#define IAP_HOST_CHECK_STATUS_REGISTER(bit)				(iAP_Host_gwStatus & (1 << bit))
#define IAP_HOST_CLEAR_STATUS_REGISTER(bit)				iAP_Host_gwStatus &= ~(1 << bit)

#define IAP_HOST_CHECK_INTERNAL_ERRORS_REGISTER(bit)	(iAP_Host_gwInternalErrorFlags & (1 << bit))
#define IAP_HOST_CLEAR_INTERNAL_ERRORS_REGISTER(bit)	iAP_Host_gwInternalErrorFlags &= ~(1 << bit)

#define IAP_HOST_CHECK_APPLICATION_ERRORS_REGISTER(bit)	(iAP_Host_gwUserAppErrorFlags & (1 << bit))
#define IAP_HOST_CLEAR_APPLICATION_ERRORS_REGISTER(bit)	iAP_Host_gwUserAppErrorFlags &= ~(1 << bit)

#define IAP_HOST_READY_TO_EXECUTE_COMMAND               (IAP_HOST_CHECK_STATUS_REGISTER(IAPH_APP_CMD_ALLOWED))
#define IAP_HOST_ACCESSORY_AUTHENTICATION_DONE          (IAP_HOST_CHECK_STATUS_REGISTER(IAPH_ACC_AUTHENTICATION_DONE))

#define IAPH_CHECK_TRACK_ARTWORK_DATA_FLAG	(iAP_Cmd_gdwStatus & (1 << IAP_CMD_ASYNC_TRACK_ARTWORK_DATA))
#define IAPH_CLEAR_TRACK_ARTWORK_DATA_FLAG	(iAP_Cmd_gdwStatus &= ~(1 << IAP_CMD_ASYNC_TRACK_ARTWORK_DATA))
#define IAPH_CHECK_REMOTE_EVENT_FLAG		(iAP_Cmd_gdwStatus & (1 << IAP_CMD_ASYNC_REM_EVENT_NOT))
#define IAPH_CLEAR_REMOTE_EVENT_FLAG		(iAP_Cmd_gdwStatus &= ~(1 << IAP_CMD_ASYNC_REM_EVENT_NOT))
#define IAPH_CHECK_NOTIFY_IPOD_STATE_FLAG	(iAP_Cmd_gdwStatus & (1 << IAP_CMD_ASYNC_NOT_IPOD_STATE))
#define IAPH_CLEAR_NOTIFY_IPOD_STATE_FLAG	(iAP_Cmd_gdwStatus &= ~(1 << IAP_CMD_ASYNC_NOT_IPOD_STATE))
#define IAPH_CHECK_ACC_STATUS_CHANGED_FLAG	(iAP_Cmd_gdwStatus & (1 << IAP_CMD_ASYNC_ACC_STATUS_NOT))
#define IAPH_CLEAR_ACC_STATUS_CHANGED_FLAG	(iAP_Cmd_gdwStatus &= ~(1 << IAP_CMD_ASYNC_ACC_STATUS_NOT))
#define IAPH_CHECK_IPOD_NOTIFICATION_FLAG	(iAP_Cmd_gdwStatus & (1 << IAP_CMD_ASYNC_IPOD_NOTIFICATION))
#define IAPH_CLEAR_IPOD_NOTIFICATION_FLAG	(iAP_Cmd_gdwStatus &= ~(1 << IAP_CMD_ASYNC_IPOD_NOTIFICATION))
#define IAPH_CHECK_VOICE_OVER_CHANGED_FLAG	(iAP_Cmd_gdwStatus & (1 << IAP_CMD_ASYNC_VOICE_OVER_CHANGED))
#define IAPH_CLEAR_VOICE_OVER_CHANGED_FLAG	(iAP_Cmd_gdwStatus &= ~(1 << IAP_CMD_ASYNC_VOICE_OVER_CHANGED))
#define IAPH_CHECK_VOICE_OVER_ITEM_FLAG		(iAP_Cmd_gdwStatus & (1 << IAP_CMD_ASYNC_VOICE_OVER_ITEM))
#define IAPH_CLEAR_VOICE_OVER_ITEM_FLAG		(iAP_Cmd_gdwStatus &= ~(1 << IAP_CMD_ASYNC_VOICE_OVER_ITEM))
#define IAPH_CHECK_NEW_SESSION_OPEN_FLAG    (iAP_Cmd_gdwStatus & (1<<IAP_CMD_ASYNC_NEW_SESSION_OPEN))
#define IAPH_CLEAR_NEW_SESSION_OPEN_FLAG    (iAP_Cmd_gdwStatus &= ~(1<<IAP_CMD_ASYNC_NEW_SESSION_OPEN))
#define IAPH_CHECK_IPOD_DATA_RECEIVED_FLAG  (iAP_Cmd_gdwStatus & (1<<IAP_CMD_ASYNC_IPOD_DATA_RECEIVED))
#define IAPH_CLEAR_IPOD_DATA_RECEIVED_FLAG  (iAP_Cmd_gdwStatus &= ~(1<<IAP_CMD_ASYNC_IPOD_DATA_RECEIVED))
#define IAPH_CHECK_WIFI_CONNECT_INFO_FLAG   (iAP_Cmd_gdwStatus & (1<<IAP_CMD_ASYNC_WIFI_INFO_DATA))
#define IAPH_CLEAR_WIFI_CONNECT_INFO_FLAG   (iAP_Cmd_gdwStatus &= ~(1<<IAP_CMD_ASYNC_WIFI_INFO_DATA))
#define IAPH_CHECK_AUDIO_ATTRIBUTES_FLAG    (iAP_Cmd_gdwStatus & (1<<IAP_CMD_ASYNC_NEW_AUDIO_ATTRIBUTES))
#define IAPH_CLEAR_AUDIO_ATTRIBUTES_FLAG    (iAP_Cmd_gdwStatus &= ~(1<<IAP_CMD_ASYNC_NEW_AUDIO_ATTRIBUTES))

#define IAPH_MAX_ACC_AUTH_RETRIES           (5)
/* Don't change. Defined by Firmware Specification */
#define IAPH_MAX_IOS_AUTH_RETRIES                       (2)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
typedef enum
{
    kHostCoprocessorStabilizationState,
    kHostReadProtVerDevIdState,
    kHostWaitCoprocessorState,
    kHostWaitCoprocessorErrorState,
    kHostIdleState,
    kHostHotPlugDebouncesState,
    kHostSendSyncByteState,
    kHostStartIdpsState,
    kHostStartIdentifyDeviceLingoesState,
    kHostContinueIdentifyDeviceLingoesState,
    kHostIdentifyDeviceLingoesDelayState,
    kHostRequestMaxPaylodState,
    kHostGetIpodOptionsForLingoState,
    kHostSetFidTokenValues,
    kHostEndIdpsState,
    kHostAccessoryIdpsAuthenticationErrorState,
    kHostReadAccCertificateState,
    kHostSendAccCertificateState,
    kHostReadAccSignatureState,
    kHostSendAccSignatureState,
#if(IAP_IOS_AUTHENTICATION_ENABLE)
    kHostGetiPodAuthenticationInfo,
    kHostValidateiOSCertificateState,
    kHostSendAckiPodAuthenticationInfo,
    kHostCreateiOSChallengeState,
    kHostSendiOSChallengeState,
    kHostCheckiOSSignatureState,
    kHostAckiOSSignatureState,
    kHostiOSAuthenticationErrorState,
#endif
    kHostEssentialRoutinesDone,
    kHostReadyState,
    kHostWaitState
}hostStates_t;

typedef enum
{
    kHostIosDetected,
    kHostIosInLowPowerMode,
    kHostIosInHibernateMode,
    kHostIdpsNotSupported,
    kHostIdpsDone,
    kHostAccessoryAuthenticationDone,
    kHostiOSAuthenticationDone,
    kHostInternalError
}hostStatus_t;

typedef enum
{
    kHostUartHalError,
    kHostI2CHalError,
    kHostIncompleteFrameError,
    kHostIosDetectionError,
    kHostIdentifyDeviceLingoesError,
    kHostGetIpodOptionsForLingoError,
    kHostRequiredFidTokensError,
    kHostOptionalFidTokensError,
    kHostAccessoryAuthenticationError,
    kHostIosAuthenticationError,
    kHostTransactionIdError,
    kHostPacketLengthError,
    kHostPayloadOverflowError,
    kHostChecksumError
}hostInternalErrors_t;

typedef enum
{
    kHostInvalidLingo,                  /* This bit must match with _iAP_CommandStatusFlags enumeration */
    kHostInvalidCommand,                /* This bit must match with _iAP_CommandStatusFlags enumeration */
    kHostPayloadSizeError,              /* This bit must match with _iAP_CommandStatusFlags enumeration */
    kHostCommandDeprecated,             /* This bit must match with _iAP_CommandStatusFlags enumeration */
    kHostIosNotSupportCommand,          /* This bit must match with _iAP_CommandStatusFlags enumeration */
    kHostAccessoryNotSupportCommand,    /* This bit must match with _iAP_CommandStatusFlags enumeration */
    kHostCommandSequenceError,          /* This bit must match with _iAP_CommandStatusFlags enumeration */
    kHostCommandBusy,                   /* This bit must match with _iAP_CommandStatusFlags enumeration */
    kHostCommandAbortedByHost,
    kHostCommandRequiredAuthentication,
    kHostCommandAckErrorReceived,
    kHostCommandAckTimeout,
    kHostMaxRetriesError
}hostApplicationErrors;



/* Values of the identification processes */
typedef enum
{
    kHostIdpsProcessDevice,
    kHostNonIdpsProcessDevice
}iap_host_process_detected_t;

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/
#define IAP_HOST_SEND_SYNC_BYTE                 IAP_INTERFACE_SEND_SYNC_BYTE

#define IAP_HOST_CANCEL_CP_COMMAND              iAP_Auth_vfnCancelCommand
#define iAP_host_read_coprocessor_ver_and_ID    iAP_Auth_vfnReadProtVerAndDevID
#define IAP_HOST_READ_CP_PROT_VER_DEV_ID        iAP_Auth_vfnReadProtVerAndDevID
#define IAP_HOST_READ_ACCESSORY_CERTIFICATE     iAP_Auth_vfnReadAccessoryCertificate
#define IAP_HOST_CREATE_SIGNATURE               iAP_Auth_vfnAccessoryChallenge
#define IAP_HOST_VALIDATE_IOS_CERTIFICATE       iAP_Auth_vfnAppleCertificateVal
#define IAP_HOST_CREATE_CHALLENGE               iAP_Auth_vfnAppleChallengeGen
#define IAP_HOST_VALIDATE_IOS_SIGNATURE         iAP_Auth_vfnAppleSignatureVal
#define IAP_HOST_COPROCESSOR_BUSY               IAP_AUTH_CP_BUSY
#define IAP_HOST_COPROCESSOR_ERROR              IAP_AUTH_CP_ERROR
#define IAP_HOST_INTERFACE_BUSY                 IAP_INTERFACE_TX_IN_PROGRESS_FLAG

#if (MFI_TRANSPORT_USB_DEVICE_MODE == _TRUE_)
#define IAP_HOST_IDEVICE_DISCONNECTED           IAPI_IOSDETECT_HIGH
#else
#define IAP_HOST_IDEVICE_DISCONNECTED           (IAPI_IOSDETECT_HIGH && IAP_HOST_IDEVICE_NOPOWER)
#endif

#if (MFI_TRANSPORT_USB_DEVICE_MODE == _TRUE_)
    #define IAP_HOST_IDEVICE_CONNECTED          IAPI_IOSDETECT_LOW
#else
    #define IAP_HOST_IDEVICE_CONNECTED          (IAPI_IOSDETECT_LOW || IAP_HOST_IDEVICE_POWER)
#endif

#if (MFI_TRANSPORT_USB_DEVICE_MODE == _TRUE_)
#define IAP_HOST_IDEVICE_POWER                  1
#define IAP_HOST_IDEVICE_NOPOWER                0
#else
#define IAP_HOST_IDEVICE_POWER                  IAPI_ACCPWR_HIGH
#define IAP_HOST_IDEVICE_NOPOWER                IAPI_ACCPWR_LOW
#endif

#if (MFI_TRANSPORT_USB_DEVICE_MODE == _TRUE_)
    #define IAP_HOST_SET_ACC_DETECT_PIN
    #define IAP_HOST_CLEAR_ACC_DETECT_PIN
#else 
    #define IAP_HOST_SET_ACC_DETECT_PIN             iap_accessory_io_set_acc_detect()
    #define IAP_HOST_CLEAR_ACC_DETECT_PIN           iap_accessory_io_clear_acc_detect()
#endif


/* Extended Interface */
#define IAP_HOST_SET_EXTENDED_INTERFACE         iap_host_request_extended_interface_mode
/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/


/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
extern uint16_t g_iapHostStatus;
extern uint16_t g_iapHostInternalErrorFlags;
extern uint16_t g_iapHostUserAppErrorFlags;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/
/** ***********************************************************************************************
*
* @brief    Function to initialize the iAP Host module. This routine takes care of initializing the interface
*           and the commands layers. There is no need to call other initialization routine.
* @return   void
*          
**************************************************************************************************/
void iap_host_initialization(void);

/** ***********************************************************************************************
*
* @brief    Function to uninitialize the iAP Host module. This routine takes care of uninitializing 
*           the interface and the commands layers. 
* @return   void
*          
**************************************************************************************************/
void iap_host_uninitialization(void);

/*************************************************************************************************
*
* @brief    Main state machine used to dispatch the functions for the Host and the Authentication modules.
* @return   void
* @details  This function is the one that determines the sequence of commands during IDPS,
*           Identify device lingoes and controls the stack state during the different power 
*           events and asynchronous commands.           
**************************************************************************************************/
void iap_host_state_machine_driver(void);

/*************************************************************************************************
*
* @brief    Register a callback routine which is called after an Apple device is connected and 
*           successfully passes through IDPS/IDL and Authentication
* @param    Callback function that will be used as callback 
* @return   uint8_t Returns OK if the function pointer is different to NULL       
**************************************************************************************************/
uint8_t iap_host_register_device_ready_callback(void(*Callback)(void)); 

/*************************************************************************************************
*
* @brief    Register a callback routine which is called after an Apple device is disconnected 
* @param    Callback function that will be used as callback 
* @return   uint8_t Returns OK if the function pointer is different to NULL       
**************************************************************************************************/
uint8_t iap_host_register_device_disconnection_callback(void(*Callback)(void)); 

/*************************************************************************************************
*
* @brief    Register a callback routine which is called after an Apple device sends a
*             new sampling frequency
* @param    Callback function that will be used as callback 
* @return   uint8_t Returns OK if the function pointer is different to NULL       
**************************************************************************************************/
uint8_t iap_host_register_new_frequency_callback(void(*callback)(uint16_t));
/*************************************************************************************************/

/*************************************************************************************************
*
* @brief Function to detect when an iOS device has been disconnected and then resets the status
*        flags and sets the state machine to a wait for a new connection state  
* @return   void 
* @sa      iAP_Host_vfnReadACCSignature_State
*
**************************************************************************************************/
void iap_host_handle_ios_disconnection(void);


void iAP_Host_bfnEventsCallback(uint8_t eventFlag);


/*************************************************************************************************
*
* @brief    Function to know the identification process of current device
* @return   uint8_t value of the identification process 
* @retval   kHostIdpsProcessDevice          if the process detected is an IDPS
* @retval   kHostNonIdpsProcessDevice       if the process detected is an Identify Device Lingo
*
**************************************************************************************************/
uint8_t iap_host_request_identification_procces_type(void);

/*! @} 
* Group iAP_Host
* @}  
*
* Group iAP1
*/

#endif /* IAP_HOST_H_ */
