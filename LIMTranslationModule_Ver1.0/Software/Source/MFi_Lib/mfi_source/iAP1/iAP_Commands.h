/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP_COMMANDS_H_
#define IAP_COMMANDS_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/

#include "iAP_Interface.h"
#include "mfi_cfg.h"
//#include "USB_Audio_Interface.h"

/*! @addtogroup iAP1 iAP1 Software Stack
* @{
* 
* @addtogroup iAP_Commands iAP1 Commands Software Layer
* @brief    This layer process the received commands and manages the commands that are waiting for 
*           response.
*           
* @details  This layer is constantly checking if there are commands that need to be processed and 
*           if so, it processes them and decides whether the received command is an expected 
*           answer, an ack or an asynchronous command to act accordingly to each situation.
* @{
 */

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/
/*! Number of lingoes specified by the protocol */
#define IAP_LINGOES_COUNT                           0x0F

#ifndef MFI_OVERRIDE_DEFAULT_CONFIGURATION
/*! Set the FID Tokens that will be used during IDPS process */
#define IDENTIFY_TOKEN_MASK                         (_TRUE_)
#define ACC_CAPABILITY_TOKEN_MASK                   (_TRUE_)
#define ACC_INFO_TOKEN_MASK                         (_TRUE_)
#define ACC_INFO_NAME_TOKEN_MASK                    (_TRUE_)
#define ACC_INFO_FIRMWARE_VERSION_TOKEN_MASK        (_TRUE_)
#define ACC_INFO_HARDWARE_VERSION_TOKEN_MASK        (_TRUE_)
#define ACC_INFO_MANUFACTURER_TOKEN_MASK            (_TRUE_)
#define ACC_INFO_MODEL_NUMBER_TOKEN_MASK            (_TRUE_)
#define ACC_INFO_SERIAL_NUMBER_TOKEN_MASK           (_TRUE_)
#define ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_MASK        (_TRUE_)
#define ACC_INFO_STATUS_TOKEN_MASK                  (_FALSE_)
#define ACC_INFO_RF_CERTIFICATION_TOKEN_MASK        (_TRUE_)
#define IPOD_PREFERENCE_TOKEN_MASK                  (_TRUE_)
#define IPOD_PREF_VIDEO_OUT_SETTINGS_MASK           (_FALSE_)
#define IPOD_PREF_SCREEN_CONFIGURATION_MASK         (_FALSE_)
#define IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK          (_FALSE_)
#define IPOD_PREF_LINE_OUT_USAGE_MASK               (_FALSE_)
#define IPOD_PREF_VIDEO_OUT_CONNECTION_MASK         (_FALSE_)
#define IPOD_PREF_CLOSED_CAPTIONING_MASK            (_FALSE_)
#define IPOD_PREF_VIDEO_ASPECT_RATIO_MASK           (_FALSE_)
#define IPOD_PREF_SUBTITLES_MASK                    (_FALSE_)
#define IPOD_PREF_VIDEO_ALTERNATE_AUDIO_MASK        (_FALSE_)
#define IPOD_PREF_PAUSE_ON_POWER_REMOVAL_MASK       (_TRUE_)
#define IPOD_PREF_VOICE_OVER_MASK                   (_TRUE_)
#define IPOD_PREF_ASSISTIVE_TOUCH_MASK              (_FALSE_)
#define ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_MASK    (_TRUE_)            /*! @todo Check GetIpodOptionsForLingo-General bit 27,28. Audio Lingo must be supported */
#define ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK    (_FALSE_)           /*! @todo Check GetIpodOptionsForLingo-General bit 26. Audio Lingo must be supported */

/*! Accessory Capability Token Configuration. See (Lingo = 0x00, Cmd = 0x28 and Cmd = 0x39) */
#define ANALOG_LINE_IN_MASK                         (_FALSE_)
#define USB_AUDIO_OUT_MASK                          (_TRUE_)
#define COMM_WITH_IOS_APPLICATIONS_MASK             (_TRUE_)
#define CHECK_IOS_VOLUME_MASK                       (_FALSE_)
#define HANDLE_ASYNC_PLAYBACK_MASK                  (_FALSE_)
#define HANDLE_MULTI_PACKET_MASK                    (_FALSE_)

/*! Accessory Status Token Configuration. See (Lingo = 0x00, Cmd = 0x28 and Cmd = 0x39) */
#define BLUETOOTH_DEVICE_STATUS                     (_TRUE_)
#define FAULT_CONDITION_STATUS                      (_TRUE_)

/*! Read Firmware Release Notes to know what lingoes are already supported */
#define IAP_GENERALLINGO_MASK                       (_TRUE_)
#define IAP_SIMPLEREMOTELINGO_MASK                  (_TRUE_)
#define IAP_DISPLAYREMOTELINGO_MASK                 (_TRUE_)
#define IAP_EXTENDEDINTERFACELINGO_MASK             (_TRUE_)
#define IAP_USBHOSTMODELINGO_MASK                   (_FALSE_)
#define IAP_RFTUNERLINGO_MASK                       (_FALSE_)
#define IAP_SPORTSLINGO_MASK                        (_FALSE_)
#define IAP_DIGITALAUDIOLINGO_MASK                  (_TRUE_)
#define IAP_STORAGELINGO_MASK                       (_FALSE_)
#define IAP_IPODOUTLINGO_MASK                       (_FALSE_)
#define IAP_LOCATIONLINGO_MASK                      (_FALSE_)
#endif // MFI_OVERRIDE_DEFAULT_CONFIGURATION 
/* ********************************************************************************************* */
/* ***************                 Do not modify after this line                 *************** */
/* ********************************************************************************************* */

#define IAP_CMD_ACC_MAX_PAYLOAD                   (uint16_t)(IAPI_MAX_RX_PAYLOAD_SIZE)

#define uint32_t_TO_uint8_t_SPLIT(x)              (uint8_t)(x >> 24), (uint8_t)(x >> 16),\
                                                  (uint8_t)(x >> 8),  (uint8_t)(x)

/*************************************************************************************************/
/*                                      Typedef Section                                          */
/*************************************************************************************************/
#pragma pack (push,1)

/* Type for the asynchronous commands callbacks */
typedef void (* iap_commands_asynchronous_callback_t)(iap_interface_buffer_rx_frame_t* rxFrame);

/** Lingoes Enumeration */
typedef enum 
{
    kGeneralLingo, 
    kSimpleRemoteLingo = 0x02,
    kDisplayRemoteLingo,
    kExtendedInterfaceLingo,
    kUSBHostModeLingo = 0x06,
    kRFTunerLingo,
    kSportsLingo = 0x09,
    kDigitalAudioLingo,
    kStorageLingo = 0x0C,
    kIpodOutLingo,
    kLocationLingo
}iap_commands_lingoes_enumeration_t;

/** USB Host Lingo Enumeration */
typedef enum
{
    IAP_USBHOST_ACCESSORYACK = 0x00u,
    IAP_USBHOST_NOTIFYUSBMODE = 0x04u,
    IAP_USBHOST_IPODACK = 0x80u,
    IAP_USBHOST_GETIPODUSBMODE,
    IAP_USBHOST_RETIPODUSBMODE,
    IAP_USBHOST_SETIPODUSBMODE
}__USB_HOST_LINGO_COMMANDS_INDEX__;

/** Notify USB Mode Status Codes enumeration */
typedef enum
{
    USB_MODE_INTERFACE_DISABLED = 0,
    USB_MODE_INTERFACE_IN_DEVICE_MODE,
    USB_MODE_INTERFACE_IN_HOST_MODE
}_NotifyUSBMode_StatusCodes_;

typedef enum
{
    IAP_IPODOUT_IPODACK = 0x00u,
    IAP_IPODOUT_GETIPODOUTOPTIONS,
    IAP_IPODOUT_RETIPODOUTOPTIONS,
    IAP_IPODOUT_SETIPODOUTOPTIONS,
    IAP_IPODOUT_ACCESORYSTATECHANGEEVENT,
    IAP_IPODOUTLINGO_COMMANDS_COUNT
}__IPODOUT_LINGO_COMMANDS_INDEX__;

   
/** Ipod Ack Status Bits Enumeration */
typedef enum
{
    kIAPAckSuccess = 0u,
    kIAPAckUnknownDBorSession,
    kIAPAckCommandFailed,
    kIAPAckDevOutOfResources,
    kIAPAckBadParameter,
    kIAPAckUnknownID,
    kIAPAckCommandPending,
    kIAPAckNotAuthenticated,
    kIAPAckBadAuthenticationVer,
    kIAPAckPowerModeFail,
    kIAPAckCertificateInvalid,
    kIAPAckCertificatePermissionInvalid,
    kIAPAckFileInUse,
    kIAPAckInvalidFileHandle,
    kIAPAckDirectoryNotEmpty,
    kIAPAckOperationTimeout,
    kIAPAckCommandUnavailableInMode,
    kIAPAckAccessoryDetectNotGND,
    kIAPAckSelectionNotGenius,
    kIAPAckMultiselectionSuccess,
    kIAPAckLingoBusy,
    kIAPAckMaxAccessoriesConnected,
    kIAPAckHIDIndexInUse,
    kIAPAckDroppedData,
    kIAPAckiPodOutIncompatible
}iap_acknowledge_t;

/*! Status of the search for an expected answer */ 
typedef enum
{
    /*! This value is returned if the command not have an expected answer*/
    kCommandError = 0xFF
} command_answer_status_t;

/*! Type of received command */
typedef enum
{
    /*! The received command is a response to a previously sent command (question) */
    kExpectedCommand,
    /*! The received command is an acknowledge to a previously sent command */
    kAckCommand,
    /*! The received command is an asynchronous command sent by the Apple device */
    kAsynchronousCommand,
    /*! */
    kUnexpectedErrorCommand
}received_command_t;

/*! Transmit command function return status */
typedef enum
{
    /*! The command was successfully queued and will be sent */
    kCommandTransmissionOk,
    /*! The command that is trying to be transmitted has an invalid/not-supported lingo */
    kTransmissionInvalidLingo,
    /*! The command that is trying to be transmitted has an invalid/not-supported command */
    kTransmissionInvalidCommand,
    /*! The transmission buffer is full so no more commands can be transmitted at the moment */
    kTransmissionInterfaceError,
    /*! The number of commands waiting for response has reached its maximum value
     *  so no more commands waiting for response can be sent */
    kCommandsWaitingForResponseFullArrayError,
    /*! There was an error in the release of a periodical command */
    kPeriodicalReleaseError,
    /*! One or more of the parameters need it to send the packet has an invalid or out-of-range value */
    kInvalidParameter
}command_transmission_status_t;

/*! Reception command function return status */
typedef enum
{
    /*! The received command was successfully processed  */
    kCommandReceptionOk,
    /*! The received command has an invalid/not-supported lingo */
    kReceptionInvalidLingo,
    /*! The received command has an invalid/not-supported command */
    kReceptionInvalidCommand,
    /*! There are no received commands */
    kNoCommandsReceived
}command_reception_status_t;


/*! Copy multisection payload return status */
typedef enum
{
    /*! The data from the multisection command has not been fully received */
    kMultisectionIncomplete,
    /*! All the data expected from the multisection command has been successfully received */
    kMultisectionDone,
    /*! The received data from the multisection command did not fit in the buffer that will store 
     * the received data */
    kMultisectionPayloadOverflow
}copy_multisection_status_t;

/** Return iPod Preferences - Assistive Touch (Lingo = 0x00, Cmd = 0x2A) Enumeration */
typedef enum
{
    ASSISTIVE_TOUCH_OFF = 0,
    ASSISTIVE_TOUCH_ON
}_IPodPrefs_AssistiveTouchValues_;

/** Return iPod Preferences (Lingo = 0x00, Cmd = 0x2A) Structure */ 
typedef struct
{
    uint8_t bPreferenceClassID;
    uint8_t bPreferenceSettingID;
}iAP_RetIPodPreferenceType;

/** Set iPod Preferences (Lingo = 0x00, Cmd = 0x2B) Structure */ 
typedef struct
{
    uint8_t bPreferenceClassID; 		/**< Use Ret iPod Preference Apple device class ID Enumeration values */
    uint8_t bPreferenceSettingID;	/**< Use Ret iPod Preference Apple device Settings ID Enumeration values */
    uint8_t bRestoreOnExit;
}iAP_SetIPodPreferenceType;


/*! Current periodical command state */
typedef enum
{
    /*! There is not a periodical command that is currently being sent */
    kPeriodicalCommandAvailable,
    /*! There is a periodical command that is currently being sent */
    kPeriodicalCommandBusy
}periodical_command_state_t;

/*! Structure to store the information of the commands waiting for response */
typedef struct{
    /*! Field to store the callback that will be called once
     *  the answer of the command that is waiting for response is received */
    void (*rxAnswerCallback) (uint8_t);
    /*! Pointer to the buffer where the payload of the answer will be saved */
    uint8_t* pointerToSaveAnswer;
    /*! Size of the buffer where the payload of the answer will be saved */
    uint16_t sizeToSaveAnswer;
    /*! Field to save the expected response of the command */
    uint16_t expectedCommand;
    /*! Field to save the command value of the expected acknowledge */
    uint8_t expectedAck;
    /*! Field to save the lingo value of the expected command */
    uint8_t expectedLingo;
    /*! Index of the interface retry commands array used to access to the transmitted packet
     *  and used to free that element of the array once the answer to the command is received */
    uint8_t indexOfInterfaceRetryArray;
    /*! Field to save the current status of the command to be transmitted */
    uint8_t commandTxState;
    /*! Field to save the payload size of the received command */
    uint16_t* totalPayloadSizePointer;
}command_waiting_for_response_t;

/*! Structure to store the information of the periodical command that is currently being sent */
typedef struct
{
    /*! Field to save the index of the interface retry commands array */
    uint8_t indexOfInterfaceRetryCommandsArray;
    /*! Field to save the index of the commands waiting for response array */
    uint8_t indexOfCommandsWaitingForResponseArray;
    /*! Field that stores the command value of the periodical command */
    uint8_t command;
    /*! Field that stores lingo the periodical command */
    uint8_t lingo;
    /*! Field that stores the current state of the periodical command*/
    periodical_command_state_t periodicalCommandState;
}current_periodical_command_t;

/*! Type that will be used to send a command */
typedef struct 
{
    /*! Structure that contains all the necessary information (the iAP_interface Layer needs) 
     * to build and send an iAP1 packet */
    iap_interface_tx_frame_data_t txFrameData;
    /*! Callback that will be called once the answer of the command that is waiting for response
     *  is received */
    void (*rxAnswerCallback)(uint8_t);
    /*! Pointer to a variable where the size of the actual received data will be saved*/
    uint16_t* pointerToSaveReceivedPayloadSize;
    /*! Pointer to the buffer where the payload of the answer will be saved */
    uint8_t* pointerToSaveAnswer;
    /*! Size of the buffer where the answer will be saved */
    uint16_t sizeToSaveAnswer;
    /*! Command ID value of the expected response */
    uint8_t expectedAnswer;
}iap_commands_tx_command_t;


#pragma pack (pop)

/*************************************************************************************************/
/*                                Function-like Macros Section                                   */
/*************************************************************************************************/

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/
/*! Array of pointers that can be used to address to the variables that stores the iPod Options
 * used by Host layer during the IDPS process */
extern const uint8_t* kIapCommandsLingoesIpodOptions[];

/*************************************************************************************************/
/*                                  Extern Variables Section                                     */
/*************************************************************************************************/
/*! Global variables that stores the TID from received asynchronous commands used in the 
 * response to those commands */
extern uint16_t g_iapCommandsTransactionID;

/*************************************************************************************************/
/*                                Function Prototypes Section                                    */
/*************************************************************************************************/

/*!
 * @brief Function used to initialise the iAP Commands layer 
 * @return void
 */
void iap_commands_initialization(void);

/*!
 * Generic function used to transmit commands
 * @param txCommandInfo Pointer to the structure that stores all the information need it to send a 
 *                      command
 * @return If the transmitted command was queued in the transmission queue successfully 
 * @retval kCommandTransmissionOk 
 * @retval kInvalidLingo 
 * @retval kInvalidCommand 
 * @retval kTransmissionBufferFullError 
 * @retval kCommandsWaitingfForResponseFullArrayError 
 */
command_transmission_status_t iap_commands_transmit_command(iap_commands_tx_command_t* txCommandInfo);

/*!
 * @brief Function used to check if there are received commands that need to be processed
 * @return value indicating if a valid command was received or not
 * @retval kCommandReceptionOk if a valid command is in the queue
 * @retval kReceptionInvalidCommand if there was a valid lingo but command is not supported or doesn't exist
 * @retval kReceptionInvalidLingo if the lingo doesn't exist or is not supported 
 */
command_reception_status_t iap_commands_receive_command(void);

/*!
 * @brief Function used to reset the data of commands layer 
 * @return void
 */
void iap_commands_reset(void);

/*!
 * @brief Function used to uninit commands layer 
 * @return void
 */
void iap_commands_destroy_mutex(void);


/*!
* 
* @brief    Function that copies an array into another array, used to copy the received payload
*           into a buffer given by the application layer
* @param    source              Pointer to the source buffer
* @param    sourceSize          Size of the source buffer
* @param    destination         Pointer to the destination buffer
* @param    destinationSize     Size of the destination buffer
* @return   void
 */
void iap_commands_copy_payload(uint8_t* source, uint16_t sourceSize,
                               uint8_t* destination, uint16_t destinationSize);

/*!
* 
* @brief    Function that copies an array into another array considering the multisection logic of
*           the protocol in order to concatenate several data packets into one buffer 

* @param    sectionCurrent          The value of the current section of the packets
* @param    sectionMax              The value of the last section of the packets
* @param    bytesOffset             Number of bytes that must be skipped from the payload in order 
*                                   to access to the actual data
* @param    sourcePayload           Pointer to a received payload that forms part of the whole data
* @param    sourcePayloadSize       Size of te received payload
* @param    destinationPayload      Pointer to the destination buffer
* @param    destinationPayloadSize  Total size of the destination buffer
* @param    totalReceivedPayloadPointer  The total received payload will be stored in the given address
* @return   A  value that signals whether the process has been completed successfully or not
* @retval   kMultisectionIncomplete         Copy not yet done
* @retval   kMultisectionDone               Copy done
* @retval   kMultisectionPayloadOverflow    Source size bigger than destination size
*/
copy_multisection_status_t iap_commands_copy_multisection_payload(uint16_t sectionCurrent,
                                                                  uint16_t sectionMax,
                                                                  uint8_t bytesOffset, 
                                                                  uint8_t* sourcePayload,
                                                                  uint16_t sourcePayloadSize,
                                                                  uint8_t* destinationPayload,
                                                                  uint16_t destinationPayloadSize,
                                                                  uint16_t* totalReceivedPayloadPointer);

/*!
 * @brief Function to release the data from the Tx Buffer of a periodical command and send a 
 *        command that indicates the release of the periodical command
 * @param txCommandInfo Pointer to the structure that stores all the information need it to send a 
 *                      command
 * @return If the transmitted command was queued in the transmission queue successfully 
 * @retval kCommandTransmissionOk 
 * @retval kInvalidLingo 
 * @retval kInvalidCommand 
 * @retval kTransmissionBufferFullError 
 * @retval kCommandsWaitingfForResponseFullArrayError 
 * @retval kPeriodicalReleaseError 
 */
command_transmission_status_t iap_commands_release_periodical_command(iap_commands_tx_command_t* txCommandInfo);

/*! @} 
* Group iAP_Commands
* @}  
*
* Group iAP1
*/

#endif /* IAP_COMMANDS_H_ */


