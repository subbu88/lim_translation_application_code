/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "osa_common.h"

#include "iAP_Commands.h"
#include "MFi_Config.h"

#include "GeneralLingo.h"
#include "SimpleRemoteLingo.h"
#include "DigitalAudioLingo.h"
#include "ExtendedInterfaceLingo.h"
#include "DisplayRemoteLingo.h"

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/

#define iAP_CMDS_NO_PAYLOAD_SIZE                (0x00u)
#define iAP_CMDS_APPLE_CERT_VAL_RESULT_SIZE     (0x01u)

#define iAP_CMDS_APPLE_MAYOR_VER_OFFSET         (0x00u)
#define iAP_CMDS_APPLE_MINOR_VER_OFFSET         (0x01u)
#define IAP_CMDS_APPLE_AUTH_MAJOR_VER_OFFSET    (0x00u)
#define IAP_CMDS_APPLE_AUTH_MINOR_VER_OFFSET    (0x01u)

#define iAP_CMDS_APPLE_MAYOR_VER                (0x02u)
#define IAP_CMDS_APPLE_MINOR_VER                (0x00u)

#define IAP_CMDS_SECT_AND_VER_BYTES             (0x04u)
#define IAP_CMDS_APPLE_SIGN_MAX_RETRIES         (0x02u)

#define IAP_CMDS_NUM_COMMANDS_WAITING_FOR_RESPONSE       NUM_OF_TX_PACKETS

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/

/*! Return bit flag values for the s_iap_commands_check_command_type function */
typedef enum
{
    /*! Value that indicates that there aren't any commands waiting for response */
    kEmptyArray = 0,
    /*! The received commands is an expected answer*/
    kExpectedCommandFlag = 1,
    /*! The received commands is an acknowledge to a previously asked question */
    kAckCommandFlag = 2,
    /*! The received commands is an asynchronous question */
    kAsynchronousCommandFlag = 4,
    /*! There aren't any commands waiting for the response from the received command lingo */
    kLingoNotFoundFlag = 8,
    /*! There are commands from the same lingo waiting for response but the received response
     *  does not match any of them */
    kCommandNotFoundFlag = 16,
    /*! The CommandId and LingoId of one of the commands waiting for response matched with 
     * the received command's Lingo and CommandID but the TID did not match */
    kCommandTIDNotFoundFlag = 32,
    
    kAckTIDNotFoundFlag = 64
}received_command_flags_t;

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/

static void s_expected_command(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand);

static void s_ack_command(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand);

static void s_async_command(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand);

static inline void s_unexpected_error_command(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand);

static uint8_t s_iap_commands_search_async_command(uint8_t lingo, uint8_t command);

static received_command_flags_t s_iap_commands_check_command_type
                            (command_waiting_for_response_t* commandWaitingForResponse,
                             iap_interface_buffer_rx_frame_t* receivedCommand);

static uint16_t s_get_section_current(iap_interface_buffer_rx_frame_t* currentRxFrame);

static uint16_t s_get_section_max(iap_interface_buffer_rx_frame_t* currentRxFrame,
                                  uint8_t* bytesOffset);

static void s_iap_commands_no_response_error_callback(uint8_t retryCommandsArrayIndex);

/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/
/*! Global variables that stores the TID from received asynchronous commands used in the 
 * response to those commands */
uint16_t g_iapCommandsTransactionID = 0;

/*! Variables used to store the information of the commands waiting for response */
random_access_array_information_t g_commandsWaitingForResponseInfo = {0, 0, 0, IAP_CMDS_NUM_COMMANDS_WAITING_FOR_RESPONSE};
command_waiting_for_response_t g_commandsWaitingForResponse[IAP_CMDS_NUM_COMMANDS_WAITING_FOR_RESPONSE]; 

/*! Variable that stores the information of the periodical command that is currently being sent */
current_periodical_command_t g_iapCommandsCurrentPeriodicalCommand =
{
        kNotAvailableIndex,
        kNotAvailableIndex,
        0x00,
        0x00,
        kPeriodicalCommandAvailable
};

/*! Variables to store the iPod Options received by the GetiPodOptionsForLingo command */
#if (IAP_GENERALLINGO_MASK)
static uint32_t s_iapCommandsGeneralLingoIpodOptions;
#endif
#if (IAP_SIMPLEREMOTELINGO_MASK)
static uint32_t s_iapCommandsSimpleRemoteIpodOptions;
#endif
#if (IAP_DISPLAYREMOTELINGO_MASK)
static uint32_t s_iapCommandsDisplayRemoteIpodOptions;
#endif
#if (IAP_EXTENDEDINTERFACELINGO_MASK)
static uint32_t s_iapCommandsExtendedInterfaceIpodOptions;
#endif
#if (IAP_USBHOSTMODELINGO_MASK)
static uint32_t s_iapCommandsUsbHostModeIpodOptions;
#endif
#if (IAP_RFTUNERLINGO_MASK)
static uint32_t s_iapCommandsRFTunerIpodOptions;
#endif
#if (IAP_SPORTSLINGO_MASK)
static uint32_t s_iapCommandsSportsIpodOptions;
#endif
#if (IAP_DIGITALAUDIOLINGO_MASK)
static uint32_t s_iapCommandsDigitalAudioIpodOptions;
#endif
#if (IAP_STORAGELINGO_MASK)
static uint32_t s_iapCommandsStorageIpodOptions;
#endif
#if (IAP_IPODOUTLINGO_MASK)
static uint32_t s_iapCommandsIpodOutIpodOptions;
#endif
#if (IAP_LOCATIONLINGO_MASK)
static uint32_t s_iapCommandsLocationIpodOptions;
#endif

/*! Array that stores pointers to the start of the arrays that contain the function pointers to the
 * each of the corresponding asynchronous commands */
void (** s_kAsynchronousCommandsFunctionsByLingos
        [IAP_LINGOES_COUNT]) (iap_interface_buffer_rx_frame_t* rxFrame) =
{
        kGeneralLingoAsyncrhonousFunctions,
        NULL,
        kSimpleRemoteAsynchronousFunctions,
        kDisplayRemoteAsynchronousFunctions,
        kExtendedInterfaceAsynchronousFunctions,    //  IAP_EXTENDEDINTERFACELINGO
        NULL,
        NULL,
        NULL,                                       //  IAP_RFTUNERLINGO,
        NULL,
        NULL,                                       //  IAP_SPORTSLINGO = 0x09,
        kDigitalAudioLingoAsynchronousFunctions,
        NULL,
        NULL,                                      //  IAP_STORAGELINGO = 0x0C,
        NULL,                                      //  IAP_IPODOUTLINGO,
        NULL                                       //  IAP_LOCATIONLINGO,
};

/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/

static OsaMutex iAPCommandsMutex;

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
/*! Array that contains the number of commands of each Lingo */
static const uint16_t lingoesCommandsCount[] = 
{
#if (IAP_GENERALLINGO_MASK)
        (uint16_t)(IAP_GENERALLINGO_COMMANDS_COUNT),
#else
        (uint16_t)(0),
#endif
        (uint16_t)(0),                                                /* Lingo does not exist */
#if (IAP_SIMPLEREMOTELINGO_MASK)
        (uint16_t)(IAP_SIMPLEREMOTE_COMMANDS_COUNT),
#else
        (uint16_t)(0),
#endif
#if (IAP_DISPLAYREMOTELINGO_MASK)
        (uint16_t)(IAP_DISPLAYREMOTELINGO_COMMANDS_COUNT),
#else
        (uint16_t)(0),
#endif
#if (IAP_EXTENDEDINTERFACELINGO_MASK)
        (uint16_t)(IAP_EXTENDEDINTERFACELINGO_COMMANDS_COUNT),
#else
        (uint16_t)(0),
#endif
        (uint16_t)(0),                                                /* Lingo does not exist */
#if (IAP_USBHOSTMODELINGO_MASK)
        (uint16_t)(IAP_USBHOSTMODELINGO_COMMANDS_COUNT),
#else
        (uint16_t)(0),
#endif
#if (IAP_RFTUNERLINGO_MASK)
        (uint16_t)(NULL),            /* no implemented yet */
#else
        (uint16_t)(0),
#endif
        (uint16_t)(0),                                                /* Lingo does not exist */
#if (IAP_SPORTSLINGO_MASK)
        (uint16_t)(0),            /* no implemented yet */
#else
        (uint16_t)(0),
#endif
#if (IAP_DIGITALAUDIOLINGO_MASK)
        (uint16_t)(IAP_DIGITALAUDIOLINGO_COMMANDS_COUNT),
#else
        (uint16_t)(0),
#endif
        (uint16_t)(0),                                                /* Lingo does not exist */
#if (IAP_STORAGELINGO_MASK)
        (uint16_t)(0),            /* no implemented yet */
#else
        (uint16_t)(0),
#endif
#if (IAP_IPODOUTLINGO_MASK)
        (uint16_t)(IAP_IPODOUTLINGO_COMMANDS_COUNT),
#else
        (uint16_t)(0),
#endif
#if (IAP_LOCATIONLINGO_MASK)
        (uint16_t)(0),            /* no implemented yet */
#else
        (uint16_t)(0),
#endif
};

/*! Array of pointers that contains the start of the arrays that contain
 *  the asynchronous commands for each lingo */
static const uint8_t* s_kAsyncrhonousCommandsByLingos[] =
{
        kGeneralLingoAsynchronousCommands,
        NULL,
        kSimpleRemoteAsynchronousCommands,
        kDisplayRemoteAsynchronousCommands,
        kExtendedInterfaceAsynchronousCommands,
        NULL,
        NULL,
        NULL,                       /* IAP_RFTUNERLINGO */
        NULL,
        NULL,                       /* IAP_SPORTSLINGO = 0x09 */
        kDigitalAudioLingoAsynchronousCommands,
        NULL,
        NULL,                       /* IAP_STORAGELINGO = 0x0C */
        NULL,                       /* IAP_IPODOUTLINGO */
        NULL                        /* IAP_LOCATIONLINGO */
};

/*! Array that contains the number of asynchronous commands for each lingo */
static const uint8_t s_kSizeOfLingosWithAsyncCommands[IAP_LINGOES_COUNT] =
{
        NUMBER_OF_GENERALLINGO_ASYNCHRONOUS_COMMANDS,
        (uint8_t) (0),
        NUMBER_OF_SIMPLEREMOTE_ASYNCHRONOUS_COMMANDS,
        NUMBER_OF_DISPLAYREMOTE_ASYNCHRONOUS_COMMANDS,
        NUMBER_OF_EXTENDEDINTERFACE_ASYNCHRONOUS_COMMANDS,              
        (uint8_t) (0),
        (uint8_t) (0),
        (uint8_t) (0),                       /* IAP_RFTUNERLINGO */
        (uint8_t) (0),
        (uint8_t) (0),                       /* IAP_SPORTSLINGO = 0x09 */
        NUMBER_OF_DIGITALAUDIO_ASYNCHRONOUS_COMMANDS,
        (uint8_t) (0),
        (uint8_t) (0),                       /* IAP_STORAGELINGO = 0x0C */
        (uint8_t) (0),                       /* IAP_IPODOUTLINGO */
        (uint8_t) (0)                        /* IAP_LOCATIONLINGO */
        
};

/*! Array that contains the Acknowledge commands value for each lingo */
static const uint8_t s_kAckCommandsByLingo[IAP_LINGOES_COUNT] = {
        kGeneralLingoIpodAck,
        (uint8_t) (0),
        kSimpleRemoteIpodAck,
        kDisplayRemoteIpodAck,
        kExtendedInterfaceIpodAck,       
        (uint8_t) (0),
        IAP_USBHOST_IPODACK,
        (uint8_t) (0),                       /* IAP_RFTUNERLINGO */
        (uint8_t) (0),
        (uint8_t) (0),                       /* IAP_SPORTSLINGO = 0x09 */
        kDigitalAudioIpodAck,
        (uint8_t) (0),
        (uint8_t) (0),                       /* IAP_STORAGELINGO = 0x0C */
        (uint8_t) (0),                       /* IAP_IPODOUTLINGO */
        (uint8_t) (0)                        /* IAP_LOCATIONLINGO */
};

/*! Private array of pointers to function used to call one of four functions 
 * depending of the type of received command */
static void (*const s_commandTypeFunction[])(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand) =
{
        s_expected_command,
        s_ack_command,
        s_async_command,
        s_unexpected_error_command
};

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/
/*! Array of pointers that can be used to address to the variables that stores the iPod Options*/
const uint8_t *kIapCommandsLingoesIpodOptions[] =
{
#if (IAP_GENERALLINGO_MASK)
        (uint8_t*)(&s_iapCommandsGeneralLingoIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
        (uint8_t*)(NULL),        /* Does not exist */
#if (IAP_SIMPLEREMOTELINGO_MASK)
        (uint8_t*)(&s_iapCommandsSimpleRemoteIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_DISPLAYREMOTELINGO_MASK)
        (uint8_t*)(&s_iapCommandsDisplayRemoteIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_EXTENDEDINTERFACELINGO_MASK)
        (uint8_t*)(&s_iapCommandsExtendedInterfaceIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
        (uint8_t*)(NULL),        /* Does not exist */
#if (IAP_USBHOSTMODELINGO_MASK)
        (uint8_t*)(&s_iapCommandsUsbHostModeIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_RFTUNERLINGO_MASK)
        (uint8_t*)(&s_iapCommandsRFTunerIpodOptions),
#else
        (uint8_t*)(NULL),    
#endif
        (uint8_t*)(NULL),        /* Does not exist */
#if (IAP_SPORTSLINGO_MASK)
        (uint8_t*)(&s_iapCommandsSportsIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_DIGITALAUDIOLINGO_MASK)
        (uint8_t*)(&s_iapCommandsDigitalAudioIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
        (uint8_t*)(NULL),        /* Does not exist */
#if (IAP_STORAGELINGO_MASK)
        (uint8_t*)(&s_iapCommandsStorageIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_IPODOUTLINGO_MASK)
        (uint8_t*)(&s_iapCommandsIpodOutIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
#if (IAP_LOCATIONLINGO_MASK)
        (uint8_t*)(&s_iapCommandsLocationIpodOptions),
#else
        (uint8_t*)(NULL),
#endif
};


/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/
/* See iAP_Commands.h for documentation of this function. */
void iap_commands_initialization(void)
{
    RandomAccessArray_flush_array(&g_commandsWaitingForResponseInfo);
#if (COMM_WITH_IOS_APPLICATIONS_MASK == _TRUE_)
    iap_general_lingo_reset();
#endif

    /* Configure Interface Layer */
    iap_interface_initialization();
    
    /* Register callback for interface no response to a command error  */
    iap_interface_register_no_response_error_callback(s_iap_commands_no_response_error_callback);
    
    osa_mutex_create(&iAPCommandsMutex, false);
}

/* See iAP_Commands.h for documentation of this function. */
command_transmission_status_t iap_commands_transmit_command(iap_commands_tx_command_t* txCommandInfo)
{
    command_transmission_status_t txCommandStatus;
    iap_interface_send_frame_t txInterfaceStatus = kInterfaceBusy;
    uint8_t indexOfInterfaceArray = kNotAvailableIndex;
    uint8_t indexOfCommandsArray = kNotAvailableIndex;
    
    //TODO check validation to block user from asking more questions 
    osa_mutex_lock(&iAPCommandsMutex);

    /* Check if the lingo is supported */
    if(IAP_CMD_LINGOES_SUPPORTED_MASK & (1 << txCommandInfo->txFrameData.lingoId))
    {    
        /* Check if the command is supported */
        if (lingoesCommandsCount[txCommandInfo->txFrameData.lingoId] > 
                                                              txCommandInfo->txFrameData.commandId)
        {
            if(!(kCommandRepeatsPeriodically & txCommandInfo->txFrameData.commandCurrentTxState))
            {
                /* Use Interface function to queue and send packet */
                txInterfaceStatus = iap_interface_send_frame(&txCommandInfo->txFrameData,
                                                             &indexOfInterfaceArray);
            }else
            {
                if(kPeriodicalCommandAvailable 
                        == g_iapCommandsCurrentPeriodicalCommand.periodicalCommandState)
                {
                    /* Use Interface function to queue and send packet */
                    txInterfaceStatus = iap_interface_send_frame(&txCommandInfo->txFrameData,
                                                                 &indexOfInterfaceArray);
            
                    g_iapCommandsCurrentPeriodicalCommand.indexOfInterfaceRetryCommandsArray = indexOfInterfaceArray; 
                    g_iapCommandsCurrentPeriodicalCommand.periodicalCommandState = kPeriodicalCommandBusy;
                    g_iapCommandsCurrentPeriodicalCommand.command = txCommandInfo->txFrameData.commandId;
                    g_iapCommandsCurrentPeriodicalCommand.lingo = txCommandInfo->txFrameData.lingoId;
                }    
                else /* Then it means there is another periodical command currently being sent */
                {
                    /* Clear this bit flag to avoid adding an unnecessary element to the commands
                     *  waiting for response array*/
                    txCommandInfo->txFrameData.commandCurrentTxState &= ~kCommandIsWaitingForResponse;
                    
                    /* Check if an update to the payload, of the command that is currently being sent,
                     * can be done*/
                    if((txCommandInfo->txFrameData.commandId == g_iapCommandsCurrentPeriodicalCommand.command) &&
                            (txCommandInfo->txFrameData.lingoId == g_iapCommandsCurrentPeriodicalCommand.lingo))
                    {
                        /* The update can only be done if the interface is not busy in order
                         *  to ensure the data of a packet will not be overwritten while 
                         *  is being sent*/
                        if(!IAP_INTERFACE_TX_IN_PROGRESS_FLAG)
                        {
                            g_commandsWaitingForResponse[g_iapCommandsCurrentPeriodicalCommand.
                                                         indexOfCommandsWaitingForResponseArray].
                                                         rxAnswerCallback  = txCommandInfo->rxAnswerCallback;
                            g_commandsWaitingForResponse[g_iapCommandsCurrentPeriodicalCommand.
                                                         indexOfCommandsWaitingForResponseArray].
                                                         pointerToSaveAnswer = txCommandInfo->pointerToSaveAnswer;
                            g_commandsWaitingForResponse[g_iapCommandsCurrentPeriodicalCommand.
                                                         indexOfCommandsWaitingForResponseArray].
                                                         sizeToSaveAnswer = txCommandInfo->sizeToSaveAnswer;
                            
                            /* The payload update can be done, because the interface is not busy */
                            iap_interface_payload_update(txCommandInfo->txFrameData.payload, 
                                                         txCommandInfo->txFrameData.payloadSize, 
                                    g_iapCommandsCurrentPeriodicalCommand.indexOfInterfaceRetryCommandsArray);
                            
                            txInterfaceStatus = kInterfaceTxSuccess;
                        }
                    }
                }
            }
          
            /* Check for interface error  */
            if(txInterfaceStatus == kInterfaceTxSuccess)
            {
                txCommandStatus = kCommandTransmissionOk;
                
                /* If commands is waiting for response save the information needed 
                 * when the answer is received ('context') */
                if(kCommandIsWaitingForResponse & txCommandInfo->txFrameData.commandCurrentTxState)
                {
                    indexOfCommandsArray = 
                            RandomAccessArray_get_next_avaliable_position(&g_commandsWaitingForResponseInfo);
                    
                    /* Check for availability in commands waiting for response array */
                    if(indexOfCommandsArray != kNotAvailableIndex)
                    {
                        g_commandsWaitingForResponse[indexOfCommandsArray].rxAnswerCallback
                            = txCommandInfo->rxAnswerCallback;
                        g_commandsWaitingForResponse[indexOfCommandsArray].pointerToSaveAnswer
                            = txCommandInfo->pointerToSaveAnswer;
                        g_commandsWaitingForResponse[indexOfCommandsArray].sizeToSaveAnswer
                            = txCommandInfo->sizeToSaveAnswer;
                        g_commandsWaitingForResponse[indexOfCommandsArray].expectedCommand
                            = txCommandInfo->expectedAnswer;
                        g_commandsWaitingForResponse[indexOfCommandsArray].expectedAck
                            = s_kAckCommandsByLingo[txCommandInfo->txFrameData.lingoId];
                        g_commandsWaitingForResponse[indexOfCommandsArray].expectedLingo
                            = txCommandInfo->txFrameData.lingoId;
                        g_commandsWaitingForResponse[indexOfCommandsArray].indexOfInterfaceRetryArray 
                            = indexOfInterfaceArray;
                        g_commandsWaitingForResponse[indexOfCommandsArray].commandTxState
                            = txCommandInfo->txFrameData.commandCurrentTxState;
                        g_commandsWaitingForResponse[indexOfCommandsArray].totalPayloadSizePointer
                            = txCommandInfo->pointerToSaveReceivedPayloadSize;
                        
                        if(kCommandRepeatsPeriodically & txCommandInfo->txFrameData.commandCurrentTxState)
                        {
                            g_iapCommandsCurrentPeriodicalCommand.indexOfCommandsWaitingForResponseArray = indexOfCommandsArray;
                        }
                        
                    }else
                    {
                         /* Commands waiting for response array is full */
                        txCommandStatus = kCommandsWaitingForResponseFullArrayError;
                    }
                }
            }else
            {
                /* The interface layer could not allocate the command */
                txCommandStatus = kTransmissionInterfaceError;
            }

        }else
        {
            /* Invalid or not supported command */
            txCommandStatus = kTransmissionInvalidCommand;
        }
    }else
    {
        /* Invalid or not supported lingo */
        txCommandStatus = kTransmissionInvalidLingo;
    }
    osa_mutex_unlock(&iAPCommandsMutex);
    return txCommandStatus;
    
}

/* See iAP_Commands.h for documentation of this function. */
command_reception_status_t iap_commands_receive_command(void)
{
    iap_interface_buffer_rx_frame_t* receivedCommand;
    command_reception_status_t rxStatus;

    rxStatus = kNoCommandsReceived;

    /* Get the last received command (in case there is one) */
    receivedCommand = iap_interface_get_rx_command();

    /* Process the received commands */
    if(receivedCommand != NULL)
    {
        /* Check if the lingo is supported */
        if(IAP_CMD_LINGOES_SUPPORTED_MASK & (1 << receivedCommand->lingoId))
        {
            /* Check if the command is supported */
            if (lingoesCommandsCount[receivedCommand->lingoId] > receivedCommand->commandId)
            {
                uint8_t searchTxCommandsIndex = 0;
                uint8_t commandTypeFlags = 0;
                uint8_t commandType = kEmptyArray;
               
                osa_mutex_lock(&iAPCommandsMutex);
                
                /* Search if the received command is either a response of a previously 
                 * sent command or an asynchronous command sent by the Apple device */
                do{
                    /* Only check the elements of the commands waiting for response array
                     *  that has a valid element */
                    if(IS_POSITION_RESERVED(g_commandsWaitingForResponseInfo, 
                            searchTxCommandsIndex))
                    {
                        commandTypeFlags |= s_iap_commands_check_command_type(
                                &g_commandsWaitingForResponse[searchTxCommandsIndex],
                                receivedCommand);

                    }
                    searchTxCommandsIndex++;
                    /* Check in the commands waiting for response array until the response 
                     * to a command is found or until we have searched in the whole array */
                }while((searchTxCommandsIndex < IAP_CMDS_NUM_COMMANDS_WAITING_FOR_RESPONSE) 
                        && !(commandTypeFlags & (kExpectedCommandFlag | kAckCommandFlag)));
                
                osa_mutex_unlock(&iAPCommandsMutex);
                
                rxStatus = kCommandReceptionOk;

                if(commandTypeFlags & kExpectedCommandFlag)
                {
                    commandType = kExpectedCommand;
                }
                else if(commandTypeFlags & kAckCommandFlag)
                {
                    commandType = kAckCommand;
                }
                else if((commandTypeFlags & (kLingoNotFoundFlag | kCommandNotFoundFlag))
                        || (commandTypeFlags == kEmptyArray))
                {
                    commandType = kAsynchronousCommand;
                }
                else
                {
                    commandType = kUnexpectedErrorCommand;
                }

                /* Jump to type of command function */
                s_commandTypeFunction[commandType]((searchTxCommandsIndex - 1), receivedCommand);
            }else
            {
                /* Invalid or not supported command */
                rxStatus = kReceptionInvalidCommand;
            }
        }else
        {
            /* Invalid or not supported lingo */
            rxStatus = kReceptionInvalidLingo;
        }
    }

    return rxStatus;
}

/* See iAP_Commands.h for documentation of this function. */
void iap_commands_reset(void)
{
    iap_interface_reset();
    
    RandomAccessArray_flush_array(&g_commandsWaitingForResponseInfo);
    
#if (COMM_WITH_IOS_APPLICATIONS_MASK == _TRUE_)
    iap_general_lingo_reset();
#endif 
}

/* See iAP_Commands.h for documentation of this function. */
void iap_commands_destroy_mutex(void)
{
    osa_mutex_destroy(&iAPCommandsMutex);
}

/* See iAP_Commands.h for documentation of this function. */
command_transmission_status_t iap_commands_release_periodical_command(iap_commands_tx_command_t* txCommandInfo)
{
    command_transmission_status_t txStatus;

    txStatus = kPeriodicalReleaseError;

    if(kPeriodicalCommandBusy 
            == g_iapCommandsCurrentPeriodicalCommand.periodicalCommandState)
    {
        if((txCommandInfo->txFrameData.commandId == g_iapCommandsCurrentPeriodicalCommand.command) &&
                (txCommandInfo->txFrameData.lingoId == g_iapCommandsCurrentPeriodicalCommand.lingo))
        {
            g_iapCommandsCurrentPeriodicalCommand.command = 0x00;
            g_iapCommandsCurrentPeriodicalCommand.lingo = 0x00;
            g_iapCommandsCurrentPeriodicalCommand.periodicalCommandState = kPeriodicalCommandAvailable;

            /* Release the transmitted command from the data buffer and the retry commands array */
            iap_interface_release_tx_command(g_iapCommandsCurrentPeriodicalCommand.indexOfInterfaceRetryCommandsArray);

            /* Release the command from the commands waiting for response array (If the command is not waiting for response 
             * the iAP_Commands_g_currentPeriodicalCommand.indexOfCommandsWaitingForResponseArray will contain an invalid 
             * index and no command will be released from the commands waiting for response array) */
            RandomAccessArray_free_position(&g_commandsWaitingForResponseInfo, 
                                            g_iapCommandsCurrentPeriodicalCommand.indexOfCommandsWaitingForResponseArray);  

            txStatus = iap_commands_transmit_command(txCommandInfo);
        }
    }

    return txStatus;
}

/* See iAP_Commands.h for documentation of this function. */
void iap_commands_copy_payload(uint8_t* source, uint16_t sourceSize,
                               uint8_t* destination, uint16_t destinationSize)
{
    uint16_t payloadSize = 0;

    /* Check if the array contains a valid pointer */
    if((source != NULL) && (destination != NULL))
    {
        /* Check if the array contains a valid size and 
         * if the received payload will fit into the buffer */
        if(destinationSize >= sourceSize)
        {
            payloadSize = sourceSize;
        }else
        {
            payloadSize = destinationSize;
        }

        /* Copy the received payload into the given buffer */
        while(payloadSize--)
        {
            destination[payloadSize] = source[payloadSize];
        }
    }
}

/* See iAP_Commands.h for documentation of this function. */
copy_multisection_status_t iap_commands_copy_multisection_payload(uint16_t sectionCurrent,
                                                                  uint16_t sectionMax,
                                                                  uint8_t bytesOffset, 
                                                                  uint8_t* sourcePayload,
                                                                  uint16_t sourcePayloadSize,
                                                                  uint8_t* destinationPayload,
                                                                  uint16_t destinationPayloadSize,
                                                                  uint16_t* totalReceivedPayloadPointer)
{
    static uint8_t* s_currentDestinationPayload;
    static uint16_t s_currentDestinationPayloadSize;
    static uint16_t s_totalReceivedPayloadSize;
    copy_multisection_status_t multisectionStatus = kMultisectionIncomplete;

    /* For the packets that aren't the first one (sectCur != 0) */ 
    if(sectionCurrent != 0) 
    {
        destinationPayload = s_currentDestinationPayload;
        destinationPayloadSize = s_currentDestinationPayloadSize;
    }else /* For the first packet (sectCur == 0) */ 
    {
        s_totalReceivedPayloadSize = 0;

        /* Initialize the static variables that will be used to store the received payload in
         *  the corresponding position of the application's buffer */
        s_currentDestinationPayload = destinationPayload;
        s_currentDestinationPayloadSize = destinationPayloadSize;
    }

    /* Update the source pointer and source size */
    sourcePayload += bytesOffset;
    sourcePayloadSize -= bytesOffset;

    s_totalReceivedPayloadSize += sourcePayloadSize; 

    /* Update the destination pointer and destination size only if the received payload fits 
     * in the application's buffer */
    if((int32_t) (s_currentDestinationPayloadSize - sourcePayloadSize) > 0x00)
    {
        s_currentDestinationPayload += sourcePayloadSize;
        s_currentDestinationPayloadSize -= sourcePayloadSize;
    }else
    {
        /* Otherwise reinit static variables*/
        s_currentDestinationPayload = NULL;
        s_currentDestinationPayloadSize = 0;
        
        *totalReceivedPayloadPointer = s_totalReceivedPayloadSize;
        
        /* Signal payload overflow The following received data
         * won't fit on the application's buffer so there */
        multisectionStatus = kMultisectionPayloadOverflow;
    }

    iap_commands_copy_payload(sourcePayload, sourcePayloadSize,
                                destinationPayload, destinationPayloadSize);
    
    /* Check if the received packet is the last packet */
    if(sectionCurrent == sectionMax)
    {
        *totalReceivedPayloadPointer = s_totalReceivedPayloadSize;
        multisectionStatus = kMultisectionDone;
    }
    
    return (multisectionStatus);
}

/*************************************************************************************************
 * 
 * @brief    Function that is called once an expected answer of a previously sent command has 
 *           been received
 * @param    index Index of the commands waiting for response array, using this index it is
 *           possible to access the data of the question packet.
 * @param    rxCommand Pointer to the received packet 
 * @return   void    
 * 
 **************************************************************************************************/
static void s_expected_command(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand)
{
    uint16_t totalReceivedPayloadSize;
    bool releaseTxCommand = false;
    void (*onAnswerReceivedCallback)(uint8_t) = NULL;
    uint8_t codeOfAck = kIAPAckSuccess;

    if((g_commandsWaitingForResponse[index].commandTxState == kCommandIsWaitingForResponse) || 
            (g_commandsWaitingForResponse[index].commandTxState == (kCommandIsWaitingForResponse |
                    kCommandRetry)))
    {
        /* For the commands that wait for a single response, copy the received payload,
         * release the Tx command and jump to the application callback */
        releaseTxCommand = true;
        totalReceivedPayloadSize = rxCommand->payloadSize;
        onAnswerReceivedCallback = g_commandsWaitingForResponse[index].rxAnswerCallback;
        
        iap_commands_copy_payload(rxCommand->payload, rxCommand->payloadSize,
                                  g_commandsWaitingForResponse[index].pointerToSaveAnswer,
                                  g_commandsWaitingForResponse[index].sizeToSaveAnswer);
        
    }else if(g_commandsWaitingForResponse[index].commandTxState & kCommandRepeatsPeriodically)
    {
        /* For the commands that repeats periodically, copy the received payload
         * and jump to the application callback, (The Tx packet is NOT released) */
        totalReceivedPayloadSize = rxCommand->payloadSize;
        onAnswerReceivedCallback = g_commandsWaitingForResponse[index].rxAnswerCallback;
        
        iap_commands_copy_payload(rxCommand->payload, rxCommand->payloadSize,
                                  g_commandsWaitingForResponse[index].pointerToSaveAnswer,
                                  g_commandsWaitingForResponse[index].sizeToSaveAnswer);
        
    }else if(g_commandsWaitingForResponse[index].commandTxState & kCommandWithMultisectionResponse)
    {
        /* For the commands that receive multiple response using a multisection format,
         * copy the corresponding part of the payload into the application buffer and once 
         * that process is done release the Tx command and jump to the application callback */
        static uint16_t sectionMax = 0; 
        static uint8_t bytesOffset = 0;
        uint16_t sectionCurrent;
        copy_multisection_status_t multisectionStatus;
        
        /* Extract the values of sectCur and sectMax from the payload */
        sectionCurrent = s_get_section_current(rxCommand);
        if(sectionCurrent == 0)
        {
            sectionMax = s_get_section_max(rxCommand, &bytesOffset);
        }
        
        multisectionStatus = iap_commands_copy_multisection_payload(sectionCurrent, sectionMax,
                                                                    bytesOffset, rxCommand->payload,
                                                                    rxCommand->payloadSize,
                                                                    g_commandsWaitingForResponse[index].pointerToSaveAnswer,
                                                                    g_commandsWaitingForResponse[index].sizeToSaveAnswer,
                                                                    &totalReceivedPayloadSize);
        /* Check if the packet has been fully received */ 
        if(multisectionStatus != kMultisectionIncomplete)
        {
            onAnswerReceivedCallback = g_commandsWaitingForResponse[index].rxAnswerCallback;
            releaseTxCommand = true;
            
            if(multisectionStatus == kMultisectionPayloadOverflow)
            {
                codeOfAck = kIAPAckDroppedData;
            }
        }

        /* Update the timestamp to stop the Interface layer from retrying the command */
        iap_interface_update_timeout(g_commandsWaitingForResponse[index].indexOfInterfaceRetryArray, 0);
        
    
    }else if(g_commandsWaitingForResponse[index].commandTxState & kCommandWithMultipleResponses)
    {
        /* The command receives multiple packets as response and the packet will be handled
         *  in other layer and signal when to release the Tx command  */
        
        uint8_t (*onMultipleResponseAnswerReceivedCallback)(uint8_t*, uint16_t);
        
        onMultipleResponseAnswerReceivedCallback = (uint8_t (*)(uint8_t* , uint16_t))
                                                    g_commandsWaitingForResponse[index].rxAnswerCallback;
        
        /* Jump to the special callback */
        releaseTxCommand = onMultipleResponseAnswerReceivedCallback(&rxCommand->payload[0], 
                                                                    rxCommand->payloadSize);
        
        /* Update the timestamp to stop the Interface layer from retrying the command */
        iap_interface_update_timeout(g_commandsWaitingForResponse[index].indexOfInterfaceRetryArray, 0);
    }

    if(releaseTxCommand)
    {
        /* Release the transmitted command using an interface function */
        iap_interface_release_tx_command(g_commandsWaitingForResponse[index].indexOfInterfaceRetryArray);

        /* Free the position in the commands waiting for response array */
        RandomAccessArray_free_position(&g_commandsWaitingForResponseInfo, index);
    }

    /* Release the received command */
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*) rxCommand);

    /* Check if the stored callback is valid, if it is jump there */
    if(onAnswerReceivedCallback != NULL)
    {
        if(g_commandsWaitingForResponse[index].totalPayloadSizePointer != NULL) 
        {
            *(g_commandsWaitingForResponse[index].totalPayloadSizePointer) 
                    = totalReceivedPayloadSize;
        }
        onAnswerReceivedCallback(codeOfAck);
    }
}

/*************************************************************************************************
* 
* @brief    Function that is called once an expected acknowledge of a previously sent command has 
*           been received
* @param    index Index of the commands waiting for response array, using this index it is
*           possible to access the data of the question packet.
* @param    rxCommand Pointer to the received packet 
* @return   void
* 
**************************************************************************************************/
static void s_ack_command(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand)
{
    uint8_t codeOfAck;
    codeOfAck = rxCommand->payload[0];
    
    if(kIAPAckSuccess == codeOfAck)
    {
        s_expected_command(index, rxCommand);
    }else if (kIAPAckCommandPending == codeOfAck)
    {
        uint32_t* timeToWait;
        timeToWait = (uint32_t*) (&rxCommand->payload[2]);
        
#if(BIG_ENDIAN_CORE == _FALSE_)
        BYTESWAP32(*timeToWait, *timeToWait);
#endif
        
        iap_interface_update_timeout(g_commandsWaitingForResponse[index].indexOfInterfaceRetryArray, *timeToWait);

        /* Release the received ack */
        buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*) rxCommand);
    }else
    {
        iap_commands_copy_payload(rxCommand->payload, rxCommand->payloadSize,
                                  g_commandsWaitingForResponse[index].pointerToSaveAnswer,
                                  g_commandsWaitingForResponse[index].sizeToSaveAnswer);

        /* Release the transmitted command using an interface function */
        iap_interface_release_tx_command(g_commandsWaitingForResponse[index].indexOfInterfaceRetryArray);

        /* Free the position in the commands waiting for response array */
        RandomAccessArray_free_position(&g_commandsWaitingForResponseInfo, index);

        /* Release the received command */
        buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*) rxCommand);
        
        if(g_commandsWaitingForResponse[index].rxAnswerCallback != NULL)
        {
           if(!(g_commandsWaitingForResponse[index].commandTxState & kCommandWithMultipleResponses))
           {
               g_commandsWaitingForResponse[index].rxAnswerCallback(codeOfAck);
           }else
           {
               uint8_t (*onMultipleResponseAnswerReceivedCallback)(uint8_t*, uint16_t);
               
               onMultipleResponseAnswerReceivedCallback = (uint8_t (*)(uint8_t* , uint16_t))
                                                           g_commandsWaitingForResponse[index].rxAnswerCallback;
               
               onMultipleResponseAnswerReceivedCallback(NULL, 0);
           }
        }
        
    }
}

/*************************************************************************************************
* 
* @brief    Function that is called if the stack determines that a received packet is an 
*           asynchronous command
* @param    index In this case is not a valid index of commands waiting for response array
*            because the received packet is an asynchronous command
* @param    rxCommand Pointer to the received packet 
* @return   void
* 
**************************************************************************************************/
static void s_async_command(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand)
{
    uint8_t indexToJumpAsynchronousCommandFunction;
    /* The received command is an async commands therefore it does not have an
     *  element in the commands waiting for response array, so the index is not used */
    (void) index;
    /* search index to go to the function of received asynchronous command */
    indexToJumpAsynchronousCommandFunction = s_iap_commands_search_async_command(rxCommand->lingoId,
                                                                                 rxCommand->commandId);
    if (kCommandError != indexToJumpAsynchronousCommandFunction)
    {
        /* Go to the function of received asynchronous command */
        s_kAsynchronousCommandsFunctionsByLingos
        [rxCommand->lingoId][indexToJumpAsynchronousCommandFunction](rxCommand);
    }else
    {
        buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*) rxCommand);
    }
}

/*************************************************************************************************
* 
* @brief    Function that is called if the stack determines that a received packet is an
*            supported command but not is an asynchronous neither is an expected or ack answer
*            into array of sent commands
* @param    index In this case is not a valid index of commands waiting for response array
*            because the received packet is an asynchronous command
* @param    rxCommand Pointer to the received packet 
* @return   void
* 
**************************************************************************************************/
static inline void s_unexpected_error_command(uint8_t index, iap_interface_buffer_rx_frame_t* rxCommand)
{
    /* The received command is an lost commands therefore it does not have an
     *  element in the commands waiting for response array, so the index is not used */
    (void) index;
    buffersAlloc_releaseSpace(kIapiReceptionBufferIndex, (uint8_t*) rxCommand);
}

/*************************************************************************************************
* 
 * @brief   Searches for an asynchronous commands within arrays containing the possible async.
 *          commands of each lingo
 * @details The search is done with a successive approximation algorithm to reduce the number 
 *          of iterations 
 * @param   lingo     Lingo of the command that wants find an expected answer
 * @param   command   Command that expect to find an expected answer
 * @return  If found the command requested, returns the value of expected answer as a command
 * @retval  kCommandError    If doesn't found the command requested, returns an error value.
 * 
**************************************************************************************************/
static uint8_t s_iap_commands_search_async_command(uint8_t lingo, uint8_t command)
{
    int8_t bottomValue;
    int8_t centreValue;
    int8_t topValue;
    uint8_t returnIndex;

    returnIndex = kCommandError;
    bottomValue = 0;
    if ((uint8_t)(0) != s_kSizeOfLingosWithAsyncCommands[lingo])
    {
        topValue = (s_kSizeOfLingosWithAsyncCommands[lingo] - 1);
        while(bottomValue <= topValue)
        {
            /* Set new centre */
            centreValue = ((bottomValue + topValue) / 2);
            /* Determine in what sublist is the element */
            if(s_kAsyncrhonousCommandsByLingos[lingo][centreValue] == command)
            {
                /* Command found */
                //s_kAsynchronousCommandsFunctionsByLingos[lingo][centreValue];
                returnIndex = centreValue;
                return returnIndex;
            }
            else if(s_kAsyncrhonousCommandsByLingos[lingo][centreValue] > command)
            {
                /* Search into left sublist */
                /* Set limit of new list */
                topValue    = (centreValue - 1);
            }
            else
            {
                /* Search into right sublist */
                /* Set limit of new list */
                bottomValue = (centreValue + 1);
            }
        }
    }
    return returnIndex;
}

/*************************************************************************************************
* 
* @brief    Function that compares the TID (if enabled), in order to determine whether the received
*           packet is an expected answer, an expected acknowledge or an asynchronous command 
* @param    commandWaitingForResponse One of the elements of the commands waiting for response array
* @param    receivedCommand Pointer to the received packet 
* @return   The type of received command (an expected answer, an expected ack. or an async. command)
* @retval   kExpectedCommand The received command is a response to a previously sent command 
* @retval   kAckCommand The received command is an acknowledge to a previously sent command
* @retval   kAsynchronousCommand The received command is an async. command sent by the Apple device
* @sa       iap_commands_receive_command
* 
**************************************************************************************************/
static received_command_flags_t s_iap_commands_check_command_type(
                                        command_waiting_for_response_t* commandWaitingForResponse,
                                        iap_interface_buffer_rx_frame_t* receivedCommand)
{
    received_command_flags_t rxCommandType = kEmptyArray;
    uint16_t transmittedTID;

    /* Get TID of command sent */
    if(IAPI_CHECK_TRANSACTION_ID_ENABLED) 
    {
        uint8_t* pointerToTxCommand;
        uint8_t transactionIDIndex;
        pointerToTxCommand = iap_interface_get_tx_command_from_index
                (commandWaitingForResponse->indexOfInterfaceRetryArray);
        transactionIDIndex = iap_interface_get_tid_position(pointerToTxCommand);
        transmittedTID =  ((uint16_t)(pointerToTxCommand[transactionIDIndex]) << 8);
        transmittedTID |=  (uint16_t)(pointerToTxCommand[transactionIDIndex + 1]);
    }

    if(receivedCommand->lingoId == commandWaitingForResponse->expectedLingo)
    {
        if((receivedCommand->commandId == commandWaitingForResponse->expectedCommand)
                && (commandWaitingForResponse->expectedCommand != commandWaitingForResponse->expectedAck))
        {
            if(IAPI_CHECK_TRANSACTION_ID_ENABLED)
            {
                if(receivedCommand->transactionId == transmittedTID)
                {
                    rxCommandType = kExpectedCommandFlag;
                }
                else
                {
                    if(commandWaitingForResponse->commandTxState & (kCommandRetry | kCommandRepeatsPeriodically))
                    {
                        rxCommandType = kExpectedCommandFlag;
                    }
                    else
                    {
                        rxCommandType = kCommandTIDNotFoundFlag;
                    }
                }
            }
            else
            {
                rxCommandType = kExpectedCommandFlag;
            }
        }
        else
        {
            if(receivedCommand->commandId == commandWaitingForResponse->expectedAck)
            {
                if(IAPI_CHECK_TRANSACTION_ID_ENABLED)
                {
                    if(receivedCommand->transactionId == transmittedTID)
                    {
                        rxCommandType = kAckCommandFlag;
                    }
                    else
                    {
                        /* define if command received is from classic device */
                        /* Per section 2.6.1.2 support of transaction IDs 
                         * shall be disabled upon receiving a General
                         * Lingo iPod Ack with payload size of 2, 
                         * frame length of 4 (Lingo, Cmd, Parameter, CmdReplied */
                        if ((kGeneralLingo == receivedCommand->lingoId) &&
                            (kGeneralLingoIpodAck == receivedCommand->commandId) &&
                            (4 == receivedCommand->frameLength) &&
                            ((receivedCommand->transactionId & 0xFF) == kGeneralLingoStartIDPS))
                        {
                            /* if this is the first time that a command 
                             * without TID was received, adjust payload */
                            IAPI_DISABLE_TRANSACTION_ID;
                            receivedCommand->payload[0]
                                                     = receivedCommand->transactionId >> 8;
                            receivedCommand->payload[1]
                                                     = receivedCommand->transactionId & 0xFF;
                            receivedCommand->payloadSize = 2;
                            rxCommandType = kAckCommandFlag;
                        }
                        else
                        {
                            if(commandWaitingForResponse->commandTxState & (kCommandRetry | kCommandRepeatsPeriodically))
                            {
                                rxCommandType = kAckCommandFlag;
                            }
                            else
                            {
                                rxCommandType = kAckTIDNotFoundFlag;
                            }
                        }
                    }
                }
                else
                {
                    rxCommandType = kAckCommandFlag;
                }
            }
            else
            {
                rxCommandType = kCommandNotFoundFlag;
            }
        }
    }
    else
    {
        rxCommandType = kLingoNotFoundFlag;
    }
    return rxCommandType;
}
/*************************************************************************************************
* 
* @brief    Function that is called if a command has not received response even after it has been 
*           retried 'IAPI_MAX_RETRY_INTENTS' times (See iAP_Interface.h) 
* @param    retryCommandsArrayIndex The index of the Interface retry commands array  
* @return   void
* @sa       s_iap_interface_check_retry_commands_array (In iAP_Interface)
* 
**************************************************************************************************/
static void s_iap_commands_no_response_error_callback(uint8_t retryCommandsArrayIndex)
{
    uint8_t searchIndex = 0;
    uint8_t foundIndex = kNotAvailableIndex;
    void (*onAnswerReceivedCallback)(uint8_t) = NULL;
    uint8_t codeOfAck = kIAPAckOperationTimeout;
    
    /* If a command has been retried 'IAPI_MAX_RETRY_INTENTS' times first look for 
     * the corresponding command in the commands waiting for response array*/
    do
    {
        if(IS_POSITION_RESERVED(g_commandsWaitingForResponseInfo, searchIndex))
        {
            /* If the index stored in the commands waiting for response array matches the index of 
             * the interface retry commands array received as parameter, this means we have found 
             * the command that never received a response */
            if(g_commandsWaitingForResponse[searchIndex].indexOfInterfaceRetryArray 
                    == retryCommandsArrayIndex)
            {
                /* Assign this variable in order to break the loop */
                foundIndex = searchIndex;
                
                if(!(g_commandsWaitingForResponse[foundIndex].commandTxState &
                        kCommandWithMultipleResponses))
                {
                    onAnswerReceivedCallback = g_commandsWaitingForResponse[foundIndex].rxAnswerCallback;
                }
                
                /* Free the position from the commands waiting for response array*/
                RandomAccessArray_free_position(&g_commandsWaitingForResponseInfo, foundIndex);
            }
        }
        searchIndex++;
    }while(searchIndex < (IAP_CMDS_NUM_COMMANDS_WAITING_FOR_RESPONSE) 
            && (kNotAvailableIndex == foundIndex));

    if(onAnswerReceivedCallback != NULL)
    {
        onAnswerReceivedCallback(codeOfAck);
    }
}

/*************************************************************************************************
* 
* @brief    Function that gets the sectCurr value for specific commands 
* @details  As a general fact the sectCur field is in different locations of the payload. So each 
*           command that uses this multi-section field has it in a different positions of it's payload.
*           Supported commands that implement the sectCur multi-section field:
*               Extended Interface lingo (0x04) RetArtworkData (0x004F)
*               General lingo (0x00) RetiPodAuthenticationInfo (0x0011)
*               Simple Remote lingo (0x02) RetCurrentVoiceOverItemProperty (0x18
* @param    currentRxFrame Pointer to the received packet 
* @return   The value of the 'sectCurr' field extracted from the command's payload

**************************************************************************************************/
static uint16_t s_get_section_current(iap_interface_buffer_rx_frame_t* currentRxFrame)
{
    uint16_t sectionCurrent;
    if((currentRxFrame->lingoId == kExtendedInterfaceLingo) && 
            ((currentRxFrame->commandId == kExtendedInterfaceRetArtworkData) ||
                    (currentRxFrame->commandId == kExtendedInterfaceRetTrackArtworkData)))
    {
        sectionCurrent = (((uint16_t) currentRxFrame->payload[0]) << 8) | 
                ((uint16_t) currentRxFrame->payload[1]);
    }else if((currentRxFrame->lingoId == kGeneralLingo) && 
            (currentRxFrame->commandId == kGeneralLingoRetIpodAuthenticationInfo))
    {
        sectionCurrent = (uint16_t) currentRxFrame->payload[2];
    }else if((currentRxFrame->lingoId == kSimpleRemoteLingo) && 
             (currentRxFrame->commandId == kSimpleRemoteRetCurrentVoiceOverItemProperty))
    {
        sectionCurrent = (((uint16_t) currentRxFrame->payload[1]) << 8) | 
                          ((uint16_t) currentRxFrame->payload[2]);
    }

    return (sectionCurrent);
}

/*************************************************************************************************
* 
* @brief    Function that gets the sectMax value for specific commands. 
* @details  As a general fact the sectMax field is in different locations of the payload. So each 
*           command that uses this multi-section field has it in a different positions of it's payload.
*           Supported commands that implement the sectMax multi-section field:
*               Extended Interface lingo (0x04) RetArtworkData (0x004F)
*               General lingo (0x00) RetiPodAuthenticationInfo (0x0011)
*               Simple Remote lingo (0x02) RetCurrentVoiceOverItemProperty (0x18)      
*           Special case:
*               Extended Interface lingo (0x04) RetTrackArtworkData (0x0011) this command does
*               not have the sectMax field in its payload, but it has other field that can be used 
*               to the determine the number of maximum packets that will be received
* @param    currentRxFrame Pointer to the received packet 
* @param    bytesOffset return value of the number of bytes of the payload that must be skipped to
*           get to the actual data of the packet
* @return   The value of the 'sectMax' field extracted from the command's payload

**************************************************************************************************/
static uint16_t s_get_section_max(iap_interface_buffer_rx_frame_t* currentRxFrame,
                                  uint8_t* bytesOffset)
{
    uint16_t sectionMax;

    if((currentRxFrame->lingoId == kExtendedInterfaceLingo) && 
            (currentRxFrame->commandId == kExtendedInterfaceRetArtworkData))
    {
        sectionMax = (((uint16_t) currentRxFrame->payload[2]) << 8) |
                ((uint16_t) currentRxFrame->payload[3]);

        /* Considering sectCur (2-bytes) and sectMax (2-bytes) */
        *bytesOffset = kFourBytesOffset;
    }else if((currentRxFrame->lingoId == kExtendedInterfaceLingo) && 
            (currentRxFrame->commandId == kExtendedInterfaceRetTrackArtworkData))
    {
        uint8_t displayPixelFormatCode = currentRxFrame->payload[2];
        uint16_t imageWidth  = (((uint16_t) currentRxFrame->payload[3]) << 8) | 
                (uint16_t) currentRxFrame->payload[4];

        uint16_t imageHeight = (((uint16_t) currentRxFrame->payload[5]) << 8) | 
                (uint16_t) currentRxFrame->payload[6];

        sectionMax = (imageWidth*imageHeight*displayPixelFormatCode)/
                (currentRxFrame->frameLength - IAPI_FRAME_RECEIVED_BYTES);

        /* Considering  only sectCur (2-bytes)*/
        *bytesOffset = kTwoBytesOffset;
    }else if((currentRxFrame->lingoId == kGeneralLingo) && 
            (currentRxFrame->commandId == kGeneralLingoRetIpodAuthenticationInfo))
    {
        sectionMax = (uint16_t) currentRxFrame->payload[3];

        /* Considering sectCur (1-byte), sectMax (1-byte), MinorVersion (1-byte) and
         *  MajorVersion (1-byte) */
        *bytesOffset = kFourBytesOffset;
    }else if((currentRxFrame->lingoId == kSimpleRemoteLingo) && 
             (currentRxFrame->commandId == kSimpleRemoteRetCurrentVoiceOverItemProperty))
    {
        sectionMax = (((uint16_t) currentRxFrame->payload[3]) << 8) |
                ((uint16_t) currentRxFrame->payload[4]);

        /* Considering sectCur (2-bytes) and sectMax (2-bytes) */
        *bytesOffset = kFiveBytesOffset;
    }

    return (sectionMax);
}

/*************************************************************************************************
* EOF
*************************************************************************************************/

