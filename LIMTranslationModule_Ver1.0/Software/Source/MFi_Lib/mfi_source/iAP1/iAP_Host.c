/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "osa_common.h"
#include "mfi_cfg.h"
#include "MFi_Config.h"
#include "iAP_Host.h"
#include "GeneralLingo.h"
#include "DigitalAudioLingo.h"
#if defined(IAPI_INTERFACE_USB_HOST)
#include "iAP_USBHost.h"
#endif
#if defined(IAPI_INTERFACE_USB_DEV)
#include "iAP_USBDevice.h"
#endif

/*************************************************************************************************/
/*                                   Defines & Macros Section                                    */
/*************************************************************************************************/
#define MAX_FID_TOKEN_RETRY       2
#define HOST_AUTH_BUFFER_SIZE   IAPI_MAX_RX_PAYLOAD_SIZE

/*************************************************************************************************/
/*                                       Typedef Section                                         */
/*************************************************************************************************/
/*! type used to create a finite state machine with different states to move depending on the code
 * status and events */
typedef struct
{
    uint8_t previousState;
    uint8_t actualState;
    uint8_t nextState;
    uint8_t timeoutState;
}iap_host_state_machine_t;

/*************************************************************************************************/
/*                                  Function Prototypes Section                                  */
/*************************************************************************************************/

/* CoProcessor States */
static void s_iap_host_coprocessor_stabilization_state                  (void);
static void s_iap_host_read_prot_ver_dev_id_state                       (void);
static void s_iap_host_wait_coprocessor_state                           (void);
static void s_iap_host_wait_coprocessor_error_state                     (void);

/* Host States */
static void s_iap_host_wait_state                                       (void);
static void s_iap_host_idle_state                                       (void);
static void s_iap_host_hot_plug_debounce_state                          (void);
static void s_iap_host_send_sync_byte_state                             (void);
static void s_iap_host_start_idps_state                                 (void);
static void s_iap_host_start_identify_device_lingoes_state              (void);
static void s_iap_host_continue_identify_device_lingoes_state           (void);
static void s_iap_host_identify_device_lingoes_delay_state              (void);
static void s_iap_host_request_max_payload_size_state                   (void);
static void s_iap_host_get_ipod_options_for_lingo_state                 (void);
static void s_iap_host_set_fid_token_values_state                       (void);
static void s_iap_host_end_idps_state                                   (void);
static void s_iap_host_accessory_idps_auth_error_state                  (void);
static void s_iap_host_essential_routines_done_state                    (void);
static void s_iap_host_ready_state                                      (void);

/* Callback routines */
static void s_iap_host_ack_callback                                     (uint8_t codeOfAck);
static void s_iap_host_idps_not_supported_callback                      (uint8_t codeOfAck);
static void s_iap_host_return_max_payload_callback                      (uint8_t codeOfAck);
static void s_iap_host_get_ipod_options_for_lingo_callback              (uint8_t codeOfAck);
static void s_iap_host_set_fid_token_values_callback                    (uint8_t codeOfAck);
static void s_iap_host_end_idps_callback                                (uint8_t codeOfAck);
static void s_iap_host_ack_authentication_info_callback                 (uint8_t codeOfAck);
static void s_iap_host_ack_authentication_signature_callback            (uint8_t codeOfAck);
static void s_iap_host_send_acc_certificate_ack_callback                (uint8_t codeOfAck);
#if(IAP_IOS_AUTHENTICATION_ENABLE)
static inline void s_iap_host_ret_ipod_authentication_info_callback     (uint8_t codeOfAck);
static inline void s_iap_host_ret_ipod_authentication_signature_callback(uint8_t codeOfAck);
#endif

/* Digital Audio Lingo Callbacks */
static void iap_host_track_new_audio_attributes(iap_interface_buffer_rx_frame_t* rxFrame);

/* Accessory Authentication states */
static void s_iap_host_send_accessory_signature_state                   (void);
static void s_iap_host_read_accessory_signature_state                   (void);
static void s_iap_host_send_accessory_certificate_state                 (void);
static void s_iap_host_read_acc_certificate_state                       (void);

/* Apple device Authentication states */
#if(IAP_IOS_AUTHENTICATION_ENABLE)
static void s_iap_host_get_ipod_auth_info_state                         (void);
static void s_iap_host_validate_iOS_certificate_state                   (void);
static void s_iap_host_send_ack_ipod_auth_info_state                    (void);
static void s_iap_host_create_iOS_challenge_state                       (void);
static void s_iap_host_send_iOS_challenge_state                         (void);
static void s_iap_host_check_iOS_signature_state                        (void);
static void s_iap_host_ack_iOS_signature_state                          (void);
static void s_iap_host_iOS_authentication_error_state                   (void);
#endif

/* ASYNC COMMANDS callbacks************************************************************************/
static inline void s_iap_host_get_accessory_authentication_info_callback        
                (iap_interface_buffer_rx_frame_t* rxFrame);
static inline void s_iap_host_get_accessory_authentication_signature_callback   
                (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_host_request_identify_callback                         
                (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_host_notify_ipod_state_change_callback                 
                (iap_interface_buffer_rx_frame_t* rxFrame);
static void s_iap_host_get_accessory_info_callback                 
                (iap_interface_buffer_rx_frame_t* rxFrame);

/* END ASYNC COMMANDS ***************************************************************************/
/*************************************************************************************************/
/*                                   Global Variables Section                                    */
/*************************************************************************************************/

/*! Variable to save the address of the authentication buffer */
uint8_t*  g_iapHostAuthCertificateBuff;
/*! Variable to save the size of the authentication buffer for acc and ios */
uint16_t  g_iapHostAuthCertificateBuffSize;
/*! Buffer to save the authentication certificate */
uint8_t   g_iapHostAuthCertificateRxBuff[HOST_AUTH_BUFFER_SIZE];
/*! Buffer size ot the authentication certificate */
uint16_t  g_iapHostAuthCertificateRxBuffSize = HOST_AUTH_BUFFER_SIZE;
/*! Host Status variable */
uint16_t  g_iapHostStatus;
/*! Host Internal Error Flags */
uint16_t  g_iapHostInternalErrorFlags;
/*! Host User Application Error Flags */
uint16_t g_iapHostUserAppErrorFlags;
/*! Variable to count the number of lingoes */
uint8_t g_iapHostLingoNumber = 0;
/*! Generic buffer to save the IDPS answer process */
uint8_t g_iapHostGenericReceptionBuffer[IAPI_MAX_RX_PAYLOAD_SIZE];
/*! Flag to indicate the arrival of new frequency */
bool g_newFrequencyReceived = false;
/*! This variable saves the last new frequency received */
uint16_t g_lastReceivedFrequency = 0;
/*! Structure to move the Host state machine */
iap_host_state_machine_t IapHostStateMachine;
/*! Variable that contains the enabled FID tokens */
uint32_t g_enabledFIDTokens = IAP_CMD_ENABLED_FID_TOKENS_MASK;
/*! Variable that contains the status of the IDPS process received by the IDPSStatus (0x3C) command  */
iap_gl_idps_status_t iAPH_gEndIDPSStatus;
/*************************************************************************************************/
/*                                   Static Variables Section                                    */
/*************************************************************************************************/
/*! Variable to keep track the number of times to retry acc. authentication */
static uint8_t s_iapHostAccessoryAuthRetriesCounter;
/*! Variable to save the signature authentication size */
static uint16_t s_iapHostSignatureSize;
static iap_gl_identify_device_lingoes_t s_IapHostIdentifyLingoes; 
/*! Variable to count the number of FID tokens to be sent */
static uint8_t s_iapHostTokenNumber = 0;
/*! Auxiliar variable to identify the multipacket transfer in retAccessoryAuthenticationInfo */
static uint8_t s_iapHostAccessoryAuthInfoPacketCounter = 0;
/*! This callback function is called when authentication process is finished so the user may
 * be able to send commands */
static void (*s_iapHostDeviceReadyCallback)(void) = NULL;
/*! This callback saves the address of the funciton application function whenever an ipod 
 * disconection has been detected */
static void (*s_iapHostDeviceDisconnectedCallback)(void) = NULL;
/*! This callback saves the address of the function to detect a change of the sample frequency 
 * in digital audio driver */
static void (*s_iapHostNewFrequencyAppCallback)(uint16_t newFrequency) = NULL;
/*! Variable to identify the default video signal format */
static uint8_t s_iapHostDefaultVideoSignalFormat = DEFAULT_VIDEO_SIGNAL_FORMAT;
/*! Variable to specify the default video out connection format */
static uint8_t s_iapHostDefaultVideoOutConnection = kVideoOutConnectionNone;
/*! Variable to specify the default video aspect ratio */
static uint8_t s_iapHostDefaultVideoAspectRatio = DEFAULT_VIDEO_ASPECT_RATIO;

/*! Variable to keep track the number of times to retry iOS. authentication */
static uint8_t s_iapHostiOSAuthRetriesCounter;
/*! Variable to keep track the number of times to retry iOS signature */
static uint8_t  s_iapHostiOSSignatureRetry;
/*! Variable to save the size of iOS Authentication information returned by
 *  the command RetiPodAuthenticationInfo */
static uint16_t s_iapHostCPCertificateSize;
/*! Variable the status of the certificate sent by the Apple device */
static uint8_t  s_iapHostCertificateStatus;

uint8_t iAP_Auth_gbaCP_VersionAndID[6];

static OsaCondition cpEvent;
static OsaMutex cpMutex;
static uint32_t cpFlag;

static bool idpsNotSupportedFirst;
/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/
/*! This constant saves the max reception payload size */
const uint16_t g_iapHostGenericReceptionBufferSize = IAPI_MAX_RX_PAYLOAD_SIZE;

/*************************************************************************************************/
/*                                   Static Constants Section                                    */
/*************************************************************************************************/
/*! Transmission payload when finishing idps */
#if(IAP_ACC_AUTHENTICATION_ENABLE)
    const uint8_t iAPH_gbAuthenticationFlag = kAccessoryEndsIdpsAndStartAuth;
#else
    const uint8_t iAPH_gbAuthenticationFlag = kAccessoryAbandonedIdps;
#endif

/*! This function pointers array saves the host function states */
static void (*const iAP_Host_vfnapHostDriver[])(void) = 
{
    s_iap_host_coprocessor_stabilization_state,
    s_iap_host_read_prot_ver_dev_id_state,
    s_iap_host_wait_coprocessor_state,
    s_iap_host_wait_coprocessor_error_state,
    s_iap_host_idle_state,
    s_iap_host_hot_plug_debounce_state,
    s_iap_host_send_sync_byte_state,
    s_iap_host_start_idps_state,
    s_iap_host_start_identify_device_lingoes_state,
    s_iap_host_continue_identify_device_lingoes_state,
    s_iap_host_identify_device_lingoes_delay_state,
    s_iap_host_request_max_payload_size_state,
    s_iap_host_get_ipod_options_for_lingo_state,
    s_iap_host_set_fid_token_values_state,
    s_iap_host_end_idps_state,
    s_iap_host_accessory_idps_auth_error_state,
    s_iap_host_read_acc_certificate_state,
    s_iap_host_send_accessory_certificate_state,
    s_iap_host_read_accessory_signature_state,
    s_iap_host_send_accessory_signature_state,
#if(IAP_IOS_AUTHENTICATION_ENABLE)
    s_iap_host_get_ipod_auth_info_state,
    s_iap_host_validate_iOS_certificate_state,
    s_iap_host_send_ack_ipod_auth_info_state,
    s_iap_host_create_iOS_challenge_state,
    s_iap_host_send_iOS_challenge_state,
    s_iap_host_check_iOS_signature_state,
    s_iap_host_ack_iOS_signature_state,
    s_iap_host_iOS_authentication_error_state,
#endif
    s_iap_host_essential_routines_done_state,
    s_iap_host_ready_state,
    s_iap_host_wait_state
};

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

/* Read iAP_Host.h file for function documentation */
void iap_host_initialization(void)
{
    dprint("iAP_Host_vfnHostInitialization start\n");

    osa_mutex_create(&cpMutex, false);
    osa_cond_create(&cpEvent);
    cpFlag = 0;
    idpsNotSupportedFirst = true;

    /* Initialize the transport layer */
#ifndef MFI_IAP2
    iAP_Transport_Init();
#endif

    /* Initialize commands layer */
    iap_commands_initialization(); 

    /* Clear all Host Status flags */
    g_iapHostStatus = 0u;
    /* Clear all internal error Flags*/
    g_iapHostInternalErrorFlags = 0u;
    /* Clear all User Application error Flags*/
    g_iapHostUserAppErrorFlags = 0u;
    /* Register host callbacks for asynchronous commands */
    set_gl_asynchronous_command_callback(kGeneralLingoRequestIdentify,
                                         s_iap_host_request_identify_callback);
    set_gl_asynchronous_command_callback(kGeneralLingoGetAccessoryAuthenticationInfo,
                                         s_iap_host_get_accessory_authentication_info_callback);
    set_gl_asynchronous_command_callback(kGeneralLingoGetAccessoryAuthenticationSignature,
                                         s_iap_host_get_accessory_authentication_signature_callback);
    set_gl_asynchronous_command_callback(kGeneralLingoNotifyIpodStateChange,
                                         s_iap_host_notify_ipod_state_change_callback);
    set_gl_asynchronous_command_callback(kGeneralLingoGetAccessoryInfo,
                                         s_iap_host_get_accessory_info_callback);

    set_dal_asynchronous_command_callback(kDigitalAudioTrackNewAudioAttributes, 
                                          iap_host_track_new_audio_attributes);

    /* Set Initial State */
#if(IAP_ACC_AUTHENTICATION_ENABLE)
    /* Set first a small delay to then start reading co-processor version and ID */
    IapHostStateMachine.actualState = kHostCoprocessorStabilizationState;
#else
    IapHostStateMachine.actualState = kHostIdleState;
#endif
}

/* Read iAP_Host.h file for function documentation */
void iap_host_uninitialization(void)
{
    dprint("iAP_Host_uninitialization start\n");
    
    osa_cond_destroy(&cpEvent);
    osa_mutex_destroy(&cpMutex);
    iap_commands_destroy_mutex();

    /* Power goes low, Stop iAP commands */
    IAP_HOST_CLEAR_ACC_DETECT_PIN;
       
    /* Reset commands layer */
    iap_commands_reset(); 

    /* Reset the TID */
    iap_interface_reset_transaction_id();
    
    /* Clear all Host Status flags */
    g_iapHostStatus = 0u;
    /* Clear all internal error Flags*/
    g_iapHostInternalErrorFlags = 0u;
    /* Clear all User Application error Flags*/
    g_iapHostUserAppErrorFlags = 0u;
    
    IapHostStateMachine.actualState = kHostIdleState;
    
    dprint("iAP_Host_uninitialization end\n");
}

/************************************************************************************************/
/*                         Coprocessor related functions and states                             */
/************************************************************************************************/
/*************************************************************************************************
*
* @brief    State to wait for  the coprocessor timer to expire. Used to create a small delay
*           when starting to use co-processor functions.
* @return   void
*          
**************************************************************************************************/
static void s_iap_host_coprocessor_stabilization_state(void)
{
    dprint("iAP_Host_vfnCoprocessor_Stabilization_State start\n");

    osa_time_delay(10);

    /* Set the host states */
    IapHostStateMachine.previousState = IapHostStateMachine.actualState;
    IapHostStateMachine.actualState = kHostReadProtVerDevIdState;
}

/*************************************************************************************************
*
* @brief    Function that sets the states and provides a single API to read the Protocol Version 
*           and Device ID
* @return      void
*          
**************************************************************************************************/
static void s_iap_host_read_prot_ver_dev_id_state(void)
{
    dprint("iAP_Host_vfnReadProtVer_DevID_State start\n");

    /*read protocol version and device id registers in burst mode*/
    iAP_host_read_coprocessor_ver_and_ID(iAP_Auth_gbaCP_VersionAndID);

    /* Set host states */
    IapHostStateMachine.previousState = IapHostStateMachine.actualState;    
    IapHostStateMachine.actualState   = kHostWaitCoprocessorState;
    IapHostStateMachine.nextState     = kHostIdleState;
    IapHostStateMachine.timeoutState  = kHostWaitCoprocessorErrorState;
}

/*************************************************************************************************
*
* @brief    State to wait for the co-processor to finish its activity and then jump to the next
* state in the state machine
* @return   void
*          
**************************************************************************************************/
static void s_iap_host_wait_coprocessor_state(void)
{
    int ret = 0;

    dprint("iAP_Host_vfnWaitCoprocessor_State start\n");

    osa_mutex_lock(&cpMutex);
    while (!cpFlag) {
        ret = osa_cond_wait(&cpEvent, &cpMutex, 5000);
        if (ret == ERRCODE_TIMED_OUT)
            break;
    }
    cpFlag = 0;
    osa_mutex_unlock(&cpMutex);

    if (ret == 0) {
        dprint("  iAP_Host_vfnWaitCoprocessor_State success\n");
        printf(" Subbu -  iAP_Host_vfnWaitCoprocessor_State success\n");
        IapHostStateMachine.actualState = IapHostStateMachine.nextState;
    } else if (ret == ERRCODE_TIMED_OUT) {
        dprint("  iAP_Host_vfnWaitCoprocessor_State timed out\n");
        print("Subbu-  iAP_Host_vfnWaitCoprocessor_State timed out\n");
        g_iapHostStatus |= (1 << kHostInternalError);
        g_iapHostInternalErrorFlags |= (1 << kHostI2CHalError);
        IapHostStateMachine.actualState = IapHostStateMachine.timeoutState;
    } else {
        dprint("  iAP_Host_vfnWaitCoprocessor_State error\n");
        dprint(" Subbu- iAP_Host_vfnWaitCoprocessor_State error\n");
        IapHostStateMachine.actualState = kHostWaitCoprocessorErrorState;
    }
}

/*************************************************************************************************
*
* @brief    State to give more time to the co-processor in case of a timeout error
* @return   void
*          
**************************************************************************************************/
static void s_iap_host_wait_coprocessor_error_state     (void)
{
    osa_time_delay(10);

    IapHostStateMachine.actualState = IapHostStateMachine.previousState;
    IapHostStateMachine.nextState = kHostWaitCoprocessorState;
}


/************************************************************************************************/
/*                         Detection and identification functions                               */
/************************************************************************************************/

/*************************************************************************************************
*
* @brief    This function is intentionally empty. It is used when the protocol is waiting for a 
*           response and there is nothing else to do in between.   
* @return   void
* @warning  Callback routines must take care of changing the host state machine, otherwise it will 
*           get stuck in this state!
* 
**************************************************************************************************/
static void s_iap_host_wait_state(void)
{
    /* Verify if there are pending commands in reception queue */
    iap_commands_receive_command();
    
    if (!(IAP_HOST_IDEVICE_CONNECTED && IAP_HOST_IDEVICE_POWER))
    {
        dprint("  iap_host_wait_state calling IOS_Disconnection\n");
        /* @todo Check that there is no hibernate flag */
        iap_host_handle_ios_disconnection();
    }
}

/*************************************************************************************************
*
* @brief    State used after initializing the stack and before an iPod is connected
* @return   void
* @details  This state is waiting for an iAP1 device connection. When a device is plugged, the
*           different states are executed to send sync bytes, go through IDPS or IDL as needed,
*           authenticate accessory and iOS device (if needed) and set a ready state when all
*           previous steps were Ok. When a device is unplugged, host has to return here. 
**************************************************************************************************/
static void s_iap_host_idle_state(void)
{
    dprint("iAP_Host_vfnIdle_State start\n");
        /*! @todo RP - How is this flag managed prior to connection? Can it be 1? */
        if(!(g_iapHostStatus &(1 << kHostIosDetected)))
        {
            /* iOS detected on pin, start IDPS process */
            if(IAP_HOST_IDEVICE_CONNECTED && IAP_HOST_IDEVICE_POWER)
            {
                IapHostStateMachine.actualState = kHostHotPlugDebouncesState;
            }
        }
}

/*************************************************************************************************
*
* @brief    State used to create a small delay with debouncing purposes once an iPod is detected. 
* @return      void
* @details  Once the timer Flag is 0 it means that the timeout has expired and it is OK to start 
*           the communication with the iPod.
*          
**************************************************************************************************/
static void s_iap_host_hot_plug_debounce_state(void)
{
    dprint("iAP_Host_vfnHotPlugDebounce_State start\n");   

    osa_time_delay(200);

        /* if an iAP1 device was connected and is still detected after a stabilization time out */
        if(IAP_HOST_IDEVICE_CONNECTED && IAP_HOST_IDEVICE_POWER)
        {
            /* Indicate Accessory is connected */
            IAP_HOST_SET_ACC_DETECT_PIN;
            /* Clear previous error flags and set that the iOS device is detected OK */
            g_iapHostInternalErrorFlags = 0;
            g_iapHostStatus |= (1 << kHostIosDetected);
            /* Restart Accessory Authentication Retries Counter */
            s_iapHostAccessoryAuthRetriesCounter = 0;
            IapHostStateMachine.actualState = kHostSendSyncByteState;
        }
        else
        {
            /* iOS device is not present, false alarm, return to initial state */
            g_iapHostStatus = 0u;
            g_iapHostInternalErrorFlags = 0u;
            IapHostStateMachine.actualState = kHostIdleState;

        }
    
}

/*************************************************************************************************
*
* @brief    State to create a delay before sending the sync byte. This state is mainly used to
*             generate a timeout before sending the sync byte when the iPod goes to low power.
* @return      void
*          
**************************************************************************************************/
static void s_iap_host_send_sync_byte_state(void)
{
    static uint8_t syncFlag = 0;

    dprint("iAP_Host_vfnSendSyncByte_State start\n");

    osa_time_delay(100);

        syncFlag++;
        /* Number 0x03 is hard coded because this is the amount of times that the timer has to expire
         * in this function to send two sync bytes and jump to the next state */
        if(0x03 == syncFlag)
        {
            IAPI_CLEAR_TX_SYNC_BYTE_FLAG;
            syncFlag = 0; 
            if(!(g_iapHostStatus & (1 << kHostIosInLowPowerMode)))
            {
                IapHostStateMachine.actualState = kHostStartIdpsState;
            }
            else
            {
                /* Device was already connected and went to low-power mode, no need to run IDPS again */
                g_iapHostStatus &= ~(1 << kHostIosInLowPowerMode);
                IapHostStateMachine.actualState = kHostReadyState; 
            }
    }
    else
    {
        IAP_HOST_SEND_SYNC_BYTE;
        osa_time_delay(30);
    }
}

/*************************************************************************************************
*
* @brief    Initial state before starting the communication with any iPod. The Start Identify Device
*             Preference and settings command is the initial step for any communication. 
* @return      void
* 
**************************************************************************************************/
static void s_iap_host_start_idps_state(void)
{
    dprint("iap_host_start_idps_state start\n");

    command_transmission_status_t currentCommandTxStatus;
    /* Clear all internal error Flags*/
    g_iapHostInternalErrorFlags = 0u;
    /* Clear all User Application error Flags*/
    g_iapHostUserAppErrorFlags = 0u;

#if(ACC_INFO_STATUS_TOKEN_MASK)
    g_iapCommandsAccStatusNotificationMask = 0;
#endif

    IAPI_ENABLE_TRANSACTION_ID;
    g_iapHostStatus &= ~(1<<kHostIdpsDone);
    /* Send start IDPS command */
    currentCommandTxStatus = gl_start_idps(s_iap_host_ack_callback);
    
    if(kCommandTransmissionOk == currentCommandTxStatus)
    {
        IapHostStateMachine.actualState = kHostWaitState;
    }
    
    /* TODO handle transmission error, validate if the command could be sent or queued and act 
     * accordingly otherwise */

}

/*************************************************************************************************
*
* @brief    Function called automatically by the protocol when the IDPS is replied with an iPod Ack
* @return   void
* 
**************************************************************************************************/
static void s_iap_host_ack_callback(uint8_t codeOfAck)
{
    dprint("iap_host_ack_callback start\n");

    if(kIAPAckSuccess == codeOfAck){
        /* IDPS process supported, start sequence to set FID and payload sizes */ 
        IapHostStateMachine.actualState = kHostRequestMaxPaylodState;
    /* if a bad parameter is received, switch to identify device lingoes */
    }else if (kIAPAckBadParameter == codeOfAck){     
        IapHostStateMachine.actualState = kHostStartIdentifyDeviceLingoesState;
    }else{
        /*! Something else, start IDPS again... */
         IapHostStateMachine.actualState = kHostStartIdpsState;
    }
}

/*************************************************************************************************
 * @brief Function used to transition from IDPS to Identify device lingoes when IDPS is not supported
 * @return void
 * @details This state is used just to wait for an iPod ack after an empty  
**************************************************************************************************/
static void s_iap_host_idps_not_supported_callback(uint8_t codeOfAck)
{
    dprint("iap_host_idps_not_supported_callback start\n");
    if(kIAPAckSuccess == codeOfAck)
    {
        /* If an Ack was replied to identify device lingoes, disable transaction ID */
        IAPI_DISABLE_TRANSACTION_ID;
        
        /* At this point it's required that iPod sends GetAccessoryInfo to continue with 
         * identify device lingoes */
        IapHostStateMachine.actualState = kHostWaitState;
    }
}

/*************************************************************************************************
*
* @brief    Send appropriate the first Identify device lingoes command with 
*             LingoesSpoken = general lingo, option = 0x00 and deviceID = 0x00
* @return   void
*          
**************************************************************************************************/
static void s_iap_host_start_identify_device_lingoes_state(void)
{   
    uint8_t currentCommandTxStatus; 

    /* iOS does not support IDPS then use IdentifyDeviceLingoes */
    g_iapHostStatus |= (1 << kHostIdpsNotSupported);

    s_IapHostIdentifyLingoes.deviceLingoesSpoken = (1 << kGeneralLingo);
    s_IapHostIdentifyLingoes.options = 0x00u;
    s_IapHostIdentifyLingoes.deviceID = 0x00u;

    iap_interface_request_max_payload_change(kiAPIDefaultMaxPayloadSizeSpec);

#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(s_IapHostIdentifyLingoes.deviceLingoesSpoken, s_IapHostIdentifyLingoes.deviceLingoesSpoken);
#endif

    /* Send an identify device lingoes with general lingo only to force GetAccessoryInfo command,
     * which is an asynchronous command */
    currentCommandTxStatus = gl_identify_device_lingoes((uint8_t*) &s_IapHostIdentifyLingoes, 
                                                        sizeof(s_IapHostIdentifyLingoes),
                                                        s_iap_host_idps_not_supported_callback);
    
   /* TODO handle transmission error, validate if the command could be sent or queued and act 
    * accordingly otherwise */

    IapHostStateMachine.actualState = kHostWaitState;
}

/*************************************************************************************************
*
* @brief    Send the appropriate the Identify device lingoes command with all the lingoes spoken  
*             and the corresponding device ID
* @return   void
*          
**************************************************************************************************/
static void s_iap_host_continue_identify_device_lingoes_state(void)
{
    uint8_t currentCommandTxStatus; 
    
    s_IapHostIdentifyLingoes.deviceLingoesSpoken = IAP_CMD_LINGOES_SUPPORTED_MASK;
    s_IapHostIdentifyLingoes.options = kAuthAfterIdentification;
    
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(s_IapHostIdentifyLingoes.deviceLingoesSpoken, s_IapHostIdentifyLingoes.deviceLingoesSpoken);
    BYTESWAP32(s_IapHostIdentifyLingoes.options, s_IapHostIdentifyLingoes.options);
    s_IapHostIdentifyLingoes.deviceID = (uint8_t)iAP_Auth_gbaCP_VersionAndID[5];
    s_IapHostIdentifyLingoes.deviceID <<= 8;
    s_IapHostIdentifyLingoes.deviceID |= (uint8_t)iAP_Auth_gbaCP_VersionAndID[4];
    s_IapHostIdentifyLingoes.deviceID <<= 8;
    s_IapHostIdentifyLingoes.deviceID |= (uint8_t)iAP_Auth_gbaCP_VersionAndID[3];
    s_IapHostIdentifyLingoes.deviceID <<= 8;
    s_IapHostIdentifyLingoes.deviceID |= (uint8_t)iAP_Auth_gbaCP_VersionAndID[2];
#else
    s_IapHostIdentifyLingoes.dwDeviceID = (uint8_t)iAP_Auth_gbaCP_VersionAndID[2];
    s_IapHostIdentifyLingoes.dwDeviceID <<= 8;
    s_IapHostIdentifyLingoes.dwDeviceID |= (uint8_t)iAP_Auth_gbaCP_VersionAndID[3];
    s_IapHostIdentifyLingoes.dwDeviceID <<= 8;
    s_IapHostIdentifyLingoes.dwDeviceID |= (uint8_t)iAP_Auth_gbaCP_VersionAndID[4];
    s_IapHostIdentifyLingoes.dwDeviceID <<= 8;
    s_IapHostIdentifyLingoes.dwDeviceID |= (uint8_t)iAP_Auth_gbaCP_VersionAndID[5];
#endif
    
    /* Send an identify device lingoes with supported lingoes mask and coprocessor unique ID  */
    currentCommandTxStatus = gl_identify_device_lingoes((uint8_t*)&s_IapHostIdentifyLingoes, 
                                                        sizeof(s_IapHostIdentifyLingoes),
                                                        s_iap_host_idps_not_supported_callback);
    
    if(kCommandTransmissionOk == currentCommandTxStatus)
    {
        IapHostStateMachine.actualState = kHostWaitState;
    }
    
/* If little endian, leave the parameters in the right order after leaving the function */
#if(BIG_ENDIAN_CORE == _FALSE_)
    BYTESWAP32(s_IapHostIdentifyLingoes.deviceLingoesSpoken, s_IapHostIdentifyLingoes.deviceLingoesSpoken);
    BYTESWAP32(s_IapHostIdentifyLingoes.options, s_IapHostIdentifyLingoes.options);
#endif
}


static void s_iap_host_identify_device_lingoes_delay_state (void)
{
    osa_time_delay(10);

    IapHostStateMachine.actualState = kHostEssentialRoutinesDone;

}

/*************************************************************************************************
*
* @brief    Get maximum number of bytes that the iPod can receive and send
* @return      void
*          
**************************************************************************************************/
static void s_iap_host_request_max_payload_size_state(void)
{
    dprint("iap_host_request_max_payload_size_state start\n");

    command_transmission_status_t currentCommandTxStatus;
    
    currentCommandTxStatus = gl_request_max_payload_size(g_iapHostGenericReceptionBuffer,
            g_iapHostGenericReceptionBufferSize, s_iap_host_return_max_payload_callback);
    
    /* If there was success in transmission go to next state */
    if(kCommandTransmissionOk == currentCommandTxStatus)
    {
        IapHostStateMachine.actualState = kHostWaitState;
    }
}

/*************************************************************************************************
*
* @brief    Read the maximum payload response and request to the interface to store it 
* @return   void
*          
**************************************************************************************************/
static void s_iap_host_return_max_payload_callback(uint8_t codeOfAck)
{
    uint16_t maxPayload;
    
    /* if the buffer has 0x1011 it indicates that the response to the max payload size was an error
     * code 0x04 (bad parameter) for command 0x11(RequestTransportMaxPayloadSize)*/
    if (!((kIAPAckBadParameter == codeOfAck) || (kIAPAckCommandUnavailableInMode == codeOfAck)))
    {
        maxPayload = ((uint16_t)g_iapHostGenericReceptionBuffer[0]) << 8;
        maxPayload |= (uint16_t) g_iapHostGenericReceptionBuffer[1];
        /* Request for maximum payload size change to the interface */
        iap_interface_request_max_payload_change(maxPayload);
    }
    else
    {
        iap_interface_request_max_payload_change(kiAPIDefaultMaxPayloadSizeSpec);
    }
    IapHostStateMachine.actualState = kHostGetIpodOptionsForLingoState;
}

/*************************************************************************************************
*
* @brief State used to store the different options supported per lingo
* @return      void
* @details This function is called several times until all lingoes supported are obtained. Once
*          all lingoes are requested, it will move to kSetFIDTokenValues
*
**************************************************************************************************/
static void s_iap_host_get_ipod_options_for_lingo_state(void)
{
    dprint("iap_host_get_ipod_options_for_lingo_state start\n");

    command_transmission_status_t currentCommandTxStatus;
    /* Calculate the number of lingoes according the preprocessor settings */
    while((!(IAP_CMD_LINGOES_SUPPORTED_MASK & (1 << g_iapHostLingoNumber))) 
            && (kLocationLingo >= g_iapHostLingoNumber))
    {
        g_iapHostLingoNumber++;
    }
    
    /* Send the getIpodOptionsForLingo for each lingo until the last one Location Lingo */
    if(kLocationLingo >= g_iapHostLingoNumber)
    {
        /* Send the GetIpodOptionsForLingo command and set the reception buffer and 
         * answer callback */
        currentCommandTxStatus = gl_get_ipod_options_for_lingo(&g_iapHostLingoNumber, 
                1, g_iapHostGenericReceptionBuffer, g_iapHostGenericReceptionBufferSize,
                s_iap_host_get_ipod_options_for_lingo_callback);

        /* TODO handle transmission error, validate if the command could be sent or queued and act 
         * accordingly otherwise */
        
        IapHostStateMachine.actualState = kHostWaitState;

    }
    /* When finished sending getIpodOptions command for each lingo proceed to SetFIDTokensValues */
    else
    {
        g_iapHostLingoNumber = 0;
        
#if(COMM_WITH_IOS_APPLICATIONS_MASK)
            kIapCommandsFidTokenSize[EA_PROTOCOL_TOKEN_OFFSET] = (kIapCommandsFidTokenFields[EA_PROTOCOL_TOKEN_OFFSET][0] + 1);
            kIapCommandsFidTokenSize[BUNDLE_SEED_ID_PREF_TOKEN_OFFSET] = (kIapCommandsFidTokenFields[BUNDLE_SEED_ID_PREF_TOKEN_OFFSET][0] + 1);
            kIapCommandsFidTokenSize[EA_PROTOCOL_METADATA_TOKEN_OFFSET] = (kIapCommandsFidTokenFields[EA_PROTOCOL_METADATA_TOKEN_OFFSET][0] + 1);
#endif
        
        IapHostStateMachine.actualState = kHostSetFidTokenValues;
    }
}

/*************************************************************************************************
*
* @brief Callback routine called when a IAP_GENERALLINGO_RETIPODOPTIONSFORLINGO is received
* @return   void
* @details  The callback routine will store the options of each lingo inside iAP_Cmd_gbpaLingoesIpodOptions.
*           This function will always return the state to get_ipod_options_for_lingo 
* @sa       iAP_Cmd_gbpaLingoesIpodOptions
* 
**************************************************************************************************/
static void s_iap_host_get_ipod_options_for_lingo_callback(uint8_t codeOfAck)
{
    uint32_t lingoOptionBits;
    uint8_t *sourcePointer = &g_iapHostGenericReceptionBuffer[5];

    dprint("iap_host_get_ipod_options_for_lingo_state start\n");

    (void) codeOfAck; /* Ignores reception status */
    
    lingoOptionBits = *sourcePointer++;
    lingoOptionBits <<= 8;
    lingoOptionBits |= *sourcePointer++;
    lingoOptionBits <<= 8;
    lingoOptionBits |= *sourcePointer++;
    lingoOptionBits <<= 8;
    lingoOptionBits |= *sourcePointer++;

    *(uint32_t*)kIapCommandsLingoesIpodOptions[g_iapHostLingoNumber] = (uint32_t)(lingoOptionBits);

    if(kGeneralLingo == g_iapHostLingoNumber)
    {
        if(!(lingoOptionBits & (1 << kGeneralRestrictedIdpsTokens)))
        {
            /* Restricted IDPS Token Handling */
            g_enabledFIDTokens &= ~(EA_PROTOCOL_METADATA_TOKEN_BIT_MASK \
                    | ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_BIT_MASK \
                    | ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_BIT_MASK);
        }

#if(IPOD_PREF_VIDEO_OUT_SETTINGS_MASK)        
        if(lingoOptionBits & (1 << kGeneralVideoOutput))
        {    
            g_enabledFIDTokens |= IPOD_PREF_VIDEO_OUT_SETTINGS_BIT_MASK;
#if(IPOD_PREF_SCREEN_CONFIGURATION_MASK)
            g_enabledFIDTokens |= IPOD_PREF_SCREEN_CONFIGURATION_BIT_MASK;
#endif

#if(IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_VIDEO_SIGNAL_FORMAT_BIT_MASK;

#if(DEFAULT_VIDEO_SIGNAL_FORMAT == VIDEO_SIGNAL_FORMAT_NTSC)
            if(lingoOptionBits & (1 << kGeneralNtscVideoSignalFormat))
            {
                s_iapHostDefaultVideoSignalFormat = VIDEO_SIGNAL_FORMAT_NTSC;
                g_enabledFIDTokens |= IPOD_PREF_VIDEO_SIGNAL_FORMAT_BIT_MASK;
            }
#endif

#if(DEFAULT_VIDEO_SIGNAL_FORMAT == VIDEO_SIGNAL_FORMAT_PAL)
            if(lingoOptionBits & (1 << GENERAL_PAL_VIDEOSIGNALFORMAT))
            {
                s_iapHostDefaultVideoSignalFormat = VIDEO_SIGNAL_FORMAT_PAL;
                g_enabledFIDTokens |= IPOD_PREF_VIDEO_SIGNAL_FORMAT_BIT_MASK;
            }
#endif
#endif

#if(IPOD_PREF_VIDEO_OUT_CONNECTION_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_VIDEO_OUT_CONNECTION_BIT_MASK;

#if(DEFAULT_VIDEO_OUT_CONNECTION == VIDEO_OUT_CONNECTION_COMPOSITE)
            if(lingoOptionBits & (1 << kGeneralCompositeVideoOutConnection))
            {
                s_iapHostDefaultVideoOutConnection = kVideoOutConnectionComposite;
                g_enabledFIDTokens |= IPOD_PREF_VIDEO_OUT_CONNECTION_BIT_MASK;
            }
#endif

#if(DEFAULT_VIDEO_OUT_CONNECTION == VIDEO_OUT_CONNECTION_S_VIDEO)
            if(lingoOptionBits & (1 << kGeneralSVideoOutConnection))
            {
                s_iapHostDefaultVideoOutConnection = kVideoOutConnectionSVideo;
                g_enabledFIDTokens |= IPOD_PREF_VIDEO_OUT_CONNECTION_BIT_MASK;
            }
#endif

#if(DEFAULT_VIDEO_OUT_CONNECTION == VIDEO_OUT_CONNECTION_COMPONENT)
            if(lingoOptionBits & (1 << kGeneralComponentVideoOutConenction))
            {
                s_iapHostDefaultVideoOutConnection = kVideoOutConnectionComponent;
                g_enabledFIDTokens |= IPOD_PREF_VIDEO_OUT_CONNECTION_BIT_MASK;
            }
#endif
#endif

#if(IPOD_PREF_VIDEO_ASPECT_RATIO_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_VIDEO_ASPECT_RATIO_BIT_MASK;

#if(DEFAULT_VIDEO_ASPECT_RATIO == VIDEO_ASPECT_RATIO_FULLSCREEN)
            if(lingoOptionBits & (1 << kGeneralVideoAspectRatioFullscreen))
            {
                s_iapHostDefaultVideoAspectRatio = kVideoAspectRatioFullscreen;
                g_enabledFIDTokens |= IPOD_PREF_VIDEO_ASPECT_RATIO_BIT_MASK;
            }
#endif

#if(DEFAULT_VIDEO_ASPECT_RATIO == VIDEO_ASPECT_RATIO_WIDESCREEN)
            if(lingoOptionBits & (1 << kGeneralVideoAspectRatioWidescreen))
            {
                s_iapHostDefaultVideoAspectRatio = kVideoAspectRatioWidescreen;
                g_enabledFIDTokens |= IPOD_PREF_VIDEO_ASPECT_RATIO_BIT_MASK;
            }
#endif                    
#endif

#if(IPOD_PREF_CLOSED_CAPTIONING_MASK)
            if(lingoOptionBits & (1 << kGeneralClosedCaptioning)) 
            {
                g_enabledFIDTokens |= IPOD_PREF_CLOSED_CAPTIONING_BIT_MASK;
            }
            else
            {
                g_enabledFIDTokens &= ~IPOD_PREF_CLOSED_CAPTIONING_BIT_MASK;
            }
#endif

#if(IPOD_PREF_SUBTITLES_MASK)
            if(lingoOptionBits & (1 << kGeneralSubtitles))
            {
                g_enabledFIDTokens |= IPOD_PREF_SUBTITLES_BIT_MASK;
            }
            else
            {
                g_enabledFIDTokens &= ~IPOD_PREF_SUBTITLES_BIT_MASK;
            }
#endif
        }
        else
        {
            g_enabledFIDTokens &= ~IPOD_PREF_VIDEO_OUT_SETTINGS_BIT_MASK;
#if(IPOD_PREF_SCREEN_CONFIGURATION_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_SCREEN_CONFIGURATION_BIT_MASK;
#endif

#if(IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_VIDEO_SIGNAL_FORMAT_BIT_MASK;
#endif

#if(IPOD_PREF_VIDEO_OUT_CONNECTION_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_VIDEO_OUT_CONNECTION_BIT_MASK;
#endif

#if(IPOD_PREF_VIDEO_ASPECT_RATIO_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_VIDEO_ASPECT_RATIO_BIT_MASK;
#endif

#if(IPOD_PREF_CLOSED_CAPTIONING_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_CLOSED_CAPTIONING_BIT_MASK;
#endif

#if(IPOD_PREF_SUBTITLES_MASK)
            g_enabledFIDTokens &= ~IPOD_PREF_SUBTITLES_BIT_MASK;
#endif
        }
#endif

#if(IPOD_PREF_LINE_OUT_USAGE_MASK)
        if(lingoOptionBits & (1 << kGeneralLineOutUsage))
        {    
            g_enabledFIDTokens |= IPOD_PREF_LINE_OUT_USAGE_BIT_MASK;    
        }
        else
        {
            g_enabledFIDTokens &= ~IPOD_PREF_LINE_OUT_USAGE_BIT_MASK;
        }
#endif

#if(IPOD_PREF_VIDEO_ALTERNATE_AUDIO_MASK)
        if(lingoOptionBits & (1 << kGeneralVideoAlternateAudio))
        {    
            g_enabledFIDTokens |= IPOD_PREF_VIDEO_ALTERNATE_AUDIO_BIT_MASK;
        }
        else
        {
            g_enabledFIDTokens &= ~IPOD_PREF_VIDEO_ALTERNATE_AUDIO_BIT_MASK;
        }
#endif

#if(IPOD_PREF_PAUSE_ON_POWER_REMOVAL_MASK)
        if(lingoOptionBits & (1 << kGeneralPauseOnPowerRemoval))
        {    
            g_enabledFIDTokens |= IPOD_PREF_PAUSE_ON_POWER_REMOVAL_BIT_MASK;
        }
        else
        {
            g_enabledFIDTokens &= ~IPOD_PREF_PAUSE_ON_POWER_REMOVAL_BIT_MASK;
        }
#endif            

#if(EA_PROTOCOL_TOKEN_MASK)
        if(lingoOptionBits & (1 << kGeneralCommunicationWithIosApp))
        {    
            g_enabledFIDTokens |= EA_PROTOCOL_TOKEN_BIT_MASK;    
        }
        else
        {
            g_enabledFIDTokens &= ~EA_PROTOCOL_TOKEN_BIT_MASK;
        }
#endif

#if(BUNDLE_SEED_ID_PREF_TOKEN_MASK)
        if(lingoOptionBits & (1 << kGeneralCommunicationWithIosApp))
        {    
            g_enabledFIDTokens |= BUNDLE_SEED_ID_PREF_TOKEN_BIT_MASK;    
        }
        else
        {
            g_enabledFIDTokens &= ~BUNDLE_SEED_ID_PREF_TOKEN_BIT_MASK;
        }
#endif

#if(EA_PROTOCOL_METADATA_TOKEN_MASK)
        if(lingoOptionBits & (1 << kGeneralCommunicationWithIosApp))
        {    
            g_enabledFIDTokens |= EA_PROTOCOL_METADATA_TOKEN_BIT_MASK;    
        }
        else
        {
            g_enabledFIDTokens &= ~EA_PROTOCOL_METADATA_TOKEN_BIT_MASK;
        }
#endif

#if(defined(IAPI_INTERFACE_USB_HOST) || defined(IAPI_INTERFACE_USB_DEV))
#if(ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_MASK)
        if((lingoOptionBits & (1 << kGeneralUsbHostModeInvokedByHardware)) &&
           (lingoOptionBits & (1 << kGeneralUsbHostModeAudioOutputStreaming)))
        {            
            g_enabledFIDTokens |= ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_BIT_MASK;
        }
        else
        {
            g_enabledFIDTokens &= ~ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_BIT_MASK;
        }
#endif

#if(ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK)
        if((lingoOptionBits & (1 << kGeneralUsbHostModeInvokedByHardware)) &&
           (lingoOptionBits & (1 << kGeneralAnalogAndUsbAudioSwitching)))
        {            
            g_enabledFIDTokens |= ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_BIT_MASK;
        }
        else
        {
            g_enabledFIDTokens &= ~ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_BIT_MASK;
        }
#endif
#else
#if(ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_MASK)
        g_enabledFIDTokens &= ~ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_BIT_MASK;
#endif
#if(ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK)
        g_enabledFIDTokens &= ~ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_BIT_MASK;
#endif
#endif
    }
    else if(kSimpleRemoteLingo == g_iapHostLingoNumber)
    {
#if(IPOD_PREF_VOICE_OVER_MASK)
        if(lingoOptionBits & (1 << kSimpleRemoteVoiceOverPreferences))
        {    
            g_enabledFIDTokens |= IPOD_PREF_VOICE_OVER_BIT_MASK;    
        }
        else
        {
            g_enabledFIDTokens &= ~IPOD_PREF_VOICE_OVER_BIT_MASK;
        }
#endif

#if(IPOD_PREF_ASSISTIVE_TOUCH_MASK)
        if(lingoOptionBits & (1 << kSimpleRemoteAssistiveTouchCursor))
        {    
            g_enabledFIDTokens |= IPOD_PREF_ASSISTIVE_TOUCH_BIT_MASK;
        }
        else
        {
            g_enabledFIDTokens &= ~IPOD_PREF_ASSISTIVE_TOUCH_BIT_MASK;
        }
#endif
    }

    g_iapHostLingoNumber++;
    IapHostStateMachine.actualState = kHostGetIpodOptionsForLingoState;

}

/*************************************************************************************************
*
* @brief State used to send all FID token values from the accessory to the device 
* @return   void
* @details This function sets one token at a time based on the compile time settings for the 
*           different supported features. These settings can't be changed in run time
* @todo    Change all hard-coded values for the masks and shifts when setting FID values
*
**************************************************************************************************/
static void s_iap_host_set_fid_token_values_state(void)
{
    uint8_t* dataToTransmitPtr;
    uint16_t dataToTransmitSize;
    iap_gl_accessory_in_max_payload_token_t *swapParameters;
    command_transmission_status_t currentCommandTxStatus;

    /* Calculate the Token numbers according the preprocessor settings */
    while((!(g_enabledFIDTokens & (1 << s_iapHostTokenNumber))) && 
            (EA_PROTOCOL_METADATA_TOKEN_BIT_MASK >= (1 << s_iapHostTokenNumber)))
    {
        s_iapHostTokenNumber++;
    }
    
    /* Send SetFIDTokens until the last token EA_PROTOCOL_METADATA_TOKEN */
    if(EA_PROTOCOL_METADATA_TOKEN_BIT_MASK >= (1 << s_iapHostTokenNumber))
    {
        uint8_t dataToTransmitBuffer[100];
        uint8_t index = 1;
        
        /* Get the Token field of the current token to send */
        dataToTransmitPtr = (uint8_t*)(kIapCommandsFidTokenFields[s_iapHostTokenNumber]);
        /* Get the size of the current token to send */
        dataToTransmitSize = (uint16_t)(kIapCommandsFidTokenSize[s_iapHostTokenNumber] + 1);
        /* Write only one Token */
        dataToTransmitBuffer[0] = 0x01;

        while(index < dataToTransmitSize)
        {
            dataToTransmitBuffer[index] = *dataToTransmitPtr++;
            index++;
        }

        if(IDENTIFY_TOKEN_BIT_MASK == (1 << s_iapHostTokenNumber))
        {
            dataToTransmitBuffer[IDENTIFY_TOKEN_SIZE] = iAP_Auth_gbaCP_VersionAndID[5];
            dataToTransmitBuffer[IDENTIFY_TOKEN_SIZE - 1] = iAP_Auth_gbaCP_VersionAndID[4];
            dataToTransmitBuffer[IDENTIFY_TOKEN_SIZE - 2] = iAP_Auth_gbaCP_VersionAndID[3];
            dataToTransmitBuffer[IDENTIFY_TOKEN_SIZE - 3] = iAP_Auth_gbaCP_VersionAndID[2];
        }

        if(ACC_CAPABILITY_TOKEN_BIT_MASK == (1 << s_iapHostTokenNumber))
        {
#if(IPOD_PREF_VOICE_OVER_MASK)
            if((*(uint32_t*)kIapCommandsLingoesIpodOptions[kSimpleRemoteLingo]) &
                    (1 << kSimpleRemoteVoiceOverPreferences))
            {
                dataToTransmitBuffer[9] |= (1 << 1);
            }
            else
            {
                dataToTransmitBuffer[9] &= ~(1 << 1);    
            }
#endif

#if(IPOD_PREF_ASSISTIVE_TOUCH_MASK)
            if((*(uint32_t*) kIapCommandsLingoesIpodOptions[kSimpleRemoteLingo]) & 
                    (1 << kSimpleRemoteAssistiveTouchCursor))
            {
                dataToTransmitBuffer[9] |= (1 << 7);    
            }
            else
            {
                dataToTransmitBuffer[9] &= ~(1 << 7);    
            }
#endif

#if defined(IAPI_INTERFACE_USB_HOST) || defined(IAPI_INTERFACE_USB_DEV)
#if(ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK)
            if((*(uint32_t*)kIapCommandsLingoesIpodOptions[kGeneralLingo]) &
                    (1 << kGeneralAnalogAndUsbAudioSwitching))
            {
                dataToTransmitBuffer[9] |= (1 << 5);    
            }
            else
            {
                dataToTransmitBuffer[9] &= ~(1 << 5);    
            }
#endif
#else
            dataToTransmitBuffer[9] &= ~(1 << 5);
#endif
        }

#if(IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK)
        if(IPOD_PREF_VIDEO_SIGNAL_FORMAT_BIT_MASK == (1 << s_iapHostTokenNumber))
        {
            dataToTransmitBuffer[5] = s_iapHostDefaultVideoSignalFormat;
        }
#endif

#if(IPOD_PREF_VIDEO_OUT_CONNECTION_MASK)
        if(IPOD_PREF_VIDEO_OUT_CONNECTION_BIT_MASK == (1 << s_iapHostTokenNumber))
        {
            dataToTransmitBuffer[5] = s_iapHostDefaultVideoOutConnection;
        }
#endif

#if(IPOD_PREF_VIDEO_ASPECT_RATIO_MASK)
        if(IPOD_PREF_VIDEO_ASPECT_RATIO_BIT_MASK == (1 << s_iapHostTokenNumber))
        {
            dataToTransmitBuffer[5] = s_iapHostDefaultVideoAspectRatio;
        }
#endif
        
#if(BIG_ENDIAN_CORE == _FALSE_)
        swapParameters = (iap_gl_accessory_in_max_payload_token_t *)(&dataToTransmitBuffer[0]);

        if(kAccessoryInfoTokenFIDSubtype == swapParameters->fidSubtype)
        {
            if(kAccessoryInMaxPayloadType == swapParameters->accInfoType)
            {
                BYTESWAP16(swapParameters->inMaxPayload, swapParameters->inMaxPayload);   
            }
            else if(kAccessoryStatusType == swapParameters->accInfoType)
            {
                iap_gl_accessory_status_token_t *swapTemp = 
                        (iap_gl_accessory_status_token_t *)(swapParameters);
                BYTESWAP32(swapTemp->accStatus, swapTemp->accStatus);  
            }
            else if(kAccessoryRfCertificationType == swapParameters->accInfoType)
            {
                iap_gl_accessory_rf_certification_token_t *swapTemp = 
                        (iap_gl_accessory_rf_certification_token_t *)(swapParameters);
                BYTESWAP32(swapTemp->rfCertification, swapTemp->rfCertification);    
            }
        }
        else if(kAccessoryDigitalAudioSampleRatesTokenFIDSubtype == swapParameters->fidSubtype)
        {
            iap_gl_audio_sample_rates_token_t *swapTemp = 
                    (iap_gl_audio_sample_rates_token_t *)(swapParameters);
        }
        else if(kAccessoryDigitalAudioVideoDelayTokenFIDSubtype == swapParameters->fidSubtype)
        {
            iap_gl_audio_video_delay_token_t *swapTemp = 
                    (iap_gl_audio_video_delay_token_t *)(swapParameters);
            BYTESWAP32(swapTemp->videoDelayMs, swapTemp->videoDelayMs);
        }
#endif
        
        /* Send the setFidTokenValues command and set the reception buffer and callback when 
         * answer ackFidTokenValues is received */
        currentCommandTxStatus = gl_set_fid_token_values(&dataToTransmitBuffer[0], dataToTransmitSize, 
                                                         g_iapHostGenericReceptionBuffer,
                                                         g_iapHostGenericReceptionBufferSize, 
                                                         s_iap_host_set_fid_token_values_callback);

        /* TODO handle transmission error, validate if the command could be sent or queued and act 
         * accordingly otherwise */
        
        IapHostStateMachine.actualState = kHostWaitState;

    }
    else
    {
        s_iapHostTokenNumber = 0;
        IapHostStateMachine.actualState = kHostEndIdpsState;
    }
}

/*************************************************************************************************
*
* @brief   State used to read the response to the previous FID token set
* @details If a token is rejected by the Apple device the IDPS process fails. This must be fixed 
*          by disabling the token that was rejected trough the token masks defined in the 
*          iAP_Commands.h file
* @return  void
* @details This functions is called when reception of ackFidTokenValues of the set token 
*
**************************************************************************************************/
static void s_iap_host_set_fid_token_values_callback(uint8_t codeOfAck)
{
    dprint("iap_host_set_fid_token_values_callback start\n");
    static uint8_t s_tokenRetryCounter = 0;
    
    /* If the ackFidToken isn't Ok then identify if the error was in a required or optional token 
     * and set corresponding flags */
    if((kTokenAccepted == g_iapHostGenericReceptionBuffer[4]) && (codeOfAck == kIAPAckSuccess))
    {
        /* Increment the actual token and return to send the reminding tokens */
        s_iapHostTokenNumber++;
        s_tokenRetryCounter = 0;
        IapHostStateMachine.actualState = kHostSetFidTokenValues;
    }else
    {
        if((s_tokenRetryCounter++) > MAX_FID_TOKEN_RETRY)
        {
            s_iapHostTokenNumber = 0;
            s_tokenRetryCounter = 0;
            IapHostStateMachine.actualState = kHostStartIdentifyDeviceLingoesState;
        }else
        {
            IapHostStateMachine.actualState = kHostSetFidTokenValues;
        }
    }
}

/*************************************************************************************************
*
* @brief When all FID tokens are set, the IDPS process can be finished 
* @return   void
**
**************************************************************************************************/
static void s_iap_host_end_idps_state(void)
{
    dprint("iap_host_end_idps_state start\n");    

    command_transmission_status_t currentCommandTxStatus;

    iAPH_gEndIDPSStatus = kIosWontAcceptAnyToken;
    
    /* A value of 0 indicates that the accessory has finished IDPS and asks to continue with 
     * Authentication */
    currentCommandTxStatus = gl_end_idps((iap_gl_accessory_end_idps_t) iAPH_gbAuthenticationFlag, 
                                          &iAPH_gEndIDPSStatus, 
                                          s_iap_host_end_idps_callback);

    if(kCommandTransmissionOk == currentCommandTxStatus)
    {
        IapHostStateMachine.actualState = kHostWaitState;
    }

}

/*************************************************************************************************
*
* @brief Read the response to the EndIDPS command sent recently. If ok, code will proceed to authenticate
* @return   void
*
**************************************************************************************************/
static void s_iap_host_end_idps_callback(uint8_t codeOfAck)
{
    dprint("iap_host_end_idps_callback start\n");
    (void) codeOfAck;
    /* Ack timer Once IDPS process finishes (independently of the result), disable timeout*/
    if(kAllRequiredTokensReceived == iAPH_gEndIDPSStatus)
    {
        g_iapHostStatus |= (1 << kHostIdpsDone);
        
        /* Wait 20ms before accept application commands */
        osa_time_delay(20);

        /* Authentication Supported, iOS must start authentication process as soon as possible */
        IapHostStateMachine.actualState = kHostWaitState;

    }else if(kIosWontAcceptAnyToken == iAPH_gEndIDPSStatus)
    {
        IapHostStateMachine.actualState = kHostStartIdentifyDeviceLingoesState;
    }
    else
    {
        s_iapHostAccessoryAuthRetriesCounter = 0;
        
        /* @TODO Error code receive, implement a better error handler for this! */
        g_iapHostStatus &= ~(1 << kHostIdpsDone);
        g_iapHostStatus |= (1 << kHostInternalError);
        g_iapHostInternalErrorFlags |= (1 << kHostRequiredFidTokensError);
        IapHostStateMachine.actualState = kHostAccessoryIdpsAuthenticationErrorState;
    }
}

/*************************************************************************************************
*
* @brief State reached when there is an error during IDPS, IDL or Authentication processes
* @return   void
*
**************************************************************************************************/
static void s_iap_host_accessory_idps_auth_error_state(void)
{
    if(IAPH_MAX_ACC_AUTH_RETRIES < s_iapHostAccessoryAuthRetriesCounter)
    {
        /* Restart Accessory Authentication Retries Counter */
        g_iapHostStatus &= ~(1 << kHostAccessoryAuthenticationDone);
        g_iapHostStatus |= (1 << kHostInternalError);
        g_iapHostInternalErrorFlags |= (1 << kHostAccessoryAuthenticationError);
    }
    else
    {
        s_iapHostAccessoryAuthRetriesCounter++;
        IapHostStateMachine.actualState = kHostStartIdpsState;
    }
}

/*************************************************************************************************
*
* @brief This state is reached after identification (either IDPS or identify device lingoes) and
*        authentication process (can be accessory only or accessory and iOS) finished successfully
* @return   void
*
**************************************************************************************************/
static void s_iap_host_essential_routines_done_state(void)
{
    IapHostStateMachine.actualState = kHostReadyState;
    g_txPayloadBufferSize = IAPI_MAX_TX_PAYLOAD_SIZE;

    if (s_iapHostDeviceReadyCallback != NULL)
    {
        s_iapHostDeviceReadyCallback();
    }   
}

/*************************************************************************************************
*
* @brief This state is reached after identification, authentication process finished successfully
*        and after executing the device ready application callback (s_iapHostDeviceReadyCallback)
* @return   void
*
**************************************************************************************************/
static void s_iap_host_ready_state(void)
{
    iap_commands_receive_command();

    if(!(IAP_HOST_IDEVICE_CONNECTED && IAP_HOST_IDEVICE_POWER))
    {
        dprint("  iAP_Host_Ready_State calling IOS_Disconnection\n");
        /* @todo Check that there is no hibernate flag */
        iap_host_handle_ios_disconnection();
    }
}

/* Accessory Authentication states */
/*************************************************************************************************
*
* @brief State used to read the certificate data from the coprocessor 
* @return   void
* @details This command reads the authentication certificate which is ~1kB of data read through
*          I2C. Once the coprocessor is ready, the code will move to a state to send this data
*          through the protocol
* @sa      iAP_Host_vfnSendACCCertificate_State
*
**************************************************************************************************/
static void s_iap_host_read_acc_certificate_state(void)
{
        dprint("iap_host_read_acc_certificate_state start\n");
    
        g_iapHostAuthCertificateBuff = &iAP_Auth_CPCertificate[0];
        g_iapHostAuthCertificateBuff[0] = iAP_Auth_gbaCP_VersionAndID[0];   /* mayor version */
        g_iapHostAuthCertificateBuff[1] = iAP_Auth_gbaCP_VersionAndID[1];   /* minor version */
        IAP_HOST_READ_ACCESSORY_CERTIFICATE((g_iapHostAuthCertificateBuff + 4),
                                            (uint16_t*)&g_iapHostAuthCertificateBuffSize);
        
        IapHostStateMachine.previousState = IapHostStateMachine.actualState;  
        IapHostStateMachine.actualState = kHostWaitCoprocessorState;
        IapHostStateMachine.nextState = kHostSendAccCertificateState;
        IapHostStateMachine.timeoutState = kHostWaitCoprocessorErrorState;
}

/*************************************************************************************************
*
* @brief State used to send the accessory certificate read from the Authentication coprocessor
* @return  void
* @details This command is somehow similar to a multi-section transfer. Multiple packets will be 
*          sent if data payload is smaller than ~1280 bytes which is an approximate value for the
*          certificate length. Therefore, this state will be called multiple times to send the
*          authentication information
*
**************************************************************************************************/
static void s_iap_host_send_accessory_certificate_state(void)
{ 
    static uint8_t* certificateStart; 
    static uint8_t maximumSection = 0;
    static uint8_t currentSection = 0;
    static uint16_t remainder = 0;
    static uint16_t transmissionIndex = 0;
    command_transmission_status_t currentCommandTxStatus;
    uint16_t transmissionSize;

    /* Increment the number of section to transmit of the authentication buffer */
    currentSection++;
    /* This only done once, calculate the remainder packets for the first packet to sent */
    if(!s_iapHostAccessoryAuthInfoPacketCounter)
    {
        s_iapHostAccessoryAuthInfoPacketCounter = 1;
        transmissionIndex = (g_txPayloadBufferSize - 4 - 4);
        /* Indicates the number of subbuffers to transmit the whole packet */
        maximumSection = (uint8_t)(g_iapHostAuthCertificateBuffSize / transmissionIndex);
        remainder = g_iapHostAuthCertificateBuffSize % transmissionIndex;
        /* Start from the first packet */
        currentSection = 0;
        certificateStart = &g_iapHostAuthCertificateBuff[0];
        g_iapHostAuthCertificateBuff[2] = currentSection;
        g_iapHostAuthCertificateBuff[3] = maximumSection;
    }
    else
    {
        /* Adjust payload to transmit according the current section and transmissionIndex */
        g_iapHostAuthCertificateBuff = &certificateStart[((transmissionIndex) * (currentSection))];
        g_iapHostAuthCertificateBuff[0] = certificateStart[0];      //mayor version
        g_iapHostAuthCertificateBuff[1] = certificateStart[1];      //minor version
        g_iapHostAuthCertificateBuff[2] = currentSection;                //current section
        g_iapHostAuthCertificateBuff[3] = maximumSection;                //maximum section
    }
    /* Transmit whole packets with the maximum transmission payload size */
    if(currentSection < maximumSection)
    {
        transmissionSize = (g_txPayloadBufferSize - 4);
        currentCommandTxStatus = gl_ret_accessory_authentication_info(
                g_iapHostAuthCertificateBuff,
                transmissionSize, 
                g_iapHostAuthCertificateRxBuff,
                g_iapHostAuthCertificateRxBuffSize,
                s_iap_host_send_acc_certificate_ack_callback,kGeneralLingoIpodAck);
        
        /* TODO handle transmission error, validate if the command could be sent or queued and act 
         * accordingly otherwise */
        
        IapHostStateMachine.actualState = kHostWaitState;
    }
    /* This is the last packet to transmit, so the response to wait is different than the 
     * previous packet transmitted */
    else
    {
        /* In case there's a remainder, transmit the last bytes */
        if(remainder)
        {
            transmissionSize = (remainder + 4);
            currentCommandTxStatus = gl_ret_accessory_authentication_info(
                    g_iapHostAuthCertificateBuff,
                    transmissionSize, 
                    g_iapHostAuthCertificateRxBuff, 
                    g_iapHostAuthCertificateRxBuffSize,
                    s_iap_host_ack_authentication_info_callback,
                    kGeneralLingoAckAccessoryAuthenticationInfo);
            
            /* TODO handle transmission error, validate if the command could be sent or queued and act 
             * accordingly otherwise */
            
            IapHostStateMachine.actualState = kHostWaitState;
            
            remainder = 0;
        }
        IapHostStateMachine.nextState = kHostWaitState;
    }
}

/*************************************************************************************************
*
* @brief State used to read the signature from the Authentication coprocessor.  
* @return   void
* @details This function calls the right function from the coprocessor to generate the signature 
*          based on the challenge data received and then sets a state to wait for the coprocessor
*          to later send the signature. 
* @sa      IAP_HOST_CREATE_SIGNATURE
*
**************************************************************************************************/
static void s_iap_host_read_accessory_signature_state(void)
{
    int ret;

    dprint("iAP_Host_vfnReadACCSignature_State start\n");

    IAP_HOST_CREATE_SIGNATURE(g_iapChallengeData+1,
                              (uint8_t*)(&iAP_Auth_gbaRxBufferForCP[0]), 
                               sizeof(g_iapChallengeData)-1, 
                               &s_iapHostSignatureSize);
        
    IapHostStateMachine.previousState = IapHostStateMachine.actualState;  
    IapHostStateMachine.actualState = kHostWaitCoprocessorState;
    IapHostStateMachine.nextState = kHostSendAccSignatureState;
    IapHostStateMachine.timeoutState  = kHostWaitCoprocessorErrorState;
}

/*************************************************************************************************
*
* @brief State used to send the signature from the Authentication coprocessor.  
* @return   void 
* @sa      iAP_Host_vfnReadACCSignature_State
*
**************************************************************************************************/
static void s_iap_host_send_accessory_signature_state(void)
{
    command_transmission_status_t currentCommandTxStatus;

    dprint("iAP_Host_vfnSendACCSignature_State start\n");

    g_iapHostAuthCertificateBuff = &iAP_Auth_gbaRxBufferForCP[0];
    g_iapHostAuthCertificateBuffSize = s_iapHostSignatureSize;
    
    currentCommandTxStatus = gl_ret_accessory_authentication_signature(g_iapHostAuthCertificateBuff,
                    g_iapHostAuthCertificateBuffSize,
                    g_iapHostAuthCertificateRxBuff, g_iapHostAuthCertificateRxBuffSize,
                    s_iap_host_ack_authentication_signature_callback);
    
    IapHostStateMachine.actualState = kHostWaitState;

}

/* Apple device Authentication states */
#if(IAP_IOS_AUTHENTICATION_ENABLE)
/*************************************************************************************************
*
* @brief State used to request the Apple device authentication information.  
* @return   void 
* @sa      s_iap_host_ret_ipod_authentication_info_callback
*
**************************************************************************************************/
static void s_iap_host_get_ipod_auth_info_state(void)
{
    command_transmission_status_t currentCommandTxStatus;
    
    currentCommandTxStatus = gl_get_ipod_authentication_info(&iAP_Auth_CPCertificate[1], 
                                                             sizeof(iAP_Auth_CPCertificate),
                                                             &s_iapHostCPCertificateSize, 
                                                             s_iap_host_ret_ipod_authentication_info_callback
                                                             );
    
    IapHostStateMachine.previousState = IapHostStateMachine.actualState;  
    IapHostStateMachine.actualState = kHostWaitState;
}

/*************************************************************************************************
*
* @brief State used to validate the certificate returned by the Apple device.  
* @return   void 
* @sa      s_iap_host_send_ack_ipod_auth_info_state
*
**************************************************************************************************/
static void s_iap_host_validate_iOS_certificate_state(void)
{
    /* Validate iOS certificate info */
    IAP_HOST_VALIDATE_IOS_CERTIFICATE(&iAP_Auth_CPCertificate[0], &s_iapHostCertificateStatus, 
                                      s_iapHostCPCertificateSize);

    IapHostStateMachine.previousState = IapHostStateMachine.actualState;  
    IapHostStateMachine.actualState = kHostWaitCoprocessorState;
    IapHostStateMachine.nextState = kHostSendAckiPodAuthenticationInfo;
    IapHostStateMachine.timeoutState = kHostiOSAuthenticationErrorState;
}

/*************************************************************************************************
*
* @brief State used to send and acknowledge to the Apple device, reporting the status of the 
*        certificate that the Apple device sent.  
* @return   void 
* @sa      s_iap_host_send_ack_ipod_auth_info_state
*
**************************************************************************************************/
static void s_iap_host_send_ack_ipod_auth_info_state(void)
{
    command_transmission_status_t currentCommandTxStatus;
    
    currentCommandTxStatus = gl_ack_ipod_authentication_info(s_iapHostCertificateStatus, NULL, 0, NULL);
    
    if(s_iapHostCertificateStatus == kAuthInfoValid)
    {
        IapHostStateMachine.actualState = kHostCreateiOSChallengeState;
    }else
    {
        IapHostStateMachine.actualState = kHostiOSAuthenticationErrorState;
    }
    
}

/*************************************************************************************************
*
* @brief State used to create a challenge signature that will be sent to the Apple device  
* @return   void 
*
**************************************************************************************************/
static void s_iap_host_create_iOS_challenge_state(void)
{
    /* Reset Signature Retry Counter */
    s_iapHostiOSSignatureRetry = 1;
    
    /* Generate Challenge */
    IAP_HOST_CREATE_CHALLENGE((uint8_t*)(&iAP_Auth_gbaRxBufferForCP[1]),
                              (uint16_t*)(&s_iapHostSignatureSize));
    
    IapHostStateMachine.actualState = kHostWaitCoprocessorState;
    IapHostStateMachine.nextState = kHostSendiOSChallengeState;
    IapHostStateMachine.timeoutState = kHostiOSAuthenticationErrorState; 
}

/*************************************************************************************************
*
* @brief  State used to send the challenge previously created 
* @return void 
* @sa     s_iap_host_create_iOS_challenge_state  
*
**************************************************************************************************/
static void s_iap_host_send_iOS_challenge_state(void)
{
    command_transmission_status_t currentCommandTxStatus;
    if(s_iapHostiOSSignatureRetry <= IAPH_MAX_IOS_AUTH_RETRIES)
    {
        /* Set Retry Counter */
        iAP_Auth_gbaRxBufferForCP[s_iapHostSignatureSize + 1] = s_iapHostiOSSignatureRetry++;
    
        currentCommandTxStatus = gl_get_ipod_authentication_signature(&iAP_Auth_gbaRxBufferForCP[1],
                                                                      (s_iapHostSignatureSize + 1),
                                                                      &iAP_Auth_CPCertificate[1],
                                                                      s_iapHostCPCertificateSize,
                                                                      &s_iapHostCPCertificateSize,
                                                                      s_iap_host_ret_ipod_authentication_signature_callback);
    
        IapHostStateMachine.actualState = kHostWaitState;
    }else
    {
        IapHostStateMachine.actualState = kHostiOSAuthenticationErrorState;
    }
}

/*************************************************************************************************
*
* @brief State used to validate the signature sent by the Apple device
* @return   void 
*
**************************************************************************************************/
static void s_iap_host_check_iOS_signature_state(void)
{

    IAP_HOST_VALIDATE_IOS_SIGNATURE(&iAP_Auth_gbaRxBufferForCP[0], &iAP_Auth_CPCertificate[0], \
            iAP_Auth_gwCertificateSize, s_iapHostCPCertificateSize, &s_iapHostCertificateStatus);
    
    IapHostStateMachine.actualState = kHostWaitCoprocessorState;
    IapHostStateMachine.nextState = kHostAckiOSSignatureState;
    IapHostStateMachine.timeoutState = kHostiOSAuthenticationErrorState;

}

/*************************************************************************************************
*
* @brief State used to acknowledge the reception of the signature
* @return   void 
*
**************************************************************************************************/
static void s_iap_host_ack_iOS_signature_state(void)
{
    command_transmission_status_t currentCommandTxStatus;
    
    currentCommandTxStatus = gl_ack_ipod_authentication_status(s_iapHostCertificateStatus, NULL, 0, NULL);
    
    if(kAuthInfoValid == s_iapHostCertificateStatus)
    {
        g_iapHostStatus |= (1 << kHostiOSAuthenticationDone);
        if(!(g_iapHostStatus & (1 << kHostIdpsNotSupported)))
        {
            IapHostStateMachine.actualState = kHostEssentialRoutinesDone;
        }else
        {
            IapHostStateMachine.actualState = kHostWaitState;
        }
    }else
    {
        g_iapHostStatus |= (1 << kHostIosAuthenticationError);
        IapHostStateMachine.actualState = kHostiOSAuthenticationErrorState;
    }
    
}

/*************************************************************************************************
*
* @brief State to handle iOS authentication errors
*  
* @return   void    
*
**************************************************************************************************/
static void s_iap_host_iOS_authentication_error_state(void)
{
    if(s_iapHostiOSAuthRetriesCounter < IAPH_MAX_IOS_AUTH_RETRIES)
    {
        s_iapHostiOSAuthRetriesCounter++;
        IapHostStateMachine.actualState = kHostGetiPodAuthenticationInfo;
    }else
    {
        g_iapHostStatus |= (1 << kHostIosAuthenticationError);
        
        IapHostStateMachine.actualState = kHostEssentialRoutinesDone;
        
    }
}

#endif



/*************************************************************************************************/
/*                      Asynchronous Commands Callbacks                                          */
/*************************************************************************************************/
/*************************************************************************************************
*
* @brief This callback is called when asynchronous Request Identify is received when ipod has gone 
*        sleep and woke up   
* @return   void 
* @sa      iap_host_request_identify_callback
*
**************************************************************************************************/
void s_iap_host_request_identify_callback(iap_interface_buffer_rx_frame_t* rxFrame)
{
    dprint("iap_host_request_identify_callback start\n");

    /* rxFrame unsued for this state since no payload is received */
    (void)rxFrame;
 
    g_enabledFIDTokens = IAP_CMD_ENABLED_FID_TOKENS_MASK;
    g_iapHostLingoNumber = 0;
    s_iapHostTokenNumber = 0;
    iap_commands_reset();
    
    /* Make again the Start IDPS process to reidentify */
    IapHostStateMachine.actualState = kHostStartIdpsState;
 
}

/*************************************************************************************************
* 
* @brief This callback is called when asynchronous GetAccessoryAuthenticationInfo has been received
* @return   void 
* @sa      iap_host_get_accessory_authentication_info_callback
* 
**************************************************************************************************/
static inline void s_iap_host_get_accessory_authentication_info_callback(iap_interface_buffer_rx_frame_t* rxFrame)
{
    dprint("iap_host_get_accessory_authentication_info_callback start\n");

    /* rxFrame unused for this callback */
    (void)rxFrame;
    /* Set the state to read the accessory certificate to return it later in multisection packets */
    IapHostStateMachine.actualState = kHostReadAccCertificateState;
    /* This flag is used to initially calculate the multisection packets to be sent  */
    s_iapHostAccessoryAuthInfoPacketCounter = 0;
}

/*************************************************************************************************
* 
* @brief This callback is called when asynchronous GetAccessoryAuthenticationSignature has been 
*        received
* @return   void 
* @sa      iap_host_get_accessory_authentication_signature_callback
* 
**************************************************************************************************/
static inline void s_iap_host_get_accessory_authentication_signature_callback(iap_interface_buffer_rx_frame_t* rxFrame)
{
    dprint("iap_host_get_accessory_authentication_signature_callback start\n");

    (void)rxFrame;
    
    /* Set the state to read the accessory signature to return it later with RetAccAuthSignature */
    IapHostStateMachine.actualState = kHostReadAccSignatureState;
}

/*************************************************************************************************
* 
* @brief This callback is called when asynchronous notification of ipod state change has been
*        received, Three states are possible, POWER_ON, SLEEP and HIBERNATE
* @return   void 
* @sa      iap_host_notify_ipod_state_change_callback
* 
**************************************************************************************************/
static void s_iap_host_notify_ipod_state_change_callback(iap_interface_buffer_rx_frame_t* rxFrame)
{
    if(kIpodGoingToPowerOn == (rxFrame->payload[0]))
    {
        /* iOS in run mode */
        IapHostStateMachine.actualState = kHostWaitState;
        g_iapHostStatus &= ~((1 << kHostIosInLowPowerMode) | (1 << kHostIosInHibernateMode));
    }
    else if(kIpodGoingToSleep == (rxFrame->payload[0]))
    {
        /* TODO Make the necessary things to be in low power mode */
    }
    /* Going to hibernation preserving menu and playback status and not preserving them */
    /* Synchronization process must be send first to come out the low power mode */
    else if((kIpodGoingToHibernateNotPreservMenu == rxFrame->payload[0]) || 
            (kIpodGoingToHibernatePreservMenu == rxFrame->payload[0]))
    {
        g_iapHostStatus |= (1<<kHostIosInHibernateMode);
        g_iapHostStatus &= ~(1<<kHostIosInLowPowerMode);
        IapHostStateMachine.actualState = kHostIdleState;
    }
}

/*************************************************************************************************
* 
* @brief This callback is called when asynchronous command GetAccessoryInfo has been
*        received
* @return   void 
* 
**************************************************************************************************/
static void s_iap_host_get_accessory_info_callback(iap_interface_buffer_rx_frame_t* rxFrame)
{
    if(kAccessoryCapabilities == rxFrame->payload[0])
    {
        IapHostStateMachine.actualState = kHostContinueIdentifyDeviceLingoesState;
    }else
    {
        IapHostStateMachine.actualState = kHostIdentifyDeviceLingoesDelayState;
    }
    
}

/* END ASYNC COMMANDS ***************************************************************************/
/*************************************************************************************************
* 
* @brief This callback is called when ack of AccessoryCertificate has been received
* @return   void 
* @sa      iap_host_send_acc_certificate_ack_callback
* 
**************************************************************************************************/

static void s_iap_host_send_acc_certificate_ack_callback (uint8_t codeOfAck)
{
    dprint("iap_host_send_acc_certificate_ack_callback start\n");
    (void) codeOfAck; /* Ignores reception status */
    if(kAuthInfoSupported == g_iapHostAuthCertificateRxBuff[0])
    {
        IapHostStateMachine.actualState = kHostReadAccCertificateState;
    }
}

/*************************************************************************************************
* 
* @brief This callback is called upon the reception of the previously sent command 
*        GetAccessoryAuthenticationInfo
* @return   void 
* @sa      iap_host_notify_ipod_state_change_callback
* 
**************************************************************************************************/
static void s_iap_host_ack_authentication_info_callback (uint8_t codeOfAck)
{
    (void) codeOfAck; 
    if(kAuthInfoSupported == g_iapHostAuthCertificateRxBuff[0])
    {
        IapHostStateMachine.actualState = kHostWaitState;
    }else if (kAuthInfoNotSupported == g_iapHostAuthCertificateRxBuff[0])
    {
        g_txPayloadBufferSize = kiAPIDefaultMaxPayloadSizeSpec;
    }
}

/************************************************************************************************
* 
* @brief This callback is called when the Apple device sends an AckAccessoryAuthenticationInfo 
*          command in response to a RetAccessoryAuthenticationInfo command, if the received data 
*          is an AUTH_INFO_SUPPORTED then the accessory should wait for the asynchronous command 
*          GetAccessoryAuthenticationSignature in order to continue with the Authentication process
* @return   void 
* @sa      iap_host_ack_authentication_status_callback
* 
**************************************************************************************************/
static void s_iap_host_ack_authentication_signature_callback (uint8_t codeOfAck)
{
    (void) codeOfAck; /* Ignores reception status */
    if(kAuthInfoSupported == g_iapHostAuthCertificateRxBuff[0])
    {
        dprint("=========== AUTH SUCCEEDED ==============\n");
        /* Accessory authentication process finished successfully */
        g_iapHostStatus |= (1 << kHostAccessoryAuthenticationDone);
    
#if(IAP_IOS_AUTHENTICATION_ENABLE)
        s_iapHostiOSAuthRetriesCounter = 0;
        IapHostStateMachine.actualState = kHostGetiPodAuthenticationInfo; 
        
#else
        if(!(g_iapHostStatus & (1 << kHostIdpsNotSupported)))
        {
            IapHostStateMachine.actualState = kHostEssentialRoutinesDone;
        }else
        {
            IapHostStateMachine.actualState = kHostWaitState;
        }
#endif    

    }else
    {
        IapHostStateMachine.actualState = kHostAccessoryIdpsAuthenticationErrorState;
    }

#if(MFICFG_PLATFORM_MQX)
    iAP_Auth_bfnDeInit();
#endif

}
#if(IAP_IOS_AUTHENTICATION_ENABLE)
/************************************************************************************************
* 
* @brief This callback is called once the accessory has received all the Authentication information
*        requested. The information can be divided in multiple packet,
*        in that case the callback will be called until all the packet are received
* @return   void 
*
**************************************************************************************************/
static inline void s_iap_host_ret_ipod_authentication_info_callback (uint8_t codeOfAck)
{
    (void) codeOfAck; 
    IapHostStateMachine.actualState = kHostValidateiOSCertificateState;
}


/************************************************************************************************
* 
* @brief This callback is called once the accessory has received the command
*        RetiPodAuthenticationSignature, in response to a GetiPodAuthenticationSignature
* @return   void  
* 
**************************************************************************************************/
static inline void s_iap_host_ret_ipod_authentication_signature_callback(uint8_t codeOfAck)
{
    (void) codeOfAck; /* Ignores reception status */
    IapHostStateMachine.actualState = kHostCheckiOSSignatureState;
}

#endif

/*************************************************************************************************
* 
* @brief This callback is called when a new sampling rate is received for digital audio
* @return   void 
* 
**************************************************************************************************/

void iap_host_track_new_audio_attributes(iap_interface_buffer_rx_frame_t *rxFrame)
{
    dprint("iap_host_track_new_audio_attributes start\n");

    uint32_t sample_rate = *((uint32_t*) &rxFrame->payload[0]);

    iAP_TrackNewAttributes(sample_rate);

}


/* Read iAP_Host.h file for function documentation */
void iap_host_handle_ios_disconnection(void)
{    
    dprint("iAP_Host_vfnHandleIOS_Disconnection start\n");
    
    /* XXX The uninit of iAP1 is done by the iAP2_Host_Uninit function.
     * This implementation is only valid for CAS because iAP1 can only work 
     * if iAP2 is also enabled but in a stand-alone implementation this routine
     * MUST be called accordingly */
#ifndef MFI_IAP2
    iap_host_uninitialization();
#endif

    IapHostStateMachine.actualState = kHostIdleState;
    
    if(s_iapHostDeviceDisconnectedCallback != NULL)
    {
        s_iapHostDeviceDisconnectedCallback();
    }
    dprint("iAP_Host_vfnHandleIOS_Disconnection end\n");

    /* Clear status flags, leave only the hibernate flag if the device went to hibernate */
    g_iapHostStatus &= ~(1 << kHostIosInHibernateMode); 
    
}

/* Read iAP_Host.h file for function documentation */
uint8_t iap_host_register_device_ready_callback(void(*Callback)(void))
{
    uint8_t returnValue = _ERROR_; 
    if (NULL != Callback)
    {
        s_iapHostDeviceReadyCallback = Callback;
        returnValue = _OK_;
    }
    return (returnValue); 
}

/* Read iAP_Host.h file for function documentation */
uint8_t iap_host_register_device_disconnection_callback(void(*Callback)(void))
{
    uint8_t returnValue = _ERROR_; 
    if (NULL != Callback)
    {
        s_iapHostDeviceDisconnectedCallback = Callback;
        returnValue = _OK_;
    }
    return (returnValue); 
}

/* Read iAP_Host.h file for function documentation */
uint8_t iap_host_register_new_frequency_callback(void(*callback)(uint16_t))
{
    uint8_t returnValue = _ERROR_; 
    if (NULL != callback)
    {
        s_iapHostNewFrequencyAppCallback = callback;
        returnValue = _OK_;
    }
    return (returnValue); 
}

/* Read iAP_Host.h file for function documentation */
void iap_host_state_machine_driver(void)
{
    iAP_Host_vfnapHostDriver[IapHostStateMachine.actualState]();
    iap_interface_periodical_task();

    if(true == iAPI_RxCallbackFlag)
    {
        /* Clear flag first so that if an interrupt/callback occur after clearing it, the reception would still be
         * enabled. Otherwise, reception could be disabled and the flag cleared, stopping the communication.
         */
        iAPI_RxCallbackFlag = false;
        iAP_Transport_DataReceive();
    }

}

void iAP_Host_bfnEventsCallback(uint8_t eventFlag)
{
    dprint("iAP_Host_bfnEventsCallback start: %d\n", eventFlag);

    osa_mutex_lock(&cpMutex);
    cpFlag = 1;
    osa_mutex_unlock(&cpMutex);

    osa_cond_signal(&cpEvent);
}


/* Read iAP_Host.h file for function documentation */
uint8_t iap_host_request_identification_procces_type(void)
{
    if(!(g_iapHostStatus & (1 << kHostIdpsNotSupported))){
        return kHostIdpsProcessDevice;
    } else {
        return kHostNonIdpsProcessDevice;
    }
}

