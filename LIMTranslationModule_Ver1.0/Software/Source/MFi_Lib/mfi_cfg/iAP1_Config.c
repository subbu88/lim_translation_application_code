/*HEADER******************************************************************************************
 *
 * Copyright 2013 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP_Config.h"
#include "iAP1_Config.h"

/*************************************************************************************************/
/*                                  Defines & Macros Section                                     */
/*************************************************************************************************/ 
#define EA_PROTOCOL_TOKEN_SIZE            (EA_PROTOCOL_TOKEN_MASK*(EA_PROTOCOL_STRING_SIZE + 4) - 1)
#define BUNDLE_SEED_ID_PREF_TOKEN_SIZE    (BUNDLE_SEED_ID_PREF_TOKEN_MASK * (BUNDLE_SEED_ID_STRING_SIZE + 3) - 1)

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/
/*! EA Protocol token array */
const uint8_t s_kFidEaProtocolTokenValues[] =
{
    #if(EA_PROTOCOL_TOKEN_MASK == _TRUE_)
        EA_PROTOCOL_TOKEN_SIZE,             /* length (1 byte) = Protocol String + 3 = 0x18 */
        0x00,                               /* FIDType = 0x00 */
        kEAProtocolTokenFIDSubtype,         /* FIDSubtype = 0x04 */
        0x01,                               /* Protocol Index (1 byte)*/
        EA_PROTOCOL_STRING_DATA             /* Protocol String = "com.freescale.www" */
    #endif
};


/*! Bundle Seed ID token array */
const uint8_t s_kFidBundleSeedIdTokenValues[] =
{
    #if(BUNDLE_SEED_ID_PREF_TOKEN_MASK  == _TRUE_)
        BUNDLE_SEED_ID_PREF_TOKEN_SIZE,         /* length (1 byte) = 0x0D */
        0x00,                                   /* FIDType = 0x00 */ 
        kBundleSeedIDPrefTokenFIDSubtype,       /* FIDSubtype = 0x05 */
        BUNDLE_SEED_ID_STRING_DATA              /* Bundle Seed ID string = "A1B2C3D4E5" */
    #endif
};
 
/*! EA Protocol Metadata token array */
const uint8_t s_kFidEaProtocolMetadataTokenValues[] =
{
    #if(EA_PROTOCOL_METADATA_TOKEN_MASK  == _TRUE_)
        0x04,                               /* length (1 byte) = 0x04 */
        0x00,                               /* FIDType = 0x00 */
        kEAProtocolMetadataTokenFIDSubtype, /* FIDSubtype = 0x08 */
        0x01,                               /* Protocol Index (1 byte)*/
        DEFAULT_EA_PROTOCOL_METADATA        /* Metadata Type = Do not Match Protocol Index */
    #endif
};
