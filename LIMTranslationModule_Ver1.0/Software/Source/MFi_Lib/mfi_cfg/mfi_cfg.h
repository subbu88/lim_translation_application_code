#ifndef _MFI_CFG_
#define _MFI_CFG_

#ifndef _TRUE_
#define _TRUE_ 1
#endif

#ifndef _FALSE_
#define _FALSE_  0
#endif

/*
* Target Setup
*/
#define MFICFG_CPU_IMX6                 (_TRUE_)
#define MFICFG_ARCH_CORTEX_A            (_TRUE_)
#define MFICFG_BOARD_IMX6_SABRE_AUTO    (_TRUE_)
#define MFICFG_PLATFORM_LINUX           (_TRUE_)

#ifdef MFI_OVERRIDE_DEFAULT_CONFIGURATION
/*
 * MFi IAP1 Configuration
 */
#define MFI_IAP1_ENABLE (_FALSE_)
/*
 * IAP1 Features
 */
#define MFI_IAP1_MAX_APPLICATION_COMMAND_RETRIES (_TRUE_)
#define MFI_IAP1_MAX_PAYLOAD 128

/*
 * IAP1 IDPS configuration
 */

/*
 * IAP1 IDPS FID Token Enablement
 */
#define IDENTIFY_TOKEN_MASK                         (_TRUE_)
#define ACC_CAPABILITY_TOKEN_MASK                   (_TRUE_)

/*
 * Accessory Information Token
 */
#define ACC_INFO_TOKEN_MASK                         (_TRUE_)
#define ACC_INFO_NAME_TOKEN_MASK                    (_TRUE_)
#define ACC_INFO_FIRMWARE_VERSION_TOKEN_MASK        (_TRUE_)
#define ACC_INFO_HARDWARE_VERSION_TOKEN_MASK        (_TRUE_)
#define ACC_INFO_MANUFACTURER_TOKEN_MASK            (_TRUE_)
#define ACC_INFO_MODEL_NUMBER_TOKEN_MASK            (_TRUE_)
#define ACC_INFO_SERIAL_NUMBER_TOKEN_MASK           (_TRUE_)
#define ACC_INFO_MAX_PAYLOAD_SIZE_TOKEN_MASK        (_TRUE_)
#define ACC_INFO_STATUS_TOKEN_MASK                  (_FALSE_)
#define ACC_INFO_RF_CERTIFICATION_TOKEN_MASK        (_TRUE_)
#define ACC_DIGITAL_AUDIO_SAMPLE_RATE_TOKEN_MASK    (_TRUE_)            
#define ACC_DIGITAL_AUDIO_VIDEO_DELAY_TOKEN_MASK    (_FALSE_)           

/*
 * IAP1 Accessory Capability Mask
 */
#define ANALOG_LINE_IN_MASK                         (_FALSE_)
#define USB_AUDIO_OUT_MASK                          (_TRUE_)
#define COMM_WITH_IOS_APPLICATIONS_MASK             (_TRUE_)
#define CHECK_IOS_VOLUME_MASK                       (_FALSE_)
#define HANDLE_ASYNC_PLAYBACK_MASK                  (_FALSE_)
#define HANDLE_MULTI_PACKET_MASK                    (_FALSE_)


/*
 * Accessory Status Notifocation
 */
#define BLUETOOTH_DEVICE_STATUS                     (_TRUE_)
#define FAULT_CONDITION_STATUS                      (_TRUE_)

/*
 * Accessory RF Certification
 */
#define IAP1_RF_CLASSTRUE                                (_FALSE_)
#define IAP1_RF_CLASS2                                   (_FALSE_)
#define IAP1_RF_CLASS4                                   (_FALSE_)
#define IAP1_RF_CLASS5                                   (_FALSE_)

/*
 * iPod Prefernces
 */
#define IPOD_PREFERENCE_TOKEN_MASK                  (_TRUE_)
#define IPOD_PREF_VIDEO_OUT_SETTINGS_MASK           (_FALSE_)
#define IPOD_PREF_SCREEN_CONFIGURATION_MASK         (_FALSE_)
#define IPOD_PREF_VIDEO_SIGNAL_FORMAT_MASK          (_FALSE_)
#define IPOD_PREF_LINE_OUT_USAGE_MASK               (_FALSE_)
#define IPOD_PREF_VIDEO_OUT_CONNECTION_MASK         (_FALSE_)
#define IPOD_PREF_CLOSED_CAPTIONING_MASK            (_FALSE_)
#define IPOD_PREF_VIDEO_ASPECT_RATIO_MASK           (_FALSE_)
#define IPOD_PREF_SUBTITLES_MASK                    (_FALSE_)
#define IPOD_PREF_VIDEO_ALTERNATE_AUDIO_MASK        (_FALSE_)
#define IPOD_PREF_PAUSE_ON_POWER_REMOVAL_MASK       (_TRUE_)
#define IPOD_PREF_VOICE_OVER_MASK                   (_TRUE_)
#define IPOD_PREF_ASSISTIVE_TOUCH_MASK              (_FALSE_)

/*
 * Lingoes Supported
 */
#define IAP_GENERALLINGO_MASK                       (_TRUE_)
#define IAP_SIMPLEREMOTELINGO_MASK                  (_TRUE_)
#define IAP_DISPLAYREMOTELINGO_MASK                 (_TRUE_)
#define IAP_EXTENDEDINTERFACELINGO_MASK             (_TRUE_)
#define IAP_USBHOSTMODELINGO_MASK                   (_FALSE_)
#define IAP_RFTUNERLINGO_MASK                       (_FALSE_)
#define IAP_SPORTSLINGO_MASK                        (_FALSE_)
#define IAP_DIGITALAUDIOLINGO_MASK                  (_TRUE_)
#define IAP_STORAGELINGO_MASK                       (_FALSE_)
#define IAP_IPODOUTLINGO_MASK                       (_FALSE_)
#define IAP_LOCATIONLINGO_MASK                      (_FALSE_)




/*
 * IAP2 Configuration
 */
#define MFI_IAP2_ENABLE                                      (_TRUE_)

/*
 * iAP2 Accessory Configuration Strings 
 */ 
//#define IAP2_IDENT_NAME_STRING_DATA                         "MFi SDK 2.0 Test Suite Demo" //NXP MFI name changes
#define IAP2_IDENT_NAME_STRING_DATA                         "Philips Lumify Ver 1.0"
#define IAP2_IDENT_MODEL_ID_STRING_DATA                     "ABCD1234"
#define IAP2_IDENT_MANUFACTURER_STRING_DATA                 "Freescale"
#define IAP2_IDENT_SERIAL_NUM_STRING_DATA                   "123456789"
#define IAP2_IDENT_FW_VERSION_STRING_DATA                   "001"
#define IAP2_IDENT_HW_VERSION_STRING_DATA                   "001"
/*
 *  External Accessory Protocol
 */
#define IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL               (_TRUE_)
//#define IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA0              "com.freescaleM.EADemo" //NXP MFI name changes
#define IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA0                "com.Philips.Lumify"

/*
 * Human Device Interface Usage Component
 */
#define IAP2_IDENT_HID_COMPONENT   			(_TRUE_) 
#define IAP2_IDENT_USB_HOST_HID_COMPONENT   (_FALSE_)
#define IAP2_HID_MEDIA_PLAYBACK_EN			(_TRUE_)
#define IAP2_HID_PLAY_EN					(_FALSE_)
#define IAP2_HID_PAUSE_EN					(_FALSE_)
#define IAP2_HID_PLAY_PAUSE_EN				(_TRUE_)
#define IAP2_HID_SCAN_NEXT_TRACK_EN			(_TRUE_)
#define IAP2_HID_SCAN_PREV_TRACK_EN			(_TRUE_)
#define IAP2_HID_VOL_UP_EN					(_FALSE_)
#define IAP2_HID_VOL_DOWN_EN                (_FALSE_)
#define IAP2_HID_RANDOM_EN                  (_FALSE_)
#define IAP2_HID_REPEAT_EN                  (_FALSE_)
#define IAP2_HID_TRACK_NORM_EN              (_FALSE_)
#define IAP2_HID_TRACK_INC_EN				(_FALSE_)
#define IAP2_HID_TRACK_DEC_EN               (_FALSE_)
#define IAP2_HID_MUTE_EN                    (_TRUE_)
#define KEY_BOARD_COUNTRY_CODE_PRESENT		(_FALSE_)

#define IAP2_HID_USAGE_CNT     (IAP2_HID_PLAY_EN + IAP2_HID_PAUSE_EN +  IAP2_HID_PLAY_PAUSE_EN \
                                +  IAP2_HID_SCAN_NEXT_TRACK_EN + IAP2_HID_SCAN_PREV_TRACK_EN + IAP2_HID_VOL_UP_EN \
                                +  IAP2_HID_VOL_DOWN_EN + IAP2_HID_RANDOM_EN +  IAP2_HID_REPEAT_EN + IAP2_HID_TRACK_NORM_EN \
                                +  IAP2_HID_TRACK_INC_EN + IAP2_HID_TRACK_DEC_EN + IAP2_HID_MUTE_EN )


/*
 * iAP2 Features
 */
#define IAP2_FEAT_AUTH                  (_TRUE_)
#define IAP2_FEAT_ID                    (_TRUE_)
#define IAP2_FEAT_APP_LAUNCH            (_TRUE_)
#define IAP2_FEAT_ASSIST_TOUCH	        (_TRUE_)
#define IAP2_FEAT_BT			        (_FALSE_)
#define IAP2_FEAT_EA			        (_TRUE_)
#define IAP2_FEAT_HID			        (_TRUE_)  
#define IAP2_FEAT_LOC			        (_FALSE_)
#define IAP2_FEAT_MEDIA_LIB		        (_TRUE_)
#define IAP2_FEAT_NOW_PLAY		        (_TRUE_)
#define IAP2_FEAT_POWER			        (_TRUE_)
#define IAP2_FEAT_USB_AUDIO		        (_FALSE_)
#define IAP2_FEAT_VOICE_OVER	        (_FALSE_)
#define IAP2_FEAT_WIFI			        (_FALSE_)
#define IAP2_COMM_WITH_IOS_FEAT         (_FALSE_)
#define IAP2_DEVICE_AUTHENTICATION      (_FALSE_) 
#define IAP2_VEHICAL_STATUS             (_FALSE_)

/*
 * iAP2 Feature Control Messages
 */
#define IAP2_CTRL_MSG_ACCESSORY_AUTHENTICATION				(_FALSE_)
#define IAP2_CTRL_MSG_ACCESSORY_IDENTIFICATION				(_FALSE_)
#define IAP2_CTRL_MSG_APP_LAUNCH							(_TRUE_)
#define IAP2_CTRL_MSG_ASSISTIVE_TOUCH						(_TRUE_)
#define IAP2_CTRL_MSG_BLUETOOTH_STATUS						(_FALSE_)
#define IAP2_CTRL_MSG_DEVICE_AUTHENTICATION					(_FALSE_)
#define IAP2_CTRL_MSG_EXT_ACC_PROTOCOL						(_TRUE_)
#define IAP2_CTRL_MSG_HUMAN_INTERFACE_DEVICE				(_TRUE_)  
#define IAP2_CTRL_MSG_LOCATION								(_FALSE_)
#define IAP2_CTRL_MSG_MEDIA_LIBRARY_ACCESS					(_TRUE_)
#define IAP2_CTRL_MSG_NOW_PLAYING							(_TRUE_)
#define IAP2_CTRL_MSG_POWER									(_TRUE_)
#define IAP2_CTRL_MSG_USB_DEVICE_MODE_AUDIO					(_FALSE_)
#define IAP2_CTRL_MSG_VOICE_OVER							(_FALSE_)
#define IAP2_CTRL_MSG_WIFI_INFO_SHARING						(_FALSE_)

/*
 * Preferred App Bundle Seed Identifier
 */
#define IAP2_IDENT_PREF_APP_BUNDLE_SEED_ID                  (_FALSE_)

/*
 * iAP2 Media Library Properties
 */
#define iAP2_MEDIA_LIBRARY_ITEM_PROPERTIES_ENABLE			(_TRUE_)
#define iAP2_MEDIA_LIBRARY_PLAYLIST_PROPERTIES_ENABLE			(_TRUE_)

/*
 * iAP2 Sessions
 */
#define IAP2_SESSIONS_FILE_TRANS_ENABLE			(_TRUE_)
#define IAP2_SESSIONS_EXT_ACC_ENABLE			(_TRUE_)

#define IAP2_SESSIONS_CTRL_SM_ENABLE			(_FALSE_)
#define IAP2_SESSIONS_FILE_TRANS_SM_ENABLE		(_FALSE_)
#define IAP2_SESSIONS_EXT_ACC_SM_ENABLE			(_TRUE_)

#define IAP2_SESSIONS_CTRL_RESET_ENABLE			(_TRUE_)
#define IAP2_SESSIONS_FILE_TRANS_RESET_ENABLE	(_TRUE_)
#define IAP2_SESSIONS_EXT_ACC_RESET_ENABLE		(_TRUE_)


/*
 * MFi Transport Config
 */
#define MFI_TRANSPORT_USB_HOST_MODE         (_TRUE_)
#define MFI_TRANSPORT_USB_DEVICE_MODE       (_FALSE_)
#define MFI_TRANSPORT_UART                  (_FALSE_)
#define IAP2_IDENT_SERIAL_TRANSPORT_COMPONENT				(_FALSE_)
#define IAP2_IDENT_SERIAL_TRANSPORT_SUPPORTS_IAP2			(_FALSE_)
#define IAP2_IDENT_TRANSPORT_UART                           (_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT              (_TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_SUPPORTS_IAP2          (_TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_8000HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_11025HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_12000HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_16000HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_22050HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_24000HZ		(_FALSE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_32000HZ		(_TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_44100HZ		(_TRUE_)
#define IAP2_IDENT_USB_DEV_TRANSPORT_COMPONENT_48000HZ		(_TRUE_)
#define IAP2_IDENT_USB_HOST_TRANSPORT_COMPONENT             (_TRUE_)
#define IAP2_IDENT_USB_HOST_TRANSPORT_SUPPORTS_IAP2         (_TRUE_)
#define IAP2_IDENT_BLUETOOTH_TRANSPORT_COMPONENT   			(_FALSE_)


/* 
 * Digital Audio configuration
 */
#define MFI_DIGITAL_AUDIO_ENABLE                             (_FALSE_)
#define MFI_AUDIO_SYNCHRONOUS_MODE                           (_FALSE_)
#define MFI_AUDIO_TCO_MODE                                   (_FALSE_)
#define MFI_SGTL_ENABLE                                      (_TRUE_)
#define AUDIO_CLASS_2_0                                      (_TRUE_)

/* 
 * Microcontroller selection
 */
#define MFI_KL25_ENABLE                                      (_FALSE_)

/*
 * Device charging configuration
 */
#define MFI_CHARGE_CURRENT_AVAILABLE                         0
#define MFI_CHARGE__TRUE_                                    (_TRUE_)
#define MFI_CHARGE_ENABLED                                   (_TRUE_)

#define IAP2_PWR_TYPE_SELF				0x02
#define IAP2_PWR_TYPE_NONE				0x00

#define IAP2_IDENT_PWR_SOURCE_TYPE_DATA		 				(IAP2_PWR_TYPE_NONE)


/* 
 * Number of allowed communication applications
 */
#define MFI_MAX_APPS_NUMBER                                  2

#endif

#endif // _MFI_CFG_
