/*HEADER******************************************************************************************
 *
 * Copyright 2015 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP_Config.h"
#include "iAP2_Config.h"

#define GET_U16_MS_BYTE(value)								((uint8_t)((value & 0xFF00)>>8))	
#define GET_U16_LS_BYTE(value)								((uint8_t)(value & 0x00FF))

/*************************************************************************************************/
/*                                   Global Constants Section                                    */
/*************************************************************************************************/
/*-----------------------------------------------------------------------------------------------*/
/* Name | Parameter ID 0x00 */
const uint8_t iAP2_Identification_baName[] = IAP2_IDENT_NAME_STRING_DATA;
const uint16_t iAP2_Identification_wNameSize = sizeof(IAP2_IDENT_NAME_STRING_DATA);
/*-----------------------------------------------------------------------------------------------*/
/* Model identifier | Parameter ID 0x01 */
const uint8_t iAP2_Identification_baModel[] = IAP2_IDENT_MODEL_ID_STRING_DATA;
const uint16_t iAP2_Identification_wModelSize = sizeof(IAP2_IDENT_MODEL_ID_STRING_DATA);
/*-----------------------------------------------------------------------------------------------*/
/* Manufacturer | Parameter ID 0x02 */
const uint8_t iAP2_Identification_baManufacturer[] = IAP2_IDENT_MANUFACTURER_STRING_DATA;
const uint16_t iAP2_Identification_wManufacturerSize = sizeof(IAP2_IDENT_MANUFACTURER_STRING_DATA);
/*-----------------------------------------------------------------------------------------------*/
/* Serial number | Parameter ID 0x03 */
const uint8_t iA2_Identification_baSerialNum[] = IAP2_IDENT_SERIAL_NUM_STRING_DATA;
const uint16_t iA2_Identification_wSerialNumSize = sizeof(IAP2_IDENT_SERIAL_NUM_STRING_DATA);
/*-----------------------------------------------------------------------------------------------*/
/* Firmware version | Parameter ID 0x04 */
const uint8_t iAP2_Identification_baFwVersion[] = IAP2_IDENT_FW_VERSION_STRING_DATA;
const uint16_t iAP2_Identification_wFwVersionSize = sizeof(IAP2_IDENT_FW_VERSION_STRING_DATA);
/*-----------------------------------------------------------------------------------------------*/
/* Hardware version | Parameter ID 0x05 */
const uint8_t iAP2_Identification_baHwVersion[] = IAP2_IDENT_HW_VERSION_STRING_DATA;
const uint16_t iAP2_Identification_wHwVersionSize = sizeof(IAP2_IDENT_HW_VERSION_STRING_DATA);

/*-----------------------------------------------------------------------------------------------*/
/* Supported External Accessory Protocol | Parameter ID 0x0A */
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL == _TRUE_)
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 0)
const uint8_t iAP2_Identification_baSupportedExtAccProtID0 = IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA0;
const uint16_t iAP2_Identification_wSupportedExtAccProtIDSize = sizeof(iAP2_Identification_baSupportedExtAccProtID0);

const uint8_t iAP2_Identification_baSupportedExtAccProtName0[] = IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA0;
const uint16_t iAP2_Identification_wSupportedExtAccProtName0Size = sizeof(iAP2_Identification_baSupportedExtAccProtName0);

const uint8_t iAP2_Identification_bSupportedExtAccProtMatch0 = IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA0;
const uint16_t iAP2_Identification_wSupportedExtAccProtMatchSize = sizeof(iAP2_Identification_bSupportedExtAccProtMatch0);

const uint16_t iAP2_Identification_wSupportedExtAccProtNativeTransIDSize = sizeof(uint16_t);

//#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID0 == _TRUE_)
const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID0[] = 
{
        GET_U16_MS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA0),
        GET_U16_LS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA0),
};
//#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 1)
const uint8_t iAP2_Identification_baSupportedExtAccProtID1 = IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA1;

const uint8_t iAP2_Identification_baSupportedExtAccProtName1[] = IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA1;
const uint16_t iAP2_Identification_wSupportedExtAccProtName1Size = sizeof(iAP2_Identification_baSupportedExtAccProtName1);

const uint8_t iAP2_Identification_bSupportedExtAccProtMatch1 = IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA1;

#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID1 == _TRUE_)
const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID1[] = 
{
        GET_U16_MS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA1),
        GET_U16_LS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA1),
};
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 2)
const uint8_t iAP2_Identification_baSupportedExtAccProtID2 = IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA2;

const uint8_t iAP2_Identification_baSupportedExtAccProtName2[] = IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA2;
const uint16_t iAP2_Identification_wSupportedExtAccProtName2Size = sizeof(iAP2_Identification_baSupportedExtAccProtName2);

const uint8_t iAP2_Identification_bSupportedExtAccProtMatch2 = IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA2;

#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID2 == _TRUE_)
const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID2[] = 
{
        GET_U16_MS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA2),
        GET_U16_LS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA2),
};
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 3)
const uint8_t iAP2_Identification_baSupportedExtAccProtID3 = IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA3;

const uint8_t iAP2_Identification_baSupportedExtAccProtName3[] = IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA3;
const uint16_t iAP2_Identification_wSupportedExtAccProtName3Size = sizeof(iAP2_Identification_baSupportedExtAccProtName3);

const uint8_t iAP2_Identification_bSupportedExtAccProtMatch3 = IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA3;

#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID3 == _TRUE_)
const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID3[] = 
{
        GET_U16_MS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA3),
        GET_U16_LS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA3),
};
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 4)
const uint8_t iAP2_Identification_baSupportedExtAccProtID4 = IAP2_IDENT_EXT_ACC_PROTOCOL_ID_DATA4;

const uint8_t iAP2_Identification_baSupportedExtAccProtName4[] = IAP2_IDENT_EXT_ACC_PROTOCOL_NAME_DATA4;
const uint16_t iAP2_Identification_wSupportedExtAccProtName4Size = sizeof(iAP2_Identification_baSupportedExtAccProtName4);

const uint8_t iAP2_Identification_bSupportedExtAccProtMatch4 = IAP2_IDENT_EXT_ACC_PROTOCOL_MATCH_DATA4;

#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID4 == _TRUE_)
const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID4[] = 
{
        GET_U16_MS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA4),
        GET_U16_LS_BYTE(IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID_DATA4),
};
#endif
#endif

#endif
/*-----------------------------------------------------------------------------------------------*/
/* Preferred App Bundle Seed Identifier | Parameter ID 0x0B */
#if (IAP2_IDENT_PREF_APP_BUNDLE_SEED_ID == _TRUE_)
const uint8_t iAP2_Identification_baPrefAppBundleSeedId[] = IAP2_IDENT_PREF_APP_BUNDLE_SEED_ID_DATA;
const uint16_t iAP2_Identification_wPrefAppBundleSeedIdSize = sizeof(iAP2_Identification_baPrefAppBundleSeedId);
#endif

const IAP2_FEAT_EA_CBK iAP2_Feat_EA_ProtCallbacks[IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES] = 
{
    IAP2_FEAT_EA_PROT0_CALLBACK
    #if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 1)
    ,IAP2_FEAT_EA_PROT1_CALLBACK
    #endif
    #if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 2)
    ,IAP2_FEAT_EA_PROT2_CALLBACK
    #endif
    #if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 3)
    ,IAP2_FEAT_EA_PROT3_CALLBACK
    #endif
    #if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 4)
    ,IAP2_FEAT_EA_PROT4_CALLBACK
    #endif
};

/*************************************************************************************************/
/*                                      Functions Section                                        */
/*************************************************************************************************/

#if defined(__IAR_SYSTEMS_ICC__)
__weak
#else
__attribute__ ((weak))
#endif
void IAP2_FEAT_EA_PROT0_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus)
{
    (void)DataRxInfo;
    (void)bEAProtID;
    (void)bStatus;
}
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 1)
#if defined(__IAR_SYSTEMS_ICC__)
__weak
#else
__attribute__ ((weak))
#endif
void IAP2_FEAT_EA_PROT1_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus)
{
    (void)DataRxInfo;
    (void)bEAProtID;
    (void)bStatus;
}
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 2)
#if defined(__IAR_SYSTEMS_ICC__)
__weak
#else
__attribute__ ((weak))
#endif
void IAP2_FEAT_EA_PROT2_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus)
{
    (void)DataRxInfo;
    (void)bEAProtID;
    (void)bStatus;
}
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 3)
#if defined(__IAR_SYSTEMS_ICC__)
__weak
#else
__attribute__ ((weak))
#endif
void IAP2_FEAT_EA_PROT3_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus)
{
    (void)DataRxInfo;
    (void)bEAProtID;
    (void)bStatus;
}
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 4)
#if defined(__IAR_SYSTEMS_ICC__)
__weak
#else
__attribute__ ((weak))
#endif
void IAP2_FEAT_EA_PROT4_CALLBACK(IAP2_FEAT_EA_RX_DATA *DataRxInfo, uint8_t bEAProtID, uint8_t bStatus)
{
    (void)DataRxInfo;
    (void)bEAProtID;
    (void)bStatus;
}
#endif
#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 5)
#error "Maximum number of External Accessory instances is 5" 
#endif

