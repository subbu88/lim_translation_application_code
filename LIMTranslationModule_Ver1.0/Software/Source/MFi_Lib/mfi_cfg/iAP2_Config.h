/*HEADER******************************************************************************************
 *
 * Copyright 2015 Freescale Semiconductor, Inc.
 *
 * Freescale Confidential Proprietary. Licensed under the Freescale MFi Software License. See the 
 * FREESCALE_MFI_LICENSE file distributed with this work for more details. You may not use this 
 * file except in compliance with the License.
 *
 *************************************************************************************************
 *
 * THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *************************************************************************************************
 *
 * Notes: 
 *    This software/document contains information restricted to MFi licensees and subject to the 
 *    MFi license terms and conditions.
 *
 ************************************************************************************************* 
 *
 * Comments:
 *
 *
 *END*********************************************************************************************/

#ifndef IAP2_CONFIG_H_
#define IAP2_CONFIG_H_

/*************************************************************************************************/
/*                                      Includes Section                                         */
/*************************************************************************************************/
#include "iAP2_Ext_Acc_Feat.h"
#include "iAP_Config.h"

/*************************************************************************************************/
/*                                  Extern Constants Section                                     */
/*************************************************************************************************/

/*-----------------------------------------------------------------------------------------------*/
/* Name | Parameter ID 0x00 */
extern const uint8_t iAP2_Identification_baName[];
extern const uint16_t iAP2_Identification_wNameSize;
/*-----------------------------------------------------------------------------------------------*/
/* Model identifier | Parameter ID 0x01 */
extern const uint8_t iAP2_Identification_baModel[];
extern const uint16_t iAP2_Identification_wModelSize;
/*-----------------------------------------------------------------------------------------------*/
/* Manufacturer | Parameter ID 0x02 */
extern const uint8_t iAP2_Identification_baManufacturer[];
extern const uint16_t iAP2_Identification_wManufacturerSize;
/*-----------------------------------------------------------------------------------------------*/
/* Serial number | Parameter ID 0x03 */
extern const uint8_t iA2_Identification_baSerialNum[];
extern const uint16_t iA2_Identification_wSerialNumSize;
/*-----------------------------------------------------------------------------------------------*/
/* Firmware version | Parameter ID 0x04 */
extern const uint8_t iAP2_Identification_baFwVersion[];
extern const uint16_t iAP2_Identification_wFwVersionSize;
/*-----------------------------------------------------------------------------------------------*/
/* Hardware version | Parameter ID 0x05 */
extern const uint8_t iAP2_Identification_baHwVersion[];
extern const uint16_t iAP2_Identification_wHwVersionSize;

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL == _TRUE_)

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 0)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtID0;
extern const uint16_t iAP2_Identification_wSupportedExtAccProtIDSize;
extern const uint8_t iAP2_Identification_baSupportedExtAccProtName0[];
extern const uint16_t iAP2_Identification_wSupportedExtAccProtName0Size;
extern const uint8_t iAP2_Identification_bSupportedExtAccProtMatch0;
extern const uint16_t iAP2_Identification_wSupportedExtAccProtMatchSize;
extern const uint16_t iAP2_Identification_wSupportedExtAccProtNativeTransIDSize;
//#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID0 == _TRUE_)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID0[];
//#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 1)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtID1;
extern const uint8_t iAP2_Identification_baSupportedExtAccProtName1[];
extern const uint16_t iAP2_Identification_wSupportedExtAccProtName1Size;
extern const uint8_t iAP2_Identification_bSupportedExtAccProtMatch1;
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID1 == _TRUE_)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID1[];
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 2)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtID2;
extern const uint8_t iAP2_Identification_baSupportedExtAccProtName2[];
extern const uint16_t iAP2_Identification_wSupportedExtAccProtName2Size;
extern const uint8_t iAP2_Identification_bSupportedExtAccProtMatch2;
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID2 == _TRUE_)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID2[];
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 3)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtID3;
extern const uint8_t iAP2_Identification_baSupportedExtAccProtName3[];
extern const uint16_t iAP2_Identification_wSupportedExtAccProtName3Size;
extern const uint8_t iAP2_Identification_bSupportedExtAccProtMatch3;
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID3 == _TRUE_)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID3[];
#endif
#endif

#if (IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES > 4)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtID4;
extern const uint8_t iAP2_Identification_baSupportedExtAccProtName4[];
extern const uint16_t iAP2_Identification_wSupportedExtAccProtName4Size;
extern const uint8_t iAP2_Identification_bSupportedExtAccProtMatch4;
#if (IAP2_IDENT_EXT_ACC_PROTOCOL_NATIVE_TRANS_ID4 == _TRUE_)
extern const uint8_t iAP2_Identification_baSupportedExtAccProtNativeTransID4[];
#endif
#endif

#endif

#if (IAP2_IDENT_PREF_APP_BUNDLE_SEED_ID == _TRUE_)
extern const uint8_t iAP2_Identification_baPrefAppBundleSeedId[];
extern const uint16_t iAP2_Identification_wPrefAppBundleSeedIdSize;
#endif

extern const IAP2_FEAT_EA_CBK iAP2_Feat_EA_ProtCallbacks[IAP2_IDENT_SUPPORTED_EXT_ACC_PROTOCOL_INSTANCES];


#endif /* IAP2_CONFIG_H_ */
